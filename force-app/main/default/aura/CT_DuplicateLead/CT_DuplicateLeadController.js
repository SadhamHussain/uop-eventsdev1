({
    doInit: function(component, event, helper) {
        if (!$A.util.isEmpty(component.get("v.duplicateLeadList"))) {
            var dupeMap = component.get("v.duplicateLeadMap");
            var leadWrapper = component.get("v.duplicateLeadList").thisLeadResultList;
            //Populate duplicateLeadMap with Duplicate record , so that retriving particular record with Id will be efficient way in further methods
            if (!$A.util.isEmpty(leadWrapper)) {
                leadWrapper.forEach(function(returnedValue) {
                    //Populate duplicateLeadMap Record Id as key and record object as value
                    dupeMap[returnedValue.IdValue] = returnedValue;
                });
            } else {
                //Set the label as below when there is no duplicate records found for the user input data
                component.set("v.radioLabel", "New Lead will be created since no matching lead found");
            }
            component.set("v.duplicateLeadMap", dupeMap);
        }
    },

    //updateSelectedValue: Method invoked when changing the Duplicate Radio Option input 
    //                     which populates the Selected Lead Id and Selected Contact Id from the selected duplicate record radio option
    updateSelectedValue: function(component, event, helper) {
        var dupeMap = component.get("v.duplicateLeadMap");
        var target = event.target;
        component.set("v.selectedLeadValue", target.value);
        if (target.value != "CreateNewValue") {
            //Populate the Selected Lead Id and Selected Contact Id from the selected duplicate record radio option
            component.set("v.selectedContatId", dupeMap[target.value].selectedContactId);
            component.set("v.selectedLeadId", dupeMap[target.value].selectedLeadId);
            component.set("v.isContactDuplicate", dupeMap[target.value].isContact);
        } else {
            //Populate the Selected Lead Id and Selected Contact Id from the selected duplicate record radio option as null if Create new option is selected
            component.set("v.selectedContatId", null);
            component.set("v.selectedLeadId", null);
            component.set("v.isContactDuplicate", false);
        }
    }
})