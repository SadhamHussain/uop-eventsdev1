({
        doInit : function(component, event, helper) {
            // Calling getAuthrisationStatus method to getting the current record authorisation status value
            var statusVal = component.get("c.getAuthrisationStatus");
            statusVal.setParams({
                //passing current record id
                "recurringDonationId": component.get("v.recordId")
            });
            statusVal.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    //getting the authorisation status value         
                    var authStatus = response.getReturnValue();
                   
                    if(authStatus == 'Awaiting Submission'){
                        //set true to show the authorisation URL
                        component.set("v.awaitingSubmission", true);

                        // Calling getProcessAuthorisationURLasString from ct_ProcessAuthorisationForDDController class to generate process authorisation URL
                        var action = component.get("c.getProcessAuthorisationURLasString");
                        action.setParams({
                            //passing current record id
                            "recurringDonationId": component.get("v.recordId")
                        });
                        action.setCallback(this, function(response) {
                            if (response.getState() == "SUCCESS") {
                                //getting the URL
                                var urlStrValue = response.getReturnValue();
                                //assign to returnValue attribute
                                component.set("v.returnValue", urlStrValue);
                            }
                            });
                        $A.enqueueAction(action);
                        }
                        else{
                            component.set("v.awaitingSubmission", false);
                            // set true to showError attribute to show the error
                            component.set("v.showError", true);
                            component.set("v.errorMessage", 'It is only possible to create an Authorisation when the Direct Debit is in the \'Awaiting Submission\' stage');
                        } 
                    }
                    
                });
                
            $A.enqueueAction(statusVal);
        }
    })