({
    
	/*
	 * Custom validation for form entry
	 *
	 */
    
    validateForm: function(cmp) {
        
        var validationResult = false;
        var firstName = cmp.find("First Name");
        var lastName = cmp.find("Last Name");
        var emailField = cmp.find("Email"); 
        var emailFieldValue = emailField.get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/  
        var subjectOfInterest = cmp.find("Subject of Interest");
        var enquiryReason = cmp.find("Reason For Enquiry");        
        
		if(firstName.get("v.value") != "") {validationResult = true;} else {return false;}

        if(lastName.get("v.value") != "") {validationResult = true;} else {return false;}
        
        if(emailFieldValue != "") {validationResult = true;} else {return false;}
        
        if(emailFieldValue.match(regExpEmailformat)) {validationResult = true;} else {return false;}
        
        if(enquiryReason.get("v.value") == "1") {validationResult = true;} else {return false;}
        
        if(subjectOfInterest.get("v.value") != "") {validationResult = true;} else {return false;}
        
       	return validationResult;            
    },
    
	/*
	 * Event which fires the function to start a chat request (by accessing the chat API component)
	 *
	 * @param cmp - The component for this state.
	 */
    onStartButtonClick: function(cmp) {
        
        var validationChecks = this.validateForm(cmp);
        
        if(validationChecks == true)
        {
            var prechatFieldComponents = [];
            
            prechatFieldComponents.push(cmp.find("Company"));
            prechatFieldComponents.push(cmp.find("First Name"));
            prechatFieldComponents.push(cmp.find("Last Name"));
            prechatFieldComponents.push(cmp.find("Email"));
            prechatFieldComponents.push(cmp.find("Subject of Interest"));
            prechatFieldComponents.push(cmp.find("Enquiry Channel"));
            
            var fields;
            
            // Make an array of field objects for the library
            fields = this.createFieldsArray(prechatFieldComponents);
            
            
            // If the pre-chat fields pass validation, start a chat
            
            
            if(cmp.find("prechatAPI").validateFields(fields).valid) 
            {
 				cmp.find("prechatAPI").startChat(fields);
            } 

            else 
            { 
                console.warn("Prechat fields did not pass validation!");
            }
        }
        
    },

	/**
	 * Create an array of field objects to start a chat from an array of pre-chat fields
	 * 
	 * @param fields - Array of pre-chat field Objects.
	 * @returns An array of field objects.
	 */
	createFieldsArray: function(prechatFieldComponents) {
		if(prechatFieldComponents.length) {
			return prechatFieldComponents.map(function(fieldCmp) {
				return {
					label: fieldCmp.get("v.label"),
					value: fieldCmp.get("v.value"),
					name: this.fieldLabelToName[fieldCmp.get("v.label")]
				};
			}.bind(this));
		} else {
			return [];
		}
	},

    /*
	 * Map of pre-chat field label to pre-chat field name
	 */
	fieldLabelToName: {
        "First Name": "FirstName",
        "Last Name": "LastName",
        "Email": "Email",
        "Company": "Company",
        "Subject of Interest": "LiveChat_Subject__c",
        "Enquiry Channel": "Enquiry_Channel__c"
    }

});