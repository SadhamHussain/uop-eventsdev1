({
    doInit: function(component, event, helper) {
        //Invoke the force:navigateToSObject event for redirecting to the Record whose Id is passed to the recordIdValue Component Attribute
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordIdValue"),
            "slideDevName": "related"
        });
        //Fire the Navigate to Sobject Event with required parameter
        navEvt.fire();
    }
})