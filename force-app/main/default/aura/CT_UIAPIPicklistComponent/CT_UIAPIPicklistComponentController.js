({
    doInit: function(component, event, helper) {
        //Invoke helper action to get the picklist values from salesforce
        helper.getPicklistValuesBasedonRecordType(component);
    }
})