({
    //getPicklistValuesBasedonRecordType: Method which is invoked to get the dynamic picklist values for 
    //                                    the requested Object restricted by Recordtype Id
    getPicklistValuesBasedonRecordType: function(component) {

        var objectAPIName = component.get("v.objectAPIName");
        var fieldAPIName = component.get("v.fieldAPIName");
        var recordTypeIdValue = component.get("v.recordTypeId");
        //Action in the component controller to which returns the picklist values list
        var action = component.get("c.getPicklistValues");
        //Set the object name, field API name and record type Id as action parameters
        action.setParams({
            ObjectApiName: objectAPIName,
            fieldApiName: fieldAPIName,
            recordTypeId: recordTypeIdValue
        });
        //Callback function which is invoked once the Server side controller is completed
        action.setCallback(this, function(response) {
            //Invoked a helper action where server side response is processed
            this.handleResponse(response, component);
        });
        //Fire the action 
        $A.enqueueAction(action);
    },

    //handleResponse: Method to handle the response of getPicklistValuesBasedonRecordType server side action 
    handleResponse: function(response, component) {
        //Verify the state of the response of server side action is successfully
        if (response.getState() === "SUCCESS") {
            //Validate weather the value returned by the server is not empty
            if (!$A.util.isEmpty(response.getReturnValue())) {
                var picklistOptions = [];
                var noneValue = {};
                noneValue["value"] = "";
                noneValue["label"] = "--None--";
                //Create a dummy value for the picklist which is show first in the drop down initially
                picklistOptions.push(noneValue);
                var returnedValues = response.getReturnValue();
                if (!$A.util.isEmpty(returnedValues)) {
                    //Loop through the Picklist values returned by the server side action call
                    returnedValues.forEach(function(returnedValue) {
                        //Initialise an object for every picklist value which has Label, value attributes
                        var picklistValue = {};
                        //Assign the label and value attributes values from the loop variable and push it to the custom list
                        picklistValue["value"] = returnedValue.value;
                        picklistValue["label"] = returnedValue.label;
                        picklistOptions.push(picklistValue);
                    });
                    //Assign the custom object list with label value pair objects that contains the required picklist values
                    component.set("v.pickListOptions", picklistOptions);
                }
            } else {
                //If the server side action returned list is empty , the handle the error.
                //For example We can also user alert function to inform the user that the server side action have not returned any values
                console.log("Couldn't find an picklist values.");
            }
        }
        //Handle the repsonse if the server side action is failed 
        else if (response.getState() === "ERROR") {
            var errors = response.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    //Handle the server side action failure using the error message
                    console.log(errors[0].message);
                }
            }
        } else {
            console.log("Unknow error!");
        }
    },
})