import { LightningElement, track, wire, api } from 'lwc';
import getCountryValues from '@salesforce/apex/ct_CountryPicklistController.parseCountryValues';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';



export default class Ct_CountryPicklist extends LightningElement {
    @track countryValues;
    //@track spinnerBoolean;
    @track selectedCountryString;
    @wire(CurrentPageReference) pageRef;

    @api
    get isCountrySelected() {
        let target = this.template.querySelector(`[data-id="countryPicklistId"]`);
        console.log('>>>targetciun>' + target);
        target.reportValidity();
    }

    connectedCallback() {

        //Listen for country change by AFD component
        registerListener('countryupdated', this.handleMessage, this);

        //this.showSpinner();
        getCountryValues().then(data => {
            if (data) {
                this.countryValues = data;
            }

        }).catch(error => {
            //this.hideSpinner();
            this.customToastNotification('Error', error);
        });


    }

    disconnectCallback() {
        unregisterAllListeners(this);
    }

    handleMessage(myMessage) {
        console.log('myMessage' + myMessage);
        this.selectedCountryString = myMessage;
    }

    handleSelectedCountry(event) {
        var selectedCountry = event.target.value;
        if (selectedCountry) {
            const countryEvent = new CustomEvent('selectedcountry', { detail: selectedCountry });
            this.dispatchEvent(countryEvent);
        }
    }

    // disableEmptyOption(event) {
    //   //  console.log('DISABLED'+event.target.options.find(opt => opt.value == ''));
    //   //  event.target.options.find(opt => opt.value == '').disabled = true; 
    //   //  console.log('kk'); 
    //   [...this.template.querySelectorAll('.inputFieldPlaceHolder')].forEach(function(btns){
    //     var x = btns.shadowRoot.firstElementChild;
    //     console.log('len'+x);
    //   })
    // }

    // showSpinner() {
    //   this.spinnerBoolean = true;
    // }

    // hideSpinner() {
    //   this.spinnerBoolean = false;
    // }

    customToastNotification(toastTitle, toastMessage) {
        const showToastEvent = new ShowToastEvent({ Title: toastTitle, message: toastMessage, variant: 'Error' });
        this.dispatchEvent(showToastEvent);
    }
}