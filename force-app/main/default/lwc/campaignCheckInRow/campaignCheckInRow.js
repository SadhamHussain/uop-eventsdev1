import { LightningElement, api } from 'lwc';

export default class CampaignCheckInRow extends LightningElement {

    @api row;
    @api showConfirmation;

    handleCheckInAttendee()
    {
        console.log('row', this.row);
        let selectedAttendee = this.row.FirstName + ' ' + this.row.LastName;
        let detailObject = {Id:this.row.Id,
                            selectedAttendee : selectedAttendee};
        this.dispatchEvent(new CustomEvent('checkin',{detail:detailObject}));
    }

    handleCheckInConfirmation(event)
    {
        console.log(event.target.value);
        this.dispatchEvent(new CustomEvent('checkinconfirm',{detail:event.target.value}));
    }
}