import { LightningElement, api } from 'lwc';

export default class CampaignCheckInFooter extends LightningElement {

    // Api considered as a reactive public property.  
   @api totalRecords;  
   @api currentPage;  
   @api pageSize;  
   // Following are the private properties to a class.  
   lastpage = false;  
   firstpage = false;  
   // getter  
   get showFirstButton() {
       console.log('current page', this.currentPage);  
     if (this.currentPage === 1) {  
       return true;  
     }  
     return false;  
   }  
   // getter  
   get showLastButton() {  
       console.log('totalRecords', this.totalRecords);
       console.log('pageSize', this.pageSize);
     if (Math.ceil(this.totalRecords / this.pageSize) === this.currentPage) {  
       return true;  
     }  
     return false;  
   }  
   //Fire events based on the button actions  
   handlePrevious() {  
     this.dispatchEvent(new CustomEvent('previous'));  
   }  
   handleNext() {  
     this.dispatchEvent(new CustomEvent('next'));  
   }  
   handleFirst() {  
     this.dispatchEvent(new CustomEvent('first'));  
   }  
   handleLast() {  
     this.dispatchEvent(new CustomEvent('last'));  
   }  
}