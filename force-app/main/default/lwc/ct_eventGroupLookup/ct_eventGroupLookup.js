import { LightningElement, track } from 'lwc';
import searchEventGroup from '@salesforce/apex/CT_EventSearchController.getEventGroups';

export default class ct_eventGroupLookup extends LightningElement {

  @track isGroup = false;
  @track showAddress = true;
  @track groupList = [];
  @track selectedGroup = {};
  @track searchString;
  @track isLoading;
  @track timeOut;
  
  connectedCallback(){
    this.onInputValueChange();
  }

  onInputValueChange(){
    this.groupList = [];
    this.isGroup = false;
    // this.searchString = event.target.value;
    // if (event.keyCode == 27 || !this.searchString.trim()) {
    //   this.nullResultValues();
    //   const groupEvent = new CustomEvent('groupresults', { detail: '' });
    //   this.dispatchEvent(groupEvent);
    // }
    // else{
      this.isLoading = true;
     
      searchEventGroup().then( result => {
        this.isGroup = true;
        this.groupList = [];

        if(result.length > 0
          /*&& this.searchString.trim()*/){
          this.groupList = result;
        }
        // else if( !this.searchString.trim()){
        //   this.isGroup = false;
        //   this.groupList = [];
        // }
        else{
          this.groupList[0] = {'label': 'No Results Found', 'value': ''};
        }
       
        this.isLoading = false;
        }).catch(error => {
          console.log('err in');
          this.groupList = [];
          var emptyAddressList = [];
          emptyAddressList[0] = {'label': 'No Results Found', 'value': ''};
          this.groupList = emptyAddressList;
        }); 
    //}
  }
  // nullResultValues(){
  //   this.groupList = [];
  //   this.isGroup = false;
  // }

  handleSelectedGroup(event){
    var target = event.target;
    console.log('target'+target.value.split('UNIQUE_ID')[1]);
    if(target.value){
      var groupId = target.value.split('UNIQUE_ID')[1];
      const groupEvent = new CustomEvent('groupresults', { detail: groupId });
      this.dispatchEvent(groupEvent);
    }
    // if(event.currentTarget.value != 'No Results Found'){
    // var selectedSobjectIndex = this.getIndexFromParent(target, "data-selected-index");
    // selectedSobjectIndex = selectedSobjectIndex.split('UNIQUE_ID')[1];
    // this.searchString = event.currentTarget.title;
    // this.nullResultValues();
    // if(selectedSobjectIndex){
    //   console.log('selectedSobjectIndex'+selectedSobjectIndex);
    //   const groupEvent = new CustomEvent('groupresults', { detail: selectedSobjectIndex });
    //   this.dispatchEvent(groupEvent);
    // }
    // }else{
    //   this.searchString = null;
    //   this.nullResultValues();
    // }
  }

  // getIndexFromParent(target, attributeToFind){
	// 	var thisIndex = target.getAttribute(attributeToFind);
	// 	while (!thisIndex) {
	// 		target = target.parentNode;
	// 		thisIndex = this.getIndexFromParent(target, attributeToFind);
	// 	}
	// 	return thisIndex
	// }
}