import { LightningElement, api } from 'lwc';

export default class Ct_dynamicRecordEditForm extends LightningElement {
  @api recordId;
  @api objectApiName;
  @api objectFields;
}