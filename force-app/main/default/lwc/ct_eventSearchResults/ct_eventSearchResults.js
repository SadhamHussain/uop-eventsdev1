import { LightningElement, wire, track, api } from "lwc";
//import searchEventsByName from "@salesforce/apex/CT_EventSearchController.searchEventsByName";
import getEventsBySearch from "@salesforce/apex/CT_EventSearchController.getEventsBySearch";
import { ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class Ct_eventSearchResults extends LightningElement {

  //Variables used for paginator
  @track recordsToDisplay = []; //Records to be displayed on the page
  @track rowNumberOffset; //Row number
  //Variables used for paginator

  @track eventSearchResults;
  @track existingEventRegistration = false;
  eventName = "";
  eventType = "";
  eventGroup = "";
  sortvalue;
  selectedEventId;
  selectedEvent = false;

  get options() {
    return [
        { label: 'A - Z', value: 'a to z' },
        { label: 'Z - A', value: 'z to a' },
    ];
  }

  // @wire(searchEventsByName, { eventName: "$eventName" })
  // wiredEvents(result) {
  //   console.log('result'+JSON.stringify(result));
  //   this.eventSearchResults = result;
  // }

  connectedCallback(){

    /*Get Id from the URL To Load A particular Event*/
    var queryString = window.location.href;
    var urlVar = new URL(queryString);
    var eventId = urlVar.searchParams.get("id");

    if(eventId){
      this.existingEventRegistration = true;
      
      // this.searchEventRecords(this.eventName, this.eventType, this.eventGroup, eventId);
      // console.log('>>>eventrec: '+JSON.stringify(this.eventSearchResults));
      // console.log('Id>> '+eventId);
      getEventsBySearch(
        {
          byName : " ",
          byType : " ",
          byGroup: " ",
          byEventId: eventId
        }
      ).then(result => {
        console.log('fetchrec>>>'+JSON.stringify(result[0]));
        this.eventSearchResults = this.createEventObject(result);
        this.selectedEventId = JSON.stringify(this.eventSearchResults[0].Id);
      //logic for pagination
      }).catch(error => {
        console.log('ERR'+JSON.stringify(error));
        this.customToastNotification('Error', error, true);
      });
      this.selectedEvent = true;
    }
    else{
      this.searchEventRecords(this.eventName, this.eventType, this.eventGroup, eventId);
    // console.log('>>>eventrec1: '+this.eventSearchResults);
    }
    //console.log('>>>selectedEventId>>: '+this.selectedEventId);
  }

  @api
  searchEventRecords(byName, byType, byGroup, byEventId){
    getEventsBySearch(
      {
        byName : byName,
        byType : byType,
        byGroup: byGroup,
        byEventId: byEventId
      }
    ).then(result => {
      console.log('fetchrec>>>');
      this.eventSearchResults = this.createEventObject(result);

      if(byName || byType || byGroup){
        this.recordsToDisplay = this.eventSearchResults;
        this.rowNumberOffset = this.recordsToDisplay[0].rowNumber-1;
     }
    //logic for pagination
    }).catch(error => {
      console.log('ERR'+JSON.stringify(error));
      this.customToastNotification('Error', error, true);
    });
  }

  sortSearchResults(event){
    if(event.target.value == 'a to z'){
      this.recordsToDisplay.sort(this.compare);
     this.recordsToDisplay = this.createEventObject(this.recordsToDisplay);
    }
    else if(event.target.value == 'z to a'){
      this.recordsToDisplay.reverse();
     this.recordsToDisplay = this.createEventObject(this.recordsToDisplay);
    }
  }

  compare(a, b) {
    // Use toUpperCase() to ignore character casing
    
    const webNameA = a.Web_Event_Name__c.toUpperCase();
    const webNameB = b.Web_Event_Name__c.toUpperCase();
  
    let comparison = 0;
    if (webNameA > webNameB) {
      comparison = 1;
    } else if (webNameA < webNameB) {
      comparison = -1;
    }
    return comparison;
  }
  
  createEventObject(result){
    console.log('getrec>>>');
    let recs = [];
    for(let i=0; i<result.length; i++){
        let eventRecord = {};
        eventRecord.rowNumber = ''+(i+1);
        eventRecord.eventRecordLink = '/'+result[i].Id;
        eventRecord = Object.assign(eventRecord, result[i]);
        recs.push(eventRecord);
    }
    console.log('recs: '+JSON.stringify(recs));
    return recs;
  }

  customToastNotification(toastTitle, toastMessage, isErrorMessage) {
    var messageString = isErrorMessage ? 'Error' : 'Success';
    const showToastEvent = new ShowToastEvent({
      Title: toastTitle,
      message: toastMessage,
      variant: messageString
    });
    this.dispatchEvent(showToastEvent);
  }

  //Capture the event fired from the paginator component
  handlePaginatorChange(event){
    this.recordsToDisplay = event.detail;
    this.rowNumberOffset = this.recordsToDisplay[0].rowNumber-1;
  }
}