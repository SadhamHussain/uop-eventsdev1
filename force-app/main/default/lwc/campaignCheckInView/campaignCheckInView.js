import { LightningElement, track, api } from 'lwc';
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import GetCampaignMemberCount from '@salesforce/apex/bg_CampaignCheckinHandlerController.GetCampaignMemberCount';
import GetCampaignMembersList from '@salesforce/apex/bg_CampaignCheckinHandlerController.GetCampaignMembersList'

export default class CampaignCheckInView extends LightningElement 
{

    @api currentpage;
    @api pagesize;
    @api campaignId;
    @track searchKey;
    @track campaignMembers;
    @track displayResults;
    @track error;
    @track totalpages;
    @track localCurrentPage = null;
    @track isSearchChangeExecuted = false;
    @track totalRecords;
    @track showSpinner;
    @track showTable = false;
    @track selectedAttendee;
    @track selectedAttendeeId;

    get hasRecords()
    {
        return this.totalRecords > 0;
    }

    renderedCallback()
    {
        if (this.isSearchChangeExecuted && (this.localCurrentPage === this.currentpage))
        {
            // this.showSpinner = false;
            return;
        }

        this.isSearchChangeExecuted = true;
        this.localCurrentPage = this.currentpage;
        this.showSpinner = true;
        GetCampaignMemberCount({searchString : this.searchKey,
                                campaignIdStr : this.campaignId})
        .then( recordsCount =>{
            console.log('recordsCount', recordsCount);
            this.totalRecords = recordsCount;
            this.showSpinner = false;
            if (recordsCount !== 0 && !isNaN(recordsCount))
            {
                this.showSpinner = true;
                this.totalpages = Math.ceil(recordsCount / this.pagesize);
                GetCampaignMembersList({pageNumber: this.currentpage,
                                        numberOfRecords: recordsCount,
                                        pageSize: this.pagesize,
                                        searchString : this.searchKey,
                                        campaignIdStr : this.campaignId})
                .then(response =>{
                    this.showSpinner = false;
                    console.log('response', response);
                    this.campaignMembers = response;
                    this.buildDisplayResults();
                    this.error = undefined;
                })
                .catch(error =>{
                    this.showSpinner = false;
                    this.campaignMembers = undefined;
                    this.error = error;
                })
            }
            else
            {
                console.log('no results');
                this.buildDisplayResults();
                this.campaignMembers = {};
                this.totalpages = 1;
                this.totalRecords = 0;
                this.showTable = false;
            }
            

            const recordsLoadEvent = new CustomEvent('recordsload', {
                detail: recordsCount
            });
            this.dispatchEvent(recordsLoadEvent);
        })
        .catch(error =>{
            console.log('error', error);
            this.error = error;
            this.totalRecords = undefined;
        })
    }

    handleKeyChange(event)
    {
        this.selectedAttendeeId = undefined;
        console.log('key change');
        if (this.searchKey !== event.target.value)
        {
            console.log('key change update');
            this.isSearchChangeExecuted = false;
            this.searchKey = event.target.value;
            this.currentpage = 1;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 21st January 2020
	//  @description: A method to handle the clearing of the search term
	//
	//  @changes:	
    //  @usage: 
	////////////////////////////////////////////////////////////////////////////
    buildDisplayResults()
    {
        let displayResults = [];
        let campaignMembersList = [];

        console.log('campaign members', this.campaignMembers);
        campaignMembersList = Object.values(this.campaignMembers);
        campaignMembersList.forEach(campaignMember => {
            let result = {};

            result.Id = campaignMember.Id;
            result.FirstName = campaignMember.FirstName;
            result.LastName = campaignMember.LastName;
            result.Email = campaignMember.Email;
            if (this.selectedAttendeeId !== result.Id)
            {
                result.showConfirmation = false;
            }
            else
            {
                result.showConfirmation = true;
            }

            if (campaignMember.Status === "Checked In")
            {
                result.IsCheckedIn = true;
            }
            else 
            {
                result.IsCheckedIn = false;
            }

            displayResults.push(result);
        });

        if (displayResults.length === 0)
        {
            displayResults = undefined;
        }
        this.displayResults = [];
        this.displayResults = displayResults;
        this.showTable = true;
    }

    
    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 21st January 2020
	//  @description: A method to 
	//
	//  @changes:	
    //  @usage: 
    ////////////////////////////////////////////////////////////////////////////
    
    handleCheckIn(event)
    {
        console.log('ID', event.detail.Id);
        console.log('event.target', event.target);
        this.selectedAttendee = event.detail.selectedAttendee;
        this.selectedAttendeeId = event.detail.Id;

        console.log('campaign members', this.campaignMembers);
        console.log('select attendee ID', this.selectedAttendeeId);
        if (this.campaignMembers.hasOwnProperty(this.selectedAttendeeId))
        {
            this.campaignMembers[this.selectedAttendeeId].showConfirmation = true;
        }
        this.buildDisplayResults();
    }

    handleCheckInConfirm(event)
    {
        console.log('button', event.detail);
        console.log('BR',this.selectedAttendeeId);
        if (event.detail === "yes")
        {
            this.checkInAttendeeConfirm(this.selectedAttendeeId);
        }
        this.selectedAttendeeId = undefined;
        this.buildDisplayResults();

    }


    checkInAttendeeConfirm(id)
    {
        const campaignMemberId = id;

        if (campaignMemberId)
        {
            this.checkInAttendee(campaignMemberId);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 21st January 2020
	//  @description: A method to 
	//
	//  @changes:	
    //  @usage: 
	////////////////////////////////////////////////////////////////////////////
    checkInAttendee(campaignMemberId)
    {
        this.showModal = false;
        this.showSpinner = true;

        const fields = {};

        fields["Id"] = campaignMemberId;
        fields["Status"] = "Checked In";
        
        const record = {fields};

        updateRecord(record)
            .then(() => {
                this.selectedAttendeeId = undefined;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Attendee Successfully Checked-In',
                        variant: 'success'
                    })
                )
                this.updateDataset(campaignMemberId);
            })
            .catch(error => {
                console.log("error", error);
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error Checking In Attendee',
                        message: error,
                        variant: 'error'
                    })
                );
            }).finally(() => {
                this.showSpinner = false;
            });
    }

    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 21st January 2020
	//  @description: A method to 
	//
	//  @changes:	
    //  @usage: 
	////////////////////////////////////////////////////////////////////////////
    updateDataset(campaignMemberId)
    {
        console.log(typeof this.campaignMembers);
        if (this.campaignMembers.hasOwnProperty(campaignMemberId))
        {
            let record = this.campaignMembers[campaignMemberId]; 
            
            record.Status = "Checked In";

            this.buildDisplayResults();
        }
    }
}