import { LightningElement, api } from "lwc";

export default class Ct_ticketTile extends LightningElement {
  @api eventTicket;
  @api selectedTicketCount = '';

  get availableTickets() {
    let availableTickets = [];
    for (let i = 1; i <= this.eventTicket.Available_Tickets__c; i++) {
      availableTickets.push({ label: "" + i, value: "" + i });
    }
    return availableTickets;
  }

  handleTicketCountChange(event) {
    console.log('count'+event.detail.value);
    this.selectedTicketCount = event.detail.value;
  }
}