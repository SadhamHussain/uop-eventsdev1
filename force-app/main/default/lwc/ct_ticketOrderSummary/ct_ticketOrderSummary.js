import { LightningElement, api } from "lwc";
import formFactorPropertyName from "@salesforce/client/formFactor";
import saveEventTicketOrder from '@salesforce/apex/CT_EventSearchController.saveEventTicketOrder';
import { ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class Ct_ticketOrderSummary extends LightningElement {
  @api tickets;
  @api orderedBy;
  @api orderedTickets;
  urlIdparam;

  columns = [
    { label: "Ticket Type", fieldName: "name", type: "text" },
    { label: "Price Per Ticket", fieldName: "ticketPrice", type: "currency" },
    { label: "Quantity", fieldName: "ticketCount", type: "number" },
    { label: "Total", fieldName: "totalPrice", type: "currency" }
  ];

  get totalPrice() {
    let totalPrice = 0;
    totalPrice = this.tickets.reduce((total, each) => {
      
      return total.totalPrice + each.totalPrice;
    });
    return totalPrice;
  }

  handleConfirmOrder(event) {
    var queryString = window.location.href;
    var urlVar = new URL(queryString);
    this.urlIdparam = urlVar.searchParams.get("id");
    console.log("handleConfirmOrder " + this.orderedBy);
    console.log("handleConfirmOrder " + this.orderedBy.lastName);
    console.log("handleConfirmOrder " + this.orderedTickets.length);
    console.log(JSON.stringify(this.orderedBy));
    console.log(JSON.stringify(this.orderedTickets));
    console.log("urlIdparam " + this.urlIdparam);
    saveEventTicketOrder({ticketOrderAsJson: JSON.stringify(this.orderedBy), eventRegistrationAsJson: JSON.stringify(this.orderedTickets), urlParamId: this.urlIdparam})
        .then(result => {
          
            console.log('--->'+result);
            this.customToastNotification('Success', 'Event registered successfully', false);
            window.location.reload();
        })
        .catch(error => {
          console.log('ERROR--->'+JSON.stringify(error));          
            // TODO Error handling
        });
    
  }

  customToastNotification(toastTitle, toastMessage, isErrorMessage) {
    var messageString = isErrorMessage ? 'Error' : 'Success';
    const showToastEvent = new ShowToastEvent({
      Title: toastTitle,
      message: toastMessage,
      variant: messageString
    });
    this.dispatchEvent(showToastEvent);
  }

  handleCancelOrder(event) {
    console.log("handleCancelOrder");
  }
}