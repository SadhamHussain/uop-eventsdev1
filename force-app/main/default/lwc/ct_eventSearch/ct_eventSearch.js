import { LightningElement, track } from 'lwc';
import { ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class Ct_eventSearch extends LightningElement {
    eventSelected = false;
    selectedEventId;
    eventId;    
    @track showSearchForm = false;

    connectedCallback(){
       /*Get Id from the URL To Load A particular Event*/
       var queryString = window.location.href;
       var urlVar = new URL(queryString);
       this.eventId = urlVar.searchParams.get("id");
 
       if(this.eventId){
         this.showSearchForm = false;
       }
       else{
        this.showSearchForm = true;
       }
    }

    handleSearch(event) {
        let eventName = event.detail.eventName;
        let eventType = event.detail.eventType;
        let eventGroup = event.detail.eventGroup;   
          
        try {            
          this.template.querySelector("c-ct_event-search-results").searchEventRecords(eventName, eventType, eventGroup, '');               
        } catch (error) {
          console.log('ERR'+JSON.stringify(error));
          this.customToastNotification('Error', error, true);
        } 
    }

    customToastNotification(toastTitle, toastMessage, isErrorMessage) {
      var messageString = isErrorMessage ? 'Error' : 'Success';
      const showToastEvent = new ShowToastEvent({
        Title: toastTitle,
        message: toastMessage,
        variant: messageString
      });
      this.dispatchEvent(showToastEvent);
    }

    handleEventSelection(event) {
        this.selectedEventId = event.detail.selectedEventId;
        this.eventSelected = true;
    }

}