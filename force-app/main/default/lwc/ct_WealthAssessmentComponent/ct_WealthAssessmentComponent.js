import { LightningElement, api, track, wire } from 'lwc';
import getWealthAssessmentRecord from '@salesforce/apex/ct_WealthAssessmentComponentController.getWealthAssessmentRecord';
import getOpportunityRecord from '@salesforce/apex/ct_WealthAssessmentComponentController.updateOpportunityRecord';
import clearOpportunityValues from '@salesforce/apex/ct_WealthAssessmentComponentController.clearWealthAssessmentValuesFromOpportunity';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { registerListener, fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';


export default class ct_WealthAssessmentComponent extends LightningElement {
  @api recordId;
  @wire(CurrentPageReference) pageRef;
  @track wealthAssessmentLink;
  @track bureauLink;
  @track wealthAssessmentName;
  @track bureauName;
  @track showSpinnerBoolean = false;
  @track dateOfWealthScreen;
  @track wealthIndicator;
  @track propertyIndicator;
  @track capacityScore;
  @track isOpportunityWon = false;
  @track wealthAssessmentList;
  @track selectedWealthAssessment;

  get isDisabled(){
    return this.isOpportunityWon ? true : false;
  }

  connectedCallback(){
    this.showSpinner();
    registerListener('refresh', this.handleRefresh, this);

    this.loadWealthAssessmentComponent();
  }

  loadWealthAssessmentComponent(){
    getOpportunityRecord({
      wealthAssessmentId : null,
      opportunityId : this.recordId
      }).then(result => {
        this.wealthAssessmentList = result.assessmentPickListWrapperList;
        this.loadWealthAssessmentValues(result);
        this.hideSpinner();
      }).catch(error => {
        console.log('ERR '+JSON.stringify(error));
        this.hideSpinner();
        this.customToastNotification(error);
    });
  }

  fetchWealthAssessmentRecord(event){
    if(event.target.value){
      this.showSpinner();
      getWealthAssessmentRecord({
      wealthAssessmentId : event.target.value,
      opportunityId : this.recordId
      }).then(result => {
        this.loadWealthAssessmentValues(result);
        this.hideSpinner();
      }).catch(error => {
        console.log('ERR '+JSON.stringify(error));
        this.hideSpinner();
        this.customToastNotification(error);
      }); 
    }
  }

  loadWealthAssessmentValues(result){
    this.wealthAssessmentName = result.wealthAssessmentName;
    this.bureauName = result.bureauName;
    this.bureauLink = result.bureauName ? window.location.origin+'/'+result.thisOpportunityRecord.Wealth_Screening_Bureau__c : '';
    this.wealthAssessmentLink = result.wealthAssessmentName ? window.location.origin+'/'+result.thisOpportunityRecord.Wealth_Assessment__c : '';
    if(result.thisOpportunityRecord.Date_Of_Wealth_Screen__c){
      this.dateOfWealthScreen = result.thisOpportunityRecord.Date_Of_Wealth_Screen__c.split('-')[2]+'-'+result.thisOpportunityRecord.Date_Of_Wealth_Screen__c.split('-')[1]+'-'+result.thisOpportunityRecord.Date_Of_Wealth_Screen__c.split('-')[0];
    }
    this.wealthIndicator = result.thisOpportunityRecord.Wealth_Indicator__c;
    this.propertyIndicator = result.thisOpportunityRecord.Property_Indicator__c;
    this.capacityScore = result.thisOpportunityRecord.Capacity_Score__c;
    this.isOpportunityWon = result.thisOpportunityRecord.StageName == 'Closed Won' ? true : false;
  }

  handleDeleteWealthAssessment(){
    this.showSpinner();
    this.wealthAssessmentLink = null;
    this.bureauLink = null;
    this.wealthAssessmentName = null;
    this.bureauName = null;
    this.dateOfWealthScreen = null;
    this.wealthIndicator = null;
    this.propertyIndicator = null;
    this.capacityScore = null;

    this.selectedWealthAssessment = null;

    clearOpportunityValues({
      opportunityId : this.recordId
    }).then(result =>{
      this.hideSpinner();
    }).catch(error => {
      console.log('ERR '+JSON.stringify(error));
      this.hideSpinner();
      this.customToastNotification(error);
    }); 
  }

  showSpinner() {
    this.showSpinnerBoolean = true;
  }

  hideSpinner() {
    this.showSpinnerBoolean = false;
  }

  customToastNotification(toastTitle, toastMessage, isErrorMessage) {
    this.hideSpinner();
    var messageString = isErrorMessage ? 'Error' : 'Success';
    const showToastEvent = new ShowToastEvent({title: toastTitle, message : toastMessage, variant: messageString});
    this.dispatchEvent(showToastEvent);
  }

  handleRefresh() {
    this.wealthAssessmentLink = null;
    this.bureauLink = null;
    this.wealthAssessmentName = null;
    this.bureauName = null;
    this.dateOfWealthScreen = null;
    this.wealthIndicator = null;
    this.propertyIndicator = null;

    this.loadWealthAssessmentComponent();
  }

  disconnectCallback() {
    unregisterAllListeners(this);
  }
}