import {
  LightningElement,
  api,
  wire,
  track
} from 'lwc';
import {
  loadScript
} from 'lightning/platformResourceLoader';
import chartjs from '@salesforce/resourceUrl/Chart';
import fetchOpportunityFieldHistory from '@salesforce/apex/ct_AmountChartComponentController.getLineChartMap';
import {
  registerListener,
  fireEvent
} from 'c/pubsub';
import {
  CurrentPageReference
} from 'lightning/navigation';


export default class ct_OpportunityAmountChangesChart extends LightningElement {
  @wire(CurrentPageReference) pageRef;
  @api recordId;
  error;
  chart;
  chartjsInitialized = false;

  @track config = [];
  @track labelSet = [];
  @track dataSet = [];
  @track tempValue = [];
  @track noDataToDrawCart = false;


  connectedCallback() {
    registerListener('refresh', this.handleRefresh, this);
    if (this.chartjsInitialized) {
      return;
    }
    this.chartjsInitialized = true;
    Promise.all([
        loadScript(this, chartjs)
      ])
      .then(() => {
        this.constructLineChart();
      })
      .catch(error => {
        this.error = error;
      });
  }
  errorCallback(error) {
    this.error = error;
  }
  constructLineChart() {
    fetchOpportunityFieldHistory({
      opportunityId: this.recordId
    }).then(result => {
      this.tempValue = JSON.parse(result);
      if (this.tempValue.length > 0) {
        for (var a = 0; a < this.tempValue.length; a++) {
          if (this.tempValue[a] != null) {
            this.labelSet.push(this.tempValue[a]["label"]);
            this.dataSet.push(this.tempValue[a]["firstValue"]);
          }
        }
        this.config = {
          type: 'line',
          data: {
            datasets: [{
              data: this.dataSet,
              backgroundColor: [
                'rgb(0,128,0,0.5)'
              ],
              label: 'Amount Changes'
            }],
            labels: this.labelSet
          },
          options: {
            responsive: true,
            legend: {
              position: 'top'
            },
            animation: {
              animateScale: true,
              animateRotate: true
            }
          }
        };
        const ctx = this.template.querySelector("canvas.stepped").getContext("2d");
        this.steppedChart = new window.Chart(ctx, this.config);
      } else {
        this.noDataToDrawCart = true;
      }
    }).catch(error => {});
  }
  handleRefresh() {
    this.noDataToDrawCart = false;
    this.tempValue = [];
    this.labelSet = [];
    this.dataSet = [];
    this.config = [];
    this.constructLineChart();
  }

  disconnectCallback() {
    unregisterAllListeners(this);
  }

}