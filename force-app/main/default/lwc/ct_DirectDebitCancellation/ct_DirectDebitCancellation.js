import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getInitialWrapper from '@salesforce/apex/ct_DirectDebitCancellationController.fetchInitialWrapper';
import invokeCancellationProcess from '@salesforce/apex/ct_DirectDebitCancellationController.cancelDirectDebit';
import { NavigationMixin } from 'lightning/navigation';

export default class Ct_DirectDebitCancellation extends NavigationMixin(LightningElement) {
    @api directDebitRecId;
    @track showSpinnerBoolean = true;
    @track isConfirmed = false;
    @track isCancellationSuccessfull = false;
    @track showModal = false;
    @track thisDirectDebit;
    @track pickListValuesObject;

    get nextDisabled() {
        return !this.isConfirmed;
    }

    connectedCallback() {
        getInitialWrapper({
                directDebitId: this.directDebitRecId
            }).then(result => {
                if (result != undefined &&
                    result != null) {
                    this.thisDirectDebit = result.thisRecurringDonation;
                    this.pickListValuesObject = result.picklistFields;
                    console.log('result>>>>>>>>>>>.', JSON.stringify(result));
                    this.isCancellationSuccessfull = this.thisDirectDebit.Direct_Debit_Status__c == 'Cancelled' ? true : false;
                    this.showModal = true;
                }

            })
            .catch(error => {
                this.customToastNotification('Error', error.body.message, true);
            });

        this.showSpinnerBoolean = false;
    }

    customToastNotification(toastTitle, toastMessage, isErrorMessage) {
        this.showSpinnerBoolean = false;
        var messageString = isErrorMessage ? 'Error' : 'Success';
        const showToastEvent = new ShowToastEvent({
            Title: toastTitle,
            message: toastMessage,
            variant: messageString
        });
        this.dispatchEvent(showToastEvent);
    }

    handleConfirmChange(event) {
        this.isConfirmed = event.target.checked;
    }

    onInputFieldOnChange(event) {
        this.thisDirectDebit[event.target.name] = event.target.value;
    }
    handleNext() {
        this.showSpinnerBoolean = true;

        const allValid = [...this.template.querySelectorAll('.formInput')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        if (!allValid) {
            this.customToastNotification('Error', 'Please provide cancellation reason and description', true);

        } else {
            invokeCancellationProcess({
                    directDebitObject: JSON.stringify(this.thisDirectDebit)
                }).then(result => {

                    this.customToastNotification('Success', 'Direct Debit Cancelled Successfully', false);
                    this.redirectToDDRecord();
                })
                .catch(error => {
                    this.customToastNotification('Error', error.body.message, true);
                });
        }
    }
    redirectToDDRecord() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": this.directDebitRecId,
                "actionName": "view"
            }
        });
    }
}