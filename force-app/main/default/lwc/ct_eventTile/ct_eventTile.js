import { LightningElement, api, track } from "lwc";
import formFactorPropertyName from '@salesforce/client/formFactor';

export default class Ct_eventTile extends LightningElement {
  @api eventDetail;
  @api selectedEventId;
  @track formattedTime;

  get getFormFactor(){
    let isMobile;
    if (formFactorPropertyName === 'Small') {
      isMobile = true;
    }
    return isMobile;
  }

  get formattedTimeValue() {
    return (
      this.convertMilliSecondsToHHMM(this.eventDetail.Start_Time__c) +
      " - " +
      this.convertMilliSecondsToHHMM(this.eventDetail.End_Time__c)
    );
  }

  @api
  set formattedTimeValue(value) {
    this.setAttribute("formattedTimeValue", value);
    this.formattedTimeValue = value;
  }

  handleEventClick(event) {
    event.preventDefault();
    this.selectedEventId = this.eventDetail.Id;
    this.dispatchEvent(new CustomEvent('selectedevent', { bubbles: true, detail: {selectedEventId : this.selectedEventId}}));
  }

  convertMilliSecondsToHHMM(durationInMs) {
    let milliseconds = parseInt((durationInMs % 1000) / 100),
      seconds = parseInt((durationInMs / 1000) % 60),
      minutes = parseInt((durationInMs / (1000 * 60)) % 60),
      hours = parseInt((durationInMs / (1000 * 60 * 60)) % 24);

    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    return hours + ":" + minutes;
  }
}