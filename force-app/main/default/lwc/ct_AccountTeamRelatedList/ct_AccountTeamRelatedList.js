import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getAccountTeamList from '@salesforce/apex/ct_AccountTeamRelatedListController.getAccountTeamMemberList';

export default class Ct_AccountTeamRelatedList extends NavigationMixin(LightningElement) {
    @api recordId;
    @track showSpinnerBoolean = true;
    @track showListTable = false;
    @track accountMemberList;
    accountId;
    contact;

    get listSize() {
        return this.accountMemberList.length;
    }

    get listSizeBody() {
        return this.accountMemberList.length > 1 ? this.accountMemberList.length.toString() + ' items' : this.accountMemberList.length.toString() + ' item';
    }

    connectedCallback() {
        getAccountTeamList({
                contactId: this.recordId
            }).then(result => {
                if (result != undefined &&
                    result != null &&
                    result.length > 0) {
                    this.accountMemberList = result;
                    for (var i = 0; i < this.accountMemberList.length; i++) {
                        this.accountMemberList[i]["IndexNo"] = i + 1;
                    }
                    this.showListTable = true;
                }
            })
            .catch(error => {
                this.customToastNotification('Error', error.body.message, true);
            });
        this.showSpinnerBoolean = false;
    }

    redirectToTeamMember(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": event.target.title,
                "actionName": "view"
            }
        });
    }

    customToastNotification(toastTitle, toastMessage, isErrorMessage) {
        this.showSpinnerBoolean = false;
        var messageString = isErrorMessage ? 'Error' : 'Success';
        const showToastEvent = new ShowToastEvent({
            Title: toastTitle,
            message: toastMessage,
            variant: messageString
        });
        this.dispatchEvent(showToastEvent);
    }
}