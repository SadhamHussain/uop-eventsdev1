/**
 * @File Name          : ct_YoutubeEmbed.js
 * @Description        : Training Video Embed controller supports on Record Page
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/01/2020, 01:35:00 PM
 * @Modification Log   : 
 * Ver       Date            Author       Modification
 * 1.0    4/29/2020   Creation Admin     Initial Version
**/
import {  LightningElement,  wire,  api} from 'lwc';
import {  getRecord} from 'lightning/uiRecordApi';
import getURL from '@salesforce/apex/ct_YouTubeVideoMapping.getURL';
import watchtrainingVideo from '@salesforce/label/c.ct_Watch_Training_Video';

export default class Ct_YoutubeEmbed extends LightningElement {
  @api recordId;
  error = '';
  showVideo = false;
  responseData = [];
  showCard = false;
  label={
    watchtrainingVideo
  };
  @wire(getURL, {    recordId: '$recordId'  })
  wiredURL({    error,    data  }) {
    if (data) {
      this.error = undefined;
      if(data['result'] && data['result'].length>0){
        this.responseData = data['result'];
        this.showCard = true;
       // this.activeSectionName = this.responseData[0].DeveloperName;
      }
    } else if (error) {
      this.error = data['error'];
      console.log('error '+this.error);
    }
  };
  handleClick(event) {
    window.open(event.target.name);
  }
  show_Video(){
    this.showVideo = true;
  }
  hide_Video(){
    this.showVideo = false;
  }
}