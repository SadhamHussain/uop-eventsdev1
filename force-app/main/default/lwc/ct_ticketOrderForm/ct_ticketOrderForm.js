import { LightningElement, api } from "lwc";

export default class Ct_ticketOrderForm extends LightningElement {
  firstName;
  lastName;
  email;
  @api eventId;
  @api selectedTickets;
  displaySummary = false;
  formTitle = "Ticket Order";
  orderedBy;
  orderedTickets;  

  handleFirstName(event) {
    this.firstName = event.target.value;
  }

  handleLastName(event) {
    this.lastName = event.target.value;
  }

  handleEmail(event) {
    this.email = event.target.value;
  }

  get ticketForms() {
    let ticketForms = [];
    let totalTickets = 0;
    for (let i = 0; i < this.selectedTickets.length; i++) {    
      for (let j = 0; j < parseInt(this.selectedTickets[i].ticketCount); j++) {
        console.log("...>"+this.selectedTickets[i].ticketId);
        ++totalTickets;
        ticketForms.push({
          ticketId: this.selectedTickets[i].ticketId,
          name: this.selectedTickets[i].name,
          ticketNumber: totalTickets
        });
      }
    }
    return ticketForms;
  }

  handleDisplaySummary(event) {

    const allValid = [...this.template.querySelectorAll('.ticketOrderField')]
            .reduce((validSoFar, inputCmp) => {
              console.log('II');
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
    
    if (allValid) {
      this.formTitle = "Order Summary";
      this.orderedBy = {
        firstName: this.firstName,
        lastName: this.lastName,
        email: this.email,
        eventId: this.eventId
      };
      try {
        let tickets = [];
        let ticketForms = this.template.querySelectorAll("c-ct_ticket-form");
        for (let i = 0; i < ticketForms.length; i++) {
          let eachTicketForm = ticketForms[i];
          // eachTicketForm.reportValidation();
          // if(eachTicketForm.allValid){
            //console.log("eachTicketForm.ticket.Id "+eachTicketForm.ticket.ticketId);
            tickets.push({
              ticketId: eachTicketForm.ticket.ticketId,
              firstName: eachTicketForm.firstName,
              lastName: eachTicketForm.lastName,
              email: eachTicketForm.email,
              isGuest: eachTicketForm.isGuest
            });
          }
          this.orderedTickets = tickets;
          this.displaySummary = true;
        //}
      } catch (error) {
        console.log(error);
      }
    }
  }

  handleConfirmOrder(event) {
    console.log("handleConfirmOrder");
  }
}