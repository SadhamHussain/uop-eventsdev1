import { LightningElement, api } from 'lwc';

export default class Ct_eventRegistrationRecordEditForm extends LightningElement {
  @api recordId;
  @api objectApiName;
}