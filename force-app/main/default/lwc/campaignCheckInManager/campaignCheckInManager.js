import { LightningElement, track, api, wire } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import { getRecord, updateRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import QRSCRIPTS from '@salesforce/resourceUrl/QRScripts';

// import EVENT_SOLD_OUT from '@salesforce/schema/EventApi__Event__c.EventApi__Sold_Out__c';
// import EVENT_URL from '@salesforce/schema/EventApi__Event__c.Consumable_Event_URL__c';


const PAGE_SIZE = 25;
export default class CampaignCheckInManager extends LightningElement 
{
    @api recordId;

    @api page = 1;
    @track totalrecords;
    @api _pagesize = PAGE_SIZE;

    @track totalPages;

    get hasRecords()
    {
        return this.totalrecords > 0;
    }
    
    get pagesize()
    {
        return this._pagesize;
    }
    set pagesize(value)
    {
        this._pagesize = value;
    }

    handlePrevious()
    {
        if (this.page > 1)
        {
            this.page = this.page - 1;
        }
    }

    handleNext()
    {
        if (this.page < this.totalPages)
        {
            this.page = this.page + 1;
        }
    }

    handleFirst()
    {
        this.page = 1;
    }

    handleLast()
    {
        this.page = this.totalPages;
    }

    handleRecordsLoad(event)
    {
        this.totalrecords = event.detail;
        this.totalPages = Math.ceil(this.totalrecords / this.pagesize);
    }

    handlePageChange(event)
    {
        this.page = event.detail;
    }

/*     // @track cardTitle;
    @track showScanComponent;
    @track showSearchComponent;
    @track showRegisterComponent;
    @track showMenu;

    @track showSpinner;
    @track pendingPromiseResponse;

    // @wire(getRecord, { recordId: '$recordId', fields: [EVENT_SOLD_OUT, EVENT_URL] }) eventRecord;

    connectedCallback()
    {
        this.initialise();
    }
    
    renderedCallback()
    {
         window.addEventListener("resize", this.handleResize.bind(this));
         
        // this.handleResize();
        this.loadScripts();
    }

    initialise()
    {
        this.showSpinner = true;
        // this.showSearchComponent = false;
        this.showSearchComponent = true;
        this.showMenu = true;
        this.pendingPromiseResponse = false;

        // this.cardTitle = "Home";
    }

    loadScripts()
    {
        loadScript(this, QRSCRIPTS + '/qr_packed.js').then(() => {
            console.log('QR Packed script loaded successfully.');
            this.showSpinner = false;
        }).catch(error =>{
            console.log(error);
        });
    }

    handleScanClick(event)
    {
        let input = this.template.querySelector(".qr-scanner");
        input.click();
    }

    handleSearchClick(event)
    {
        this.showSearchComponent = true;
        this.showMenu = false;
    }

    handleRegisterClick(event)
    {
        const eventURL = getFieldValue(this.eventRecord.data, EVENT_URL);
        window.open(eventURL);
    }

    handleResize(event)
    {
        if (this.showMenu)
        {
            let menuItems = this.template.querySelectorAll('.resizable-element');

            if (menuItems)
            {
                const width = window.getComputedStyle(menuItems[0]).width;

                menuItems.forEach(item => {
                    item.style.height = width;
                });
            }
        }
    }

    handleBackClick()
    {
        this.showSearchComponent = false;
        this.showMenu = true;
    }

    handleFilesChange(event)
    {
        if (event.target.files.length > 0) 
        {
            const filesUploaded = event.target.files;
            const file = filesUploaded[0];

            let reader = new FileReader();
            let comp = this;
            
            this.showSpinner = true;

            reader.onload = function() 
            {
                qrcode.callback = function(res) 
                {
                    if(res instanceof Error) 
                    {
                        alert("No QR code found. Please make sure the QR code is within the camera's frame and try again.");
                        this.showSpinner = false;
                    } 
                    else 
                    {
                        if (res.includes("/CheckInHandler?"))
                        {
                            let token = "?cmid=";
                            let position = res.indexOf(token);
                            let campaignMemberId = res.substring((position + token.length));

                            const fields = {};

                            fields["Id"] = campaignMemberId;
                            fields["Status"] = "Checked In";

                            const record = {fields};

                            comp.pendingPromiseResponse = true;

                            updateRecord(record).then(() => {
                                comp.dispatchEvent(
                                    new ShowToastEvent({
                                        title: 'Success',
                                        message: 'Attendee Successfully Checked-In',
                                        variant: 'success'
                                    })
                                );

                                comp.showSpinner = false;
                                comp.pendingPromiseResponse = false;
                            })
                            .catch(error => {
                                comp.dispatchEvent(
                                    new ShowToastEvent({
                                        title: 'Error Checking In Attendee',
                                        message: error.body.message,
                                        variant: 'error'
                                    })
                                );

                                comp.showSpinner = false;
                                comp.pendingPromiseResponse = false;
                            });
                        }
                        
                        alert("QR Code processed.");
                    }
                };

                qrcode.decode(reader.result);
            };

            reader.readAsDataURL(file);      
        }
    } */
}