import { LightningElement, api } from "lwc";

export default class Ct_ticketForm extends LightningElement {
  @api ticket;
  @api firstName;
  @api lastName;
  @api email;
  @api allValid;
  @api isGuest = false;
  attendeeType;

  handleFirstName(event) {
    this.firstName = event.detail.value;
  }

  handleLastName(event) {
    this.lastName = event.detail.value;
  }

  handleEmail(event) {
    this.email = event.detail.value;
  }

  get attendees() {
    let attendess = [];
    attendess.push({ label: "Named", value: "Named" });
    attendess.push({ label: "Guest", value: "Guest" });
    return attendess;
  }

  reportValidation(){
     this.allValid = [...this.template.querySelectorAll('.ticketOrderField')]
    .reduce((validSoFar, inputCmp) => {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
    }, true);
  }

  handleAttehndeeType(event) {
    this.attendeeType = event.detail.value;
    this.isGuest = this.attendeeType === "Guest" ? true : false;
  }
}