/**
 * @File Name          : ct_DirectDebitForm.js
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 7/2/2020, 12:03:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/30/2020   Creation Admin     Initial Version
**/
import { LightningElement,track, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';

import UOP_LOGO from '@salesforce/resourceUrl/ct_UOP_Logo';
import getMetaDataWrapperAction from '@salesforce/apex/ct_DirectDebitFormController.fetchMetadataRecords';
import createAuthorisation from '@salesforce/apex/ct_DirectDebitFormController.createAsepratoAuthorization';
import createRecurringDonationRecord from '@salesforce/apex/ct_DirectDebitFormController.createRecurringDonation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
const FIELDS = [
  'Account.RecordTypeId',
  'Account.RecordType.DeveloperName',
  'Account.Id',
  'Account.Name',
];

export default class Ct_DirectDebitForm extends NavigationMixin(LightningElement) {
  uopLogoUrl = UOP_LOGO;
  @track showErrorcustomToast = false;
  @track showSuccessCustomToast = false;
  @track showSpinnerBoolean = true;
  @track directDebitObject = {};
  @api recordId;
  @track directDebitMetaDataObject;
  @track pickListValuesObject;
  @track paymentIframeURL;
  @track authorizationRecord = null;
  @track showAsperatoPayment = false;
  @track businessOrganisationType;

  @track isDirectDebitSuccessfully = false;


  @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
  wiredAccount({ error, data }) {
    if (data) {
      if(data.fields.RecordType.value.fields.DeveloperName.value != this.businessOrganisationType){
        this.customToastNotification('Error', 'Please select only Business Organisation Account Type', true);

      }
    } else if (error) {
      this.handleErrors(error);
    }
  }
  
  get openEndedStatus(){
    return this.directDebitObject.npe03__Open_Ended_Status__c == 'None'? true: false;    
  }

  onInputFieldOnChange(event) {
    if(event.target.name == 'Private__c'
    || event.target.name == 'Gift_Aid__c'){
      this.directDebitObject[event.target.name] = event.target.checked;
    }
    else{
      this.directDebitObject[event.target.name] = event.target.value;
    }
  }
  
  closeModel() {
    this.showAsperatoPayment = false;
  }
  handleContactChange(event){
    if(event.detail.value[0]){
      this.directDebitObject[event.target.name] = event.detail.value[0];
      this.recordId = event.target.name == 'npe03__Organization__c'?event.detail.value[0]: null;
    }
    else{
    this.directDebitObject[event.target.name] = null;
    }
  }
  connectedCallback() {
    getMetaDataWrapperAction()
      .then(result => {
          let metaDataWrapper = result;
          let directDebitRecords = metaDataWrapper.directDebitMetadataRecords;
          let thisDirectDebitObject = {};
          let metadataObject = {};
          for (let i = 0; i < directDebitRecords.length; i++) {
            thisDirectDebitObject[directDebitRecords[i].SObject_Field__c] = null;
              metadataObject[directDebitRecords[i].SObject_Field__c] = directDebitRecords[i];
          }
          thisDirectDebitObject['CollectionDate']= null;
          this.directDebitMetaDataObject = metadataObject;
          this.pickListValuesObject = metaDataWrapper.picklistFields;
          this.directDebitObject = thisDirectDebitObject;
          this.businessOrganisationType = metaDataWrapper.businessOrganisationRecordType;
          for (var i = 0; i < result.advancmentSettingRecords.length; i++) {
            this.paymentIframeURL = result.advancmentSettingRecords[i].Asperato_Payment_URL__c;
          }
          this.isDirectDebitSuccessfully = true;
          this.hideSpinner();
      })
      .catch(error => {
      this.customToastNotification('Error', error, true);
      });
      const that = this;
      //Aspereto
      window.addEventListener('message', function(e) {
          if (e.data == 'asp--complete') {
              that.showSpinner();
              //window.removeEventListener('message', that, false);
              that.processDirectDebitRecords();
          }
      }, false);     
  }

  validateForm(event) {
    this.showSpinner();
    window.scrollTo(0, 0);
    if(!this.directDebitObject.npe03__Installments__c){
      this.directDebitObject['npe03__Installments__c'] = 0;
    }
    const allValid = [...this.template.querySelectorAll('.ddformField')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

    if (!allValid) {
      this.customToastNotification('Error', 'Please fill required and update all invalid form entries', true);
    } else if ((this.directDebitObject.npe03__Contact__c  && this.directDebitObject.npe03__Organization__c)
    || (!this.directDebitObject.npe03__Contact__c  && !this.directDebitObject.npe03__Organization__c)) {
      this.customToastNotification('Error', 'Please select either Contact or Account for Direct Debit', true);
    } else if (event.target.name == 'Process Authorisation Now') {
      this.saveDetails();
    }
    else if(event.target.name == 'Process Authorisation Later'){
      this.processDirectDebitRecords();
    }
  }
  processDirectDebitRecords(){
    this.showErrorcustomToast = false;
    this.showAsperatoPayment = false;
    this.showSpinner();
    window.scrollTo(0, 0);

    createRecurringDonationRecord({directDebitDetails: JSON.stringify(this.directDebitObject),thisAuthorisation:this.authorizationRecord}).then(
      result => {
        this.hideSpinner();
        this.customToastNotification('Success', 'Direct Debit Record Inserted Successfully', false);
        window.open(result, "_self");
        // //Show Asperato Modal 
        // this[NavigationMixin.Navigate]({
        //   type: 'standard__recordPage',
        //   attributes: {
        //     "recordId": result,
        //     "actionName": "view"
        //   }
        // });
      }
      ).catch(error => {
      this.hideSpinner();
      this.handleErrors(error);
    });
  }
  saveDetails(){
    this.showErrorcustomToast = false;
    this.showSpinner();
    window.scrollTo(0, 0);
    createAuthorisation({ directDebitDetails: JSON.stringify(this.directDebitObject), thisAuthorisation:this.authorizationRecord}).then(
      result => {
          this.hideSpinner();
          //Show Asperato Modal
          this.authorizationRecord = result;
          this.paymentIframeURL = this.paymentIframeURL + '&aid=' + result.Id;
          this.showAsperatoPayment = true;

      }
  ).catch(error => {
      this.hideSpinner();

      this.handleErrors(error);
  });
  }
  oncancel() {
    this.showSpinner();
    window.scrollTo(0, 0);
    window.history.back();
  }
  showSpinner() {
    this.showSpinnerBoolean = true;
  }

  hideSpinner() {
    this.showSpinnerBoolean = false;
  }
  handleErrors(errors) {
    
    this.customToastNotification('Error', errors.body.message, true);
  }
  customToastNotification(toastTitle, toastMessage, isErrorMessage) {
    this.hideSpinner();
    var messageString = isErrorMessage ? 'Error' : 'Success';
    const showToastEvent = new ShowToastEvent({title: toastTitle, message : toastMessage, variant: messageString});
    this.dispatchEvent(showToastEvent);
  }
}