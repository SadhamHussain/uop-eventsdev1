import { LightningElement ,track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import sessionsRelatedToEvent from '@salesforce/apex/ct_SessionRegistrationController.getPageInitData';
import createSessionReg from '@salesforce/apex/ct_SessionRegistrationController.createSessionReg';

export default class Ct_SessionRegistrationForm extends LightningElement {
@track SessionData = [];
@track SessionDataSelected = [];
@track SessionIds = [];
//@track SessionRecs = [];
@track eventId;
@track selectedSession;
@track isSessions = true;
@track isChecked;
@track chkbx;
@api recordId; 
@track page = 1;
    perpage = 5;
@track pages = [];
    set_size = 5;

    
    renderedCallback(){
        this.renderButtons();   
      }
      renderButtons = ()=>{
          this.template.querySelectorAll('button').forEach((but)=>{
              but.style.backgroundColor = this.page===parseInt(but.dataset.id,10)?'#2574a9':'white';
              but.style.color = this.page===parseInt(but.dataset.id,10)?'white':'#2574a9';
           });
      }
      get pagesList(){
          let mid = Math.floor(this.set_size/2) + 1 ;
          if(this.page > mid){
              return this.pages.slice(this.page-mid, this.page+mid-1);
          } 
          return this.pages.slice(0,this.set_size);
       }

connectedCallback(){
    
    console.log('connectedCallback***');
    var queryString = window.location.href;
    var urlVar = new URL(queryString);
    var urlRecId = urlVar.searchParams.get("recid");
    this.recordId = urlRecId;
    sessionsRelatedToEvent({recordId: this.recordId})
    .then(result => {
        this.SessionData = result.lstSessions;
        this.eventId = result.eventId;
        this.setPages(this.SessionData);
    })
    .catch(error => {
        this.error = error;
        console.log('error: ' + error);
    })
}
hanldeProgressValueChange(event) {
    this.SessionData = event.detail;
    this.pages = [];
    this.setPages(this.SessionData);
    pagesList()

  }
onChecked(event){
    debugger;
    this.chkbx = event.target.checked;
    var checBoxID = event.target.dataset.id;
    for(var i=0; i< this.SessionData.length; i++){
        if(checBoxID == this.SessionData[i].session.Id){
            this.SessionData[i].selected =this.chkbx;
           // this.SessionDataSelected.push(this.SessionData[i]);
           this.SessionDataSelected[checBoxID]= this.SessionData[i];
        }
    }

}
pageData = ()=>{
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = (page*perpage) - perpage;
    let endIndex = (page*perpage);
    console.log('SessionData before>> '+JSON.stringify(this.SessionData));
    for(var i=0; i< this.SessionData.length; i++){
        // for(var j=0; j< this.SessionDataSelected.length; j++){
        //     if(this.SessionData[i].session.Id == this.SessionDataSelected[j].session.Id){
        //         this.SessionData[i] = this.SessionDataSelected[j];
        //     }
        // }
        if(this.SessionDataSelected[this.SessionData[i].session.Id] !== undefined){
            this.SessionData[i] = this.SessionDataSelected[this.SessionData[i].session.Id];
        }
         
    }
    console.log('SessionData after>> '+JSON.stringify(this.SessionData));
    console.log('newCollection>>'+JSON.stringify(this.SessionDataSelected));
    return this.SessionData.slice(startIndex,endIndex);
 }

setPages = (SessionData)=>{
    let numberOfPages = Math.ceil(SessionData.length / this.perpage);
    for (let index = 1; index <= numberOfPages; index++) {
        this.pages.push(index);
    }
 }  

get hasPrev(){
    return this.page > 1;
}

get hasNext(){
    return this.page < this.pages.length
}

onNext = ()=>{
    ++this.page;
}

onPrev = ()=>{
    --this.page;
}

onPageClick = (e)=>{
    this.page = parseInt(e.target.dataset.id,10);
    
}

get currentPageData(){
    return this.pageData();
}
getSelectedSessions(event){
    console.log('lstSessions>>>'+JSON.stringify(this.SessionData));
    var SessionRecs = [];
    //createSessionReg(this.SessionData);
    for(var idx in this.SessionDataSelected) {
        if(this.SessionDataSelected[idx].selected) {
            SessionRecs.push(this.SessionDataSelected[idx]);
        }
    }
    console.log('SessionRecs: ' + JSON.stringify(SessionRecs));
    
    createSessionReg({jsonData: JSON.stringify(SessionRecs), evtRegId: this.recordId})
    .then(result => {
        console.log('result'+JSON.stringify(result))
        var resultMsg = result;
        this.SessionDataSelected = [];
        if(resultMsg == 'Success'){
            const evt = new ShowToastEvent({
                title: 'Toast Success',
                message: 'Your Sessions are registered Successfully.',
                variant: 'success',
                mode: 'dismissable'
            });
           
            this.pages = [];
            this.connectedCallback();
            this.dispatchEvent(evt);
        }else{
            const evt = new ShowToastEvent({
                title: 'Toast Error',
                message: resultMsg,
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
        }
        
    })
    .catch(error => {
        this.error = error;
        console.log('error: ' + JSON.stringify(error));
        console.log('error: ' + error.message);
        if(this.error.message !== undefined || this.error.message !== ''){
            const evt = new ShowToastEvent({
                title: 'Toast Error',
                message: this.error.message,
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
        }
        console.log('error: ' + JSON.stringify(error));
    })
    
}
}