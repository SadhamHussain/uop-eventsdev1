/**
 * @File Name          : ct_VolunteerRegistrationForm.js
 * @Description        : 
 * @Author             : Umashankar Creation
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 6/10/2020, 2:09:03 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/12/2020   Umashankar Creation     Initial Version
 **/
import {
    LightningElement,
    wire,
    api,
    track
} from 'lwc';
import UOP_LOGO from '@salesforce/resourceUrl/ct_UOP_Logo';
import UOP_KEEP_IN_TOUCH_POINT1 from '@salesforce/label/c.ct_Donation_Keep_in_Touch';
import UOP_KEEP_IN_TOUCH_POINT2 from '@salesforce/label/c.Donation_Keep_In_Touch_Point_2';
import UOP_TERMS_CONDITIONS_PRIVACY_STATEMENT from '@salesforce/label/c.Volunteer_Terms_Conditions_And_Privacy_Statement';
import formFactorPropertyName from '@salesforce/client/formFactor';
import getWebFormMetaData from '@salesforce/apex/ct_VolunteerRegistrationFormController.fetchMetadataRecords';
import processVolunteerDetails from '@salesforce/apex/ct_VolunteerRegistrationFormController.saveVolunteerDetails';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import customSR from '@salesforce/resourceUrl/jQueryLibrary';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class Ct_VolunteerRegistrationForm extends LightningElement {

    @track spinnerBoolean;
    @track hasError = false;
    @track registrationSuccess;
    @track uopLOGO = UOP_LOGO;
    @track keepInTouchPoint1 = UOP_KEEP_IN_TOUCH_POINT1;
    @track keepInTouchPoint2 = UOP_KEEP_IN_TOUCH_POINT2;
    @track termsAndConditionsAndPrivacyStatements = UOP_TERMS_CONDITIONS_PRIVACY_STATEMENT;
    @track volunteerObjectFields;
    @track volunteerObject;
    @track formFactorPropertyName;
    @track pickListValuesObject;
    @track providingConsent = false;
    @track hasOtherSkills = false;

    connectedCallback() {

        //Event Listener to To prompt user and get confirmation before unload ie
        // Back button press, refresh, reload events and closing tab
        window.addEventListener('beforeunload', (event) => {
            if (this.registrationSuccess != true) {
                event.preventDefault();
                this.hideSpinner();
                event.returnValue = '';
                return '';
            }
        });

        Promise.all([
          loadScript(this, customSR),
        ])
        .then(() => {
            console.log('jquery loaded.');
        })
        .catch(error => {
            console.log(error.body.message);
        });

        const queryString = window.location.href;
        /*Get First Name, Last Name, Email from url When It Is Being Redirected From Site Page*/
        var urlVar = new URL(queryString);
        var firstName = urlVar.searchParams.get("volunteerFirstName");
        var lastName = urlVar.searchParams.get("volunteerLastName");
        var email = urlVar.searchParams.get("volunteerEmail");

        this.showSpinner();
        getWebFormMetaData().then(result => {

            if (result) {
                let thisvolunteerObject = {};
                let thisvolunteerObjectFields = {};
                for (var i = 0; i < result.volunteerMetadataRecords.length; i++) {
                    thisvolunteerObject[result.volunteerMetadataRecords[i].SObject_Field__c] = null;
                    thisvolunteerObjectFields[result.volunteerMetadataRecords[i].SObject_Field__c] = result.volunteerMetadataRecords[i];
                }

                if (firstName && lastName && email) {
                    thisvolunteerObject.FirstName = firstName;
                    thisvolunteerObject.LastName = lastName;
                    thisvolunteerObject.Email = email;
                }

                this.pickListValuesObject = result.picklistFields;
                this.volunteerObject = thisvolunteerObject;
                this.volunteerObjectFields = thisvolunteerObjectFields;
                this.hideSpinner();
            }
        }).catch(error => {
            this.hideSpinner();
            this.customToastNotification('Error', error, true);
        });

    }

    get errTxtStyle() {
        return 'font-family: "\'Encode Sans Expanded\', sans-serif" !important;color: red;font-size: 15px !important;font-weight: normal;';
    }

    handleSelectedCourse(event) {
        var selectedCourse = event.detail;
        var volunteerRecord = this.volunteerObject;
        volunteerRecord.Web_Course__c = selectedCourse;
        this.volunteerObject = volunteerRecord;
    }

    get getMainDivWidth() {
        let widthStyle;
        if (formFactorPropertyName === 'Large') {
            widthStyle = 'width:100%';
        } else if (formFactorPropertyName === 'Medium') {
            widthStyle = 'width:75%';
        } else {
            widthStyle = 'width:100%';
        }
        return widthStyle;
    }

    get getUopLogoStyle() {
        let logoStyle;
        if (formFactorPropertyName === 'Large') {
            logoStyle = 'width: 25%; height:20%;';
        } else {
            logoStyle = 'width: 200px !important;';
        }
        return logoStyle;
    }

    get thankYouFontStyle() {
        let fontStyle;
        if (formFactorPropertyName === 'Large') {
            fontStyle = 'font-size: xx-large !important;color: #621360;font-weight: bold !important;font-family: "\'Encode Sans Expanded\', sans-serif";'
        } else if (formFactorPropertyName === 'Medium') {
            fontStyle = 'font-size:larger !important;color: #621360;font-weight: bold !important;font-family: "\'Encode Sans Expanded\', sans-serif";';
        } else {
            fontStyle = 'font-size:larger !important;color: #621360;font-weight: bold !important;font-family: "\'Encode Sans Expanded\', sans-serif";';
        }
        return fontStyle;
    }

    get getNameClass() {
        return formFactorPropertyName === 'Large' ? 'slds-grid slds-grid--pull-padded' : 'slds-grid slds-grid--pull-padded slds-grid_vertical';
    }


    validateInputFields() {

        const allValid = [...this.template.querySelectorAll('.volunteerFormField')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if(this.validateConsentValues()){
          this.hasError = true; 

          var allFields = [...this.template.querySelectorAll('.volunteerFormField')];

          allFields.forEach(fields => {
            if(fields.type == 'radio'){
              $(fields).prop('required',true);
            }
          });
          [...this.template.querySelectorAll('.activeButton')][0].click();
        }
        else if (!allValid) {
            this.hasError = true;
            //Force form to scroll to the field where the value doesn't exists
          
            

            const inputComponents = [...this.template.querySelectorAll('.volunteerFormField')].find(cmps => cmps.required && !cmps.value || !cmps.reportValidity());

            var ios = navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
            if(!ios){
              inputComponents.focus();
            }

        } else {
            this.hasError = false;
            this.showSpinner();
            this.saveVolunteerData();
        }
    }

    saveVolunteerData() {
        this.volunteerObject.Email = this.volunteerObject.hed__AlternateEmail__c;
        processVolunteerDetails({
                volunteerDetails: JSON.stringify(this.volunteerObject)
            })
            .then(result => {
              var topInput = this.template.querySelector(`[data-id="topElement"]`);
              topInput.scrollIntoView();

              this.registrationSuccess = true;
              this.hideSpinner();
            })
            .catch(error => {
                this.hideSpinner();
                this.customToastNotification('Error', error, true);
            });
    }

    showSpinner() {
        this.spinnerBoolean = true;
    }

    hideSpinner() {
        this.spinnerBoolean = false;
    }

    handleErrors(errors) {
        this.customToastNotification('Error', errors, true);
    }

    onInputFieldOnChange(event) {
        this.volunteerObject[event.target.name] = event.target.value;
    }

    onCheckBoxInputFieldOnChange(event) {
        this.volunteerObject[event.target.name] = event.target.checked;
        if (event.target.title &&
            (event.target.title == 'Other_Volunteer_Organisations__c' || event.target.title == 'Additional_Relationship_Details__c') &&
            event.target.checked) {
            this.volunteerObject[event.target.title] = null;
        }
    }

    onVolunteerSkillChange(event) {
        if (this.volunteerObject.Volunteering_Skills__c == 'Other') {
            this.hasOtherSkills = true;
        }
    }

    handleConsentValue(event) {
        this.providingConsent = event.target.checked;
    }

    handleMultiPickListValues(event) {
        let multiPicklistValues = [];
        multiPicklistValues = event.target.value;
        this.volunteerObject[event.target.name] = multiPicklistValues.join(';');

        // if (this.volunteerObject.Volunteering_Skills__c.includes('Other')) {
        //     this.hasOtherSkills = true;
        // } else {
        //     this.hasOtherSkills = false;
        //     this.volunteerObject.Other_Skills__c = null;
        // }
    }

    handleCountrySelect(event) {
        var selectedCountry = event.detail;
        var contactRecord = this.volunteerObject;
        contactRecord.MailingCountry = selectedCountry;
        this.volunteerObject = contactRecord;
    }

    customToastNotification(toastTitle, toastMessage, isErrorMessage) {
        var messageString = isErrorMessage ? 'Error' : 'Success';
        const showToastEvent = new ShowToastEvent({
            Title: toastTitle,
            message: toastMessage,
            variant: messageString
        });
        this.dispatchEvent(showToastEvent);
    }

    handleMarketingPerferences(event) {

        [...this.template.querySelectorAll('.keepInTouchAllYes')].forEach(function(btns) {
            btns.checked = false;
        })

        if (event.target.name == 'smsConsent') {
            event.target.value == 'Yes' ? this.volunteerObject.Alumni_SMS_Consent__c = 'Yes' : this.volunteerObject.Alumni_SMS_Consent__c = 'No';
        } else if (event.target.name == 'emailConsent') {
            event.target.value == 'Yes' ? this.volunteerObject.Alumni_Email_Consent__c = 'Yes' : this.volunteerObject.Alumni_Email_Consent__c = 'No';
        } else if (event.target.name == 'phoneConsent') {
            event.target.value == 'Yes' ? this.volunteerObject.Alumni_Phone_Consent__c = 'Yes' : this.volunteerObject.Alumni_Phone_Consent__c = 'No';
        } else if (event.target.name == 'directMailConsent') {
            event.target.value == 'Yes' ? this.volunteerObject.Alumni_Directmail_Consent__c = 'Yes' : this.volunteerObject.Alumni_Directmail_Consent__c = 'No';
        }
    }

    validateConsentValues() {
        debugger;
        if (!this.volunteerObject.Alumni_SMS_Consent__c) {
            return true;
        }
        if (!this.volunteerObject.Alumni_Email_Consent__c) {
            return true;
        }
        if (!this.volunteerObject.Alumni_Phone_Consent__c) {
            return true;
        }
        if (!this.volunteerObject.Alumni_Directmail_Consent__c) {
            return true;
        }
        return false;
    }

    handleSelectAll(event) {
        if (event.target.value == 'Yes') {
            [...this.template.querySelectorAll('.keepInTouchNo')]
            .forEach(function(btns) {
                btns.checked = false;
            });

            [...this.template.querySelectorAll('.keepInTouchYes')]
            .forEach(function(btns) {
                btns.checked = true;
            });

            this.volunteerObject.Alumni_SMS_Consent__c = "Yes";
            this.volunteerObject.Alumni_Email_Consent__c = "Yes";
            this.volunteerObject.Alumni_Phone_Consent__c = "Yes";
            this.volunteerObject.Alumni_Directmail_Consent__c = "Yes";
        } else if (event.target.value == 'No') {
            [...this.template.querySelectorAll('.keepInTouchYes')]
            .forEach(function(btns) {
                btns.checked = false;
            });

            [...this.template.querySelectorAll('.keepInTouchNo')]
            .forEach(function(btns) {
                btns.checked = true;
            });

            this.volunteerObject.Alumni_SMS_Consent__c = "No";
            this.volunteerObject.Alumni_Email_Consent__c = "No";
            this.volunteerObject.Alumni_Phone_Consent__c = "No";
            this.volunteerObject.Alumni_Directmail_Consent__c = "No";
        }
    }

    onCancel() {
        this.showSpinner();

        var topInput = this.template.querySelector(`[data-id="topElement"]`);
        topInput.scrollIntoView();

        window.history.back();
    }
}