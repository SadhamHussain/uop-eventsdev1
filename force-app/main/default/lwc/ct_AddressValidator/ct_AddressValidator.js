/**
 * @File Name          : ct_AddressValidator.js
 * @Description        : 
 * @Author             : Umashankar Creation
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 07-05-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/12/2020   Creation Admin     Initial Version
**/
import { LightningElement, track } from 'lwc';
import makeAddressCallout from '@salesforce/apex/ct_AddressVerificationController.addressVerificationCallout';
import formFactorPropertyName from '@salesforce/client/formFactor';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


export default class Ct_AddressValidator extends LightningElement {
  @track isAddress          = false;
  @track showAddress        = true;
  @track addressList        = [];
  @track selectedAddress    = {};
  @track searchString       = '';
  @track showSpinner        = false;
  @track showError          = false;

  get getStyleClass() {
    return formFactorPropertyName === 'Large' ? 'slds-grid slds-form-element__control' : 'slds-grid slds-grid_vertical slds-form-element__control';
  }

  get getContainerClass() {
    return formFactorPropertyName === 'Large' ? 'slds-grid slds-size--5-of-7 slds-p-right_medium slds-combobox_container' : 'slds-grid slds-size--7-of-7 slds-p-bottom_medium slds-combobox_container';
  }

  get errTxtStyle() {
    return 'font-family: "\'Encode Sans Expanded\', sans-serif" !important;color: red;font-size: 14px !important;font-weight: normal;'; 
  }

  onInputFieldChange(event){
    this.addressList      = [];
    this.isAddress        = false;
    this.searchString     = event.target.value;
  }

//Make Address Callout
verifyAddress(){
  
  var searchText    = this.searchString;
  this.addressList  = null;
  this.searchString = searchText;
  this.isAddress    = false;
  this.showError    = false;

  if(!this.searchString){
    this.showError = true;
    //this.customToastNotification('Error', 'Please enter the address to search.', true);
  }
  else{
    this.showSpinner  = true;
    makeAddressCallout({ addressString: searchText }).then( result => {
    this.addressList  = result;
    if(this.addressList.length > 0){
      this.isAddress  = true;
    }
    else{
      var emptyAddressList    = [];
      emptyAddressList[0]     = {'label': 'No Results Found', 'value': ''};
      this.addressList        = emptyAddressList;
      this.isAddress  = true;
      setTimeout(() => {
        this.isAddress = false;
  }, 2000);
      
    }
    this.showSpinner  = false;
    }).catch(error => {
      var emptyAddressList    = [];
      emptyAddressList[0]     = {'label': 'No Results Found', 'value': ''};
      this.addressList        = emptyAddressList;
      this.showSpinner        = false;
    }); 
  } 
}

handleSelectedAddress(event){
  
  this.isAddress = false;
  var selectedSobjectIndex;
  if(this.addressList.length > 0){
    var target                  = event.target;
    this.searchString           = this.getIndexFromParent(target, "title");
    if(this.searchString != 'No Results Found'){
      selectedSobjectIndex    = this.getIndexFromParent(target, "data-selectedIndex");
    }
    else{
      this.searchString = null;
    }
    this.addressList            = [];
    if(selectedSobjectIndex){
      const addressEvent = new CustomEvent('addressresults', { detail: selectedSobjectIndex });
      this.dispatchEvent(addressEvent);
    }
  }
}

  getIndexFromParent(target, attributeToFind){
    var thisIndex = target.getAttribute(attributeToFind);
    while (!thisIndex) {
      target    = target.parentNode;
      thisIndex = this.getIndexFromParent(target, attributeToFind);
    }
    return thisIndex
  }

  // customToastNotification(toastTitle, toastMessage, isErrorMessage) {
  //   var messageString       = isErrorMessage ? 'Error' : 'Success';
  //   const showToastEvent    = new ShowToastEvent({Title: toastTitle, message : toastMessage, variant: messageString});
  //   this.dispatchEvent(showToastEvent);
  // }
}