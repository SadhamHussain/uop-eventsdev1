import {
  LightningElement,
  track,
  api
} from 'lwc';
import setAccountAsPrivate from '@salesforce/apex/ct_SetAccountAsPrivateController.setAccountAsPrivate';
import getInitialAccountStatus from '@salesforce/apex/ct_SetAccountAsPrivateController.getInitialAccountPrivacyStatus';

import {
  ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class Ct_SetAccountAsPrivateFromContact extends LightningElement {
  @track showSpinnerBoolean = false;
  @api recordId;
  @track isSetToPrivate;
  @track isPrivate = false;

  connectedCallback() {
    getInitialAccountStatus({
      contactId: this.recordId
    }).then(
      result => {
        if (result) {
          this.isPrivate = result;
          this.hideSpinner();
          this.customToastNotification('Success', '', false);
        }
      }).catch(error => {
      console.log('ERR' + JSON.stringify(error));
      this.hideSpinner();
      this.customToastNotification('Error', error, true);
    });
  }

  updateRelatedAccount(event) {
    this.showSpinner();
    this.isPrivate = event.target.checked;
    this.isSetToPrivate = event.target.checked;
    setAccountAsPrivate({
      contactId: this.recordId,
      isPrivate: event.target.checked
    }).then(
      result => {
        if (result) {
          this.hideSpinner();
          this.customToastNotification('Success', '', false);
        }
      }).catch(error => {
      console.log('ERR' + JSON.stringify(error));
      this.hideSpinner();
      this.customToastNotification('Error', error, true);
    });
  }

  customToastNotification(toastTitle, message, isErrorMessage) {
    var messageString = isErrorMessage ? 'Error' : 'Success';
    var message = this.isPrivate ? 'The related Account & Address has been set to private' : 'The related Account & Address has been reverted from private';
    const showToastEvent = new ShowToastEvent({
      Title: toastTitle,
      message: message,
      variant: messageString
    });
    this.dispatchEvent(showToastEvent);
  }

  showSpinner() {
    this.showSpinnerBoolean = true;
  }

  hideSpinner() {
    this.showSpinnerBoolean = false;
  }
}