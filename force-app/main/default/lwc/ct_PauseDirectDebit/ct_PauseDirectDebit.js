import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getDirectDebitRecord from '@salesforce/apex/ct_PauseDirectDebitController.getDirectDebitRecord';
import { NavigationMixin } from 'lightning/navigation';
import invokePausingProcess from '@salesforce/apex/ct_PauseDirectDebitController.updateDirectDebit';


export default class Ct_PauseDirectDebit extends NavigationMixin(LightningElement) {

    @api directDebitRecId;
    @track showSpinnerBoolean = true;
    @track showForm = false;
    @track thisDirectDebit;
    @track isDirectDebitPaused;

    get modalHeading() {
        return this.isDirectDebitPaused ? 'Resume Direct Debit' : 'Pause Direct Debit';
    }

    get showPauseDirectDebit() {
        return (this.isDirectDebitPaused != null && this.isDirectDebitPaused != undefined) && !this.isDirectDebitPaused;
    }

    get showResumeDirectDebit() {
        return (this.isDirectDebitPaused != null && this.isDirectDebitPaused != undefined) && this.isDirectDebitPaused;
    }


    connectedCallback() {

        getDirectDebitRecord({
                directDebitId: this.directDebitRecId
            }).then(result => {
                if (result != undefined &&
                    result != null) {
                    this.thisDirectDebit = result;
                    this.isDirectDebitPaused = result.Paused__c;
                    this.showSpinnerBoolean = false;
                    this.showForm = true;
                }
            })
            .catch(error => {
                this.customToastNotification('Error', error.body.message, true);
            });
    }
    handleConfirm() {
        this.showForm = false;
        this.showSpinnerBoolean = true;
        this.thisDirectDebit.Paused__c = !this.isDirectDebitPaused;
        invokePausingProcess({
                directDebitObject: JSON.stringify(this.thisDirectDebit)
            }).then(result => {
                var pauseSuccessMessage = this.isDirectDebitPaused ? 'Direct Debit resumed successfully' : 'Direct Debit paused successfully';
                this.redirectToDDRecord();
                this.customToastNotification('Success', pauseSuccessMessage, false);
                setTimeout(location.reload(), 1500);
            })
            .catch(error => {
                this.customToastNotification('Error', error.body.message, true);
            });
    }
    customToastNotification(toastTitle, toastMessage, isErrorMessage) {
        this.showSpinnerBoolean = false;
        var messageString = isErrorMessage ? 'Error' : 'Success';
        const showToastEvent = new ShowToastEvent({
            Title: toastTitle,
            message: toastMessage,
            variant: messageString
        });
        this.dispatchEvent(showToastEvent);
    }
    redirectToDDRecord() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": this.directDebitRecId,
                "actionName": "view"
            }
        });
    }
}