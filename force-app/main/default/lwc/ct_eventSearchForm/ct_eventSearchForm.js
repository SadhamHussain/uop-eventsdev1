import { LightningElement, track, wire } from 'lwc';
import getPickListValues from '@salesforce/apex/CT_EventSearchController.getPicklistValues';
import { ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class Ct_eventSearchForm extends LightningElement {
    eventName;
    eventType;
    eventGroup;
    eventTypeValues;
    showSearch = true;

    connectedCallback(){
      getPickListValues({
        objectName: 'Event__c',
        fieldName: 'Event_Type__c'
      }).then(result => {
        this.eventTypeValues = result;
      }).catch(error => {
        console.log('ERR'+JSON.stringify(error));
        this.customToastNotification('Error', error, true);
      });
    }

    customToastNotification(toastTitle, toastMessage, isErrorMessage) {
      var messageString = isErrorMessage ? 'Error' : 'Success';
      const showToastEvent = new ShowToastEvent({
        Title: toastTitle,
        message: toastMessage,
        variant: messageString
      });
      this.dispatchEvent(showToastEvent);
    }

    handleEventName(event) {
      this.eventName = event.target.value;
    }

    handleEventType(event) {
      this.eventType = event.target.value;
    }

    handleGroupEvent(event) {
      this.eventGroup = event.detail;
    }

    handleSearch(event) {

      var searchObject = { eventName:this.eventName, eventType:this.eventType, eventGroup:this.eventGroup};

      this.dispatchEvent(new CustomEvent('search', { detail: searchObject }));

    //   if (this.eventName) {
    //     this.dispatchEvent(new CustomEvent('search', { detail: {eventName: this.eventName}}));
    //   }
    //   else if(this.eventType){
    //     this.dispatchEvent(new CustomEvent('search', { detail: {eventType: this.eventType}}));
    //   }
    //   else if(this.eventGroup){
    //     this.dispatchEvent(new CustomEvent('search', { detail: {eventGroup: this.eventGroup}}));
    //   }
    //  else {
    //     console.log('Please enter a value.');
    //   }
    }

    show_Search(){
      this.showSearch = true;
    }
    hide_Search(){
      this.showSearch = false;
    }

    clearSearchTerms(){
      this.eventName = '';
      this.eventType = '';
      this.eventGroup = '';
    }
}