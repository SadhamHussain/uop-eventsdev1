import { LightningElement ,track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import sessionsRelatedToEvent from '@salesforce/apex/ct_SessionRegistrationController.getPageInitData';
import createSessionReg from '@salesforce/apex/ct_SessionRegistrationController.createSessionReg';

export default class Ct_SessionRegistrationFormClone extends LightningElement {
@track SessionData = [];
@track SessionDataSelected = [];
@track SessionIds = [];
@track SessionRecs = [];
@track eventId;
@track selectedSession;
@track testData;
@track isSessions = true;
@track isChecked;
@track chkbx;
@track str;
@api recordId; 
@track page = 1;
    perpage = 5;
@track pages = [];
    set_size = 5;


    renderedCallback(){
        this.renderButtons();   
      }
      renderButtons = ()=>{
          this.template.querySelectorAll('button').forEach((but)=>{
              but.style.backgroundColor = this.page===parseInt(but.dataset.id,10)?'yellow':'white';
           });
      }
      get pagesList(){
          let mid = Math.floor(this.set_size/2) + 1 ;
          if(this.page > mid){
              return this.pages.slice(this.page-mid, this.page+mid-1);
          } 
          return this.pages.slice(0,this.set_size);
       }

connectedCallback(){
    //this.correntrecordId = this.recordId;
    var queryString = window.location.href;
    var urlVar = new URL(queryString);
    var urlRecId = urlVar.searchParams.get("recid");
    this.recordId = urlRecId;
    sessionsRelatedToEvent({recordId: this.recordId})
    .then(result => {
        this.SessionData = result.lstSessions;
        //this.SessionDataOriginal = result.lstSessions;
        this.eventId = result.eventId;
        console.log('lstSessions>>>'+JSON.stringify(result.lstSessions[0].session));
        console.log('>>>'+JSON.stringify(result));
        this.setPages(this.SessionData);
    })
    .catch(error => {
        this.error = error;
        console.log('error: ' + error);
    })
}
hanldeProgressValueChange(event) {
    this.SessionData = event.detail;
    console.log('detail>>>'+JSON.stringify(event.detail));
    console.log('event.detail>>'+event.detail);
    console.log('SessionData>>'+this.SessionData);
    this.pages = [];
    this.setPages(this.SessionData);
    pagesList()

  }
onChecked(event){
    this.chkbx = event.target.checked;
    var checBoxID = event.target.dataset.id;
    console.log('Checkbox:>>'+event.target.checked);
    console.log('CheckId:>>'+event.target.dataset.id);
    console.log('checBoxID>>'+checBoxID);
    for(var i=0; i< this.SessionData.length; i++){
        if(checBoxID == this.SessionData[i].session.Id){
            this.SessionData[i].selected =this.chkbx;
            this.SessionDataSelected.push(this.SessionData[i]);
        }
    }
}
pageData = ()=>{
    let page = this.page;
    let perpage = this.perpage;
    let startIndex = (page*perpage) - perpage;
    let endIndex = (page*perpage);
    return this.SessionData.slice(startIndex,endIndex);
 }

setPages = (SessionData)=>{
    let numberOfPages = Math.ceil(SessionData.length / this.perpage);
    for (let index = 1; index <= numberOfPages; index++) {
        this.pages.push(index);
    }
 }  

get hasPrev(){
    return this.page > 1;
}

get hasNext(){
    return this.page < this.pages.length
}

onNext = ()=>{
    ++this.page;
}

onPrev = ()=>{
    --this.page;
}

onPageClick = (e)=>{
    this.page = parseInt(e.target.dataset.id,10);
    
}

get currentPageData(){
    return this.pageData();
}
getSelectedSessions(event){
    console.log('lstSessions>>>'+JSON.stringify(this.SessionData));
    //createSessionReg(this.SessionData);
    for(var i=0; i< this.SessionData.length; i++){
        if(this.SessionData[i].selected == true){
            this.SessionRecs.push(this.SessionData[i].session);
        }
    }
    console.log('SessionRecs: ' + JSON.stringify(this.SessionRecs));
    console.log('SessionRecs1: ' + this.SessionRecs);
    let parameterObject = {lstSessions:this.SessionData, eventId:'' }
    createSessionReg({jsonData: JSON.stringify(this.SessionDataSelected), evtRegId: this.recordId})
    .then(result => {
        console.log('result'+JSON.stringify(result))
        var resultMsg = result;
        this.SessionDataSelected = [];
        if(resultMsg == 'Success'){
            const evt = new ShowToastEvent({
                title: 'Toast Success',
                message: 'Your Sessions are registered Successfully.',
                variant: 'success',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
        }else{
            const evt = new ShowToastEvent({
                title: 'Toast Error',
                message: resultMsg,
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
        }
        
    })
    .catch(error => {
        this.error = error;
        const evt = new ShowToastEvent({
            title: 'Toast Error',
            message: resultMsg,
            variant: 'error',
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
        console.log('error: ' + JSON.stringify(error));
    })
    
}

}