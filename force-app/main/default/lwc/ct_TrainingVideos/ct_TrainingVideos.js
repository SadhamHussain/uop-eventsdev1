import { LightningElement, wire } from 'lwc';
import getAllRecords from '@salesforce/apex/ct_YouTubeVideoMapping.getAllRecords';
import watchtrainingVideo from '@salesforce/label/c.ct_Watch_Training_Video';
const DELAY = 350;
export default class Ct_TrainingVideos extends LightningElement {

    label={
        watchtrainingVideo
      };
      @wire(getAllRecords,{'searchKey':''})
      result;

      handleClick(event) {
        window.open(event.target.name);
      }
      handleKeyChange(event) {
        // Debouncing this method: Do not actually invoke the Apex call as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            getAllRecords({ searchKey })
                .then((result) => {
                    this.result.data = result;
                    this.error = undefined;
                })
                .catch((error) => {
                    this.error = error;
                    this.result = undefined;
                });
        }, DELAY);
    }
}