/**
 * @File Name          : ct_AddressValidatorInternalUse.js
 * @Description        : 
 * @Author             : Umashankar Creation
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 07-03-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/19/2020   Umashankar Creation     Initial Version
**/

import { LightningElement, api, track, wire } from 'lwc';
import makeAddressCallout from '@salesforce/apex/ct_AddressVerificationController.addressVerificationCallout';
import saveAddressDetailAction from '@salesforce/apex/ct_AddressVerificationController.saveAddressDetails';
import { getPicklistValues,getPicklistValuesByRecordType} from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import ADDRESS_OBJECT from '@salesforce/schema/hed__Address__c';
import ADDRESS_TYPE from '@salesforce/schema/hed__Address__c.hed__Address_Type__c';



import formFactorPropertyName from '@salesforce/client/formFactor';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {
  fireEvent
} from 'c/pubsub';
import {
  CurrentPageReference
} from 'lightning/navigation';

export default class Ct_AddressValidatorInternalUse extends LightningElement {
  @api recordId;
  @track isAddress          = false;
  @track showAddress        = true;
  @track addressList        = [];
  @track searchString       = '';
  @track showSpinner        = false;
  @track addressObject      = {};
  @api isLightningRecordPage = false;
  @track showForm              = false;
  @api showSection;
  @api addressRelatedField;
  @wire(CurrentPageReference) pageRef;
  @track pickListvalues;

  
  @wire(getObjectInfo, { objectApiName: ADDRESS_OBJECT })
  objectInfo;

  @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: ADDRESS_TYPE})
  wiredPickListValue({ data, error }){
    if(data){
        this.pickListvalues = data.values;
    }
    if(error){
        console.log('Error while fetching Picklist values');
    }
}
  //alert('AddressTypePicklistValues ' + JSON.stringify(AddressTypePicklistValues));

  get getStyleClass() {
    return formFactorPropertyName === 'Large' ? 'slds-grid slds-form-element__control' : 'slds-grid slds-grid_vertical slds-form-element__control';
  }

  get getContainerClass() {
    if(this.recordId){
      return 'slds-grid slds-size--4-of-6 slds-p-right_medium slds-combobox_container'
    }
    else{
    return formFactorPropertyName === 'Large' ? 'slds-grid slds-size--5-of-7 slds-p-right_medium slds-combobox_container' : 'slds-grid slds-size--7-of-7 slds-p-bottom_medium slds-combobox_container';
    }
  }
  onCheckBoxInputFieldOnChange(event) {
    this.addressObject[event.target.name] = event.target.checked;
  }

  show_Form(event){
    this.showSpinner  = true;
    this.showSection  = false;
    this.showForm     = true;
    this.showSpinner  = false;

  }
  hide_Form(event){
    this.showSpinner  = true;
    this.showForm     = false;
    this.showSection  = true;
    this.showSpinner  = false;

  }
  onInputFieldOnChange(event) {
    this.addressObject[event.target.name] = event.target.value;

  }

  onInputFieldChange(event){
    this.addressList      = [];
    this.isAddress        = false;
    this.searchString     = event.target.value;

  }
  handleCountrySelect(event) {
    let selectedCountry = event.detail;
    let addressRecord  = this.addressObject;
    addressRecord.Country = selectedCountry;
    this.addressObject = addressRecord;
  }
  clearValues(){
    this.showSpinner  = true;
    this.searchString = null;
    let addressObjectRecord = this.addressObject;
    addressObjectRecord['Street']       = null;
    addressObjectRecord['City']         = null;
    addressObjectRecord['PostalCode']   = null;
    addressObjectRecord['Country']      = null;
    addressObjectRecord['State']        = null;
    addressObjectRecord['hed__Address_Type__c'] = null;
    addressObjectRecord['Address1']   = null;
    addressObjectRecord['Address2']   = null;

    

    this.addressObject = addressObjectRecord;

    //Fire event to update AFD country in Country picklist
    fireEvent(this.pageRef, 'countryupdated', addressObjectRecord.Country);

    this.showSpinner  = false;
  }
  validateInputFields() {
      this.showSpinner  = true;
      let addressRecord = this.addressObject;
      const allValid = [...this.template.querySelectorAll('.addressFormField')]
          .reduce((validSoFar, inputCmp) => {
              inputCmp.reportValidity();
              return validSoFar && inputCmp.checkValidity();
          }, true);
      if (!allValid) {
        this.showSpinner  = false;

          this.customToastNotification('Error', 'Please fill required and update all invalid form entries', true);

      } 
      else if(!addressRecord.Country){
        this.showSpinner  = false;

          this.customToastNotification('Error', 'Please select valid country', true);
      }
      else {
          this.updateAddressDetails(this.addressObject);
          // this.delayTimeout = setTimeout(() => {
          //     this.onErrorMessageClose();
          // }, TOAST_MESSAGE_TIMEOUT);
      }
  }
  updateAddressDetails(addressRecord){

    saveAddressDetailAction({ addressDetails: JSON.stringify(addressRecord), recordId: this.recordId, relatedAPIName : this.addressRelatedField})
            .then(result => {

                this.showSpinner = false;
                this.customToastNotification('Success', 'Update Address Details Successfully', false);
                window.location.reload();
            })
            .catch(error => {

              this.showSpinner = false;
              this.customToastNotification('Error', error.body.message, true);
            });
  }

  //Make Address Callout
  verifyAddress(){
  
  var searchText    = this.searchString;
  this.addressList  = null;
  this.searchString = searchText;
  this.isAddress    = false;
  
  if(!this.searchString){
    this.customToastNotification('Error', 'Please enter the address to search.', true);
  }
  else{
    this.showSpinner  = true;
    makeAddressCallout({ addressString: searchText }).then( result => {
    this.addressList  = result;
    if(this.addressList.length > 0){
      this.isAddress  = true;
    }
    else{
      var emptyAddressList    = [];
      emptyAddressList[0]     = {'label': 'No Results Found', 'value': ''};
      this.addressList        = emptyAddressList;
      this.isAddress  = true;
      setTimeout(() => {
        this.isAddress = false;
    }, 2000);

    }
    this.showSpinner  = false;
    }).catch(error => {
      var emptyAddressList    = [];
      emptyAddressList[0]     = {'label': 'No Results Found', 'value': ''};
      this.addressList        = emptyAddressList;
      this.showSpinner        = false;
    }); 
  } 
}
connectedCallback() {
 if(this.isLightningRecordPage && this.showSection ){
  this.showSpinner  = true;
  this.showSection  = false;
  this.showForm     = true;
  this.showSpinner  = false;
 }
 var addressObjectVar = {};
  addressObjectVar['Street']        = null;
  addressObjectVar['City']          = null;
  addressObjectVar['PostalCode']    = null;
  addressObjectVar['Country']       = null;
  addressObjectVar['State']         = null;
  addressObjectVar['hed__Address_Type__c']       = null;
  addressObjectVar['hed__Default_Address__c']    = true;
  addressObjectVar['Address1']   = null;
  addressObjectVar['Address2'] = null;
  this.addressObject = addressObjectVar;
}


handleSelectedAddress(event){
  this.isAddress = false;

  if(this.addressList.length > 0){

    let thisAddressObject = this.addressObject;
    var selectedSobjectIndex ;
    var target                  = event.target;
    this.searchString           = this.getIndexFromParent(target, "title");
    if(this.searchString != 'No Results Found'){
      selectedSobjectIndex    = this.getIndexFromParent(target, "data-selectedIndex");
    }
    else{
      this.searchString = null;
    }
    this.addressList            = [];
    if(selectedSobjectIndex){
      let parseAddressObject = JSON.parse(selectedSobjectIndex);
      let thisStreet = parseAddressObject.Address1;
      if(parseAddressObject.Address2.toLowerCase() != parseAddressObject.Town.toLowerCase()){
        thisStreet = thisStreet+'\n'+parseAddressObject.Address2;
        thisAddressObject['Address2']     = parseAddressObject.Address2;
      }
        thisAddressObject['Street']       = thisStreet;
        thisAddressObject['City']         = parseAddressObject.Town;
        thisAddressObject['PostalCode']   = parseAddressObject.Postcode;
        thisAddressObject['Country']      = parseAddressObject.Country;
        thisAddressObject['State']        = parseAddressObject.Region;
        thisAddressObject['Address1']     = parseAddressObject.Address1;
      this.addressObject = thisAddressObject;
      //Fire event to update AFD country in Country picklist
    fireEvent(this.pageRef, 'countryupdated', thisAddressObject.Country);
      //const addressEvent = new CustomEvent('addressresults', { detail: selectedSobjectIndex });
      //this.dispatchEvent(addressEvent);
    }
    if(!this.recordId && !this.addressObject){
    } 
  }
}

  getIndexFromParent(target, attributeToFind){

    var thisIndex = target.getAttribute(attributeToFind);
    while (!thisIndex) {
      target    = target.parentNode;
      thisIndex = this.getIndexFromParent(target, attributeToFind);
    }
    return thisIndex
  }

  customToastNotification(toastTitle, toastMessage, isErrorMessage) {
    var messageString       = isErrorMessage ? 'Error' : 'Success';
    const showToastEvent    = new ShowToastEvent({Title: toastTitle, message : toastMessage, variant: messageString});
    this.dispatchEvent(showToastEvent);
  }
}