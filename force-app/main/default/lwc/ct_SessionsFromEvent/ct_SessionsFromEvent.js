import { LightningElement,api,track } from 'lwc';
import sessionsOnSearchTitle from '@salesforce/apex/ct_SessionRegistrationController.sessionsOnTitleSearch';
import sessionsOnTrackSearch from '@salesforce/apex/ct_SessionRegistrationController.sessionsOnTrackSearch';
export default class Ct_SessionsFromEvent extends LightningElement {
    @api eventId;
    //@track isSession = false;
    @api searchkeyonTitle ;
    @api searchkeyonTrack ;
    @track sessionsSearchByTitle = [];
    @track sessionsSearchByTrack = [];


    onTrackChange(event){
        this.searchkeyonTrack = event.target.value;
        console.log('eventId:track >>'+this.eventId);
        console.log('searchkey:title >>'+this.searchkeyonTrack);
        sessionsOnTrackSearch({evtId: this.eventId, searchKey: this.searchkeyonTrack})
    .then(result => {
        this.sessionsSearchByTrack = result;
        console.log('sessionsSearchByTrack>>>'+JSON.stringify(result));
        const selectedEvent = new CustomEvent('progressvaluechange', {
            detail: this.sessionsSearchByTrack
          });

        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
      
    })
    .catch(error => {
        this.error = error;
        console.log('error: ' + error);
    });
    }
    
    

    onTitleChange(event){
        this.searchkeyonTitle = event.target.value;
        sessionsOnSearchTitle({evtId: this.eventId, searchKey: this.searchkeyonTitle})
    .then(result => {
        this.sessionsSearchByTitle = result;
        const selectedEvent = new CustomEvent('progressvaluechange', {
            detail: this.sessionsSearchByTitle
          });

        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
      
    })
    .catch(error => {
        this.error = error;
        console.log('error: ' + error);
    });
    
     
    }
}