/**
 * @File Name          : ct_WebDonationComponent.js
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 07-09-2020
 * @Modification Log   : 
 * Ver       Date            Author              Modification
 * 1.0    4/27/2020   Creation Admin     Initial Version
 **/
import {
    LightningElement,
    wire,
    api,
    track
} from 'lwc';
import GIFT_AID_LOGO from '@salesforce/resourceUrl/ct_Gift_Aid_Logo';
import UOP_LOGO from '@salesforce/resourceUrl/ct_UOP_Logo';
import PAYPAL_LOGO from '@salesforce/resourceUrl/ct_Paypal_Logo';
import UOP_KEEP_IN_TOUCH_POINT1 from '@salesforce/label/c.ct_Donation_Keep_in_Touch';
import UOP_KEEP_IN_TOUCH_POINT2 from '@salesforce/label/c.Donation_Keep_In_Touch_Point_2';
import UOP_TERMS_CONDITIONS_PRIVACY_STATEMENT from '@salesforce/label/c.Donation_Terms_Conditions_And_Privacy_Statement';
import GIFT_AID_FOR_MY_DONATIONS from '@salesforce/label/c.Gift_Aid_For_My_Donations';
import getWebFormMetaData from '@salesforce/apex/ct_UOPWebDonationFormController.fetchWebformMetadata';
import createPaymentInformation from '@salesforce/apex/ct_UOPWebDonationFormController.createNewAsperatoPayment';
import getPaypalAsperatoReference from '@salesforce/apex/ct_WebDataFormHelper.getAsperatoReference';
import createAuthorizationInformation from '@salesforce/apex/ct_UOPWebDonationFormController.createAuthorizationRecord';
import createDonationRecords from '@salesforce/apex/ct_UOPWebDonationFormController.saveDonationDetails';
//import getContact from '@salesforce/apex/ct_UOPWebDonationFormController.getIsEmailBounceValues';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import customSR from '@salesforce/resourceUrl/jQueryLibrary';
import formFactorPropertyName from '@salesforce/client/formFactor';

import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    fireEvent
} from 'c/pubsub';
import {
    CurrentPageReference
} from 'lightning/navigation';

export default class ct_WebDonationComponent extends LightningElement {
    //Attributes
    @track spinnerBoolean;
    @track showError = false;
    @track showDonationForm = false;
    @track providingConsent = false;
    @track donationDetails;
    @track projectDetails;
    @track paymentSuccess = false;
    @track donateToList;
    @track collectionPointList;
    @track donationReasonsList;
    @track donationTypeList;
    @track contactConsentList;
    @track errorMessage;
    @track donationObjectFields = {};
    @track backgroundImageURL;
    @track uopWebDonationFormControllerVar;
    @track paymentIframeURL;
    @track paymentURL;
    @track showAsperatoPayment;
    @track asperatoOpportunityId;
    @track retriveOppBoolean;
    @track contactTitleList;
    @track donationFrequenyOptions = "[{'label': 'Monthly', 'value': 'Monthly'},{'label': 'Single', 'value': 'Single'}]";
    @track giftAidLogo = GIFT_AID_LOGO;
    @track uopLOGO = UOP_LOGO;
    @track paypalLOGO = PAYPAL_LOGO;
    @track giftAidLabel = GIFT_AID_FOR_MY_DONATIONS;
    @track keepInTouchPoint1 = UOP_KEEP_IN_TOUCH_POINT1;
    @track keepInTouchPoint2 = UOP_KEEP_IN_TOUCH_POINT2;
    @track termsAndConditionsAndPrivacyStatements = UOP_TERMS_CONDITIONS_PRIVACY_STATEMENT;
    @track hasReasonAsMemorial = false;
    @track hasReasonAsHonour = false;
    @track showErrorcustomToast = false;
    @track showSuccessCustomToast = false;
    @track customToastMessage;
    @track customToastAssistiveText;
    @track donationObject = {};
    @track recurringPaymentMethod;
    @track singlePaymentMethod;
    @track collectionButtonStyle;
    @track asperatoForm;
    @track successMessageCard;
    @track paypalRedirect = false;
    @track paypalThankYouUrl;
    @track successMessageDD;
    @track showPaypalButton = false;
    @wire(CurrentPageReference) pageRef;

    get getMainDivWidth() {
        let widthStyle;
        if (formFactorPropertyName === 'Large') {
            widthStyle = 'width:100%';
        } else if (formFactorPropertyName === 'Medium') {
            widthStyle = 'width:75%';
        } else {
            widthStyle = 'width:100%';
        }
        return widthStyle;
    }

    get getButtonGroupWidth() {
        let widthStyle;
        if (formFactorPropertyName === 'Large') {
            widthStyle = 'width:50%;';
        } else if (formFactorPropertyName === 'Medium') {
            widthStyle = 'width:100%;';
        } else if (formFactorPropertyName === 'Small') {
            widthStyle = 'width:100%;';
        }
        return widthStyle;
    }

    get getErrorDialogStyle() {
        let widthStyle;
        if (formFactorPropertyName === 'Large') {
            widthStyle = 'width:100%;align: center';
        } else if (formFactorPropertyName === 'Medium') {
            widthStyle = 'width:55%;align: center';
        } else if (formFactorPropertyName === 'Small') {
            widthStyle = 'width:10%;align: center';
        }
        return widthStyle;
    }

    get getUopLogoStyle() {
        let logoStyle;
        if (formFactorPropertyName === 'Large') {
            logoStyle = 'width: 25%; height:20%;';
        } else {
            logoStyle = 'width: 200px !important;';
        }
        return logoStyle;
    }

    get thankYouFontStyle() {
        let fontStyle;
        if (formFactorPropertyName === 'Large') {
            fontStyle = 'font-size: 24px;color: #621360;font-weight: bold;font-family: "\'Encode Sans Expanded\', sans-serif" !important;'
        } else if (formFactorPropertyName === 'Medium') {
            fontStyle = 'font-size: 24px;color: #621360;font-weight: bold;font-family: "\'Encode Sans Expanded\', sans-serif" !important;';
        } else {
            fontStyle = 'font-size: 24px;color: #621360;font-weight: bold;font-family: "\'Encode Sans Expanded\', sans-serif" !important;';
        }

        if (this.paypalRedirect) {
            if (formFactorPropertyName === 'Large') {
                fontStyle = 'font-size: 5rem;color: #621360;font-weight: 700;font-family: "\'Encode Sans Expanded Extra Bold\', sans-serif" !important;'
            } else if (formFactorPropertyName === 'Medium') {
                fontStyle = 'font-size: 5rem;color: #621360;font-weight: 700;font-family: "\'Encode Sans Expanded Extra Bold\', sans-serif" !important;';
            } else {
                fontStyle = 'font-size: 5rem;color: #621360;font-weight: 700;font-family: "\'Encode Sans Expanded Extra Bold\', sans-serif" !important;';
            }
        }
        return fontStyle;
    }

    get iframeLoadAttr() {
        return "window.parent.scrollTo(0,0)";
    }

    get thankYouMessageFontStyle() {
        let fontStyle;
        if (formFactorPropertyName === 'Large') {
            fontStyle = 'font-size: large !important;color: #621360;font-family: "\'Encode Sans Expanded\', sans-serif" !important; text-align: center !important; width: 75%';
        } else if (formFactorPropertyName === 'Medium') {
            fontStyle = 'font-size:medium !important;color: #621360;font-family: "\'Encode Sans Expanded\', sans-serif" !important; text-align: center !important; width: 75%';
        } else {
            fontStyle = 'font-size:medium !important;color: #621360;font-family: "\'Encode Sans Expanded\', sans-serif" !important; text-align: center !important; width: 75%';
        }
        return fontStyle;
    }

    get errTxtStyle() {
        return 'font-family: "\'Encode Sans Expanded\', sans-serif" !important;color: red;font-size: 15px !important;font-weight: normal;';
    }

    get getNameClass() {
        return formFactorPropertyName === 'Large' ? 'slds-grid slds-grid--pull-padded' : 'slds-grid slds-grid--pull-padded slds-grid_vertical';
    }

    get getSinglepaymentFrequency() {
        return this.donationObject.paymentFrequency == "Single" ? "activeButton slds-button slds-button_success" :
            "inactiveButton slds-button slds-button_destructive";
    }

    get getMonthlypaymentFrequency() {
        return this.donationObject.paymentFrequency == "Monthly" ? "activeButton slds-button slds-button_success" :
            "inactiveButton slds-button slds-button_destructive";
    }

    get renderDonationButton() {
        return (this.donationObject.Amount != '' || this.donationObject.Amount != undefined) && this.donationObject.Amount >= 1 ?
            true : false;
    }

    get getDonationType() {
        return this.donationObject.paymentFrequency == 'Single' ? true : false;
    }

    get showForm() {
        return ((formFactorPropertyName === 'Large' && !this.paymentSuccess) || (!this.paymentSuccess && !this.showAsperatoPayment));
    }

    get isMobile() {
        return formFactorPropertyName === 'Small';
    }

    get iframeLoadStyle() {
        var styleString;

        if (navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
            styleString = 'border:none;  width: 100%; height: 800px;position:absolute; top:2000px; left: 0;';
        } else {
            styleString = 'border:none;  width: 100%; height: 1500px;';

        }
        return styleString;
    }

    get iframeStyle() {
        let styleString = 'background-color: white; position:absolute; top:5rem;  width: 100%; min-height: 500px; clear:both;';
        if (window.location !== window.parent.location) {
            styleString = 'background-color: white; position:absolute; top:5rem;  width: 100%; min-height: 810px; clear:both;';
        } else {
            styleString = 'background-color: white; width: 100%; min-height: 500px;  clear:both;';
        }
        return styleString;
    }

    get iframeHeaderStyle() {
        let styleString;
        if (window.location !== window.parent.location) {
            styleString = 'position:absolute; top:3rem;  width: 100%;';
        } else {
            styleString = 'width: 100%;';
        }
        return styleString;
    }

    connectedCallback() {

        //Event Listener to To prompt user and get confirmation before unload ie
        // Back button press, refresh, reload events and closing tab
        window.addEventListener('beforeunload', (event) => {
            if (this.donationObject.paymentType != 'Paypal' &&
                this.paymentSuccess != true) {
                event.preventDefault();
                this.hideSpinner();
                event.returnValue = '';
                return '';
            }
        });

        Promise.all([
                loadScript(this, customSR),
            ])
            .then(() => {
                console.log('jquery loaded.');
            })
            .catch(error => {
                console.log(error.body.message);
            });

        /*Get Fund and Donation Type value from URL and populate to appropriate fields    */
        var queryString = window.location.href;
        var urlVar = new URL(queryString);
        var fundValue = urlVar.searchParams.get("f");
        var donationType = urlVar.searchParams.get("t");
        var projectAmount = urlVar.searchParams.get("amount");
        var paypalAspertoRef;

        if (queryString.includes('redirect_from_payment')) {
            getPaypalAsperatoReference({
                paymentRecordId: urlVar.searchParams.get("asperatoId")
            }).then(result => {
                paypalAspertoRef = result;
            }).catch(error => {
                this.hideSpinner();
                this.customToastNotification('Error', error, true);
            });
        }
        this.showSpinner();
        /*Modified by Rajesh on 09/04/2020  */
        getWebFormMetaData({
            formName: 'Donation'
        }).then(result => {
            var thisDonationObject = {
                "donationMotivation": "General Payment",
                "donationToProject": null,
                "paymentType": "Card",
                "paymentFrequency": "Single",
                "donateToList": null,
                "projectName": null,
                "hasOpportunity": null,
                "opportunityId": null,
                "asperatoId": null,
                "authorizationId": '',
                "myOwnDonation": true,
                "fundraisingOrCollected": false,
                "wishToGiftAid": true,
                "notWishToGiftAid": false,
                "preferredChannelList": null,
                "preferredCommunication": null,
                "paymentCollection": 'Monthly',
                "paymentCollectionPoint": ''
            };
            /*Fetch Active General Accounting Unit Web Label Values for Where your Donation goes? field  */
            this.donateToList = result.picklistFields.donateTo;
            //this.collectionPointList = result.picklistFields.collectionPointInMonth;
            this.donationReasonsList = result.picklistFields.donationReason;
            //this.donationTypeList = result.picklistFields.donationType;
            this.contactConsentList = result.picklistFields.contactConsent;
            this.contactTitleList = result.picklistFields.Salutation;
            //this.preferredChannelList = data['preferredChannel'];
            var thisDonationObjectFields = {};

            if (result) {
                for (var i = 0; i < result.donationMetadataRecords.length; i++) {
                    thisDonationObject[result.donationMetadataRecords[i].SObject_Field__c] = '';
                    thisDonationObjectFields[result.donationMetadataRecords[i].SObject_Field__c] = result.donationMetadataRecords[i];
                }
                this.donationObject = thisDonationObject;

                for (var gau in this.donateToList) {
                    if (this.donateToList[gau].label == fundValue) {
                        this.donationObject.donationToProject = this.donateToList[gau].value;
                    }
                }
                this.donationObject.paymentFrequency = (donationType == 'Regular Gift' ? "Monthly" : "Single");
                //Based on donation type parameter, update the Payment Type
                this.donationObject.paymentType = (this.donationObject.paymentFrequency == 'Single' ? "Card" : "Direct Debit");
                for (var i = 0; i < result.advancmentSettingRecords.length; i++) {
                    this.singlePaymentMethod = result.advancmentSettingRecords[i].One_Off_Payment_Method__c != null ?
                        result.advancmentSettingRecords[i].One_Off_Payment_Method__c : 'Card';
                    this.recurringPaymentMethod = result.advancmentSettingRecords[i].Recurring_Payment_Method__c != null ?
                        result.advancmentSettingRecords[i].Recurring_Payment_Method__c : 'Direct Debit';
                    this.paymentURL = result.advancmentSettingRecords[i].Asperato_Payment_URL__c;

                }
                this.paymentIframeURL = this.paymentURL;
                //Initialize Default Donation Amount
                this.donationObject.paymentFrequency == 'Single' ? this.donationObject.Amount = 50 : this.donationObject.Amount = 10;
                this.donationObjectFields = thisDonationObjectFields;
                this.paypalThankYouUrl = result.paypalRedirectURL;
                this.successMessageCard = result.donationCardSuccessMessage;
                this.successMessageDD = result.donationDDSuccessMessage;
                this.showPaypalButton = result.showPaypalButton && result.showPaypalButton.toLowerCase() == 'true' ? true : false;

                if (this.donationObject.paymentFrequency == 'Single' && queryString.includes('redirect_from_payment')) {
                    var thisSucessMessage = this.successMessageCard;
                    var thankMessage = thisSucessMessage.replace("AMOUNT", projectAmount).replace("REFERENCE", paypalAspertoRef);
                    this.successMessageCard = thankMessage;
                    this.paymentSuccess = true;
                    this.paypalRedirect = true;
                } else {
                    this.showDonationForm = true;
                }

                this.hideSpinner();
            }
        }).catch(error => {
            this.hideSpinner();
            this.customToastNotification('Error', error, true);
        });
        const that = this;

        //Aspereto
        window.addEventListener('message', function(e) {
            if (e.data == 'asp--error') {
                that.closeModel();
            }

            if (e.data == 'asp--complete') {
                that.showSpinner();
                that.processDonationRecords();
            }
            if (e.data == 'asp--processing') {
                var topInput = that.template.querySelector(`[data-id="topElement"]`);
                topInput.scrollIntoView();
            }

            if (e.data == 'asp--pcdialog') {
                console.log('asp--pcdialog');
                var iframe = that.template.querySelector(`[data-id="aspframe"]`);
                iframe.style.height = '1100px';
            }

            if (e.data == 'asp--dddialog') {
                console.log('asp--dddialog');
                var topInput = that.template.querySelector(`[data-id="topElement"]`);
                topInput.scrollIntoView();
            }

        }, false);



        // const headerOutsideIframe = parentELE.clientHeight;

    }

    iframeMobileOnload() {
        let target = this.template.querySelector(`[data-id="topElement"]`);
        target.scrollIntoView();
    }

    iframeNonMobileOnload() {
        console.log('INN');
        var iframe = this.template.querySelector(`[data-id="aspframe"]`);

        iframe.style.height = iframe.contentWindow.document.body.scrollHeight + 'px';
    }

    showSpinner() {
        this.spinnerBoolean = true;
    }

    hideSpinner() {
        this.spinnerBoolean = false;
    }

    updatePaymentAmount(event) {

        this.donationObject.Amount = event.target.title;
        const that = this;
        [...this.template.querySelectorAll('.amtInactiveButton')]
        .forEach(function(btns) {
            if (btns.title == that.donationObject.Amount) {
                btns.classList.remove("amtInactiveButton");
                btns.classList.add("amtActiveButton");
            }
        });

        [...this.template.querySelectorAll('.amtActiveButton')]
        .forEach(function(btns) {
            if (btns.title != that.donationObject.Amount) {
                btns.classList.remove("amtActiveButton");
                btns.classList.add("amtInactiveButton");
            }
        });
    }

    updatePaymentFrequency(event) {
        this.donationObject.paymentFrequency = event.target.title;

        //Set Default Donation Amount
        this.donationObject.paymentFrequency == 'Single' ? this.donationObject.Amount = 50 : this.donationObject.Amount = 10;

        if (event.target.title == 'Single') {
            this.donationObject.paymentType = this.singlePaymentMethod;
        } else {
            this.donationObject.paymentType = this.recurringPaymentMethod;
        }
    }

    paypalCheckout() {
        this.donationObject.paymentType = 'Paypal';

        this.validateDOnationForm();
    }

    cardCheckout() {
        this.donationObject.paymentType = this.singlePaymentMethod;
        this.validateDOnationForm();
    }

    validateDOnationForm() {
        const allValid = [...this.template.querySelectorAll('.donationFormField')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if (this.validateConsentValues()) {
            this.showError = true;

            var allFields = [...this.template.querySelectorAll('.donationFormField')];

            allFields.forEach(fields => {
                if (fields.type == 'radio') {
                    $(fields).prop('required', true);
                }
            });

            if (this.donationObject.paymentType == 'Paypal') {
                [...this.template.querySelectorAll('img')][0].click();
            } else {
                [...this.template.querySelectorAll('.completeDonationActiveButton')][0].click();
            }

        } else if (!allValid) {
            //Force form to scroll to the field where the value doesn't exists

            var inputComponents = [...this.template.querySelectorAll('.donationFormField')].find(cmps => cmps.required && !cmps.value || !cmps.reportValidity());
            var labelId = inputComponents.id;
            var inputLabel = this.template.querySelector(`[for=${labelId}]`);
            this.showError = true;
            //inputLabel.scrollIntoView();
            var ios = navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
            if (!ios) {
                inputComponents.focus();
            }

            //var topInput = this.template.querySelector(`[data-id="topElement"]`);


            /*inputLabel.scrollIntoView();
          
            if(navigator.userAgent.indexOf("Safari") > -1 &&
            (navigator.userAgent.match(/iPad/i) ||
            navigator.userAgent.match(/iPhone/i))){
              window.scrollTo(0, inputLabel.getBoundingClientRect().top);
            }*/
            // $(inputComponents).on('touchstart', function() {
            //     this.showError = true;
            //     // $(inputComponents).focus();
            //     $(inputComponents.scrollIntoView();
            // });

            // $(inputComponents).trigger('touchstart');

            // var topInput = this.template.querySelector(`[data-id="topElement"]`);
            // topInput.scrollIntoView();

            // if (inputComponents) {
            //     this.customToastNotification('Error', 'Please fill required and update all invalid form entries', true);
            // }

            // for (var i = 0; i < inputComponents.length; i++) {
            //     if (inputComponents[i].required && !inputComponents[i].value) {
            //       console.log('ENT 1');
            //       $(inputComponents[i]).trigger('touchstart');
            //       console.log('ENT 2');
            //       $(inputComponents[i]).on('touchstart', function () {
            //         $(this).focus();
            //         focused = $(this);
            //     });
            //     console.log('ENT 3');
            //         //let target = this.template.querySelector(`[data-id="topElement"]`);
            //         //this.scrollCustomFunction(target);
            //         //target.scrollIntoView();
            //         this.showError = true;
            //         //this.customToastNotification('Error', 'Please fill required and update all invalid form entries', true);
            //         break;
            //     }

        } else if (!this.donationObject.MailingCountry) {


            //Force form to scroll to the field where the value doesn't exists
            var inputComponents = this.template.querySelector(`[data-id="countryElement"]`);
            this.showError = true;
            var ios = navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
            if (!ios) {
                inputComponents.focus();
            }
            // $(inputComponents).on('touchstart', function() {
            //     this.showError = true;
            //     //$(this).focus();
            //     $(this).scrollIntoView();
            // });
            //this.customToastNotification('Error', 'Please choose your country', true);
            // $(inputComponents).trigger('touchstart');
            this.template.querySelector('c-ct_-country-picklist').isCountrySelected();

            // let inputComponents = [...this.template.querySelectorAll('.donationFormField')];
            // for (var i = 0; i < inputComponents.length; i++) {
            //     let target = this.template.querySelector(`[data-id="topElement"]`);
            //     target.scrollIntoView();
            //     this.showError = true;
            //     this.customToastNotification('Error', 'Please choose you country', true);
            //     break;
            // }
        } else {
            this.showError = false;
            this.createPayment();
            // getContact({
            //     firstNameVal: this.donationObject.FirstName,
            //     lastNameVal: this.donationObject.LastName,
            //     emailVal: this.donationObject.Email
            // }).then(result => {
            //     if (result == 'Yes') {
            //         this.customToastNotification('Error', 'The email address for this contact is not valid and it bounced. Can you fix it before you donate?', true);
            //     } else {

            //     }
            //     this.hideSpinner();
            // }).catch(error => {
            //     this.hideSpinner();
            //     this.customToastNotification('Error', error, true);
            // });
        }
    }

    customToastNotification(toastTitle, toastMessage, isErrorMessage) {
        var messageString = isErrorMessage ? 'Error' : 'Success';
        const showToastEvent = new ShowToastEvent({
            Title: toastTitle,
            message: toastMessage,
            variant: messageString
        });
        this.dispatchEvent(showToastEvent);
    }

    onErrorMessageClose() {
        window.clearTimeout(this.delayTimeout);
        this.showErrorcustomToast = false;
        this.showSuccessCustomToast = false;
    }

    handleSelectedAddress(event) {
        var seletedAddress = event.detail;
        var addressObject = JSON.parse(seletedAddress);
        var contactRecord = this.donationObject;
        let thisStreet = addressObject.Address1;
        if (addressObject.Address2.toLowerCase() != addressObject.Town.toLowerCase()) {
            thisStreet = thisStreet + '\n' + addressObject.Address2;
        }
        contactRecord.MailingStreet = thisStreet;
        contactRecord.MailingCity = addressObject.Town;
        contactRecord.MailingPostalCode = addressObject.Postcode;
        contactRecord.MailingCountry = addressObject.Country;
        contactRecord.MailingState = addressObject.Region;

        fireEvent(this.pageRef, 'countryupdated', addressObject.Country);
    }

    handleCountrySelect(event) {
        var selectedCountry = event.detail;
        var contactRecord = this.donationObject;
        contactRecord.MailingCountry = selectedCountry;
        this.donationObject = contactRecord;
    }

    showAsperatoPaymentController() {
        this.showAsperatoPayment = true;
    }

    closeModel() {
        this.showAsperatoPayment = false;
    }

    handleGifAidDonation(event) {
        this.donationObject.myOwnDonation = event.target.checked;

        if (!this.donationObject.myOwnDonation) {
            this.donationObject.wishToGiftAid = false;
        }
    }

    handleGifAidInterest(event) {
        this.donationObject.wishToGiftAid = event.target.checked;
    }

    onInputFieldOnChange(event) {
        this.donationObject[event.target.name] = event.target.value;
    }

    handleProjectSelection(event) {
        this.donationObject[event.target.name] = event.target.value;
        this.donationObject.projectName = event.target.options.find(opt => opt.value === event.detail.value).label;
    }

    handleprovidingConsent(event) {

        this.providingConsent = event.target.checked;
    }

    /* All Helper Functions Goes Below */

    handleMarketingPerferences(event) {

        [...this.template.querySelectorAll('.keepInTouchAllYes')].forEach(function(btns) {
            btns.checked = false;
        })

        if (event.target.name == 'smsConsent') {
            event.target.value == 'Yes' ? this.donationObject.Alumni_SMS_Consent__c = 'Yes' : this.donationObject.Alumni_SMS_Consent__c = 'No';
        } else if (event.target.name == 'emailConsent') {
            event.target.value == 'Yes' ? this.donationObject.Alumni_Email_Consent__c = 'Yes' : this.donationObject.Alumni_Email_Consent__c = 'No';
        } else if (event.target.name == 'phoneConsent') {
            event.target.value == 'Yes' ? this.donationObject.Alumni_Phone_Consent__c = 'Yes' : this.donationObject.Alumni_Phone_Consent__c = 'No';
        } else if (event.target.name == 'directMailConsent') {
            event.target.value == 'Yes' ? this.donationObject.Alumni_Directmail_Consent__c = 'Yes' : this.donationObject.Alumni_Directmail_Consent__c = 'No';
        }
    }

    createPayment() {
        this.showErrorcustomToast = false;
        this.showSuccessCustomToast = false;
        var addressObj = this.donationObject;
        this.paymentIframeURL = this.paymentURL;
        if (addressObj.MailingStreet.includes('\n')) {
            addressObj.hed__MailingStreet__c = addressObj.MailingStreet.split('\n')[0];
            addressObj.hed__MailingStreet2__c = addressObj.MailingStreet.split('\n')[1];
        } else {
            addressObj.hed__MailingStreet__c = addressObj.MailingStreet;
        }
        addressObj.hed__MailingState__c = addressObj.MailingState;
        addressObj.hed__MailingCity__c = addressObj.MailingCity;
        addressObj.hed__MailingStreet__c = addressObj.MailingStreet;
        addressObj.hed__MailingCountry__c = addressObj.MailingCountry;
        addressObj.hed__MailingPostalCode__c = addressObj.MailingPostalCode;

        this.donationObject = addressObj;
        this.donationObject.Email = this.donationObject.hed__AlternateEmail__c;


        this.showSpinner();
        if (this.donationObject.paymentFrequency == 'Single') {
            createPaymentInformation({
                donationDetails: JSON.stringify(this.donationObject)
            }).then(
                result => {

                    // var inputComponents = [...this.template.querySelectorAll('.donationFormField')];
                    // $(inputComponents[0]).on('touchstart', function() {
                    //     this.showError = true;
                    //     //$(this).focus();
                    //     $(inputComponents[0]).scrollIntoView();
                    // });
                    // $(inputComponents[0]).trigger('touchstart');

                    var topInput = this.template.querySelector(`[data-id="topElement"]`);
                    topInput.scrollIntoView();

                    // $(inputComponents).trigger('touchstart');

                    this.hideSpinner();
                    //Show Asperato Modal
                    this.donationObject.asperatoId = result;
                    this.paymentSuccess = false;
                    this.paymentIframeURL = this.paymentIframeURL + '&pid=' + result;
                    if (this.donationObject.paymentType == 'Paypal') {
                        this.showSpinner();
                        this.processDonationRecords();
                    } else {
                        this.showAsperatoPayment = true;
                    }
                }
            ).catch(error => {
                this.hideSpinner();
                this.customToastNotification('Error', error, true);
            });
        } else {
            createAuthorizationInformation({
                donationDetails: JSON.stringify(this.donationObject)
            }).then(
                result => {

                    var topInput = this.template.querySelector(`[data-id="topElement"]`);
                    topInput.scrollIntoView();

                    // var inputComponents = [...this.template.querySelectorAll('.donationFormField')];
                    // $(inputComponents[0]).on('touchstart', function() {
                    //     this.showError = true;
                    //     //$(this).focus();
                    //     $(this).scrollIntoView();
                    // });
                    // $(inputComponents[0]).trigger('touchstart');

                    // var inputComponents = this.template.querySelector(`[data-id="topElement"]`);

                    // $(inputComponents).on('touchstart', function() {
                    //     $(this).focus();
                    // });

                    // $(inputComponents).trigger('touchstart');

                    this.hideSpinner();
                    //Show Asperato Modal
                    this.donationObject.authorizationId = result;
                    this.paymentSuccess = false;
                    this.showAsperatoPayment = true;
                    this.paymentIframeURL = this.paymentIframeURL + '&aid=' + result;
                }
            ).catch(error => {
                this.hideSpinner();
                this.customToastNotification('Error', error, true);
            });
        }
    }

    processDonationRecords() {
        var hasOpp = this.donationObject;
        hasOpp.hasOpportunity = true;
        hasOpp.opportunityId = this.asperatoOpportunityId;
        this.donationObject = hasOpp;

        createDonationRecords({
            donationDetails: JSON.stringify(this.donationObject)
        }).then(result => {
            if (this.donationObject.paymentType == 'Paypal') {
                window.open(this.paymentIframeURL, '_self');
            } else {
                var thisSucessMessage = this.successMessageCard;
                var thankMessage = thisSucessMessage.replace("AMOUNT", this.donationObject.Amount).replace("REFERENCE", result);
                this.successMessageCard = thankMessage;
                this.paymentSuccess = true;
                this.showAsperatoPayment = false;
            }
            this.hideSpinner();
        }).catch(error => {
            this.hideSpinner();
            this.customToastNotification('Error', error, true);
        });
    }

    validateGiftAidValues() {
        if (!this.donationObject.wishToGiftAid) {
            return true;
        }
        return false;
    }

    handleRegularDonations(event) {
        this.donationObject.paymentCollection = event.target.title;
        const that = this;
        [...this.template.querySelectorAll('.inactiveCollectionButton')]
        .forEach(function(btns) {

            if (btns.title == that.donationObject.paymentCollection) {
                btns.classList.remove("inactiveCollectionButton");
                btns.classList.add("activeCollectionButton");
            }
        });

        [...that.template.querySelectorAll('.activeCollectionButton')]
        .forEach(function(btns) {
            if (btns.title != that.donationObject.paymentCollection) {
                btns.classList.remove("activeCollectionButton");
                btns.classList.add("inactiveCollectionButton");
            }
        });
    }

    handlePaymentCollectionPoint(event) {
        this.donationObject[event.target.name] = event.target.value;
    }

    validateConsentValues() {
        if (!this.donationObject.Alumni_SMS_Consent__c) {
            return true;
        }
        if (!this.donationObject.Alumni_Email_Consent__c) {
            return true;
        }
        if (!this.donationObject.Alumni_Phone_Consent__c) {
            return true;
        }
        if (!this.donationObject.Alumni_Directmail_Consent__c) {
            return true;
        }
        return false;
    }

    handleSelectAll(event) {
        if (event.target.value == 'Yes') {
            [...this.template.querySelectorAll('.keepInTouchNo')]
            .forEach(function(btns) {
                btns.checked = false;
            });

            [...this.template.querySelectorAll('.keepInTouchYes')]
            .forEach(function(btns) {
                btns.checked = true;
            });

            this.donationObject.Alumni_SMS_Consent__c = "Yes";
            this.donationObject.Alumni_Email_Consent__c = "Yes";
            this.donationObject.Alumni_Phone_Consent__c = "Yes";
            this.donationObject.Alumni_Directmail_Consent__c = "Yes";
        } else if (event.target.value == 'No') {
            [...this.template.querySelectorAll('.keepInTouchYes')]
            .forEach(function(btns) {
                btns.checked = false;
            });

            [...this.template.querySelectorAll('.keepInTouchNo')]
            .forEach(function(btns) {
                btns.checked = true;
            });

            this.donationObject.Alumni_SMS_Consent__c = "No";
            this.donationObject.Alumni_Email_Consent__c = "No";
            this.donationObject.Alumni_Phone_Consent__c = "No";
            this.donationObject.Alumni_Directmail_Consent__c = "No";
        }
    }

    redirectAlumni() {
        console.log('this.paypalThankYouUrl ' + this.paypalThankYouUrl);
        window.open(this.paypalThankYouUrl, '_top');
    }

    onCancel() {
        this.showSpinner();
        window.scrollTo(0, 0);
        window.history.back();
    }

    scrollCustomFunction(element) {
        // console.log('insode>>>>>>>.....');
        // console.dir('element>>>>>>>>>>>' + element);
        // let scrollTop = element.scrollTop;
        // console.dir('element>>>>>>>>>>>' + element);
        // let parentELE = document.body.parentElement;

        // console.log('parentELE>>>>>>>.....' + parentELE);

        // const headerOutsideIframe = parentELE.clientHeight;
        // console.log('headerOutsideIframe>>>>>>>.....' + headerOutsideIframe);

        // const finalOffset = element.getBoundingClientRect().top + scrollTop + headerOutsideIframe;
        // console.log('finalOffset b top>>>>>>>.....' + element.getBoundingClientRect().top);
        // console.log('finalOffset>>>>>>>.....' + finalOffset);
        // // window.parent.scrollTo({
        // //     top: 18,
        // //     behavior: 'auto'
        // // });
        //window.parent.scrollTo(0, 0);
        //location.href = "#";
        console.log('loc her');
        location.href = "#topElementDiv";
    }
}