import { LightningElement, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { NavigationMixin } from "lightning/navigation";
import TIME_ZONE from "@salesforce/i18n/timeZone";
import getCalendar from "@salesforce/apex/CT_AddToCalendarController.getCalendar";

const LINE_SEPARATOR = "\n";
const GOOGLE_CALENDAR_URL = "https://calendar.google.com/calendar/r/eventedit";
const YAHOO_CALENDAR_URL = "https://calendar.yahoo.com/?v=60";

export default class Ct_addToCalendar extends NavigationMixin(
  LightningElement
) {
  clientName;
  subject;
  startDateTime;
  endDateTime;
  description;
  location;
  @api recordId;

  connectedCallback() {
    getCalendar({ recordId: this.recordId })
      .then((result) => {
        this.subject = result.subject;
        this.description = result.description;
        this.location = result.location;
        this.startDateTime = result.startDateTime;
        this.endDateTime = result.endDateTime;
      })
      .catch((error) => {
        this.displayErrorMessage(error);
      });
  }
  addToCalendar(event) {
    this.clientName = event.target.value;
    switch (this.clientName) {
      case "google":
        this.navigateToWebPage(this.getGoogleCalendarUrl());
        break;
      case "yahoo":
        this.navigateToWebPage(this.getYahooCalendarUrl());
        break;
      default:
        this.downloadCalendar(this.getInternetCalendar());
        break;
    }
  }

  downloadCalendar(calendarToDownload) {
    let downloadElement = document.createElement("a");
    downloadElement.href =
      "data:text/calendar;charset=utf-8," + encodeURI(calendarToDownload);
    downloadElement.target = "_blank";
    downloadElement.download = "download";
    document.body.appendChild(downloadElement);
    downloadElement.click();
  }

  getGoogleCalendarUrl() {
    let googleCalendar = GOOGLE_CALENDAR_URL;
    googleCalendar += "?text=" + this.subject;
    googleCalendar +=
      "&dates=" +
      this.removeSpecialCharacters(this.startDateTime) +
      "/" +
      this.removeSpecialCharacters(this.endDateTime);
    googleCalendar += "&ctx=" + TIME_ZONE;
    googleCalendar += "&details=" + this.description;
    googleCalendar += "&location=" + this.location;
    return googleCalendar;
  }

  getYahooCalendarUrl() {
    let yahooCalendar = YAHOO_CALENDAR_URL;
    yahooCalendar += "&title=" + this.subject;
    yahooCalendar += "&st=" + this.removeSpecialCharacters(this.startDateTime);
    yahooCalendar += "&et=" + this.removeSpecialCharacters(this.endDateTime);
    yahooCalendar += "&desc=" + this.description;
    yahooCalendar += "&in_loc=" + this.location;
    return yahooCalendar;
  }

  getInternetCalendar() {
    let icalendar = "";
    icalendar += "BEGIN:VCALENDAR" + LINE_SEPARATOR;
    icalendar += "VERSION:2.0" + LINE_SEPARATOR;
    icalendar += "PRODID:-//UoP//University of Portsmouth//EN" + LINE_SEPARATOR;
    icalendar += "BEGIN:VEVENT" + LINE_SEPARATOR;
    icalendar +=
      "DTSTART:" +
      this.removeSpecialCharacters(this.startDateTime) +
      LINE_SEPARATOR;
    icalendar +=
      "DTEND:" +
      this.removeSpecialCharacters(this.endDateTime) +
      LINE_SEPARATOR;
    icalendar += "SUMMARY:" + this.subject + LINE_SEPARATOR;
    icalendar += "DESCRIPTION:" + this.description + LINE_SEPARATOR;
    icalendar += "LOCATION:" + this.location + LINE_SEPARATOR;
    icalendar += "END:VEVENT" + LINE_SEPARATOR;
    icalendar += "END:VCALENDAR";
    return icalendar;
  }

  navigateToWebPage(urlToNavigate) {
    this[NavigationMixin.Navigate](
      {
        type: "standard__webPage",
        attributes: {
          url: urlToNavigate
        }
      },
      false
    );
  }

  removeSpecialCharacters(dateToFormat) {
    dateToFormat = dateToFormat.replace(/-/g, "");
    dateToFormat = dateToFormat.replace(/:/g, "");
    dateToFormat = dateToFormat.replace(/.000Z/g, "Z");
    return dateToFormat;
  }

  displayErrorMessage(error) {
    let message = "Unknown error";
    if (Array.isArray(error.body)) {
      message = error.body.map((e) => e.message).join(", ");
    } else if (typeof error.body.message === "string") {
      message = error.body.message;
    }
    this.dispatchEvent(
      new ShowToastEvent({
        title: "Error loading event",
        message,
        variant: "error"
      })
    );
  }
}