/**
 * @File Name          : ct_CourseLookup.js
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 6/23/2020, 2:50:08 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/17/2020   Creation Admin     Initial Version
**/
import { LightningElement, track } from 'lwc';
import searchRelatedCourse from '@salesforce/apex/ct_CourseLookupController.getCourse';


export default class Ct_CourseLookup extends LightningElement {

  @track isCourse = false;
  @track showAddress = true;
  @track courseList = [];
  @track selectedCourse = {};
  @track searchString;
  @track isLoading;
  @track timeOut;
  
  onInputValueChange(event){
    this.courseList = [];
    this.isCourse = false;
    this.searchString = event.target.value;
    if (event.keyCode == 27 || !this.searchString.trim()) {
      this.nullResultValues();
      const courseEvent = new CustomEvent('courseresults', { detail: '' });
      this.dispatchEvent(courseEvent);
    }
    else{
      this.isLoading = true;
     
      // console.log('timeout'+ this.timeOut);
      // clearTimeout(this.timeOut);
      // this.timeOut = setTimeout(function(){
      // console.log('inside Timeout');
      searchRelatedCourse({ courseName: this.searchString }).then( result => {
        this.isCourse = true;
        this.courseList = [];

        if(result.length > 0
          && this.searchString.trim()){
          this.courseList = result;
        }
        else if( !this.searchString.trim()){
          this.isCourse = false;
          this.courseList = [];
        }
        else{
          this.courseList[0] = {'label': 'No Results Found', 'value': ''};

        }
       
        this.isLoading = false;
        }).catch(error => {
          console.log('err in');
        this.courseList = [];
        var emptyAddressList = [];
        emptyAddressList[0] = {'label': 'No Results Found', 'value': ''};
        this.courseList = emptyAddressList;
        }); 

      // }, 2000);
    }
  }
  nullResultValues(){
    this.courseList = [];
    this.isCourse = false;
  }
  handleSelectedCourse(event){
    var target = event.target;
    if(event.currentTarget.title != 'No Results Found'){
    var selectedSobjectIndex = this.getIndexFromParent(target, "data-selected-index");
    selectedSobjectIndex = selectedSobjectIndex.split('UNIQUE_ID')[0];
    this.searchString = event.currentTarget.title;
    this.nullResultValues();
    if(selectedSobjectIndex){
      const courseEvent = new CustomEvent('courseresults', { detail: selectedSobjectIndex });
      this.dispatchEvent(courseEvent);
    }
  }else{
    this.searchString = null;
    this.nullResultValues();

  }
  }
  getIndexFromParent(target, attributeToFind){
		var thisIndex = target.getAttribute(attributeToFind);
		while (!thisIndex) {
			target = target.parentNode;
			thisIndex = this.getIndexFromParent(target, attributeToFind);
		}
		return thisIndex
	}

}