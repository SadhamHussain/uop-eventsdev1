import { LightningElement, api, wire, track } from "lwc";
import fetchEventAndTickets from "@salesforce/apex/CT_EventSearchController.fetchEventAndTickets";

export default class Ct_ticketSelection extends LightningElement {
  @api selectedEventId;
  @track eventTickets;
  @track selectedEvent;
  @track selectedTickets;
  @track showSearchForm = false;
  eventId;
  error;
  //fieldsForRecordEditForm = ['Name', 'Registration_Type__c', 'Status__c', 'CurrencyIsoCode', 'Event__c', 'Event_Product__c', 'Enquirer__c', 'Contact__c', 'Account_Name__c', 'Event_Order__c', 'Session_Registration_URL__c'];

  @wire(fetchEventAndTickets, { selectedEventId: "$selectedEventId" })
  wiredEvent({ error, data }) {
    if (data) {
      this.selectedEvent = data;
      this.eventTickets = this.selectedEvent.Tickets__r;
      this.error = undefined;
    } else if (error) {
      this.error = error;
      this.selectedEvent = undefined;
    }
  }

  connectedCallback(){
    /*Get Id from the URL To Load A particular Event*/
    var queryString = window.location.href;
    var urlVar = new URL(queryString);
    this.eventId = urlVar.searchParams.get("id");

    if(this.eventId){
      this.showSearchForm = false;
    }
    else{
     this.showSearchForm = true;
    }
  }
  handleNext(event) {
    try {
      let selectedTickets = [];
      let ticketTiles = this.template.querySelectorAll("c-ct_ticket-tile");
      for (let i = 0; i < ticketTiles.length; i++) {
        let eachTicket = ticketTiles[i].eventTicket;
        console.log("eachTicket.Id "+eachTicket.Id);
        console.log("selectedTicketCount "+eachTicket.selectedTicketCount);
        console.log("Total_Price_Per_Ticket__c "+eachTicket.Total_Price_Per_Ticket__c);
        console.log("this.selectedEventId "+this.selectedEventId);
        selectedTickets.push({
          eventId: this.selectedEventId,
          ticketId: eachTicket.Id,
          name: eachTicket.Name,
          ticketCount: ticketTiles[i].selectedTicketCount,
          ticketPrice: ticketTiles[i].eventTicket.Total_Price_Per_Ticket__c,
          totalPrice: parseInt(ticketTiles[i].selectedTicketCount) * parseInt(ticketTiles[i].eventTicket.Total_Price_Per_Ticket__c)
        });
      }
      this.selectedTickets = selectedTickets;
      this.selectedEvent = false;
    } catch (error) {
      console.log(error);
    }
  }
}