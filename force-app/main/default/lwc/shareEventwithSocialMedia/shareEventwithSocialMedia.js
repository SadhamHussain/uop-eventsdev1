import { LightningElement, wire, track, api } from 'lwc';
import getEventRec from '@salesforce/apex/ShareEventWithSocialMedia.getEventRec';

export default class ShareEventwithSocialMedia extends LightningElement {
    @api recordId;
    @track correntrecordId;
    @track evt;
    @track error;
    // @wire(getEventRec) wiredEvent({ error, data }) {
    //     if (data) {
    //         console.log('Event', data);
    //         this.evt = data;
    //     } else if (error) {
    //         console.log(error);
    //         this.error = error;
    //     }
    // }
    
    connectedCallback(){
        this.correntrecordId = this.recordId;
        getEventRec({recordId: this.recordId})
        .then(result => {
            console.log('>>>'+JSON.stringify(result));
            // alert(JSON.stringify(result));
            this.evt = result;
            //this.modalMessages = result;
        })
        .catch(error => {
            this.error = error;
            console.log('error: ' + error);
        })
    }
    handleClickFb(event){
        window.open('https://www.facebook.com/dialog/share?app_id=87741124305&href=https://eventsdev1-universityofportsmouth.cs100.force.com/s/event-details%3Dshare&display=popup');
    }
    handleClickTw(event){
        window.open('https://twitter.com/intent/tweet?url=https%3A//eventsdev1-universityofportsmouth.cs100.force.com/s/event-details');
    }
    handleClickLn(event){
        window.open('https://www.linkedin.com/sharing/share-offsite/?url=https%3A%2F%2Feventsdev1-universityofportsmouth.cs100.force.com/s/event-details%3Fv%3Dpr-4GbR4DpQ%26feature%3Dshare');
    }
}