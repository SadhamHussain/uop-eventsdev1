/**
 * @File Name          : ct_AlumniRegistrationForm.js
 * @Description        : 
 * @Author             : Umashankar Creation
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 07-03-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/6/2020   Creation Admin     Initial Version
 **/
import { LightningElement, track, api, wire } from 'lwc';
import UOP_LOGO from '@salesforce/resourceUrl/ct_UOP_Logo';
import formFactorPropertyName from '@salesforce/client/formFactor';
import getMetaDataWrapperAction from '@salesforce/apex/ct_AlumniRegistrationController.fetchMetadataRecords';
import saveAlumniDetailsAction from '@salesforce/apex/ct_AlumniRegistrationController.saveContactDetails';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {
    fireEvent
  } from 'c/pubsub';
  import {
    CurrentPageReference
  } from 'lightning/navigation';

const TOAST_MESSAGE_TIMEOUT = 3000;

export default class Ct_AlumniRegistrationForm extends LightningElement {
    uopLogoUrl = UOP_LOGO;
    @track showErrorcustomToast = false;
    @track showSuccessCustomToast = false;
    @track showSpinnerBoolean = true;
    @track customToastMessage;
    @track customToastAssistiveText;
    @track alumniObject;
    @track hasError = false;
    @track alumniMetaDataObject;
    @track pickListValuesObject;
    @track todayDate;
    @track isAlumniRegisterSuccessfully = false;
    @wire(CurrentPageReference) pageRef;


    get getMainDivWidth() {
        let widthStyle;
        if (formFactorPropertyName === 'Large') {
            widthStyle = 'width:100%';
        } else if (formFactorPropertyName === 'Medium') {
            widthStyle = 'width:75%';
        } else {
            widthStyle = 'width:100%';
        }
        return widthStyle;
    }

    get getLogoStyle() {
        let logoStyle;
        if (formFactorPropertyName === 'Large') {
            logoStyle = 'width: 25%;height:20%;'
        } else if (formFactorPropertyName === 'Medium') {
            logoStyle = 'width: 200px !important;';
        } else {
            logoStyle = 'width: 200px !important;';
        }
        return logoStyle;
    }

    get getTodayDateValue() {
        var todayDateValue = new Date();
        var nis = todayDateValue.toISOString();
        return nis;


    }

    get errTxtStyle() {
        return 'font-family: "\'Encode Sans Expanded\', sans-serif" !important;color: red;font-size: 15px !important;font-weight: normal;';
    }

    get thankYouFontStyle() {
        let fontStyle;
        if (formFactorPropertyName === 'Large') {
            fontStyle = 'font-size: xx-large !important;font-family:"\'Encode Sans Expanded\', sans-serif";'
        } else if (formFactorPropertyName === 'Medium') {
            fontStyle = 'font-size:larger !important;font-family:"\'Encode Sans Expanded\', sans-serif";';
        } else {
            fontStyle = 'font-size:larger !important;font-family:"\'Encode Sans Expanded\', sans-serif";';
        }
        return fontStyle;
    }

    get getNameClass() {
        return formFactorPropertyName === 'Large' ? 'slds-grid slds-grid--pull-padded slds-grid_vertical' : 'slds-grid slds-grid--pull-padded slds-grid_vertical';
    }

    connectedCallback() {

        //Event Listener to To prompt user and get confirmation before unload ie
        // Back button press, refresh, reload events and closing tab
        window.addEventListener('beforeunload', (event) => {
            if (this.isAlumniRegisterSuccessfully != true) {
                event.preventDefault();
                this.hideSpinner();
                event.returnValue = '';
                return '';
            }
        });



        getMetaDataWrapperAction()
            .then(result => {
                let metaDataWrapper = result;
                let alumniRecords = metaDataWrapper.alumniMetadataRecords;
                let thisAlumniObject = {};
                let metadataObject = {};
                for (let i = 0; i < alumniRecords.length; i++) {
                    thisAlumniObject[alumniRecords[i].SObject_Field__c] = null;
                    metadataObject[alumniRecords[i].SObject_Field__c] = alumniRecords[i];
                }

                this.alumniMetaDataObject = metadataObject;
                this.pickListValuesObject = metaDataWrapper.picklistFields;
                this.alumniObject = thisAlumniObject;
                this.todayDate = metaDataWrapper.todayDate;
                this.hideSpinner();
            })
            .catch(error => {
                this.customToastNotification('Error', error.body.message, true);
            });
    }

    validateInputFields() {
        this.showSpinner();
        var contactRecord = this.alumniObject;
        const allValid = [...this.template.querySelectorAll('.alumniformField')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        if (!allValid) {
            this.hideSpinner();
            this.hasError = true;
            const inputComponents = [...this.template.querySelectorAll('.alumniformField')].find(cmps => cmps.required && !cmps.value || !cmps.reportValidity());

            var ios = navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
            if(!ios){
              inputComponents.focus();
            }
          

        } else if (!contactRecord.MailingCountry) {
            this.hasError = true;
            this.hideSpinner();
            //Force form to scroll to the field where the value doesn't exists
            var inputComponents = this.template.querySelector(`[data-id="countryElement"]`);
            var ios = navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
            if(!ios){
              inputComponents.focus();
            }
            this.template.querySelector('c-ct_-country-picklist').isCountrySelected();
        } else {
            this.hasError = false;
            this.hideSpinner();
            var topInput = this.template.querySelector(`[data-id="topElement"]`);
            topInput.scrollIntoView();
            this.saveAlumniDetails();
            // this.delayTimeout = setTimeout(() => {
            //     this.onErrorMessageClose();
            // }, TOAST_MESSAGE_TIMEOUT);
        }
    }

    onErrorMessageClose() {
        window.clearTimeout(this.delayTimeout);
        this.showErrorcustomToast = false;
        this.showSuccessCustomToast = false;
    }

    onInputFieldOnChange(event) {
        this.alumniObject[event.target.name] = event.target.value;
        if (event.target.name == 'Alumni_Interest__c') {
            let alumniInterestValues = [];
            alumniInterestValues = event.target.value;
            this.alumniObject[event.target.name] = alumniInterestValues.join(';')
        }

    }
    handleSelectedAddress(event) {
        var seletedAddress = event.detail;
        var addressObject = JSON.parse(seletedAddress);
        var contactRecord = this.alumniObject;
        let thisStreet = addressObject.Address1;
      if(addressObject.Address2.toLowerCase() != addressObject.Town.toLowerCase()){
        thisStreet = thisStreet+'\n'+addressObject.Address2;
      }
        
        contactRecord.MailingStreet       = thisStreet;
        contactRecord.MailingCity         = addressObject.Town;
        contactRecord.MailingPostalCode   = addressObject.Postcode;
        contactRecord.MailingCountry      = addressObject.Country;
        contactRecord.MailingState        = addressObject.Region;
        fireEvent(this.pageRef, 'countryupdated', addressObject.Country);

    }
    handleSelectedCourse(event) {
        var selectedCourse = event.detail;
        var contactRecord = this.alumniObject;
        contactRecord.Web_Course__c = selectedCourse;
        this.alumniObject = contactRecord;

    }
    handleCountrySelect(event) {

        var selectedCountry = event.detail;
        var contactRecord = this.alumniObject;
        contactRecord.MailingCountry = selectedCountry;
        this.alumniObject = contactRecord;

    }


    onDateInputFieldOnChange(event) {
        this.alumniObject[event.target.name] = event.target.value;
        event.target.reportValidity();

    }

    showSpinner() {
        this.showSpinnerBoolean = true;
    }

    hideSpinner() {
        this.showSpinnerBoolean = false;
    }

    oncancel() {
        this.showSpinner();
        var topInput = this.template.querySelector(`[data-id="topElement"]`);
        topInput.scrollIntoView();
        window.history.back();
    }

    saveAlumniDetails() {
        if(this.alumniObject.MailingStreet.includes('\n')){
            this.alumniObject.hed__MailingStreet__c = this.alumniObject.MailingStreet.split('\n')[0];
            this.alumniObject.hed__MailingStreet2__c = this.alumniObject.MailingStreet.split('\n')[1];
        }
        else{
            this.alumniObject.hed__MailingStreet__c = this.alumniObject.MailingStreet;
        }
        this.alumniObject.hed__MailingState__c = this.alumniObject.MailingState;
        this.alumniObject.hed__MailingCity__c = this.alumniObject.MailingCity;
        this.alumniObject.hed__MailingCountry__c = this.alumniObject.MailingCountry;
        this.alumniObject.hed__MailingPostalCode__c = this.alumniObject.MailingPostalCode;
        this.alumniObject.Email = this.alumniObject.hed__AlternateEmail__c;
        saveAlumniDetailsAction({ contactDetails: JSON.stringify(this.alumniObject) })
            .then(result => {
                this.isAlumniRegisterSuccessfully = true;
                this.hideSpinner();
            })
            .catch(error => {
                this.customToastNotification('Error', error.body.message, true);
                // this.delayTimeout = setTimeout(() => {
                //     this.onErrorMessageClose();
                // }, TOAST_MESSAGE_TIMEOUT);
            });

    }
    customToastNotification(toastTitle, toastMessage, isErrorMessage) {
        this.hideSpinner();
        var messageString = isErrorMessage ? 'Error' : 'Success';
        const showToastEvent = new ShowToastEvent({ Title: toastTitle, message: toastMessage, variant: messageString });
        this.dispatchEvent(showToastEvent);
        // this.customToastMessage = toastMessage;
        // this.customToastAssistiveText = toastTitle;
        // isErrorMessage ? this.showErrorcustomToast = true : this.showSuccessCustomToast = true;
    }

}