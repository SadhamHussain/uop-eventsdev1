<!-- 
    Copyright (c) 2016, Salesforce.org
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Salesforce.org nor the names of
      its contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
 
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
    COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.
-->  

<apex:page showHeader="false" id="SiteTemplate" standardStylesheets="false" controller="GW_Volunteers.VOL_CTRL_PersonalSiteTemplate" cache="false">

<!-- Load jQuery  -->
    <apex:includeScript value="{!URLFOR($Resource.GW_Volunteers__jQueryFiles, 'js/jquery.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.GW_Volunteers__jQueryFiles, 'js/jquery-ui.custom.min.js')}"/>

<!--Load Bootstrap -->     
    <apex:stylesheet value="{!URLFOR($Resource.GW_Volunteers__Bootstrap4Force)}"/>    
    <apex:stylesheet value="{!URLFOR($Resource.GW_Volunteers__Bootstrap, 'bootstrap/css/bootstrap.min.css')}"/>
    <apex:includescript value="{!URLFOR($Resource.GW_Volunteers__Bootstrap, 'bootstrap/js/bootstrap.min.js')}"/>
    
<!-- Load VolunteersPersonalSiteCSS.css -->
    <apex:stylesheet value="{!strURLtoCSSFile}" />

    <apex:insert name="header">
        <div class="navbar navbar-static-top">
         <div class="row">
        <div class="column" style="float:left;width:40%;padding-left:2%">
         <apex:image value="{!$Resource.ct_UOP_Logo}" width="200" height="200"/>
        </div>
        <div class="column" style="width:65%;">
         <h2 style="color:#3C023C;font-size:24px;text-align:center;"><b>Volunteer Site</b></h2>
        </div>
        </div>
     
          
  
      
        
            
            <div class="navbar-inner" style="padding:0 20px;">              
                <ul class="nav">
                    <li class="{!IF(CONTAINS($CurrentPage.Name, 'PersonalSiteContactInfo'), 'active', '')}"><a href="{!$Page.ct_GW_Volunteers_PersonalSiteContactInfo + '?Language=' + Language}" >{!$Label.GW_Volunteers__labelPersonalSiteContactInfoTitle}</a></li>
                    <li class="{!IF(CONTAINS($CurrentPage.Name, 'ct_PersonalSiteJobListing'), 'active', '')}"><a href="{!$Page.ct_PersonalSiteJobListing + '?Language=' + Language + '&nMonthsToShow=1'}" >{!$Label.GW_Volunteers__labelPersonalSiteJobListingTitle}</a></li>
                    <li class="{!IF(CONTAINS($CurrentPage.Name, 'PersonalSiteReportHours'), 'active', '')}"><a href="{!$Page.GW_Volunteers__PersonalSiteReportHours + '?Language=' + Language}" >{!$Label.GW_Volunteers__labelPersonalSiteReportHoursTitle}</a></li>                
                </ul>
            </div>
        </div>
    </apex:insert>
    
    <div style="padding:20px;" >
        <apex:insert name="body" />
    </div>
    
    <apex:insert name="footer">    
        <div class="navbar navbar-static-bottom">
            <div class="navbar-inner" style="padding:18px 0 0 20px;">
                <p>&copy;  University of Portsmouth 2020</p>
            </div>
        </div>    
        <site:googleAnalyticsTracking />            
    </apex:insert>
    
</apex:page>