/**
 * @File Name          : ct_ContentDocumnetLinkTrigger.trigger
 * @Description        : Trigger for ContentDocumentLink
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 3/19/2020, 5:47:16 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/19/2020   Creation Admin     Initial Version
**/
trigger ct_ContentDocumnetLinkTrigger on ContentDocumentLink (after insert) {
  System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>ContentDocumentLink TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

  if(Trigger.isAfter){
    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>ContentDocumentLink AFTER TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    if(Trigger.isInsert){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>ContentDocumentLink AFTER INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      ct_ContentDocumentLinkHelper.afterInsert(Trigger.newMap); 
    }
  }
}