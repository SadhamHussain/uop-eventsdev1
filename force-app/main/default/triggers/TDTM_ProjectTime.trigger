trigger TDTM_ProjectTime on Project_Time__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    hed.TDTM_Global_API.run(Trigger.isBefore, Trigger.isAfter, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, 
        Trigger.isUnDelete, Trigger.new, Trigger.old, Schema.Sobjecttype.Project_Time__c );
}