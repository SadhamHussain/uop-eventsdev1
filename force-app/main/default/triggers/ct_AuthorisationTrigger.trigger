/**
 * @File Name          : ct_AuthorisationTrigger.cls
 * @Description        : Custom trigger for asperato Authorisation Build over TDTM Framework
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Sadham Creation
 * @Last Modified On   : 5/1/2020, 8:34:39 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/17/2020   Creation Admin     Initial Version
**/

trigger ct_AuthorisationTrigger on asp04__Authorisation__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

  System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CT ASPERATO AUTHORISTAION TRIGGER TDTM TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    hed.TDTM_Global_API.run(Trigger.isBefore, Trigger.isAfter, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, 
        Trigger.isUnDelete, Trigger.new, Trigger.old, Schema.SObjectType.asp04__Authorisation__c);
}