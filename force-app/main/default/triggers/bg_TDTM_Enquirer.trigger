/*****************************************************************
* bg_TDTM_Enquirer
*
* Trigger for Enquirer (Lead)
* 
* Author: Stuart Barber
* Created: 12-09-2018
******************************************************************/

trigger bg_TDTM_Enquirer on Lead (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    hed.TDTM_Global_API.run(Trigger.isBefore, Trigger.isAfter, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, 
        Trigger.isUnDelete, Trigger.new, Trigger.old, Schema.Sobjecttype.Lead);
}