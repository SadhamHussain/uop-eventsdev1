/**
 * @File Name          : ct_EventTrigger.trigger
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 12/12/2019, 2:13:06 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/12/2019   Creation Admin     Initial Version
**/
trigger ct_EventTrigger on Event__c (after insert) {

  if(Trigger.isAfter){
    if(Trigger.isInsert){
      ct_EventTriggerHandler.createCampaignFromEvent(Trigger.newMap);
      ct_EventHandler.updateEventwithRegistrationURL(Trigger.newMap);
    }
  }
}