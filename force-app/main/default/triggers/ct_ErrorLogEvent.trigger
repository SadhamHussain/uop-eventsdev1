/**
 * @File Name          : ct_ErrorLogEvent.trigger
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 19/5/2020, 9:53:34 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/4/2020   Creation Admin     Initial Version
**/
trigger ct_ErrorLogEvent on Error_Log_Event__e (after insert) {
  List<Error_Log_Event__e> errorLogEventObjectList = new List<Error_Log_Event__e>();

  //Collect All Event Records To Pass Into the Handler
  for(Error_Log_Event__e thisEvent : trigger.new){
    errorLogEventObjectList.add(thisEvent);
  }

  if(!errorLogEventObjectList.isEmpty()){
    ct_ErrorLogEventTriggerHandler.insertErrorLogs(errorLogEventObjectList);
    //npsp.TDTM_Config_API.run(false, Trigger.isAfter, false, false, false, false, errorLogEventObjectList, null, Schema.SObjectType.Error_Log_Event__e);
  }
}