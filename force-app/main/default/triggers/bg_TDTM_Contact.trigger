/*
 * Trigger for contact
 * Author: Lizzy Baird
 * Created: 26-07-2019
 */ 

trigger bg_TDTM_Contact on Contact (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    hed.TDTM_Global_API.run(Trigger.isBefore, Trigger.isAfter, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, 
        Trigger.isUnDelete, Trigger.new, Trigger.old, Schema.SObjectType.Contact);
}