/**
 * @File Name          : WebSiteSubmissionEventTrigger.trigger
 * @Description        : Trigger for Web_Site_Submission_Event__e object
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 8/5/2020, 9:39:51 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/17/2020   Creation Admin     Initial Version
**/
trigger ct_WebSiteSubmissionEventTrigger on Web_Site_Submission_Event__e (after insert) {
  
  //hed.TDTM_Global_API.run(false, Trigger.isAfter, false, false, false, false, donationSubmissionRecordDataList, null, Schema.SObjectType.Web_Site_Submission_Event__e);

    List<String> donationSubmissionRecordDataList = new List<String>();
    List<String> alumniSubmissionRecordDataList  = new List<String>();
    List<String> volunteerSubmissionRecordDataList  = new List<String>();

    for(Web_Site_Submission_Event__e thisEvent : trigger.new){
        if(thisEvent.Source__c == ct_Constants.WEB_SUBMISSION_SOURCE_ALUMNI){
            alumniSubmissionRecordDataList.add(thisEvent.Web_Site_Submission_Record__c);
        } 
        else if(thisEvent.Source__c == ct_Constants.WEB_SUBMISSION_SOURCE_DONATION ){
            donationSubmissionRecordDataList.add(thisEvent.Web_Site_Submission_Record__c); 
        } 
        else if(thisEvent.Source__c == ct_Constants.WEB_SUBMISSION_SOURCE_VOLUNTEER ){
          volunteerSubmissionRecordDataList.add(thisEvent.Web_Site_Submission_Record__c);
        }
    }
    if(!donationSubmissionRecordDataList.isEmpty()){
        ct_WebSiteSubmissionEventTriggerHandler.manageWebSiteSubmissionData(donationSubmissionRecordDataList);
    }
    if(!alumniSubmissionRecordDataList.isEmpty()){
        ct_WebSiteSubmissionEventTriggerHandler.manageAlumniSubmissionData(alumniSubmissionRecordDataList);
    }
    if(!volunteerSubmissionRecordDataList.isEmpty()){
      ct_WebSiteSubmissionEventTriggerHandler.manageVolunteerSubmissionData(volunteerSubmissionRecordDataList);
  }
}