/*****************************************************************
* bg_TDTM_Opportunity
*
* Trigger for Opportunity
* 
* Author: Lizzy Baird
* Created: 25-07-2019
******************************************************************/

trigger bg_TDTM_Opportunity on Opportunity (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    hed.TDTM_Global_API.run(Trigger.isBefore, Trigger.isAfter, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, 
        Trigger.isUnDelete, Trigger.new, Trigger.old, Schema.SObjectType.Opportunity);
}