/**
   * @File Name          : ct_VolunteerHoursTrigger
   * @Author             : Creation Admin
   * @Last Modified By   : Creation Admin
   * Description         : To execute ct_VolunteerHoursTriggerHandler class
  **/
trigger ct_VolunteerHoursTrigger on GW_Volunteers__Volunteer_Hours__c (after insert,before update) {
     if(Trigger.isAfter && Trigger.isInsert){
       ct_VolunteerHoursTriggerHandler.afterInsert(Trigger.newMap);
     }
     if(Trigger.isBefore && Trigger.isUpdate){
       ct_VolunteerHoursTriggerHandler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
     }
}