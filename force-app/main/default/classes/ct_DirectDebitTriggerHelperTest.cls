/**
 * @File Name          : ct_DirectDebitTriggerHelperTest.cls
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Description        : To test the ct_DirectDebitTriggerHelper.cls
 */
@isTest public class ct_DirectDebitTriggerHelperTest {
    
  /**
  * @description test GAU population for DD to Allocation
  * @return void 
  **/
  @isTest(SeeAllData=true)
  public static void populateGAUFromDDtoAllocation(){
    List<npsp__Trigger_Handler__c> triggerHandlers = npsp.TDTM_Config_API.getCachedRecords();

		// Add our Trigger Handler to cached Trigger Handlers
		npsp__Trigger_Handler__c th = new npsp__Trigger_Handler__c();
		th.Name                     = 'DirectDebitTriggerHandlerTDTM';
		th.npsp__Class__c           = 'ct_DirectDebitTriggerHandler_TDTM';
		th.npsp__Object__c          = 'npe03__Recurring_Donation__c';
		th.npsp__Trigger_Action__c  = 'BeforeInsert;BeforeUpdate;AfterInsert;AfterUpdate;';
		th.npsp__Active__c          = true;
		th.npsp__Load_Order__c      = 3;
		th.npsp__Asynchronous__c    = false;

    triggerHandlers.add(th);
    
    //Create contact
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    //Create contact
    Campaign thisCampaign = new Campaign();
    thisCampaign.Name = 'Test Campaign';  
    insert thisCampaign;
    //Create Campaign  
    Project__c thisCreationFundProject = ct_TestDataFactory.createProject();
    insert thisCreationFundProject;
    //Create General Accounting Unit
    npsp__General_Accounting_Unit__c thisGAUForDD = ct_TestDataFactory.createGAURecord('Test GAU');
    thisGAUForDD.Project__c = thisCreationFundProject.Id;
    insert thisGAUForDD; 
      
    //Create General Accounting Unit
    npsp__General_Accounting_Unit__c thisTestGAU = ct_TestDataFactory.createGAURecord('Test GAU');
    thisTestGAU.Project__c = thisCreationFundProject.Id;
    insert thisTestGAU;
      
    //Create Authorisation  
    asp04__Authorisation__c thisAuthorisation = ct_TestDataFactory.createAuthorisation(thisContact);
    insert thisAuthorisation;

    Test.startTest();
    //Create Recurring Donation
    npe03__Recurring_Donation__c thisRecurringDonation =  ct_TestDataFactory.createRecurringDonation(thisContact, thisAuthorisation.Id, 200.00);
    thisRecurringDonation.General_Accounting_Unit__c = thisGAUForDD.Id;
    thisRecurringDonation.Primary_Campaign_Source__c = thisCampaign.Id;
    insert thisRecurringDonation;
    Test.stopTest();
     
    List<Opportunity> thisOpportunityQuery                   = new List<Opportunity>([SELECT Id, 
                                                                                             Name, npe03__Recurring_Donation__c, CampaignId 
                                                                                             FROM Opportunity 
                                                                                             WHERE npe03__Recurring_Donation__c =: thisRecurringDonation.Id]);  
   
    // To Check if Recurring Donation primary campaign source and Opportunity primary campaign source is equal or not    
    System.assertEquals(thisCampaign.Id, thisOpportunityQuery[0].CampaignId);
  }

  /**
  * @description test email alert for DD payments
  * @return void 
  **/
  @isTest(SeeAllData=true)
  public static void sentEmailAlertForDirectDebitTest(){
    List<npsp__Trigger_Handler__c> triggerHandlers = npsp.TDTM_Config_API.getCachedRecords();
    // Add our Trigger Handler to cached Trigger Handlers
		npsp__Trigger_Handler__c th = new npsp__Trigger_Handler__c();
		th.Name                     = 'DirectDebitTriggerHandlerTDTM';
		th.npsp__Class__c           = 'ct_DirectDebitTriggerHandler_TDTM';
		th.npsp__Object__c          = 'npe03__Recurring_Donation__c';
		th.npsp__Trigger_Action__c  = 'BeforeInsert;BeforeUpdate;AfterInsert;AfterUpdate;';
		th.npsp__Active__c          = true;
		th.npsp__Load_Order__c      = 3;
		th.npsp__Asynchronous__c    = false;

    triggerHandlers.add(th);

    //Create contact
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    //Create Authorisation  
    asp04__Authorisation__c thisAuthorisation = ct_TestDataFactory.createAuthorisation(thisContact);
    insert thisAuthorisation;
    //Create Recurring Donation
    npe03__Recurring_Donation__c thisRecurringDonation =  ct_TestDataFactory.createRecurringDonation(thisContact, thisAuthorisation.Id, 100.00);
    insert thisRecurringDonation;
    //Create Opportunity  
    Opportunity thisOpportunity = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
    thisOpportunity.Amount = 200.00;
    thisOpportunity.npe03__Recurring_Donation__c = thisRecurringDonation.id;
    insert thisOpportunity;
    //Create Project
    Project__c thisCreationFundProject = ct_TestDataFactory.createProject();
    insert thisCreationFundProject;
    //Create General Accounting Unit
    npsp__General_Accounting_Unit__c thisGAU = ct_TestDataFactory.createGAURecord('Creation GAU');
    thisGAU.Project__c = thisCreationFundProject.Id;
    insert thisGAU;
    
    //Create Allocation
    npsp__Allocation__c thisGAUAllocationOne = ct_TestDataFactory.createGAUAllocation(thisGAU.Id, thisOpportunity.Id, 200.00);
    insert thisGAUAllocationOne;
         
    Test.startTest();
      thisRecurringDonation.Authorisation_Status__c = ct_Constants.AUTHORIZATION_INFORCE_STATUS;
      update thisRecurringDonation;
    Test.stopTest();
  }
}