public class ct_DDMandateRefExtension {
   public List<Document> docRecord {get; set;}
   public Id docId {get; set;}
   public ct_DDMandateRefExtension(ApexPages.StandardController stdController) {
       docRecord = [Select Id,DeveloperName,LastModifiedById from Document where DeveloperName = 'Direct_Debit_Logo' limit 1];  
       docId = docRecord[0].Id;
   }
   
   public ct_DDMandateRefExtension() {
   }
   
   public PageReference addPDFAttachment() {
    Id recordId              = Apexpages.currentPage().getParameters().get('id');
    String isAttachment      = Apexpages.currentPage().getParameters().get('attachment');
    
    PageReference pdfDownloaderPage = Page.ct_DDMandateReferencePage;        
    pdfDownloaderPage.getParameters().put('id', recordId);    

   

    if(isAttachment == 'true'){
      Blob pdfSummary = (Test.isRunningTest() ? Blob.valueOf('Test') : pdfDownloaderPage.getContentAsPDF() );            
      ContentVersion thisContentver   = new ContentVersion();
      thisContentver.ContentLocation  = 'S';
      thisContentver.VersionData      = pdfSummary;
      thisContentver.Title            = 'DD Mandate Reference PDF';
      thisContentver.PathOnClient     = '.pdf';
      insert thisContentver;

      ContentDocumentLink thisLink    = new ContentDocumentLink();
      thisLink.ContentDocumentId      = [Select Id,ContentDocumentId FROM ContentVersion WHERE Id =:thisContentver.Id].ContentDocumentId;
      thisLink.LinkedEntityId         = recordId;
      thisLink.ShareType              = 'I';
      insert thisLink;
      
      PageReference pageRef = new PageReference('/'+recordId);
      pageRef.setRedirect(true);
      return pageRef;
    }
    return pdfDownloaderPage;  
  } 
}