/*****************************************************************
* bg_AppPotentialEnquirerDupesUtils_Test
*
* test methods for bg_AppPotentialEnquirerDupesUtils 
*
* Author: BrightGen
* Created: 27-11-2018
******************************************************************/

@isTest
private class bg_AppPotentialEnquirerDupesUtils_Test {
	
	@isTest static void testCreationOfApplicantPotentialEnquirerRecords() 
	{
		Queuable_Job_Size__c qjs = new Queuable_Job_Size__c(Name = 'Queuable Job Size', Job_Size__c = 50);
        insert qjs;

        Bypass_Configuration__c bc = new Bypass_Configuration__c(Are_Processes_Off__c = true);
        insert bc;
		
		Lead testEnquirer1 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer1.FirstName = 'Stuart';
		testEnquirer1.LastName = 'Little';
		testEnquirer1.Email = 'stuart@little.com';
		testEnquirer1.Phone = '0739357673';
		testEnquirer1.PostalCode = 'E2 8YS';
		
		Lead testEnquirer2 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer2.FirstName = 'S';
		testEnquirer2.LastName = 'Little';
		testEnquirer2.Email = 'stuart@little.com';
		testEnquirer2.Phone = '0795948383';
		testEnquirer2.PostalCode = 'RH14 9AP';

		List<Lead> testEnquirers = new List<Lead>{testEnquirer1, testEnquirer2};
		insert testEnquirers;

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.First_Name__c = 'Stuart'; 
	    testApp1.Last_Name__c = 'Little';
	    testApp1.Email_Address__c = 'stuart@little.com';
	    testApp1.Phone_Number__c = '0739357673';
	    testApp1.Postal_Code__c = 'E2 8YS';

	    List<Application__c> testApplications = new List<Application__c>{testApp1};
	    insert testApplications;

	    List<Application__c> createdTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, 
												 First_Name__c, Last_Name__c, Email_Address__c,
												 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c, Origin__c
												 FROM Application__c
												 WHERE Id IN :testApplications];

		Test.startTest();

			bg_ApplicationUtils.processApplications(createdTestApplications);

			List<Applicant_Potential_Enquirer_Duplicate__c> apedRecords = [SELECT Id, Application__c, Potential_Enquirer_Duplicate__c 
																		   FROM Applicant_Potential_Enquirer_Duplicate__c
																		   WHERE Application__c IN :testApplications];

			system.assertEquals(2, apedRecords.size(), 'Two Applicant Potential Enquirer Duplicate records should be created');

			/*List<Application__c> updatedTestApplication = [SELECT Id, Processing_Status__c, Error_Description__c
												 		   FROM Application__c WHERE Id IN :testApplications];

			String multipleDupeEnquirersErrorMessage = System.Label.App_Multiple_Duplicate_Enquirers_Error_Message;

			system.assertEquals('Errored', updatedTestApplication[0].Processing_Status__c, 'The processing status of the Application should be errored');
			system.assertEquals(multipleDupeEnquirersErrorMessage, updatedTestApplication[0].Error_Description__c, 'The error description should be populated');
			*/
		Test.stopTest();
	}

	/*@isTest static void testFailedCreationOfApplicantPotentialEnquirerRecords() 
	{
		
		Queuable_Job_Size__c qjs = new Queuable_Job_Size__c(Name = 'Queuable Job Size', Job_Size__c = 50);
        insert qjs;

        Bypass_Configuration__c bc = new Bypass_Configuration__c(Are_Processes_Off__c = true);
        insert bc;
		
		Lead testEnquirer1 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer1.FirstName = 'Stuart';
		testEnquirer1.LastName = 'Little';
		testEnquirer1.Email = 'stuart@little.com';
		testEnquirer1.Phone = '0739357673';
		testEnquirer1.PostalCode = 'E2 8YS';
		
		// this has not been inserted and therefore has no Id
		Lead testEnquirer2 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer2.FirstName = 'S';
		testEnquirer2.LastName = 'Little';
		testEnquirer2.Email = 'stuart@little.com';
		testEnquirer2.Phone = '0795948383';
		testEnquirer2.PostalCode = 'RH14 9AP';

		Set<Lead> testEnquirers = new Set<Lead>{testEnquirer1, testEnquirer2};

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.First_Name__c = 'Stuart'; 
	    testApp1.Last_Name__c = 'Little';
	    testApp1.Email_Address__c = 'stuart@little.com';
	    testApp1.Phone_Number__c = '0739357673';
	    testApp1.Postal_Code__c = 'E2 8YS';
		insert testApp1;

		Map<Id, Set<Lead>> multipleDuplicateEnquirersByAppId = new Map<Id, Set<Lead>>();
		multipleDuplicateEnquirersByAppId.put(testApp1.Id, testEnquirers);

	    List<Application__c> createdTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, 
												 First_Name__c, Last_Name__c, Email_Address__c,
												 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c
												 FROM Application__c
												 WHERE Id = :testApp1.Id];

		Test.startTest();

			Map<Id, String> errorMessageByAppId = bg_AppPotentialEnquirerDupesUtils.insertApplicationPotentialEnquirerDuplicateRecords(multipleDuplicateEnquirersByAppId);

			List<Applicant_Potential_Enquirer_Duplicate__c> apedRecords = [SELECT Id, Application__c, Potential_Enquirer_Duplicate__c 
																		   FROM Applicant_Potential_Enquirer_Duplicate__c
																		   WHERE Application__c = :testApp1.Id];

			system.assert(apedRecords.isEmpty(), 'No Appicant Potential Enquirer Duplicate records should be created');

			Application__c updatedTestApplication = [SELECT Id, Processing_Status__c, Error_Description__c
												 		   FROM Application__c WHERE Id = :testApp1.Id];

			String multipleDupeEnquirersErrorMessage = System.Label.App_Multiple_Duplicate_Enquirers_Error_Message;

			system.assertEquals('Errored', updatedTestApplication.Processing_Status__c, 'The processing status of the Application should be errored');
			system.assertNotEquals(multipleDupeEnquirersErrorMessage, updatedTestApplication.Error_Description__c, 'The error description should be populated');

		Test.stopTest();
	}*/
}