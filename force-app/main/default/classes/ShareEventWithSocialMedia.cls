public class ShareEventWithSocialMedia {
    @AuraEnabled(cacheable=true)
    public static Event__c getEventRec(String recordId){
        system.debug('recordId'+recordId);
        Event__c evt = [Select Id, Name, Event_Description__c from Event__c where Id=: recordId];
        return evt;
    }
}