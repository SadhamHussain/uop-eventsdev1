/**
 * @File Name          : ct_CustomSettingService.cls
 * @Description        : Service Class for Custom Settings
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/5/2020, 12:56:12 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/5/2020   Creation Admin     Initial Version
**/
public class ct_CustomSettingService {

  public static Map<Id,  Bypass_Configuration__c> byPassConfigurationMapByUserId = new  Map<Id,  Bypass_Configuration__c>();

  /**
  * @description User's Bypass Configurations custom setting Configuration by User ID
  * @return Bypass_Configuration__c 
  **/
  public static Bypass_Configuration__c getByPassConfigurationByUserId(Id userId){
    if(byPassConfigurationMapByUserId.isEmpty() 
       || byPassConfigurationMapByUserId.containsKey(userId)){
        byPassConfigurationMapByUserId.put(userId, Bypass_Configuration__c.getInstance(userId));
      }
    return byPassConfigurationMapByUserId.get(userId);
  }

  /**
  * @description get User's Bypass Configurations custom setting Configuration
  * @return Bypass_Configuration__c 
  **/
  public static Bypass_Configuration__c getCurrentUserByPassConfiguration(){
    return getByPassConfigurationByUserId(UserInfo.getUserId());
  }
}