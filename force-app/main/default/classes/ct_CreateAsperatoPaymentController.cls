/**
 * @File Name          : ct_CreateAsperatoPaymentController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 28/5/2020, 4:00:31 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    29/4/2020   Creation Admin     Initial Version
**/
public class ct_CreateAsperatoPaymentController {
  public static npe01__OppPayment__c thisGEMPayment;
  public static asp04__Payment__c thisAsperatoPayment;
  public static String showThankyouScreen { get; set;}
  public static string sucessMessage { get; set;}
  public ct_CreateAsperatoPaymentController(){}

  /**
  * @description Manage different source of Asperto Post record creation action
  * @author Creation Admin | 29/4/2020 
  * @return PageReference 
  **/
  public static PageReference managePaymentCreation(){
    Id recordId  = Apexpages.currentPage().getParameters().get('recordId');
    String actionType  = Apexpages.currentPage().getParameters().get('actionType');
    String amount  = Apexpages.currentPage().getParameters().get('amount');
    showThankyouScreen  = Apexpages.currentPage().getParameters().get('showThankyouScreen');
    String asperatoPaymentURL = '';

    if(showThankyouScreen == 'true'){
      //Get Advancment Setting Metadata To Get Payment Method & Asperato URL
      thisGEMPayment = getGEMPaymentRecord(recordId);
      String successMessage = '';
      sucessMessage = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.WEB_DONATION_FORM_CARD_SUCCESS_MESSAGE).Notification_Message__c.replace('PROJECTNAME',thisGEMPayment.npsp__Allocations__r.size() > 0 ? thisGEMPayment.npsp__Allocations__r[0].npsp__General_Accounting_Unit__r.Name : 'N/A').replace('AMOUNT', amount != null ? amount : 'N/A').replace('REFERENCE', thisGEMPayment.Asperato_Payment__r.asp04__Asperato_Reference__c != null ? thisGEMPayment.Asperato_Payment__r.asp04__Asperato_Reference__c : 'N/A');
      return null;
    }
    system.debug('recordId '+recordId);
    thisGEMPayment = getGEMPaymentRecord(recordId);

    if(thisGEMPayment.Asperato_Payment__c != null && thisGEMPayment.Asperato_Payment_Stage__c != ct_Constants.ASPERATO_PAYMENT_STAGE_COLLECTED){
      thisAsperatoPayment = createAsperatoPayment(thisGEMPayment, thisGEMPayment.Asperato_Payment__c);
    } 
    else{
      thisAsperatoPayment = createAsperatoPayment(thisGEMPayment, null);
    }
    
    thisGEMPayment.Asperato_Payment__c = thisAsperatoPayment.Id;
    thisGEMPayment.npe01__Paid__c = false;
    thisGEMPayment.npe01__Payment_Date__c = null;
    thisGEMPayment.npe01__Scheduled_Date__c = System.today();
    thisGEMPayment.npe01__Payment_Method__c = ct_WebDataFormHelper.mapAsperatoPaymentMethod(thisAsperatoPayment.asp04__Payment_Route_Selected__c);
    try{
      update thisGEMPayment;
    }
    catch(Exception e){
      ct_Logger.logMessage(e, 'GEM Payment');
      throw e;
    }

    thisAsperatoPayment.asp04__Success_Endpoint_Long__c = System.URL.getSalesforceBaseUrl().toExternalForm()+'/apex/ct_CreateAsperatoPayment?recordId='+thisGEMPayment.Id+'&actionType=showPaymentModal&showThankyouScreen=true&amount='+thisGEMPayment.npe01__Payment_Amount__c;
    try{
      update thisAsperatoPayment;
    }
    catch(Exception e){
      ct_Logger.logMessage(e, 'Asperato Payment');
      throw e;
    }
    
    asperatoPaymentURL = [SELECT Id, asp04__eCommerce_URL__c 
                                 FROM asp04__Payment__c 
                                 WHERE Id = :thisAsperatoPayment.Id LIMIT 1].asp04__eCommerce_URL__c.split('>')[1].split('<')[0].unescapeHtml4();
    system.debug('asperatoPaymentURL '+asperatoPaymentURL);
    if(actionType == 'showPaymentModal'){
      PageReference page = new PageReference(asperatoPaymentURL);
      page.setRedirect(true);
      return page;
    }
    else if(actionType == 'sendEmail'){
      PageReference page = new PageReference('/'+thisGEMPayment.Id);
      page.setRedirect(true);
      return page;
    }
    
    return null;
  }

  /**
  * @description - Method to fetch the GEM  Payment record
  * @param Id recordId - GEM Payment Record Id
  * @return GEM Payment 
  **/
  public static npe01__OppPayment__c getGEMPaymentRecord(Id recordId){
    return [SELECT Id, npe01__Payment_Amount__c, npe01__Payment_Method__c, npe01__Opportunity__r.npsp__Primary_Contact__r.Email,
                  npe01__Opportunity__c, npe01__Opportunity__r.npsp__Primary_Contact__c, Asperato_Payment_Stage__c, Asperato_Payment__c,
                  Asperato_Payment__r.asp04__Asperato_Reference__c, (SELECT Id, npsp__General_Accounting_Unit__r.Name FROM npsp__Allocations__r LIMIT 1)
                  FROM npe01__OppPayment__c 
                  WHERE Id=: recordId];
  }

  /**
  * @description Create Apserato Payment record
  * @author Creation Admin | 29/4/2020 
  * @param gemPayment 
  * @return asp04__Payment__c 
  **/
  public static asp04__Payment__c createAsperatoPayment(npe01__OppPayment__c gemPayment, Id asperatoId){
    asp04__Payment__c thisAsperatoPayment = new asp04__Payment__c();
    thisAsperatoPayment.Id                                  = asperatoId;
    thisAsperatoPayment.Opportunity__c                      = gemPayment.npe01__Opportunity__c;
    thisAsperatoPayment.asp04__Amount__c                    = gemPayment.npe01__Payment_Amount__c;
    thisAsperatoPayment.asp04__Payment_Route_Selected__c    = ct_Constants.ASPERATO_CARD_PAYMENT_METHOD;
    thisAsperatoPayment.asp04__Payment_Route_Options__c     = ct_Constants.ASPERATO_CARD_PAYMENT_METHOD;
    try{
      upsert thisAsperatoPayment;
    }
    catch(Exception e){
      ct_Logger.logMessage(e, 'Asperato Payment');
      throw e;
    }
    return thisAsperatoPayment;
  }
}