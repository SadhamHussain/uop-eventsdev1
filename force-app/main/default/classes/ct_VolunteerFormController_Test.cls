/**
 * @File Name          : ct_VolunteerFormController_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 15/6/2020, 1:17:15 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/4/2020   Creation Admin     Initial Version
**/
@IsTest
public class ct_VolunteerFormController_Test {
	
    /**
    * @description test volunteer submission from Web Form Data
    * @author Creation Admin | 17/4/2020 
    * @return void 
    **/
    @IsTest(SeeAllData=true)
    static void testVolunteerRecordProcessing(){
        String volunteerFormData = '{'+
                              '"Salutation": "Mr.",'+
                              '"LastName": "uniqueSalesforce",'+
                              '"Email": "javid.sherif@uuik-tec.com",'+
                              '"SMS_Consent__c": "No",'+
                              '"FirstName": "JavidAsperato",'+
                              '"Phone": "9597799033",'+
                              '"Directmail_Consent__c": "Yes",'+
                              '"Email_Consent__c": "No",'+
                              '"hed__AlternateEmail__c": "test@test.com",'+
            				          '"GW_Volunteers__Volunteer_Skills__c": "Computer usage",'+
                              '"GW_Volunteers__Volunteer_Availability__c": "Weekdays",'+
                              '"GW_Volunteers__Volunteer_Notes__c": "Test Note"'+
                            '}';
       ct_VolunteerRegistrationFormController volunteerControllerObj = new ct_VolunteerRegistrationFormController();
       Test.startTest();
        ct_VolunteerRegistrationFormController.fetchMetadataRecords();
        ct_VolunteerRegistrationFormController.saveVolunteerDetails(volunteerFormData);
       Test.stopTest();
    }
}