/***********************************************************************************
* bg_Enquirer_DeleteDuplicateLEIs_TDTM_Test
*
* Test Class of Enquirer Trigger Handler for deleting the duplicate LEIs while merging the enquirers
* Duplicates will be identified based on key combination of Enquirer__c,Level_of_Study__c and Year_of_Entry__c fields of Latest Enquiry Information. 
* After Delete
* 
* Author: Koteswararao Tangella
* Created: 26-06-2019
***********************************************************************************/
@isTest
public class bg_Enquirer_DeleteDuplicateLEIs_TDTMTest {
    
    //Test duplicate LEI deletion on merging the Lead(Enquirer) records
    @isTest
    private static void testDuplicateLEIDeletion(){
        Id studentRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Student').getRecordTypeId();
        List<Latest_Enquiry_Information__c> studentCourseInformations = new List<Latest_Enquiry_Information__c>();
        
        
        //Create bg_Lead_DeleteDuplicateLEIs Trigger Handler records using TDTM structure
        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Enquirer_DeleteDuplicateLEIs_TDTM', 'Lead', 'AfterDelete', 1.00));
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        
        
        //Create a Student Enquirer
        Lead masterStudent = new Lead(RecordTypeId = studentRecordTypeId, Title = 'Mr.', FirstName = 'Test', LastName = 'Test', Company = 'Test', Email = 'test@testuop.com', Status = 'New - Unverified', Enquiry_Channel__c = 'Email');
        insert masterStudent;
        
        //Insert an LEI for the student
        
        studentCourseInformations.add(new Latest_Enquiry_Information__c(Enquirer__c = masterStudent.Id, Level_of_Study__c = 'Undergraduate',Year_of_Entry__c = '2021'));
        
        
        //Create 2 duplicate Student Enquirers with same details as the previous Student
        List<Lead> duplicateStudents = new List<Lead>();
        duplicateStudents.add(new Lead(RecordTypeId = studentRecordTypeId, Title = 'Mr.', FirstName = 'Test1', LastName = 'Test1', Company = 'Test', Email = 'test@testuop.com', Status = 'New - Unverified', Enquiry_Channel__c = 'Email'));
        duplicateStudents.add(new Lead(RecordTypeId = studentRecordTypeId, Title = 'Mr.', FirstName = 'Test2', LastName = 'Test2', Company = 'Test', Email = 'test@testuop.com', Status = 'New - Unverified', Enquiry_Channel__c = 'Email'));
        insert duplicateStudents;
        
        //Insert a duplicate LEI  and a new LEI for the student
        studentCourseInformations.add(new Latest_Enquiry_Information__c(Enquirer__c = duplicateStudents[0].Id, Level_of_Study__c = 'Undergraduate',Year_of_Entry__c = '2021'));
        studentCourseInformations.add(new Latest_Enquiry_Information__c(Enquirer__c = duplicateStudents[1].Id, Level_of_Study__c = 'Undergraduate',Year_of_Entry__c = '2022'));
        
        insert studentCourseInformations;
        
        Test.startTest();
        //Merge the Leads to test duplicate elimination of LEIs associated to the Enquirers
        Database.MergeResult[] results = Database.merge(masterStudent, duplicateStudents, false);                
        Test.stopTest();
        //After merging the 3 leads, only 2 LEIs will be left as one of the LEI is duplicate.
        List<Latest_Enquiry_Information__c> remainingLEIs = [SELECT Id FROM Latest_Enquiry_Information__c];
        System.assertEquals(2, remainingLEIs.size());
    }
}