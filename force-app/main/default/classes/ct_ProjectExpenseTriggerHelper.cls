/**
 * @File Name          : ct_ProjectExepnseTriggerHandler_TDTM.cls
 * @Description        : TDTM Trigger Handler for Project Expense Object
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/18/2020, 7:57:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/18/2020   Creation Admin     Initial Version
**/
public class ct_ProjectExpenseTriggerHelper {
	@future
    public static void updateProjectTaskCustomRollup(set<Id> ProjectTaskIds){
        try{
		//ProjectTask Map to Update
            Map<Id,project_Task__c> projectTaskToUpdateMap = new Map<Id,project_Task__c>();
            //Reintialize the Rollup Field of select ProjectIds
            for(project_Task__c thisProjectTask :[Select Id,Total_Expense__c  FROM project_Task__c WHERE Id IN: ProjectTaskIds]){
                thisProjectTask.Total_Expense__c  = 0.00;
                projectTaskToUpdateMap.put(thisProjectTask.Id,thisProjectTask);
            }
		//AggregateResult SOQL on get Count of Campaigns
		for(AggregateResult thisResult :[Select Sum(Amount__c ) totalAmount, Project_Task__c projectTaskId FROM Project_Expense__c Where Project_Task__c != null and Project_Task__c IN: ProjectTaskIds group by Project_Task__c]){
            project_Task__c thisProjectTask = projectTaskToUpdateMap.get((Id)thisResult.get('projectTaskId'));
            thisProjectTask.Total_Expense__c  =(Decimal)thisResult.get('totalAmount');
            projectTaskToUpdateMap.put((Id)thisResult.get('projectTaskId'),thisProjectTask);
		}
        //Update the database with Rollup count of Campaigns related to project
		if(projectTaskToUpdateMap.size()>0)
	     upsert projectTaskToUpdateMap.values();
	}catch(Exception e){
		ct_Logger.logMessage(e, 'Project_Expense__c');
		throw e;
	}
    }
}