/*****************************************************************
* bg_ApplicationUtils_Test
*
* Test methods for bg_ApplicationUtils_Test
* 
* Author: Stuart Barber
* Created: 26-11-2018
******************************************************************/

@isTest
private class bg_ApplicationUtils_Test {

	@isTest (seeAllData = true)
	private static void testApplicantMatchingExistingEnquirerIsConverted() 
	{
		Lead testEnquirer1 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer1.FirstName = 'test928384';
		testEnquirer1.LastName = 'user';
		testEnquirer1.Email = 'test928384@user.com';
		testEnquirer1.Phone = '01403785206';
		testEnquirer1.PostalCode = 'E2 8YS';
		insert testEnquirer1;

		Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount.Course_Start_Month__c = '09';
		courseAccount.Course_Start_Year__c = '2020';
	    insert courseAccount;

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.First_Name__c = 'test928384'; 
	    testApp1.Last_Name__c = 'user';
	    testApp1.Preferred_Name__c = 'Derek';
	    testApp1.Email_Address__c = 'test928384@user.com';
	    testApp1.Phone_Number__c = '0739357673';
	    testApp1.Postal_Code__c = 'E2 8YS';
	    testApp1.Admission_Cycle__c = 2020;
	    testApp1.Application_Year__c = '2019';
	    testApp1.Domicile_Country_Code__c = 'UK';
	    testApp1.Type__c = 'UCAS';
	    testApp1.Address__c = 'testAddressLine1,testAddressLine2,testAddressLine3,testAddressLine4,testAddressLine5,testAddressLine6,testAddressLine7,testAddressLine8,testAddressLine9,testAddressLine10,testAddressLine11,testAddressLine12,testAddressLine13,testAddressLine14,testAddressLine15,testAddressLine16,testAddressLine17,testAddressLine18,testAddressLine19,testAddressLine20';
	    testApp1.Course__c = courseAccount.Id;
	    testApp1.School_College__c = schoolAccount.Id;


	    List<Application__c> testApplications = new List<Application__c>{testApp1};
	    insert testApplications;

	    List<Application__c> createdTestApplications = [SELECT Id, Processing_Status__c, Enquirer__c,
	    												First_Name__c, Last_Name__c, Email_Address__c,
														Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
														Student_Id__c, SCMS_Application_Id__c,
	    												Applicant__c, Opportunity__c, Origin__c 
														FROM Application__c
														WHERE Id IN :testApplications];

		system.assertEquals(null, createdTestApplications[0].Enquirer__c);
		system.assertEquals(null, createdTestApplications[0].Applicant__c);
		system.assertEquals(null, createdTestApplications[0].Opportunity__c);

		Test.startTest();

		bg_ApplicationUtils.processApplications(createdTestApplications);

		List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
															 Enquirer__c, Opportunity__c, Applicant__c, Valid_Application_Data__c
															 FROM Application__c
															 WHERE Id IN :testApplications];

		Contact applicantContact = [SELECT Id FROM Contact WHERE Student_Applicant_Id__c = :updatedTestApplications[0].Student_Id__c]; 
		Opportunity newOpportunity = [SELECT Id FROM Opportunity WHERE Application_Id__c = :updatedTestApplications[0].SCMS_Application_Id__c]; 

		system.assertEquals(testEnquirer1.Id, updatedTestApplications[0].Enquirer__c);
		system.assertEquals(applicantContact.Id, updatedTestApplications[0].Applicant__c);
		system.assertEquals(newOpportunity.Id, updatedTestApplications[0].Opportunity__c);

		Test.stopTest();
	}

	@isTest static void testUpdateApplicationRecordsWithMultipleDupeEnquirers() {
		
		Queuable_Job_Size__c qjs = new Queuable_Job_Size__c(Name = 'Queuable Job Size', Job_Size__c = 50);
        insert qjs;

		Lead testEnquirer1 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer1.FirstName = 'test928384';
		testEnquirer1.LastName = 'user';
		testEnquirer1.Email = 'test928384@user.com';
		testEnquirer1.Phone = '0739357673';
		testEnquirer1.PostalCode = 'E2 8YS';
		
		Lead testEnquirer2 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer2.FirstName = 'S';
		testEnquirer2.LastName = 'user';
		testEnquirer2.Email = 'test928384@user.com';
		testEnquirer2.Phone = '0795948383';
		testEnquirer2.PostalCode = 'RH14 9AP';

		List<Lead> testEnquirers = new List<Lead>{testEnquirer1, testEnquirer2};
		insert testEnquirers;

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.First_Name__c = 'test928384'; 
	    testApp1.Last_Name__c = 'user';
	    testApp1.Email_Address__c = 'test928384@user.com';
	    testApp1.Phone_Number__c = '0739357673';
	    testApp1.Postal_Code__c = 'E2 8YS';

	    List<Application__c> testApplications = new List<Application__c>{testApp1};
	    insert testApplications;

	    List<Application__c> createdTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, 
												 First_Name__c, Last_Name__c, Email_Address__c,
												 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c, Origin__c
												 FROM Application__c
												 WHERE Id IN :testApplications];

		Test.startTest();

			bg_ApplicationUtils.processApplications(createdTestApplications);

			List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Error_Description__c
															FROM Application__c
															WHERE Id IN :createdTestApplications];

			system.assert(!updatedTestApplications.isEmpty());

			String expectedProcessingStatus = 'Errored';

			for(Application__c app : updatedTestApplications)
			{
				system.assertEquals(expectedProcessingStatus, app.Processing_Status__c, 
									'The expected Processing Status should equal: ' + expectedProcessingStatus);
			}

		Test.stopTest();
	}

	@isTest (seeAllData = true)
	private static void testApplicationForExistingContactIsConvertedIntoContact() 
	{
		Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount.Course_Start_Month__c = '09';
		courseAccount.Course_Start_Year__c = '2020';
	    insert courseAccount;

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.SCMS_Application_Id__c = 'testapp0001';
		testApp1.First_Name__c = 'test928384'; 
	    testApp1.Last_Name__c = 'user';
	    testApp1.Preferred_Name__c = 'Derek';
	    testApp1.Email_Address__c = 'test928384@user.com';
	    testApp1.Phone_Number__c = '0739357673';
	    testApp1.Postal_Code__c = 'E2 8YS';
	    testApp1.Admission_Cycle__c = 2020;
	    testApp1.Application_Year__c = '2019';
	    testApp1.Domicile_Country_Code__c = 'UK';
	    testApp1.Type__c = 'UCAS';
	    testApp1.Address__c = 'testAddressLine1,testAddressLine2,testAddressLine3,testAddressLine4,testAddressLine5,testAddressLine6,testAddressLine7,testAddressLine8,testAddressLine9,testAddressLine10,testAddressLine11,testAddressLine12,testAddressLine13,testAddressLine14,testAddressLine15,testAddressLine16,testAddressLine17,testAddressLine18,testAddressLine19,testAddressLine20';
	    testApp1.Course__c = courseAccount.Id;
	    testApp1.School_College__c = schoolAccount.Id;

	    List<Application__c> testApplications = new List<Application__c>{testApp1};
	    insert testApplications;

		bg_ApplicationUtils.processApplications(testApplications);

		List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
															 Enquirer__c, Opportunity__c, Applicant__c, Valid_Application_Data__c
															 FROM Application__c
															 WHERE Id IN :testApplications];

		Contact applicantContact = [SELECT Id, Most_Recent_Application__c FROM Contact WHERE Student_Applicant_Id__c = :updatedTestApplications[0].Student_Id__c]; 
		Opportunity newOpportunity = [SELECT Id FROM Opportunity WHERE Application_Id__c = :updatedTestApplications[0].SCMS_Application_Id__c]; 

		system.assertEquals(applicantContact.Id, updatedTestApplications[0].Applicant__c);
		system.assertEquals(applicantContact.Most_Recent_Application__c, updatedTestApplications[0].Id);
		system.assertEquals(newOpportunity.Id, updatedTestApplications[0].Opportunity__c);

		Application__c testApp2 = bg_UnitTestDataFactory.createApplication();
	    testApp2.SCMS_Application_Id__c = 'testapp0002';
		testApp2.First_Name__c = 'test928384'; 
	    testApp2.Last_Name__c = 'user';
	    testApp2.Preferred_Name__c = 'Derek';
	    testApp2.Email_Address__c = 'test928384@user.com';
	    testApp2.Phone_Number__c = '0739357673';
	    testApp2.Postal_Code__c = 'E2 8YS';
	    testApp2.Admission_Cycle__c = 2021;
	    testApp2.Application_Year__c = '2020';
	    testApp2.Domicile_Country_Code__c = 'UK';
	    testApp2.Type__c = 'UCAS';
	    testApp2.Address__c = 'testAddressLine1,testAddressLine2,testAddressLine3,testAddressLine4,testAddressLine5,testAddressLine6,testAddressLine7,testAddressLine8,testAddressLine9,testAddressLine10,testAddressLine11,testAddressLine12,testAddressLine13,testAddressLine14,testAddressLine15,testAddressLine16,testAddressLine17,testAddressLine18,testAddressLine19,testAddressLine20';
	    testApp2.Course__c = courseAccount.Id;
	    testApp2.School_College__c = schoolAccount.Id;

		List<Application__c> newTestApplication = new List<Application__c>{testApp2};
	    insert newTestApplication;

		Test.startTest();

		bg_ApplicationUtils.processApplications(newTestApplication);

		Test.stopTest();

		List<Application__c> appRelatedToExistingContact = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
															 Enquirer__c, Opportunity__c, Applicant__c, Valid_Application_Data__c
															 FROM Application__c
															 WHERE Id IN :newTestApplication];
		
		Contact updatedApplicantContact = [SELECT Id, Most_Recent_Application__c FROM Contact WHERE Student_Applicant_Id__c = :appRelatedToExistingContact[0].Student_Id__c]; 
		List<Opportunity> newOpportunityForExistingContact = [SELECT Id FROM Opportunity WHERE Applicant__c = :updatedApplicantContact.Id]; 

		system.assertEquals(appRelatedToExistingContact[0].Id, updatedApplicantContact.Most_Recent_Application__c);
		system.assertEquals(2, newOpportunityForExistingContact.size(), 'There should be two opportunities for a single contact');
	}
	
	@isTest (seeAllData = true)
	private static void testMultipleApplicationsMatchingExistingEnquirerIsConverted() 
	{
		Lead testEnquirer1 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer1.FirstName = 'test928384';
		testEnquirer1.LastName = 'user';
		testEnquirer1.Email = 'test928384@user.com';
		testEnquirer1.Phone = '01403785206';
		testEnquirer1.PostalCode = 'E2 8YS';
		insert testEnquirer1;

		Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount.Course_Start_Month__c = '09';
		courseAccount.Course_Start_Year__c = '2020';
	    insert courseAccount;

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.First_Name__c = 'test928384'; 
	    testApp1.Last_Name__c = 'user';
	    testApp1.Preferred_Name__c = 'Derek';
	    testApp1.Email_Address__c = 'test928384@user.com';
	    testApp1.Phone_Number__c = '0739357673';
	    testApp1.Postal_Code__c = 'E2 8YS';
	    testApp1.Admission_Cycle__c = 2020;
	    testApp1.Application_Year__c = '2019';
	    testApp1.Domicile_Country_Code__c = 'UK';
	    testApp1.Type__c = 'UCAS';
	    testApp1.Address__c = 'testAddressLine1,testAddressLine2,testAddressLine3,testAddressLine4,testAddressLine5,testAddressLine6,testAddressLine7,testAddressLine8,testAddressLine9,testAddressLine10,testAddressLine11,testAddressLine12,testAddressLine13,testAddressLine14,testAddressLine15,testAddressLine16,testAddressLine17,testAddressLine18,testAddressLine19,testAddressLine20';
	    testApp1.Course__c = courseAccount.Id;
	    testApp1.School_College__c = schoolAccount.Id;
		testApp1.SCMS_Application_Id__c = 'testapp1';

		Application__c testApp2 = bg_UnitTestDataFactory.createApplication();
	    testApp2.First_Name__c = 'test928384'; 
	    testApp2.Last_Name__c = 'user';
	    testApp2.Preferred_Name__c = 'Derek';
	    testApp2.Email_Address__c = 'test928384@user.com';
	    testApp2.Phone_Number__c = '0739357673';
	    testApp2.Postal_Code__c = 'E2 8YS';
	    testApp2.Admission_Cycle__c = 2020;
	    testApp2.Application_Year__c = '2019';
	    testApp2.Domicile_Country_Code__c = 'UK';
	    testApp2.Type__c = 'UCAS';
	    testApp2.Address__c = 'testAddressLine1,testAddressLine2,testAddressLine3,testAddressLine4,testAddressLine5,testAddressLine6,testAddressLine7,testAddressLine8,testAddressLine9,testAddressLine10,testAddressLine11,testAddressLine12,testAddressLine13,testAddressLine14,testAddressLine15,testAddressLine16,testAddressLine17,testAddressLine18,testAddressLine19,testAddressLine20';
	    testApp2.Course__c = courseAccount.Id;
	    testApp2.School_College__c = schoolAccount.Id;
		testApp2.SCMS_Application_Id__c = 'testapp2';


	    List<Application__c> testApplications = new List<Application__c>{testApp1,testApp2};
	    insert testApplications;

	    List<Application__c> createdTestApplications = [SELECT Id, Processing_Status__c, Enquirer__c,
	    												First_Name__c, Last_Name__c, Email_Address__c,
														Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
														Student_Id__c, SCMS_Application_Id__c,
	    												Applicant__c, Opportunity__c, Origin__c 
														FROM Application__c
														WHERE Id IN :testApplications];

		system.assertEquals(null, createdTestApplications[0].Enquirer__c);
		system.assertEquals(null, createdTestApplications[0].Applicant__c);
		system.assertEquals(null, createdTestApplications[0].Opportunity__c);
		system.assertEquals(null, createdTestApplications[1].Enquirer__c);
		system.assertEquals(null, createdTestApplications[1].Applicant__c);
		system.assertEquals(null, createdTestApplications[1].Opportunity__c);

		Test.startTest();

		bg_ApplicationUtils.processApplications(createdTestApplications);

		List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
															 Enquirer__c, Opportunity__c, Applicant__c, Valid_Application_Data__c
															 FROM Application__c
															 WHERE Id IN :testApplications];

		system.assertEquals(null, updatedTestApplications[0].Enquirer__c);
		system.assertEquals(null, updatedTestApplications[0].Applicant__c);
		system.assertEquals(null, updatedTestApplications[0].Opportunity__c);
		system.assertNotEquals(null, updatedTestApplications[1].Enquirer__c);
		system.assertNotEquals(null, updatedTestApplications[1].Applicant__c);
		system.assertNotEquals(null, updatedTestApplications[1].Opportunity__c);

		Contact applicantContact = [SELECT Id FROM Contact WHERE Student_Applicant_Id__c = :updatedTestApplications[1].Student_Id__c]; 
		List<Opportunity> noNewOpportunityCreated = [SELECT Id FROM Opportunity WHERE Application_Id__c = :updatedTestApplications[0].SCMS_Application_Id__c];
		List<Opportunity> newOpportunityCreated = [SELECT Id FROM Opportunity WHERE Application_Id__c = :updatedTestApplications[1].SCMS_Application_Id__c];  

		system.assert(noNewOpportunityCreated.isEmpty());
		system.assertEquals(testEnquirer1.Id, updatedTestApplications[1].Enquirer__c);
		system.assertEquals(applicantContact.Id, updatedTestApplications[1].Applicant__c);
		system.assertEquals(newOpportunityCreated[0].Id, updatedTestApplications[1].Opportunity__c);

		Test.stopTest();
	}

}