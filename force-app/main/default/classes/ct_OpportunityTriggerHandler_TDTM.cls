/**
 * @File Name          : ct_OpportunityTriggerHandler.cls
 * @Description        : Opportunity TDTM Trigger Handler 
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 24/6/2020, 6:36:52 pm
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    3/9/2020   Creation Admin     Initial Version
**/
global class ct_OpportunityTriggerHandler_TDTM extends hed.TDTM_Runnable {
  static boolean hasExecuted = false;
  global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
  hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {

  System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>OPPORTUNITY TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
  hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();

  if(triggerAction == hed.TDTM_Runnable.Action.BeforeInsert){
    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>OPPORTUNITY BEFORE INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    Set<Id> accountIds = new Set<Id>();
    //Set of Id to store Paused Recurring Donation Id
    Set<Id> recurringDonationIds = new Set<Id>();
    Set<Id> volunteerOppWonContactIds  = new Set<Id>();
    Map<Id, Account> accountMap = new Map<Id, Account>();
    //Set of Id to store Paused Recurring Donations
    Map<Id, npe03__Recurring_Donation__c> pausedRecurringDonations = new Map<Id, npe03__Recurring_Donation__c>();

    for(Opportunity thisOpportunity: (list<Opportunity>)newlist){
      //Process Application Opportunity for disabling duplicate Contact role creation
      ct_OpportunityTriggerHelper.processApplicationOpportunity(thisOpportunity);
      
      if((thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE
         || thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_VOLUNTEER)
         && thisOpportunity.AccountId != null){
        accountIds.add(thisOpportunity.AccountId);
      }

      if(thisOpportunity.npe03__Recurring_Donation__c != null){
       recurringDonationIds.add(thisOpportunity.npe03__Recurring_Donation__c);
      }

      //Invoke method for processing Donation size in opportunity
      ct_OpportunityTriggerHelper.populateDonationSize(thisOpportunity);
    }
    accountMap = ct_OpportunityTriggerHelper.getAccountMap(accountIds);
    if(!recurringDonationIds.isEmpty()){
      pausedRecurringDonations = ct_OpportunityTriggerHelper.getPausedRecurringDonations(recurringDonationIds);
    }

    for(Opportunity thisOpportunity: (list<Opportunity>)newlist){
      if(thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE 
        || thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_VOLUNTEER){
        ct_OpportunityTriggerHelper.updateOpportunityName(thisOpportunity, accountMap);
      }
      //Process Paused Donations
      if(thisOpportunity.npe03__Recurring_Donation__c != null
        && thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE
        && !pausedRecurringDonations.isEmpty()
        && pausedRecurringDonations.keyset().contains(thisOpportunity.npe03__Recurring_Donation__c)){
          ct_OpportunityTriggerHelper.processPausedOpportunity(thisOpportunity);
       }
    }
  }

  if (triggerAction == hed.TDTM_Runnable.Action.BeforeUpdate){
  System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>OPPORTUNITY BEFORE UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    Map<Id, Opportunity> triggerOldMap = new Map<Id, Opportunity>((list<Opportunity>)oldlist);
    Map<Id, Opportunity> triggerNewMap = new Map<Id, Opportunity>((list<Opportunity>)newlist);

    Set<Id> paidDonationOpportunitiesId   = new Set<Id>();
    Set<Id> accountIds                    = new Set<Id>();
    Map<Id, Account> accountMap           = new Map<Id, Account>();
    Set<Id> volunteerOppWonContactIds = new Set<Id>();
       
    for(Opportunity thisOpportunity : triggerNewMap.values()){
      if((thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE
         || thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_VOLUNTEER)
         && thisOpportunity.AccountId != null){
        accountIds.add(thisOpportunity.AccountId);
      }
        
      if( thisOpportunity.Amount != triggerOldMap.get(thisOpportunity.Id).Amount ){
        //Invoke method for processing Donation size in opportunity
        ct_OpportunityTriggerHelper.populateDonationSize(thisOpportunity);
      }
    }
    accountMap = ct_OpportunityTriggerHelper.getAccountMap(accountIds);

    for(Opportunity thisOpportunity: triggerNewMap.values()){
      if(thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE
         || thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_VOLUNTEER){
        ct_OpportunityTriggerHelper.updateOpportunityName(thisOpportunity, accountMap);
      }
    }
  }

  if (triggerAction == hed.TDTM_Runnable.Action.BeforeDelete) {
   System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>OPPORTUNITY BEFORE DELETE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
   Map<Id, Opportunity> triggerOldMap = new Map<Id, Opportunity>((list<Opportunity>)oldlist);
   
   Set<Id> accountIds  = new Set<Id>();
   Set<Id> donorContactToBeProcess = new Set<Id>(); // Set of Contact Id which for which Donor status and type needs to be processed
   Map<Id, Opportunity> deletedOpportunityWithAccountId = new Map<Id, Opportunity>();
   //Set Of Donation Opportunity Record type Ids
   Set<Id> donationOpportunityRecordTypeIds = new Set<Id>{ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_LEGACY_DONATION_PLEDGE,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_MAJOR_DONATION,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_IN_KIND_GIFT};
      
   for(Opportunity thisOpportunity : triggerOldMap.values()){
    if(thisOpportunity.AccountId != null){
      accountIds.add(thisOpportunity.AccountId);
      deletedOpportunityWithAccountId.put(thisOpportunity.Id, thisOpportunity);
    }

    if(donationOpportunityRecordTypeIds.contains(thisOpportunity.RecordTypeId)
      && thisOpportunity.npsp__Primary_Contact__c != null){
      donorContactToBeProcess.add(thisOpportunity.npsp__Primary_Contact__c);
    }
   }

   if(!deletedOpportunityWithAccountId.isEmpty()){
    //Process Account Team list for opportunities Account once deleted
    dmlWrapper.objectsToDelete.addAll(ct_OpportunityTriggerHelper.reevaluateAccountTeamMemberList(deletedOpportunityWithAccountId, accountIds));
   }
   if(!donorContactToBeProcess.isEmpty()
     && !System.isBatch()
     && !System.isFuture()
     && !System.isQueueable()
     && !System.isScheduled()){
     //Invokes Future Method to update donor details in Contact record
     ct_OpportunityTriggerHelper.updateDonorDetails(donorContactToBeProcess);
   }
  }

  if (triggerAction == hed.TDTM_Runnable.Action.AfterInsert){
    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>OPPORTUNITY AFTER INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    Set<Id> opportunityIds                                  = new Set<Id>();
    Set<Id> recurringDonationIds                            = new Set<Id>();
    Set<Id> accountIdToBeProcessAccountTeam                 = new Set<Id>();
    Set<Id> opportunityWithAccountIds                       = new Set<Id>();

    Map<Id, Opportunity> triggerNewMap = new Map<Id, Opportunity>((list<Opportunity>)newlist);

    Set<Id> donorContactToBeProcess = new Set<Id>(); // Set of Contact Id which for which Donor status and type needs to be processed
    //Set Related Administrative Account To Private If Opportunity Recordtype Is Otherthan Application
    ct_OpportunityTriggerHelper.updateRelatedAccountToPrivate(triggerNewMap.keySet());
    //Set Of Donation Opportunity Record type Ids
    Set<Id> donationOpportunityRecordTypeIds = new Set<Id>{ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_LEGACY_DONATION_PLEDGE,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_MAJOR_DONATION,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_IN_KIND_GIFT};
   
    for(Opportunity thisOpportunity : triggerNewMap.values()){
      if(thisOpportunity.npe03__Recurring_Donation__c != null){
        opportunityIds.add(thisOpportunity.Id);
        recurringDonationIds.add(thisOpportunity.npe03__Recurring_Donation__c);
      }

      if(donationOpportunityRecordTypeIds.contains(thisOpportunity.RecordTypeId)
        && thisOpportunity.npsp__Primary_Contact__c != null){
        donorContactToBeProcess.add(thisOpportunity.npsp__Primary_Contact__c);
      }
      
      if(thisOpportunity.AccountId != null){
        //For creating Account Team Member
        accountIdToBeProcessAccountTeam.add(thisOpportunity.AccountId);
        opportunityWithAccountIds.add(thisOpportunity.Id);
      }
    }

    if(!recurringDonationIds.isEmpty()){
      //ct_OpportunityTriggerHelper.getRelatedRecurringAttachment(recurringDonationIds, opportunityIds);
    }

    if(!donorContactToBeProcess.isEmpty()
      && !System.isBatch()
      && !System.isFuture()
      && !System.isQueueable()
      && !System.isScheduled()){
      //Invokes Future Method to update donor details in Contact record
      ct_OpportunityTriggerHelper.updateDonorDetails(donorContactToBeProcess);
    }

    if(!accountIdToBeProcessAccountTeam.isEmpty()){
      //Create Account Team list for opportunities Account based on Department user configuration
      dmlWrapper.objectsToInsert.addAll(ct_OpportunityTriggerHelper.createAccountTeamMember(triggerNewMap, accountIdToBeProcessAccountTeam, opportunityWithAccountIds));
    }
  }

  if (triggerAction == hed.TDTM_Runnable.Action.AfterUpdate){
    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>OPPORTUNITY AFTER UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    Map<Id, Opportunity> triggerOldMap = new Map<Id, Opportunity>((list<Opportunity>)oldlist);
    Map<Id, Opportunity> triggerNewMap = new Map<Id, Opportunity>((list<Opportunity>)newlist);

    Set<Id> activeOpportunityIds = new Set<Id>();
    set<Id> applicationOpportunities = new Set<Id>();
    Set<Id> applicantIds = new Set<Id>();
    Set<Id> courseIds = new Set<Id>();
    Set<Id> accountIdToBeProcessAccountTeam = new Set<Id>();
    Set<Id> opportunityWithAccountIds                       = new Set<Id>();
    Set<Id> opportunitiesWithUpdatedRecordType = new Set<Id>();
    Set<Id> oldAccountIdToBeReevaluatedAccountTeam  = new Set<Id>();
    Map<Id, Opportunity> updatedOpportunityWithAccountIdToBeReevaluated = new Map<Id, Opportunity>();

    Set<Id> donorContactToBeProcess = new Set<Id>(); // Set of Contact Id which for which Donor status and type needs to be processed
    //Set Of Donation Opportunity Record type Ids
    Set<Id> donationOpportunityRecordTypeIds = new Set<Id>{ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_LEGACY_DONATION_PLEDGE,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_MAJOR_DONATION,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_IN_KIND_GIFT};

    //Rejected Opportunity whose Affilation need to be updated
    Map<String, Opportunity> applicationOpportunityMap = new Map<String, Opportunity>();

      for(Opportunity thisOpportunity : triggerNewMap.values()){

        if(thisOpportunity.RecordTypeId != triggerOldMap.get(thisOpportunity.Id).RecordTypeId){
          opportunitiesWithUpdatedRecordType.add(thisOpportunity.Id);
        }

        if(thisOpportunity.StageName == ct_Constants.PLEDGE_OPPORTUNITY_STAGE
        && thisOpportunity.StageName != triggerOldMap.get(thisOpportunity.Id).StageName){
          activeOpportunityIds.add(thisOpportunity.Id);
        }

      if(thisOpportunity.Applicant__c != null
        && thisOpportunity.Course__c != null
        && triggerOldMap.get(thisOpportunity.Id).Applicant__c == null
        && thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_COURSE_ADMISSION
        && thisOpportunity.StageName == ct_Constants.OPPORTUNITY_STAGE_APPLIED){
          applicationOpportunities.add(thisOpportunity.Id);
      }

      if(thisOpportunity.Applicant__c != null
        && thisOpportunity.Course__c != null
        && thisOpportunity.StageName != triggerOldMap.get(thisOpportunity.Id).StageName
        && (thisOpportunity.StageName == ct_Constants.OPPORTUNITY_STAGE_CLOSED_LOST
          || thisOpportunity.StageName == ct_Constants.OPPORTUNITY_STAGE_DECLINED_OFFER
          || thisOpportunity.StageName == ct_Constants.OPPORTUNITY_STAGE_NOT_OFFER
          || thisOpportunity.StageName == ct_Constants.OPPORTUNITY_STAGE_WITHDRAWN)
        && thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_COURSE_ADMISSION){
          applicantIds.add(thisOpportunity.Applicant__c);
          courseIds.add(thisOpportunity.Course__c);
          applicationOpportunityMap.put(thisOpportunity.Course__c+'_'+thisOpportunity.Applicant__c, thisOpportunity);
      }

      if(donationOpportunityRecordTypeIds.contains(thisOpportunity.RecordTypeId)
        && thisOpportunity.npsp__Primary_Contact__c != null
        && thisOpportunity.StageName != triggerOldMap.get(thisOpportunity.Id).StageName){
        donorContactToBeProcess.add(thisOpportunity.npsp__Primary_Contact__c);
      }

      if(thisOpportunity.AccountId != null
       && thisOpportunity.AccountId != triggerOldMap.get(thisOpportunity.Id).AccountId){
        accountIdToBeProcessAccountTeam.add(thisOpportunity.AccountId);
        opportunityWithAccountIds.add(thisOpportunity.Id);

        if(triggerOldMap.get(thisOpportunity.Id).AccountId != null){
          oldAccountIdToBeReevaluatedAccountTeam.add(triggerOldMap.get(thisOpportunity.Id).AccountId);
          updatedOpportunityWithAccountIdToBeReevaluated.put(thisOpportunity.Id, triggerOldMap.get(thisOpportunity.Id));
        }
      }
    }

    if(!opportunitiesWithUpdatedRecordType.isEmpty()){
      //Set Related Administrative Account To Private If Opportunity Recordtype Is Otherthan Application
      ct_OpportunityTriggerHelper.updateRelatedAccountToPrivate(opportunitiesWithUpdatedRecordType);
    }

    if(!applicantIds.isEmpty()
    && !courseIds.isEmpty()
    && !applicationOpportunityMap.isEmpty()){
      //Update relate Affiliation records status for rejected Application opportunity
      dmlWrapper.objectsToUpdate.addAll(ct_OpportunityTriggerHelper.updateAffiliationStatus(applicationOpportunityMap, courseIds, applicantIds));
    }

    if(!activeOpportunityIds.isEmpty()){
      ct_OpportunityTriggerHelper.createAsperatoPaymentForDirectDebitPayments(activeOpportunityIds);
    }
    
    if(!applicationOpportunities.isEmpty() & !hasExecuted){
      hasExecuted = true;
      //Create affiliation record once Application opportunity is created
      dmlWrapper.objectsToInsert.addAll(ct_OpportunityTriggerHelper.createAffiliation(triggerNewMap, applicationOpportunities));
    }

    if(!donorContactToBeProcess.isEmpty()
      && !System.isBatch()
      && !System.isFuture()
      && !System.isQueueable()
      && !System.isScheduled()){
      //Invokes Future Method to update donor details in Contact record
      ct_OpportunityTriggerHelper.updateDonorDetails(donorContactToBeProcess);
    }

    if(!updatedOpportunityWithAccountIdToBeReevaluated.isEmpty()){
      //Process Account Team list for opportunities whose account was updated
      dmlWrapper.objectsToDelete.addAll(ct_OpportunityTriggerHelper.reevaluateAccountTeamMemberList(updatedOpportunityWithAccountIdToBeReevaluated, oldAccountIdToBeReevaluatedAccountTeam));
    }

    if(!accountIdToBeProcessAccountTeam.isEmpty()){
      //Create Account Team list for opportunities Account based on Department user configuration
      dmlWrapper.objectsToInsert.addAll(ct_OpportunityTriggerHelper.createAccountTeamMember(triggerNewMap, accountIdToBeProcessAccountTeam, opportunityWithAccountIds));
    }
  }  
  return dmlWrapper;
  }
}