/*****************************************************************
* bg_CountryCodeHelper
*
* Utility methods mapping country codes to country mapping object
* 
* Test Class: bg_CountryCodeHelper_Test
******************************************************************/

global class bg_CountryCodeHelper {
    
    private static Map<string, Country_Mapping__c> countryMappingByCountryCode;
    
    public static void setResidenceStatusOnContactInsert(List<Contact> insertedContacts)
    {
        for(Contact insertedContact : insertedContacts)
        {            
            if(insertedContact.Country_of_Residence_Code__c != null)
            {
                insertedContact.Residence_Status__c = getResidenceStatusFromCountryCode(insertedContact.Country_of_Residence_Code__c);
            }
        }
    }
    
    public static void setResidenceStatusOnUpdatedContacts(List<Contact> updatedContacts, Map<Id, Contact> oldContactsById)
    {
        for(Contact updatedContact : updatedContacts)
        {
            Contact oldContact = oldContactsById.get(updatedContact.Id);
            if(updatedContact.Country_of_Residence_Code__c != oldContact.Country_of_Residence_Code__c && updatedContact.Country_of_Residence_Code__c != null)
            {
                updatedContact.Residence_Status__c = getResidenceStatusFromCountryCode(updatedContact.Country_of_Residence_Code__c);
            }
        }  
    }
    
    private static string getResidenceStatusFromCountryCode(string countryCode)
    {
        string residenceStatus = '';
        setCountryMappingsByCountryCode();
        
        if(countryMappingByCountryCode.containsKey(countryCode))
        {
            Country_Mapping__c mappingToUse = countryMappingByCountryCode.get(countryCode);
            residenceStatus = mappingToUse.Residence_Status__c;
        }
        return residenceStatus;
    }
    
    private static void setCountryMappingsByCountryCode()
    {
        if(countryMappingByCountryCode == null)
        {
            countryMappingByCountryCode = new Map<string, Country_Mapping__c>(); 
            List<Country_Mapping__c> allCountryMappings = Country_Mapping__c.getAll().Values();
           
            for(Country_Mapping__c retrievedMapping : allCountryMappings)
            {
                countryMappingByCountryCode.put(retrievedMapping.Country_Code__c, retrievedMapping);
            }
        }        
    }
    
    

}