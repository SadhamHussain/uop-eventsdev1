/**
 * @File Name          : ct_EventHandler.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 08/06/2020, 5:10:58 PM
 * @Modification Log   : 
**/
public class ct_EventHandler {
    /**
     * description: method to get Base URL for communities
     */
    public static string getBaseURLforCommunity(){
        List<Network> listOfntwrk = [SELECT id, Name,UrlPathPrefix FROM Network];
        String communityUrl = Network.getLoginUrl(listOfntwrk[0].Id);
        string baseUrl = communityUrl.substring(0,57);
        return baseUrl;
    }
    /**
     * description: update affected Event record with Event Registration URL field.
     */
    public static void updateEventwithRegistrationURL(Map<Id,Event__c> mapOfEvents){
        //List of affected Events
        List<Event__c> events = [Select id,Event_Registration_URL__c from Event__c where Id =:mapOfEvents.keyset()];
        List<Event__c> lstOfEventstoUpdate = new List<Event__c>();
        //get base Url for community
        String registrationURL = ct_EventHandler.getBaseURLforCommunity();
        //concatenate base URL with community suffix and URL parameter.
        registrationURL = registrationURL + System.Label.community_page_URL_suffix;
        system.debug('registrationURL>> '+registrationURL);
        for(Event__c evt : events){
            evt.Event_Registration_URL__c = registrationURL+evt.id;
            lstOfEventstoUpdate.add(evt);
        }
        if(lstOfEventstoUpdate.size()>0){
            //update affected Events.
            update lstOfEventstoUpdate;
        }
    }
}