/**
 * @File Name          : ct_LeadDuplicateResult.cls
 * @Description        : Provides a wrapper for display and selection purposes for the options presented in the duplicate list in the visualflow.
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 2/11/2020, 8:14:54 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    12/15/2019   Creation Admin     Initial Version
**/
public class ct_LeadDuplicateResult {
  /**
   * If Extra attributes are required then create an Aura Enabled Attribute with specific type for this wrapper 
   * and assign the values to it in ct_DuplicateCheckApexAction Class
  **/

  //Attribute which is used as the Option label in Duplicate Records radio input
  @AuraEnabled 
  public String displayText;    
  //Attribute to store Lead's First Name
  @AuraEnabled 
  public String leadFirstName;
  //Attribute to store Lead's Last Name
  @AuraEnabled 
  public String leadlastName;
  //Attribute to store Lead's Email Address
  @AuraEnabled 
  public String leadEmailAddress;
  //Attribute to store Lead's Record Id
  @AuraEnabled 
  public String IdValue;
  //Attribute to store Lead's Record URL which is used to redirect the page to record on clicking the radio option
  @AuraEnabled 
  public String recordURL;
  //Attribute to store selected Contact Id if present
  @AuraEnabled 
  public String selectedContactId;
  //Attribute to store selected Lead 
  @AuraEnabled 
  public String selectedLeadId;
  //Boolean Attribute which denotes that the duplicate record is contact record or not
  @AuraEnabled 
  public Boolean isContact;
}