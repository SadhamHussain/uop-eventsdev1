/**
 * @File Name          : ct_GEMPaymentTriggerHelper.cls
 * @Description        : Helper class for ct_GEMPaymentTriggerHandler class 
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 18/5/2020, 8:35:44 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/13/2020   Creation Admin     Initial Version
**/
public class ct_GEMPaymentTriggerHelper {
  /**
  * @getRelatedOpportunityRecords - Method to query related Recurring Donation record using opportunity Ids
  * @param Set<Id> opportunityIds - Set of Opportunity
  * @return Map<Id, String> - Map od Opportunity Id to Payment method values
  **/
  public static Map<Id, Opportunity> getRelatedOpportunityRecords(Set<Id> opportunityIds){
    Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
    for(Opportunity thisOpportunity : [SELECT Id, npe03__Recurring_Donation__c, npe03__Recurring_Donation__r.npsp__PaymentMethod__c, AccountId,
                                                npsp__Primary_Contact__c     
                                                FROM Opportunity
                                                WHERE Id IN: opportunityIds]){
      opportunityMap.put(thisOpportunity.Id, thisOpportunity);
                                              
    }
    return opportunityMap;
  }

  /**
  * @description - Method to send Email Alerts for Paid Donation Opportunities to Donor or Fundraiser
  * @param opportunityNewMap - Opportunity trigger New Map
  * @param paidGemPaymentIds - Paid Donation Opportunity Ids
  * @return void 
  **/
  public static void sentEmailAlertForPaidDonation(Set<Id> paidGemPaymentIds){
    //Using this set to store Processed Opportunity Id to avoid re-processing again
    Set<Id> opportunityIdSet = new Set<Id>();
    //Get UOP Advancement Team Email Address 
    Id orgWideEmailId;
    OrgWideEmailAddress[] orgWideAddress = ct_Util.getOrgWideEmail(ct_Constants.UOP_ADVANCEMENT_TEAM_ADDRESS); 
    if (orgWideAddress.size() > 0 ) {
      orgWideEmailId = orgWideAddress.get(0).Id; 
    }
    //Get Thank You Donation to Donor Email Template
    EmailTemplate thisThankYouEmailTemplate = [SELECT Id, DeveloperName, HTMLValue, Subject 
                                                FROM EmailTemplate 
                                                WHERE DeveloperName =: ct_Constants.THANK_DONOR_FOR_DONATION_EMAIL_TEMPLATE  
                                                AND IsActive = true];

      //List of SingleEmailMessage to be Sent
      List<Messaging.SingleEmailMessage> mailListToSend = new List<Messaging.SingleEmailMessage>();
      if(orgWideEmailId != null){
      //Get All GAU allocation records to Paid Payment Record 
      for(npsp__Allocation__c thisGAUAllocation : [SELECT Id, npsp__General_Accounting_Unit__c, 
                                                      npsp__Opportunity__c,
                                                      npsp__Amount__c,
                                                      npsp__Payment__r.Asperato_Payment__r.asp04__Asperato_Reference__c,
                                                      npsp__Payment__r.npe01__Opportunity__c,
                                                      npsp__Payment__r.npe01__Opportunity__r.Need_Transactional_Email__c,
                                                      npsp__Payment__r.npe01__Opportunity__r.Amount,
                                                      npsp__Payment__r.npe01__Opportunity__r.RecordTypeId,
                                                      npsp__Payment__r.npe01__Opportunity__r.npsp__Primary_Contact__c,
                                                      npsp__Payment__r.npe01__Opportunity__r.npsp__Primary_Contact__r.Email,
                                                      npsp__Payment__r.npe01__Opportunity__r.npsp__Primary_Contact__r.IsEmailBounced,
                                                      npsp__General_Accounting_Unit__r.Name,
                                                      npsp__General_Accounting_Unit__r.Project__c,
                                                      npsp__General_Accounting_Unit__r.Project__r.OwnerId
                                                      FROM npsp__Allocation__c 
                                                      WHERE npsp__Payment__c IN: paidGemPaymentIds]){
      //Update GAU Name in the HTML content of the template
      EmailTemplate thisEmailTemplate = thisThankYouEmailTemplate;
      if(thisGAUAllocation.npsp__General_Accounting_Unit__r.Name != null){
        string templateBody   = thisEmailTemplate.HTMLValue.replace('GAU NAME', thisGAUAllocation.npsp__General_Accounting_Unit__r.Name);
        thisEmailTemplate.HTMLValue = templateBody;
      }
      else{
        string templateBody   = thisEmailTemplate.HTMLValue.replace('GAU NAME', '');
        thisEmailTemplate.HTMLValue = templateBody;
      }
       
      if(thisGAUAllocation.npsp__Payment__r.Asperato_Payment__r.asp04__Asperato_Reference__c != null){
        string templateBody   = thisEmailTemplate.HTMLValue.replace('ASPERATO REFERENCE', thisGAUAllocation.npsp__Payment__r.Asperato_Payment__r.asp04__Asperato_Reference__c);
        thisEmailTemplate.HTMLValue = templateBody;
      }
      else{
        string templateBody   = thisEmailTemplate.HTMLValue.replace('ASPERATO REFERENCE', '');
        thisEmailTemplate.HTMLValue = templateBody;
      }
                                                          
        /* If Donation amount is greater than or equal to High value Donation as per configured in Advancement Setting Metadata
        *  then send email alert to Fundraiser
        */
        if(thisGAUAllocation.npsp__Amount__c >= ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.HIGH_VALUE_ONLINE_DONATION_ADVANCEMENT_SETTING).Amount_Value__c
          && thisGAUAllocation.npsp__General_Accounting_Unit__r.Project__c != null
          && thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE
          && thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.Need_Transactional_Email__c == ct_Constants.OPPORTUNITY_SEND_TRANSACTIONAL_EMAIL_YES
          ){
          mailListToSend.add(createHighValueDonationEmailMessage(String.valueOf(thisGAUAllocation.npsp__General_Accounting_Unit__r.Project__r.OwnerId),String.valueOf(thisGAUAllocation.Id),orgWideEmailId));                                                      
        }

        /* If Donation amount is less than High value Donation as per configured in Advancement Setting Metadata
        *  then send Thank You Email to Donor
        */
        else if(thisGAUAllocation.npsp__Amount__c < ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.HIGH_VALUE_ONLINE_DONATION_ADVANCEMENT_SETTING).Amount_Value__c
         && thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.npsp__Primary_Contact__c != null
         && thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE
         && !thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.npsp__Primary_Contact__r.IsEmailBounced
         && thisThankYouEmailTemplate != null
         && thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.npsp__Primary_Contact__r.Email != null
         && thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.Need_Transactional_Email__c == ct_Constants.OPPORTUNITY_SEND_TRANSACTIONAL_EMAIL_YES){
          opportunityIdSet.add(thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__c);
          mailListToSend.add(createNonHighValueDonationEmailMessage(thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.npsp__Primary_Contact__r.Email, thisGAUAllocation.npsp__Payment__c, thisGAUAllocation.npsp__Payment__r.npe01__Opportunity__r.npsp__Primary_Contact__c,  thisEmailTemplate.Id, thisEmailTemplate.HTMLValue , thisEmailTemplate.Subject, orgWideEmailId));                                                      
        }                                                          
      }
      if(!mailListToSend.isEmpty()
      && !Test.isRunningTest()){
        //Sent Single Email Messages to respective recipients 
        Messaging.sendEmail(mailListToSend);         
      }
      //Update Acknowledgment Status and Acknowledgment Date fields once 
      if(!opportunityIdSet.isEmpty()){         
        updateOpportunityAcknowledgmentFields(opportunityIdSet); 
      }
    }
  }

  /**
  * @description - Method to create SingleEmailMessage for High value Donation Opportunity to Fundraiser
  * @param fundraiserOwnerId - Fundraiser Owner Id
  * @param gauAllocationId - GAU Allocation Record Id
  * @param orgwideEmailId - UOP Advancement Team Org wide Address Id
  * @return Messaging.SingleEmailMessage for Fundraiser 
  **/
  public static Messaging.SingleEmailMessage createHighValueDonationEmailMessage(String fundraiserOwnerId, String gauAllocationId, Id orgwideEmailId){
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setOrgWideEmailAddressId(orgwideEmailId); 
    mail.toAddresses = new String[] { fundraiserOwnerId };
    mail.subject = 'High value Donation';
    String messageBody = '<html><body>' + ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.HIGH_VALUE_ONLINE_DONATION_ADVANCEMENT_SETTING).Notification_Message__c+'<br/> Donation Allocation Link:<br/>'+URL.getSalesforceBaseUrl().toExternalForm() + '/'+gauAllocationId+'</body></html>';
    mail.setHtmlBody(messageBody);
    return mail;
  }

  /**
  * @description - Method to create SingleEmailMessage for NON High value Donation Opportunity to Donor
  * @param toEmailAddress - Donor Email Address
  * @param opportunityId - Donation Opportunity Record Id
  * @param thisContactId - Donor Contact Id
  * @param thankYouEmailTempId - Thank You Donation to Donor Email Template
  * @param orgwideEmailId - UOP Advancement Team Org wide Address Id
  * @return Messaging.SingleEmailMessage for Donor 
  **/
  public static Messaging.SingleEmailMessage createNonHighValueDonationEmailMessage(String toEmailAddress,Id gemPaymentId, Id thisContactId, Id thankYouEmailTempId, String templateBody, String templateSubject, Id orgwideEmailId){
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setOrgWideEmailAddressId(orgwideEmailId); 
    mail.setToAddresses(new String[]{toEmailAddress});
    mail.setTargetObjectId(thisContactId);
    mail.setHtmlBody(templateBody);
    mail.setSubject(templateSubject);
    mail.setTemplateId(thankYouEmailTempId);
    mail.setWhatId(gemPaymentId);
    mail.setTreatBodiesAsTemplate(true);
    return mail;
  }

  /**
  * @description 
  * @param gemPaymentNewMap 
  * @param paidGEMIds 
  * @return void 
  **/
  public static List<Opportunity> updateOpportunityPaymentStatus(Map<Id, npe01__OppPayment__c> gemPaymentNewMap, Set<Id> paidGEMIds){
    Map<Id, Opportunity> opportunityMapToUpdate = new Map<Id, Opportunity>();
    
    for(Id thisGEMId : paidGEMIds){
       Opportunity thisOpportunity          = new Opportunity();
       thisOpportunity.Id                   = gemPaymentNewMap.get(thisGEMId).npe01__Opportunity__c;
       thisOpportunity.Payment_Status__c    = ct_Constants.OPPORTUNITY_PAYMENT_STATUS_PAID;
       opportunityMapToUpdate.put(thisOpportunity.Id, thisOpportunity);
    }
    return opportunityMapToUpdate.values();
    // try{
    //   update opportunityMapToUpdate.values();
    // }
    // catch(Exception e){
    //   ct_Logger.logMessage(e, 'Opportunity');
    //   throw e;
    // }
  }

  /**
  * @description - Update Acknowledgment Status and Date field once email sent to Donor
  * @param opportunityIdSet 
  * @return void 
  **/
  public static void updateOpportunityAcknowledgmentFields(Set<Id> opportunityIdSet){
    List<Opportunity> opportunityListToUpdate = new List<Opportunity>();
    
    for(Id thisOpportunityId : opportunityIdSet){
       Opportunity thisOpportunity                    = new Opportunity();
       thisOpportunity.Id                             = thisOpportunityId;
       thisOpportunity.npsp__Acknowledgment_Status__c = ct_Constants.EMAIL_ACKNOWLEDGMENT_STATUS_ACKNOWLEDGED;
       thisOpportunity.npsp__Acknowledgment_Date__c   = System.today();
       opportunityListToUpdate.add(thisOpportunity);
    }
    try{
      update opportunityListToUpdate;
    }
    catch(Exception e){
      ct_Logger.logMessage(e, 'Opportunity');
      throw e;
    }
  }  
}