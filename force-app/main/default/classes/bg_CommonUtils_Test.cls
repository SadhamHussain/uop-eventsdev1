/*****************************************************************
 * bg_CommonUtils_Test
 *
 * Test methods for bg_CommonUtils class
 *
 * Author: Stuart Barber
 * Created: 11-09-2018
 *************** Version history ***********************
 * CT, Dinesh : 10/07/2020 - Update the query to get the Record type Id by Record type Developer Name 
 *                                            instead of Record type Name
******************************************************************/

@isTest
private class bg_CommonUtils_Test {
	
	/*
	*
	* To test the recordType Id is returned when providing the Object name and Record Type name
	*
	*/
	@isTest static void testGetRecordTypeID() 
	{
		Test.startTest();
    //CT, Dinesh : 10/07/2020 : Update the query to get the Record type Id by Record type Developer Name instead of Record type Name
		RecordType leadRecordType = [SELECT Id, DeveloperName, SObjectType FROM RecordType WHERE SObjectType = 'Lead' LIMIT 1];
		Id leadRecordTypeId = bg_CommonUtils.getRecordTypeID(leadRecordType.SObjectType, leadRecordType.DeveloperName);

		System.assertEquals(leadRecordType.Id, leadRecordTypeId, 'Record Type Ids should match');

		Test.stopTest();
	}
	
	/*
	*
	* To test whether a record Id is of a certain type
	*
	*/
	@isTest static void testIsIdOfType() 
	{
		Contact testContact = new Contact();
		testContact.FirstName = 'Test';
		testContact.LastName = 'User';
		testContact.Email = 'test@user.com';

		insert testContact;

		Test.startTest();

		Contact createdContact = [SELECT Id, Name FROM Contact WHERE Id = :testContact.Id LIMIT 1];
		Boolean isContact = bg_CommonUtils.isIdOfType(createdContact.Id, '003');

		System.assertEquals(TRUE, isContact, 'Id should be of type Contact');

		Test.stopTest();
	}

	/*
	*
	* To test whether a record Id is of type 'Group' / 'Queue'
	*
	*/
	@isTest static void testIsGroupId() 
	{
		Group testQueue = new Group();
		testQueue.Name = 'Test Domestic Postgraduate Marketing';
		testQueue.DeveloperName = 'Test_Domestic_Postgraduate_Marketing';
		testQueue.Type = 'Queue';

		insert testQueue;

		Test.startTest();

		Group createdQueue = [SELECT Id, Type, Name FROM Group WHERE Id = :testQueue.Id LIMIT 1];
		Boolean isQueue = bg_CommonUtils.isGroupId(createdQueue.Id);

		System.assertEquals(TRUE, isQueue, 'Id should be of type Queue');

		Test.stopTest();
	}
}