/*****************************************************************
* bg_LeadTestDataFactory
*
* Utility class for loading leads and accounts for the unit test class
* bg_GetAccountFromName_Test
* 
* Author: Mark Brown
* Created: 14-06-2019
* Update MB 12/07/2019 added new methods for bg_EnquirerUtils.convertNewLeadAndMergeToExistingContact 00043307
******************************************************************/
@isTest
public class bg_LeadTestDataFactory
{
    public static List<Account> createTestAccount(Id AdminAccountRecordID,Id EducationalInstituteRecordID, Integer numAccts)
    {
        List<Account> accts = new List<Account>();

        // create 3 educational institution accounts
        for(Integer i=0;i<numAccts;i++) {
            Integer accNumber = i + 1;
            Account a = new Account(RecordtypeId=EducationalInstituteRecordID,Name='Educational Account '+accNumber);
            accts.add(a);
        }

        // create 3 non-educational institution accounts
        for(Integer i=3;i<numAccts*2;i++) {
            Integer accNumber = i - 2;
            Account a = new Account(RecordtypeId=AdminAccountRecordID,Name='Admin Account '+accNumber);
            accts.add(a);
        }

        // Create 2 educational instatution accunts with identical names
        Account a = new Account(RecordtypeId=EducationalInstituteRecordID,Name='Duplicate Account');
        accts.add(a);
        

        Account b = new Account(RecordtypeId=EducationalInstituteRecordID,Name='Duplicate Account');
        accts.add(b);

        return accts;
    }

    // Create 2 leads with different error states for testing updates
    public static List<Lead> createTestLeads(Id StudentEnquirerRecordID)
    {
        List<Lead> leads = new List<Lead>();
 
        Lead lead1 = new Lead(RecordtypeId=StudentEnquirerRecordID,FirstName='Test',LastName='Lead',Company ='Test Lead',School_College_Freetext__c='Duplicate Account',Status='New - Unverified',Enquiry_Channel__c='Email');
        leads.add(lead1);

        Lead lead2 = new Lead(RecordtypeId=StudentEnquirerRecordID,FirstName='Test',LastName='Lead',Company ='Test Lead',School_College_Freetext__c='invalidAccountName',Status='New - Unverified',Enquiry_Channel__c='Email');
        leads.add(lead2);

        return leads;
    }

    // Getter for leads
    public static Lead getWrittenLead(ID leadID)
    {
        Lead writtenLead = [
            SELECT  Id,
            School_College__c,
            Multiple_School_Account_Matches__c,
            Failed_School_Account_Matches__c,
            School_College_Freetext__c,                                    
            RecordTypeId,
            Enquiry_Channel__c,
            FirstName,
            LastName,
            Email,
            Phone,
            PostalCode,
            IsConverted                                   
            FROM Lead WHERE Id=:leadID
        ];
        return writtenLead;
    }


    public static void createTestStudentAccount()
    {
        Account account = new Account(Name='TestStudentAccount');
        insert account;

        List<Contact> contactList = new List<Contact>();

        Contact contact = new Contact();
        contact.FirstName='Joe';
        contact.LastName='Bloggs';
        contact.Email = 'joe.bloggs@test.com';
        contact.Phone = '0121 123 4567';
        contact.MailingStreet = '1 Main Street';
        contact.MailingCity = 'London';
        contact.MailingPostalCode = 'N1 1AA';
        contact.MailingCountry = 'United Kingdom';
        contact.Accountid= account.id;
        //insert contact;
        contactList.add(contact);

        contact contact2 = new contact();
        contact2.FirstName='';
        contact2.LastName='Nofirstname';
        contact2.Email = 'joe.nofirstname@test.com';
        contact2.Phone = '0121 123 4567';
        contact2.MailingStreet = '1 Main Street';
        contact2.MailingCity = 'London';
        contact2.MailingPostalCode = 'N1 1AA';
        contact2.MailingCountry = 'United Kingdom';
        contact2.Accountid= account.id;
        contactList.add(contact2);

        contact contact3 = new contact();
        contact3.FirstName='';
        contact3.LastName='NoPhone';
        contact3.Email = 'joe.nophone@test.com';
        contact3.Phone = '0121 123 4567';
        contact3.MailingStreet = '1 Main Street';
        contact3.MailingCity = 'London';
        contact3.MailingPostalCode = 'N1 1AA';
        contact3.MailingCountry = 'United Kingdom';
        contact3.Accountid= account.id;
        contactList.add(contact3);

        Contact contact4 = new Contact();
        contact4.FirstName='';
        contact4.LastName='NoPostalCode';
        contact4.Email = 'joe.nopostalcode@test.com';
        contact4.Phone = '0121 123 4567';
        contact4.MailingStreet = '1 Main Street';
        contact4.MailingCity = 'London';
        contact4.MailingPostalCode = 'N1 1AA';
        contact4.MailingCountry = 'United Kingdom';
        contact4.Accountid= account.id;
        contactList.add(contact4);
        insert contactList;                       
    }


    public static Lead createSingleLead(ID rt,String eq,String fn, String ln, String em, String pc, String phone)
    {
        Lead lead = new Lead();
        lead.RecordTypeId = rt;
        lead.Enquiry_Channel__c = eq;
        lead.FirstName = fn;
        lead.LastName = ln;
        lead.Email = em;
        lead.Phone = phone;
        lead.PostalCode = pc;
        lead.Company = fn + ' ' + ln;
        return lead;
    }

}