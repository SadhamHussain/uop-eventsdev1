/*****************************************************************
* bg_Contact_AppProcessing_TDTM
*
* Contact Trigger Handler for processing Application records
******************************************************************/

global class bg_Contact_AppProcessing_TDTM extends hed.TDTM_Runnable{
    
    public static boolean afterInsertHasRun = false;
    public static boolean afterUpdateHasRun = false;
    
    /*
    * Main method for handling logic for each trigger context
    */
	public override DmlWrapper run(List<SObject> newList, List<SObject> oldList, 
    Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {  
        Bypass_Configuration__c configBypass = Bypass_Configuration__c.getInstance(UserInfo.getProfileId());
        if(triggerAction == Action.beforeInsert)
        {
            List<Contact> newContacts = newList;
            bg_CountryCodeHelper.setResidenceStatusOnContactInsert(newContacts);
        }
        else if(triggerAction == Action.afterInsert && !bg_Contact_AppProcessing_TDTM.afterInsertHasRun)
        {
            List<Contact> newContacts = newList;
            bg_ContactConsentManager.createAndInsertConsentsForNewContacts(newContacts);
            bg_Contact_AppProcessing_TDTM.afterInsertHasRun = true;
        }
        else if(triggerAction == Action.beforeUpdate)
        {
            List<Contact> newContacts = newList;
            List<Contact> oldContacts = oldList;
            
            Map<Id,Contact> oldContactsById = new Map<Id,Contact>(oldContacts);
            bg_CountryCodeHelper.setResidenceStatusOnUpdatedContacts(newContacts, oldContactsById);
        }
        else if(!configBypass.Are_Processes_Off__c)
        {
            if (triggerAction == Action.AfterUpdate && !bg_Contact_AppProcessing_TDTM.afterUpdateHasRun)
            {
                List<Contact> newContacts = newList;
                List<Contact> oldContacts = oldList;
    
                Map<Id,Contact> oldContactsById = new Map<Id,Contact>(oldContacts);
                bg_ContactUtils.processUpdatedContactsAfterUpdate(newContacts, oldContactsById);
                bg_Contact_AppProcessing_TDTM.afterUpdateHasRun = true;
            }
        }

        return null;
    }

}