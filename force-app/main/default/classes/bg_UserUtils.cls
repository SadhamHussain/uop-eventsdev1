/*****************************************************************
* bg_UserUtils
*
* Utility methods mapping country codes to country mapping object
* 
* Test Class: bg_UserUtils_Test
******************************************************************/

public class bg_UserUtils
{

	public static string getProfileName(Id profileId)
    {
        string profileName = 'NoProfile';
        List<Profile> retrievedProfile = [SELECT Id, Name FROM Profile WHERE Id =: profileId];

        if(!retrievedProfile.isEmpty())
        {
            profileName = retrievedProfile[0].Name;
        }

        return profileName;
    }

}