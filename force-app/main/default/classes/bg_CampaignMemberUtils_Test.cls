////////////////////////////////////////////////////////////////////////////////
//  bg_CampaignMemberUtils_Test
//
//	Test methods for bg_CampaignMemberUtils class
//
//	@Author:		Ben Riminton - BrightGen Ltd
//	@Created:		06/09/2019
//
//	@Changes: 
////////////////////////////////////////////////////////////////////////////////
@isTest
private class bg_CampaignMemberUtils_Test 
{

    @TestSetup
    private static void SetupTestData()
    {
        // Create Campaign
        Campaign testCampaign = new Campaign();

        testCampaign.Name = 'Test Campaign';

        insert testCampaign;

        // Create Account
        Account account = new Account();

        account.Name = 'R Systems';

        insert account;

        // Create Contact
        Contact contact = new Contact();

        contact.FirstName = 'Mike';
        contact.LastName = 'Miller';
        contact.Email = 'mike@gmail.com';
        contact.AccountId = account.Id;

        insert contact;

        // Create CampaignMember
        CampaignMember member = new CampaignMember();

        member.CampaignId = testCampaign.Id;
        member.ContactId = contact.Id;

        insert member;
    }

    ////////////////////////////////////////////////////////////////////////////
	//  @author: Ben Riminton (BrightGen)
	//  @created: 6th September 2019
	//  @description: A method to test the retrieval of campaign members
    //
	//
	//  @changes:	
	////////////////////////////////////////////////////////////////////////////
    @isTest
    private static void GetCampaignMembers_Test()
    {
        List<CampaignMember> campaignMembers = new List<CampaignMember>([SELECT Id FROM CampaignMember]);

        system.assertNotEquals(0, campaignMembers.size());

        Id campaignMemberId = campaignMembers.get(0).Id;
        
        Test.startTest();
        
        Map<Id, CampaignMember> members = bg_CampaignMemberUtils.GetCampaignMembers(new Set<Id>{campaignMemberId});

        Test.stopTest();

        system.assertNotEquals(null, members);
        system.assertEquals(1, members.size());
    }

    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 22nd January 2019
	//  @description: A method to test that campaign members are returned from
    //                the associating campaign Id
	//
	//  @changes:	
	////////////////////////////////////////////////////////////////////////////
    @isTest
    private static void GetCampaignMembersFromCampaignId()
    {
        List<Campaign> campaigns = new List<Campaign>([SELECT Id FROM Campaign]);

        system.assertNotEquals(0, campaigns.size());

        Id campaignId = campaigns.get(0).Id;

        Test.startTest();
        
        Map<Id, CampaignMember> members = bg_CampaignMemberUtils.GetCampaignMembersFromCampaignId(campaignId);

        Test.stopTest();

        system.assertNotEquals(null, members);
        system.assertEquals(1, members.size());
    }
}