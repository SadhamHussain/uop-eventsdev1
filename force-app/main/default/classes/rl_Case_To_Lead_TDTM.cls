/*******************************************************************************************************************************
* @name:            rl_Case_To_Lead_TDTM
* @description:     Class referenced by Trigger Handler 'a0D0Q000001cRIH' to override After Insert on Case
* 
*
* TRIGGERS HANDLED
* @AfterInsert      If Case with Live Chat fields does not have an associated Lead or Contact, this triggers uses Duplicate
*                   Check (via rl_DuplicateCheckApexAPI) to check for existing matches, or creates a new Lead to associate to
*
*
* @Created:   27-04-2020 v0.1 - initial version - Rob Liesicke
**************** Version history ***********************
* @Updated:   28-04-2020 v0.2 - Moved reusable functionality to separate class
* CT, Rajesh : 10/07/2020 - Added String.IsNotBlank(cs.LiveChat_LastName__c) condition instead of LiveChat_LastName__c !=''
*                                            to avoid addition of null values inside the set variable
********************************************************************************************************************************/

global class rl_Case_To_Lead_TDTM extends hed.TDTM_Runnable {
    
    // Standard code required for overiding trigger action and declaring DML wrapper return
    public override DmlWrapper run(List<SObject> newlist, List<SObject> oldlist, 
    Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {
        hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
        
        
        // Check trigger action is after insert
        if (triggerAction == Action.AfterInsert)
        {
            list<String> caseIds = new list<String>();
            
            // Loop through the cases to check which ones require processing
            for (Case cs : (list<Case>)newlist) 
            {
                // Add Case Id to list if it's not associated to a Lead or Contact, and has Live Chat details
                //CT, Rajesh: added String.IsNotBlank(cs.LiveChat_LastName__c) condition
                if (String.IsNotBlank(cs.LiveChat_LastName__c) && cs.ContactId == null && cs.Enquirer__c == null) 
                {
                   caseIds.add(cs.Id);
                }
            }  
            
            // If any cases need processing, call method to find or create parent record
            if(caseIds.size() > 0)
            {
                List<Case> updatedCases = processCases(caseIds);
                
                // Add list of cases now with parent record (lead or contact) to DML Wrapper for bulk update 
                dmlWrapper.objectsToUpdate.addAll((list<SObject>)updatedCases);   
            }       
                    
       }
        
       return dmlWrapper;
    }
    
    
    public List<Case> processCases(List<String> Ids)
    {
            // Declare variables
            String matchingId;
            
            list<Lead> leadsToInsert = new List<Lead>();
            list<Case> casesToProcess = new List<Case>();
            Map<Id,Lead> caseToLeadMap = new Map<Id,Lead>();

            
           // Get list of Case Sobject that require processing
           casesToProcess = [ SELECT Id, OwnerId, LiveChat_FirstName__c, LiveChat_LastName__c, LiveChat_Email__c, 
                                     LiveChat_Mobile__c, LiveChat_Reason__c
                              FROM Case 
                              WHERE Id in :Ids ];
            
           
           // Create a new lead for every case using the Live Chat related fields 
           for (Case cs : casesToProcess) 
           {
               
              // Assign Case Owner according to reason provided
               switch on cs.LiveChat_Reason__c{
                   when 'LiveChat_Reason_1' {
                       cs.OwnerId = Label.LiveChat_OwnerId_1;
                   }
                   when 'LiveChat_Reason_2' {
                       cs.OwnerId = Label.LiveChat_OwnerId_2;
                   }
                   when 'LiveChat_Reason_3' {
                       cs.OwnerId = Label.LiveChat_OwnerId_3;
                   }
                   when 'LiveChat_Reason_4' {
                       cs.OwnerId = Label.LiveChat_OwnerId_4;
                   }
                   when 'LiveChat_Reason_5' {
                       cs.OwnerId = Label.LiveChat_OwnerId_5;
                   }
                   when 'LiveChat_Reason_6' {
                       cs.OwnerId = Label.LiveChat_OwnerId_6;
                   }                   
                   when else {                      
                       cs.OwnerId = Label.LiveChat_OfflineCaseOwnerId;
                   }                   
               }
               
               Lead newLead = new Lead();
               
               newLead.FirstName = cs.LiveChat_FirstName__c;
               newLead.LastName = cs.LiveChat_LastName__c;
               newLead.Email = cs.LiveChat_Email__c;
               newLead.MobilePhone = cs.LiveChat_Mobile__c;
               newLead.Enquiry_Channel__c = 'Live Chat';
               newLead.Company = 'Defaulted';
               
               // Pass new Lead to method that uses Duplicate Check API to check for matching Contacts and Leads. 
               // Returns with blank string or a related record Id
               matchingId = rl_DuplicateCheckApexAPI.checkForExistingMatches(newLead);
               
               
               // If the returned Id is related to a Contact, update case with the Contact
               if(matchingId.left(3) == '003')
               {
                   cs.ContactId = matchingId;
               }

               
               // If the returned Id is related to a Lead, update case with the Lead
               else if(matchingId.left(3) == '00Q')
               {
                   cs.Enquirer__c = matchingId;
               }
               
               
               // If no Contact or Lead was found, add Lead to list for bulk insert and insert the Case Id and Lead Sobject
               // into a map ready for matching later on 
               else
               {
                   leadsToInsert.add(newLead);
                   caseToLeadMap.put(cs.Id,newLead);
               }
               
           }

            // If required bulk insert leads
            try 
            {
                if(leadsToInsert.size() > 0)
                {
                    insert leadsToInsert; 
                }
            } 
            
            catch(DmlException e) 
            {
                System.debug('The following exception has occurred: ' + e.getMessage());
            } 
            
           
           // Associate the remaining cases to the newly inserted Leads 
           if(leadsToInsert.size() > 0)
           {
               for (Case cs : casesToProcess) 
               {
                   if(caseToLeadMap.get(cs.Id) != null) 
                   {
                       Lead insertedLead = caseToLeadMap.get(cs.Id);
                       cs.Enquirer__c = insertedLead.Id;   
                   }
               }
           } 
            
           return casesToProcess;

}        
       
    
}