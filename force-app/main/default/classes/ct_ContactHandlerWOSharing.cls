/**
 * @File Name          : ct_ContactTriggerHelper.cls
 * @Description        : Creating Communication Details record. 
 *                       Communication Detail is new object so using keyword "without sharing"
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 25/6/2020, 3:34:58 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/23/2020   Creation Admin     Initial Version
**/
public without sharing class ct_ContactHandlerWOSharing {
    
    /**
    * @description Create Communication Detail record every time when a Contact is Created or Communication Fields Updated
    * @author Creation Admin | 25/6/2020 
    * @param contactNewList 
    * @param contactOldMap 
    * @return void 
    **/
    public static void createCommunicationDetailForContact(List<Contact> contactNewList, Map<Id, Contact> contactOldMap){

      List<Communication_Detail__c> communicationDetailList = new List<Communication_Detail__c>();

      for(Contact thisContact : contactNewList){ 
        if((contactOldMap == null && assessCommunicationDetailNotNullCriteria(thisContact)) 
        || (contactOldMap != null && assessCommunicationDetailIsChangedCriteria(thisContact, contactOldMap.get(thisContact.Id))
        && assessCommunicationDetailNotNullCriteria(thisContact))){
          Communication_Detail__c thisCommunicationDetail             = new Communication_Detail__c();
          thisCommunicationDetail.Contact__c                          = thisContact.Id;
          thisCommunicationDetail.Email__c                            = thisContact.Email ;
          thisCommunicationDetail.Alternative_Email__c                = thisContact.hed__AlternateEmail__c ;
          thisCommunicationDetail.Event_Email__c                      = thisContact.Event_Email__c ;
          thisCommunicationDetail.University_Email__c                 = thisContact.hed__UniversityEmail__c ;
          thisCommunicationDetail.Work_Email__c                       = thisContact.hed__WorkEmail__c ;
          thisCommunicationDetail.Phone__c                            = thisContact.Phone ;
          thisCommunicationDetail.Mobile__c                           = thisContact.MobilePhone  ;
          thisCommunicationDetail.Event_Mobile__c                     = thisContact.Event_Mobile__c  ;
          thisCommunicationDetail.Day_Telephone_Number__c             = thisContact.Default_Day_Phone__c ;
          thisCommunicationDetail.Evening_Telephone_Number__c         = thisContact.Default_Evening_Phone__c ;
          thisCommunicationDetail.Preferred_Email__c                  = thisContact.hed__Preferred_Email__c ;
          thisCommunicationDetail.Preferred_Phone__c                  = thisContact.hed__PreferredPhone__c;
          thisCommunicationDetail.Emergency_Contact_Name__c           = thisContact.Emergency_Contact_Name__c ;
          thisCommunicationDetail.Emergency_Contact_Phone_Number__c   = thisContact.Emergency_Contact_Phone_Number__c;
          communicationDetailList.add(thisCommunicationDetail);
        }
      }

      try{
        insert communicationDetailList;
      }
      catch(Exception e){
         //Exception mechanism to log any exceptions occured during the dml operation
         ct_Logger.logMessage(e, 'Communication Detail');
         throw e;
      }
    }
    /**
    * @description Method to assess the criteria to Create Communication Detail Record
    * @author Creation Admin | 18/6/2020 
    * @return Boolean 
    **/
    public static Boolean assessCommunicationDetailIsChangedCriteria(Contact thisNewContact, Contact thisOldContact){
      
      if((thisNewContact.Email != null && thisNewContact.Email != thisOldContact.Email) ||
      (thisNewContact.hed__AlternateEmail__c != null && thisNewContact.hed__AlternateEmail__c != thisOldContact.hed__AlternateEmail__c) ||
      (thisNewContact.Event_Email__c != null && thisNewContact.Event_Email__c != thisOldContact.Event_Email__c )||
      (thisNewContact.hed__UniversityEmail__c != null && thisNewContact.hed__UniversityEmail__c != thisOldContact.hed__UniversityEmail__c )||
      (thisNewContact.hed__WorkEmail__c != null && thisNewContact.hed__WorkEmail__c != thisOldContact.hed__WorkEmail__c )||
      (thisNewContact.Phone != null && thisNewContact.Phone != thisOldContact.Phone )||
      (thisNewContact.MobilePhone != null && thisNewContact.MobilePhone != thisOldContact.MobilePhone  )||
      (thisNewContact.Event_Mobile__c != null && thisNewContact.Event_Mobile__c != thisOldContact.Event_Mobile__c  )||
      (thisNewContact.Default_Day_Phone__c != null && thisNewContact.Default_Day_Phone__c !=  thisOldContact.Default_Day_Phone__c )||
      (thisNewContact.Default_Evening_Phone__c != null && thisNewContact.Default_Evening_Phone__c !=  thisOldContact.Default_Evening_Phone__c )||
      (thisNewContact.hed__Preferred_Email__c != null && thisNewContact.hed__Preferred_Email__c != thisOldContact.hed__Preferred_Email__c )||
      (thisNewContact.hed__PreferredPhone__c != null && thisNewContact.hed__PreferredPhone__c != thisOldContact.hed__PreferredPhone__c )||
      (thisNewContact.Emergency_Contact_Name__c != null && thisNewContact.Emergency_Contact_Name__c != thisOldContact.Emergency_Contact_Name__c )||
      (thisNewContact.Emergency_Contact_Phone_Number__c != null && thisNewContact.Emergency_Contact_Phone_Number__c != thisOldContact.Emergency_Contact_Phone_Number__c)){
        return true;
      }

      return false;
    }

    /**
    * @description Method to assess the criteria to Create Communication Detail Record
    * @author Creation Admin | 18/6/2020 
    * @return Boolean 
    **/
    public static Boolean assessCommunicationDetailNotNullCriteria(Contact thisNewContact){
      
      if(thisNewContact.Email != null || thisNewContact.hed__AlternateEmail__c != null ||
        thisNewContact.Event_Email__c != null || thisNewContact.hed__UniversityEmail__c != null ||
        thisNewContact.hed__WorkEmail__c != null || thisNewContact.Phone != null ||
        thisNewContact.MobilePhone != null || thisNewContact.Event_Mobile__c != null ||
        thisNewContact.Default_Day_Phone__c != null || thisNewContact.Default_Evening_Phone__c != null ||
        thisNewContact.hed__Preferred_Email__c != null || thisNewContact.hed__PreferredPhone__c != null ||
        thisNewContact.Emergency_Contact_Name__c != null || thisNewContact.Emergency_Contact_Phone_Number__c != null ){
        return true;
      }
      return false;
    }


}