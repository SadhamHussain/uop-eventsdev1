/**
 * @File Name          : ct_ProcessAuthForDDControllerTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 17/4/2020, 8:02:31 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/14/2020   Creation Admin     Initial Version
**/
@isTest public class ct_ProcessAuthForDDControllerTest {

    /**
  * @description test authorization process for direct debit
  * @return void 
  **/
    @isTest(SeeAllData=true)
    public static void processAuthorisationForDDTest(){

    // Getting the web donation payment details data in advancement settings 
    ct_Advancement_Setting__mdt thisPaymentMetadata = new ct_Advancement_Setting__mdt();
    thisPaymentMetadata.MasterLabel = 'Web Donation Payment Details';
    thisPaymentMetadata.Recurring_Payment_Method__c = 'Direct Debit';
    thisPaymentMetadata.One_Off_Payment_Method__c = 'Credit / Debit Card';
    thisPaymentMetadata.DeveloperName = ct_Constants.WEB_DONATION_PAYMENT_DETAILS;
    
    ct_MetadataService.advancementSettingMetaData.put(ct_Constants.WEB_DONATION_PAYMENT_DETAILS, thisPaymentMetadata);
    // Create Contact    
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    // Create Authorisation 
    asp04__Authorisation__c thisAuthorisation = ct_TestDataFactory.createAuthorisation(thisContact);
    insert thisAuthorisation;
    // Create RecurringDonation 
    npe03__Recurring_Donation__c thisRecurringDonation =  ct_TestDataFactory.createRecurringDonation(thisContact, thisAuthorisation.Id, 100.00);
    insert thisRecurringDonation;

    // Calling the getAuthrisationStatus method in ct_ProcessAuthorisationForDDController class
    ct_ProcessAuthorisationForDDController.getAuthrisationStatus(thisRecurringDonation.Id);
    // Calling the getProcessAuthorisationURLasString method in ct_ProcessAuthorisationForDDController class
    ct_ProcessAuthorisationForDDController.getProcessAuthorisationURLasString(thisRecurringDonation.Id);
  
    }
}