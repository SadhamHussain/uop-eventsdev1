/**
 * @File Name          : ct_HED_AddressTriggerHelper.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 25/6/2020, 4:20:45 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/6/2020   Creation Admin     Initial Version
**/
public class ct_HED_AddressTriggerHelper {
  
  /**
  * @description Set Address Record As Private If Related Contact=>Account Is Set As Private
  * @author Creation Admin | 24/6/2020 
  * @param addressNewList 
  * @return void 
  **/
  public static void setAddressAsPrivate(List<hed__Address__c> addressNewList){
    Set<Id> contactIds = new Set<Id>();
    Map<Id, Boolean> mapContactToAccountVisibility = new Map<Id, Boolean>();

    for(hed__Address__c thisAddress : addressNewList){
      if(thisAddress.hed__Parent_Contact__c != null){
        contactIds.add(thisAddress.hed__Parent_Contact__c);
      }
    }
    
    for(Contact thisContact : [SELECT Id, Account.AAA_Private_Record__c 
                              FROM Contact
                              WHERE Id IN: contactIds
                              AND AccountId != null]){
      mapContactToAccountVisibility.put(thisContact.Id, thisContact.Account.AAA_Private_Record__c);
    }
    
    for(hed__Address__c thisAddress : addressNewList){
      if(mapContactToAccountVisibility.containsKey(thisAddress.hed__Parent_Contact__c)){
        thisAddress.AAA_Private_Record__c = mapContactToAccountVisibility.get(thisAddress.hed__Parent_Contact__c);
      }
    }
  }
}