/**
 * @File Name          : ct_AddressVerificationController.cls
 * @Description        : Test Class for ct_AddressVerificationController class
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 6/1/2020, 6:25:17 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/12/2020   Creation Admin     Initial Version
**/
@isTest
global class ct_AddressVerificationControllerTest {
   
    global class addressHttpCalloutMock implements HttpCalloutMock {
        // Implement this interface method
        global HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"Result":"1","ErrorText":"","Reason":"","Other":"","Status":"success","Item": [{"@value": "1","Postcode": "W4 4PH","PostcodeFrom": "","List": "W4 4PH Parker Williams Design Ltd, Voysey House, Barley Mow Passage, LONDON","Key": "25819610","CountryISO": "GBR","Address1": "Parker Williams Design Ltd","Address2": "Voysey House","Address3": "Barley Mow Passage","Address4": "LONDON", "Address5": "W4 4PH", "Address6": "", "Address7": "","Department": "", "OrganisationName": "Parker Williams Design Ltd","SubBuilding": "", "Building": "Voysey House","Number": "", "DependentThoroughfare": "","Thoroughfare": "Barley Mow Passage","Locality": "","Town": "London","Principality": "","Cedex": "","Region": "","Product": "AFD Postcode Plus","Country": "United Kingdom"}]}');
            response.setStatusCode(200);
            return response; 
        }
    }
    /**
    * @description Mock callout for test callout
    * @return void 
    **/
    @isTest static  void testGetCallout() {
        Test.setMock(HttpCalloutMock.class, new addressHttpCalloutMock());
        Test.startTest(); 

        List<ct_AddressVerificationController.AddressWrapper> thisAddressWrapperList = new List<ct_AddressVerificationController.AddressWrapper>();
        thisAddressWrapperList = ct_AddressVerificationController.addressVerificationCallout('test');
        Test.stopTest();          
    }
    /**
    * @description- Update Contact Address detais Test
    * @return void 
    **/
    @isTest static  void testContactUpdateAddressDetails() {
        Test.startTest(); 
        Contact thisContact = ct_TestDataFactory.createContact();
        insert thisContact;
        String addressDetails = '{"Street":"10 Barley Mow Passage","City":"London","PostalCode":"W4 4PH","Country":"United Kingdom","hed__Default_Address__c":"true"}';
        ct_AddressVerificationController.saveAddressDetails(addressDetails, String.valueOf(thisContact.Id), 'hed__Parent_Contact__c');
        Test.stopTest(); 
        Contact thisResultContact = [SELECT Id, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry
                                            FROM Contact
                                            WHERE Id=: thisContact.Id];
        System.debug('thisResultContact'+ thisResultContact);
        System.assertEquals('10 Barley Mow Passage', thisResultContact.MailingStreet, 'Should return 10 Barley Mow Passage');
        System.assertEquals('London', thisResultContact.MailingCity, 'Should return London');                                 
        System.assertEquals('W4 4PH', thisResultContact.MailingPostalCode, 'Should return W4 4PH');
        System.assertEquals('United Kingdom', thisResultContact.MailingCountry, 'Should return United Kingdom');                                 
    }
    /**
    * @description- Update Account  Address detais Test
    * @return void 
    **/
    @isTest static  void testAccountUpdateAddressDetails() {
        Test.startTest(); 
        Account thisAccount = ct_TestDataFactory.createEducationalAccount('Creation Technology');
        insert thisAccount;
        String addressDetails = '{"Street":"10 Barley Mow Passage","City":"London","PostalCode":"W4 4PH","Country":"United Kingdom","hed__Default_Address__c":"true"}';
        ct_AddressVerificationController.saveAddressDetails(addressDetails, String.valueOf(thisAccount.Id),'hed__Parent_Contact__c');
        Test.stopTest(); 
        Account thisResultAccount = [SELECT Id, BillingStreet, BillingCity, BillingPostalCode, BillingState, BillingCountry
                                            FROM Account
                                            WHERE Id=: thisAccount.Id];
        System.debug('thisResultAccount'+ thisResultAccount);
        System.assertEquals('10 Barley Mow Passage', thisResultAccount.BillingStreet, 'Should return 10 Barley Mow Passage');
        System.assertEquals('London', thisResultAccount.BillingCity, 'Should return London');                                 
        System.assertEquals('W4 4PH', thisResultAccount.BillingPostalCode, 'Should return W4 4PH');
        System.assertEquals('United Kingdom', thisResultAccount.BillingCountry, 'Should return United Kingdom');                                 
    }
    /**
    * @description- Update Contact Address detais Test
    * @return void 
    **/
    @isTest static  void testCreateAddressRecord() {
        Test.startTest(); 
        Contact thisContact = ct_TestDataFactory.createContact();
        insert thisContact;
        String addressDetails = '{"Street":"10 Barley Mow Passage","City":"London","PostalCode":"W4 4PH","Country":"United Kingdom","hed__Default_Address__c":"false","hed__Address_Type__c":"Home"}';
        ct_AddressVerificationController.saveAddressDetails(addressDetails, String.valueOf(thisContact.Id), 'hed__Parent_Contact__c');
        Test.stopTest(); 
        // Contact thisResultContact = [SELECT Id, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry
        //                                     FROM Contact
        //                                     WHERE Id=: thisContact.Id];
        // System.debug('thisResultContact'+ thisResultContact);
        // System.assertEquals('10 Barley Mow Passage', thisResultContact.MailingStreet, 'Should return 10 Barley Mow Passage');
        // System.assertEquals('London', thisResultContact.MailingCity, 'Should return London');                                 
        // System.assertEquals('W4 4PH', thisResultContact.MailingPostalCode, 'Should return W4 4PH');
        // System.assertEquals('United Kingdom', thisResultContact.MailingCountry, 'Should return United Kingdom');                                 
    }
}