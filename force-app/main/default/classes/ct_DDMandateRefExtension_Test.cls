@isTest public class ct_DDMandateRefExtension_Test {
   @IsTest(SeeAllData=true)
   static void getAndSaveDocument(){

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    //Create Authorisation  
    asp04__Authorisation__c thisAuthorisation = ct_TestDataFactory.createAuthorisation(thisContact);
    insert thisAuthorisation;
    //Create Recurring Donation
    npe03__Recurring_Donation__c thisRecurringDonation =  ct_TestDataFactory.createRecurringDonation(thisContact, thisAuthorisation.Id, 100.00);
    insert thisRecurringDonation;
    
    Test.startTest();
    
    PageReference pageRef = Page.ct_DDMandateDownloadPage;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('id', thisRecurringDonation.id);
    pageRef.getParameters().put('attachment', 'true');
    ApexPages.StandardController stc = new ApexPages.StandardController(thisRecurringDonation );

    ct_DDMandateRefExtension ddController = new ct_DDMandateRefExtension(stc);
    ddController.addPDFAttachment();
    Test.stopTest();
  }
}