/**
 * @File Name          : ct_DirectDebitFormController_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 7/2/2020, 12:01:33 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/2/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_DirectDebitFormController_Test {
  /**
  * @description test picklist entry mapping using metadata
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @isTest
  public static void picklistValuesMethodTest(){
    Webform__mdt directDebit = new Webform__mdt();
    directDebit.MasterLabel = 'Direct Debit';
    directDebit.DeveloperName = 'Direct_Debit';

    List<Webform_Data__mdt> metaDataList = new List<Webform_Data__mdt>();
    metaDataList.add(new Webform_Data__mdt(MasterLabel = 'DD Name', Form_Field_Name__c = 'DD_Name', SObject__c = 'npe03__Recurring_Donation__c',
    SObject_Field__c  = 'Name', Webform__c = directDebit.Id, Web_Label__c='Name', Web_Placeholder__c ='Enter  your Name'));
    metaDataList.add(new Webform_Data__mdt(MasterLabel = 'DD Open Ended', Form_Field_Name__c = 'DD_Open_Ended', SObject__c = 'npe03__Recurring_Donation__c',
    SObject_Field__c  = 'npe03__Open_Ended_Status__c', Webform__c = directDebit.Id, Web_Label__c='Open Ended Status', Web_Placeholder__c ='Enter  your Name'));
    Test.startTest();
    ct_DirectDebitFormController.MetadataWrapper thisWrapper = ct_DirectDebitFormController.fetchMetadataRecords();
    Test.stopTest();
  }
  
  /**
  * @description test Asperato payment creation using web form data
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @IsTest(SeeAllData=true) 
  public static void getContactTest(){
    Account thisAccount = ct_TestDataFactory.createEducationalAccount('Engineering');
    insert thisAccount;
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact; 
    Test.startTest();
      Contact newContact= ct_DirectDebitFormController.getContact(thisContact.Id);
      System.assertEquals(thisContact.LastName, newContact.LastName, 'Should return same name');
      Account newAccount= ct_DirectDebitFormController.getAccount(thisAccount.Id);
      System.assertEquals(thisAccount.Name, newAccount.Name, 'Should return same name');

      String directDebit = '{'+
      '"npe03__Schedule_Type__c": null,'+
      '"npe03__Organization__c": null,'+
      '"Tribute_Type__c": "Honor",'+
      '"Notification_Recipient_Contact__c": null,'+
      '"Honoree_Name__c": null,'+
      '"Gift_Aid__c": true,'+
      '"Primary_Campaign_Source__c": null,'+
      '"Private__c": true,'+
      '"Name": "Shankar Direct Debit 03/04/2020",'+
      '"Notification_Preference__c": "Email",'+
      '"Description__c": "Test",'+
      '"Notification_Recipient_Name__c": null,'+
      '"npe03__Open_Ended_Status__c": "None",'+
      '"Notification_Recipient_Information__c": null,'+
      '"General_Accounting_Unit__c": null,'+
      '"npe03__Contact__c": "'+thisContact.Id+'",'+
      '"Lead_Source__c": "Application",'+
      '"Honoree_Contact__c": null,'+
      '"Notification_Message__c": null,'+
      '"npe03__Amount__c": "1200",'+
      '"npe03__Date_Established__c": "2020-04-03",'+
      '"npsp__PaymentMethod__c": null,'+
      '"npe03__Installments__c": "2",'+
      '"npe03__Installment_Period__c": "Monthly",'+
      '"CollectionDate": "15th of the Month"'+
      '}';
      asp04__Authorisation__c thisAuthorisation = ct_DirectDebitFormController.createAsepratoAuthorization(directDebit, null);
      System.debug('thisAuthorisation###'+ thisAuthorisation);
      System.assertEquals('Direct Debit', thisAuthorisation.asp04__Payment_Route_Options__c, 'Should return Direct Debit');
      System.assertEquals(thisContact.FirstName, thisAuthorisation.asp04__First_Name__c, 'Should return contact first name');
      System.assertEquals(thisContact.LastName, thisAuthorisation.asp04__Last_Name__c, 'Should return contact last name');
    Test.stopTest();
  }
  
  /**
  * @description test Autorization creation for Account
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @IsTest(SeeAllData=true)
  public static void createAuthorisationforAccountTest(){
    Account thisAccount = ct_TestDataFactory.createEducationalAccount('Engineering');
    insert thisAccount;
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact; 
    Test.startTest();
      String directDebit = '{'+
      '"npe03__Schedule_Type__c": null,'+
      '"npe03__Organization__c": "'+thisAccount.Id+'",'+
      '"Tribute_Type__c": "Honor",'+
      '"Notification_Recipient_Contact__c": null,'+
      '"Honoree_Name__c": null,'+
      '"Gift_Aid__c": true,'+
      '"Primary_Campaign_Source__c": null,'+
      '"Private__c": true,'+
      '"Name": "Shankar Direct Debit 03/04/2020",'+
      '"Notification_Preference__c": "Email",'+
      '"Description__c": "Test",'+
      '"Notification_Recipient_Name__c": null,'+
      '"npe03__Open_Ended_Status__c": "None",'+
      '"Notification_Recipient_Information__c": null,'+
      '"General_Accounting_Unit__c": null,'+
      '"npe03__Contact__c": null,'+
      '"Lead_Source__c": "Application",'+
      '"Honoree_Contact__c": null,'+
      '"Notification_Message__c": null,'+
      '"npe03__Amount__c": "1200",'+
      '"npe03__Date_Established__c": "2020-04-03",'+
      '"npsp__PaymentMethod__c": null,'+
      '"npe03__Installments__c": "2",'+
      '"npe03__Installment_Period__c": "Monthly",'+
      '"CollectionDate": "15th of the Month"'+
      '}';
      asp04__Authorisation__c thisAuthorisation = ct_DirectDebitFormController.createAsepratoAuthorization(directDebit, null);
      System.assertEquals('Direct Debit', thisAuthorisation.asp04__Payment_Route_Options__c, 'Should return Direct Debit');
      System.assertEquals(thisAccount.Name, thisAuthorisation.asp04__First_Name__c, 'Should return Account name');
      String thisId = ct_DirectDebitFormController.createRecurringDonation(directDebit, thisAuthorisation);
      npe03__Recurring_Donation__c thisDonation = [SELECT Id, Asperato_Authorisation__c FROM npe03__Recurring_Donation__c 
                                                                      WHERE Asperato_Authorisation__c=: thisAuthorisation.Id];
      List<Opportunity> opportunityList = [SELECT Id FROM Opportunity WHERE npe03__Recurring_Donation__c =: thisDonation.Id];
      System.assertEquals(2, opportunityList.size(), 'Should return 2 because number of installment is 2');
    Test.stopTest();
  }
}