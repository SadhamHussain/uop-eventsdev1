/**
 * @File Name          : ct_OpportunityTriggerHelper.cls
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 07-07-2020
 * Ver       Date            Author                 Modification
 * 1.0    3/9/2020   Creation Admin     Initial Version
**/
public class ct_OpportunityTriggerHelper {
  public Id opportunityId {get;set;}

  /**
  * @createAsperatoPaymentForDirectDebitPayments - method to create Asperato payment records for DD when opportunity is set to Active
  * @param Set<Id> activeOpportunityIds - active Opportunity Ids
  * @return void 
  **/
  public static void createAsperatoPaymentForDirectDebitPayments(Set<Id> activeOpportunityIds){
    Map<Id, asp04__Payment__c> asperatoPaymentMapToInsert = new Map<Id, asp04__Payment__c>();
    List<npe01__OppPayment__c> gemPaymentListToUpdate = new List<npe01__OppPayment__c>();
    for(npe01__OppPayment__c thisGEMPayment : [SELECT Id, npe01__Opportunity__c, npe01__Opportunity__r.Id, npe01__Opportunity__r.Name, npe01__Payment_Method__c,npe01__Opportunity__r.AccountId,
                                                            npe01__Opportunity__r.npsp__Primary_Contact__c, npe01__Opportunity__r.CloseDate, npe01__Payment_Amount__c,
                                                            npe01__Opportunity__r.Amount, npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__r.asp04__First_Name__c,
                                                            npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__r.asp04__Last_Name__c, npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__r.asp04__Email__c,
                                                            npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__r.asp04__Billing_Address_Street__c, npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__r.asp04__Billing_Address_City__c,
                                                            npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__r.asp04__Billing_Address_Country__c, npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__r.asp04__Billing_Address_State__c, 
                                                            npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__r.asp04__Billing_Address_PostalCode__c, npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__c,
                                                            npe01__Opportunity__r.npsp__Primary_Contact__r.FirstName, npe01__Opportunity__r.npsp__Primary_Contact__r.LastName,
                                                            npe01__Opportunity__r.npsp__Primary_Contact__r.Email, npe01__Opportunity__r.npsp__Primary_Contact__r.MailingStreet,
                                                            npe01__Opportunity__r.npsp__Primary_Contact__r.MailingCity, npe01__Opportunity__r.npsp__Primary_Contact__r.MailingCountry,
                                                            npe01__Opportunity__r.npsp__Primary_Contact__r.MailingState, npe01__Opportunity__r.npsp__Primary_Contact__r.MailingPostalCode
                                                            FROM npe01__OppPayment__c
                                                            WHERE npe01__Opportunity__c IN: activeOpportunityIds
                                                            AND  npe01__Opportunity__r.npsp__Primary_Contact__c != null
                                                            AND npe01__Payment_Method__c =: ct_Constants.DIRECT_DEBIT_PAYMENT_METHOD]){

      asperatoPaymentMapToInsert.put(thisGEMPayment.Id, ct_WebDataFormHelper.createAsperatoPayment(JSON.serialize(ct_WebDataFormHelper.constructContactRecord(thisGEMPayment)), thisGEMPayment.npe01__Payment_Method__c, Integer.valueOf(thisGEMPayment.npe01__Payment_Amount__c), thisGEMPayment.npe01__Opportunity__r.npe03__Recurring_Donation__r.Asperato_Authorisation__c, thisGEMPayment.npe01__Opportunity__r.CloseDate));
    }
    try{
      insert asperatoPaymentMapToInsert.values();
    }
    catch(Exception e){
      ct_Logger.logMessage(e, 'Asperato Payment');
      throw e;
    }

    for(Id thisGEMPaymentId : asperatoPaymentMapToInsert.keySet()){
      npe01__OppPayment__c thisGEMPayment = new npe01__OppPayment__c();
      thisGEMPayment.Id = thisGEMPaymentId;
      thisGEMPayment.Asperato_Payment__c = asperatoPaymentMapToInsert.get(thisGEMPaymentId).Id;
      gemPaymentListToUpdate.add(thisGEMPayment);
    }
    try{
      update gemPaymentListToUpdate;
    }
    catch(Exception e){
      ct_Logger.logMessage(e, 'GEM Payment');
      throw e;
    }
  }
    
  
  /**
  * @getRelatedRecurringAttachment- Method to Query Recurring donation attachment
  * @param Set<Id> recurringDonationIds 
  * @param Set<Id> opportunityIds 
  * @return void 
  **/
  public static void getRelatedRecurringAttachment(Set<Id> recurringDonationIds, Set<Id> opportunityIds){
    Map<Id, Set<Id>> recurringDonationIdMap = new Map<Id, Set<Id>>();
    Map<Id, ContentDocumentLink> contentDocumentLinkMap      = new Map<Id, ContentDocumentLink>();
    for(ContentDocumentLink thisLink : [SELECT Id, ContentDocumentId, LinkedEntityId, ShareType,
                                                        Visibility
                                                        FROM ContentDocumentLink
                                                        WHERE LinkedEntityId IN: recurringDonationIds]){
      contentDocumentLinkMap.put(thisLink.ContentDocumentId, thisLink);
      if(!recurringDonationIdMap.containsKey(thisLink.LinkedEntityId)){
        recurringDonationIdMap.put(thisLink.LinkedEntityId, new Set<Id>{});
      }
      recurringDonationIdMap.get(thisLink.LinkedEntityId).add(thisLink.ContentDocumentId);

    }
    if(!contentDocumentLinkMap.isEmpty()){
      ct_ContentDocumentLinkHelper.addAttachmentsToChildOpportunities(recurringDonationIdMap, contentDocumentLinkMap, opportunityIds);
    }
  }
  /**
  * @getAccountMap - Method to get Account record 
  * @param Set<Id> accountIds 
  * @return Map<Id, Account> 
  **/
  public static Map<Id, Account> getAccountMap(Set<Id> accountIds){
    return  new Map<Id, Account>([SELECT Id, Name, RecordTypeId
                                        FROM Account 
                                        WHERE Id IN: accountIds]);
  } 

  /**
  * @getAccountMap - Method to get paused Recurring donation records
  * @param Set<Id> recurringDonationId 
  * @return Map<Id, npe03__Recurring_Donation__c>  
  **/
  public static Map<Id, npe03__Recurring_Donation__c> getPausedRecurringDonations(Set<Id> recurringDonationId){
    return  new Map<Id, npe03__Recurring_Donation__c>([SELECT Id, Paused__c
                                                        FROM npe03__Recurring_Donation__c 
                                                        WHERE Id IN: recurringDonationId
                                                        AND Paused__c = true]);
  }

  /**
  * @description Set Related Administrative Account To Private If Opportunity Recordtype Is Otherthan Application
  * @author Creation Admin | 24/6/2020 
  * @param opportunityNewList 
  * @return void 
  **/
  public static void updateRelatedAccountToPrivate(Set<Id> opportunityIds){
    List<String> opportunityRecordTypeToExclude;
    String OPPORTUNITY_RECORD_TYPE_AAA_PRIVATE_EXCLUSION = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.OPPORTUNITY_RECORD_TYPE_AAA_PRIVATE_EXCLUSION_SETTING).Value__c;
    String dynamicQueryString = 'SELECT Id, Account.AAA_Private_Record__c,RecordType.DeveloperName, npsp__Primary_Contact__c,npsp__Primary_Contact__r.AccountId, AccountId FROM Opportunity';
    

    if(OPPORTUNITY_RECORD_TYPE_AAA_PRIVATE_EXCLUSION != null){
      opportunityRecordTypeToExclude = OPPORTUNITY_RECORD_TYPE_AAA_PRIVATE_EXCLUSION.split(',');
      dynamicQueryString = dynamicQueryString+ ' WHERE RecordType.DeveloperName NOT IN :opportunityRecordTypeToExclude AND Id IN: opportunityIds AND Account.AAA_Private_Record__c = false';
    }else {
      dynamicQueryString = dynamicQueryString+ ' WHERE Id IN: opportunityIds Account.AAA_Private_Record__c = false';
    }

    List<Opportunity> opportunityListToProcess = (List<Opportunity>)Database.query(dynamicQueryString);
    
    Set<Id> accountIds = new Set<Id>();
    List<Account> accountListToUpdate = new List<Account>();
    //Process Accounts To Update
    for(Opportunity thisOpportunity : opportunityListToProcess){
      if(thisOpportunity.npsp__Primary_Contact__c != null){
        accountIds.add(thisOpportunity.npsp__Primary_Contact__r.AccountId);
      }
      else if(thisOpportunity.AccountId != null){
        accountIds.add(thisOpportunity.AccountId);
      }
    }

    for(Account thisAccount : [SELECT Id, AAA_Private_Record__c FROM Account WHERE Id IN:accountIds AND Account.RecordTypeId = :ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE]){
      thisAccount.AAA_Private_Record__c = true;
      accountListToUpdate.add(thisAccount);
    }

    try{
        System.debug('AAA');
      update accountListToUpdate;
    }
    catch(Exception e){
      //Exception mechanism to log any exceptions occured during the dml operation
      ct_Logger.logMessage(e, 'Account');
      throw e;
    }
  }

  /**
  * @updateOpportunityName - method to update the opportunity name 
  * @param Opportunity thisOpportunity 
  * @param Map<Id Account> accountMap 
  **/
  public static void updateOpportunityName(Opportunity thisOpportunity, Map<Id, Account> accountMap){
    DateTime closeDateTime =  dateTime.newInstance(thisOpportunity.CloseDate.year(),thisOpportunity.CloseDate.month(), thisOpportunity.CloseDate.day());
    if(thisOpportunity.npe03__Recurring_Donation__c != null
    && accountMap.containsKey(thisOpportunity.AccountId)){
      thisOpportunity.Name = accountMap.get(thisOpportunity.AccountId).Name+' '+'Recurring Donation'+' '+closeDateTime.format('dd/MM/YYYY');
      thisOpportunity.Type  = ct_Constants.OPPORTUNITY_RECURRING_DONATION_TYPE;
    }
    else if(!String.isEmpty(thisOpportunity.Naming_Convention__c)
    && accountMap.containsKey(thisOpportunity.AccountId)){
      thisOpportunity.Name = accountMap.get(thisOpportunity.AccountId).Name+' '+thisOpportunity.Naming_Convention__c+' '+closeDateTime.format('dd/MM/YYYY');
    }
    else if(accountMap.containsKey(thisOpportunity.AccountId)
           && thisOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_VOLUNTEER){
      thisOpportunity.Name = accountMap.get(thisOpportunity.AccountId).Name+' '+'Volunteer Recruitment'+' '+closeDateTime.format('dd/MM/YYYY');
    }
  }

  /**
  * @description - Method for populating Donation Size based on Opportunity Record Type Developer Name
  * @param thisOpportunity - Opportunity Record for which Donation Size needs to be populated
  **/
  public static void populateDonationSize(Opportunity thisOpportunity){
    //Get the Donation size configuration based on record type and process donation size as per the configuration
    Opportunity_Donation_Size_Configuration__mdt thisDonationSizeConfiguration = ct_MetadataService.getDonationSizeConfigurationByRecordTypeDeveloperName(ct_Util.getRecordTypeDeveloperNameById(thisOpportunity.RecordTypeId));
    thisOpportunity.Donation_Size__c = null;
    if(thisDonationSizeConfiguration != null
      && thisOpportunity.Amount != null
      && thisOpportunity.Amount > 0){
      if(thisDonationSizeConfiguration.Large_Donation_Value__c != null
        && thisOpportunity.Amount >= thisDonationSizeConfiguration.Large_Donation_Value__c){
          thisOpportunity.Donation_Size__c = thisDonationSizeConfiguration.Large_Donation_Size_Value__c;
        }
      else if(thisDonationSizeConfiguration.Medium_Donation_Value__c != null
              && thisOpportunity.Amount >= thisDonationSizeConfiguration.Medium_Donation_Value__c){
                thisOpportunity.Donation_Size__c = thisDonationSizeConfiguration.Medium_Donation_Size_Value__c;
      }
      else{
        thisOpportunity.Donation_Size__c = thisDonationSizeConfiguration.Small_Donation_Size_Value__c;
      }
    }
  }
  
  /**
  * @description Create Account Team Member for UOP user based on Departmental Setting Metadata
  * @param opportunityNewMap : Opportunity new Map records whose account need to be added an account team
  * @param accountIds : Set of Account Id
  * @return AccountTeamMember : New Account team Member record 
  **/
  public static  List<AccountTeamMember> createAccountTeamMember(Map<Id, Opportunity> opportunityNewMap, Set<Id> accountIds, Set<Id> opportunityWithAccountIds){
    Map<Id, Account> accountMap = new Map<Id, Account>();
    //Get existing account team memebers for the account
    Map<Id, Set<Id>> existingAccountMemberIds = getExistingAccountTeamMemberMap(accountIds);
    //List of account team members to be inserted
    List<AccountTeamMember> accountTeamMemberListToInsert = new List<AccountTeamMember>();
    //Department user configured for particular opportunity record type
    Department_User__mdt thisUserSetting;
    Opportunity thisOpportunity;
    accountMap = getAccountMap(accountIds);

    for(Id thisOpportunityId : opportunityWithAccountIds){
      thisOpportunity = opportunityNewMap.get(thisOpportunityId);
      //Get Department user configuration for particular opportunity record type
      thisUserSetting = ct_MetadataService.getDepartmentalUsersSettingsByRecordTypeDeveloperName(ct_Util.getRecordTypeDeveloperNameById(thisOpportunity.RecordTypeId));
      if(thisUserSetting != null
        && accountMap.get(thisOpportunity.AccountId).RecordTypeId == ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE 
        && !String.isEmpty(thisUserSetting.Departmental_User_Id__c)
        && ((existingAccountMemberIds.isEmpty() || !existingAccountMemberIds.containsKey(thisOpportunity.AccountId))  
          || (existingAccountMemberIds.containsKey(thisOpportunity.AccountId) && !existingAccountMemberIds.get(thisOpportunity.AccountId).contains(Id.valueOf(thisUserSetting.Departmental_User_Id__c))))){
          //Create account team member for the account if configuration is present for the record type
          // and no same existing member present for the account
          AccountTeamMember thisUOPTeamMember         = new AccountTeamMember();
          thisUOPTeamMember.AccountId                 = thisOpportunity.AccountId;
          //Account team member User as per configuration
          thisUOPTeamMember.UserId                    = Id.valueOf(thisUserSetting.Departmental_User_Id__c);
          thisUOPTeamMember.TeamMemberRole            = ct_Constants.ACCOUNT_TEAM_MEMBER_ROLE_CONTACT_MANAGER;
          //Provide read and edit access to the Account and its case and opportunity
          thisUOPTeamMember.AccountAccessLevel        = ct_Constants.ACCOUNT_TEAM_MEMBER_READ_EDIT_ACCESS;
          thisUOPTeamMember.CaseAccessLevel           = ct_Constants.ACCOUNT_TEAM_MEMBER_READ_EDIT_ACCESS;
          thisUOPTeamMember.OpportunityAccessLevel    = ct_Constants.ACCOUNT_TEAM_MEMBER_READ_EDIT_ACCESS;
          if(!existingAccountMemberIds.containsKey(thisOpportunity.AccountId)){
            existingAccountMemberIds.put(thisOpportunity.AccountId, new Set<Id>());
          }
          existingAccountMemberIds.get(thisOpportunity.AccountId).add(Id.valueOf(thisUserSetting.Departmental_User_Id__c));
          accountTeamMemberListToInsert.add(thisUOPTeamMember);
      }
    }

    return accountTeamMemberListToInsert;
  }

  /**
  * @description Method to get existing Account team member for the account ids
  * @param accountIds : Account Ids whose Account team to be retrieved
  * @param existingAccountMemberIds : Existing team member user id per account id
  **/
  private static Map<Id, Set<Id>> getExistingAccountTeamMemberMap(Set<Id> accountIds){
    Map<Id, Set<Id>> existingAccountMemberIds = new Map<Id, Set<Id>>();

    for(AccountTeamMember thisAccountMember :[SELECT Id, UserId, AccountId
                                                FROM AccountTeamMember 
                                                WHERE AccountId IN :accountIds]){

          if(!existingAccountMemberIds.containsKey(thisAccountMember.AccountId)){
            existingAccountMemberIds.put(thisAccountMember.AccountId, new Set<Id>{});
          }
          existingAccountMemberIds.get(thisAccountMember.AccountId).add(thisAccountMember.UserId);
    }
    return existingAccountMemberIds;
  } 

   /**
  * @description Method to reevaluate Account Team Member List once opportunity is Deleted
  * @param deletedOpportunityMap : Deleted Opportunity records whose account team member needs to be reevaluated
  * @param accountIds : Set of Account Id
  **/
  public static List<AccountTeamMember> reevaluateAccountTeamMemberList(Map<Id, Opportunity> deletedOpportunityMap, Set<Id> accountIds){
    //Account team member to be deleted
    List<AccountTeamMember> accountTeamMemberToDelete    = new List<AccountTeamMember>();
    Map<String, AccountTeamMember> accountTeamMemberMap  = new Map<String, AccountTeamMember>();
    Map<String, Integer> accountTeamMemberAggregateMap   = new Map<String, Integer>();
    String accountToOppRecTypeKey;
    String accountToUserIdKey;
    Department_User__mdt thisUserSetting;
    // Account team member map with key as AccountId_UserId 
    populateAccountTeamMemberMap(accountTeamMemberMap, accountIds);
    //Account Aggregate result to get count of its opportunity record based on record type
    //Key is of structure AccountId_oppRectypeId
    populateAccountTeamAggregateMap(accountTeamMemberAggregateMap, accountIds, deletedOpportunityMap.keySet());
    
    for(Opportunity thisDeletedOpportunity : deletedOpportunityMap.values()){
      //Get Department user configuration for particular opportunity record type
      thisUserSetting        = ct_MetadataService.getDepartmentalUsersSettingsByRecordTypeDeveloperName(ct_Util.getRecordTypeDeveloperNameById(thisDeletedOpportunity.RecordTypeId));

      if(!String.isEmpty(thisUserSetting.Departmental_User_Id__c)){
        accountToOppRecTypeKey = String.valueOf(thisDeletedOpportunity.AccountId)+'_'+String.valueOf(thisDeletedOpportunity.RecordTypeId);
        accountToUserIdKey     = String.valueOf(thisDeletedOpportunity.AccountId)+'_'+String.valueOf(Id.valueOf(thisUserSetting.Departmental_User_Id__c));
        if((accountTeamMemberAggregateMap.isEmpty() || !accountTeamMemberAggregateMap.containsKey(accountToOppRecTypeKey)) 
          && accountTeamMemberMap.containsKey(accountToUserIdKey)){
            //If there is only one opportunity with same record type id for the account and that opp is deleted
            // and account team member is present then delete the account team member
            accountTeamMemberToDelete.add(accountTeamMemberMap.get(accountToUserIdKey));
          }
        }
    }
    return accountTeamMemberToDelete;
  }

  /**
  * @description Method to populate Account team member map with key as AccountId_UserId 
  * @param accountTeamMemberMap : Account team member Map
  * @param accountIds : Set of Account Id 
  **/
  private static void populateAccountTeamMemberMap(Map<String, AccountTeamMember> accountTeamMemberMap, Set<Id> accountIds){
    for(AccountTeamMember thisAccountMember :[SELECT Id, UserId, AccountId
                                                FROM AccountTeamMember 
                                                WHERE AccountId IN :accountIds]){
         accountTeamMemberMap.put(String.valueOf(thisAccountMember.AccountId)+'_'+String.valueOf(thisAccountMember.UserId), thisAccountMember);
    }
  }

   /**
  * @description Method to get count of its opportunity record based on record type
  * @param accountTeamMemberAggregateMap : The aggregate map which stores the count
  * @param accountIds : Set of Account Id 
  * @param deleteOpportunityIds : Set of deleted Opportunity Ids
  **/
  private static void populateAccountTeamAggregateMap(Map<String, Integer> accountTeamMemberAggregateMap, Set<Id> accountIds, Set<Id> deleteOpportunityIds){
    for(AggregateResult thisResult : [SELECT  AccountId, RecordTypeId,  COUNT(Id) totalRecords
                                          FROM Opportunity
                                          WHERE AccountId IN :accountIds
                                          AND Id NOT IN : deleteOpportunityIds
                                          GROUP BY  AccountId, RecordTypeId]){
      accountTeamMemberAggregateMap.put((string)thisResult.get('AccountId')+'_'+(string)thisResult.get('RecordTypeId'), (Integer)thisResult.get('totalRecords')); 
    }
  }

  /**
  * @description : Method to create Affiliation for the Application Opportunity
  * @param opportunityNewMap : Trigger New Map Opportunity
  * @param applicationOpportunityIds : Application Opportunity Ids for which Affiliation needs to be created
  * @return void 
  **/
  public static List<hed__Affiliation__c> createAffiliation(Map<Id, Opportunity> opportunityNewMap, Set<Id> applicationOpportunityIds){
    system.debug('%%%%%Inside create Affiliation');
    List<hed__Affiliation__c> affiliationsToInsert = new List<hed__Affiliation__c>();
    for(Id thisApplicationOppId : applicationOpportunityIds){
      hed__Affiliation__c thisNewAffiliation    = new hed__Affiliation__c();
      thisNewAffiliation.hed__Contact__c        = opportunityNewMap.get(thisApplicationOppId).Applicant__c;
      thisNewAffiliation.hed__Account__c        = opportunityNewMap.get(thisApplicationOppId).Course__c;
      //The Role and status for the affililiation record created for Application is Applicant and current
      thisNewAffiliation.hed__Role__c           = ct_Constants.AFFILIATION_ROLE_APPLICANT;
      thisNewAffiliation.hed__Status__c         = ct_Constants.AFFILIATION_STATUS_CURRENT;
      thisNewAffiliation.hed__StartDate__c      = opportunityNewMap.get(thisApplicationOppId).CloseDate;
      affiliationsToInsert.add(thisNewAffiliation);
    }
    return affiliationsToInsert;
  }


  /**
  * @description  : Method to Process Application Opportunity before Insert
  * @param thisApplicationOpportunity : Application Opportunity
  * @return void 
  **/
  public static void processApplicationOpportunity(Opportunity thisApplicationOpportunity){
    if(thisApplicationOpportunity.RecordTypeId == ct_Constants.OPPORTUNITY_RECORDTYPE_COURSE_ADMISSION){
      //Set the below checkbox to prevent duplicate contact role creation for the Application opportunity
      thisApplicationOpportunity.npsp__DisableContactRoleAutomation__c = true;
    }
  }


  /**
  * @description  : Method to Update Affiliation status for rejected Application Opportunity
  * @param rejectedApplication Rejected Application
  * @param courseIds : Set of course Ids
  * @param applicantIds :Set of Applicant
  * @return void 
  **/
  public static List<hed__Affiliation__c> updateAffiliationStatus(Map<String, Opportunity> rejectedApplication, Set<Id> courseIds, Set<Id> applicantIds){
    List<hed__Affiliation__c> affiliationsToUpdate = new List<hed__Affiliation__c>();
        for(hed__Affiliation__c thisAffiliation : [SELECT Id, hed__Role__c, hed__Status__c, hed__Contact__c, hed__Account__c
                                                        FROM hed__Affiliation__c
                                                        WHERE hed__Contact__c IN: applicantIds
                                                        AND hed__Account__c IN:courseIds]){
              if(rejectedApplication.containsKey(thisAffiliation.hed__Account__c+'_'+thisAffiliation.hed__Contact__c)
              && rejectedApplication.get(thisAffiliation.hed__Account__c+'_'+thisAffiliation.hed__Contact__c).Course__c == thisAffiliation.hed__Account__c
              && rejectedApplication.get(thisAffiliation.hed__Account__c+'_'+thisAffiliation.hed__Contact__c).Applicant__c == thisAffiliation.hed__Contact__c){
                thisAffiliation.hed__Role__c   = ct_Constants.AFFILIATION_ROLE_APPLICANT;
                thisAffiliation.hed__Status__c = ct_Constants.AFFILIATION_STATUS_FORMER;
                affiliationsToUpdate.add(thisAffiliation);
              }
          }
    return affiliationsToUpdate;
  }

  /**
  * @description : Method to update Donor Status and Donor type based on Opportunity Details
  * @param contactIds 
  * @return void 
  **/
  @future
  public static void updateDonorDetails(Set<Id> contactIds){
    Date LAST_12_MONTH_DATE = System.today().addMonths(-12);
    Set<Id> preProcessContactId = new Set<Id>();
    Set<Id> donationOpportunityRecordTypeIds = new Set<Id>{ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_LEGACY_DONATION_PLEDGE,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_MAJOR_DONATION,
                                                          ct_Constants.OPPORTUNITY_RECORDTYPE_IN_KIND_GIFT};

    Map<Id, Contact> contactMapToUpdate = new Map<Id, Contact>([SELECT Id, Name, RecordTypeId, Donor_Type__c, Donor_Status__c
                                                                  FROM Contact
                                                                  WHERE Id IN:contactIds
                                                                  AND RecordTypeId =: ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT]);
    //Aggregate Result for Processing donor status in contact
    Map<Id,AggregateResult> donorTypeMap = getDonorStatusAggregateResult(contactMapToUpdate.keySet(), donationOpportunityRecordTypeIds);

    Contact thisTempContactToProcess;
    //Get All donation opportunity for the donor Contacts
    for(Opportunity thisOpportunity : [SELECT Id, Name, npsp__Primary_Contact__c, StageName, CloseDate, IsClosed, IsWon
                                        FROM Opportunity
                                        WHERE npsp__Primary_Contact__c IN: contactMapToUpdate.keySet()
                                        AND RecordTypeId IN:donationOpportunityRecordTypeIds
                                        ORDER By CloseDate DESC]){
                                          
        if(preProcessContactId.isEmpty()
        || !preProcessContactId.contains(thisOpportunity.npsp__Primary_Contact__c)){
          //Reset Donor status and type initially
          resetDonorDetails(thisOpportunity.npsp__Primary_Contact__c, contactMapToUpdate, preProcessContactId, donorTypeMap);
        }
        
        thisTempContactToProcess = contactMapToUpdate.get(thisOpportunity.npsp__Primary_Contact__c);
        
        //Process Open Opportunity 
        if(!thisOpportunity.IsClosed
          && thisOpportunity.StageName != ct_Constants.PLEDGE_OPPORTUNITY_STAGE
          && (thisTempContactToProcess.Donor_Status__c == null 
            || !thisTempContactToProcess.Donor_Status__c.contains(ct_Constants.CONTACT_DONOR_STATUS_PROSPECT))) {
              thisTempContactToProcess.Donor_Status__c = thisTempContactToProcess.Donor_Status__c == null ? ct_Constants.CONTACT_DONOR_STATUS_PROSPECT : thisTempContactToProcess.Donor_Status__c +';'+ct_Constants.CONTACT_DONOR_STATUS_PROSPECT;
        }

        //Process Pledge Opportunity
        else if(!thisOpportunity.IsClosed
          && thisOpportunity.StageName == ct_Constants.PLEDGE_OPPORTUNITY_STAGE
          && (thisTempContactToProcess.Donor_Status__c == null 
            || !thisTempContactToProcess.Donor_Status__c.contains(ct_Constants.CONTACT_DONOR_STATUS_PLEDGED))){
              thisTempContactToProcess.Donor_Status__c = thisTempContactToProcess.Donor_Status__c == null ? ct_Constants.CONTACT_DONOR_STATUS_PLEDGED : thisTempContactToProcess.Donor_Status__c +';'+ct_Constants.CONTACT_DONOR_STATUS_PLEDGED;
        }

        //Process Current Donation Opportunity
        else if(thisOpportunity.IsWon
        && thisOpportunity.CloseDate >= LAST_12_MONTH_DATE
        && (thisTempContactToProcess.Donor_Status__c == null 
            || !thisTempContactToProcess.Donor_Status__c.contains(ct_Constants.CONTACT_DONOR_STATUS_CURRENT))){
            thisTempContactToProcess.Donor_Status__c = thisTempContactToProcess.Donor_Status__c == null ? ct_Constants.CONTACT_DONOR_STATUS_CURRENT : thisTempContactToProcess.Donor_Status__c +';'+ct_Constants.CONTACT_DONOR_STATUS_CURRENT;
        }

        //Process Lapsed Opportunity 
        else if(thisOpportunity.IsWon
          && thisOpportunity.CloseDate < LAST_12_MONTH_DATE
          && thisTempContactToProcess.Donor_Status__c == null){
            thisTempContactToProcess.Donor_Status__c =  ct_Constants.CONTACT_DONOR_STATUS_LAPSED;
          }
        //Update Contact Map with new updated Contact Record;
        contactMapToUpdate.put(thisTempContactToProcess.Id, thisTempContactToProcess);
                                       
    }

    if(!contactMapToUpdate.isEmpty()){
      try{
        //Update the Donor details for the processed contacts
        update contactMapToUpdate.values();
      }
      catch(Exception e){
        //Invoke Error Logger if any error occurs
        ct_Logger.logMessage(e, 'Contact');
        throw e;
      }
    }
  }

  /**
  * @description : Method to reset the Donor status and donor type as pre process step
  * @param thisContactId Contact Id
  * @param contactMap Contact Map
  * @param preProcessContactId Set of Pre Processed Id
  * @return void 
  **/
  private static void resetDonorDetails(Id thisContactId, Map<Id, Contact> contactMap, Set<Id> preProcessContactIds, Map<Id,AggregateResult> donorTypeAggResultMap){
    if(!contactMap.isEmpty()
    && contactMap.containsKey(thisContactId)){
      contactMap.get(thisContactId).Donor_Status__c = null;
      contactMap.get(thisContactId).Donor_Type__c   = null;
      //Process donor type based on the aggregate result of opportunity amount
      processContactDonorType(contactMap.get(thisContactId), donorTypeAggResultMap);
      preProcessContactIds.add(thisContactId);
    }
  }

  /**
  * @description : Aggregate Result based on opportunity amount for Processing donor status in contact 
  * @param contactIds : Set Of contact Ids for which AggregateResult need to be calculated
  * @param opportunityRecordTypeIds : Set of opportunity Record type Id filter
  * @return Map<Id, AggregateResult> AggregateResult of Current Closed Won Opportunity Amount roll up
  **/
  public static Map<Id,AggregateResult> getDonorStatusAggregateResult(Set<Id> contactIds, Set<Id> opportunityRecordTypeIds){
    //Get the result based on opportunity amount whose close date is within 12 months from today
    Date LAST_12_MONTH_DATE = System.today().addMonths(-12);
    Map<Id,AggregateResult> donorTypeAggregateMap = new Map<Id,AggregateResult>([SELECT npsp__Primary_Contact__c Id, 
                                                                          SUM(Amount) oppAmount 
                                                                          FROM Opportunity 
                                                                          WHERE npsp__Primary_Contact__c != null
                                                                          AND Amount != null
                                                                          AND isWon = true
                                                                          AND RecordTypeId IN:opportunityRecordTypeIds
                                                                          AND npsp__Primary_Contact__c IN:contactIds
                                                                          AND CloseDate >=: LAST_12_MONTH_DATE
                                                                          GROUP BY npsp__Primary_Contact__c]);

    return donorTypeAggregateMap; 
  }

  /**
  * @description : Method to process Contact Donor Type Based on AggregateResult of Current Closed Won Opportunity Amount roll up
  * @param thisContact   : Contact record to processed
  * @return void 
  **/
  public static void processContactDonorType(Contact thisContact, Map<Id,AggregateResult> donorTypeAggResultMap){
    if(!donorTypeAggResultMap.isEmpty()
        && donorTypeAggResultMap.containsKey(thisContact.Id)){

        if(Decimal.valueOf(String.valueOf(donorTypeAggResultMap.get(thisContact.Id).get('oppAmount'))) > 0
          && Decimal.valueOf(String.valueOf(donorTypeAggResultMap.get(thisContact.Id).get('oppAmount'))) < 1000){
            thisContact.Donor_Type__c    = ct_Constants.CONTACT_DONOR_TYPE_COMMUNITY_DONOR;
        }
        else if(Decimal.valueOf(String.valueOf(donorTypeAggResultMap.get(thisContact.Id).get('oppAmount'))) >= 1000
          && Decimal.valueOf(String.valueOf(donorTypeAggResultMap.get(thisContact.Id).get('oppAmount'))) < 5000){
            thisContact.Donor_Type__c   = ct_Constants.CONTACT_DONOR_TYPE_LEADERSHIP_DONOR;
        }
        else if(Decimal.valueOf(String.valueOf(donorTypeAggResultMap.get(thisContact.Id).get('oppAmount'))) >= 5000
          && Decimal.valueOf(String.valueOf(donorTypeAggResultMap.get(thisContact.Id).get('oppAmount'))) < 10000){
            thisContact.Donor_Type__c   = ct_Constants.CONTACT_DONOR_TYPE_MAJOR_DONOR;
        }
        else if(Decimal.valueOf(String.valueOf(donorTypeAggResultMap.get(thisContact.Id).get('oppAmount'))) >= 10000){
            thisContact.Donor_Type__c   = ct_Constants.CONTACT_DONOR_TYPE_PRINCIPAL_DONOR;
        }
      }
  }

  /**
  * @description Method to Set the Opportunity stage to Close Lost and update Lost reason Details for Paused Recurring donations 
  * @param thisOpportunity :Opportunity record to be processed
  **/
  public static void processPausedOpportunity(Opportunity thisOpportunity){
    thisOpportunity.StageName                     = ct_Constants.OPPORTUNITY_STAGE_CLOSED_LOST;
    thisOpportunity.Lost_Reason__c                = ct_Constants.OPPORTUNITY_LOST_REASON_PAUSED_DIRECT_DEBIT;
    thisOpportunity.npsp__Closed_Lost_Reason__c   = ct_Constants.OPPORTUNITY_LOST_REASON_PAUSED_DIRECT_DEBIT;
    thisOpportunity.npe01__Do_Not_Automatically_Create_Payment__c   = true;
  }
}