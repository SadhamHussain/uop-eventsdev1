/**
 * @File Name          : ct_ProjectTimeTriggerHelper_Test.cls
 * @Description        : Test class for ct_ProjectTimeTriggerHelper_Test,ct_ct_ProjectTimeTriggerHandler_TDTM
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/19/2020, 12:09:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/19/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_ProjectTimeTriggerHelper_Test {

    testMethod static void unit1(){
        // first retrieve default EDA trigger handlers
       List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_ProjectTimeTriggerHandler_TDTM', 'Project_Time__c', 'AfterInsert;AfterUpdate;AfterDelete;BeforeUpdate', 1.00));

       // Pass trigger handler config to set method for this test run
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        Project__c thisProject = ct_TestDataFactory.createProject();
		insert thisProject;
		Project_Task__c thisTask = ct_TestDataFactory.createProjectTask(thisProject.Id);
		insert thisTask;
        Test.startTest();
        Project_Time__c thisTime = ct_TestDataFactory.createProjectTime(thisProject.Id,thisTask.Id);
		insert thisTime;
        delete thisTime;
        System.assertEquals(thisTask.Project__c, thisProject.Id);
        Test.stopTest();
    }
    testMethod static void unit2(){
        // first retrieve default EDA trigger handlers
       List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_ProjectTimeTriggerHandler_TDTM', 'Project_Time__c', 'AfterInsert;AfterUpdate;AfterDelete;BeforeUpdate', 1.00));

       // Pass trigger handler config to set method for this test run
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        
        Project__c thisProject = ct_TestDataFactory.createProject();
		insert thisProject;
        Project_Task__c thisTask = ct_TestDataFactory.createProjectTask(thisProject.Id);
		insert thisTask;
        Project_Task__c thisTask1 = ct_TestDataFactory.createProjectTask(thisProject.Id);
        insert thisTask1;
        Test.startTest();
        Project_Time__c thisTime = ct_TestDataFactory.createProjectTime(thisProject.Id,thisTask.Id);
        insert thisTime;
        thisTime.Project_Task__c = thisTask1.Id;
        update thisTime;
        System.assertEquals(thisTime.Project_Task__c, thisTask1.Id);
        Test.stopTest();
    }
}