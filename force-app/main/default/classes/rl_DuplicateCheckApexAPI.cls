/*******************************************************************************************************************************
* @name:           rl_DuplicateCheckApexAPI
* @description:    Provides static methods that utilise the Duplicate Check Apex API
* 
*
* METHODS
* @checkForExistingMatches(Lead leadSobject)	Checks if Lead object matches with existing Contact or Lead using specific filters
*												and returns a single matching record Id (as String) or a blank string
* 
*
* @Created:   28-04-2020 v0.1 - initial version - Rob Liesicke
* @Updated:   
**********************************************************************************************************************************/

global class rl_DuplicateCheckApexAPI {
    
    public static String checkForExistingMatches(Lead leadSobject)
    {
    	
        // Get instance of Duplicate Check API
		dupcheck.dc3Api api = new dupcheck.dc3Api(); 
		Map<String, List<dupcheck.dc3SearchResult>> searchResult;
        
        if (test.isRunningTest()) 
        {
            
            if(leadSobject.LastName=='testLead1-UN1QU3')
            {
                List<Lead> testLeadList = [SELECT Id, LastName FROM Lead WHERE LastName = 'testLead1-UN1QU3'];
                
                searchResult = new Map<String, List<dupcheck.dc3SearchResult>>();
                searchResult.put('00Q', new List<dupcheck.dc3SearchResult>());
                dupcheck.dc3SearchResult dup1 = new dupcheck.dc3SearchResult();
                dup1.score = 50; 
                dup1.ScenarioScores = new List<dupcheck.dc3SearchResult.ScenarioScore>();
                
                if(testLeadList.size() > 0)
                {
                    dup1.objectData = testLeadList.get(0);
                    
                    dupcheck.dc3SearchResult.ScenarioScore sc1 = new dupcheck.dc3SearchResult.ScenarioScore('a501r000000uxw1AAA','Any Name',50);
                    dup1.ScenarioScores.add(sc1);
                }
                
                searchResult.get('00Q').add(dup1); 
            }
            
            
            else if(leadSobject.LastName=='testContact1-UN1QU3')
            {
                List<Contact> testContactList = [SELECT Id, LastName FROM Contact WHERE LastName = 'testContact1-UN1QU3'];
                
                searchResult = new Map<String, List<dupcheck.dc3SearchResult>>();
                searchResult.put('003', new List<dupcheck.dc3SearchResult>());
                dupcheck.dc3SearchResult dup1 = new dupcheck.dc3SearchResult();
                dup1.score = 70; 
                dup1.ScenarioScores = new List<dupcheck.dc3SearchResult.ScenarioScore>();
                
                if(testContactList.size() > 0)
                {
                    dup1.objectData = testContactList.get(0);
                    
                    dupcheck.dc3SearchResult.ScenarioScore sc1 = new dupcheck.dc3SearchResult.ScenarioScore('a501r000000uxnnAAA','Any Name',70);
                    dup1.ScenarioScores.add(sc1);
                }
                
                searchResult.get('003').add(dup1); 
            }

            else
            {
            	return '';
            }            
            
         }
        
        else 
        {
           searchResult = api.doSearch(leadSobject);
        }
        
        // Get search results related to Contacts and loop through the scenario scores
        if (searchResult.containsKey('003')) 
		{   
            List<dupcheck.dc3SearchResult> contactSearchResults = searchResult.get('003');
                 
            for (dupcheck.dc3SearchResult contactSearchResult : contactSearchResults) 
			{     
                for (dupcheck.dc3SearchResult.ScenarioScore scenarioScore : contactSearchResult.ScenarioScores)
				{
                    
                    // Hardcoded reference to Duplicate Check filter and required scenario score
                	if(scenarioScore.scenarioId == 'a501r000000uxnnAAA' && scenarioScore.scenarioScore >= 70)
                    {
                        System.Debug('Score:' + scenarioScore.scenarioScore);
                        
                        // Return first result that meets criteria for scenario. 
                        // Limitation here is that the results are not sorted so better matches may be missed
                        return contactSearchResult.objectData.Id;
                    }
				}
			}
		}

        // Get search results related to Leads and loop through the scenario scores        
        if (searchResult.containsKey('00Q')) 
		{   
            List<dupcheck.dc3SearchResult> leadSearchResults = searchResult.get('00Q');
            
            for (dupcheck.dc3SearchResult leadSearchResult : leadSearchResults) 
			{     
                for (dupcheck.dc3SearchResult.ScenarioScore scenarioScore : leadSearchResult.ScenarioScores)
				{
                    
                    // Hardcoded reference to Duplicate Check filter and required scenario score
                	if(scenarioScore.scenarioId == 'a501r000000uxw1AAA' && scenarioScore.scenarioScore >= 50)
                    {
                        System.Debug('Score:' + scenarioScore.scenarioScore);
                        
                        // Return first result that meets criteria for scenario. 
                        // Limitation here is that the results are not sorted so better matches may be missed                        
                        return leadSearchResult.objectData.Id;
                    }
				}
			}
		}
        
        // If no suitable matches for the specific scenarios are found, return blank string
        return '';
	}
}