/*****************************************************************
* bg_OpportunityUtils_Test
*
* Test class for bg_OpportunityUtils
*
* Author: 
* Created: 
******************************************************************/

@isTest(SeeAllData=true) 
public class bg_OpportunityUtils_Test {
    
    static Application__c setNewApp(){
        Application__c newApp = new Application__c();
        newApp.Application_Year__c = '2020';
        newApp.Fee_Category__c = 'foo';
        newApp.Interview__c = true;
        newApp.Entry_Year__c = '2021';
        newApp.Entry_Month__c = 'March';
        newApp.Faculty__c = '0061j000007r5TQAAY';
        newApp.Agent_Application__c = true;
        newApp.Mature_Student__c = true;
        newApp.Type__c = 'Fun';
        newApp.Course__c = '0061j000007r5TQAAY';
        newApp.Start_Date__c = Date.today();
        return newApp;
    }
    
    static Application__c setOldApp(){
        Application__c oldApp = new Application__c();
        oldApp.Application_Year__c = '2019';
        oldApp.Fee_Category__c = 'oof';
        oldApp.Interview__c = false;
        oldApp.Entry_Year__c = '2020';
        oldApp.Entry_Month__c = 'June';
        oldApp.Faculty__c = '0061j000007r5TQAAZ';
        oldApp.Agent_Application__c = false;
        oldApp.Mature_Student__c = false;
        oldApp.Type__c = 'Not';
        oldApp.Course__c = '0061j000007r5TQAAZ';
        oldApp.Start_Date__c = Date.newInstance(1960, 2, 17);
        return oldApp;
    }
    
    @isTest static void testUpdateOpportunityFieldsPosi(){
        Application__c newApp = setNewApp();
        Application__c oldApp = setOldApp();
        Opportunity oppToUpdate = new Opportunity(Id = '0061j000007r5TQAAY');
        
		Opportunity opp = bg_OpportunityUtils.updateOpportunityFields(newApp, oldApp, oppToUpdate.Id, 'Name');    
        
        
        System.assertEquals(opp.Application_Year__c, '2020');
        System.assertEquals(opp.Fee_Status__c, 'foo');
        System.assertEquals(opp.Interview__c, true);
        System.assertEquals(opp.Entry_Year__c, '2021');
        System.assertEquals(opp.Entry_Month__c, 'March');
        System.assertEquals(opp.Faculty__c, '0061j000007r5TQAAY');
        System.assertEquals(opp.Agent_Application__c, true);
        System.assertEquals(opp.Mature_Student__c, true);
        System.assertEquals(opp.Type, 'Fun');
        System.assertEquals(opp.Course__c, '0061j000007r5TQAAY');
        System.assertEquals(opp.CloseDate, Date.today());
    }
    
    @isTest static void testUpdateOpportunityFieldsNegi() {
        Application__c newApp = setNewApp();
        Application__c oldApp = setNewApp();
        Opportunity oppToUpdate = new Opportunity(Id = '0061j000007r5TQAAZ');
        oppToUpdate.Application_Year__c = '2020';
		Opportunity opp = bg_OpportunityUtils.updateOpportunityFields(newApp, newApp, oppToUpdate.Id, 'Name');    
        
        
        System.assertEquals(opp.Application_Year__c, null);
    }
    
    @isTest static void testUpdateNewOpportunity(){
        Application__c newApp = setNewApp();
        newApp.First_Name__c = 'Fred';
        newApp.Last_Name__c = 'James';
        
        Opportunity opp = bg_OpportunityUtils.updateNewOpportunity(newApp, 'Maths', '07/06/2020');
        
        System.assertEquals(newApp.Applicant__c, opp.Applicant__c);
        System.assertEquals(newApp.Application_Stage__c, opp.StageName);
        System.assertEquals(newApp.Start_Date__c, opp.CloseDate);
    }
    
    static testMethod void testProcessUpdatedOpportunitiesAfterUpdateMakesNoChangeWhenApplicantNull()
    {
        
        Contact applicant = bg_UnitTestDataFactory.createApplicantContact();
        insert applicant;        
        Opportunity testOpportunity = bg_UnitTestDataFactory.createOpportunity(applicant);
        testOpportunity.Applicant__c = null;
        insert testOpportunity;
        
        Map<Id, Opportunity> testOpportunityById = new Map<Id, Opportunity>();
        testOpportunityById.put(testOpportunity.Id, testOpportunity);
        
        test.startTest();
        bg_OpportunityUtils.processUpdatedOpportunitiesAfterUpdate(new List<Opportunity>{testOpportunity}, testOpportunityById);
        test.stopTest();
        
        List<Opportunity> updatedOpportunity = [SELECT Id, Applicant_Admission_Cycle__c FROM Opportunity WHERE Id =: testOpportunity.Id];
        
        system.assertEquals(null, updatedOpportunity[0].Applicant_Admission_Cycle__c);
    }
    
    static testMethod void testProcessUpdatedOpportunitiesAfterUpdateCreatesAdmissionCycleForOppWithApplicant()
    {
        Contact applicant = bg_UnitTestDataFactory.createApplicantContact();
        insert applicant;        
        Opportunity testOpportunity = bg_UnitTestDataFactory.createOpportunity(applicant);
        
        Map<Id, Opportunity> testOpportunityById = new Map<Id, Opportunity>();
        testOpportunityById.put(testOpportunity.Id, testOpportunity);
        
        testOpportunity.Applicant__c = applicant.Id;
        insert testOpportunity;
        bg_OpportunityUtils.aicToBeUpserted = true;
        
        test.startTest();
        bg_OpportunityUtils.processUpdatedOpportunitiesAfterUpdate(new List<Opportunity>{testOpportunity}, testOpportunityById);
        test.stopTest();
        
        List<Opportunity> updatedOpportunity = [SELECT Id, Applicant_Admission_Cycle__c FROM Opportunity WHERE Id =: testOpportunity.Id];
        
        system.assertNotEquals(null, updatedOpportunity[0].Applicant_Admission_Cycle__c);
    }
    
    static testMethod void testProcessUpdatedOpportunitiesAfterUpdateUpdatesAdmissionCycleForOppWithApplicantAndUpdatedCycle()
    {
        Contact applicant = bg_UnitTestDataFactory.createApplicantContact();
        insert applicant;        
        Opportunity testOpportunity = bg_UnitTestDataFactory.createOpportunity(applicant);
        
        Map<Id, Opportunity> testOpportunityById = new Map<Id, Opportunity>();
       
        
        testOpportunity.Applicant__c = applicant.Id;
        insert testOpportunity;
        
        Applicant_Admission_Cycle__c admissionCycle = new Applicant_Admission_Cycle__c(Applicant__c = applicant.Id, Admission_Cycle__c = '2022');
        insert admissionCycle;
        
        
        bg_OpportunityUtils.aicToBeUpserted = true;
        Opportunity newTestOpportunity = new Opportunity(StageName = 'Closed - Enrolled', Id = testOpportunity.Id, Applicant__c = applicant.Id,
                                                        	Applicant_Admission_Cycle__c = admissionCycle.Id, Admissions_Cycle__c = '2020');
        testOpportunity.Applicant_Admission_Cycle__c = admissionCycle.Id;
         testOpportunityById.put(testOpportunity.Id, testOpportunity);
        test.startTest();
        bg_OpportunityUtils.processUpdatedOpportunitiesAfterUpdate(new List<Opportunity>{newTestOpportunity}, testOpportunityById);
        test.stopTest();
        
        List<Opportunity> updatedOpportunity = [SELECT Id, Applicant_Admission_Cycle__c FROM Opportunity WHERE Id =: testOpportunity.Id];
        
        system.assertNotEquals(admissionCycle.Id, updatedOpportunity[0].Applicant_Admission_Cycle__c);
    }
    
    static testMethod void testCreateMapOfOpportunityNamesByIdReturnsEmptyMapIfApplicationNotUpdated()
    {
        Application__c testApplication = new Application__c(First_Name__c = 'george', Last_Name__c = 'graham');
        
        Map<Id, Application__c> oldApplicationById = new Map<Id, Application__c>();
        oldApplicationById.put(testApplication.Id, testApplication);
        
        test.startTest();
        Map<Id,String> resultMap = bg_OpportunityUtils.createMapOfOpportunityNamesById(new List<Application__c>{testApplication}, oldApplicationById);
        test.stopTest();
        
		system.assertEquals(0, resultMap.size());
    }
    
    static testMethod void testCreateMapOfOpportunityNamesByIdReturnsPopulatedMapIfCourseChanged()
    {
        Application__c testApplication = new Application__c(First_Name__c = 'george', Last_Name__c = 'graham');
        Map<Id, Application__c> oldApplicationById = new Map<Id, Application__c>();
        oldApplicationById.put(testApplication.Id, testApplication);
        
        Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
        courseAccount.Academic_Programme_Display_Course_Name__c = 'great name';
        courseAccount.Course_Instance_Start_Date__c = Date.today();
        insert courseAccount;
        
        Application__c testNewApplication = new Application__c(First_Name__c = 'percy', Last_Name__c = 'graham', Course__c = courseAccount.Id, Opportunity__c = courseAccount.Id);
        
        test.startTest();
        Map<Id,String> resultMap = bg_OpportunityUtils.createMapOfOpportunityNamesById(new List<Application__c>{testNewApplication}, oldApplicationById);
        test.stopTest();
               
        system.assertEquals(1, resultMap.size());
    }
    
    static testMethod void testCreateMapOfOpportunityNamesByIdReturnsFNameLNameCourseNameCourseDate()
    {
        Application__c testApplication = new Application__c(First_Name__c = 'george', Last_Name__c = 'graham');
        Map<Id, Application__c> oldApplicationById = new Map<Id, Application__c>();
        oldApplicationById.put(testApplication.Id, testApplication);
        
        Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
        courseAccount.Academic_Programme_Display_Course_Name__c = 'great name';
        courseAccount.Course_Instance_Start_Date__c = Date.today();
        insert courseAccount;
        
        Application__c testNewApplication = new Application__c(First_Name__c = 'percy', Last_Name__c = 'graham', Course__c = courseAccount.Id, Opportunity__c = courseAccount.Id);
        
        test.startTest();
        Map<Id,String> resultMap = bg_OpportunityUtils.createMapOfOpportunityNamesById(new List<Application__c>{testNewApplication}, oldApplicationById);
        test.stopTest();
        
        string expectedResult = 'percy graham - great name - ' + Date.today().format();
        system.assertEquals(expectedResult, resultMap.values()[0]);
    }
    
    static testMethod void testPerformUpdateDMLForApplicationOpportunitiesSuccessfullyUpdatesGivenOpportunity()
    {
        Contact applicant = bg_UnitTestDataFactory.createApplicantContact();
        insert applicant;        
        Opportunity testOpportunity = bg_UnitTestDataFactory.createOpportunity(applicant);
        insert testOpportunity;
        
        testOpportunity.Name = 'new test name';
        
        Map<Id, Opportunity> testOpportunityById = new Map<Id, Opportunity>();
        testOpportunityById.put(testOpportunity.Id, testOpportunity);
        
        test.startTest();
        bg_OpportunityUtils.performUpdateDMLForApplicationOpportunities(testOpportunityById);
        test.stopTest();
        
        List<Opportunity> resultList = [SELECT Id, Name FROM Opportunity WHERE Id =: testOpportunity.Id];
        
        system.assertEquals('new test name', resultList[0].Name);
    }
 
    
    /*    /*
    * Perform update of Opportunity records and catch any errors to record on associated Application
    *
    private static void performUpdateDMLForApplicationOpportunities(Map<Id,Opportunity> opportunitiesToUpdateByAppId)
    {
        Database.SaveResult[] updateOpportunities = Database.update(opportunitiesToUpdateByAppId.values(), false); 
        
        Map<Id, String> errorMessageByAppId = new Map<Id, String>();
		
		for(Integer i = 0; i < updateOpportunities.size(); i++) 
		{
	       Id appId = opportunitiesToUpdateByAppId.values()[i].Application__c;
           

	       if(!updateOpportunities[i].isSuccess())
	       {
	       		String errorMessage = '';

		        for(Database.Error error : updateOpportunities[i].getErrors()) 
		        {
		            if(String.isEmpty(errorMessage)) 
		            {
		                errorMessage = error.getMessage();
		            } 
		        }

		        errorMessageByAppId.put(appId, errorMessage);
	       }
        }

        if(!errorMessageByAppId.isEmpty())
        {
            bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
        }
    }
     * createCourseAccount
     *     private static Map<Id,String> createMapOfOpportunityNamesById(List<Application__c> newApplications, Map<Id,Application__c> oldApplicationsById)
    {
        Map<Id,Id> courseIdsByAppId = new Map<Id,Id>();
        Map<Id, Account> courseAccountsById = new Map<Id, Account>();
        Map<Id, String> opportunityNameByOppId = new Map<Id, String>();
        List<Application__c> updatedApps = new List<Application__c>();

		for(Application__c newApp : newApplications)
        {
            Application__c oldApp = oldApplicationsById.get(newApp.Id);
            Id oppId = newApp.Opportunity__c;

            if(String.isNotBlank(oppId))
            {
                if((newApp.Course__c != oldApp.Course__c) ||
                   (newApp.First_Name__c != oldApp.First_Name__c) ||
                   (newApp.Last_Name__c != oldApp.Last_Name__c))
                {
                    courseIdsByAppId.put(newApp.Id, newApp.Course__c);
                    updatedApps.add(newApp);
                }
            }
        }

        if(!courseIdsByAppId.isEmpty())
        {
            courseAccountsById = new Map<Id, Account>([SELECT Id, Name, Academic_Programme_Display_Course_Name__c, Course_Instance_Start_Date__c 
													          FROM Account
													          WHERE Id IN :courseIdsByAppId.values()]);
        }

        if(!updatedApps.isEmpty())
        {
            for(Application__c updatedApp : updatedApps)
            {
                Application__c oldApp = oldApplicationsById.get(updatedApp.Id);
                Id oppId = updatedApp.Opportunity__c;
                String courseDisplayName;
                string courseStartDate;

                if(String.isNotBlank(oppId))
                {
                    if(!courseAccountsById.isEmpty())
                    {
                        courseDisplayName = courseAccountsById.get(updatedApp.Course__c).Academic_Programme_Display_Course_Name__c;
                        courseStartDate = courseAccountsById.get(updatedApp.Course__c).Course_Instance_Start_Date__c.format();

                        if(courseDisplayName != null && courseStartDate != null)
                        {
                            String oppName = updatedApp.First_Name__c + ' ' + updatedApp.Last_Name__c + ' - ' + courseDisplayName + ' - ' + courseStartDate;
                            opportunityNameByOppId.put(oppId, oppName);
                        }
                    }
                } 
            }
        }

        return opportunityNameByOppId;
    }
     * public static void updateOpportunityLinkedToUpdatedApplication(List<Application__c> newApplications, Map<Id,Application__c> oldApplicationsById)
    {
        Map<Id, Opportunity> opportunitiesToUpdateByAppId = new Map<Id, Opportunity>();
        Map<Id, String> opportunityNameByOppId = createMapOfOpportunityNamesById(newApplications, oldApplicationsById);

        for(Application__c newApp : newApplications)
        {
            Application__c oldApp = oldApplicationsById.get(newApp.Id);
            Id oppId = newApp.Opportunity__c;
            String oppName; 

            if(String.isNotBlank(oppId))
            {
                if(!opportunityNameByOppId.isEmpty())
                {
                    oppName = opportunityNameByOppId.get(oppId);
                }

                Opportunity oppToUpdate = updateOpportunityFields(newApp, oldApp, oppId, oppName);
                
                if(oppToUpdate != null)
                {
                    opportunitiesToUpdateByAppId.put(newApp.Id, oppToUpdate);
                }
            }
        }

        if(!opportunitiesToUpdateByAppId.isEmpty())
        {
            performUpdateDMLForApplicationOpportunities(opportunitiesToUpdateByAppId);
        }
    }
     * 
     public static void processUpdatedOpportunitiesAfterUpdate(List<Opportunity> newOpportunities, Map<Id,Opportunity> oldOpportunitiesById)
    {

        Map<Id, Opportunity> oppsRequiringApplicantIntakeCycleProcessing = new Map<Id, Opportunity>();
        Map<Id, Opportunity> oldOppsRequiringApplicantIntakeCycleProcessing = new Map<Id, Opportunity>();
        Map<Id, Opportunity> oppsWithApplicantIntakeCycle = new Map<Id, Opportunity>();
        Map<Id, Opportunity> oldOppsWithApplicantIntakeCycle = new Map<Id, Opportunity>();
        
        for(Opportunity newOpp : newOpportunities)
        {
            Opportunity oldOpp = oldOpportunitiesById.get(newOpp.Id);

            if(newOpp.Applicant_Admission_Cycle__c == null && String.isNotBlank(newOpp.Applicant__c))
            {
                oppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, newOpp);
                oldOppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, newOpp);
            }
            else if(newOpp.Applicant_Admission_Cycle__c != null && String.isNotBlank(newOpp.Applicant__c) && string.isNotBlank(oldOpp.Applicant_Admission_Cycle__c)
                   && newOpp.StageName != oldOpp.StageName)
            {
                
                oppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, newOpp);
                oldOppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, oldOpp);
            }
        }

        if(!oppsRequiringApplicantIntakeCycleProcessing.isEmpty() && aicToBeUpserted)
        {
            bg_ApplicantAdmissionCycleUtils.processApplicantIntakeCyclesForOpportunities(oppsRequiringApplicantIntakeCycleProcessing, oldOppsRequiringApplicantIntakeCycleProcessing);
        }

    } 
     * 
     */
    
    @isTest static void testCreateMapOfOpportunitiesByContact(){
        Account acc1 = new Account();
        acc1.Name = 'Acc1';
        insert acc1;
        
        Contact cont1 = new Contact();
        cont1.AccountId = acc1.id;
        cont1.FirstName = 'Cont';
        cont1.LastName = 'act1';
        insert cont1;
        
        
        Opportunity opp1 = new Opportunity(Applicant__c = cont1.id, Name = 'Opp1', StageName = 'Applied', CloseDate = Date.today(), AccountId = acc1.id);
        Opportunity opp2 = new Opportunity(Applicant__c = cont1.id, Name = 'Opp2', StageName = 'Applied',  CloseDate = Date.today(), AccountId = acc1.id);

        insert new List<Opportunity>{opp1, opp2};
        
        Map<Id, Opportunity> oppsById = new Map<Id, Opportunity>();
        oppsById.put(opp1.Id, opp1);
        oppsById.put(opp2.id, opp2);

        
		List<Opportunity> cont1Opps = new List<Opportunity>();
		cont1Opps.add(opp1);
        cont1Opps.add(opp2);

        
        Map<Id, List<Opportunity>> testOppsByContactId = new Map<Id, List<Opportunity>>();
        testOppsByContactId.put(cont1.id, cont1Opps);
        System.debug(testOppsByContactId.values());
        
        Map<Id, List<Opportunity>> oppsByContactId = bg_OpportunityUtils.createMapOfOpportunitiesByContact(oppsById);
        List<Opportunity> resultList = oppsByContactId.values()[0];
        System.assertEquals(2, resultList.size());
    }

}