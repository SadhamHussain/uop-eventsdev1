/****************************************************************************
*  bg_ApplicationSchoolAccount_Batch
*
*  Batch class to update applications with a school account
*  Test class is bg_ApplicationSchoolAccountBatch_Test
* 
*  Author: Ismail Basser - Brightgen
*  Created: 05/11/2019
* 
*  Changes: 
*  VERSION  AUTHOR            DATE              DETAIL		DESCRIPTION
*  
***********************************************************************/
global class bg_ApplicationSchoolAccount_Batch implements Database.Batchable<sObject>, Database.Stateful {
    global List<Application__c> applicationsToProcess = new List<Application__c>();
    
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        String query = 'SELECT Id, School_College__c, UCAS_School_Code__c, Applicant__c FROM Application__c WHERE School_College__c = null AND UCAS_School_Code__c != null ORDER BY LastModifiedDate ASC';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        Map<Application__c, String> ucasSchoolCodeByApplication = new Map<Application__c, String>();
        List<Application__c> applicationsToUpdateList = new List<Application__c>();
        Map<Id, List<Application__c>> applicationsByApplicantId = new Map<Id, List<Application__c>>();
        
        for(sObject s : scope)
        {
            Application__c app = (Application__c)s;
            if(!applicationsByApplicantId.containsKey(app.Applicant__c))
            {
                applicationsByApplicantId.put(app.Applicant__c, new List<Application__c>{app});
            }
            else
            {
                applicationsByApplicantId.get(app.Applicant__c).add(app);
            }
        }
        
        for(Id applicantId : applicationsByApplicantId.KeySet())
        {
            List<Application__c> relatedApps = applicationsByApplicantId.get(applicantId);
            Application__c app = relatedApps[0];
            ucasSchoolCodeByApplication.put(app, app.UCAS_School_Code__c);
            
            if(relatedApps.size() > 1)
            {
                for(Integer i=1; i<relatedApps.size(); i++)
                {
                    applicationsToProcess.add(relatedApps[i]);
                }
            }
        }

        List<Account> schoolAccounts = [SELECT Id, School_College_UCAS_Code__c FROM ACCOUNT WHERE School_College_UCAS_Code__c IN:ucasSchoolCodeByApplication.values()];
        Map<String, Id> ucasCodeBySchoolAccountId = new Map<String, Id>();
        
        for(Account schoolAcc : schoolAccounts)
        {
            ucasCodeBySchoolAccountId.put(schoolAcc.School_College_UCAS_Code__c, schoolAcc.Id);
        }
        
        for(Application__c appToUpdate : ucasSchoolCodeByApplication.KeySet())
        {
            String applicationUcasSchoolCode = ucasSchoolCodeByApplication.get(appToUpdate);
            Id schoolAccountId = ucasCodeBySchoolAccountId.get(applicationUcasSchoolCode);
            appToUpdate.School_College__c = schoolAccountId;
            applicationsToUpdateList.add(appToUpdate);
            
        }
        
        Database.SaveResult[] srList = Database.update(applicationsToUpdateList, false);
        
        for (Database.SaveResult sr : srList) 
        {
            if(!sr.isSuccess())
            {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) 
                {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Application fields that affected this error: ' + err.getFields());
                }
            }
        }

        
    }
    
    global void finish(Database.BatchableContext bc)
    {
        //if there were any duplicate applicants, run again
        if(!applicationsToProcess.isEmpty())
        {
            bg_ApplicationSchoolAccount_Batch applicationsBatch = new bg_ApplicationSchoolAccount_Batch();
            Database.executeBatch(applicationsBatch);
        }
    }

}