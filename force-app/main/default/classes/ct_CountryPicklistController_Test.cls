/**
 * @File Name          : ct_CountryPicklistController_Test.cls
 * @Description        : 
 * @Author             : Javid Sherif
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 17/4/2020, 6:29:42 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/4/2020   Javid Sherif     Initial Version
**/
@IsTest
public class ct_CountryPicklistController_Test {

  /**
  * @description test Invoke country picklist parser method
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @IsTest
  static void testCountryParser(){
    Test.startTest();
    ct_CountryPicklistController.parseCountryValues();
    Test.stopTest();
  }
}