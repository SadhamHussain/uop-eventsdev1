/**
 * @File Name          : ct_ScheduledAsperatoPaymentReportTest.cls
 * @Description        : Test Class for ct_ScheduledAsperatoPaymentReport Class
 * @Author             : Creation Admin  - Creation Technology Solutions
 * @Last Modified By   : Creation Admin  - Creation Technology Solutions
 * @Last Modified On   : 1/6/2020, 8:16:11 pm
**/
@isTest
public class ct_ScheduledAsperatoPaymentReportTest {    
    /**
    * @description - Test the ScheduledAsperatoPaymentReport functionality
    * @author Creation Admin  - Creation Technology Solutions | 21/5/2020 
    * @return void 
    **/
    @isTest(SeeAllData=true)
    public static void scheduledAsperatoPaymentReportTest() {
        Test.startTest();
        Contact thisContact = ct_TestDataFactory.createContact();
        insert thisContact;

        Opportunity thisOpportunity = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
        thisOpportunity.Amount = 200.00;
        insert thisOpportunity;

        npsp__General_Accounting_Unit__c thisGAU = ct_TestDataFactory.createGAURecord('Creation GAU');
        insert thisGAU;
   
        npsp__Allocation__c thisGAUAllocationOne = ct_TestDataFactory.createGAUAllocation(thisGAU.Id, thisOpportunity.Id, 200.00);
        insert thisGAUAllocationOne;

        npe01__OppPayment__c thisGEMPayment   = ct_TestDataFactory.createGEMPayment(thisOpportunity, ct_Constants.CASH_PAYMENT_METHOD);
        thisGEMPayment.npe01__Paid__c         = true;
        thisGEMPayment.npe01__Payment_Date__c = System.today();
        insert thisGEMPayment;


        String sch = '0 0 0 ? * * *';
        ct_ScheduledAsperatoPaymentReport thisScheduledAsperatoPaymentReport = new ct_ScheduledAsperatoPaymentReport();
        System.schedule('Test Asperato Payment Transaction Report', sch, thisScheduledAsperatoPaymentReport); 
        Test.stopTest();
    }
}