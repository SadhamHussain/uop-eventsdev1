/**
 * @File Name          : ct_ContactTriggerHelper.cls
 * @Description        : Process Alumni, Volunteer and Donation fuctionalities
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 25/6/2020, 5:28:29 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/23/2020   Creation Admin     Initial Version
**/
 public class ct_ContactTriggerHelper {

    /**
    * @description Submit for approval if the Alumni Verification Status is Unverified
    * @author Creation Admin | 18/6/2020 
    * @param contactNewList 
    * @return void 
    **/
    public static void submitAlumniVerificationApproval(List<Contact> contactNewList){
      List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();

      for(Contact thisContact : contactNewList){
        if(thisContact.Alumni_Verification_Status__c == ct_Constants.CONTACT_ALUMNI_VERIFICATION_STATUS_UNVERIFIED){
          Approval.ProcessSubmitRequest alumniVerificationRequest = new Approval.ProcessSubmitRequest();
          alumniVerificationRequest.setComments('Submitting request for Alumni Verification Approval');
          alumniVerificationRequest.setObjectId(thisContact.Id);
          alumniVerificationRequest.setSubmitterId(thisContact.OwnerId);
          requests.add(alumniVerificationRequest);
        }
      }

      if(!requests.isEmpty()){
        Approval.ProcessResult[] processResults = null;
        try {
          if(ct_CustomSettingService.getCurrentUserByPassConfiguration() == null
          || (!ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Emails_Off__c
          && !ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Alumni_Emails_off__c)){
            processResults = Approval.process(requests, true);
          }
        }catch (Exception e) {
          //Exception mechanism to log any exceptions occured during the dml operation
          ct_Logger.logMessage(e, 'Contact');
          throw e;
        }
      }
    }
    
    /**
    * @description Set Deceased Value to True if Date Of Death is present
    * @author Creation Admin | 18/6/2020 
    * @param contactNewMap 
    * @param contactOldMap 
    * @return void 
    **/
    public static void setDeceasedValue(List<Contact> contactNewList){
      for(Contact thisContact : contactNewList){
        if(thisContact.Date_of_Death__c != null && !thisContact.hed__Deceased__c){
          thisContact.hed__Deceased__c = true;
        }
      }
    }
 }