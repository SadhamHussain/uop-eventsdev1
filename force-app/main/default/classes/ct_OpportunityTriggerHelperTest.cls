/**
 * @File Name          : ct_OpportunityTriggerHelperTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 7/2/2020, 8:09:46 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/18/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_OpportunityTriggerHelperTest {
  @isTest(SeeAllData=true)
  public static void createAsperatoPaymentForDirectDebitPaymentTest(){
     // first retrieve default EDA trigger handlers
     List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
     // Create our trigger handler using the constructor
     tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_OpportunityTriggerHandler_TDTM', 'Opportunity', 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate', 2.00));
     // Pass trigger handler config to set method for this test run
     hed.TDTM_Global_API.setTdtmConfig(tokens);

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    npe03__Recurring_Donation__c thisRecurring =  ct_TestDataFactory.createRecurringDonation(thisContact, null, 100.00);
    thisRecurring.npsp__PaymentMethod__c = ct_Constants.DIRECT_DEBIT_PAYMENT_METHOD;
    insert thisRecurring;
    List<Opportunity> thisOpportunity = [SELECT Id, Amount FROM Opportunity WHERE npe03__Recurring_Donation__c =:thisRecurring.Id];
    //insert thisOpportunity;
    System.debug('thisOpportunity '+ thisOpportunity);

    Test.startTest();
    List<npe01__OppPayment__c> thisGEMPayment = [SELECT Id FROM npe01__OppPayment__c WHERE npe01__Opportunity__c IN: thisOpportunity];
    // insert thisGEMPayment;
    System.debug('thisGEMPayment '+ thisGEMPayment);
    for(Opportunity thisOpp : thisOpportunity){
      thisOpp.StageName =ct_Constants.PLEDGE_OPPORTUNITY_STAGE;
    }
    update thisOpportunity;
    Test.stopTest();
    npe03__Recurring_Donation__c  thisDonation = [SELECT Id, npe03__Contact__c, npe03__Amount__c, npe03__Open_Ended_Status__c
                                                            FROM npe03__Recurring_Donation__c
                                                            WHERE npe03__Contact__c=:  thisContact.Id];
    System.assertEquals(thisContact.Id, thisDonation.npe03__Contact__c, 'Should relate the primary contact Id');
    System.assertEquals(thisOpportunity[0].Amount, thisDonation.npe03__Amount__c, 'Should return Opportunity amount');
    //System.assertEquals('Open', thisDonation.npe03__Open_Ended_Status__c, 'Should return Open');
    List<npe01__OppPayment__c> thisPayment = [SELECT Id, Asperato_Payment__c FROM npe01__OppPayment__c 
                                                            WHERE Id =:thisGEMPayment[0].Id
                                                            AND Asperato_Payment__c != null];
    System.assertEquals(1,thisPayment.size(), 'Should return 1');

    

  }
    
  @isTest(SeeAllData=true)
  public static void createAsperatoPaymentTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_OpportunityTriggerHandler_TDTM', 'Opportunity', 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate', 2.00));
    // Pass trigger handler config to set method for this test run
    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
      
    asp04__Authorisation__c thisAuthorisation = ct_TestDataFactory.createAuthorisation(thisContact);
    insert thisAuthorisation;
    npe03__Recurring_Donation__c thisRecurringDonation =  ct_TestDataFactory.createRecurringDonation(thisContact, thisAuthorisation.Id, 100.00);
    insert thisRecurringDonation;
      
    thisAuthorisation.asp04__Status__c = ct_Constants.AUTHORIZATION_INFORCE_STATUS;
    update thisAuthorisation;
      
    Opportunity thisOpportunity = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
    thisOpportunity.Amount = 200.00;
    thisOpportunity.npe03__Recurring_Donation__c = thisRecurringDonation.id;
    insert thisOpportunity;

    Project__c thisCreationFundProject = ct_TestDataFactory.createProject();
    insert thisCreationFundProject;

    npsp__General_Accounting_Unit__c thisGAU = ct_TestDataFactory.createGAURecord('Creation GAU');
    thisGAU.Project__c = thisCreationFundProject.Id;
    insert thisGAU;
   
    npsp__Allocation__c thisGAUAllocationOne = ct_TestDataFactory.createGAUAllocation(thisGAU.Id, thisOpportunity.Id, 200.00);
    insert thisGAUAllocationOne;
      
    
      
    Test.startTest();
    npe01__OppPayment__c thisGEMPayment =  ct_TestDataFactory.createGEMPayment(thisOpportunity, ct_Constants.CASH_PAYMENT_METHOD);
    thisGEMPayment.npe01__Paid__c       =  true;
    insert thisGEMPayment;

    Test.stopTest();
  }
    
  @isTest(SeeAllData = true)
  public static void sentEmailAlertForVolunteerTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_OpportunityTriggerHandler_TDTM', 'Opportunity', 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate', 2.00));
    // Pass trigger handler config to set method for this test run
    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    
    Contact queriedContact = [SELECT Id,AccountId FROM Contact WHERE Id =: thisContact.Id];

    Opportunity thisOpportunity = ct_TestDataFactory.createVolunteerOpportunity(thisContact);
    thisOpportunity.AccountId = queriedContact.AccountId;

    //thisOpportunity.EventbriteSync__EventbriteId__c = String.valueOf(System.now());
    //thisOpportunity.EventbriteSync__Buyer__c = thisContact.Id;

    Test.startTest();
      insert thisOpportunity; 
    Test.stopTest();
  }  

  @isTest(SeeAllData=true)
  public static void createAffiliationAndStatusUpdateTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_OpportunityTriggerHandler_TDTM', 'Opportunity', 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate', 2.00));
    // Pass trigger handler config to set method for this test run
    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    insert thisAccount;

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    Opportunity thisOpportunity = ct_TestDataFactory.createApplicationOpportunity();
    insert thisOpportunity; 
    thisOpportunity.Applicant__c = thisContact.Id;
    thisOpportunity.Course__c = thisAccount.Id;
    update thisOpportunity;
    Test.startTest();
    List<hed__Affiliation__c> thisAffiliationList = new List<hed__Affiliation__c>();

    thisAffiliationList= [SELECT Id, hed__Role__c, hed__Status__c, hed__Contact__c, hed__Account__c
                                                        FROM hed__Affiliation__c
                                                        WHERE hed__Contact__c =: thisContact.Id
                                                        AND hed__Account__c =:thisAccount.Id];

    System.assertEquals(thisAffiliationList[0].hed__Role__c, ct_Constants.AFFILIATION_ROLE_APPLICANT, 'Affiliation need to be in Applicant Role');
    System.assertEquals(thisAffiliationList[0].hed__Status__c, ct_Constants.AFFILIATION_STATUS_CURRENT, 'Affiliation need to be in Current Status');
    
    thisOpportunity.StageName = ct_Constants.OPPORTUNITY_STAGE_DECLINED_OFFER;
    update thisOpportunity;

    thisAffiliationList= [SELECT Id, hed__Role__c, hed__Status__c, hed__Contact__c, hed__Account__c
                                                        FROM hed__Affiliation__c
                                                        WHERE hed__Contact__c =: thisContact.Id
                                                        AND hed__Account__c =:thisAccount.Id];
    System.assertEquals(thisAffiliationList[0].hed__Status__c, ct_Constants.AFFILIATION_STATUS_FORMER, 'Affiliation need to be in Former Status once rejected');
    Test.stopTest();
  }  
  
  @isTest(SeeAllData=true)
  public static void updateDonorDetailsProspectDonation(){
    List<Contact> contactListToInsert = new List<Contact>();
    Contact thisContactOne       = ct_TestDataFactory.createContact();
    thisContactOne.RecordTypeId  = ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT;
    contactListToInsert.add(thisContactOne);
    Contact thisContactTwo       = ct_TestDataFactory.createContact();
    thisContactTwo.RecordTypeId  = ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT;
    contactListToInsert.add(thisContactTwo);
    insert contactListToInsert;

    List<Opportunity> opportunityListToInsert = new List<Opportunity>();
    Opportunity thisOpenOpportunity   = ct_TestDataFactory.createDonationPledgeOpportunity(contactListToInsert[0]);
    thisOpenOpportunity.StageName     = ct_Constants.PROSPECTING_OPPORTUNITY_STAGE;
    thisOpenOpportunity.Amount        = 5000;
    opportunityListToInsert.add(thisOpenOpportunity);

    Test.startTest();
    insert opportunityListToInsert;
    Test.stopTest();
    
    Contact principalDonor = [SELECT Id, Name, Donor_Type__c, Donor_Status__c FROM Contact WHERE Id =: contactListToInsert[0].Id];
    System.assertEquals(true, principalDonor.Donor_Status__c.contains(ct_Constants.CONTACT_DONOR_STATUS_PROSPECT), 'Since one open opportunity is present');
  }

  @isTest(SeeAllData=true)
  public static void updateDonorDetailsPledgeDonationTest(){

    Test.startTest();
    Contact thisContact       = ct_TestDataFactory.createContact();
    thisContact.RecordTypeId  = ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT;
    insert thisContact;

    Opportunity thisPledgeOpportunity   = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
    thisPledgeOpportunity.StageName     = ct_Constants.PLEDGE_OPPORTUNITY_STAGE;
    thisPledgeOpportunity.Amount        = 5000;
    thisPledgeOpportunity.RecordTypeId  = ct_Constants.OPPORTUNITY_RECORDTYPE_MAJOR_DONATION;
    insert thisPledgeOpportunity;

    Test.stopTest();
    Contact principalDonor = [SELECT Id, Name, Donor_Type__c, Donor_Status__c FROM Contact WHERE Id =: thisContact.Id];
    System.assertEquals(true, principalDonor.Donor_Status__c.contains(ct_Constants.CONTACT_DONOR_STATUS_PLEDGED), 'Since one Pledge opportunity is present');
  }

  @isTest(SeeAllData=true)
  public static void updateDonorDetailsPaidDonationTest(){

    Test.startTest();
    Contact thisContact       = ct_TestDataFactory.createContact();
    thisContact.RecordTypeId  = ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT;
    insert thisContact;

    Opportunity thisCurrentCloseWonOpportunity    = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
    thisCurrentCloseWonOpportunity.StageName      = ct_Constants.OPPORTUNITY_STAGE_CLOSED_WON;
    thisCurrentCloseWonOpportunity.Amount         = 12000;
    thisCurrentCloseWonOpportunity.RecordTypeId   = ct_Constants.OPPORTUNITY_RECORDTYPE_IN_KIND_GIFT;
    insert thisCurrentCloseWonOpportunity;
    
    Test.stopTest();
    Contact principalDonor = [SELECT Id, Name, Donor_Type__c, Donor_Status__c FROM Contact WHERE Id =: thisContact.Id];
    System.assertEquals(ct_Constants.CONTACT_DONOR_TYPE_PRINCIPAL_DONOR, principalDonor.Donor_Type__c, 'Since Current Opportunity amount is 12000');
    System.assertEquals(true, principalDonor.Donor_Status__c.contains(ct_Constants.CONTACT_DONOR_STATUS_CURRENT), 'Since one Closed Won opportunity is present within last 12 months');
  }

  @isTest(SeeAllData=true)
  public static void updateDonorDetailsLapsedDonationTest(){

    Test.startTest();
    Contact thisContact       = ct_TestDataFactory.createContact();
    thisContact.RecordTypeId  = ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT;
    insert thisContact;

    Opportunity thisLapsedCloseWonOpportunity   = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
    thisLapsedCloseWonOpportunity.StageName     = ct_Constants.OPPORTUNITY_STAGE_CLOSED_WON;
    thisLapsedCloseWonOpportunity.Amount        = 5000;
    thisLapsedCloseWonOpportunity.CloseDate     = System.today().addMonths(-13);
    insert thisLapsedCloseWonOpportunity;
    Test.stopTest();
    Contact lapsedDonor    = [SELECT Id, Name, Donor_Type__c, Donor_Status__c FROM Contact WHERE Id =: thisContact.Id];
    System.assertEquals(true, lapsedDonor.Donor_Status__c.contains(ct_Constants.CONTACT_DONOR_STATUS_LAPSED), 'Since one Closed Won opportunity is not within last 12 months');
  }



  @isTest(SeeAllData=true)
  public static void createAccountTeamMemberTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_OpportunityTriggerHandler_TDTM', 'Opportunity', 'BeforeInsert;BeforeUpdate;BeforeDelete;AfterInsert;AfterUpdate', 2.00));
    // Pass trigger handler config to set method for this test run
    hed.TDTM_Global_API.setTdtmConfig(tokens);
    User systemAdminUser = ct_TestDataFactory.createUser();
    insert systemAdminUser;
    System.runAs(systemAdminUser){

      Map<String,  Department_User__mdt> depUserMetadata = new Map<String,  Department_User__mdt>();
      Department_User__mdt thisDonationSetting    = new Department_User__mdt();
      thisDonationSetting.Departmental_User_Id__c = String.valueOf(systemAdminUser.Id);
      depUserMetadata.put(ct_Util.getRecordTypeDeveloperNameById(ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE), thisDonationSetting);

      ct_MetadataService.departmentalUsersSettingsMetaData = depUserMetadata;

      Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
      thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE;
      insert thisAccount;
      
      Contact thisContact   = ct_TestDataFactory.createContact();
      thisContact.AccountId = thisAccount.Id;
      insert thisContact;

      Opportunity thisOpportunity   = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
      thisOpportunity.Amount        = 5000;
      thisOpportunity.AccountId     = thisAccount.Id;
      insert thisOpportunity;

      Opportunity thisSecondOpportunity   = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
      thisSecondOpportunity.Amount        = 5000;
      thisSecondOpportunity.AccountId     = thisAccount.Id;
      Test.startTest();
      insert thisSecondOpportunity;
      List<AccountTeamMember> thisTeamList = [SELECT Id, AccountId 
                                              FROM AccountTeamMember 
                                              WHERE AccountId =: thisAccount.Id];
      System.assertEquals(thisTeamList.size(), 1, 'Since Only One Account Team Member will be inserted in trigger');
      delete thisSecondOpportunity;
      Test.stopTest();
    }
  }
}