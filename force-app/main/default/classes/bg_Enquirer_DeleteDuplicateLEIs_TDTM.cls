/*************************************************************************************
* bg_Enquirer_DeleteDuplicateLEIs_TDTM
*
* Enquirer Trigger Handler for deleting the duplicate LEIs while merging the enquirers
* Duplicates will be identified based on key combination of Enquirer__c,Level_of_Study__c and Year_of_Entry__c fields of Latest Enquiry Information. 
* After Delete
* 
* Author: Koteswararao Tangella
* Created: 26-06-2019
*************************************************************************************/

global class bg_Enquirer_DeleteDuplicateLEIs_TDTM extends hed.TDTM_Runnable 
{
    /*
    * Trigger Handler on AfterDelete for deleting duplicate LEIs
    */
    public override DmlWrapper run(List<SObject> newlist, List<SObject> oldlist, Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {
        
        if (triggerAction == Action.AfterDelete) 
        {
            List<Lead> leadRecords = oldlist;
            
            // call delete the LEI method defined in Utils Class
            bg_EnquirerUtils.deleteDuplicateLEIs(leadRecords);
        }

        return null;
    }
}