/****************************************************************************
 *  bg_ContactConsentManager_Test
 *
 * Test class for bg_ContactConsentManager
 * 
 *  Author: Ismail Basser - Brightgen
 *  Created: 01/08/2019
 * ************** Version history ***********************
 * CT, Rajesh : 01/05/2020 - Added Alumni Consent Preference fields
 *
***********************************************************************/
@isTest
public class bg_ContactConsentManager_Test {
    
    @testSetup static void setup() 
    {
        Campaign testCampaign = new Campaign(Name = 'Test Campaign 1');
        insert testCampaign;
        
        Account account1 = new Account(Name = 'Test Account 1');
        insert account1;
        
        Contact testContact                                       = new Contact();
        testContact.FirstName                                     = 'John';
        testContact.LastName                                      = 'Doe';
        testContact.accountId                                     = account1.Id;
        testContact.Phone_Consent__c                              = 'Yes';
        testContact.WhatsApp_Consent__c                           = 'No';
        testContact.Directmail_Consent__c                         = 'Yes';
        testContact.Latest_Consent_Change_Campaign__c             = testCampaign.Id;
        //Added by CT, Rajesh on 01/05/2020 to assign the values for Alumni Consent preferences
        testContact.Alumni_Do_not_Contact__c                      = true;
        testContact.Alumni_Do_not_Contact_Notes__c               = 'Out of City';
        testContact.Alumni_Do_not_Contact_Source__c               = 'Phone';
        testContact.Alumni_Do_Not_Contact_Effective_Date__c       = System.today();
        testContact.Alumni_Directmail_Consent__c                  = 'No';
        testContact.Alumni_Directmail_Consent_Source__c           = 'No';
        testContact.Alumni_Email_Consent__c                       = 'Yes';
        testContact.Alumni_Phone_Consent__c                       = 'No';
        testContact.Alumni_SMS_Consent__c                         = 'Yes';
        //Added by CT, Sahitha on 01/06/2020 to change the 'webform' values to 'online donation' 
        testContact.Alumni_Email_Consent_Source__c                = ct_Constants.CONSENT_ONLINE_DONATION_VALUE;
        testContact.Alumni_Phone_Consent_Source__c                = ct_Constants.CONSENT_ONLINE_DONATION_VALUE;
        testContact.Alumni_SMS_Consent_Source__c                  = ct_Constants.CONSENT_ONLINE_DONATION_VALUE;
        testContact.Alumni_Directmail_Consent_Effective_Date__c   = System.today();
        testContact.Alumni_Email_Consent_Effective_Date__c        = System.today();
        testContact.Alumni_Phone_Consent_Effective_Date__c        = System.today();
        testContact.Alumni_SMS_Consent_Effective_Date__c          = System.today();
        insert testContact;
    }
    
    static testMethod void createAndInsertConsentsForNewContactsTestSysAdminWeb() 
    {
        Test.startTest();
        User sysAdminUser = bg_UserTestUtils.createSystemAdminUser('testIb1');
        insert sysAdminUser;
        
        List<Consent__c> existingConsents = [SELECT Id FROM Consent__c];
        delete existingConsents;
        
        //Modified by CT, Rajesh on 01/05/2020 : Added Alumni Consent preferences fields in the query
        List<Contact> contactsList = [SELECT Id, 
                                        accountId, Phone_Consent__c, WhatsApp_Consent__c, Directmail_Consent__c, Latest_Consent_Change_Campaign__c,
                                        SMS_Consent__c, Latest_Consent_Change_Source__c, Alumni_Do_not_Contact__c, Alumni_Email_Consent_Notes__c, 
                                        Alumni_Do_not_Contact_Notes__c, Alumni_Do_not_Contact_Source__c, Alumni_Do_Not_Contact_Effective_Date__c, Alumni_Consent_End_Date__c, Alumni_Email_Consent_Source__c,
                                        Alumni_Directmail_Consent__c, Alumni_Directmail_Consent_Source__c, Alumni_Email_Consent__c, Alumni_Phone_Consent__c, 
                                        Alumni_SMS_Consent__c, Alumni_Phone_Consent_Source__c, Alumni_SMS_Consent_Source__c, 
                                        Alumni_Email_Consent_Effective_Date__c, Alumni_Directmail_Consent_Notes__c, Alumni_Directmail_Consent_Effective_Date__c, 
                                        Alumni_Phone_Consent_Notes__c, Alumni_Phone_Consent_Effective_Date__c, Alumni_SMS_Consent_Notes__c, Alumni_SMS_Consent_Effective_Date__c
                                        FROM Contact];
        
        
        System.runAs(sysAdminUser)
        {
        bg_ContactConsentManager.createAndInsertConsentsForNewContacts(contactsList);
        }
        Test.stopTest();
        //Modified by CT, Rajesh on 01/05/2020 : Added Alumni Consent preferences fields in the query
        List<Consent__c> consentList = [SELECT Id, 
                                            Campaign__c, Contact__c, Directmail_Consent_Prior__c, Directmail_Consent__c,
                                            Email_Consent_Prior__c, Phone_Consent__c, Phone_Consent_Prior__c, SMS_Consent__c,
                                            SMS_Consent_Prior__c, Whatsapp_Consent_Prior__c, Whatsapp_Consent__c, Change_Source__c,
                                            Alumni_Do_not_Contact__c, New_Alumni_Do_not_Contact__c, New_Alumni_Do_not_Contact_Notes__c,
                                            New_Alumni_Do_not_Contact_Source__c, New_Alumni_Do_Not_Contact_Effective_Date__c, New_Alumni_Consent_End_Date__c,
                                            New_Alumni_Email_Consent_Source__c, New_Alumni_Directmail_Consent__c, Alumni_Directmail_Consent__c, New_Alumni_Directmail_Consent_Source__c,
                                            New_Alumni_Email_Consent__c, Alumni_Email_Consent__c, Alumni_Phone_Consent__c, Alumni_SMS_Consent__c, 
                                            New_Alumni_Phone_Consent__c, New_Alumni_SMS_Consent__c, New_Alumni_Phone_Consent_Source__c, New_Alumni_SMS_Consent_Source__c
                                            FROM Consent__c];
        
        System.assertEquals(1, consentList.size());
        System.assertEquals(consentList[0].Contact__c, contactsList[0].Id);
        System.assertEquals(consentList[0].Campaign__c, contactsList[0].Latest_Consent_Change_Campaign__c);
        System.assertEquals(consentList[0].Directmail_Consent__c, 'Yes');
        System.assertEquals(consentList[0].Phone_Consent__c, 'Yes');
        System.assertEquals(consentList[0].Whatsapp_Consent__c, 'No');
        System.assertEquals(consentList[0].Change_Source__c, 'Webform');
        //Added by CT, Rajesh on 01/05/2020
        System.assertEquals(consentList[0].New_Alumni_Do_not_Contact__c, contactsList[0].Alumni_Do_not_Contact__c);
        System.assertEquals(consentList[0].New_Alumni_Do_not_Contact_Notes__c, contactsList[0].Alumni_Do_not_Contact_Notes__c);
        System.assertEquals(consentList[0].New_Alumni_Do_Not_Contact_Effective_Date__c, contactsList[0].Alumni_Do_Not_Contact_Effective_Date__c);
        System.assertEquals(consentList[0].New_Alumni_Directmail_Consent__c, contactsList[0].Alumni_Directmail_Consent__c);
        System.assertEquals(consentList[0].New_Alumni_Email_Consent__c, contactsList[0].Alumni_Email_Consent__c);
        System.assertEquals(consentList[0].New_Alumni_SMS_Consent__c, contactsList[0].Alumni_SMS_Consent__c);
    }
    
    static testMethod void createAndInsertConsentsForNewContactsTestSysAdminPC() 
    {   
        User sysAdminUser = bg_UserTestUtils.createSystemAdminUser('testIb1');
        insert sysAdminUser;
        
        Test.startTest();
        //Modified by CT, Rajesh on 01/05/2020 : Added Alumni Consent preferences fields in the query
        List<Contact> contactsList = [SELECT Id, 
                                        accountId, Phone_Consent__c, WhatsApp_Consent__c, Directmail_Consent__c, Latest_Consent_Change_Campaign__c,
                                        SMS_Consent__c, Latest_Consent_Change_Source__c, Alumni_Do_not_Contact__c, Alumni_Do_not_Contact_Notes__c, 
                                        Alumni_Do_not_Contact_Source__c, Alumni_Do_Not_Contact_Effective_Date__c, Alumni_Consent_End_Date__c, Alumni_Email_Consent_Source__c,
                                        Alumni_Directmail_Consent__c, Alumni_Directmail_Consent_Source__c, Alumni_Email_Consent__c, Alumni_Phone_Consent__c, 
                                        Alumni_SMS_Consent__c, Alumni_Phone_Consent_Source__c, Alumni_SMS_Consent_Source__c, Alumni_Email_Consent_Notes__c,
                                        Alumni_Email_Consent_Effective_Date__c, Alumni_Directmail_Consent_Notes__c, Alumni_Directmail_Consent_Effective_Date__c, 
                                        Alumni_Phone_Consent_Notes__c, Alumni_Phone_Consent_Effective_Date__c, Alumni_SMS_Consent_Notes__c, Alumni_SMS_Consent_Effective_Date__c
                                        FROM Contact
                                        WHERE FirstName = 'John'
                                        AND LastName = 'Doe'
                                        LIMIT 1];
        
        contactsList[0].Latest_Consent_Change_Source__c = 'Preference Centre';
        update contactsList;
              
        System.runAs(sysAdminUser)
        {
        bg_ContactConsentManager.createAndInsertConsentsForNewContacts(contactsList);
        }
        Test.stopTest(); 
        //Modified by CT, Rajesh on 01/05/2020 : Added Alumni Consent preferences fields in the query
        List<Consent__c> consentList = [SELECT Id, Campaign__c, Contact__c, Directmail_Consent_Prior__c, Directmail_Consent__c,
                                            Email_Consent_Prior__c, Phone_Consent__c, Phone_Consent_Prior__c, SMS_Consent__c,
                                            SMS_Consent_Prior__c, Whatsapp_Consent_Prior__c, Whatsapp_Consent__c, Change_Source__c,
                                            Alumni_Do_not_Contact__c, New_Alumni_Do_not_Contact__c, New_Alumni_Do_not_Contact_Notes__c,
                                            New_Alumni_Do_not_Contact_Source__c, New_Alumni_Do_Not_Contact_Effective_Date__c, New_Alumni_Consent_End_Date__c,
                                            New_Alumni_Email_Consent_Source__c, New_Alumni_Directmail_Consent__c, Alumni_Directmail_Consent__c, New_Alumni_Directmail_Consent_Source__c,
                                            New_Alumni_Email_Consent__c, Alumni_Email_Consent__c, Alumni_Phone_Consent__c, Alumni_SMS_Consent__c, 
                                            New_Alumni_Phone_Consent__c, New_Alumni_SMS_Consent__c, New_Alumni_Phone_Consent_Source__c, New_Alumni_SMS_Consent_Source__c
                                            FROM Consent__c];
        
        System.assertEquals(1, consentList.size());
        System.assertEquals(consentList[0].Contact__c, contactsList[0].Id);
        System.assertEquals(consentList[0].Campaign__c, contactsList[0].Latest_Consent_Change_Campaign__c);
        System.assertEquals(consentList[0].Directmail_Consent__c, 'Yes');
        System.assertEquals(consentList[0].Phone_Consent__c, 'Yes');
        System.assertEquals(consentList[0].Whatsapp_Consent__c, 'No');
        System.assertEquals(consentList[0].Change_Source__c, 'Preference Centre');
        //Added by CT, Rajesh on 01/05/2020 to assert the Alumni Consent Preferences fields
        System.assertEquals(consentList[0].New_Alumni_Do_not_Contact__c, contactsList[0].Alumni_Do_not_Contact__c);
        System.assertEquals(consentList[0].New_Alumni_Do_not_Contact_Notes__c, contactsList[0].Alumni_Do_not_Contact_Notes__c);
        System.assertEquals(consentList[0].New_Alumni_Do_Not_Contact_Effective_Date__c, contactsList[0].Alumni_Do_Not_Contact_Effective_Date__c);
        System.assertEquals(consentList[0].New_Alumni_Directmail_Consent__c, contactsList[0].Alumni_Directmail_Consent__c);
        System.assertEquals(consentList[0].New_Alumni_Email_Consent__c, contactsList[0].Alumni_Email_Consent__c);
        System.assertEquals(consentList[0].New_Alumni_SMS_Consent__c, contactsList[0].Alumni_SMS_Consent__c);
    }
    
    static testMethod void createAndInsertConsentsForNewContactsTestStandard() 
    {              
        List<Contact> contactsList = new List<Contact>();

        Test.startTest();
        
        User sysAdminUser = bg_UserTestUtils.createStandardUser('testIb2');
        insert sysAdminUser;
        //Modified by CT, Rajesh on 01/05/2020 : Assign Alumni_and_Advancement permission set to the standard user
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Alumni_and_Advancement'];
        insert new PermissionSetAssignment(AssigneeId = sysAdminUser.Id, PermissionSetId = ps.Id);
        Campaign testCampaign = [SELECT Id FROM Campaign WHERE Name = 'Test Campaign 1' LIMIT 1];
              
        System.runAs(sysAdminUser)
        {
            Account account1 = new Account(Name = 'Test Account 1');
            insert account1;
            Contact testContact                                       = new Contact();
            testContact.FirstName                                     = 'John';
            testContact.LastName                                      = 'Doe';
            testContact.accountId                                     = account1.Id;
            testContact.Phone_Consent__c                              = 'Yes';
            testContact.WhatsApp_Consent__c                           = 'No';
            testContact.Directmail_Consent__c                         = 'Yes';
            testContact.Latest_Consent_Change_Campaign__c             = testCampaign.Id;
            //Added by CT, Rajesh on 01/05/2020 to assign the values for Alumni Consent preferences
            testContact.Alumni_Do_not_Contact__c                      = true;
            testContact.Alumni_Do_not_Contact_Notes__c               = 'Out of City';
            testContact.Alumni_Do_not_Contact_Source__c               = 'Phone';
            testContact.Alumni_Do_Not_Contact_Effective_Date__c       = System.today();
            testContact.Alumni_Directmail_Consent__c                  = 'No';
            testContact.Alumni_Directmail_Consent_Source__c           = 'No';
            testContact.Alumni_Email_Consent__c                       = 'Yes';
            testContact.Alumni_Phone_Consent__c                       = 'No';
            testContact.Alumni_SMS_Consent__c                         = 'Yes';
            //Added by CT, Sahitha on 01/06/2020 to change the 'webform' values to 'online donation'
            testContact.Alumni_Email_Consent_Source__c                = ct_Constants.CONSENT_ONLINE_DONATION_VALUE;
            testContact.Alumni_Phone_Consent_Source__c                = ct_Constants.CONSENT_ONLINE_DONATION_VALUE;
            testContact.Alumni_SMS_Consent_Source__c                  = ct_Constants.CONSENT_ONLINE_DONATION_VALUE;
            testContact.Alumni_Directmail_Consent_Effective_Date__c   = System.today();
            testContact.Alumni_Email_Consent_Effective_Date__c        = System.today();
            testContact.Alumni_Phone_Consent_Effective_Date__c        = System.today();
            testContact.Alumni_SMS_Consent_Effective_Date__c          = System.today();
            contactsList.add(testContact);
            insert contactsList;
            bg_ContactConsentManager.createAndInsertConsentsForNewContacts(contactsList);
        }
        //Modified by CT, Rajesh on 01/05/2020 : Added Alumni Consent preferences fields in the query
        List<Consent__c> consentList = [SELECT Id, Campaign__c, Contact__c, Directmail_Consent_Prior__c, Directmail_Consent__c,
                                            Email_Consent_Prior__c, Phone_Consent__c, Phone_Consent_Prior__c, SMS_Consent__c,
                                            SMS_Consent_Prior__c, Whatsapp_Consent_Prior__c, Whatsapp_Consent__c, Change_Source__c,
                                            Alumni_Do_not_Contact__c, New_Alumni_Do_not_Contact__c, New_Alumni_Do_not_Contact_Notes__c,
                                            New_Alumni_Do_not_Contact_Source__c, New_Alumni_Do_Not_Contact_Effective_Date__c, New_Alumni_Consent_End_Date__c,
                                            New_Alumni_Email_Consent_Source__c, New_Alumni_Directmail_Consent__c, Alumni_Directmail_Consent__c, New_Alumni_Directmail_Consent_Source__c,
                                            New_Alumni_Email_Consent__c, Alumni_Email_Consent__c, Alumni_Phone_Consent__c, Alumni_SMS_Consent__c, 
                                            New_Alumni_Phone_Consent__c, New_Alumni_SMS_Consent__c, New_Alumni_Phone_Consent_Source__c, New_Alumni_SMS_Consent_Source__c
                                            FROM Consent__c];
        
        System.assertEquals(1, consentList.size());
        System.assertEquals(consentList[0].Contact__c, contactsList[0].Id);
        System.assertEquals(consentList[0].Campaign__c, contactsList[0].Latest_Consent_Change_Campaign__c);
        System.assertEquals(consentList[0].Directmail_Consent__c, 'Yes');
        System.assertEquals(consentList[0].Phone_Consent__c, 'Yes');
        System.assertEquals(consentList[0].Whatsapp_Consent__c, 'No');
        System.assertEquals(consentList[0].Change_Source__c, 'Internal User');
        //Added by CT, Rajesh on 01/05/2020 to assert the Alumni Consent Preferences fields
        System.assertEquals(consentList[0].New_Alumni_Do_not_Contact__c, contactsList[0].Alumni_Do_not_Contact__c);
        System.assertEquals(consentList[0].New_Alumni_Do_not_Contact_Notes__c, contactsList[0].Alumni_Do_not_Contact_Notes__c);
        System.assertEquals(consentList[0].New_Alumni_Do_Not_Contact_Effective_Date__c, contactsList[0].Alumni_Do_Not_Contact_Effective_Date__c);
        System.assertEquals(consentList[0].New_Alumni_Directmail_Consent__c, contactsList[0].Alumni_Directmail_Consent__c);
        System.assertEquals(consentList[0].New_Alumni_Email_Consent__c, contactsList[0].Alumni_Email_Consent__c);
        System.assertEquals(consentList[0].New_Alumni_SMS_Consent__c, contactsList[0].Alumni_SMS_Consent__c);
        Test.stopTest();
    }
}