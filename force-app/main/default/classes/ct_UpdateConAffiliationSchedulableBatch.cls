/**
 * @File Name          : ct_UpdateConAffiliationSchedulableBatch.cls
 * @Description        : Batch Class to Create and process Academic Affiliation for Exiting Contact
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 6/30/2020, 1:57:22 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/18/2020   Creation Admin     Initial Version
**/
global class ct_UpdateConAffiliationSchedulableBatch implements Schedulable, Database.Batchable<sObject>,Database.Stateful {
  //Set of String to store all Closed Lost Stage Name
  public static Set<String> closedLostStageName = new Set<String>{ct_Constants.OPPORTUNITY_STAGE_CLOSED_LOST,
                                                                  ct_Constants.OPPORTUNITY_STAGE_DECLINED_OFFER,
                                                                  ct_Constants.OPPORTUNITY_STAGE_NOT_OFFER,
                                                                  ct_Constants.OPPORTUNITY_STAGE_WITHDRAWN};

  public static Set<String> openStageName = new Set<String>{ct_Constants.OPPORTUNITY_STAGE_APPLIED,
                                                                  ct_Constants.OPPORTUNITY_STAGE_OFFERED,
                                                                  ct_Constants.OPPORTUNITY_STAGE_DEFERRED,
                                                                  ct_Constants.OPPORTUNITY_STAGE_OFFER_ACCEPTED};

/**
* @param Database.BatchableContext BC 
* @return List<Opportunity> - return list of Opportunity Records
**/
global Database.QueryLocator start(Database.BatchableContext BC) {
  String batchSize = System.Label.ct_Update_Contact_Academic_Affiliation_Batch_Size;
  //Get Application Opportunity records for which Affiliation needs to be created where Course Id should be populated and
  //Applicant Id Should be populated and exclude the contacts that were created from SITS Database using ThankQ External Ref fields
  String queryString = 'SELECT Id, ThankQ_External_Ref__c,'+
                        '(SELECT Id, Name, Applicant__c, Course__c, StageName,Course__r.RecordTypeId, RecordTypeId, CloseDate'+
                        ' FROM Opportunities__r'+
                        ' WHERE RecordTypeId ='+'\''+ct_Constants.OPPORTUNITY_RECORDTYPE_COURSE_ADMISSION+'\' '+
                        ' AND Applicant__c != null '+
                        ' AND Course__c != null '+
                        ' AND Course__r.RecordTypeId ='+'\''+ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM+'\''+')'+
                        ' FROM Contact WHERE ThankQ_External_Ref__c = null';
                                 
  if(Test.isRunningTest()){
    queryString = queryString + ' ORDER BY CreatedDate DESC LIMIT 1';
  }
  else{
    if(batchSize != null 
      && batchSize.toUpperCase() != 'ALL'){
        queryString = queryString +' LIMIT '+batchSize;
      }
  }
  return Database.getQueryLocator(queryString);
}

global void execute(Database.BatchableContext BC, List<Contact> scope){
  Set<Id> processedAffiliation = new Set<Id>();
  //Set of Applicant Ids
  Set<Id> applicantIds = new Set<Id>();
  //Set of Course Ids
  Set<Id> courseIds     = new Set<Id>();
  //Set of New Affiliations created for existing Closed Enrolled Opportunities
  Set<String> newClosedEnrolledAffiliations = new Set<String>();
  //Map to store Program Enrollments Records Based on Applicant Id and Course Id with
  //Key Structure => ApplicantId_CourseId
  Map<String, hed__Program_Enrollment__c> existingProgramEnrollmentMap = new Map<String, hed__Program_Enrollment__c>(); 
  //Map to store Affiliation Records Based on Applicant Id and Course Id with
  //Key Structure => ApplicantId_CourseId
  Map<String, List<hed__Affiliation__c>> existingAffiliationMap         = new Map<String, List<hed__Affiliation__c>>(); 
  List<hed__Program_Enrollment__c> programEnrollmentsToUpdate          = new List<hed__Program_Enrollment__c>();
  Map<String, hed__Affiliation__c> affiliationsToUpsert                = new Map<String, hed__Affiliation__c>();

  //getApplicantAndCourseIds(scope, applicantIds, courseIds);
  getExistingProgramEnrollMent(existingProgramEnrollmentMap, scope);
  getExistingAffiliations(existingAffiliationMap, scope);

  //Contact record Id for which UOP Academic Rollup 
  Set<Id> applicantIdToProcess                   = new Set<Id>();
  String uniqueMapKey;
  Boolean isClosedEnrolled = false;

  for(Contact thisContact : scope){

  for(Opportunity thisApplicationOpp : thisContact.Opportunities__r){
    hed__Affiliation__c thisAffiliation;
    uniqueMapKey = String.valueOf(thisApplicationOpp.Applicant__c)+'_'+String.valueOf(thisApplicationOpp.Course__c);
    if(!existingAffiliationMap.isEmpty()
      && existingAffiliationMap.containsKey(uniqueMapKey)
      && existingAffiliationMap.get(uniqueMapKey).size() > 0
      && !processedAffiliation.contains(existingAffiliationMap.get(uniqueMapKey)[0].Id)){
        thisAffiliation = existingAffiliationMap.get(uniqueMapKey)[0];
        processedAffiliation.add(thisAffiliation.Id);
        processExistingAffiliation(affiliationsToUpsert, thisApplicationOpp, thisAffiliation, programEnrollmentsToUpdate, existingProgramEnrollmentMap);
        existingAffiliationMap.get(uniqueMapKey).remove(0);
        applicantIdToProcess.add(thisApplicationOpp.Applicant__c);
    }
    else{
      thisAffiliation                        = new hed__Affiliation__c();
      thisAffiliation.hed__Contact__c        = thisApplicationOpp.Applicant__c;
      thisAffiliation.hed__Account__c        = thisApplicationOpp.Course__c;
      thisAffiliation.hed__Role__c           = ct_Constants.AFFILIATION_ROLE_APPLICANT;
      thisAffiliation.hed__StartDate__c      = thisApplicationOpp.CloseDate;
      //The status for the affiliation record created for Open Application is current
      if(openStageName.contains(thisApplicationOpp.StageName)){
        thisAffiliation.hed__Status__c  = ct_Constants.AFFILIATION_STATUS_CURRENT;
      }
      //The status for the affiliation record created for Closed Lost Application is former
      else if(closedLostStageName.contains(thisApplicationOpp.StageName)){
        thisAffiliation.hed__Status__c  = ct_Constants.AFFILIATION_STATUS_FORMER;
      }

      //The Status and role for the affiliation record for Close - Enrolled Application is Current, Student
      else if(thisApplicationOpp.StageName == ct_Constants.OPPORTUNITY_STAGE_CLOSED_ENROLLED
      && thisAffiliation.hed__Status__c != ct_Constants.AFFILIATION_STATUS_CURRENT
      && thisAffiliation.hed__Role__c != ct_Constants.AFFILIATION_ROLE_STUDENT){
        thisAffiliation.hed__Status__c    = ct_Constants.AFFILIATION_STATUS_CURRENT;
        thisAffiliation.hed__Role__c      = ct_Constants.AFFILIATION_ROLE_STUDENT;

        if(!existingProgramEnrollmentMap.isEmpty()
          && existingProgramEnrollmentMap.containsKey(uniqueMapKey)){
            affiliationsToUpsert.put(uniqueMapKey+'_'+existingProgramEnrollmentMap.get(uniqueMapKey).Id, thisAffiliation);
            newClosedEnrolledAffiliations.add(uniqueMapKey);
            isClosedEnrolled = true;
        }
        
      }
      if(isClosedEnrolled){
        isClosedEnrolled = false;
      }
      else {
        affiliationsToUpsert.put(String.valueOf(thisAffiliation.hed__Contact__c)+'_'+String.valueOf(thisAffiliation.hed__Account__c)+'_'+String.valueOf(thisApplicationOpp.Id), thisAffiliation);
      }
      applicantIdToProcess.add(thisApplicationOpp.Applicant__c);
    }    
  }
}

  try{
    upsert affiliationsToUpsert.values();
    if(!newClosedEnrolledAffiliations.isEmpty()){
      for(String thisUniqueKey : newClosedEnrolledAffiliations){
        if(!existingProgramEnrollmentMap.isEmpty()
          && existingProgramEnrollmentMap.containsKey(thisUniqueKey)
          && affiliationsToUpsert.containsKey(thisUniqueKey+'_'+existingProgramEnrollmentMap.get(thisUniqueKey).Id)){
            populateAffiliationId(affiliationsToUpsert.get(thisUniqueKey+'_'+existingProgramEnrollmentMap.get(thisUniqueKey).Id), programEnrollmentsToUpdate, existingProgramEnrollmentMap.get(thisUniqueKey));
        }
      }
    }
    
    if(!programEnrollmentsToUpdate.isEmpty()){
      update programEnrollmentsToUpdate;
    }
    ct_hed_AffiliationTriggerHelper.processContactAcademicAffiliationRollup(applicantIdToProcess);
  }
  catch(Exception ex){
    ct_Logger.logMessage(ex, 'Update Contact Affiliation Batch');
    throw ex;
  }
}

/**
* @description Method Which gets invoked after the batch has completed
* @param BC BatchableContext
* @return void 
**/
global void finish(Database.BatchableContext BC) {
 
}

/**
* @description Method Which gets Applicant Id and Course Id from Application Opportunity List
* @param opportunitList Application Opportunity List
* @param applicantIdSet Set to store Applicant Ids
* @param courseIdSet Set to store Course Ids
* @return void 
**/
private static void getApplicantAndCourseIds(List<Opportunity> opportunitList, Set<Id> applicantIdSet, Set<Id> courseIdSet){
  for(Opportunity thisApplicationOpp : opportunitList){
    applicantIdSet.add(thisApplicationOpp.Applicant__c);
    courseIdSet.add(thisApplicationOpp.Course__c);
  }
}

/**
* @description Method to get Existing Program Enrollment records based on Applicant Id and Course Id
* @param programEnrollmentMap Map to store Program Enrollments Records Based on Applicant Id and Course Id
* @param applicantIdSet Set to store Applicant Ids
* @param courseIdSet Set to store Course Ids
* @return void 
**/
private static void getExistingProgramEnrollMent(Map<String, hed__Program_Enrollment__c> programEnrollmentMap, List<Contact> contactScope){
  for(hed__Program_Enrollment__c thisProgramEnrollment : [SELECT Id, hed__Contact__c, hed__Account__c, hed__Admission_Date__c,hed__Affiliation__c
                                                            FROM hed__Program_Enrollment__c
                                                            WHERE hed__Affiliation__c = null
                                                            AND hed__Account__c != null
                                                            AND hed__Contact__c IN :contactScope]){
    programEnrollmentMap.put(String.valueOf(thisProgramEnrollment.hed__Contact__c)+'_'+String.valueOf(thisProgramEnrollment.hed__Account__c), thisProgramEnrollment);
  }
}

/**
* @description Method to get Existing Affiliations Records based on Applicant Id and Course Id 
* @param affiliationMap Map to store Affiliations Records Records Based on Applicant Id and Course Id
* @param applicantIdSet Set to store Applicant Ids
* @param courseIdSet Set to store Course Ids
* @return void 
**/
private static void getExistingAffiliations(Map<String, List<hed__Affiliation__c>> affiliationMap, List<Contact> contactScope){
  for(hed__Affiliation__c thisAffiliation : [SELECT Id, hed__Status__c, hed__Role__c, hed__Contact__c, hed__Account__c
                                              FROM hed__Affiliation__c
                                              WHERE hed__Contact__c IN :contactScope
                                              AND hed__Account__c != null]){
    if(!affiliationMap.containsKey(String.valueOf(thisAffiliation.hed__Contact__c)+'_'+String.valueOf(thisAffiliation.hed__Account__c))){
      affiliationMap.put(String.valueOf(thisAffiliation.hed__Contact__c)+'_'+String.valueOf(thisAffiliation.hed__Account__c), new List<hed__Affiliation__c>());
    }
    affiliationMap.get(String.valueOf(thisAffiliation.hed__Contact__c)+'_'+String.valueOf(thisAffiliation.hed__Account__c)).add(thisAffiliation);
  }
}

/**
* @description Method to Process Existing Affiliations Based on Opportunity Stage 
* @param affiliationsListToUpsert Affiliations records to be updated
* @param thisOpportunity Opportunity record based on which Affiliation needs to be processed
* @param thisAffiliationToProcess existing affiliation records to be processed
* @return void 
**/
private static void processExistingAffiliation(Map<String, hed__Affiliation__c> affiliationsListToUpsert, Opportunity thisOpportunity, hed__Affiliation__c thisAffiliationToProcess, List<hed__Program_Enrollment__c> programEnrollmentsListToUpdate,  Map<String, hed__Program_Enrollment__c> programEnrollmentMap){
  //The status for the affiliations for Open Application is current
  if(openStageName.contains(thisOpportunity.StageName)
    && (thisAffiliationToProcess.hed__Status__c != ct_Constants.AFFILIATION_STATUS_CURRENT
      || thisAffiliationToProcess.hed__Role__c != ct_Constants.AFFILIATION_ROLE_APPLICANT)){
    thisAffiliationToProcess.hed__Status__c   = ct_Constants.AFFILIATION_STATUS_CURRENT;
    thisAffiliationToProcess.hed__Role__c     = ct_Constants.AFFILIATION_ROLE_APPLICANT;
    affiliationsListToUpsert.put(String.valueOf(thisAffiliationToProcess.hed__Contact__c)+'_'+String.valueOf(thisAffiliationToProcess.hed__Account__c)+'_'+String.valueOf(thisOpportunity.Id), thisAffiliationToProcess);
  }

  //The status for the affiliations for Closed Lost Application is former
  else if(closedLostStageName.contains(thisOpportunity.StageName)
    && (thisAffiliationToProcess.hed__Status__c != ct_Constants.AFFILIATION_STATUS_FORMER
    || thisAffiliationToProcess.hed__Role__c != ct_Constants.AFFILIATION_ROLE_APPLICANT)){
      thisAffiliationToProcess.hed__Status__c   = ct_Constants.AFFILIATION_STATUS_FORMER;
    thisAffiliationToProcess.hed__Role__c       = ct_Constants.AFFILIATION_ROLE_APPLICANT;
        affiliationsListToUpsert.put(String.valueOf(thisAffiliationToProcess.hed__Contact__c)+'_'+String.valueOf(thisAffiliationToProcess.hed__Account__c)+'_'+String.valueOf(thisOpportunity.Id), thisAffiliationToProcess);
  }

  //The Status and role for the affiliation record for Close - Enrolled Application is Current, Student
  else if(thisOpportunity.StageName == ct_Constants.OPPORTUNITY_STAGE_CLOSED_ENROLLED
    &&(thisAffiliationToProcess.hed__Status__c != ct_Constants.AFFILIATION_STATUS_CURRENT
      || thisAffiliationToProcess.hed__Role__c != ct_Constants.AFFILIATION_ROLE_STUDENT)){
      thisAffiliationToProcess.hed__Status__c    = ct_Constants.AFFILIATION_STATUS_CURRENT;
      thisAffiliationToProcess.hed__Role__c      = ct_Constants.AFFILIATION_ROLE_STUDENT;
          affiliationsListToUpsert.put(String.valueOf(thisAffiliationToProcess.hed__Contact__c)+'_'+String.valueOf(thisAffiliationToProcess.hed__Account__c)+'_'+String.valueOf(thisOpportunity.Id), thisAffiliationToProcess);
      //Populate Affiliation Id in Existing Program Enrollment
      if(!programEnrollmentMap.isEmpty()
        && programEnrollmentMap.containsKey(String.valueOf(thisOpportunity.Applicant__c)+'_'+String.valueOf(thisOpportunity.Course__c))){
          populateAffiliationId(thisAffiliationToProcess, programEnrollmentsListToUpdate, programEnrollmentMap.get(String.valueOf(thisOpportunity.Applicant__c)+'_'+String.valueOf(thisOpportunity.Course__c)));
      }
  }
}

/**
* @description Method to get Existing Affiliations Records based on Applicant Id and Course Id 
* @param thisAffiliationToProcess Affiliation Record
* @param programEnrollmentsListToUpdate List of program enrollments to be updated
* @param thisprogramEnrollment Existing program Enrollment 
* @return void 
**/
private static void populateAffiliationId(hed__Affiliation__c thisAffiliationToProcess, List<hed__Program_Enrollment__c> programEnrollmentsListToUpdate, hed__Program_Enrollment__c thisprogramEnrollment){
  thisProgramEnrollment.hed__Affiliation__c = thisAffiliationToProcess.Id;
  programEnrollmentsListToUpdate.add(thisProgramEnrollment);
}

 /**
  * @description Execute method which gets invoked when class is scheduled
  * @param SC SchedulableContext
  * @return void 
  **/
  public void execute(SchedulableContext SC) {
    ct_UpdateConAffiliationSchedulableBatch batch = new ct_UpdateConAffiliationSchedulableBatch();
    Database.executebatch(batch, 200);
  }
}