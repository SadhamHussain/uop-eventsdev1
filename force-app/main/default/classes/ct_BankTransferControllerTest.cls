/**
 * @File Name          : ct_BankTransferControllerTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 30/4/2020, 3:47:30 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/4/2020   Creation Admin     Initial Version
**/
@IsTest
public class ct_BankTransferControllerTest {

  @IsTest(SeeAllData=true)
  static void testCreateAttachmentForPayment(){

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;

    Opportunity thisOpportunity = ct_TestDataFactory.createVolunteerOpportunity(thisContact);
    thisOpportunity.StageName = 'Identification';
    thisOpportunity.RecordTypeId = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Major_Donation');
    insert thisOpportunity;

    Test.startTest();
    npe01__OppPayment__c thisGEMPayment   =  ct_TestDataFactory.createGEMPayment(thisOpportunity, ct_Constants.CASH_PAYMENT_METHOD);
    thisGEMPayment.npe01__Payment_Date__c = System.Today();
    thisGEMPayment.npe01__Scheduled_Date__c  = System.Today();
    insert thisGEMPayment;
    
    PageReference pageRef = Page.ct_BankTransferDownloaderPage;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('id', thisGEMPayment.id);
    pageRef.getParameters().put('attachment', 'true');
    ApexPages.StandardController stc = new ApexPages.StandardController(thisGEMPayment);

    ct_BankTransferController paymentController = new ct_BankTransferController(stc);
    paymentController.addPDFAttachment();
    Test.stopTest();
  }
}