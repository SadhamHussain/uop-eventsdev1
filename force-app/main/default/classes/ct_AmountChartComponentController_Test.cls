/**
 * @File Name          : ct_AmountChartComponentController_Test.cls
 * @Author             : Rajeshkumar  - Creation Technology Solutions
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 17/4/2020, 6:19:12 pm
 * **/
@isTest
public with sharing class ct_AmountChartComponentController_Test {
    /**
    * @description to test chart value contruction 
    * @author Creation Admin | 17/4/2020 
    * @return void 
    **/
    @isTest(SeeAllData=true)
    public static void getLineChartMapTest(){
        Contact thisContact = ct_TestDataFactory.createContact();
        insert thisContact;

        Opportunity thisOpportunity = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
        insert thisOpportunity;

        Test.startTest();
        Opportunity opportunityToUpdate = [SELECT 
                                                Id, 
                                                Amount 
                                            FROM Opportunity 
                                            WHERE Id = :thisOpportunity.Id];
        opportunityToUpdate.Amount = 200;
        update opportunityToUpdate;

        ct_AmountChartComponentController.getLineChartMap(opportunityToUpdate.Id);
        ct_AmountChartComponentController.constructLineChartValue(System.now(), '200', '100', System.now().addDays(-3));
        Test.stopTest();
     }
}