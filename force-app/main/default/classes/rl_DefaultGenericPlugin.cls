/*
* Generic plug-in for Duplicate Check
*  
* Vendor documentation can be found here: https://support.duplicatecheck.com/development-guide/apex-plugin-generic
*
* The customisation provided by this class minimises the risk of the correct contact information being lost during a merge by Duplicate Check,
* by deduplicating contact details and storing them in separate fields (2 for each type). See comments in code for more detail.
*
* With the Merge Set Field plugin the field selection of a merge can be altered or even custom values can be set for individual fields.
*
* Created By: Rob Liesicke (UoP)
* Created Date: 11/11/2019
*
* Changes:
*
* 17/12/2019  Added code in the direct processing override to move related objects to selected contact
*
* 19/12/2019  Removed code that handles direct processing as Convert plugin is now running without it
*
*/


// Implement the duplicate check plug-in inteface
global class rl_DefaultGenericPlugin implements dupcheck.dc3PluginInterface {
    
    
    // Implement the events for Merge Set & Direct Processing Plugins
    private static Set<String> implementedEvents = new Set<String>{'MERGE_SET_FIELD'};
        
        global Boolean isAvailable(dupcheck.dc3Plugin.PluginEventType eventType) {
            return rl_DefaultGenericPlugin.implementedEvents.contains(eventType.name());
        }
    
    // Method that decides what code to run based on event type
    global Object execute(dupcheck.dc3Plugin.PluginEventType eventType, Object eventData) {
        
        
        switch on eventType {
            
            // if event type is for merge set plugin run custom code and return
            when MERGE_SET_FIELD {
                return this.mergeSetField((dupcheck.dc3PluginModel.MergeSetFieldInput) eventData);
            }
            
            // ..otherwise return null
            when else {
                return null;
            }
        }
    }
    
    
    /*
     * This method takes the Merge Set Field Input, runs custom code to process contact information, 
     * and then returns the updated state as Merge Set Field Output
     */
    
    public dupcheck.dc3PluginModel.MergeSetFieldOutput mergeSetField(dupcheck.dc3PluginModel.MergeSetFieldInput input) {
        
        dupcheck.dc3PluginModel.MergeSetFieldOutput output = new dupcheck.dc3PluginModel.MergeSetFieldOutput(input);
        
        // Check if the input is related to a lead record
        if (input.objectPrefix == '00Q') {
            
            // Convert list of sobjects held in the input vaiarble, into a list of Leads
            List<Lead> allLeads = input.objectDataList;
            
            // Create List to store records (with master record at top)
            List<Lead> sortedLeads = new List<Lead>();
            
            // Get master records from objectDataList and add to sortedLeads list
            for(Lead lead : allLeads)
            {
                if (lead.Id == input.masterRecordId)
                {
                    sortedLeads.add(lead);
                }
            }
            
            // Get remaining records from objectDataList and add to sortedLeads list
            for(Lead lead : allLeads)
            {
                if (lead.Id != input.masterRecordId)
                {
                    sortedLeads.add(lead);
                }
            } 
            
            // Create a list to store email addressess and another to store phone numbers
            List<String> emailAddresses = new List<String>();
            List<String> phoneNumbers = new List<String>();
            
            // For every lead in the list of Leads..
            for(Lead lead : sortedLeads)
            {
                // ..add all email addresses to a list of email addresses
                emailAddresses.add(lead.Email);
                emailAddresses.add(lead.Email_Alternative__c);
                
                // ..and add all phone numbers to a list of phone numbers
                phoneNumbers.add(lead.Phone);
                phoneNumbers.add(lead.MobilePhone);
                phoneNumbers.add(lead.Phone_Alternative__c);
                phoneNumbers.add(lead.Mobile_Alternative__c);            
            }
            
            // Call method to process emails and return in custom wrapper class
            emailWrapper processedEmails = processEmails(emailAddresses);
            
            // Call method to process phone numbers and return in custom wrapper class
            phoneWrapper processedPhones = processPhones(phoneNumbers);
            
            
            // Loop through the input's map of fields, and when it gets to the email field, set to the email variable set above.         
            for (String fieldName : input.fieldSourceMap.keySet()) {
                
                if (fieldName == 'Email') {
                    // Set a custom value for the email field. Custom is required to ensure it is set (see documentation for my info)
                    output.fieldSourceMap.put(fieldName, 'custom');
                    output.customValueMap.put(fieldName, processedEmails.email);
                } 
                
                // Do the same for email alternative
                else if (fieldName == 'Email_Alternative__c') {
                    // Set a custom value for the email field. Custom is required to ensure it is set (see documentation for my info)
                    output.fieldSourceMap.put(fieldName, 'custom');
                    output.customValueMap.put(fieldName, processedEmails.emailAlt);
                } 
                
                // Do the same for phone
                else if (fieldName == 'Phone') {
                    // Set a custom value for the email field. Custom is required to ensure it is set (see documentation for my info)
                    output.fieldSourceMap.put(fieldName, 'custom');
                    output.customValueMap.put(fieldName, processedPhones.phone);
                } 
                
                // Do the same for mobile phone
                else if (fieldName == 'MobilePhone') {
                    // Set a custom value for the email field. Custom is required to ensure it is set (see documentation for my info)
                    output.fieldSourceMap.put(fieldName, 'custom');
                    output.customValueMap.put(fieldName, processedPhones.mobile);
                }   
                
                // Do the same for phone alternative
                else if (fieldName == 'Phone_Alternative__c') {
                    // Set a custom value for the email field. Custom is required to ensure it is set (see documentation for my info)
                    output.fieldSourceMap.put(fieldName, 'custom');
                    output.customValueMap.put(fieldName, processedPhones.phoneAlt);
                } 
                
                // Do the same for mobile alternative
                else if (fieldName == 'Mobile_Alternative__c') {
                    // Set a custom value for the email field. Custom is required to ensure it is set (see documentation for my info)
                    output.fieldSourceMap.put(fieldName, 'custom');
                    output.customValueMap.put(fieldName, processedPhones.mobileAlt);
                }                                                
            }
            
        }
        
        // Return the amended Merge Field Set
        return output;
    }
    
    
   
    
   /*
    * This method takes a list of phone numbers, processes them, de-duplicates them, and returns a wrapper
    * class containing up to 4 phone numbers
    */    
    
    private static phoneWrapper processPhones( List<String> phoneNumbers )
        
    {
        // Dedupe list of email addresses with use of Set
        Set<String> tempSet = new Set<String>();
        
        // List to store list of unique phone numbers and another for unique mobile numbers
        List<String> uniquePhoneNumbers = new List<String>();
        List<String> uniqueMobileNumbers = new List<String>();
        
        // Loop through every phone number
        for (String phoneNo : phoneNumbers)
        {
            
            if (phoneNo != null)
            {
                
                // Remove spaces, brackets, dashes, commas, full stops, replace o's with zeros
                phoneNo = phoneNo.replaceAll( '\\s+', '');
                phoneNo = phoneNo.replaceAll( 'o', '0');
                phoneNo = phoneNo.replaceAll( 'O', '0');
                phoneNo = phoneNo.replaceAll( '\\(', '');
                phoneNo = phoneNo.replaceAll( '\\)', '');
                phoneNo = phoneNo.replaceAll( '\\-', '');
                phoneNo = phoneNo.replaceAll( '\\,', '');
                phoneNo = phoneNo.replaceAll( '\\.', '');
                
                // If number starts with 00, replace start with + and remove first 2 digits 
                if (phoneNo.left(2) == '00') { phoneNo = '+' + phoneNo.right( (phoneNo.length() - 2) ); }
                
                // If number starts with 7, replace start with +44 
                // Business descision has been made to risk this change despite number potentially not being UK
                if (phoneNo.left(1) == '7') { phoneNo = '+44' + phoneNo; }
                
                // If number starts with 07, replace start with +44 and remove first digit
                if (phoneNo.left(2) == '07') { phoneNo = '+44' + phoneNo.right( (phoneNo.length() - 1) ); }            
                
                // If number starts with 447, prefix number with +
                if (phoneNo.left(3) == '447') { phoneNo = '+' + phoneNo; }   
                
                // If number starts with +4407, replace start with +447
                if( phoneNo.left(5) == '+4407') { phoneNo = '+44' + phoneNo.right( (phoneNo.length() - 4) ); }
                
                
                // Add processed phone number to unique list of numbers, using set to ensure duplicates are not added
                if (phoneNo != null && tempSet.add(phoneNo)) 
                {
                    // If phone number looks like a mobile number, add to mobile list
                    if (phoneNo.left(4) == '+447')
                    {
                        uniqueMobileNumbers.add(phoneNo);
                    } 
                    
                    // Add any other number to phone number list
                    else 
                    { 
                        uniquePhoneNumbers.add(phoneNo); 
                    }
                }            
            }
        }
        
        
        // Create phone wrapper to store phone numbers that will be kept
        phoneWrapper processedPhones = new phoneWrapper();
        
        // Calculate and store the number of unique phone numbers in the deduped list
        Integer phoneCount = uniquePhoneNumbers.size();
        
        // Calculate and store the number of unique mobile numbers in the deduped list
        Integer mobileCount = uniqueMobileNumbers.size(); 
        
        // Mobile numbers have priority over other numbers
        // If number of mobile numbers is 1, store it in mobile variable
        if (mobileCount == 1)
        {
            processedPhones.mobile = uniqueMobileNumbers.get(0); 
        }       
        
        // If number of mobile numbers is 2, populate them in the mobile and mobile alternative fields
        if (mobileCount == 2)
        {
            processedPhones.mobile = uniqueMobileNumbers.get(0); 
            processedPhones.mobileAlt = uniqueMobileNumbers.get(1); 
        }
        
        // If number of mobile numbers is 3, put third one in the phone field
        if (mobileCount == 3)
        {
            processedPhones.mobile = uniqueMobileNumbers.get(0); 
            processedPhones.mobileAlt = uniqueMobileNumbers.get(1); 
            processedPhones.phone = uniqueMobileNumbers.get(2);             
        }
        
        // If number of mobile numbers is 4 or more, put fourth one in the phone alternative field        
        if (mobileCount >= 4)
        {
            processedPhones.mobile = uniqueMobileNumbers.get(0); 
            processedPhones.mobileAlt = uniqueMobileNumbers.get(1); 
            processedPhones.phone = uniqueMobileNumbers.get(2);  
            processedPhones.phoneAlt = uniqueMobileNumbers.get(3);                       
        }
        
        // If number of other phone numbers is 1..
        if (phoneCount == 1)
        {
            // ..if phone is still null, put it there
            if (processedPhones.phone == null)
            {
                processedPhones.phone = uniquePhoneNumbers.get(0);
            }
            
            // .. else if phoneAlt is still null, put it there
            else if (processedPhones.phoneAlt == null)
            {
                processedPhones.phoneAlt = uniquePhoneNumbers.get(0); 
            } 
        }  
        
        // If number of other phone numbers is 2 or more
        if (phoneCount >= 2)
        {
            // ..if phone is still null, put it there
            if (processedPhones.phone == null)
            {
                processedPhones.phone = uniquePhoneNumbers.get(0);
                processedPhones.phoneAlt = uniquePhoneNumbers.get(1); 
            }
            
            // .. else if phoneAlt is still null, put it there
            else if (processedPhones.phoneAlt == null)
            {
                processedPhones.phoneAlt = uniquePhoneNumbers.get(0); 
            } 
        }
        
        return processedPhones;
    }
    
    
    
    
   /*
    * This method takes a list of emails, de-duplicates them, and returns a wrapper
    * class containing up to 2 email addresses
    */   
    
    private static emailWrapper processEmails( List<String> emailAddresses )
        
    {
        
        // Dedupe list of email addresses with use of Set
        Set<String> tempSet = new Set<String>();
        List<String> uniqueEmailAddresses = new List<String>();
        
        // Whislt looping through, ensure that null values are not added to the set as we don't want those..
        for (String email : emailAddresses) 
        {
            if (email != null && tempSet.add(email)) 
            {
                uniqueEmailAddresses.add(email);
            }
        }
        
        // Calculate and store the number of unique email addresses in the deduped list
        Integer emailCount = uniqueEmailAddresses.size();
        
        // Create wrapper class to store email addresses that will be kept
        emailWrapper processedEmails = new emailWrapper();
        
        // If list size of email addresses is one, set the email variable with that email address
        if (emailCount == 1) 
        {
            processedEmails.email = uniqueEmailAddresses.get(0);
        }
        
        // If list size of email addresses is two or more, copy first email to email variable, and second email alternative variable. 
        // Ultimately this means that if the list has more than 2 email addressses, they will be lost in the merge.
        if (emailCount >= 2) 
        {
            processedEmails.email = uniqueEmailAddresses.get(0); 
            processedEmails.emailAlt = uniqueEmailAddresses.get(1);
        }  
        
        return processedEmails;
        
    }
    
    
   /*
    * Custom wrapper class that can hold 2 email addresses
    */   
    
    public class emailWrapper{
        String email;
        String emailAlt;
    }  
    
    
   /*
    * Custom wrapper class that can hold 2 phone numbers
    */  
    
    public class phoneWrapper{
        String phone;
        String phoneAlt;
        String mobile;
        String mobileAlt;
    }         
    
}