@isTest
public class ct_SessionRegistrationControllerTest {
    static testMethod void testSessionRegistrationController(){
        
        Profile profile = [Select Id From Profile where Name = 'System Administrator' limit 1];
       
        User usr1= new User();
        usr1.ProfileID = profile.Id;
        usr1.Username ='Test@Testemail.com'+ System.currentTimeMillis(); 
        usr1.LastName ='TestLastname';
        usr1.Email ='Test@Testemail.com';
        usr1.Alias ='TestAlia';
        usr1.TimeZoneSidKey ='TestCommunityNickname';
        usr1.CommunityNickname ='TestCommunityNickname';
        usr1.TimeZoneSidKey ='America/Los_Angeles';
        usr1.LocaleSidKey ='en_US';
        usr1.EmailEncodingKey ='UTF-8';
        usr1.LanguageLocaleKey ='en_US';
        insert usr1;
        
        Account testAcc = new Account();
        testAcc.name = 'TestAcc';
        insert testAcc;
        
        Event__c evt = new Event__c();
        evt.Name = 'Test Event';
        evt.Event_Type__c = 'Open Day';
        evt.Event_Manager__c = usr1.Id;
        insert evt;
        
        Event_Product__c tkt = new Event_Product__c();
        tkt.Name = 'Test Ticket';
        tkt.Event__c = evt.Id;
        insert tkt;
        
        Event_Registration__c evtReg = new Event_Registration__c();
        evtReg.Event__c = evt.Id;
        evtReg.Account_Name__c = testAcc.Id;
        evtReg.Event_Product__c = tkt.Id;
        insert evtReg;
        
        Track__c trk = new Track__c();
        trk.Name = 'Test Track';
        trk.Track_Status__c ='Sessions Pending';
        trk.Event__c = evt.Id;
        insert trk;
        
        Session__c session = new Session__c();
        session.Name = 'Test Session';
        session.Session_Abstract__c = 'Test Abstract Session';
        session.Track__c = trk.Id;
        session.Event__c = evt.Id;
        session.Start_Time__c = time.newInstance(11, 00, 00, 00);
        session.End_Time__c = time.newInstance(12, 00, 00, 00);
        session.Owner__c = usr1.Id;
        insert session;
        
        test.startTest();
        ct_SessionRegistrationController.getPageInitData(evtReg.Id);
        ct_SessionRegistrationController.sessionsOnTitleSearch(evt.Id,'Test Session');
        ct_SessionRegistrationController.sessionsOnTrackSearch(evt.Id,'Test Track');
        test.stopTest();
        List<Event__c> lstEvt = [select id,Name from Event__c where id=:evt.Id];
        system.assertEquals(lstEvt[0].Name,'Test Event');
    }
    static testMethod void testCreateSessionReg(){
           
        Profile profile = [Select Id From Profile where Name = 'System Administrator' limit 1];
       
        User usr1= new User();
        usr1.ProfileID = profile.Id;
        usr1.Username ='Test@Testemail.com'+ System.currentTimeMillis(); 
        usr1.LastName ='TestLastname';
        usr1.Email ='Test@Testemail.com';
        usr1.Alias ='TestAlia';
        usr1.TimeZoneSidKey ='TestCommunityNickname';
        usr1.CommunityNickname ='TestCommunityNickname';
        usr1.TimeZoneSidKey ='America/Los_Angeles';
        usr1.LocaleSidKey ='en_US';
        usr1.EmailEncodingKey ='UTF-8';
        usr1.LanguageLocaleKey ='en_US';
        insert usr1;
        
        Account testAcc = new Account();
        testAcc.name = 'TestAcc';
        insert testAcc;
        
        Event__c evt = new Event__c();
        evt.Name = 'Test Event';
        evt.Event_Type__c = 'Open Day';
        evt.Event_Manager__c = usr1.Id;
        insert evt;
        
        Event_Product__c tkt = new Event_Product__c();
        tkt.Name = 'Test Ticket';
        tkt.Event__c = evt.Id;
        insert tkt;
        
        Event_Registration__c evtReg = new Event_Registration__c();
        evtReg.Event__c = evt.Id;
        evtReg.Account_Name__c = testAcc.Id;
        evtReg.Event_Product__c = tkt.Id;
        insert evtReg;
        
        Track__c trk = new Track__c();
        trk.Name = 'Test Track';
        trk.Track_Status__c ='Sessions Pending';
        trk.Event__c = evt.Id;
        insert trk;
        
        Session__c session = new Session__c();
        session.Name = 'Test Session';
        session.Session_Abstract__c = 'Test Abstract Session';
        session.Track__c = trk.Id;
        session.Event__c = evt.Id;
        session.Start_Time__c = time.newInstance(11, 00, 00, 00);
        session.End_Time__c = time.newInstance(12, 00, 00, 00);
        session.Owner__c = usr1.Id;
        insert session;
        
        String sessionJson='[{"selected":true,"session":{"Id":"'+session.Id+'","Session_Abstract__c":"'+session.Session_Abstract__c+'","Name":"'+session.Name+'","Start_Time__c":"'+session.Start_Time__c +'","End_Time__c":"'+session.End_Time__c+'","Event__c":"'+evt.Id+'"}}]';
       
        test.startTest();
        string str = ct_SessionRegistrationController.createSessionReg(sessionJson, evtReg.Id);
        System.assert(String.IsNotEmpty(sessionJson));
        test.stopTest();
        
        List<Session__c> lstSession =[select id,Name,Session_Abstract__c from Session__c where id=:Session.Id];
        system.assertEquals(lstSession[0].Name, 'Test Session');
        system.assertEquals(lstSession[0].Session_Abstract__c, 'Test Abstract Session');
    }
}