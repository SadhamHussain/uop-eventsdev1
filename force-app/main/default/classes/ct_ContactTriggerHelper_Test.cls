/**
 * @File Name          : ct_ContactTriggerHelper_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 25/6/2020, 5:30:07 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/6/2020   Creation Admin     Initial Version
**/
@IsTest(SeeAllData = true)
public class ct_ContactTriggerHelper_Test {
  @IsTest
  static void testAlumniApprovalAndCommunicationDetail() {
    Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE;
    insert thisAccount;
    
    Contact thisContact   = ct_TestDataFactory.createContact();
    thisContact.AccountId = thisAccount.Id;
    thisContact.Alumni_Verification_Status__c = ct_Constants.CONTACT_ALUMNI_VERIFICATION_STATUS_UNVERIFIED;

    Test.StartTest();
    insert thisContact;
    Test.StopTest();
    
	if(ct_CustomSettingService.getCurrentUserByPassConfiguration() == null
    	|| (!ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Emails_Off__c
        && !ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Alumni_Emails_off__c)){
    	ProcessInstance pi = [SELECT TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :thisContact.Id];
    	System.assertEquals(true, pi != null);
    }
    System.assertEquals(1, [SELECT Id FROM Communication_Detail__c WHERE Contact__c =:thisContact.Id].size());
  }

  @IsTest
  static void testAlumniApprovalAndCommunicationDetailUpdate() {
    Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE;
    insert thisAccount;
    
    Contact thisContact   = ct_TestDataFactory.createContact();
    thisContact.AccountId = thisAccount.Id;
    thisContact.Alumni_Verification_Status__c = ct_Constants.CONTACT_ALUMNI_VERIFICATION_STATUS_UNVERIFIED;
    thisContact.Emergency_Contact_Phone_Number__c = '9597799033';
    thisContact.Email = null;
    insert thisContact;

    Test.StartTest();
    ct_ContactTriggerHandler_TDTM.hasExecuted = false;
    thisContact.Emergency_Contact_Phone_Number__c = '9443553332';
    update thisContact;
    Test.StopTest();

    System.assertEquals(2, [SELECT Id FROM Communication_Detail__c WHERE Contact__c =:thisContact.Id].size());
  }

  @IsTest
  static void testUpdatingDeceasedAndROrgasationalContact(){
    Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_EDUCATIONAL_INSTITUTION;
    thisAccount.R_O_Organisation__c = true;
    insert thisAccount;
    
    Contact thisContact   = ct_TestDataFactory.createContact();
    thisContact.AccountId = thisAccount.Id;
    thisContact.Date_of_Death__c = System.today();

    Test.StartTest();
    insert thisContact;
    Test.StopTest();

    System.assertEquals(true, [SELECT Id, hed__Deceased__c FROM Contact WHERE Id =: thisContact.Id].hed__Deceased__c);
  }

  
}