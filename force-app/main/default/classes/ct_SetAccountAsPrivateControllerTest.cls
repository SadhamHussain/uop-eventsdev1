/**
 * @File Name          : ct_SetAccountAsPrivateControllerTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 25/6/2020, 3:06:32 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/6/2020   Creation Admin     Initial Version
**/
@IsTest(SeeAllData=true)
public class ct_SetAccountAsPrivateControllerTest {
  @IsTest
  static void testSetAccountAsPrivate(){
    Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE;
    thisAccount.AAA_Private_Record__c = false;
    insert thisAccount;
  
    Contact thisContact   = ct_TestDataFactory.createContact();
    thisContact.AccountId = thisAccount.Id;
    insert thisContact;

    hed__Address__c thisAddress = ct_TestDataFactory.createHedAddress(thisContact.id);
    insert thisAddress;
    
    test.starttest();
    ct_SetAccountAsPrivateController.setAccountAsPrivate(thisContact.Id, true);
    ct_SetAccountAsPrivateController.getInitialAccountPrivacyStatus(thisContact.Id);
    test.stoptest();

    System.assertEquals(true, [SELECT Id, AAA_Private_Record__c FROM Account WHERE Id = :thisAccount.Id].AAA_Private_Record__c);

  }
}