/**
 * @File Name          : ct_GenericDeleteBatch.cls
 * @Description        : Using this Generic batch class to delete the records based on the Object and Duration parameter.
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin  - Creation Technology Solutions
 * @Last Modified On   : 5/5/2020, 12:57:01 pm
 */
global class ct_GenericDeleteBatch implements Database.Batchable<sObject>{
    String queryString; //To build the dynamic query
    String objectName; //Use this variable to store it in Error Log 
    /**
     * @description          - Build the query to fetch records based on the parameter 
     * @param objectName     - sObject Name
     * @param duration       - Fetch records based on this value  
     */
    global ct_GenericDeleteBatch(String objectNameParam, String durationParam) {
        objectName  = objectNameParam; //Store sObject Name
        queryString = 'SELECT Id FROM '+ objectNameParam +' WHERE CreatedDate < '+durationParam;
    }
    /**
     * @description          - Fetch the records based on the query param
     * @param BC 
     * @return               - List of sObject records 
     */
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(queryString);
    }
    /**
     * @description          - Perform the Delete operation for list of sObject records and 
                              store error in Error Log object if batch failed
     * @param BC 
     * @param scope         - sObject list 
     * @return void 
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try{
            //delete the sObject list
            delete scope;
        }
        catch(Exception e){
            //Store the error in Error Log object if this batch failed
            ct_Logger.logMessage(e, objectName);
            throw e;
        }
        
    }
    global void finish(Database.BatchableContext BC){
        
    }
}