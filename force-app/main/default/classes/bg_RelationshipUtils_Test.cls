/*****************************************************************
* bg_RelationshipUtils_Test
*
* test methods for bg_RelationshipUtils
*
* Author: BrightGen
* Created: 05-12-2018
******************************************************************/

@isTest
public class bg_RelationshipUtils_Test {
	
	@isTest (seeAllData = true)
	private static void testApplicantMatchingExistingEnquirerWithInfluencerAreBothConverted() 
	{
		Lead influencerEnquirer = bg_UnitTestDataFactory.createInfluencerEnquirer();
		insert influencerEnquirer;

		Lead studentEnquirer = bg_UnitTestDataFactory.createStudentEnquirerLinkedToInfluencerEnquirer(influencerEnquirer.Id);
		studentEnquirer.FirstName = 'test928384';
		studentEnquirer.LastName = 'user';
		studentEnquirer.Email = 'test928384@user.com';
		studentEnquirer.Phone = '01403785206';
		studentEnquirer.PostalCode = 'E2 8YS';
		insert studentEnquirer;

		Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount.Course_Start_Month__c = '09';
		courseAccount.Course_Start_Year__c = '2020';
	    insert courseAccount;

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.First_Name__c = 'test928384'; 
	    testApp1.Last_Name__c = 'user';
	    testApp1.Email_Address__c = 'test928384@user.com';
	    testApp1.Phone_Number__c = '0739357673';
	    testApp1.Postal_Code__c = 'E2 8YS';
	    testApp1.Course__c = courseAccount.Id;
	    testApp1.School_College__c = schoolAccount.Id;


	    List<Application__c> testApplications = new List<Application__c>{testApp1};
	    insert testApplications;

		Test.startTest();

		bg_ApplicationUtils.processApplications(testApplications);

		List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
															 Enquirer__c, Opportunity__c, Applicant__c
															 FROM Application__c
															 WHERE Id IN :testApplications];

		Contact applicantContact = [SELECT Id FROM Contact WHERE Student_Applicant_Id__c = :updatedTestApplications[0].Student_Id__c];
		Contact influencerContact = [SELECT Id, FirstName, LastName FROM Contact WHERE FirstName = :influencerEnquirer.FirstName AND LastName = :influencerEnquirer.LastName LIMIT 1]; 
		Opportunity newOpportunity = [SELECT Id FROM Opportunity WHERE Application_Id__c = :updatedTestApplications[0].SCMS_Application_Id__c];

		system.assertEquals(applicantContact.Id, updatedTestApplications[0].Applicant__c);
		system.assertNotEquals(null, influencerContact);
		system.assertEquals(applicantContact.Id, updatedTestApplications[0].Applicant__c);
		system.assertEquals(newOpportunity.Id, updatedTestApplications[0].Opportunity__c);

		List<hed__Relationship__c> newRelationships = [SELECT Id, Name, hed__RelatedContact__c, hed__Contact__c, hed__type__c FROM hed__Relationship__c
												 	   WHERE hed__Contact__c = :applicantContact.Id AND hed__RelatedContact__c = :influencerContact.Id];

		system.assertEquals(1, newRelationships.size());
		system.assertEquals('Child', newRelationships[0].hed__type__c, 'Relationship should be Child');

		Test.stopTest();
	}
}