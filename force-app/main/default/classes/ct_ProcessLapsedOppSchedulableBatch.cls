/**
 * @File Name          : ct_ProcessLapsedOppSchedulableBatch.cls
 * @Description        : Batch class to process Lapsed Donation Opportunity
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/11/2020, 12:49:51 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/26/2020   Creation Admin     Initial Version
**/
global class ct_ProcessLapsedOppSchedulableBatch implements Schedulable, Database.Batchable<sObject>,Database.Stateful{
  //Donor Contact records which has lapsed donation opportunity and needs to be processed
  Map<Id, Contact> contactMapToUpdate = new Map<Id, Contact>();
  //Aggregate result which is used to for process Donor type
  Map<Id,AggregateResult> donorTypeAggregateMap;

  global ct_ProcessLapsedOppSchedulableBatch(){
    //Get all lapsed opportunity ie the donation opportunity whose close date is equal to or greater than 12 months ago
    contactMapToUpdate = getDonorWithLapsedOpportunity();
  }

  global List<Contact> start(Database.BatchableContext BC) {
    //The Donor contact that has lapsed opportunity are passed as scope
    return contactMapToUpdate.values();
  }

  /**
  * @description : The executed method of the batch which process the donor contact with lapsed opportunity
  * @param BC BatchableContext
  * @param scope List of Contact records to be processed
  * @return void 
  **/
  global void execute(Database.BatchableContext BC, List<Contact> scope){
    for(Contact thisDonor : scope){
      //Set the Donor type value as null initially
      thisDonor.Donor_Type__c   = null;
      //Process Donor status 
      ct_OpportunityTriggerHelper.processContactDonorType(thisDonor, donorTypeAggregateMap);
      //If the donor status is still null then it means that there are Closed won opportunities which has closed date within 12 months
      if(thisDonor.Donor_Type__c == null){
        //Since the donor has only lapsed opportunity set the donor status to lapsed
        thisDonor.Donor_Status__c = ct_Constants.CONTACT_DONOR_STATUS_LAPSED;
      }
    }

    try{
      //Update the processed Donor Contact
      update scope;
    }
    catch(Exception e){
      //Invoke Error Logger if any error occurs
      ct_Logger.logMessage(e, 'Contact');
      throw e;
    }
  }

  /**
  * @description Method Which gets invoked after the batch has completed
  * @param BC BatchableContext
  * @return void 
  **/
  global void finish(Database.BatchableContext BC) {
   
  }

  /**
  * @description Execute method which gets invoked while class is scheduled
  * @param SC SchedulableContext
  * @return void 
  **/
  public void execute(SchedulableContext SC) {
    //Schedule the above batch for processing Lapsed opportunity with 200 as batch size
    ct_ProcessLapsedOppSchedulableBatch batch = new ct_ProcessLapsedOppSchedulableBatch();
    Database.executebatch(batch, 200);
  }

  /**
  * @description : Method to get Lapsed Opportunity Donation record's Contact record
  * @return Map<Id, Contact> Donor records  With Lapsed Opportunity
  **/
  public Map<Id, Contact> getDonorWithLapsedOpportunity(){
    //Date variable to store the date 12 Months ago from today used in querying records
    Date LAST_12_MONTH_DATE              = System.today().addMonths(-12);
    //Set to store contact ids with lapsed opportunities
    Set<Id> donorIdWithLapsedOpportunity = new Set<Id>();
    //Set to store all donation opportunity record types
    Set<Id> donationOpportunityRecordTypeIds = new Set<Id>{ct_Constants.OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE,
                                                            ct_Constants.OPPORTUNITY_RECORDTYPE_LEGACY_DONATION_PLEDGE,
                                                            ct_Constants.OPPORTUNITY_RECORDTYPE_MAJOR_DONATION,
                                                            ct_Constants.OPPORTUNITY_RECORDTYPE_IN_KIND_GIFT};
    //Get all lapsed opportunity from the org whose contact with no open , prospect , pledge opportunity or Lapsed Donor status
    for(Opportunity thisLapsedOpportunity : [SELECT Id, Name, npsp__Primary_Contact__c, npsp__Primary_Contact__r.Donor_Status__c,
                                              CloseDate, isWon, RecordTypeId
                                              FROM Opportunity
                                              WHERE npsp__Primary_Contact__r.Donor_Status__c != null
                                              AND npsp__Primary_Contact__r.Donor_Status__c EXCLUDES (:ct_Constants.CONTACT_DONOR_STATUS_PROSPECT, :ct_Constants.CONTACT_DONOR_STATUS_PLEDGED, :ct_Constants.CONTACT_DONOR_STATUS_LAPSED)
                                              AND isWon = true
                                              AND RecordTypeId IN :donationOpportunityRecordTypeIds
                                              AND CloseDate < :LAST_12_MONTH_DATE]){
      //The donor contact that has only lapsed opportunity that needs to be processed                                 
      donorIdWithLapsedOpportunity.add(thisLapsedOpportunity.npsp__Primary_Contact__c);
    }
    //Aggregate Result for Processing donor status in contact
    donorTypeAggregateMap = ct_OpportunityTriggerHelper.getDonorStatusAggregateResult(donorIdWithLapsedOpportunity, donationOpportunityRecordTypeIds);
    //Return the contacts that needs to be processed by Batch's as scope
    return new Map<Id, Contact>([SELECT Id, Name, RecordTypeId, Donor_Type__c, Donor_Status__c
                                  FROM Contact
                                  WHERE Id IN:donorIdWithLapsedOpportunity
                                  AND RecordTypeId =: ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT]);

  }
}