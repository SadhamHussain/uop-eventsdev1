/*****************************************************************
* bg_AccountUtils_Test
*
* Unit Test Class for bg_AccountUtils
* 
* Author: Mark Brown
* Created: 13-06-2019
* Case: 00042630
******************************************************************/
@isTest
private class bg_AccountUtils_Test
{

    static Id EmployerEnquirerRecordID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Employer').getRecordTypeId();
    static Id StudentEnquirerRecordID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Student').getRecordTypeId(); 
    static Id EducationalInstituteRecordID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Educational_Institution').getRecordTypeId();
    static Id AdminAccountRecordID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Administrative').getRecordTypeId();
    public class myException extends Exception {}
    static Boolean expectedExceptionThrown;

    @TestSetup
    private static void testDataSetup()
    {
        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Enquirer_GetSchoolAccount_TDTM', 'Lead', 'BeforeInsert;BeforeUpdate', 1.00));                                    
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        Account[] testAccts = bg_LeadTestDataFactory.createTestAccount(AdminAccountRecordID,EducationalInstituteRecordID,3);
        insert testAccts;
        Lead[] testLeads = bg_LeadTestDataFactory.createTestLeads(StudentEnquirerRecordID);
        insert testLeads;
    }
/*
@isTest static void TestForAcountQueryException() 
{
    try
    {
        // Set up the bits and bobs
        Lead lead = new Lead(RecordtypeId='crap',FirstName='Test', LastName='Lead', Company ='Test Lead', Status='New - Unverified',Enquiry_Channel__c='Email');

        // Perform test
        Test.startTest();
        insert lead; 
        Test.stopTest();

        // check the above insert failed with the correct exception
        //1. If we get to this line it means an error was not added and the test class should throw an exception here. 
        //2. MyException class extends Exception.                  
        throw new myException('An exception should have been thrown by the trigger but was not.'); 

    } catch (Exception e) 
    {
    //System.debug('error= '+e.getMessage());
        expectedExceptionThrown =  e.getMessage().contains('An exception should have been thrown by the trigger but was not') ? false : true;

    }
    //Check the correct exception is thrown
    System.AssertEquals(expectedExceptionThrown, true);

}
*/



    @isTest static void TestCreateNonStudentLead() // test employer lead created. nothing should be checked
    {
        // Set up the bits and bobs
        Lead lead = new Lead(RecordtypeId=EmployerEnquirerRecordID,FirstName='Test', LastName='Lead', Company ='Test Lead', Status='New - Unverified',Enquiry_Channel__c='Email');

        // Perform test
        Test.startTest();

        insert lead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(lead.Id);

        Test.stopTest();

        //Check nothing is set
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals(null,writtenLead.School_College__c);
    }

    @isTest static void TestCreateStudentLeadWithNonSchoolAccount() // test student lead created with a non school account
    {
        // Set up the bits and bobs      
        Lead lead = new Lead(RecordtypeId=StudentEnquirerRecordID,FirstName='Test', LastName='Lead', Company ='Test Lead', School_College_Freetext__c='Admin Account 1', Status='New - Unverified',Enquiry_Channel__c='Email');

        // Perform test
        Test.startTest();

        insert lead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(lead.Id);

        Test.stopTest();        
        
        //Check failed flag is set, no llokup is populated an d the freetext is present
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(true,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals('Admin Account 1',writtenLead.School_College_Freetext__c);
        System.assertEquals(null,writtenLead.School_College__c);
    }

    @isTest static void TestCreateLeadWithNoFreetext() // test for blank school/college freetxt
    {
        // Set up the bits and bobs   
        Lead lead = new Lead(RecordtypeId=StudentEnquirerRecordID,FirstName='Test', LastName='Lead', Company ='Test Lead', School_College_Freetext__c='', Status='New - Unverified',Enquiry_Channel__c='Email');

        // Perform test
        Test.startTest();

        insert lead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(lead.Id);

        Test.stopTest();

        //Check failed flag is not set, no lookup is populated, NO freetext is present
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals(null,writtenLead.School_College_Freetext__c);
        System.assertEquals(null,writtenLead.School_College__c);        
    }

    @isTest static void TestCreateLeadWithNoMatchingAccount() // test for no matching result
    {        
        // Set up the bits and bobs
        Lead lead = new Lead(RecordtypeId=StudentEnquirerRecordID,FirstName='Test', LastName='Lead', Company ='Test Lead', School_College_Freetext__c='invalidAccountName', Status='New - Unverified',Enquiry_Channel__c='Email');

        // Perform test
        Test.startTest();

        insert lead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(lead.Id);

        Test.stopTest();

        //Check failed flag is set, no lookup is populated, NO freetext is present
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(true,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals('invalidAccountName',writtenLead.School_College_Freetext__c);
        System.assertEquals(null,writtenLead.School_College__c);        
    }

    @isTest static void TestCreateLeadWithManyMatchingAccounts() // test for multiple Results 
    {
        // Set up the bits and bobs
        Lead lead = new Lead(RecordtypeId=StudentEnquirerRecordID,FirstName='Test', LastName='Lead', Company ='Test Lead', School_College_Freetext__c='Duplicate Account', Status='New - Unverified',Enquiry_Channel__c='Email');

        // Perform test
        Test.startTest();

        insert lead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(lead.Id);

        Test.stopTest();

        //Check failed flag is set, no lookup is populated and the freetext is present
        System.assertEquals(true,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals('Duplicate Account',writtenLead.School_College_Freetext__c);
        System.assertEquals(null,writtenLead.School_College__c);      
    }
    
    @isTest static void TestCreateLeadWithGoodMatchingAccount() // test for good result
    {
        // Set up the bits and bobs
        Lead lead = new Lead(RecordtypeId=StudentEnquirerRecordID,FirstName='Test', LastName='Lead', Company ='Test Lead', School_College_Freetext__c='Educational Account 2', Status='New - Unverified',Enquiry_Channel__c='Email');

        // Perform test
        Test.startTest();

        insert lead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(lead.Id);

        Test.stopTest();

        //Check no flag is set, the lookup is populated correctly and the freetext is present
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Failed_School_Account_Matches__c);
        Account EdAc2 = [SELECT Id FROM Account WHERE Name = 'Educational Account 2'];  
        System.assertEquals(EdAc2.Id,writtenLead.School_College__c);      
    }
        
    @isTest static void TestUpdateLeadWithMatchingFlagAndAccount() // test flags are removed from duplicate account errors when account is updated
    {
        // Get an Account with a Duplicate name
        Account DuplicateAccount1 = [SELECT Id FROM Account WHERE Name = 'Duplicate Account' LIMIT 1];

        // Retrieve the first write of the lead 
        Lead DuplicateAccountLead = [SELECT Id,Multiple_School_Account_Matches__c,Failed_School_Account_Matches__c,School_College__c FROM Lead WHERE School_College_Freetext__c = 'Duplicate Account'];

        // update the account on the lead
        DuplicateAccountLead.School_College__c = DuplicateAccount1.Id;
        DuplicateAccountLead.School_College_Freetext__c = null;

        // Perform test
        Test.startTest();

        update DuplicateAccountLead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(DuplicateAccountLead.Id);

        Test.stopTest();
  
        //Check multiple failed flags is not set, the lookup is populated correctly and the freetext is present
        System.assertEquals(true, DuplicateAccountLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false, DuplicateAccountLead.Failed_School_Account_Matches__c);        
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals(DuplicateAccount1.Id,writtenLead.School_College__c);      
    }
    
    @isTest static void TestUpdateLeadWithFailedFlagAndAccountWhenAccountSet() // test for failed flag being removed on update to lookup field on record 
    {    
        Account EducationalAccount2 = [SELECT Id FROM Account WHERE Name = 'Educational Account 2' LIMIT 1];  
system.debug('edac2 id = '+EducationalAccount2.Id);
        // Retrieve the first write of the lead 
        Lead invalidAccountNameLead = [SELECT Id,Multiple_School_Account_Matches__c,Failed_School_Account_Matches__c,School_College__c FROM Lead WHERE School_College_Freetext__c = 'invalidAccountName'];

        // update the account on the lead
        invalidAccountNameLead.School_College__c = EducationalAccount2.Id;
        invalidAccountNameLead.School_College_Freetext__c = null;

system.debug('invalidAccountNameLead.School_College__c = '+invalidAccountNameLead.School_College__c);
        // Perform test
        Test.startTest();

        update invalidAccountNameLead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(invalidAccountNameLead.Id);

        Test.stopTest();
  
        //Check failed flag was intiially set, then multiple & failed flags are not set, the lookup is populated correctly and the freetext is present with the original value. 
        System.assertEquals(true, invalidAccountNameLead.Failed_School_Account_Matches__c);
        System.assertEquals(false, invalidAccountNameLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals(EducationalAccount2.Id,writtenLead.School_College__c);       
    }
    
    @isTest static void TestUpdateLeadWithFailedFlagAndAccountWithNewFreetext() // now check the flags are reset and the lookup is populated when just the freetext is updated to correct value
    {       
        Account EducationalAccount3 = [SELECT Id FROM Account WHERE Name = 'Educational Account 3' LIMIT 1];  
        // Retrieve the first write of the lead 
        Lead invalidAccountNameLead = [SELECT Id,Multiple_School_Account_Matches__c,Failed_School_Account_Matches__c,School_College__c FROM Lead WHERE School_College_Freetext__c = 'invalidAccountName'];         

        // update the freetetx on the lead
        invalidAccountNameLead.School_College_Freetext__c = 'Educational Account 3';

        // Perform test
        Test.startTest();

        update invalidAccountNameLead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(invalidAccountNameLead.Id);

        Test.stopTest();
  
        //Check failed flag was initially set, but now multiple failed flags is not set, the lookup is populated correctly and the freetext is present
        System.assertEquals(true, invalidAccountNameLead.Failed_School_Account_Matches__c);
        System.assertEquals(false, invalidAccountNameLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals(EducationalAccount3.Id,writtenLead.School_College__c);       
    }

    @isTest static void TestUpdateLeadWithFailedFlagAndAccountFromMultiple() //test from a duplicate being changed to a failed
    {       
        // Retrieve the first write of the lead
        Lead DuplicateAccountLead = [SELECT Id,Multiple_School_Account_Matches__c,Failed_School_Account_Matches__c,School_College__c,School_College_Freetext__c FROM Lead WHERE School_College_Freetext__c = 'Duplicate Account'];

        // update the freetetx on the lead
        DuplicateAccountLead.School_College_Freetext__c = 'invalidAccountName';

        // Perform test
        Test.startTest();

        update DuplicateAccountLead;

        // Retrieve the lead
        Lead writtenLead = bg_LeadTestDataFactory.getWrittenLead(DuplicateAccountLead.Id); 

        Test.stopTest();
  
        //Check failed flag was initially set but multiple wasn't, but now the opposite is true, the lookup is null and the freetext has the new value
        System.assertEquals(true, DuplicateAccountLead.Multiple_School_Account_Matches__c);
        System.assertEquals(false, DuplicateAccountLead.Failed_School_Account_Matches__c);
        System.assertEquals(false,writtenLead.Multiple_School_Account_Matches__c);
        System.assertEquals(true,writtenLead.Failed_School_Account_Matches__c);
        System.assertEquals('invalidAccountName',writtenLead.School_College_Freetext__c);
        System.assertEquals(null,writtenLead.School_College__c);       
    }
}