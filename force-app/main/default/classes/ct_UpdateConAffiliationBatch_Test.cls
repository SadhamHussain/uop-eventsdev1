/**
 * @File Name          : ct_UpdateConAffiliationBatch_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 6/26/2020, 3:22:36 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_UpdateConAffiliationBatch_Test {
  /**
  * @description Test method to test ct_UpdateConAffiliationSchedulableBatch which creates new Affiliation for Opportunities
  * @return void 
  **/
  @isTest (seeAllData = true)
  private static void creating_New_AffiliationBatchTest(){
    Test.startTest();
    Application__c thisCreationApplication = ct_TestDataFactory.createApplication();
    insert thisCreationApplication;

    List<Account> accountListToInsert = new List<Account>();

    Account CSEAccademicAccount        = ct_TestDataFactory.createEducationalAccount('Computer Science Creation Tec');
    CSEAccademicAccount.RecordTypeId   = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(CSEAccademicAccount);

    Account ITAccademicAccount        = ct_TestDataFactory.createEducationalAccount('Information Creation Tec');
    ITAccademicAccount.RecordTypeId   = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(ITAccademicAccount);

    Account BSCCSEAccademicAccount        = ct_TestDataFactory.createEducationalAccount('BSC Computer Science Creation Tec');
    BSCCSEAccademicAccount.RecordTypeId   = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(BSCCSEAccademicAccount);

    Account MBAAccademicAccount        = ct_TestDataFactory.createEducationalAccount('MBA Creation Tec');
    MBAAccademicAccount.RecordTypeId   = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(MBAAccademicAccount);
    insert accountListToInsert;

    Contact applicant = ct_TestDataFactory.createContact();
    insert applicant;

    List<Opportunity> opportunityListToInsert = new List<Opportunity>();
    Opportunity appliedApplicationOpportunity  = ct_TestDataFactory.createApplicationOpportunity();
    appliedApplicationOpportunity.Applicant__c = applicant.Id;
    appliedApplicationOpportunity.Course__c    = CSEAccademicAccount.Id;
    opportunityListToInsert.add(appliedApplicationOpportunity);

    Opportunity rejectedApplicationOpportunity  = ct_TestDataFactory.createApplicationOpportunity();
    rejectedApplicationOpportunity.Applicant__c = applicant.Id;
    rejectedApplicationOpportunity.Course__c    = ITAccademicAccount.Id;
    rejectedApplicationOpportunity.StageName    = ct_Constants.OPPORTUNITY_STAGE_DECLINED_OFFER;
    opportunityListToInsert.add(rejectedApplicationOpportunity);

    Opportunity offerAcceptedApplicationOpportunity  = ct_TestDataFactory.createApplicationOpportunity();
    offerAcceptedApplicationOpportunity.Applicant__c = applicant.Id;
    offerAcceptedApplicationOpportunity.Course__c    = BSCCSEAccademicAccount.Id;
    offerAcceptedApplicationOpportunity.StageName    = ct_Constants.OPPORTUNITY_STAGE_OFFER_ACCEPTED;
    opportunityListToInsert.add(offerAcceptedApplicationOpportunity);

    Opportunity enrolledApplicationOpportunity  = ct_TestDataFactory.createApplicationOpportunity();
    enrolledApplicationOpportunity.Applicant__c = applicant.Id;
    enrolledApplicationOpportunity.Course__c    = MBAAccademicAccount.Id;
    enrolledApplicationOpportunity.StageName    = ct_Constants.OPPORTUNITY_STAGE_CLOSED_ENROLLED;
    enrolledApplicationOpportunity.Application__c = thisCreationApplication.Id;
    opportunityListToInsert.add(enrolledApplicationOpportunity);

    insert opportunityListToInsert;

    
      Database.executebatch(new ct_UpdateConAffiliationSchedulableBatch(), 200);
    Test.stopTest();

    Contact thisTestContact = [SELECT Id, Name, Primary_Role__c, R_O_Organisational_Contact__c, 
                                  No_Prospect_Affiliations__c,
                                  No_Applicant_Affiliations__c,
                                  No_Student_Affiliations__c,
                                  No_Alumni_Affiliations__c,
                                  No_Rejected_Affiliations__c,
                                  UOP_Academic_Affiliations__c
                                  FROM Contact 
                                  WHERE Id =:applicant.Id];
    System.assertEquals(2, thisTestContact.No_Applicant_Affiliations__c, 'Should return 2');
    System.assertEquals(1, thisTestContact.No_Student_Affiliations__c, 'Should return 1');
    System.assertEquals(1, thisTestContact.No_Rejected_Affiliations__c, 'Should return 1');
  }

   /**
  * @description Test method to test ct_UpdateConAffiliationSchedulableBatch which updates existing Affiliation for Opportunities
  * @return void 
  **/
  @isTest (seeAllData = true)
  private static void updating_AffiliationBatchTest(){
    Test.startTest();
    Application__c thisCreationApplication = ct_TestDataFactory.createApplication();
    insert thisCreationApplication;

    List<Account> accountListToInsert = new List<Account>();

    Account CSEAccademicAccount        = ct_TestDataFactory.createEducationalAccount('Computer Science Creation Tec');
    CSEAccademicAccount.RecordTypeId   = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(CSEAccademicAccount);

    Account ITAccademicAccount        = ct_TestDataFactory.createEducationalAccount('Information Creation Tec');
    ITAccademicAccount.RecordTypeId   = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(ITAccademicAccount);

    Account BSCCSEAccademicAccount        = ct_TestDataFactory.createEducationalAccount('BSC Computer Science Creation Tec');
    BSCCSEAccademicAccount.RecordTypeId   = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(BSCCSEAccademicAccount);

    Account MBAAccademicAccount        = ct_TestDataFactory.createEducationalAccount('MBA Creation Tec');
    MBAAccademicAccount.RecordTypeId   = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(MBAAccademicAccount);
    insert accountListToInsert;

    Contact applicant = ct_TestDataFactory.createContact();
    insert applicant;

    List<Opportunity> opportunityListToInsert = new List<Opportunity>();
    Opportunity appliedApplicationOpportunity  = ct_TestDataFactory.createApplicationOpportunity();
    appliedApplicationOpportunity.Applicant__c = applicant.Id;
    appliedApplicationOpportunity.Course__c    = CSEAccademicAccount.Id;
    opportunityListToInsert.add(appliedApplicationOpportunity);

    Opportunity rejectedApplicationOpportunity  = ct_TestDataFactory.createApplicationOpportunity();
    rejectedApplicationOpportunity.Applicant__c = applicant.Id;
    rejectedApplicationOpportunity.Course__c    = ITAccademicAccount.Id;
    rejectedApplicationOpportunity.StageName    = ct_Constants.OPPORTUNITY_STAGE_DECLINED_OFFER;
    opportunityListToInsert.add(rejectedApplicationOpportunity);

    Opportunity offerAcceptedApplicationOpportunity  = ct_TestDataFactory.createApplicationOpportunity();
    offerAcceptedApplicationOpportunity.Applicant__c = applicant.Id;
    offerAcceptedApplicationOpportunity.Course__c    = BSCCSEAccademicAccount.Id;
    offerAcceptedApplicationOpportunity.StageName    = ct_Constants.OPPORTUNITY_STAGE_OFFER_ACCEPTED;
    opportunityListToInsert.add(offerAcceptedApplicationOpportunity);

    Opportunity enrolledApplicationOpportunity  = ct_TestDataFactory.createApplicationOpportunity();
    enrolledApplicationOpportunity.Applicant__c = applicant.Id;
    enrolledApplicationOpportunity.Course__c    = MBAAccademicAccount.Id;
    enrolledApplicationOpportunity.StageName    = ct_Constants.OPPORTUNITY_STAGE_CLOSED_ENROLLED;
    enrolledApplicationOpportunity.Application__c = thisCreationApplication.Id;
    opportunityListToInsert.add(enrolledApplicationOpportunity);

    insert opportunityListToInsert;

    List<hed__Affiliation__c> affiliationListToInsert   = new List<hed__Affiliation__c>();

    affiliationListToInsert.add(ct_TestDataFactory.createHedaAffiliation(CSEAccademicAccount.Id, applicant.Id));
    affiliationListToInsert.add(ct_TestDataFactory.createHedaAffiliation(ITAccademicAccount.Id, applicant.Id));
    affiliationListToInsert.add(ct_TestDataFactory.createHedaAffiliation(BSCCSEAccademicAccount.Id, applicant.Id));
    affiliationListToInsert.add(ct_TestDataFactory.createHedaAffiliation(MBAAccademicAccount.Id, applicant.Id));
    insert affiliationListToInsert;

    
      Database.executebatch(new ct_UpdateConAffiliationSchedulableBatch(), 200);
    Test.stopTest();

    Contact thisTestContact = [SELECT Id, Name, Primary_Role__c, R_O_Organisational_Contact__c, 
                                  No_Prospect_Affiliations__c,
                                  No_Applicant_Affiliations__c,
                                  No_Student_Affiliations__c,
                                  No_Alumni_Affiliations__c,
                                  No_Rejected_Affiliations__c,
                                  UOP_Academic_Affiliations__c
                                  FROM Contact 
                                  WHERE Id =:applicant.Id];
    System.assertEquals(2, thisTestContact.No_Applicant_Affiliations__c, 'Should return 2');
    System.assertEquals(1, thisTestContact.No_Student_Affiliations__c, 'Should return 1');
    System.assertEquals(1, thisTestContact.No_Rejected_Affiliations__c, 'Should return 1');
    
  }


  @IsTest
  public static void updateConAffiliationSchedulerTest(){
    Test.startTest();
    ct_UpdateConAffiliationSchedulableBatch schedulerClassObj = new ct_UpdateConAffiliationSchedulableBatch();
    schedulerClassObj.execute(Null);
    Test.stopTest();
  }
}