/**
 * @File Name          : ct_UpdateCourseDetailsToConScheduleBatch.cls
 * @Description        : update latest course details
 * @Author             : Umashankar Creation
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 5/15/2020, 2:33:43 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    5/7/2020   Umashankar Creation     Initial Version
**/
global class ct_UpdateCourseDetailsToConScheduleBatch implements Schedulable, Database.Batchable<sObject>,Database.Stateful {
  /**
  * @description 
  * @param Database.BatchableContext BC 
  * @return Database.QueryLocator - return list of contact records
  **/
  global Database.QueryLocator start(Database.BatchableContext BC) {
    return getContactRecords();
  }
  /**
  * @description 
  * @param BC BatchableContext
  * @param scope List of Contact records to be processed
  * @return void 
  **/
  global void execute(Database.BatchableContext BC, List<Contact> scope){
    // Invoke this method to populate the Last Course Year, Last Course to contact which is taken from the latest program enrollment
    ct_hed_ProgramEnrollmentHelper.stampContactsWithLatestProgramEnrollmentRecords(new Map<Id, Contact>(scope).keySet());
  }

  /**
  * @description Method Which gets invoked after the batch has completed
  * @param BC BatchableContext
  * @return void 
  **/
  global void finish(Database.BatchableContext BC) {
   
  }

  /**
  * @description Execute method which gets invoked when class is scheduled
  * @param SC SchedulableContext
  * @return void 
  **/
  public void execute(SchedulableContext SC) {
    ct_UpdateCourseDetailsToConScheduleBatch batch = new ct_UpdateCourseDetailsToConScheduleBatch();
    Database.executebatch(batch, 200);
  }

  /**
  * @description : Method to get Contact records 
  * @return List<Contact> contact records 
  **/
  public Database.QueryLocator getContactRecords(){
    ct_Advancement_Setting__mdt thisMetadata = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName('Program_Enrollment_Run_for_All');
    String queryString = 'SELECT Id, RecordTypeId, Last_Course_Year__c, Last_Course_Taken__c'+
                                  ' FROM Contact'+
                                  ' WHERE RecordTypeId ='+'\''+ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT+'\' ';
    if(thisMetadata != null && thisMetadata.Value__c.toUpperCase() == 'TRUE'){
      return Database.getQueryLocator(queryString) ;
    }
    else if(thisMetadata != null && thisMetadata.Value__c.toUpperCase() == 'FALSE'){
      queryString = queryString + ' AND (Last_Course_Year__c = null OR Last_Course_Taken__c = null)';
      return Database.getQueryLocator(queryString);
    }
    return null;
  }
}