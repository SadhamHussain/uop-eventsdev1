/*****************************************************************
* bg_ApplicationProcessingInvocable
*
* Invocable Class for processing new Applications via Process Builder
* 
* Test Class: bg_ApplicationProcessingInvocable_Test
*
* Author: BrightGen
* Created: 07-12-2018
******************************************************************/

public class bg_ApplicationProcessingInvocable {

	/*
	*
	* Invoked via Applications Processing PB to initalise bg_ApplicationProcessingQueuable job
	*
	*/
	@InvocableMethod
	public static void processApplicationsForConversion (List<Id> listOfApplicationIds) {
		
		if(!isQueueableJobCurrentlyRunning())
		{
			Set<Id> applicationIdsToBeProcessed = new Set<Id>();
			applicationIdsToBeProcessed.addAll(listOfApplicationIds);
			System.enqueueJob(new bg_ApplicationProcessingQueuable(applicationIdsToBeProcessed));
		}
	}

	private static Boolean isQueueableJobCurrentlyRunning()
	{
		Integer enqueuedJobs = [SELECT COUNT() FROM AsyncApexJob 
								WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE Name = 'bg_ApplicationProcessingQueuable') 
								AND Status IN ('Processing','Preparing','Queued')];

		if(enqueuedJobs > 0)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}