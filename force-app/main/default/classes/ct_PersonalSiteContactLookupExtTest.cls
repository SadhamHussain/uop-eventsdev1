/**
 * @File Name          : ct_PersonalSiteContactLookupExtTest.cls
 * @Description        : Test Class for ct_PersonalSiteContactLookupExtension Class
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 7/1/2020, 11:31:46 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/14/2020   Creation Admin     Initial Version
**/

@isTest
public class ct_PersonalSiteContactLookupExtTest {

  /**
  * @description test construct site redirection url based on contact's Information 
  * @return void 
  **/
  @isTest(SeeAllData = true)
  static void getSiteURLTest() {
    List<Contact> contactToInsert                         = new List<Contact>();
    Contact thisDupeContact                               = ct_TestDataFactory.createContact();
    thisDupeContact.FirstName                             = 'CreationTestOne';
    thisDupeContact.Email                                 = 'CreationTestOne@test.com';
    thisDupeContact.GW_Volunteers__Volunteer_Status__c    = ct_Constants.VOLUNTEER_STATUS_ACTIVE;
    contactToInsert.add(thisDupeContact);
    Contact thisDupeContactTwo                            = ct_TestDataFactory.createContact();
    thisDupeContactTwo.GW_Volunteers__Volunteer_Status__c = ct_Constants.VOLUNTEER_STATUS_ACTIVE;
    contactToInsert.add(thisDupeContactTwo);
    insert contactToInsert;
    
    Test.startTest();

    Map<String, List<dupcheck.dc3SearchResult>> searchResult;
    searchResult = new Map<String, List<dupcheck.dc3SearchResult>>();

    searchResult.put('003', new List<dupcheck.dc3SearchResult>());
    dupcheck.dc3SearchResult firstDupeContact = new dupcheck.dc3SearchResult();
    firstDupeContact.score      = 80;
    firstDupeContact.objectData = [SELECT Id, FirstName, MobilePhone,LastName, Postcode__c,Student_Applicant_Id__c,Phone, Name, Email FROM Contact WHERE Id =:thisDupeContact.Id LIMIT 1];
    searchResult.get('003').add(firstDupeContact);

    dupcheck.dc3SearchResult secondDupeContact = new dupcheck.dc3SearchResult();
    secondDupeContact.score      = 70;
    secondDupeContact.objectData = [SELECT Id, FirstName, MobilePhone,LastName, Postcode__c,Student_Applicant_Id__c,Phone, Name, Email FROM Contact WHERE Id =:thisDupeContactTwo.Id LIMIT 1];
    searchResult.get('003').add(secondDupeContact);

    ct_PersonalSiteContactLookupExtension.searchResult = searchResult;

    PageReference pageRef = Page.ct_Volunteers_PersonalSiteContactLookup;
    Test.setCurrentPage(pageRef);
    ct_PersonalSiteContactLookupExtension.contactFirstname = 'CreationTestOne';
    ct_PersonalSiteContactLookupExtension.contactLastname  = 'Technology';
    ct_PersonalSiteContactLookupExtension.contactEmail     = 'creationtechnology@creation.com';
    PageReference thisSitePage = ct_PersonalSiteContactLookupExtension.personalSiteURL();
    Test.stopTest();
  }
}