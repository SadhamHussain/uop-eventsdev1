/**
 * @File Name          : ct_ScheduledDeleteErrorLogs.cls
 * @Description        : Execute the ct_GenericDeleteBatch class by sending Error Log object API 
 *                       and Duration to delete the records which are older than 1 month
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin  - Creation Technology Solutions
 * @Last Modified On   : 5/5/2020, 12:57:50 pm
**/
global class ct_ScheduledDeleteErrorLogs implements Schedulable{
    global void execute(SchedulableContext sc){
        Database.executeBatch(new ct_GenericDeleteBatch('Error_Log__c', 'LAST_N_DAYS: 30'));    
    }
}