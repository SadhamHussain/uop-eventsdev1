/**
 * @File Name          : ct_EventTriggerHandler_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 1/22/2020, 1:11:37 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/17/2019   Creation Admin     Initial Version
**/
@isTest
public class ct_EventTriggerHandler_Test {

  @isTest
  static void createCampaignFromEventTest() {
    Event__c facultyRecruitmentEvent = ct_TestDataFactory.createFacultyRecruitmentEvent();
    Test.startTest();
    insert facultyRecruitmentEvent;
    //Assert New inserted Campaign record
    Campaign newStudentCampaign = [SELECT Id, 
                                          IsActive, 
                                          Event__c, 
                                          Name, 
                                          Description, 
                                          EndDate, 
                                          StartDate, 
                                          Year_of_Entry__c
                                    FROM Campaign
                                    WHERE Event__c =:facultyRecruitmentEvent.Id];

    System.assertEquals(newStudentCampaign.IsActive, true, 'New Campaign should be created with active status');
    System.assertEquals(newStudentCampaign.Name, facultyRecruitmentEvent.Name, 'Campaign Name should be same As event name');
    System.assertEquals(newStudentCampaign.Description, facultyRecruitmentEvent.Event_Description__c, 'Campaign Desciption should be same As Event Description');
    System.assertEquals(newStudentCampaign.EndDate, facultyRecruitmentEvent.Event_End_Date__c, 'Campaign End Date should be same As Event End Date');
    System.assertEquals(newStudentCampaign.StartDate, facultyRecruitmentEvent.Event_Start_Date__c, 'Campaign Start Date should be same As Event Start Date');
    System.assertEquals(newStudentCampaign.Year_of_Entry__c, facultyRecruitmentEvent.Year_of_Entry__c, 'Campaign Year of Entry should be same As event Year of Entry');
    Test.stopTest();
  }
}