@isTest(SeeAllData=true)
public class RHX_TEST_Event_Registration 
{
    static testMethod void RHX_Testmethod() 
    {
        List<Event_Registration__c> sourceList = [SELECT Id FROM Event_Registration__c LIMIT 1];

        if(sourceList.size() == 0) 
        {
            Event__c testEvent = new Event__c(Name = 'BG Test Event');
            insert testEvent;

            sourceList.add(new Event_Registration__c(Event__c = testEvent.Id));
        }
        rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}