/**
 * @description       : 
 * @author            : Creation Admin
 * @group             : 
 * @last modified on  : 08-10-2020
 * @last modified by  : Creation Admin
 * Modifications Log 
 * Ver   Date         Author           Modification
 * 1.0   07-29-2020   Creation Admin   Initial Version
**/
public without sharing class CT_EventSearchController {
	@AuraEnabled(cacheable=true)
    public static List<Event__c> searchEventsByName(String eventName) {
        eventName = '%' + eventName + '%';
        return [SELECT Id, Web_Event_Name__c, Web_Event_Location__c, Event_Start_Date__c, 
        FORMAT(Start_Time__c), FORMAT(End_Time__c), Web_Event_Short_Description__c, Web_Event_Pricing__c,
        Web_Event_Image__c, Registration_Status__c FROM Event__c WHERE Web_Event_Name__c LIKE :eventName AND Active__c = true];
    }

    @AuraEnabled
    public static List<Event__c> getEventsBySearch(String byName, String byType, String byGroup, String byEventId){
      String quertString = 'SELECT Id, Web_Event_Name__c, Web_Event_Location__c, Event_Start_Date__c,'+ 
                            'FORMAT(Start_Time__c), FORMAT(End_Time__c), Web_Event_Short_Description__c, Web_Event_Pricing__c,'+
                            'Web_Event_Image__c, Registration_Status__c FROM Event__c WHERE Id != null';

      if(String.isNotBlank(byEventId)){
        if(byEventId.startsWith('a0I')){
          quertString = quertString + ' AND Id IN (SELECT Event__c FROM Event_Registration__c WHERE Id = :byEventId)';
        }else if(byEventId.startsWith('a0J'))
        quertString = quertString + ' AND Id = :byEventId';
      }
      else {
        
        if(String.isNotBlank(byName)){
          byName = '%'+byName+'%';
          quertString = quertString + ' AND Web_Event_Name__c LIKE :byName';
        }
  
        if(String.isNotBlank(byType)){
          byType = '%'+byType+'%';
          quertString = quertString + ' AND Event_Type__c LIKE :byType';
        }
  
        if(String.isNotBlank(byGroup)){
          quertString = quertString + ' AND Event_Group__c = :byGroup';
        } 
      }

      quertString = quertString + ' AND Active__c = true ORDER BY Web_Event_Name__c ASC';

      return Database.query(quertString);
    }

    @AuraEnabled(cacheable=true)  
    public static Event__c fetchEventAndTickets(String selectedEventId) {
        Event__c selectedEvent = [SELECT Id, Web_Event_Name__c, Web_Event_Location__c, Event_Start_Date__c, 
        FORMAT(Start_Time__c), FORMAT(End_Time__c), Web_Event_Short_Description__c, Web_Event_Long_Description__c, Web_Event_Pricing__c,
        Web_Event_Image__c, Registration_Status__c, (SELECT Id, Name, Event__r.Web_Event_Pricing__c, Event__r.Name, Total_Tickets__c, Event_Product_Price__c, Available_Tickets__c FROM Tickets__r) 
        FROM Event__c WHERE Active__c = true AND Id =:selectedEventId];
        return selectedEvent;
    }
    
    @AuraEnabled
    public static void saveEventTicketOrder(String ticketOrderAsJson, String eventRegistrationAsJson, String urlParamId) {
        TicketOrder ticketOrderData = (TicketOrder) Json.deserialize(ticketOrderAsJson, TicketOrder.class);
        List<EventRegistration> registrations = (List<EventRegistration>) Json.deserialize(eventRegistrationAsJson, List<EventRegistration>.class);
        
        Event_Order__c ticketOrder = new Event_Order__c();
        ticketOrder.Event__c = ticketOrderData.eventId;
        ticketOrder.First_Name__c = ticketOrderData.firstName;
        ticketOrder.Last_Name__c = ticketOrderData.lastName;
        ticketOrder.Email__c = ticketOrderData.email;
        
        try{
          insert ticketOrder;
        }
        catch(Exception e){
          //Exception mechanism to log any exceptions occured during the dml operation
          ct_Logger.logMessage(e, 'Event Registration');
          throw e;
        }

        List<Event_Registration__c> eventRegistrations = new List<Event_Registration__c>();
        for (eventRegistration eachRegistration : registrations) {
            Event_Registration__c eventRegistration = new Event_Registration__c();
            if(String.isNotBlank(urlParamId) && urlParamId != null){
              if(urlParamId.startsWith('a0I')){
                eventRegistration.Id = urlParamId;
              }
            }
            if((String.isBlank(urlParamId) && urlParamId == null) || urlParamId.startsWith('a0J')){
              eventRegistration.Event__c = ticketOrderData.eventId;
              eventRegistration.Event_Product__c = eachRegistration.ticketId;
            }
            eventRegistration.First_Name__c = eachRegistration.firstName;
            eventRegistration.Last_Name__c = eachRegistration.lastName;
            eventRegistration.Email__c = eachRegistration.email;
            //eventRegistration.Ticket__c = eachRegistration.ticketId;
            //eventRegistration.Ticket_Order__c = ticketOrder.Id;
            eventRegistrations.add(eventRegistration);
        }
        try{
          upsert eventRegistrations;
        }
        catch(Exception e){
          //Exception mechanism to log any exceptions occured during the dml operation
          ct_Logger.logMessage(e, 'Event Registration');
          throw e;
        }
    }

    @AuraEnabled
    public static  List<PicklistEntryWrapper> getPicklistValues(String objectName, String fieldName){
      return constructPicklistEntryWrapper(ct_WebDataFormHelper.getPicklistValues(objectName, fieldName));
    }

    private static List<PicklistEntryWrapper> constructPicklistEntryWrapper(List<Schema.PicklistEntry> picklistEntryList){

      List<PicklistEntryWrapper> thisPickListWrapperList        = new List<PicklistEntryWrapper>();
      //Loop through the picklist entries to construct picklist options
      for( Schema.PicklistEntry thisPicklistEntry : picklistEntryList){
        thisPickListWrapperList.add(new PicklistEntryWrapper(thisPicklistEntry.getLabel(), thisPicklistEntry.getValue()));
      }
      return thisPickListWrapperList;
    }

    @AuraEnabled
    public static List<Wrapper> getEventGroups(){
    List<Wrapper> eventGroupWrapperList = new List<Wrapper>();
    try{
      for(Event_Group__c thisEventGroup : [SELECT Id, Name FROM Event_Group__c 
                                          /*WHERE Name LIKE :'%'+eventGroupName+'%' */
                                          ORDER BY Name asc
                                          LIMIT 50000]){
        eventGroupWrapperList.add(new Wrapper(thisEventGroup));
      }
    }
    catch (Exception ex){
      ct_Logger.logMessage(ex, 'Event Group Lookup Component');
      throw new AuraHandledException('Error '+ ex.getMessage() + ex.getLineNUmber());

    }
    //return list of course records
    return eventGroupWrapperList;
    }

    public class TicketOrder{
        public String firstName;
        public String lastName;
        public String email;
        public String eventId;
    }
    public class EventRegistration{
        public String firstName;
        public String lastName;
        public String email;
        public String ticketId;
        public Boolean isGuest;
    }

  /**
    * Wrapper that contains Picklist Values
  */
  public class PicklistEntryWrapper{
    //get set string to store lable of picklist entry
    @AuraEnabled
    public String label                  {get;set;}
    //get set string to store value of picklist entry
    @AuraEnabled
    public string value                  {get;set;}
    
    public PicklistEntryWrapper(String entryLabel,String entryValue){
      this.label    = entryLabel;
      this.value    = entryValue;
    }
  }

  /**
    * Wrapper that contains Event Group data as Picklist Values
  */
  public class Wrapper{    
    @AuraEnabled
    public String label {get;set;} 
    @AuraEnabled
    public String value {get;set;}
    Wrapper(Event_Group__c thisEventGroup){
      label = thisEventGroup.Name;
      value = thisEventGroup.Name+'UNIQUE_ID'+thisEventGroup.Id;
    }
  }
}