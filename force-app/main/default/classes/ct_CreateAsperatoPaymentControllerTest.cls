/**
 * @File Name          : ct_CreateAsperatoPaymentControllerTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 12/5/2020, 7:26:22 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/4/2020   Creation Admin     Initial Version
**/
@IsTest
public class ct_CreateAsperatoPaymentControllerTest {

  /**
  * @description Test Creation of Asperato Payment & Redirection
  * @author Creation Admin | 30/4/2020 
  * @return void 
  **/
  @IsTest(SeeAllData=true)
  static void testCreateAsperatoModal(){

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;

    Opportunity thisOpportunity = ct_TestDataFactory.createVolunteerOpportunity(thisContact);
    thisOpportunity.StageName = 'Identification';
    thisOpportunity.RecordTypeId = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Major_Donation');
    insert thisOpportunity;

    Test.startTest();
    npe01__OppPayment__c thisGEMPayment   =  ct_TestDataFactory.createGEMPayment(thisOpportunity, ct_Constants.CASH_PAYMENT_METHOD);
    thisGEMPayment.npe01__Payment_Date__c = System.Today();
    thisGEMPayment.npe01__Scheduled_Date__c  = System.Today();
    insert thisGEMPayment;
    
    PageReference pageRef = Page.ct_CreateAsperatoPayment;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('recordId', thisGEMPayment.id);
    pageRef.getParameters().put('amount', '');
    pageRef.getParameters().put('actionType', 'showPaymentModal');
    pageRef.getParameters().put('showThankyouScreen', 'false');
    
    ct_CreateAsperatoPaymentController paymentController = new ct_CreateAsperatoPaymentController();
    ct_CreateAsperatoPaymentController.managePaymentCreation();
    Test.stopTest();
  }

  @IsTest(SeeAllData=true)
  static void testShowSuccessPage(){

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;

    Opportunity thisOpportunity = ct_TestDataFactory.createVolunteerOpportunity(thisContact);
    thisOpportunity.StageName = 'Identification';
    thisOpportunity.RecordTypeId = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Major_Donation');
    insert thisOpportunity;
    
    Test.startTest();
    npe01__OppPayment__c thisGEMPayment   =  ct_TestDataFactory.createGEMPayment(thisOpportunity, ct_Constants.CASH_PAYMENT_METHOD);
    thisGEMPayment.npe01__Payment_Date__c = System.Today();
    thisGEMPayment.npe01__Scheduled_Date__c  = System.Today();
    insert thisGEMPayment;
    
    PageReference pageRef = Page.ct_CreateAsperatoPayment;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('recordId', thisGEMPayment.Id);
    pageRef.getParameters().put('amount', '');
    pageRef.getParameters().put('actionType', 'sendEmail');
    pageRef.getParameters().put('showThankyouScreen', 'true');

    ct_CreateAsperatoPaymentController paymentController = new ct_CreateAsperatoPaymentController();
    ct_CreateAsperatoPaymentController.managePaymentCreation();
    Test.stopTest();
  }
}