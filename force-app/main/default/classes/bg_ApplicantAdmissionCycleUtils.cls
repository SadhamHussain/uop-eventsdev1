public class bg_ApplicantAdmissionCycleUtils {
    /*****************************************************************
* bg_ApplicantIntakeCycleUtils
*
* Utility methods relating to Applicant_Intake_Cycle__c object
* 
* Test Class: bg_ApplicantIntakeCycleUtils_Test
*************** Version History **********************
 * KA, CT, 19/03/2020 - UOPS-5 - Error while updating collection - The fix will make sure only 
 * unique elements in the collection will be updated.
******************************************************************/
    
    /*
* Main method for processing new/existing Applicant Intake Cycles for Contact
*/
    public static void processApplicantIntakeCyclesForOpportunities(Map<Id, Opportunity> newOppsMap, Map<Id, Opportunity> oldOpportunitiesById)
    {    
        system.debug('in processApplicantIntakeCyclesForOpportunities');
        Map<Id, Opportunity> opportunitiesById = new Map<Id, Opportunity>([SELECT Id, Applicant__c,Admissions_Cycle__c, 
                                                                           Course__r.Course_Level_of_Study__c, Applicant_Admission_Cycle__c, 
                                                                           CloseDate, StageName, Applicant_Choice__c, Application__c, Agent_Application__c FROM Opportunity 
                                                                           WHERE Id IN:newOppsMap.KeySet()]);
        
        Map<Id, List<Opportunity>> opportunitiesByContactId = bg_OpportunityUtils.createMapOfOpportunitiesByContact(opportunitiesById);
        Map<Id, List<Applicant_Admission_Cycle__c>> applicantIntakeCycleByContactId = createMapOfApplicantAdmissionCycleByContact(opportunitiesByContactId);
        //update so that you get matching aacs that match on year, contact and level of study
        //
        Map<Id,Applicant_Admission_Cycle__c> applicantIntakeCycleToUpsertByOppId = new Map<Id,Applicant_Admission_Cycle__c>();
        
        for(Opportunity newOpp : opportunitiesById.values())
        {
            Opportunity oldOpp = oldOpportunitiesById.get(newOpp.Id);
            List<Opportunity> oppsLinkedToContact = opportunitiesByContactId.get(newOpp.Applicant__c);
            
            if(!applicantIntakeCycleByContactId.containsKey(newOpp.Applicant__c))
            {                
                system.debug('about to create aic');
                Applicant_Admission_Cycle__c aicToCreate = createApplicantIntakeCycleForNewOpportunity(newOpp);
                applicantIntakeCycleToUpsertByOppId.put(newOpp.Id, aicToCreate);
            }
            else
            {   
                List<Applicant_Admission_Cycle__c> aicsLinkedToContact = applicantIntakeCycleByContactId.get(newOpp.Applicant__c);
                Applicant_Admission_Cycle__c aicForSameIntake = new Applicant_Admission_Cycle__c();
                Map<Id, Opportunity> oppsForSameIntake = new Map<Id, Opportunity>();
                
                for(Applicant_Admission_Cycle__c aic : aicsLinkedToContact)
                {
                    for(Opportunity opp : oppsLinkedToContact)
                    {   
                        if(aic.Admission_Cycle__c == newOpp.Admissions_Cycle__c && aic.Level_of_Study__c == newOpp.Course__r.Course_Level_of_Study__c)
                        {
                            aicForSameIntake = aic;
                        }
                        //maybe don't need code below
                        if(aic.Id == opp.Applicant_Admission_Cycle__c && aic.Admission_Cycle__c == newOpp.Admissions_Cycle__c && aic.Level_of_Study__c == newOpp.Course__r.Course_Level_of_Study__c)
                        {
                        oppsForSameIntake.put(opp.Id, opp);
                        } 
                    }
                }
                
                if(aicForSameIntake != null && !oppsForSameIntake.isEmpty())
                {
                    System.debug('matching aac');
                Applicant_Admission_Cycle__c aicToUpdate = updateApplicantIntakeCycleForOpportunity(newOpp, oldOpp, aicForSameIntake, oppsForSameIntake.values());
                applicantIntakeCycleToUpsertByOppId.put(newOpp.Id, aicForSameIntake);
                }
                else
                             {
                    Applicant_Admission_Cycle__c aicToCreate = createApplicantIntakeCycleForNewOpportunity(newOpp);
                    applicantIntakeCycleToUpsertByOppId.put(newOpp.Id, aicToCreate);
                }
            } 
        }
        
        if(!applicantIntakeCycleToUpsertByOppId.isEmpty())
        {
            System.debug('applicantIntakeCycleToUpsertByOppId ' + applicantIntakeCycleToUpsertByOppId);
            System.debug('opportunitiesById ' + opportunitiesById);
            upsertAICsAndUpdateRelatedRecords(opportunitiesById, applicantIntakeCycleToUpsertByOppId);
        }
    }
    
    /*
* Insert/Update AIC records and populate AIC lookups on associated Opportunity and Application record
*/
    private static void upsertAICsAndUpdateRelatedRecords(Map<Id, Opportunity> oppsByOppId, Map<Id,Applicant_Admission_Cycle__c> applicantIntakeCycleToUpsertByOppId)
    {        
        List<Id> oppIdList = new List<Id>(applicantIntakeCycleToUpsertByOppId.keySet());
        List<Applicant_Admission_Cycle__c> aicListToProcess = new List<Applicant_Admission_Cycle__c>(applicantIntakeCycleToUpsertByOppId.values());
        system.debug('listToProcess ' + aicListToProcess);
        
        Map<Id, Applicant_Admission_Cycle__c> existingAICsById = new Map<Id, Applicant_Admission_Cycle__c>();
        Map<Id, Applicant_Admission_Cycle__c> aicsToProcessByOppId = new Map<Id, Applicant_Admission_Cycle__c>();
        Map<Id, Applicant_Admission_Cycle__c> outStandingAICsToProcessByOppId = new Map<Id, Applicant_Admission_Cycle__c>();
        List<Applicant_Admission_Cycle__c> aicListForDupeChecking = new List<Applicant_Admission_Cycle__c>();
        
        for(Integer i = 0; i < applicantIntakeCycleToUpsertByOppId.size(); i++)
        {
            Id oppId = oppIdList.get(i);
            Applicant_Admission_Cycle__c aic =  aicListToProcess.get(i);
            
            if(aic.Id != null)
            {
                if(existingAICsById.containsKey(aic.Id))
                {
                    outStandingAICsToProcessByOppId.put(oppId, aic);
                }
                else 
                {
                    existingAICsById.put(aic.Id, aic);
                    aicsToProcessByOppId.put(oppId, aic);
                }
            }
            else if(aicListForDupeChecking.contains(aic))
            {
                outStandingAICsToProcessByOppId.put(oppId, aic);
            }
            else
            {
                aicListForDupeChecking.add(aic);
                aicsToProcessByOppId.put(oppId, aic);
            }       
        }
        
        System.debug('aicsToProcessByOppId ' + aicsToProcessByOppId);
        Database.UpsertResult[] upsertAICs = Database.upsert(aicsToProcessByOppId.values(), false);
        
        //List<SObject> relatedRecordToUpdate = new List<SObject>();
        List<Opportunity> relatedOppsToUpdate = new List<Opportunity>();
        List<Application__C> appsToUpdate = new List<Application__c>();
        List<Id> oppIdsRelatedToAICs = new List<Id>(aicsToProcessByOppId.keySet());
        Map<Id, String> errorMessageByAppId = new Map<Id, String>();
        
        for(Integer i = 0; i < upsertAICs.size(); i++) 
        {
            System.debug('in the for loop after aacs upserted');
            Id oppId = oppIdsRelatedToAICs[i];
            Opportunity opp = oppsByOppId.get(oppId);
            Id appId = opp.Application__c;
            
            if(upsertAICs[i].isSuccess())
            {
                Id aicId = upsertAICs[i].getId();
                System.debug('aicId ' + aicId);
                Opportunity oppToUpdate = new Opportunity(Id = oppId, Applicant_Admission_Cycle__c = aicId);
                relatedOppsToUpdate.add(oppToUpdate);
                
                Application__c appToUpdate = new Application__c(Id = opp.Application__c, Applicant_Admission_Cycle__c = aicId);
                appsToUpdate.add(appToUpdate);
            }
            else
            {
                String errorMessage = '';
                
                for(Database.Error error : upsertAICs[i].getErrors()) 
                {
                    if(String.isEmpty(errorMessage)) 
                    {
                        errorMessage = error.getMessage();
                    } 
                }
                
                errorMessageByAppId.put(appId, errorMessage);
            }
        }
        
        if(!relatedOppsToUpdate.isEmpty())
        {
            Database.SaveResult[] updateRelatedRecords = Database.update(relatedOppsToUpdate, false);
        }
        if(!appsToUpdate.isEmpty())
        {
            Database.SaveResult[] updateRelatedAppRecords = Database.update(appsToUpdate, false);
        }
        
        if(!errorMessageByAppId.isEmpty())
        {   
            bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
        }
        
        // This is used when there are duplicate AICs being processed as part of the same transaction. We therefore want to exclude them from the initial upsert and reprocess them
        if(!outStandingAICsToProcessByOppId.isEmpty())
        {
            upsertAICsAndUpdateRelatedRecords(oppsByOppId, outStandingAICsToProcessByOppId);
        }
    }
    
    /*
* Create an AIC upon creation of a New Opportunity
*/
    public static Applicant_Admission_Cycle__c createApplicantIntakeCycleForNewOpportunity(Opportunity newOpp)
    {
        Applicant_Admission_Cycle__c aicToCreate = new Applicant_Admission_Cycle__c();
        
        aicToCreate.Applicant__c = newOpp.Applicant__c;
        aicToCreate.Admission_Cycle__c = newOpp.Admissions_Cycle__c;
        aicToCreate.Level_of_Study__c = newOpp.Course__r.Course_Level_of_Study__c;
        aicToCreate.Expected_Enrollment_Date__c = newOpp.CloseDate;
        
        system.debug('newOpp stage name is ' + newOpp.StageName);
        
        /*  aicToCreate = setApplicationAdmissionCycleFlagsFromOpportunity(aicToCreate, aicToCreate, newOpp);
if(newOpp.StageName == 'Closed - Declined Offer')
{
aicToCreate.All_Offers_Declined__c = true;
}
else if(newOpp.StageName == 'Closed - Not Offered')
{
aicToCreate.All_Applications_Rejected__c = true;
} */
        
        return aicToCreate;
        
    }
    
    @testVisible private static Applicant_Admission_Cycle__c setApplicationAdmissionCycleFlagsFromOpportunity(Applicant_Admission_Cycle__c aicToCheck, Applicant_Admission_Cycle__c aicToSet, Opportunity newOpp)
    {
        system.debug('newOpp.stagename ' + newOpp.stageName);
        if(aicToCheck.Offered__c != true) 
        { 
            if(newOpp.StageName == 'Offer Accepted' || newOpp.StageName == 'Closed - Enrolled' || newOpp.StageName == 'Offered' || newOpp.StageName == 'Closed - Declined Offer')
            {
                aicToSet.Offered__c = true;
                aicToSet.First_Application_Offered__c = newOpp.Id;
            }
        }
        if(aicToCheck.Accepted__c != true)
        {
            if(newOpp.StageName == 'Offer Accepted' || newOpp.StageName == 'Closed - Enrolled')
            {
                aicToSet.Accepted__c = true;
                aicToSet.First_Application_Accepted__c = newOpp.Id;
            }
        }
        if(aicToCheck.Firmed__c != true)
        {
            if(newOpp.Applicant_Choice__c == 'Firm')
            {
                aicToSet.Firmed__c = true;
                aicToSet.First_Application_Firmed__c = newOpp.Id;               
            }
        }
        if(aicToCheck.Enrolled__c != true)
        {
            if(newOpp.StageName == 'Closed - Enrolled')
            {
                aicToSet.Enrolled__c = true;
                aicToSet.Application_Enrolled__c = newOpp.Id;
            }
        }
        if(aicToCheck.Deferred__c != true)
        {
            if(newOpp.StageName == 'Deferred')
            {
                aicToSet.Deferred__c = true;
            }
        }
        if(aicToCheck.Withdrawn__c != true)
        {
            if(newOpp.StageName == 'Closed - Withdrawn')
            {
                aicToSet.Withdrawn__c = true;
            }
        }
        if(aicToCheck.Agent_Submitted_Application__c != true)
        {
            if(newOpp.Agent_Application__c == true)
            {
                aicToSet.Agent_Submitted_Application__c = true;
            }
        }
        
        return aicToSet;
        
    }
    
    /*
* Update AIC record based on updates to Opportunities within the same Intake Cycle / Level of Study
*/
    @testVisible private static Applicant_Admission_Cycle__c updateApplicantIntakeCycleForOpportunity(Opportunity newOpp, Opportunity oldOpp, Applicant_Admission_Cycle__c aicForSameIntake, List<Opportunity> oppsForSameIntake)
    {
        Applicant_Admission_Cycle__c aicToupdate = new Applicant_Admission_Cycle__c(Id = aicForSameIntake.Id);
        aicToupdate = setApplicationAdmissionCycleFlagsFromOpportunity(aicForSameIntake, aicToUpdate, newOpp);
        
        Boolean allDeclined = true;
        Boolean allRejected = true;
        Opportunity oppToUse = newOpp;
        
        Date expectedEnrollmentDate;
        
        if(oppsForSameIntake.size() > 1)
        {
            for(Opportunity oppToCompare : oppsForSameIntake)
            {
                if(allDeclined)
                {
                    if(oppToCompare.StageName != 'Closed - Declined Offer')
                    {   
                        allDeclined = false;
                    }
                }
                
                if(allRejected)
                {
                    if(oppToCompare.StageName != 'Closed - Not Offered')
                    {
                        allRejected = false;
                    }
                }
                
                Opportunity oppClosestToEnrollment = identifyOppotunityClosestToEnrollment(oppToUse, oppToCompare, aicForSameIntake);
                
                expectedEnrollmentDate = oppClosestToEnrollment.CloseDate;
                oppToUse = oppClosestToEnrollment;
            }
            
            if(allDeclined)
            {
                aicToUpdate.All_Offers_Declined__c = true;
            }
            else
            {
                aicToUpdate.All_Offers_Declined__c = false;
            }
            if(allRejected)
            {
                aicToUpdate.All_Applications_Rejected__c = true;
            }
            else
            {
                aicToUpdate.All_Applications_Rejected__c = false;
            }
        }
        else
        {         
            if(newOpp.StageName == 'Closed - Declined Offer')
            {
                aicToUpdate.All_Offers_Declined__c = true;
            }
            else if(newOpp.StageName == 'Closed - Not Offered')
            {
                aicToUpdate.All_Applications_Rejected__c = true;
            }
            expectedEnrollmentDate = newOpp.CloseDate;
        }
        
        aicToUpdate.Expected_Enrollment_Date__c = expectedEnrollmentDate;
        
        
        if(String.isBlank(aicToUpdate.Applicant__c))
        {
            aicToUpdate.Applicant__c = oppToUse.Applicant__c;
        }
        
        return aicToupdate;
    }
    
    
    
    /*
* To calculate the Enrollment Date based on the Opportunity Close Date's - the Opportunity that is the furthest down the Sales process will take priority
*/
    @testVisible private static Opportunity identifyOppotunityClosestToEnrollment(Opportunity oppToUse, Opportunity oppToCompare, Applicant_Admission_Cycle__c aicForSameIntake)
    {
        Opportunity oppClosestToEnrollment;
        
        Integer oppToUseStagePriority = setOpportunityStageAsInteger(oppToUse.StageName);
        Integer oppToCompareStagePriority = setOpportunityStageAsInteger(oppToCompare.StageName);
        
        if(oppToUseStagePriority < oppToCompareStagePriority)
        {
            oppClosestToEnrollment = oppToUse;
        }
        else if(oppToUseStagePriority > oppToCompareStagePriority)
        {
            oppClosestToEnrollment = oppToCompare;
        }
        else
        {
            if(oppToUse.CloseDate <= oppToCompare.CloseDate)
            {
                oppClosestToEnrollment = oppToUse;
            }
            else
            {
                oppClosestToEnrollment = oppToCompare;
            }
        }
        return oppClosestToEnrollment;
    }
    
    /*
* To assign a priority integer to an Opportunity StageName in order to easily calculate which Opportunity Close Date to use for calculating the AIC Enrollment Date
*/
    @testVisible private static Integer setOpportunityStageAsInteger(String oppStage)
    {
        Integer stagePriority;
        
        if(oppStage == 'Closed - Enrolled')
        {
            stagePriority = 1;
        }
        else if(oppStage == 'Firmed Offer')
        {
            stagePriority = 2;
        }
        else if(oppStage == 'Offer Accepted')
        {
            stagePriority = 3;
        }
        else if(oppStage == 'Offered')
        {
            stagePriority = 4;
        }
        else if(oppStage == 'Applied')
        {
            stagePriority = 5;
        }
        else if(oppStage.StartsWith('Closed') && oppStage != 'Closed - Enrolled')
        {
            stagePriority = 6;
        }
        
        return stagePriority;
    }
    
    /*
* Creates a Map of all Opportunities linked to an Applicant Intake Cyle - to be used for processing the Applicant Admission Cycle
*/
    @testVisible private static Map<Id, List<Applicant_Admission_Cycle__c>> createMapOfApplicantAdmissionCycleByContact(Map<Id, List<Opportunity>> opportunitiesByContactId)
    {
        Map<Id, List<Applicant_Admission_Cycle__c>> applicantIntakeCycleByContactId = new Map<Id, List<Applicant_Admission_Cycle__c>>();
        
        Set<Id> appIntakeCycleIds = new Set<Id>();
        
        for(List<Opportunity> oppList : opportunitiesByContactId.values())
        {
            for(Opportunity opp : oppList)
            {
                if(String.isNotBlank(opp.Applicant_Admission_Cycle__c))
                {
                    appIntakeCycleIds.add(opp.Applicant_Admission_Cycle__c);
                }
            }
        }
        
        if(!appIntakeCycleIds.isEmpty())
        {
            Map<Id,Applicant_Admission_Cycle__c> relatedApplicantIntakeCyclebyId = new Map<Id, Applicant_Admission_Cycle__c>(
                [SELECT All_Applications_Rejected__c, All_Declined__c, All_Offers_Declined__c, Agent_Submitted_Application__c, 
                 Expected_Enrollment_Date__c, Application_Enrolled__c, Firmed__c,  
                 First_Application_Firmed__c, First_Application_Offered__c, Admission_Cycle__c, Level_of_Study__c, 
                 Offered__c, Applicant__c, Accepted__c, Enrolled__c, Deferred__c, Withdrawn__c 
                 FROM Applicant_Admission_Cycle__c WHERE Id IN :appIntakeCycleIds]);
            
            for(Id contactId : opportunitiesByContactId.keySet())
            {
                List<Opportunity> oppsForContact = opportunitiesByContactId.get(contactId);
                
                for(Opportunity opp : oppsForContact)
                {
                    if(String.isNotBlank(opp.Applicant_Admission_Cycle__c))
                    {
                        Applicant_Admission_Cycle__c aic = relatedApplicantIntakeCyclebyId.get(opp.Applicant_Admission_Cycle__c);
                        
                        if(applicantIntakeCycleByContactId.containsKey(contactId))
                        {
                            applicantIntakeCycleByContactId.get(contactId).add(aic);
                        }
                        else
                        {
                            applicantIntakeCycleByContactId.put(contactId, new List<Applicant_Admission_Cycle__c>{aic});
                        }
                    }
                }
            }
        }
        
        return applicantIntakeCycleByContactId;
    }
    
    /*
* Go through all opps related to an AAC and set checkboxes lookups on AAC
*/
    public static void setFlagsOnAacFromOpps(Set<Id> aacIds)
    {
        System.debug('new AAC method  ' + aacIds);
        List<Applicant_Admission_Cycle__c> aacsToUpdate = new List<Applicant_Admission_Cycle__c>();
        List<Applicant_Admission_Cycle__c> aacsToUpdateFirm = new List<Applicant_Admission_Cycle__c>();
        Map<Id, List<Opportunity>> oppsByAacId = new Map<Id, List<Opportunity>>();
        Map<Id, Applicant_Admission_Cycle__c> aacIdByAac = new Map<Id, Applicant_Admission_Cycle__c>([SELECT Id, All_Applications_Rejected__c, All_Offers_Declined__c, 
                                                                                                      Offered__c, Accepted__c, Deferred__c, Withdrawn__c, Agent_Submitted_Application__c, 
                                                                                                      Firmed__c, Enrolled__c, All_Declined__c, BestOffer__c, BestOffer__r.AAC_Hierarchy__c, First_Application_Accepted__c, 
                                                                                                      First_Application_Firmed__c, First_Application_Offered__c, Application_Enrolled__c, AllClosedandNotEnrolled__c 
                                                                                                      FROM Applicant_Admission_Cycle__c WHERE Id IN:aacIds]);
        
        Map<Id, Opportunity> oppIdByAaacRelatedOpps = new Map<Id, Opportunity>([SELECT Id, Name, StageName, Applicant_Admission_Cycle__c, 
                                                                                Applicant_Choice__c, CloseDate, Offer_Status__c, LastModifiedDate, AAC_Hierarchy__c
                                                                                FROM Opportunity WHERE Applicant_Admission_Cycle__c IN:aacIds]);
        
        System.debug('oppIdByAacRelatedOpps ' + oppIdByAaacRelatedOpps);
        
        Map<Applicant_Admission_Cycle__c, List<Opportunity>> oppsByAAC = new Map<Applicant_Admission_Cycle__c, List<Opportunity>>();
        
        for(Opportunity opp : oppIdByAaacRelatedOpps.values())
        {
            if(!oppsByAAC.containsKey(aacIdByAac.get(opp.Applicant_Admission_Cycle__c)))
            {
                system.debug('adding with Id ' + opp.Applicant_Admission_Cycle__c);
                oppsByAAC.put(aacIdByAac.get(opp.Applicant_Admission_Cycle__c), new List<Opportunity>{opp});
            }
            else
            {
                oppsByAAC.get(aacIdByAac.get(opp.Applicant_Admission_Cycle__c)).add(opp);
            }
        }
        
        System.debug('oppsByAAC size is ' + oppsByAAC.size());
        
        for(Applicant_Admission_Cycle__c aac : oppsByAAC.keySet())
        {
            system.debug('in loop');
            List<Opportunity> relatedOpps = oppsByAAC.get(aac);
            List<Opportunity> newOpenOpps = new List<Opportunity>();
            System.debug('relatedOpps for AAC ' + relatedOpps + ' ' + aac.Id);
            Integer withdrawn = 0;
            Integer rejected = 0;
            Integer open = 0;
            Integer declined = 0;
            Opportunity lowestHierarchy = null;
            
            if(aac.BestOffer__c != null)
            {
                lowestHierarchy = oppIdByAaacRelatedOpps.get(aac.BestOffer__c);
            }
            
            Set<Opportunity> multipleLowHierarchy = new Set<Opportunity>();
            
            for(Opportunity relatedOpp : relatedOpps)
            {
                if(relatedOpp.stageName == 'Closed - Withdrawn')
                {
                    withdrawn ++;
                }
                else if(relatedOpp.stageName == 'Closed - Not Offered')
                {
                    rejected ++;
                }
                else if(relatedOpp.stageName == 'Closed - Declined Offer')
                {
                    declined ++;
                }
                else
                {
                    open ++;
                    newOpenOpps.add(relatedOpp);
                }
                
                
            }
            system.debug('withdrawn is ' + withdrawn + ' rejected is ' + rejected + ' declines is ' + declined + ' open is ' + open);
            
            Integer withdrawnOrRejected = withdrawn + rejected + declined;
            Boolean toUpdate = false;
            Boolean enrolled = false;
            
            for(Opportunity relatedOpp : relatedOpps)
            {
                system.debug('in for line 569');
                if(relatedOpp.stageName == 'Closed - Enrolled')
                {
                    aac.Enrolled__c = true;
                    aac.BestOffer__c = relatedOpp.Id;
                    aac.Application_Enrolled__c = null;
                    aac.All_Applications_Rejected__c = false;
                    aac.All_Offers_Declined__c = false;
                    aac.Offered__c = false;
                    aac.Accepted__c = false;
                    aac.Deferred__c = false;
                    aac.Withdrawn__c = false;
                    aac.Agent_Submitted_Application__c = false;
                    aac.Firmed__c = false;
                    aac.All_Declined__c = false;
                    aac.AllClosedandNotEnrolled__c = false;
                    aac.Expected_Enrollment_Date__c = relatedOpp.CloseDate;
                    aacsToUpdate.add(aac);
                    enrolled = true;
                    break;
                }
                else if(relatedOpps.size() == withdrawn)
                {
                    aac.Enrolled__c = false;
                    aac.Application_Enrolled__c = null;
                    aac.BestOffer__c = null;
                    aac.First_Application_Accepted__c = null; 
                    aac.First_Application_Firmed__c = null;
                    aac.First_Application_Offered__c = null; 
                    aac.All_Applications_Rejected__c = false;
                    aac.All_Offers_Declined__c = false;
                    aac.Offered__c = false;
                    aac.Accepted__c = false;
                    aac.Deferred__c = false;
                    aac.Withdrawn__c = true;
                    aac.Agent_Submitted_Application__c = false;
                    aac.Firmed__c = false;
                    aac.All_Declined__c = false;
                    aac.AllClosedandNotEnrolled__c = false;
                    aacsToUpdate.add(aac);
                    break;
                }
                else if(relatedOpps.size() == rejected)
                {
                    aac.Enrolled__c = false;
                    aac.Application_Enrolled__c = null;
                    aac.BestOffer__c = null;
                    aac.Application_Enrolled__c = null;
                    aac.BestOffer__c = null;
                    aac.First_Application_Accepted__c = null; 
                    aac.First_Application_Firmed__c = null;
                    aac.First_Application_Offered__c = null; 
                    aac.All_Applications_Rejected__c = true;
                    aac.All_Offers_Declined__c = false;
                    aac.Offered__c = false;
                    aac.Accepted__c = false;
                    aac.Deferred__c = false;
                    aac.Withdrawn__c = false;
                    aac.Agent_Submitted_Application__c = false;
                    aac.Firmed__c = false;
                    aac.All_Declined__c = false;
                    aac.AllClosedandNotEnrolled__c = false;
                    aacsToUpdate.add(aac);
                    break;
                }
                else if(relatedOpps.size() == declined)
                {
                    aac.Enrolled__c = false;
                    aac.Application_Enrolled__c = null;
                    aac.BestOffer__c = null;
                    aac.Application_Enrolled__c = null;
                    aac.BestOffer__c = null;
                    aac.First_Application_Accepted__c = null; 
                    aac.First_Application_Firmed__c = null;
                    aac.First_Application_Offered__c = null; 
                    aac.All_Applications_Rejected__c = false;
                    aac.All_Declined__c = true;
                    aac.All_Offers_Declined__c = false;
                    aac.Offered__c = false;
                    aac.Accepted__c = false;
                    aac.Deferred__c = false;
                    aac.Withdrawn__c = false;
                    aac.Agent_Submitted_Application__c = false;
                    aac.Firmed__c = false;
                    aac.AllClosedandNotEnrolled__c = false;
                    aacsToUpdate.add(aac);
                    break;
                    
                }
                else if(relatedOpps.size() == withdrawnOrRejected)
                {
                    aac.Enrolled__c = false;
                    aac.Application_Enrolled__c = null;
                    aac.BestOffer__c = null;
                    aac.First_Application_Accepted__c = null; 
                    aac.First_Application_Firmed__c = null;
                    aac.First_Application_Offered__c = null; 
                    aac.All_Applications_Rejected__c = false;
                    aac.All_Offers_Declined__c = false;
                    aac.Offered__c = false;
                    aac.Accepted__c = false;
                    aac.Deferred__c = false;
                    aac.Withdrawn__c = false;
                    aac.Agent_Submitted_Application__c = false;
                    aac.Firmed__c = false;
                    aac.All_Declined__c = false;
                    aac.AllClosedandNotEnrolled__c = true;
                    aacsToUpdate.add(aac);
                    break;
                    
                }
                else if(relatedOpp.Applicant_Choice__c == 'Firm')
                {
                    aac.Enrolled__c = false;
                    aac.Application_Enrolled__c = null;
                    aac.BestOffer__c = relatedOpp.Id;
                    aac.First_Application_Accepted__c = null; 
                    aac.First_Application_Firmed__c = null;
                    aac.First_Application_Offered__c = null; 
                    aac.All_Applications_Rejected__c = false;
                    aac.All_Offers_Declined__c = false;
                    aac.Offered__c = false;
                    aac.Accepted__c = false;
                    aac.Deferred__c = false;
                    aac.Withdrawn__c = false;
                    aac.Agent_Submitted_Application__c = false;
                    aac.Firmed__c = true;
                    aac.All_Declined__c = false;
                    aac.AllClosedandNotEnrolled__c = false;
                    aac.Expected_Enrollment_Date__c = relatedOpp.CloseDate;
                    //aacsToUpdate.add(aac);
                    aacsToUpdateFirm.add(aac);
                    system.debug('adding to firm');
                    
                }
                else
                {
                    if(relatedOpps.size() == 1 || (newOpenOpps.size() == 1 && newOpenOpps[0].Id == relatedOpp.Id))
                    {
                        aac.BestOffer__c = newOpenOpps[0].Id;
                        aac.Expected_Enrollment_Date__c = newOpenOpps[0].CloseDate;
                        aac.Enrolled__c = false;
                        aac.Application_Enrolled__c = null;
                        aac.First_Application_Accepted__c = null; 
                        aac.First_Application_Firmed__c = null;
                        aac.First_Application_Offered__c = null; 
                        aac.All_Applications_Rejected__c = false;
                        aac.All_Offers_Declined__c = false;
                        aac.Offered__c = false;
                        aac.Accepted__c = false;
                        aac.Deferred__c = false;
                        aac.Withdrawn__c = false;
                        aac.Agent_Submitted_Application__c = false;
                        aac.Firmed__c = false;
                        aac.All_Declined__c = false;
                        aac.AllClosedandNotEnrolled__c = false;
                        
                        aacsToUpdate.add(aac);
                        break;
                    }
                    else if (lowestHierarchy != null)
                    {

                        if(relatedOpp.AAC_Hierarchy__c < lowestHierarchy.AAC_Hierarchy__c)
                        {
                            lowestHierarchy = relatedOpp;
                            toUpdate = true;
                        }
                        else if (relatedOpp.AAC_Hierarchy__c == lowestHierarchy.AAC_Hierarchy__c && relatedOpp.Id != lowestHierarchy.Id && toUpdate)
                        {
                            multipleLowHierarchy.add(relatedOpp);
                            multipleLowHierarchy.add(lowestHierarchy);
                            //toUpdate = true;
                        }
                    }
                }

            }
            system.debug('aacsToUpdate at line 746 ' + aacsToUpdate.size());
                            
                if(withdrawn != relatedOpps.size() && rejected != relatedOpps.size() && withdrawnOrRejected != relatedOpps.size() && !enrolled && !aacsToUpdateFirm.IsEmpty())
                {
                    system.debug('setting to firm');
                    aacsToUpdate = aacsToUpdateFirm;
                }
                else if(multipleLowHierarchy.size() > 1)
                {
                    system.debug('multipleLowHierarchy > 1');
                    Opportunity oppLastModified = new List<Opportunity>(multipleLowHierarchy)[0];
                    for(Opportunity sameHierarchyOpp : multipleLowHierarchy)
                    {
                        if(sameHierarchyOpp.LastModifiedDate > oppLastModified.LastModifiedDate)
                        {
                            oppLastModified = sameHierarchyOpp;
                        }
                    }
                    
                    aac.BestOffer__c = oppLastModified.Id;
                    aac.Expected_Enrollment_Date__c = oppLastModified.CloseDate;
                    aac.Enrolled__c = false;
                    aac.Application_Enrolled__c = null;
                    aac.First_Application_Accepted__c = null; 
                    aac.First_Application_Firmed__c = null;
                    aac.First_Application_Offered__c = null; 
                    aac.All_Applications_Rejected__c = false;
                    aac.All_Offers_Declined__c = false;
                    aac.Offered__c = false;
                    aac.Accepted__c = false;
                    aac.Deferred__c = false;
                    aac.Withdrawn__c = false;
                    aac.Agent_Submitted_Application__c = false;
                    aac.Firmed__c = false;
                    aac.All_Declined__c = false;
                    aac.AllClosedandNotEnrolled__c = false;
                    aacsToUpdate.add(aac);
                    
                }  
                else if(toUpdate)
                {
                    system.debug('in toUpdate');
                    aac.BestOffer__c = lowestHierarchy.Id;
                    aac.Expected_Enrollment_Date__c = lowestHierarchy.CloseDate;
                    aac.Enrolled__c = false;
                    aac.Application_Enrolled__c = null;
                    aac.First_Application_Accepted__c = null; 
                    aac.First_Application_Firmed__c = null;
                    aac.First_Application_Offered__c = null; 
                    aac.All_Applications_Rejected__c = false;
                    aac.All_Offers_Declined__c = false;
                    aac.Offered__c = false;
                    aac.Accepted__c = false;
                    aac.Deferred__c = false;
                    aac.Withdrawn__c = false;
                    aac.Agent_Submitted_Application__c = false;
                    aac.Firmed__c = false;
                    aac.All_Declined__c = false;
                    aac.AllClosedandNotEnrolled__c = false;
                    aacsToUpdate.add(aac);
                }
        }
        System.debug('all aacs ' + oppsByAAC);
        System.debug('aacs to update ' + aacsToUpdate);
        /**
         * KA, CT, 19/03/2020 - UOPS-5 - Error while updating collection - The fix will make sure only 
         * unique elements in the collection will be updated.
         */
        Map<Id, Applicant_Admission_Cycle__c> mapaacsUpdate = new Map<Id, Applicant_Admission_Cycle__c>();
        for(Applicant_Admission_Cycle__c acc : aacstoUpdate){
            mapaacsUpdate.put(acc.Id, acc);
        } 
        if(mapaacsUpdate.keyset().size() > 0) {
            update mapaacsUpdate.values();
        }
    }
    
}