/**
   * @File Name          : ct_VolunteerHoursTriggerHandler.cls
   * @Author             : Creation Admin
   * @Last Modified By   : Creation Admin
   * Description         : To call ct_VolunteerHoursTriggerHelper's sentEmailAlertToCompletedVolunteerHours method
  **/
public class ct_VolunteerHoursTriggerHandler {
    //While changing status to 'Completed'
    public static void beforeUpdate(Map<Id, GW_Volunteers__Volunteer_Hours__c> volunteerHourNewMap, Map<Id, GW_Volunteers__Volunteer_Hours__c> volunteerHourOldMap){
       Set<Id> volunteerHoursIds = new Set<Id>();
       for(GW_Volunteers__Volunteer_Hours__c thisVolunteerHour : volunteerHourNewMap.values()){
           if(thisVolunteerHour.GW_Volunteers__Status__c == ct_Constants.VOLUNTEER_COMPLETED_STATUS &&
              thisVolunteerHour.GW_Volunteers__Status__c != volunteerHourOldMap.get(thisVolunteerHour.Id).GW_Volunteers__Status__c) {
              volunteerHoursIds.add(thisVolunteerHour.Id);
           }
       }
       if(!volunteerHoursIds.isEmpty()){
            // Checking by pass settings for email process
            if(ct_CustomSettingService.getCurrentUserByPassConfiguration() == null
            || (!ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Emails_Off__c 
            && !ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Alumni_Emails_off__c)){
            
                ct_VolunteerHoursTriggerHelper.sentEmailAlertToCompletedVolunteerHours(volunteerHoursIds);
            }
       }
    }
    //While creating hours status with 'Completed'
    public static void afterInsert(Map<Id, GW_Volunteers__Volunteer_Hours__c> volunteerHourNewMap){
       Set<Id> volunteerHoursIds = new Set<Id>();
       for(GW_Volunteers__Volunteer_Hours__c thisVolunteerHour : volunteerHourNewMap.values()){
           if(thisVolunteerHour.GW_Volunteers__Status__c == ct_Constants.VOLUNTEER_COMPLETED_STATUS) {
              volunteerHoursIds.add(thisVolunteerHour.Id);
           }
       }
       if(!volunteerHoursIds.isEmpty()){
            // Checking by pass settings for email process
            if(ct_CustomSettingService.getCurrentUserByPassConfiguration() == null
            || (!ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Emails_Off__c 
            && !ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Alumni_Emails_off__c)){
            
                ct_VolunteerHoursTriggerHelper.sentEmailAlertToCompletedVolunteerHours(volunteerHoursIds);
            }
       }
    }
}