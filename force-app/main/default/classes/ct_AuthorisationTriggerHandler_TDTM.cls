/**
 * @File Name          : ct_AuthorisationTriggerHandler_TDTM.cls
 * @Description        : TDTM Trigger Handler class for Asperato Authorisation
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/4/2020, 1:51:27 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/1/2020   Creation Admin     Initial Version
**/
global class ct_AuthorisationTriggerHandler_TDTM extends hed.TDTM_Runnable {
  global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
  hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {

    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CT ASPERATO AUTHORISATION TDTM TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
    //Set of recurring donation Record Id to be cancelled
    Set<Id> recurringDonationToCancel  = new Set<Id>();

    if (triggerAction == hed.TDTM_Runnable.Action.AfterUpdate){
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ASPERATO AUTHORISATION AFTER UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

        //Asperato Authorisation Trigger Old Map
        Map<Id, asp04__Authorisation__c> triggerOldMap = new Map<Id, asp04__Authorisation__c>((list<asp04__Authorisation__c>)oldlist);
        //Asperato Authorisation Trigger New Map
        Map<Id, asp04__Authorisation__c> triggerNewMap = new Map<Id, asp04__Authorisation__c>((list<asp04__Authorisation__c>)newlist);

        for(asp04__Authorisation__c thisAuthorisation : triggerNewMap.values()){

          //Loop to get cancelled Recurring Donation based if authorisation is cancelled
          if(thisAuthorisation.asp04__Status__c != triggerOldMap.get(thisAuthorisation.Id).asp04__Status__c
            && thisAuthorisation.asp04__Status__c == ct_Constants.ASPERATO_AUTHORISATION_STATUS_CANCELLED
            && thisAuthorisation.Recurring_Donations__c != null){
            recurringDonationToCancel.add(thisAuthorisation.Recurring_Donations__c);
          }
        }
      if(!recurringDonationToCancel.isEmpty()){
        //Cancel Recurring donations
        ct_AuthorisationTriggerHelper.directDebitGoCardlessCancellationProcess(recurringDonationToCancel);
      }
    }
    return dmlWrapper;
  }
}