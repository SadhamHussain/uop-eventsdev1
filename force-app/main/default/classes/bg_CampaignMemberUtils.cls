////////////////////////////////////////////////////////////////////////////////
//  bg_CampaignMemberUtils
//
//  Utils class for the CampaignMember object
//
//	@Author:		Thomas Packer - BrightGen Ltd
//	@Created:		07/08/2019
//
//	@Changes: 
////////////////////////////////////////////////////////////////////////////////
public without sharing class bg_CampaignMemberUtils 
{
    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 21st January 2019
	//  @description: A method to return campaign members by campaign Id
	//
	//  @changes:	
	////////////////////////////////////////////////////////////////////////////
    public static Map<Id, CampaignMember> GetCampaignMembersFromCampaignId(Id campaignId)
    {
        Map<Id, CampaignMember> members = new Map<Id, CampaignMember>([SELECT Id,
                                                                              Status, 
                                                                              FirstName, 
                                                                              LastName,
                                                                              ContactId,
                                                                              LeadId,
                                                                              Email
                                                                       FROM CampaignMember
                                                                       WHERE CampaignId = :campaignId]);
        
        if (!members.isEmpty())
        {
            return members;
        }

        return null;
    }

    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 7th August 2019
	//  @description: A method to return campaign members by Id
	//
	//  @changes:	
	////////////////////////////////////////////////////////////////////////////
    public static Map<Id, CampaignMember> GetCampaignMembers(Set<Id> campaignMemberIds)
    {
        Map<Id, CampaignMember> members = new Map<Id, CampaignMember>([SELECT Id,
                                                                              Status
                                                                       FROM CampaignMember
                                                                       WHERE Id IN :campaignMemberIds]);
        
        if (!members.isEmpty())
        {
            return members;
        }

        return null;
    }
}