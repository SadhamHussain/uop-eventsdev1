public class bg_RelationshipUtils 
{
	private static List<hed__Relationship__c> createListOfRelationshipsToBeInserted(Map<Id, Map<Id, Lead>> relatedContactsByAppId)
	{
		List<hed__Relationship__c> relationshipsToBeCreated = new List<hed__Relationship__c>();

		for(Id appId : relatedContactsByAppId.keySet())
		{
			Map<Id, Lead> convertedEnquirerByContactId = relatedContactsByAppId.get(appId);
			hed__Relationship__c rel = new hed__Relationship__c();

			for(Id contactId : convertedEnquirerByContactId.keySet())
			{
				Lead enq = convertedEnquirerByContactId.get(contactId);
				if(enq.Influencer__c != null)
				{
					rel.hed__RelatedContact__c = contactId;
				}
				else
				{
					rel.hed__Contact__c = contactId;
					rel.Start_Date__c = Date.today();

					if(enq.Type__c != null)
					{
						rel.hed__Type__c = enq.Type__c;
					}
				}
			}
			relationshipsToBeCreated.add(rel);
		}

		return relationshipsToBeCreated;
	}

	public static void createRelationshipRecords(Map<Id, Map<Id, Lead>> relatedContactsByAppId)
	{
		List<hed__Relationship__c> relationshipsToBeCreated = createListOfRelationshipsToBeInserted(relatedContactsByAppId);

		Database.SaveResult[] insertedRelationships = Database.insert(relationshipsToBeCreated, false);

		Set<Id> successfulEnquirerIds = new Set<Id>();
		List<Lead> successfulEnquirers = new List<Lead>();
		List<String> errors = new List<String>();
		
		for(Integer i = 0; i < insertedRelationships.size(); i++) 
		{

	       if(insertedRelationships[i].isSuccess())
	       {
	       		system.debug('+++ the following record is successful: ' + insertedRelationships[i]);
	       }
	       else
	       {
		       	String errorsConcatenated = '';

		        for(Database.Error error : insertedRelationships[i].getErrors()) 
		        {
		            if(String.isEmpty(errorsConcatenated)) 
		            {
		                errorsConcatenated = error.getMessage();
		            } 
		            else 
		            {
		                errorsConcatenated = errorsConcatenated + ' | ' + error.getMessage();
		            }
		        }

		        errors.add(errorsConcatenated);
	       }
	    }

	    if(!errors.isEmpty())
	    {
	    	system.debug('+++ errors: ' + errors);
	    }	
	}
}