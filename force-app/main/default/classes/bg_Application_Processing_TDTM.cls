/*****************************************************************
* bg_Application_Processing_TDTM
*
* Application Trigger Handler for processing Application records
* 
******************************************************************/

global class bg_Application_Processing_TDTM extends hed.TDTM_Runnable {
    
    /*
* Main method for handling logic for each trigger context
*/
    public override DmlWrapper run(List<SObject> newList, List<SObject> oldList, 
                                   Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {
        Bypass_Configuration__c configBypass = Bypass_Configuration__c.getInstance(UserInfo.getProfileId());
        if(!configBypass.Are_Processes_Off__c)
        {
            if (triggerAction == Action.BeforeInsert)
            {
                List<Application__c> newApplications = newList;
                bg_ApplicationUtils.setupNewApplicationsPreProcessing(newApplications);
            }
            
            else if (triggerAction == Action.AfterInsert)
            {
                if(!bg_ApplicationUtils.isQueueableJobCurrentlyRunning())
                {
                    
                    List<Application__c> newApplicationsToBeProcessed = newList;
                    Set<Id> newApplicationIds = (new Map<Id, SObject>(newApplicationsToBeProcessed)).keySet();
                    System.enqueueJob(new bg_ApplicationProcessingQueuable(newApplicationIds));
                }
            }
            
            else if (triggerAction == Action.BeforeUpdate)
            {
                List<Application__c> newApplications = newList;
                List<Application__c> oldApplications = oldList;
                Map<Id,Application__c> oldApplicationsById = new Map<Id,Application__c>(oldApplications);
                
                bg_ApplicationUtils.processUpdatedApplicationsBeforeUpdate(newApplications, oldApplicationsById);
            }        
            else if (triggerAction == Action.AfterUpdate)
            {
                List<Application__c> newApplications = newList;
                List<Application__c> oldApplications = oldList;
                Map<Id,Application__c> oldApplicationsById = new Map<Id,Application__c>(oldApplications);
                
                bg_ApplicationUtils.processApplicationsAfterUpdate(newApplications, oldApplicationsById);
            }
            
        }
        
        
        return null;
    }
    
    
}