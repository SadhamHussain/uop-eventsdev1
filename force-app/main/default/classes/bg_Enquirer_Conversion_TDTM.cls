/*****************************************************************
* bg_Enquirer_CompanyFormatting_TDTM
*
* Enquirer Trigger Handler for checking Company field format BeforeInsert or BeforeUpdate
* 
* Author: Stuart Barber
* Created: 12-09-2018
******************************************************************/

global class bg_Enquirer_Conversion_TDTM extends hed.TDTM_Runnable {
	
	/*
    *
    * Trigger Handler on BeforeInsert & BeforeUpdate for checking if Enquirer is converted
    *
    */
	public override DmlWrapper run(List<SObject> newlist, List<SObject> oldlist, 
    Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {
		if (triggerAction == Action.BeforeInsert || triggerAction == Action.BeforeUpdate)
		{
			List<Lead> enquirerRecords = newlist;

			setConversionFlagUponConversionOfEnquirer(enquirerRecords);
		}

		return null;
	}

	/*
    *
    * To set Has Enquirer Converted field to true upon conversion of Enquirer (used for matching criteria
    * on Formstack webforms)
    *
    */
	private static void setConversionFlagUponConversionOfEnquirer(List<Lead> enquirerRecords)
	{
		for(Lead enquirer : enquirerRecords)
		{
			if(enquirer.isConverted)
			{
				enquirer.Has_Enquirer_Converted__c = true;
			}
		}
	}
}