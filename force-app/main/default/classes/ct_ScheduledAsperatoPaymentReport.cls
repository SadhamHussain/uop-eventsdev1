/**
 * @File Name          : ct_ScheduledAsperatoPaymentReport.cls
 * @Description        : Using this class to fetch Asperato Payments Transations for last month and make csv 
 *                       to send "Payment Txn Recipients" group users on monthly basis. 
 * @Author             : Creation Admin  - Creation Technology Solutions
 * @Last Modified By   : Rajeshkumar Creation
 * @Last Modified On   : 7/09/2020, 11:23:18 AM
**/
global class ct_ScheduledAsperatoPaymentReport implements System.Schedulable {
    global void execute(SchedulableContext sc) {         
            //Generate CSV using Asperato records      
            Blob csvData = Blob.valueOf(ct_PaymentsExporterCtlr.fetchAsperatoTxnData(null, null));
            //Make the report as csv and send email to "Payment Txn Recipients" group users
            if(csvData.size() > 0){
                //Collect Org wide address
                Id orgWideAddressId = ct_Util.getOrgWideEmail(ct_Constants.UOP_ADVANCEMENT_TEAM_ADDRESS)[0].Id;
               
                String sessionId = UserInfo.getSessionId();
                Messaging.EmailFileAttachment thisAttachment = new Messaging.EmailFileAttachment();
                //Setting file name
                thisAttachment.setFileName('salesforce_donations_'+System.now().year()+System.now().format('MM')+System.now().format('dd')+'.csv');
                //Attach the generated CSV file
                thisAttachment.setBody(!Test.IsRunningTest() ? csvData : Blob.valueOf('Payment(ASP) Transactions Report'));
                thisAttachment.setContentType('application/vnd.ms-excel');
                Messaging.SingleEmailMessage thisSingleMessage = new Messaging.SingleEmailMessage();
                //Set Org wide address
                if (String.IsNotBlank(orgWideAddressId)) { 
                    thisSingleMessage.setOrgWideEmailAddressId(orgWideAddressId); 
                }
                //Attach the csv report 
                thisSingleMessage.setFileAttachments(new Messaging.EmailFileAttachment[] { thisAttachment } );
                thisSingleMessage.setSubject('Donation Transactions Daily Report - salesforce_donations_'+System.now().year()+System.now().format('MM')+System.now().format('dd')+' csv');
                //Set Email content using custom label
                thisSingleMessage.setPlainTextBody(System.Label.Payment_Transactions_Email_Content);
                //Set Email Recipients
                thisSingleMessage.setToAddresses(new List<String>{System.Label.Payment_Txn_Recipients});
                //Checking bypass settings before sending emails
                if(!Test.isRunningTest()
                && (ct_CustomSettingService.getCurrentUserByPassConfiguration() == null
                    || (!ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Emails_Off__c && 
                        !ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Alumni_Emails_off__c))){
                    //Send Email for collected recipients
                    Messaging.sendEmail( new Messaging.SingleEmailMessage[] { thisSingleMessage } );
                }
            }
        }
}