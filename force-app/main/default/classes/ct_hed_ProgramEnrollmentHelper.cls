/**
 * @File Name          : ct_hed_ProgramEnrollmentHelper.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 5/25/2020, 2:01:03 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/17/2020   Creation Admin     Initial Version
**/
public class ct_hed_ProgramEnrollmentHelper {
  /**
  * @description - Method to update Affiliation role and status to Student and Alumni when program enrollment is completed
  * @author Creation Admin | 4/17/2020 
  * @param Set<Id> affiliationIds - Set of Affiliation Ids
  * @return List<hed__Affiliation__c> - list of affiliation to update
  **/
  public static List<hed__Affiliation__c> updateRelatedAffiliationRoleAndStatus(Set<Id> affiliationIds){
    List<hed__Affiliation__c> affiliationListToUpdate = new List<hed__Affiliation__c>();
    for(hed__Affiliation__c thisAffiliation : [SELECT Id, hed__Role__c, hed__Status__c
                                                      FROM hed__Affiliation__c
                                                      WHERE Id IN: affiliationIds
                                                      AND (hed__Role__c !=: ct_Constants.AFFILIATION_ROLE_STUDENT
                                                      OR hed__Status__c !=: ct_Constants.AFFILIATION_STATUS_ALUMNI)]){
      thisAffiliation.hed__Role__c    = ct_Constants.AFFILIATION_ROLE_STUDENT;
      thisAffiliation.hed__Status__c  = ct_Constants.AFFILIATION_STATUS_ALUMNI; 
      affiliationListToUpdate.add(thisAffiliation);                                           
    }
    return affiliationListToUpdate;
  }
  /**
  * @description-  Method to query the contact and update latest course details
  * @param Set<Id> contactIds 
  * @return void 
  **/
  @Future
  public static void updateLatestCourseDetailsToRelatedContact(Set<Id> contactIds){
    stampContactsWithLatestProgramEnrollmentRecords(contactIds);
  }

  /**
  * @description - Method to query latest program enrollment record and populate course details to contact
  * @param Set<Id> contactIds -  Set of ContactIds
  * @param Map<Id Contact> contactMap - Contact records stored in map
  * @return void 
  **/
  public static void stampContactsWithLatestProgramEnrollmentRecords(Set<Id> contactIds){
    List<Contact> contactListToUpdate = new List<Contact>();
    // Fetch Latest program enrollment record using the contact Id
    for(Contact thisContact : [SELECT Id, RecordTypeId, Last_Course_Year__c, Last_Course_Taken__c,
                                      (SELECT Id, hed__Account__c, hed__Account__r.Name, hed__Graduation_Year__c, hed__End_Date__c, hed__Contact__c, CreatedDate 
                                      FROM hed__Program_Enrollments__r
                                      ORDER BY hed__Graduation_Year__c DESC NULLS LAST, CreatedDate DESC
                                      LIMIT 1)
                                      FROM Contact
                                      WHERE Id IN: contactIds
                                      AND RecordTypeId =: ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT]){
      if(thisContact.hed__Program_Enrollments__r.size() == 0){
        // null those values because there is no related program enrollment for this contact
        thisContact.Last_Course_Year__c  = null;
        thisContact.Last_Course_Taken__c = null;
        // Add thisContact record to update
        contactListToUpdate.add(thisContact);
      }
      else if(thisContact.hed__Program_Enrollments__r.size() == 1
      && (thisContact.Last_Course_Year__c != thisContact.hed__Program_Enrollments__r[0].hed__Graduation_Year__c
      || thisContact.Last_Course_Taken__c != thisContact.hed__Program_Enrollments__r[0].hed__Account__r.Name)){
        //Populate latest Course year and Course Taken from the program enrollment
        thisContact.Last_Course_Year__c   = thisContact.hed__Program_Enrollments__r[0].hed__End_Date__c != null ? thisContact.hed__Program_Enrollments__r[0].hed__Graduation_Year__c : null;
        thisContact.Last_Course_Taken__c  = thisContact.hed__Program_Enrollments__r[0].hed__Account__r.Name;
        // Add thisContact record to update
        contactListToUpdate.add(thisContact);
      }
    }
    try{
      update contactListToUpdate;
    }
    catch(Exception ex){
      ct_Logger.logMessage(ex, 'Contact');
      throw ex;
    }
  }
}