/*****************************************************************
* bg_FindDuplicateRecords_Test
*
* test methods for bg_FindDuplicateRecords
*
* Author: BrightGen
* Created: 27-11-2018
******************************************************************/

@isTest
private class bg_FindDuplicateRecords_Test 
{
	
	@isTest 
	private static void testApplicationsWithNoDuplicatesFound() 
	{
		Queuable_Job_Size__c qjs = new Queuable_Job_Size__c(Name = 'Queuable Job Size', Job_Size__c = 50);
        insert qjs;

		Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount.Course_Start_Month__c = '09';
		courseAccount.Course_Start_Year__c = '2020';
	    insert courseAccount;

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp = bg_UnitTestDataFactory.createApplication();
	    testApp.First_Name__c = 'testNoMatch'; 
	    testApp.Last_Name__c = 'Duplicate';
	    testApp.Email_Address__c = 'testNoMatch@Duplicate.com';
	    testApp.Phone_Number__c = '0739357673';
	    testApp.Postal_Code__c = 'E2 8YS';
	    testApp.Course__c = courseAccount.Id;
	    testApp.School_College__c = schoolAccount.Id;
	    insert testApp;

	    Lead tempEnquirer = bg_UnitTestDataFactory.createStudentEnquirer();
		tempEnquirer.FirstName = 'testNoMatch';
		tempEnquirer.LastName = 'Duplicate';
		tempEnquirer.Email = 'testNoMatch@Duplicate.com';
		tempEnquirer.Phone = '01403785206';
		tempEnquirer.PostalCode = 'E2 8YS';
		tempEnquirer.Most_Recent_Application__c = testApp.Id;

		List<Lead> tempEnquirers = new List<Lead>{tempEnquirer};

		Test.startTest();

			Map<Id, Set<Lead>> enquirerSetByAppIdMap = bg_FindDuplicateRecords.checkForApplicationsRelatedToZeroToManyEnquirers(tempEnquirers);

			system.assert(!enquirerSetByAppIdMap.isEmpty(), 'The map should be populated');

			for(Id appId : enquirerSetByAppIdMap.keySet())
			{
				Set<Lead> enquirerSet = enquirerSetByAppIdMap.get(appId);

				system.assertEquals(testApp.Id, appId, 'The key should be the Application inserted');
				system.assertEquals(1, enquirerSet.size(), 'A single enquirer should be contained in the Set');
				
				for(Lead enq : enquirerSet)
				{
					system.assertEquals(null, enq.Id, 'The enquirer should be a temporary (in-memory) enquirer');	
				}
			}

		Test.stopTest();
	}

	@isTest 
	private static void testApplicationsWithSingleDuplicateEnquirerFound() 
	{
		Queuable_Job_Size__c qjs = new Queuable_Job_Size__c(Name = 'Queuable Job Size', Job_Size__c = 50);
        insert qjs;

		Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount.Course_Start_Month__c = '09';
		courseAccount.Course_Start_Year__c = '2020';
	    insert courseAccount;

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp = bg_UnitTestDataFactory.createApplication();
	    testApp.First_Name__c = 'testSingle'; 
	    testApp.Last_Name__c = 'Duplicate';
	    testApp.Email_Address__c = 'testSingle@Duplicate.com';
	    testApp.Phone_Number__c = '0739357673';
	    testApp.Postal_Code__c = 'E2 8YS';
	    testApp.Course__c = courseAccount.Id;
	    testApp.School_College__c = schoolAccount.Id;
	    insert testApp;

	    Lead existingEnquirer = bg_UnitTestDataFactory.createStudentEnquirer();
		existingEnquirer.FirstName = 'testSingle';
		existingEnquirer.LastName = 'Duplicate';
		existingEnquirer.Email = 'testSingle@Duplicate.com';
		existingEnquirer.Phone = '01403785206';
		existingEnquirer.PostalCode = 'E2 8YS';
		insert existingEnquirer;

	    Lead tempEnquirer = bg_UnitTestDataFactory.createStudentEnquirer();
		tempEnquirer.FirstName = 'testSingle';
		tempEnquirer.LastName = 'Duplicate';
		tempEnquirer.Email = 'testSingle@Duplicate.com';
		tempEnquirer.Phone = '01403785206';
		tempEnquirer.PostalCode = 'E2 8YS';
		tempEnquirer.Most_Recent_Application__c = testApp.Id;

		List<Lead> tempEnquirers = new List<Lead>{tempEnquirer};

		Test.startTest();

			Map<Id, Set<Lead>> enquirerSetByAppIdMap = bg_FindDuplicateRecords.checkForApplicationsRelatedToZeroToManyEnquirers(tempEnquirers);

			system.assert(!enquirerSetByAppIdMap.isEmpty(), 'The map should be populated');

			for(Id appId : enquirerSetByAppIdMap.keySet())
			{
				Set<Lead> enquirerSet = enquirerSetByAppIdMap.get(appId);

				system.assertEquals(testApp.Id, appId, 'The key should be the Application inserted');
				system.assertEquals(1, enquirerSet.size(), 'A single enquirer should be contained in the Set');
				
				for(Lead enq : enquirerSet)
				{
					system.assertEquals(existingEnquirer.Id, enq.Id, 'The enquirer should an existing enquirer');	
				}
			}

		Test.stopTest();
	}

	@isTest 
	private static void testApplicationsWithMultipleDuplicateEnquirersFound() 
	{
		Queuable_Job_Size__c qjs = new Queuable_Job_Size__c(Name = 'Queuable Job Size', Job_Size__c = 50);
        insert qjs;

		Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount.Course_Start_Month__c = '09';
		courseAccount.Course_Start_Year__c = '2020';
	    insert courseAccount;

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp = bg_UnitTestDataFactory.createApplication();
	    testApp.First_Name__c = 'testMultiple'; 
	    testApp.Last_Name__c = 'Duplicate';
	    testApp.Email_Address__c = 'testMultiple@Duplicate.com';
	    testApp.Phone_Number__c = '0739357673';
	    testApp.Postal_Code__c = 'E2 8YS';
	    testApp.Course__c = courseAccount.Id;
	    testApp.School_College__c = schoolAccount.Id;
	    insert testApp;

	    Lead existingEnquirer1 = bg_UnitTestDataFactory.createStudentEnquirer();
		existingEnquirer1.FirstName = 'testMultiple';
		existingEnquirer1.LastName = 'Duplicate';
		existingEnquirer1.Email = 'testMultiple@Duplicate.com';
		existingEnquirer1.Phone = '01403785206';
		existingEnquirer1.PostalCode = 'E2 8YS';

		Lead existingEnquirer2 = bg_UnitTestDataFactory.createStudentEnquirer();
		existingEnquirer2.FirstName = 'tm';
		existingEnquirer2.LastName = 'Duplicate';
		existingEnquirer2.Email = 'testMultiple@Duplicate.com';
		existingEnquirer2.Phone = '01403785206';
		existingEnquirer2.PostalCode = 'E2 8YS';
		
		List<Lead> existingEnquirers = new List<Lead>{existingEnquirer1, existingEnquirer2};
		insert existingEnquirers;

		Set<Id> existingEnquirerIds = new Set<Id>{existingEnquirer1.Id, existingEnquirer2.Id};

	    Lead tempEnquirer = bg_UnitTestDataFactory.createStudentEnquirer();
		tempEnquirer.FirstName = 'testMultiple';
		tempEnquirer.LastName = 'Duplicate';
		tempEnquirer.Email = 'testMultiple@Duplicate.com';
		tempEnquirer.Phone = '01403785206';
		tempEnquirer.PostalCode = 'E2 8YS';
		tempEnquirer.Most_Recent_Application__c = testApp.Id;

		List<Lead> tempEnquirers = new List<Lead>{tempEnquirer};

		Test.startTest();

			Map<Id, Set<Lead>> enquirerSetByAppIdMap = bg_FindDuplicateRecords.checkForApplicationsRelatedToManyEnquirers(tempEnquirers);

			system.assert(!enquirerSetByAppIdMap.isEmpty(), 'The map should be populated');

			for(Id appId : enquirerSetByAppIdMap.keySet())
			{
				Set<Lead> enquirerSet = enquirerSetByAppIdMap.get(appId);

				system.assertEquals(testApp.Id, appId, 'The key should be the Application inserted');
				system.assertEquals(2, enquirerSet.size(), 'A single enquirer should be contained in the Set');
				
				for(Lead enq : enquirerSet)
				{
					system.assert(existingEnquirerIds.contains(enq.Id), 'The set should be populated with the two existing Enquirer Ids');	
				}
			}

		Test.stopTest();
	}

}