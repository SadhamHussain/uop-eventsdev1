/**
 * @File Name          : ct_ContentDocumentLinkHelper.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : Helper class for ct_ContentDocumentLinkTrigger
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 5/4/2020, 2:04:32 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/19/2020   Creation Admin     Initial Version
**/
public class ct_ContentDocumentLinkHelper {

  /**
  * @description Method to invoke functions in after trigger context
  * @author Creation Admin | 17/4/2020 
  * @param documentLinkNewMap 
  * @return void 
  **/
  public static void afterInsert(Map<Id, ContentDocumentLink> documentLinkNewMap){
    Set<Id> recordIds = new Set<Id>();
    // Store Recurring donation Id and content documentlinkId in a Map
    Map<Id, Set<Id>> recurringDonationIdMap = new Map<Id, Set<Id>>();
    Map<Id, ContentDocumentLink>  contentDocumentLinkMap = new Map<Id, ContentDocumentLink>();
    for(ContentDocumentLink thisLink : documentLinkNewMap.values()){
      // Check whether the sobjecttype of Entity type Id is Recurring Donation
      if(thisLink.LinkedEntityId.getSobjectType() == ct_Constants.IS_RECURRING_DONATION){
        recordIds.add(thisLink.LinkedEntityId);
        if(!recurringDonationIdMap.containskey(thisLink.LinkedEntityId)){
          recurringDonationIdMap.put(thisLink.LinkedEntityId, new Set<Id>{});
        }
        recurringDonationIdMap.get(thisLink.LinkedEntityId).add(thisLink.contentDocumentId);
        contentDocumentLinkMap.put(thisLink.contentDocumentId, thisLink);
      }
    }
    if(!recurringDonationIdMap.isEmpty()){
      addAttachmentsToChildOpportunities(recurringDonationIdMap, contentDocumentLinkMap, recordIds);
    }
  }

  /**
  * @addAttachmentsToChildOpportunities - Method to add attachments to the Child opportunities once DD attached to Recurring donation 
  * @author Creation Admin | 3/19/2020 
  * @param Map<Id Id> recurringDonationIdMap - to store recurring donation Id and content document Id
  * @param Map<Id ContentDocumentLink> contentDocumentLinkMap - store Content Document Id and content document link record
  **/
  public static void addAttachmentsToChildOpportunities(Map<Id, Set<Id>> recurringDonationIdMap, Map<Id, ContentDocumentLink> contentDocumentLinkMap, Set<Id> recordIds){
    List<ContentDocumentLink> contentDocumentLinkListToInsert = new List<ContentDocumentLink>();
    // Query to fetch all the child opportunities under the recurring donation
    for(Opportunity thisOpportunity : [SELECT Id, npe03__Recurring_Donation__c 
                                                              FROM Opportunity
                                                              WHERE (Id IN: recordIds
                                                              OR npe03__Recurring_Donation__c IN: recordIds)]){
      if(recurringDonationIdMap.containsKey(thisOpportunity.npe03__Recurring_Donation__c)){
        //Attach the document for the child opportunities
        contentDocumentLinkListToInsert.addAll(constructContentDocumentLink(contentDocumentLinkMap,  thisOpportunity.Id,  recurringDonationIdMap.get(thisOpportunity.npe03__Recurring_Donation__c)));
      }
    }
    try{
      //insert the content document link for all child opportunities
      insert contentDocumentLinkListToInsert;
    }
    catch(Exception e){
      ct_Logger.logMessage(e, 'ContentDocumentLink');
      throw e;
    } 
  }
  /**
  * @constructContentDocumentLink - Method to construct Content Document Link for child opportunities
  * @param ContentDocumentLink thisLink 
  * @param Id OpportunityId 
  * @return ContentDocumentLink 
  **/
  public static List<ContentDocumentLink> constructContentDocumentLink( Map<Id, ContentDocumentLink> contentDocumentLinkMap, Id OpportunityId, Set<Id> contentDocumnetIds){
    //creating new contentdocumentlink for the opportunity and returns the list of contentdocumentlink
    List<ContentDocumentLink> contentDocumentLinkList = new List<ContentDocumentLink>();
    for(Id thisId : contentDocumnetIds){
      ContentDocumentLink newLink = new ContentDocumentLink();
      ContentDocumentLink oldLink = contentDocumentLinkMap.get(thisId);
      newLink.ContentDocumentId = oldLink.ContentDocumentId;
      newLink.LinkedEntityId    = OpportunityId;
      newLink.ShareType         = oldLink.ShareType;
      newLink.Visibility        = oldLink.Visibility;
      contentDocumentLinkList.add(newLink);
    }
    return contentDocumentLinkList;
  }
}