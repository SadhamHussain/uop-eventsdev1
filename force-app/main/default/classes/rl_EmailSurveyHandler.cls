/*******************************************************************************************************************************
* @name:           rl_EmailSurveyHandler
* @description:    Controller for pages that collect responses from email surveys
* 
* PAGE PARAMETERS
* @rating:  	   Rating with value of 1 - 5
* @caseId:         15 or 18 character case ID which needs to be updated with the rating
*
* METHODS
* @createEmailSurveyRecord():							Creates an Email_Survey__c record with Related Case and Rating populated. Currently utilised by rl_EmailSurveyPage.vfp
* @updateRelatedCaseRating(List<Id> emailSurveyIds):	Uses the Related Case ID text field to find the related Case object and update it with the rating on the Email Survey
*														Currently called as an Apex Action by Process Builder "Update Case With Email Survey"
*
* @Created:   19-04-2020 v0.1 - initial version - Rob Liesicke
* @Updated:   20-04-2020 v0.2 - Added invocable method to process related case - Rob Liesicke

********************************************************************************************************************************/

public class rl_EmailSurveyHandler  
{
    public String rating { get; set; }
    public String caseId { get; set; }

    
/* =========================================================== */   
/*                       CONSTRUCTOR                     	   */
/* =========================================================== */ 
    
    public rl_EmailSurveyHandler() 
    {
        // Get rating and case ID from current page
        rating = apexpages.currentpage().getparameters().get('rating');
        caseId = apexpages.currentpage().getparameters().get('caseId');      
    }	

    
/* =========================================================== */   
/*                    CREATE EMAIL SURVEY                  	   */
/* =========================================================== */ 
    
    public void createEmailSurveyRecord()
    {
        
        // If Rating and Case ID are not null, create an email survey record
        if( rating != null && caseId != null)
        {
        
            Email_Survey__c emailSurvey = new Email_Survey__c();
    
            emailSurvey.rating__c = rating;
            
            emailSurvey.Related_Case_Id__c = caseId;
            
            try 
            {
                insert emailSurvey;
            } 
            
            catch(DmlException e) 
            {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
            
        }
        
    }
   
/* =========================================================== */   
/*                UPDATE RATING ON RELATED CASES          	   */
/* =========================================================== */     
    
    @InvocableMethod
    public static void updateRelatedCaseRating(List<Id> emailSurveyIds)
    {
        List<Id> caseIds = new List<Id>();
        List<Case> updatedCases = new List<Case>();
		
        
        // Get Email Survey records for provided Ids
        List<Email_Survey__c> emailSurveyRecords = [ SELECT Id, Rating__c, Related_Case_Id__c 
                                                     FROM Email_Survey__c 
                                                     WHERE Id IN :emailSurveyIds ];
        
        
        // Extract the related case IDs into a list
        for(Email_Survey__c es : emailSurveyRecords)
        {
            caseIds.add(es.Related_Case_Id__c);
        }
        
        
        // Create a map of the cases found in the case list
        Map<Id,Case> caseRecords = new Map<Id,Case>  ([ SELECT Id, Rating__c
                                   						FROM Case
                                  						WHERE Id IN :caseIds ]);
        
        
        // For each email survey, update the rating on the related case
        for(Email_Survey__c es : emailSurveyRecords)
        {
            Case relatedCase = caseRecords.get(es.Related_Case_Id__c);
            
            relatedCase.Rating__c = es.Rating__c;
            
            updatedCases.add(relatedCase);
        }
        
        
        // Bulk update related cases
        try 
        {
        	update updatedCases;
        } 
        
        catch(DmlException e) 
        {
        	System.debug('The following exception has occurred: ' + e.getMessage());
		}        
        
    }
    
}