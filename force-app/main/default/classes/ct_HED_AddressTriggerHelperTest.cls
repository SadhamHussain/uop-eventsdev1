/**
 * @File Name          : ct_HED_AddressTriggerHelperTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 24/6/2020, 8:13:45 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/6/2020   Creation Admin     Initial Version
**/
@IsTest(SeeAllData = true)
public class ct_HED_AddressTriggerHelperTest {
    @IsTest
    static void testSetAddressAsPrivate(){
      Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
      thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE;
      thisAccount.AAA_Private_Record__c = true;
      insert thisAccount;
    
      Contact thisContact   = ct_TestDataFactory.createContact();
      thisContact.AccountId = thisAccount.Id;
      insert thisContact;

      hed__Address__c thisAddress = ct_TestDataFactory.createHedAddress(thisContact.id);

      Test.starttest();
      insert thisAddress;
      Test.stoptest();

      System.assertEquals(true, [SELECT Id, AAA_Private_Record__c FROM hed__Address__c WHERE Id = :thisAddress.Id].AAA_Private_Record__c);
    }
}