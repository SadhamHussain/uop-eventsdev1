/*
* Test Class - rl_DuplicateCheckCustomMerge_Test
*
* This class tests rl_DuplicateCheckCustomMerge, which is a class used to customise the 
* behaviour of Duplicate Check (Managed Package) merges. 
*
* Created By: Rob Liesicke (UoP)
* Created On: 8th November 2019
*
* Changes:
*
* N/A
*
*/

@isTest
private class rl_DuplicateCheckCustomMerge_Test 
{
 
   /* 
    *
    * Create test data for use by other test class methods
    *
    */
    
    @testSetup static void setupDate() 
    {
    
    // Uses unit test helper to create lead one, and updates email to make it identifiable later
    Lead leadOne = bg_UnitTestDataFactory.createEnquirer();
    leadOne.Email = 'lead@one.com';
    insert leadOne;
 
    // Uses unit test helper to create lead two, and updates email to make it identifiable later    
    Lead leadTwo = bg_UnitTestDataFactory.createEnquirer();
    leadTwo.Email = 'lead@two.com';
    insert leadTwo;

    // Uses unit test helper to create lead three, and updates first name to make it identifiable later    
    Lead leadThree = bg_UnitTestDataFactory.createEnquirer();
    leadThree.Email = '';
    leadThree.FirstName = 'Lead Three';
    insert leadThree;
    
    }
    
    
   /* 
    *
    * Test that, IF number of email addresses is two or more AND master record email is not blank
    * second email address is copied to email alternative on master record
    *
    */    
  
    @isTest static void testStoreChildEmailAsAlternative()   
    {
    
    String objectPrefix = '00Q';
    
    Sobject masterLead = [SELECT Id, Email, Email_Alternative__c FROM Lead WHERE Email = 'lead@one.com'];
    Sobject childLead = [SELECT Id, Email, Email_Alternative__c FROM Lead WHERE Email = 'lead@two.com'];    
    
    List<sobject> childLeads = new List<sobject>(); 
    childLeads.add(childLead);
    
    
    Test.startTest();
    
        rl_DuplicateCheckCustomMerge rl_DuplicateCheckCustomMerge = new rl_DuplicateCheckCustomMerge();
        rl_DuplicateCheckCustomMerge.beforeMerge(objectPrefix,masterLead,childLeads);
   
    Test.stopTest();
    
    Lead checkMasterLead = [SELECT Id, Email, Email_Alternative__c FROM Lead WHERE Email = 'lead@one.com'];
    
    // Check that the email address from the child record is populated on Email Alternative on the master record    
    system.assertEquals(checkMasterLead.Email_Alternative__c, 'lead@two.com');
  }
  


   /* 
    *
    * Test that, IF email list size is one AND master record email is blank
    * copy second email address to email on master record
    *
    */  
  
    @isTest static void testStoreChildEmailAddressAsEmail() 
    {
    
    String objectPrefix = '00Q';
    
    Sobject masterLead = [SELECT Id, Email, Email_Alternative__c, FirstName FROM Lead WHERE FirstName = 'Lead Three'];
    Sobject childLead = [SELECT Id, Email, Email_Alternative__c FROM Lead WHERE Email = 'lead@two.com'];    
    
    List<sobject> childLeads = new List<sobject>(); 
    childLeads.add(childLead);
    
    
    Test.startTest();
    
        rl_DuplicateCheckCustomMerge rl_DuplicateCheckCustomMerge = new rl_DuplicateCheckCustomMerge();
        rl_DuplicateCheckCustomMerge.beforeMerge(objectPrefix,masterLead,childLeads);
   
    Test.stopTest();
    
    Lead checkMasterLead = [SELECT Id, Email, Email_Alternative__c, FirstName FROM Lead WHERE FirstName = 'Lead Three'];
    
    // Check that the email from the child record is populated on the email on the master record    
    system.assertEquals(checkMasterLead.Email, 'lead@two.com');
    
    }



   /* 
    *
    * Test that, IF list size is two or more AND master record email is blank
    * copy first email address to email on master record AND
    * copy second email address to email alternative on master record
    *
    */  

  @isTest static void testStoreBothChildEmailAddresses() 
  {
    String objectPrefix = '00Q';
    
    Sobject masterLead = [SELECT Id, Email, Email_Alternative__c, FirstName FROM Lead WHERE FirstName = 'Lead Three'];
    Sobject childLeadOne = [SELECT Id, Email, Email_Alternative__c FROM Lead WHERE Email = 'lead@one.com']; 
    Sobject childLeadTwo = [SELECT Id, Email, Email_Alternative__c FROM Lead WHERE Email = 'lead@two.com'];    
    
    List<sobject> childLeads = new List<sobject>(); 
    childLeads.add(childLeadOne);
    childLeads.add(childLeadTwo);    
    
    Test.startTest();
    
        rl_DuplicateCheckCustomMerge rl_DuplicateCheckCustomMerge = new rl_DuplicateCheckCustomMerge();
        rl_DuplicateCheckCustomMerge.beforeMerge(objectPrefix,masterLead,childLeads);
   
    Test.stopTest();
    
    Lead checkMasterLead = [SELECT Id, Email, Email_Alternative__c, FirstName FROM Lead WHERE FirstName = 'Lead Three'];
     
    // Check that both email addresses in the child records are populated on the master record    
    system.assertEquals(checkMasterLead.Email, 'lead@one.com');
    system.assertEquals(checkMasterLead.Email_Alternative__c, 'lead@two.com');    
   }
   

   /* 
    *
    * The remaining 2 methods in the class do not contain any code, 
    * so this test is only for code coverage
    *
    */
        
      @isTest static void testRemainingMethods() 
      { 
      
      String objectPrefix = '00Q';
      Sobject masterRecord = new Lead();
      Set<id> mergedRecordsIds = new Set<id>();
      dupcheck.dc3Exception.MergeException exceptionData = new dupcheck.dc3Exception.MergeException();
      
      rl_DuplicateCheckCustomMerge rl_DuplicateCheckCustomMerge = new rl_DuplicateCheckCustomMerge();
      
      rl_DuplicateCheckCustomMerge.mergeFailed(objectPrefix,masterRecord,mergedRecordsIds,exceptionData);
      
      rl_DuplicateCheckCustomMerge.afterMerge(objectPrefix,masterRecord,mergedRecordsIds);
    
      }
  
}