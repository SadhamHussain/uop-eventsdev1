/**
 * @File Name          : ct_DirectDebitFormController.cls
 * @Description        : Handler class for New Direct Debit Form
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 7/2/2020, 11:20:21 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/30/2020   Creation Admin     Initial Version
**/
public class ct_DirectDebitFormController {

  /**
  * @description - method to fetch metadata records and picklist values
  * @return MetadataWrapper 
  **/
  @AuraEnabled
  public static MetadataWrapper fetchMetadataRecords(){
    //recurringDonationFielsMap - contains field result for Direct Debit object
    Map<String,Schema.SObjectField> recurringDonationFielsMap = ct_WebDataFormHelper.getSojectFields('npe03__Recurring_Donation__c');
    //picklistMap - Stores picklist Values that are configured for the web form
    Map<String, List<PicklistEntryWrapper>> picklistMap = new  Map<String, List<PicklistEntryWrapper>>();
    MetadataWrapper thisWrapper = new MetadataWrapper();
    //For Loop for running all the Web Form metadata records configured for Direct Debit Webform
    for(Webform_Data__mdt thisDirectDebitMetadata : ct_WebDataFormHelper.getWebFormMetadataRecords(ct_Constants.WEB_FORMDATA_SOURCE_DIRECT_DEBIT)){
      thisWrapper.directDebitMetadataRecords.add(thisDirectDebitMetadata);
      if(recurringDonationFielsMap.containskey(thisDirectDebitMetadata.SObject_Field__c)
      && (String.valueOf(recurringDonationFielsMap.get(thisDirectDebitMetadata.SObject_Field__c).getDescribe().getType()) == 'PICKLIST'
      || String.valueOf(recurringDonationFielsMap.get(thisDirectDebitMetadata.SObject_Field__c).getDescribe().getType()) == 'MultiPicklist')){
        picklistMap.put(thisDirectDebitMetadata.SObject_Field__c, constructPicklistEntryWrapper(ct_WebDataFormHelper.getPicklistValues(thisDirectDebitMetadata.SObject__c, thisDirectDebitMetadata.SObject_Field__c)));
      }
    }
    picklistMap.put('CollectionDate', new List<PicklistEntryWrapper>{new PicklistEntryWrapper('1st of the Month','1st of the Month'),new PicklistEntryWrapper('15th of the Month','15th of the Month')});
    picklistMap.put('OpenEndedStatus', new List<PicklistEntryWrapper>{new PicklistEntryWrapper('None','None'),new PicklistEntryWrapper('Open','Open')});

    picklistMap.put('General_Accounting_Unit__c', getGAUUnits());
    thisWrapper.picklistFields = picklistMap;
    thisWrapper.advancmentSettingRecords = ct_WebDataFormHelper.getAdvancementSettingRecords();
    return thisWrapper;
  }
  /**
  * @description - Method to construct picklist values ha Label, value pair
  * @param List<Schema.PicklistEntry> picklistEntryList - List of picklist values
  * @return List<PicklistEntryWrapper> 
  **/
  public static List<PicklistEntryWrapper> constructPicklistEntryWrapper(List<Schema.PicklistEntry> picklistEntryList){
    //thisPickListWrapperList - stores all individual picklist values in a PicklistEntryWrapper list
    List<PicklistEntryWrapper> thisPickListWrapperList        = new List<PicklistEntryWrapper>();
    for( Schema.PicklistEntry thisPicklistEntry : picklistEntryList){
      thisPickListWrapperList.add(new PicklistEntryWrapper(thisPicklistEntry.getLabel(), thisPicklistEntry.getValue()));
    }
    return thisPickListWrapperList;
  }
   /**
    * Method to Populate Picklist Wrapper with GAU Units to the Map from set of string 
    * @param mapKey : Key for which Picklist Wrapper to be assigned
    * @param thisPicklistMap : Map of Picklist Wrapper List
    * @param picklistValuesSet : Set of string which contains Picklist values
  */
  public  static List<PicklistEntryWrapper> getGAUUnits(){
    //stores object variable in a set of string 
    Set<String> gauFieldsToQuery = new Set<String>{'Id', 'Name', 'npsp__Active__c'};
        
    List<PicklistEntryWrapper> thisPickListWrapperList = new List<PicklistEntryWrapper>();
    //perform dynamic query to fetch General Accounting Unit record and strores in a picklistentry wrapper
      for(npsp__General_Accounting_Unit__c thisGAUUnit : Database.query(ct_Util.getQueryStringForSObject('npsp__General_Accounting_Unit__c', gauFieldsToQuery)+' WHERE npsp__Active__c = true'+' ORDER BY Name ASC ')){
      	thisPickListWrapperList.add(new PicklistEntryWrapper(thisGAUUnit.Name, thisGAUUnit.Id));
    }
    return thisPickListWrapperList;
  }

  /**
  * @description - Method to create Asperato authorisation Record
  * @param String directDebitDetails - JSON Direct Debit Object
  * @return asp04__Authorisation__c - Asperato authorisation record 
  **/
  @AuraEnabled
  public static asp04__Authorisation__c createAsepratoAuthorization(String directDebitDetails, asp04__Authorisation__c thisAuthorisation){
    try{
        // check that asperato authorisation is null.If null we need to create new authorisation record under the direct debit
        if(thisAuthorisation == null){
          thisAuthorisation = new asp04__Authorisation__c();
        }
        //Deserialize the webform data directDebitDetails to Map
        Map<String, Object> directDebitObject = (Map<String, Object>)JSON.deserializeUntyped(directDebitDetails);
        if(directDebitObject.containskey('npe03__Contact__c')
        && !String.isEmpty(String.valueOf(directDebitObject.get('npe03__Contact__c')))){
          Contact thisContact = getContact(Id.valueOf(String.valueOf(directDebitObject.get('npe03__Contact__c'))));
          // Create a new asperato authorisation record and link the contact
          thisAuthorisation   = ct_WebDataFormHelper.createAuthorizationRecord(JSON.serialize(thisContact), ct_Constants.DIRECT_DEBIT_PAYMENT_METHOD, thisAuthorisation.Id);
        }
        else if(directDebitObject.containskey('npe03__Organization__c')
        && !String.isEmpty(String.valueOf(directDebitObject.get('npe03__Organization__c')))){
          Account thisAccount = getAccount(Id.valueOf(String.valueOf(directDebitObject.get('npe03__Organization__c'))));
          // Create a new asperato authorisation record and link the Account
          thisAuthorisation   = ct_WebDataFormHelper.createAuthorizationRecordForAccount(JSON.serialize(thisAccount), ct_Constants.DIRECT_DEBIT_PAYMENT_METHOD,  thisAuthorisation.Id);
          thisAuthorisation.Account__c = thisAccount.Id;
        }
      upsert thisAuthorisation;
      return thisAuthorisation;
    }
    catch (Exception e) {
      ct_Logger.logMessage(e, 'Asperato Authorization');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }

  /**
  * @description - Method to create Direct Debit records 
  * @param String directDebitDetails - Json String direct debit object
  * @param asp04__Authorisation__c thisAuthorisation - Asperato authorisation record 
  * @return Id - Direct Debit record Id to redirect
  **/
  @AuraEnabled
  public static string createRecurringDonation(String directDebitDetails, asp04__Authorisation__c thisAuthorisation){
    try{
      String returnURL =  Url.getOrgDomainUrl().toExternalForm();
      Map<String,Schema.SObjectField> recurringDonationFielsMap = ct_WebDataFormHelper.getSojectFields('npe03__Recurring_Donation__c');
      npe03__Recurring_Donation__c thisDonation = new npe03__Recurring_Donation__c();

      if(thisAuthorisation == null){
        thisAuthorisation = createAsepratoAuthorization(directDebitDetails, null);
      }
      // Deserialize the DD webform data from string to Map
      Map<String, Object> directDebitObject = (Map<String, Object>)JSON.deserializeUntyped(directDebitDetails);
      // Creating new recurring donation record
      for(String thisField : directDebitObject.keySet()){
      if(!String.isEmpty(String.valueOf(directDebitObject.get(thisField)))){
        if(recurringDonationFielsMap.containskey(thisField)
        && String.valueOf(recurringDonationFielsMap.get(thisField).getDescribe().getType()) == 'DATE'){
          thisDonation.put(thisField, Date.valueOf(String.valueOf(directDebitObject.get(thisField))));
        }
        else if(recurringDonationFielsMap.containskey(thisField)
        && String.valueOf(recurringDonationFielsMap.get(thisField).getDescribe().getType()) == 'BOOLEAN'){

          thisDonation.put(thisField, Boolean.valueOf(String.valueOf(directDebitObject.get(thisField))));
        }
        else if(recurringDonationFielsMap.containskey(thisField)
        && String.valueOf(recurringDonationFielsMap.get(thisField).getDescribe().getType()) == 'CURRENCY'){
          thisDonation.put(thisField, Decimal.valueOf(String.valueOf(directDebitObject.get(thisField))));
        }
        else if(recurringDonationFielsMap.containskey(thisField)
        && String.valueOf(recurringDonationFielsMap.get(thisField).getDescribe().getType()) == 'DOUBLE'){
          thisDonation.put(thisField, Integer.valueOf(String.valueOf(directDebitObject.get(thisField))));
        }
        else if(recurringDonationFielsMap.containskey(thisField)){
          thisDonation.put(thisField, String.valueOf(directDebitObject.get(thisField)));
        }
      }
     }
     //link the asperato authorisation record that we are created and linked to the recurring donation
      thisDonation.Asperato_Authorisation__c = thisAuthorisation.Id;
      thisDonation.npe03__Schedule_Type__c   = 'Multiply By';
      //Set payment method as Direct Debit
      thisDonation.npsp__PaymentMethod__c    = ct_Constants.DIRECT_DEBIT_PAYMENT_METHOD;
      if(directDebitObject.containskey('CollectionDate')
      && !String.isEmpty(String.valueOf(directDebitObject.get('CollectionDate')))){
        thisDonation.npsp__Day_of_Month__c = String.valueOf(directDebitObject.get('CollectionDate')) == '15th of the Month' ? '15': '1';
      }
      DateTime establisheDate = thisDonation.npe03__Date_Established__c != null ? dateTime.newInstance(thisDonation.npe03__Date_Established__c.year(),thisDonation.npe03__Date_Established__c.month(), thisDonation.npe03__Date_Established__c.day()) : System.now();

      if(String.isEmpty(thisDonation.Name)){
        thisDonation.Name = thisAuthorisation.asp04__First_Name__c != null? thisAuthorisation.asp04__First_Name__c+' '+thisDonation.npsp__PaymentMethod__c+' '+establisheDate.format('dd/MM/YYYY') : thisAuthorisation.asp04__Last_Name__c+' '+thisDonation.npsp__PaymentMethod__c+' '+establisheDate.format('dd/MM/YYYY');
      }
      insert thisDonation;
      //Update GAU with Recurring Donation Id
      thisAuthorisation.Recurring_Donations__c = thisDonation.Id;
      update thisAuthorisation;
      returnURL = returnURL+'/'+thisDonation.Id;
      return returnURL;
    }
    catch (Exception e) {
      ct_Logger.logMessage(e, 'Recurring Donation(Direct Debit)');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }
  
  /**
  * @description - Method get Contact record
  * @author Creation Admin | 4/2/2020 
  * @param Id contactId 
  * @return Contact 
  **/
  @TestVisible
  private static Contact getContact(Id contactId){
    // return the contact that is selected from the DDform for creating recurring donation 
    return [SELECT Id, AccountId, FirstName, LastName, Email, MailingState,
                            MailingStreet, MailingCity, MailingCountry, MailingPostalCode
                            FROM Contact 
                            WHERE Id=: contactId];
                        
  }
  /**
  * @description - method to get Account record
  * @author Creation Admin | 4/2/2020 
  * @param Id accountId 
  * @return Account 
  **/
  @TestVisible
  private static Account getAccount(Id accountId){
    // return the account that is selected from the DDform for creating recurring donation 

    return [SELECT Id, Name, BillingStreet, BillingCity, BillingState
                            ,BillingCountry, BillingPostalCode
                            FROM Account 
                            WHERE Id=:accountId];
  }
  
  public class MetadataWrapper{
    //To store all webform data configured in metadata for DDform
    @AuraEnabled
    public List<Webform_Data__mdt> directDebitMetadataRecords {get;set;}
    //To store all picklist field and values for displaying in the form
    @AuraEnabled
    public Map<String, List<PicklistEntryWrapper>> picklistFields {get;set;}
    //Store advancement metadata records
    @AuraEnabled
    public List<ct_Advancement_Setting__mdt> advancmentSettingRecords {get;set;}
    //store business organisation account record type
    @AuraEnabled
    public String  businessOrganisationRecordType {get;set;}
    MetadataWrapper(){
      directDebitMetadataRecords      = new List<Webform_Data__mdt>();
      picklistFields                  = new  Map<String, List<PicklistEntryWrapper>>();
      advancmentSettingRecords        = new List<ct_Advancement_Setting__mdt>();
      businessOrganisationRecordType  = ct_Constants.ACCOUNT_BUSINESS_ORGANISATION_RECORDTYPE_NAME;
    }
  }
  /**
    * Wrapper that contains Donation Object wrapper and Picklist Values
  */
  public class PicklistEntryWrapper{
    @AuraEnabled
    public String label                  {get;set;}
    @AuraEnabled
    public string value                  {get;set;}
    
    public PicklistEntryWrapper(String entryLabel,String entryValue){
      this.label    = entryLabel;
      this.value    = entryValue;
    }
  }

}