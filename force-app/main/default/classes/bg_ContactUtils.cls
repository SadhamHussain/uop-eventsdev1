/*****************************************************************
* bg_ContactUtils
*
* Utility methods relating to Contact object
* 
* Test Class: bg_ContactUtils_Test
******************************************************************/

public class bg_ContactUtils {
    
    /*
    * Update application fields of Contact after conversion of Lead into Contact
    */
    public static Contact updateContactKeyFields(Application__c app)
    {
        Contact newContact = new Contact();
        newContact.Id = app.Applicant__c;
        newContact.Birthdate = app.Date_Of_Birth__c;
        newContact.hed__Gender__c = app.Student_Gender__c;
        newContact.Qualification_1__c = app.Qualification_1__c;
        newContact.Qualification_2__c = app.Qualification_2__c;
        newContact.Qualification_3__c = app.Qualification_3__c;
        newContact.Qualification_4__c = app.Qualification_4__c;
        newContact.Qualification_5__c = app.Qualification_5__c;
        newContact.Qualification_6__c = app.Qualification_6__c;
        newContact.Qualification_7__c = app.Qualification_7__c;
        newContact.Qualification_8__c = app.Qualification_8__c;
        newContact.hed__Primary_Address_Type__c = app.Address_Type__c;
        if(app.Origin__c == 'SITS')
        {
            newContact.Address_1__c = app.Address_1__c;
            newContact.Address_2__c = app.Address_2__c;
            newContact.Address_3__c = app.Address_3__c;
            newContact.Address_4__c = app.Address_4__c;
            newContact.Address_5__c = app.Address_5__c;
            newContact.Postcode__c = app.Postal_Code__c;
        }
            
        
        return newContact;
    }
    
    /*
    * Process Contact updates upon change to Application record
    */
    public static void updateContactLinkedToUpdatedApplication(List<Application__c> newApplications, Map<Id,Application__c> oldApplicationsById)
    {
        Map<Id, Contact> contactsToUpdateByAppId = new Map<Id, Contact>();
        
        for(Application__c newApp : newApplications)
        {
            Application__c oldApp = oldApplicationsById.get(newApp.Id);
            Id contactId = newApp.Applicant__c;

            if(String.isNotBlank(contactId))
            {
                Contact contactToUpdate = updateContactFields(newApp, oldApp, contactId);
                
                if(contactToUpdate != null)
                {
                    contactsToUpdateByAppId.put(newApp.Id, contactToUpdate);
                }
            }
        }

        if(!contactsToUpdateByAppId.isEmpty())
        {
            performUpdateDMLForApplicationContacts(contactsToUpdateByAppId);
        }
    }
    
    public static void processUpdatedContactsAfterUpdate(List<Contact> newContacts, Map<Id, Contact> oldContactsById)
    {
        bg_AffiliationUtils.createSchoolAffiliationForUpdatedContacts(newContacts, oldContactsById);
    }
    
     /*
    * Update individual Contact details based on changes to Application record
    */
    private static Contact updateContactFields(Application__c newApp, Application__c oldApp, Id contactId)
    {  
        Boolean updateContact = false;
        Contact contactToUpdate = new Contact(Id = newApp.Applicant__c);
        
        List<Application_Contact_Mapping__mdt> fieldMappings = [SELECT Id, Application_Field__c, Contact_Field__c FROM Application_Contact_Mapping__mdt];
        List<String> addressFields = new List<String>{'Address_1_c', 'Address_2_c', 'Address_3_c', 'Address_4_c', 'Address_5_c', 'Postal_Code_c'};

        for(Application_Contact_Mapping__mdt fieldMapping : fieldMappings)
        {
            system.debug('app field is ' + fieldMapping.Application_Field__c);

            if(newApp.Origin__c == 'SITS')
            {
                if(newApp.get(fieldMapping.Application_Field__c) != oldApp.get(fieldMapping.Application_Field__c))
                {
                    system.debug('updating');
                    
                    if(fieldMapping.Application_Field__c == 'Care_Leaver__c' && newApp.get(fieldMapping.Application_Field__c) == null)
                    {
                        contactToUpdate.put(fieldMapping.Contact_Field__c, false);
                    }
                    else
                    {
                        contactToUpdate.put(fieldMapping.Contact_Field__c, newApp.get(fieldMapping.Application_Field__c));
                    }
                    updateContact = true;
                }
            }
            else if(!addressFields.contains(fieldMapping.Application_Field__c))
            {
                system.debug('updating non address fields');
                if(fieldMapping.Application_Field__c == 'Care_Leaver__c' && newApp.get(fieldMapping.Application_Field__c) == null)
                {
                    contactToUpdate.put(fieldMapping.Contact_Field__c, false);
                }
                else
                {
                contactToUpdate.put(fieldMapping.Contact_Field__c, newApp.get(fieldMapping.Application_Field__c));
                }
                updateContact = true;
            }
        }

        String newAddressLines = newApp.Address__c;
        String oldAddressLines = oldApp.Address__c;

		if(newAddressLines != oldAddressLines)
		{
			if(newAddressLines != null && newAddressLines.length() > 255)
			{
				String first255CharsOfAddress = newAddressLines.substring(0,255);
				String formatedStreetAddress = first255CharsOfAddress.substringBeforeLast(',');
				contactToUpdate.MailingStreet = formatedStreetAddress;
			    
				String remainingAddressUnformatted = newAddressLines.remove(formatedStreetAddress);
			    contactToUpdate.Additional_Address_Lines__c = remainingAddressUnformatted.substringAfter(',');
			}
			else
			{
				contactToUpdate.MailingStreet = newAddressLines;
			}
            updateContact = true;
		}

        if(!updateContact)
        {
            contactToUpdate = null;
        }

        return contactToUpdate;
    }
    
    /*
    * Performs update on Contacts
    */
    public static void performUpdateDMLForApplicationContacts(Map<Id,Contact> contactsToUpdateByAppId)
    {
        System.debug('contactsToUpdateByAppId ' + contactsToUpdateByAppId);
        Database.SaveResult[] updateContacts = Database.update(contactsToUpdateByAppId.values(), false); 

        Map<Id, String> errorMessageByAppId = new Map<Id, String>();
        List<Id> appIds = new List<Id>(contactsToUpdateByAppId.keySet());
		
		for(Integer i = 0; i < updateContacts.size(); i++) 
		{
	       Id appId = appIds[i];

	       if(!updateContacts[i].isSuccess())
	       {
               System.debug('not successful ' + updateContacts[i].getErrors());
	       		String errorMessage = '';

		        for(Database.Error error : updateContacts[i].getErrors()) 
		        {
		            if(String.isEmpty(errorMessage)) 
		            {
		                errorMessage = error.getMessage();
		            } 
		        }

		        errorMessageByAppId.put(appId, errorMessage);
	       }
            else
            {
                System.debug('success update ' +updateContacts[i].Id);
            }
        }

        if(!errorMessageByAppId.isEmpty())
        {
	        bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
        }
    } 
    
    /*
    * To identify if newly creates Contacts have been created via Lead conversion, in order to determine if child records of the Lead need to be reparented
    * 
    * Caused issues with SITS deployment - added but commented to resolve
    */
    public static void processUpdatedContactsAfterInsert(List<Contact> newContacts)
    {
        /*System.Debug('### ASDF In processUpdatedContactsAfterInsert');
        Map<Id, Contact> convertedContactsById = new Map<Id, Contact>();
        
        for(Contact newContact : newContacts)
        {
            if(String.isNotBlank(newContact.Converted_Lead_Id__c) && newContact.LeadSource != 'Application')
            {
                convertedContactsById.put(newContact.Id, newContact);
            }
        }

        if(!convertedContactsById.isEmpty())
        {
            processConvertedLeadChildRecordsToReparentToContact(convertedContactsById);
        }
        System.Debug('### ASDF Out processUpdatedContactsAfterInsert');*/
    }
        
          ////////////////////////////////////////////////////////////////////////////
  //  @author: Thomas Packer (BrightGen)
  //  @created: 12th September 2019
  //  @description: A method to get contact records
  //
  //  @changes:  
    //  @usage: 
  ////////////////////////////////////////////////////////////////////////////
  /*  public static Map<Id, Contact> GetContactsWithAttendeesWithEventsThisAcademicYear(Set<Id> contactIds)
    {
        // Attributes to track years
        Date academicYearStart;
        Date academicYearEnd;
        
        // Calculate academic years
        if (Date.today().month() >= 8)
        {
            academicYearStart = Date.newInstance(Date.today().year(), 8, 1);
            academicYearEnd = Date.newInstance(Date.today().addYears(1).year(), 7, 1);
        }
        else
        {
            academicYearStart = Date.newInstance(Date.today().addYears(-1).year(), 8, 1); 
            academicYearEnd = Date.newInstance(Date.today().year(), 7, 1);
        }
        
        Map<Id, Contact> contacts = new Map<Id, Contact>([SELECT Id,
                                                          FirstName,
                                                          LastName,
                                                          CreatedDate,
                                                          LastModifiedDate,
                                                          (
                                                              SELECT Id, 
                                                              Name, 
                                                              EventApi__First_Name__c,
                                                              EventApi__Last_Name__c,
                                                              Attendance_Response_Received__c,
                                                              EventApi__Event__r.EventApi__Display_Name__c,
                                                              EventApi__Event__r.EventApi__Start_Date__c,
                                                              EventApi__Event__r.EventApi__Start_Date_Time__c,
                                                              EventApi__Event__r.EventApi__End_Date__c,
                                                              EventApi__Event__r.EventApi__End_Date_Time__c,
                                                              EventApi__Event__r.EventApi__Description__c,
                                                              EventApi__Event__r.EventApi__Event_Category__c,
                                                              EventApi__Event__r.Event_Category_Name__c,
                                                              EventApi__Contact__c, 
                                                              Fonteva_Event_Lead__c
                                                              FROM EventApi__Attendees__r
                                                              WHERE EventApi__Event__r.EventApi__Start_Date__c >= :academicYearStart 
                                                              AND EventApi__Event__r.EventApi__End_Date__c <= :academicYearEnd
                                                          )
                                                          FROM Contact
                                                          WHERE Id IN :contactIds]);
        
        if (!contacts.isEmpty())
        {
            return contacts;
        }
        
        return null;
    } */
}