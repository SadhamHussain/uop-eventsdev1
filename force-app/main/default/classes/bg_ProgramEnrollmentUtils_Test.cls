@isTest
private class bg_ProgramEnrollmentUtils_Test {

	@isTest (seeAllData = true)
	private static void test_createProgramEnrollmentPostApplicationEnrollment_positive_single() {
    	
    	Bypass_Configuration__c configBypass = Bypass_Configuration__c.getInstance(UserInfo.getProfileId());
		configBypass.Are_Processes_Off__c = false;
		insert configBypass;
    	
    	//Given
    	List<Account> accountsToCreate = new List<Account>();
    	Account testSchool1 = bg_UnitTestDataFactory.createSchoolAccount();
    	accountsToCreate.add(testSchool1);
    	Account testCourse1 = bg_UnitTestDataFactory.createCourseAccount();
    	accountsToCreate.add(testCourse1);
    	insert accountsToCreate;
        
        system.debug(accountsToCreate);

        Contact testContact = bg_UnitTestDataFactory.createContact();
    	testContact.School_College__c = testSchool1.Id;
    	insert testContact;

        Opportunity testOpp = bg_UnitTestDataFactory.createOpportunity(testContact);
        insert testOpp;

    	Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.First_Name__c = 'test928384'; 
	    testApp1.Last_Name__c = 'user';
	    testApp1.Preferred_Name__c = 'Derek';
	    testApp1.Email_Address__c = 'test928384@user.com';
	    testApp1.Phone_Number__c = '0739357673';
	    testApp1.Postal_Code__c = 'E2 8YS';
	    testApp1.Admission_Cycle__c = 2020;
	    testApp1.Application_Year__c = '2019';
	    testApp1.Domicile_Country_Code__c = 'UK';
	    testApp1.Type__c = 'UCAS';
	    testApp1.Address__c = 'testAddressLine1,testAddressLine2,testAddressLine3,testAddressLine4,testAddressLine5,testAddressLine6,testAddressLine7,testAddressLine8,testAddressLine9,testAddressLine10,testAddressLine11,testAddressLine12,testAddressLine13,testAddressLine14,testAddressLine15,testAddressLine16,testAddressLine17,testAddressLine18,testAddressLine19,testAddressLine20';
	    testApp1.Course__c = testCourse1.Id;
	    testApp1.School_College__c = testSchool1.Id;
	    testApp1.Opportunity__c = testOpp.Id;
	    testApp1.Applicant__c = testContact.Id;
	    testApp1.Rolled_Status__c  = 'A';
	    List<Application__c> testApplications = new List<Application__c>{testApp1};
	    insert testApplications;
	    List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, Mode_of_Study__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c, Entry_Year__c, Entry_Year_Formula__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c, CreatedDate,
															 Enquirer__c, Opportunity__c, Applicant__c, Application_Enrolled__c, Course__c
															 FROM Application__c
															 WHERE Id =:testApp1.Id];
	    Application__c testApp2 = testApp1;
	    testApp2.Rolled_Status__c = 'U';
	    update testApp2;
        Map<Id, Application__c> oldAppMap = new Map<Id, Application__c>();
        oldAppMap.put(testApp2.Id, testApp2);
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        oppMap.put(testOpp.Id, testOpp);

    	//When
    	bg_ProgramEnrollmentUtils.createProgramEnrollmentPostApplicationEnrollment(updatedTestApplications, oldAppMap, oppMap);

    	//Then
    	List<hed__Program_Enrollment__c> insertedProgramEntrollments = [select id from hed__Program_Enrollment__c where hed__Contact__c = :testContact.Id];

    	system.assertEquals(1, insertedProgramEntrollments.size(), 'Wrong number of programs inserted');

    }
}