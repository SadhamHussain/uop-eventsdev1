/*
* rl_SessionAssignmentUtils
*
* Methods creating for interim process for events with sessions
* 
* Is schedulable and will be set for a daily schedule using Apex Scheduling
*
* Uses a custom labels to pass event name and entry year into method queries - BatchEventName & BatchEntryYear
* 
* Test Class: rl_SessionAssignmentUtils_Test 
* 
* Created: 19th February 2020
*/


public class rl_SessionAssignmentUtils implements Schedulable{
    
    public void execute(SchedulableContext ctx)
    {
        
        // List to store updated Event Registrations
        List<Event_Registration__c> updatedEventRegistrations = new List<Event_Registration__c>();

        
        // Get all Events that start with 2020 Applicant Experience Day
        List<Event__c> eventsToProcess = [ SELECT Id, Name 
                                           FROM Event__c
                                           WHERE Name LIKE :Label.BatchEventName
                                         ];
        
        
        for( Event__c event : eventsToProcess)
        {
        
            // Get all Event Registrations associated to Contacts that don't have a session and are associated to the provided Event ID
            List<Event_Registration__c> registrationsToProcess = [ SELECT Id, Session__c, Contact__c, Event__r.Id
                                                                   FROM Event_Registration__c
	                                                               WHERE Session__c = null
                                                                   AND Contact__c != null
                                                                   AND Event__r.Id = :event.Id
                                                                 ];
            
            
            // Get list of the Contacts associated to the Event Registrations
            List<String> contactsToProcess = new List<String>();       
            
            for (Event_Registration__c eventReg : registrationsToProcess)
            {
                contactsToProcess.add(eventReg.Contact__c);
            }
            
            
            
            // Get map of all Sessions from related Event
            List<Session__c> sessionList = [ SELECT Id, Name, Event__c 
                                            FROM Session__c
                                            WHERE Event__c = : event.Id
                                        ];
            
            Map<String,String> sessionMap = new  Map<String,String>();
            
            for( Session__c session : sessionList )
            {
                sessionMap.put(session.Name, session.Id);
            }
            
            
            // Get map of Course Name and Session Name from Custom MetaData    
            List<AED_Course_Session_Map__mdt> courseSessionList = [ SELECT Course_Name__c, Session_Name__c
                                                                FROM AED_Course_Session_Map__mdt
                                                                ]; 
            
            Map<String,String> courseSessionMap = new  Map<String,String>();
            
            for( AED_Course_Session_Map__mdt metadata : courseSessionList )
            {
                courseSessionMap.put(metadata.Course_Name__c, metadata.Session_Name__c);
            }   
            
            
            // Get map of Contact ID and Course Name from Course on Best Offer on AAC        
            List<Applicant_Admission_Cycle__c> aacList = [ SELECT Id, Applicant__r.Id, BestOffer__r.Course__c ,BestOffer__r.Course__r.Academic_Programme_Display_Course_Name__c
                                                        FROM Applicant_Admission_Cycle__c
                                                        WHERE Applicant__r.Id IN :contactsToProcess
                                                        AND Admission_Cycle__c = :Label.BatchEntryYear
                                                        ];
            
            Map<String,String> contactCourseMap = new  Map<String,String>();
            
            for( Applicant_Admission_Cycle__c aac : aacList)
            {
                contactCourseMap.put(aac.Applicant__r.Id,aac.BestOffer__r.Course__r.Academic_Programme_Display_Course_Name__c);
            }
            
            
            
            // Use the maps to populate the session on the Event Registrations being processed         
            for( Event_Registration__c eventReg : registrationsToProcess)
            {
                
                // Get course name related to the contact
                String courseName = contactCourseMap.get(eventReg.Contact__c);
                
                // Get session name related to the course name
                String sessionName = courseSessionMap.get(courseName);
                
                // Get session ID related to the session Name
                eventReg.Session__c = sessionMap.get(sessionName);

                updatedEventRegistrations.add(eventReg);
            }
        
        }

        // Bulk Update Event Regisrtations
        update updatedEventRegistrations;
        
    }
}