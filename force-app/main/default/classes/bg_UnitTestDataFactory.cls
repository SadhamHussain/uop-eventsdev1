/*****************************************************************
 * bg_UnitTestDataFactory
 *
 * Helper methods for Unit Tests
 *
 * Author: Stuart Barber
 * Created: 11-09-2018
 **************** Version history ***********************
 * CT, Dinesh : 10/07/2020 - Introduced custom label to store Record Type Developer Name 
 *                           and get Record Type Id by Record Type Developer Name
******************************************************************/

public with sharing class bg_UnitTestDataFactory {
	
	/**********************************************************************************/
	/********************************* CONTACT METHODS ********************************/
	/**********************************************************************************/

	/*
	*
	* Creates a generic Contact record
	*
	*/
	public static Contact createContact()
	{
		Contact testContact = new Contact();
		testContact.FirstName = 'test';
		testContact.LastName = 'mcTesterson';
		testContact.Email = 'test@mctesterson.com';

		return testContact;
	}

	/*
	*
	* Creates an Applicant Contact record
	*
	*/
	public static Contact createApplicantContact()
	{
		Contact testContact = createContact();
		testContact.RecordTypeId = bg_CommonUtils.studentContactRTId;

		return testContact;
	}

	/**********************************************************************************/
	/******************************** ENQUIRER METHODS ********************************/
	/**********************************************************************************/

	/*
	*
	* Creates a generic Enquirer record
	*
	*/
	public static Lead createEnquirer()
	{
		Lead genericEnquirer = new Lead();
		genericEnquirer.FirstName = 'test';
		genericEnquirer.LastName = 'mcTesterson';
		genericEnquirer.Email = 'test@mctesterson.com';
		genericEnquirer.Company = genericEnquirer.FirstName + ' ' + genericEnquirer.LastName;
		genericEnquirer.Enquiry_Channel__c = 'Phone';
		genericEnquirer.Initial_Enquiry__c = 'General Enquiry'; 
		genericEnquirer.Status = 'Open - Not Contacted';

		return genericEnquirer;
	}

	/*
	*
	* Creates a Influencer Enquirer record
	*
	*/
	public static Lead createInfluencerEnquirer()
	{
		Lead influencerEnquirer = createEnquirer();
		influencerEnquirer.Type__c = 'Parent / Guardian';
		influencerEnquirer.RecordTypeId = bg_CommonUtils.influencerEnquirerRTId;

		return influencerEnquirer;
	}

	/*
	*
	* Creates a Student Enquirer record
	*
	*/
	public static Lead createStudentEnquirer()
	{
		Lead studentEnquirer = createEnquirer();
		studentEnquirer.Type__c = 'Student';
		studentEnquirer.RecordTypeId = bg_CommonUtils.studentEnquirerRTId;

		return studentEnquirer;
	}

	/*
	*
	* Creates a Student Enquirer linked to an Influencer Enquirer record
	*
	*/
	public static Lead createStudentEnquirerLinkedToInfluencerEnquirer(Id influencerEnquirerId)
	{
		Lead studentEnquirer = createStudentEnquirer();
		studentEnquirer.Influencer__c = influencerEnquirerId;

		return studentEnquirer;
	}
    
    /*
	*
	* Creates 200 Student Leads with associated Cases
	*
	*/
    
    public static void insert200LeadsWithCases()
    {
        List<Lead> leadList = new List<Lead>();
        
        for(integer i=0; i<200; i++)
		{
            
            Lead genericLead = new Lead();
            
            genericLead.FirstName = 'test ' + i;
            genericLead.LastName = 'mcTesterson ' + i;
            genericLead.Email = i + 'test@mctesterson.com';
            genericLead.Company = genericLead.FirstName + ' ' + genericLead.LastName;
            genericLead.Enquiry_Channel__c = 'Phone';
            genericLead.Initial_Enquiry__c = 'General Enquiry'; 
            genericLead.Status = 'Open - Not Contacted';
            leadList.add(genericLead);
            
        }
        insert leadList;
        
        
        List<Case> genericCases = new List<Case>();
        
        for( Lead genericLead : leadList )
        {
            Case testCase = new Case();
            
            testCase.Subject = genericLead.Company;
            testCase.Status = 'New';
            testCase.Origin = 'Phone';
            testCase.Enquirer__c = genericLead.Id;
            genericCases.add(testCase);          
        }   
                    
        insert genericCases;
        
    }

	/**********************************************************************************/
	/******************************** CASE METHODS ********************************/
	/**********************************************************************************/

	/*
	*
	* Creates a generic Enquiry record
	*
	*/
	public static Case createEnquiry()
	{
		Case testCase = new Case();
		testCase.Description = 'Test Case Description';
		testCase.Status = 'New';
		testCase.Origin = 'Phone';

		return testCase;
	}

	/*
	*
	* Creates an Enquiry record with a Record Type provided by the User
	*
	*/
	public static Case createEnquiryOfRecordType(String recordTypeName)
	{
		Id caseRecordTypeId = bg_CommonUtils.getRecordTypeID('Case', recordTypeName);

		Case testCase = createEnquiry();
		testCase.RecordTypeId = caseRecordTypeId;

		return testCase;
	}

	/**********************************************************************************/
	/******************************* OPPORTUNITY METHODS ******************************/
	/**********************************************************************************/

	/*
	*
	* Creates a generic Opportunity record
	*
	*/
	public static Opportunity createOpportunity(Contact testContact)
	{
		Contact createdContact = [SELECT Id, AccountId FROM Contact WHERE Id = :testContact.Id LIMIT 1];

		Opportunity testOpportunity = new Opportunity();
		testOpportunity.Name = 'Test Opportunity Test';
		testOpportunity.AccountId = createdContact.AccountId;
		testOpportunity.CloseDate = Date.today().addDays(10);
		testOpportunity.StageName = 'Prospecting';

		return testOpportunity;
	}

	/**********************************************************************************/
	/******************************* APPLICATION METHODS ******************************/
	/**********************************************************************************/

	/*
	*
	* Creates a generic Application record
	*
	*/
	public static Application__c createApplication()
	{
		Application__c testApplication = new Application__c();
		testApplication.UCAS_Student_Id__c = 'testucasstudent1234';
		testApplication.Student_Id__c = '0293842727';
		testApplication.SCMS_Application_Id__c = 'testapp1234';
		testApplication.Course_Code__c = 'COMPSCI1234';
		testApplication.Start_Date__c = Date.today() + 90;
		testApplication.UCAS_School_Code__c = 'scucas1234';
		testApplication.Nationality__c = 'British';
		testApplication.Domicile__c = 'UK';
		testApplication.Entry_Month__c = '09';
		testApplication.Entry_Year__c = '2020';
		testApplication.First_Name__c = 'Stuart'; 
		testApplication.Last_Name__c = 'Little';
		testApplication.Email_Address__c = 'stuart@little.com';
		testApplication.Phone_Number__c = '0784837173';
		testApplication.Postal_Code__c = 'RH14 9AG';

		return testApplication;
	}

	/**********************************************************************************/
	/********************************* ACCOUNT METHODS ********************************/
	/**********************************************************************************/

	/*
	*
	* Creates a Course account
	*
	*/
	public static Account createCourseAccount()
	{
    //CT, Dinesh : Introduced custom label to store Record Type Developer Name and get Record Type Id by Record Type Developer Name
		Id courseRecordTypeId = bg_CommonUtils.getRecordTypeID('Account', System.Label.Account_Academic_Program_Record_Type);

		Account course = new Account();
		course.Name = 'Computer Science 2019';
		course.Course_Level_of_Study__c = 'Undergraduate';
		course.Course_Instance_Start_Date__c = Date.today() + 50;
		course.Course_Start_Month__c = '09';
		course.Course_Start_Year__c = '2020';
		course.Academic_Programme_Course_Code__c = 'COMPSCI1234';
		course.UCAS_Course_Code__c = 'UOP-COM-67890';
		course.RecordTypeId = courseRecordTypeId;

		return course;
	}

	/*
	*
	* Creates a Course account
	*
	*/
	public static Account createSchoolAccount()
	{
    //CT, Dinesh : Introduced custom label to store Record Type Developer Name and get Record Type Id by Record Type Developer Name
		Id schoolRecordTypeId = bg_CommonUtils.getRecordTypeID('Account', System.Label.Account_Educational_Institution_Record_Type);

		Account school = new Account();
		school.RecordTypeId = schoolRecordTypeId;
		school.School_College_UCAS_Code__c = 'scucas1234';
		school.Name = 'The Test School';

		return school;
	}

}