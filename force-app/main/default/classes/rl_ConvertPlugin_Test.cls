/*
* Test Class - rl_ConvertPlugin_Test
*
* This class tests rl_ConvertPlugin, which is a class used to customise the 
* behaviour of Duplicate Check (Managed Package) conversions. 
*
* Created By: Rob Liesicke (UoP)
* Created On: 28th November 2019
*
***************** Version history ***********************
* 17/12/19 - Updated test scripts to increase code coverage
*
* CT, Sadham : 10/07/2020 -  Updated the class to assert the LEI once Campaign member is inserted
*********************************************************************************************************************************/

// See all data is required due to avoid EventbriteSync.EventbriteOpportunityTrigger errors
@IsTest(SeeAllData=true) 
public class rl_ConvertPlugin_Test 
{

   /*
    *
    */  
    
    @isTest static void testConvertProcessing() 
    {  
      
      // Uses unit test helper to create lead for testing custom conversion code
      Lead leadSix = bg_UnitTestDataFactory.createEnquirer();
      leadSix.Email = 'contact@one.com';
      leadSix.Phone = '6666666';
      leadSix.MobilePhone = '0766666';
      insert leadSix; 
    
      // Insert account as it's required in lead conversion  
      Account accountOne = new Account();
      accountOne.Name='Test Account' ;
      insert accountOne;
    
      
      // Uses unit test helper to create contact one for testing custom conversion code
      Contact contactOne = bg_UnitTestDataFactory.createContact();
      contactOne.Email = 'contact@one.com';
      contactOne.Phone = '1111111';
      contactOne.MobilePhone = 'o7,1-1 1(1)1.1';
      contactOne.AccountID = accountOne.Id;
      insert contactOne;  
    
      // Insert Campaign for use with creating Campaign Members
      Campaign testCampaign = new Campaign(); 
      testCampaign.Name = 'Test Campaign';
      insert testCampaign;
      
      // Insert Campaign Member for the Lead
      CampaignMember testCampaignMember = new CampaignMember();
      testCampaignMember.LeadId = leadSix.Id;
      testCampaignMember.CampaignId = testCampaign.Id;
      testCampaignMember.Year_of_Entry__c = '2020';
      testCampaignMember.Level_of_Study__c = 'Postgraduate';
      insert testCampaignMember;
      //CT, Sadham : 10/07/2020: Assert the LEI once Campaign member is inserted
      List<Latest_Enquiry_Information__c> leadLEIList = [SELECT Id, Enquirer__c, Contact__c
                                                                  FROM Latest_Enquiry_Information__c
                                                                  WHERE Enquirer__c = :leadSix.Id];
      //Latest Enquiry Information will be inserted when a Campaign member is created
      system.assertEquals(1, leadLEIList.size());   

      // Create a LeadConvert object with the lead, contact and account added
      Database.LeadConvert lc = new Database.LeadConvert();
      lc.setLeadId(leadSix.Id);
      lc.setContactId(contactOne.Id);
      lc.setAccountId(contactOne.AccountId);      

      // Set lead convert status
      LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
      lc.setConvertedStatus(convertStatus.MasterLabel);

      // Store the LeadConvertResult returned by converting the lead, as it's required by the custom convert plugin method
      Database.LeadConvertResult lcr = Database.convertLead(lc);
      
      // Ensure the Lead conversion was successful
      System.assert(lcr.isSuccess());


      Test.startTest();
      
      // Create an instance of the custom convert plugin
      rl_ConvertPlugin plugin = new rl_ConvertPlugin();
      
      // Call the after convert method, giving it the LeadConvertResult and a null Task
      plugin.afterConvert(lcr,null);
   
      Test.stopTest(); 

      // Retrieve the updated contact to check values after conversion
      Contact contactToTest = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Contact WHERE Id = :contactOne.Id];
    
      // The Mobile Phone that was originally on the Contact should still be present. This also tests the special characters were removed.
      system.assertEquals(contactToTest.MobilePhone, '+447111111');   
      
      // The Mobile Phone that was originally on the Lead should now be on the Contact's mobile alternative field
      system.assertEquals(contactToTest.Mobile_Alternative__c , '+44766666');       
      
      // The Email that was originally on the Contact should still be present
      system.assertEquals(contactToTest.Email , 'contact@one.com');          
      
      // The Email that was originally on the Lead was a duplicate so it should not have been copied to the email alternative field
      system.assertEquals(contactToTest.Email_Alternative__c , null);   
      
      
      // Retrieve the updated Campaign Member to check lead was replaced by the contact
      CampaignMember campaignMemberToTest = [SELECT Id, CampaignId, LeadId, Type, ContactId FROM CampaignMember WHERE ContactId = :contactOne.Id];
      
      
      system.assertEquals(campaignMemberToTest.ContactId, contactOne.Id);    
      system.assertEquals(campaignMemberToTest.Type, 'Contact');  
        
      //Latest Enquiry Information will be transferred to Contact record once Lead is converted
      List<Latest_Enquiry_Information__c> contactLEIList = [SELECT Id, Enquirer__c, Contact__c
                                                              FROM Latest_Enquiry_Information__c
                                                              WHERE Contact__c = :contactOne.Id];
      system.assertEquals(1, contactLEIList.size());
  }  
  
}