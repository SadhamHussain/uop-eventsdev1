/**
 * @File Name          : ct_hed_CampaignTriggerHelper.cls
 * @Description        : TDTM Trigger Handler for Campaign Object
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/11/2020, 7:57:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/11/2020   Creation Admin     Initial Version
**/
public class ct_hed_CampaignTriggerHelper {
	@future
	public static void updateProjectCustomRollup(set<Id> projectIds){
		try{
		//Project Map to Update
            Map<Id,Project__c> projectToUpdateMap = new Map<Id,Project__c>();
            //Reintialize the Rollup Field of select ProjectIds
            for(Project__c thisProject :[Select Id,Number_Of_Related_Campaings__c FROM project__c WHERE Id IN: ProjectIds]){
                thisProject.Number_Of_Related_Campaings__c = 0;
                projectToUpdateMap.put(thisProject.Id,thisProject);
            }
		//AggregateResult SOQL on get Count of Campaigns
		for(AggregateResult thisResult :[Select Count(Id) totalRecords, Project__c projectId FROM Campaign Where Project__c != null and Project__c IN: projectIds group by Project__c]){
            Project__c thisProject = projectToUpdateMap.get((Id)thisResult.get('projectId'));
            thisProject.Number_Of_Related_Campaings__c =(Decimal)thisResult.get('totalRecords');
            projectToUpdateMap.put((Id)thisResult.get('projectId'),thisProject);
		}
        //Update the database with Rollup count of Campaigns related to project
		if(projectToUpdateMap.size()>0)
	     upsert projectToUpdateMap.values();
	}catch(Exception e){
		ct_Logger.logMessage(e, 'Project__c');
		throw e;
	}
	}
}