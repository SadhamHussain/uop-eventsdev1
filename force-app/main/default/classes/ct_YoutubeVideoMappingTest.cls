@isTest
public with sharing class ct_YoutubeVideoMappingTest {
    @TestSetup
    static void makeData(){
        Id  RecordTypeId= Schema.getGlobalDescribe().get('Contact').getDescribe().getRecordTypeInfosByDeveloperName().get('UOP_Contact').getRecordTypeId();
        Contact uopContact = new Contact(LastName='Test_Contact',RecordTypeId=RecordTypeId);
        insert uopContact;
    }
    private testmethod static void unit1(){
        Test.startTest();
        Contact con = [SELECT Id FROM Contact Where RecordTypeId =: Schema.getGlobalDescribe().get('Contact').getDescribe().getRecordTypeInfosByDeveloperName().get('UOP_Contact').getRecordTypeId()];
        if(con != null){
            ct_YoutubeVideoMapping.getURL(con.Id);
        }
        Test.stopTest();
    }
    private testmethod static void unit2(){
        Test.startTest();
            ct_YoutubeVideoMapping.findObjectNameFromRecordIdPrefix('003');
        Test.stopTest();
    }
    /**Negative test */
    private testmethod static void unit3(){
        Test.startTest();
        ct_YoutubeVideoMapping.getURL('10000000000000');
        Test.stopTest();
    }
    private testmethod static void unit4(){
        Test.startTest();
            ct_YoutubeVideoMapping.getAllRecords('');
            ct_YoutubeVideoMapping.getAllRecords('E');
        Test.stopTest();
    }
}