/**
 * @File Name          : ct_hed_AffiliationTriggerHelper_Test.cls
 * @Description        : Test Class for ct_hed_AffiliationTriggerHandler_TDTM Class
 * @Author             : Creation Admin
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 6/25/2020, 2:21:52 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/16/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_hed_AffiliationTriggerHelper_Test {
  /**
  * @description Test method to test Student Organisation Account roll up 
  * @return void 
  **/
  @isTest (seeAllData = true)
  private static void testStudentOrganisationAccountRollupTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_AffiliationTriggerHandler_TDTM', 'hed__Affiliation__c', 'BeforeDelete;AfterInsert;AfterUpdate', 4.00));
    // Pass trigger handler config to set method for this test run

    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    Account thisAccount         = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId    = ct_Constants.ACCOUNT_RECORDTYPE_STUDENT_ORGANISATION;
    insert thisAccount;

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;

    hed__Affiliation__c thisAffiliation = ct_TestDataFactory.createHedaAffiliation(thisAccount.Id, thisContact.Id);
    insert thisAffiliation;
    Test.stopTest();
    Account thisTestAccount = [SELECT Id, Actual_Affiliated_Members__c, RecordTypeId
                                    FROM Account 
                                    WHERE Id =: thisAccount.Id];
    System.assertEquals(1, thisTestAccount.Actual_Affiliated_Members__c, 'Should return 1 because we have relate one affiliation to that account');
  }
  /**
  * @description Test method to test updateContactCustomRollupFields Method on inserting Affiliation
  * @return void 
  **/
  @isTest (seeAllData = true)
  private static void testContactRollupTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_AffiliationTriggerHandler_TDTM', 'hed__Affiliation__c', 'BeforeDelete;AfterInsert;AfterUpdate', 4.00));
    // Pass trigger handler config to set method for this test run

    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    List<Account> accountListToInsert = new List<Account>();
    Account thisAccount               = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId          = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    thisAccount.R_O_Organisation__c   = true;
    accountListToInsert.add(thisAccount);
    Account thisROAccount             = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisROAccount.R_O_Organisation__c = true;
    accountListToInsert.add(thisROAccount);
    insert accountListToInsert;

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    List<hed__Affiliation__c> affiliationListToInsert   = new List<hed__Affiliation__c>();
    hed__Affiliation__c thisCurrentAffiliation          = ct_TestDataFactory.createHedaAffiliation(thisAccount.Id, thisContact.Id);
    thisCurrentAffiliation.hed__Role__c                 = ct_Constants.AFFILIATION_ROLE_STUDENT;
    thisCurrentAffiliation.hed__Status__c               = ct_Constants.AFFILIATION_STATUS_CURRENT;
    thisCurrentAffiliation.hed__Primary__c              = true;
    affiliationListToInsert.add(thisCurrentAffiliation);
    hed__Affiliation__c thisAlumniAffiliation           = ct_TestDataFactory.createHedaAffiliation(thisAccount.Id, thisContact.Id);
    thisAlumniAffiliation.hed__Role__c                  = ct_Constants.AFFILIATION_ROLE_STUDENT;
    thisAlumniAffiliation.hed__Status__c                = ct_Constants.AFFILIATION_STATUS_ALUMNI;
    affiliationListToInsert.add(thisAlumniAffiliation);
    hed__Affiliation__c thisProspectAffiliation         = ct_TestDataFactory.createHedaAffiliation(thisAccount.Id, thisContact.Id);
    thisProspectAffiliation.hed__Role__c                = ct_Constants.AFFILIATION_ROLE_PROSPECT;
    thisProspectAffiliation.hed__Status__c              = ct_Constants.AFFILIATION_STATUS_CURRENT;
    affiliationListToInsert.add(thisProspectAffiliation);
    hed__Affiliation__c thisCareerAdvAffiliation         = ct_TestDataFactory.createHedaAffiliation(thisROAccount.Id, thisContact.Id);
    thisCareerAdvAffiliation.hed__Role__c                = ct_Constants.AFFILIATION_ROLE_CAREERS_ADVISER;
    affiliationListToInsert.add(thisCareerAdvAffiliation);
    insert affiliationListToInsert;
    Test.stopTest();
    Contact thisTestContact = [SELECT Id, Name, Primary_Role__c, R_O_Organisational_Contact__c, 
                                        No_Prospect_Affiliations__c,
                                        No_Applicant_Affiliations__c,
                                        No_Student_Affiliations__c,
                                        No_Alumni_Affiliations__c,
                                        No_Rejected_Affiliations__c,
                                        UOP_Academic_Affiliations__c
                                        FROM Contact 
                                        WHERE Id =:thisContact.Id];
    System.assertEquals(ct_Constants.AFFILIATION_ROLE_STUDENT, thisTestContact.Primary_Role__c, 'Should return student');
    System.assertEquals(false, thisTestContact.R_O_Organisational_Contact__c, 'Should return false');
    System.assertEquals(1, thisTestContact.No_Prospect_Affiliations__c,'Should return 1');
    System.assertEquals(1, thisTestContact.No_Student_Affiliations__c, 'Should return 1');
  }
  /**
  * @description Test method to test updateContactCustomRollupFields Method on updating Affiliation 
  * @return void 
  **/
  @isTest (seeAllData = true)
  private static void testContactRollupUpdateTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_AffiliationTriggerHandler_TDTM', 'hed__Affiliation__c', 'BeforeDelete;AfterInsert;AfterUpdate', 4.00));
    // Pass trigger handler config to set method for this test run

    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    Account thisAccount               = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId          = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    thisAccount.R_O_Organisation__c   = true;
    insert thisAccount;

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    List<hed__Affiliation__c> affiliationListToInsert   = new List<hed__Affiliation__c>();
    hed__Affiliation__c thisCurrentAffiliation          = ct_TestDataFactory.createHedaAffiliation(thisAccount.Id, thisContact.Id);
    thisCurrentAffiliation.hed__Role__c                 = ct_Constants.AFFILIATION_ROLE_STUDENT;
    thisCurrentAffiliation.hed__Status__c               = ct_Constants.AFFILIATION_STATUS_CURRENT;
    thisCurrentAffiliation.hed__Primary__c              = true;
    affiliationListToInsert.add(thisCurrentAffiliation);
    insert affiliationListToInsert;
    affiliationListToInsert[0].hed__Role__c             = ct_Constants.AFFILIATION_ROLE_PROSPECT;
    update affiliationListToInsert;
    Test.stoptest();
    Contact thisTestContact = [SELECT Id, Name, Primary_Role__c, R_O_Organisational_Contact__c, 
                                        No_Prospect_Affiliations__c,
                                        No_Applicant_Affiliations__c,
                                        No_Student_Affiliations__c,
                                        No_Alumni_Affiliations__c,
                                        No_Rejected_Affiliations__c,
                                        UOP_Academic_Affiliations__c
                                        FROM Contact 
                                        WHERE Id =:thisContact.Id];
    System.assertEquals(ct_Constants.AFFILIATION_ROLE_PROSPECT, thisTestContact.Primary_Role__c, 'Should return prospect');
    System.assertEquals(1, thisTestContact.No_Prospect_Affiliations__c,'Should return 1');
  }
  @isTest (seeAllData = true)
  private static void RAOAccountandContactTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_AffiliationTriggerHandler_TDTM', 'hed__Affiliation__c', 'BeforeDelete;AfterInsert;AfterUpdate', 4.00));
    // Pass trigger handler config to set method for this test run

    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    List<Account> accountListToInsert = new List<Account>();
    Account thisAccount               = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId          = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(thisAccount);
    insert accountListToInsert;
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    List<hed__Affiliation__c> affiliationListToInsert   = new List<hed__Affiliation__c>();
    hed__Affiliation__c thisCurrentAffiliation          = ct_TestDataFactory.createHedaAffiliation(thisAccount.Id, thisContact.Id);
    thisCurrentAffiliation.hed__Role__c                 = ct_Constants.AFFILIATION_ROLE_STUDENT;
    thisCurrentAffiliation.hed__Status__c               = ct_Constants.AFFILIATION_STATUS_CURRENT;
    thisCurrentAffiliation.RAO_Affiliation__c              = true;
    thisCurrentAffiliation.hed__Account__c  = accountListToInsert[0].id;
    thisCurrentAffiliation.hed__Contact__c  = thisContact.Id;
    affiliationListToInsert.add(thisCurrentAffiliation);
    insert affiliationListToInsert;
    Test.stopTest();
    Contact thisTestContact = [SELECT Id, R_O_Organisational_Contact__c FROM Contact WHERE Id =:thisContact.Id];
    System.assertEquals(true,thisTestContact.R_O_Organisational_Contact__c, 'Should return true');
    Account thisTestAccount = [SELECT Id, R_O_Organisation__c FROM Account WHERE Id=:accountListToInsert[0].Id];
    System.assertEquals(true,thisTestAccount.R_O_Organisation__c, 'Should return true');
  }
  @isTest (seeAllData = true)
  private static void updateRAOAccountandContactTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_AffiliationTriggerHandler_TDTM', 'hed__Affiliation__c', 'BeforeDelete;AfterInsert;AfterUpdate', 4.00));
    // Pass trigger handler config to set method for this test run

    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    List<Account> accountListToInsert = new List<Account>();
    Account thisAccount               = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId          = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(thisAccount);
    insert accountListToInsert;
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    List<hed__Affiliation__c> affiliationListToInsert   = new List<hed__Affiliation__c>();
    hed__Affiliation__c thisCurrentAffiliation          = ct_TestDataFactory.createHedaAffiliation(thisAccount.Id, thisContact.Id);
    thisCurrentAffiliation.hed__Role__c                 = ct_Constants.AFFILIATION_ROLE_STUDENT;
    thisCurrentAffiliation.hed__Status__c               = ct_Constants.AFFILIATION_STATUS_CURRENT;
    //thisCurrentAffiliation.RAO_Affiliation__c              = true;
    thisCurrentAffiliation.hed__Account__c  = accountListToInsert[0].id;
    thisCurrentAffiliation.hed__Contact__c  = thisContact.Id;
    affiliationListToInsert.add(thisCurrentAffiliation);
    insert affiliationListToInsert;
    affiliationListToInsert[0].RAO_Affiliation__c = true;
    update affiliationListToInsert;
    Test.stopTest();
    Contact thisTestContact = [SELECT Id, R_O_Organisational_Contact__c FROM Contact WHERE Id =:thisContact.Id];
    System.assertEquals(true,thisTestContact.R_O_Organisational_Contact__c, 'Should return true');
    Account thisTestAccount = [SELECT Id, R_O_Organisation__c FROM Account WHERE Id=:accountListToInsert[0].Id];
    System.assertEquals(true,thisTestAccount.R_O_Organisation__c, 'Should return true');
  }
  @isTest (seeAllData = true)
  private static void resetRAOAccountandContactTest(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_AffiliationTriggerHandler_TDTM', 'hed__Affiliation__c', 'BeforeDelete;AfterInsert;AfterUpdate', 4.00));
    // Pass trigger handler config to set method for this test run

    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    List<Account> accountListToInsert = new List<Account>();
    Account thisAccount               = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId          = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accountListToInsert.add(thisAccount);
    insert accountListToInsert;
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    List<hed__Affiliation__c> affiliationListToInsert   = new List<hed__Affiliation__c>();
    hed__Affiliation__c thisCurrentAffiliation          = ct_TestDataFactory.createHedaAffiliation(thisAccount.Id, thisContact.Id);
    thisCurrentAffiliation.hed__Role__c                 = ct_Constants.AFFILIATION_ROLE_STUDENT;
    thisCurrentAffiliation.hed__Status__c               = ct_Constants.AFFILIATION_STATUS_CURRENT;
    thisCurrentAffiliation.RAO_Affiliation__c              = true;
    thisCurrentAffiliation.hed__Account__c  = accountListToInsert[0].id;
    thisCurrentAffiliation.hed__Contact__c  = thisContact.Id;
    affiliationListToInsert.add(thisCurrentAffiliation);
    insert affiliationListToInsert;
    affiliationListToInsert[0].RAO_Affiliation__c = false;
    update affiliationListToInsert;
    Test.stopTest();
    Contact thisTestContact = [SELECT Id, R_O_Organisational_Contact__c FROM Contact WHERE Id =:thisContact.Id];
    System.assertEquals(false,thisTestContact.R_O_Organisational_Contact__c, 'Should return false');
    Account thisTestAccount = [SELECT Id, R_O_Organisation__c FROM Account WHERE Id=:accountListToInsert[0].Id];
    System.assertEquals(false,thisTestAccount.R_O_Organisation__c, 'Should return false');
  }

}