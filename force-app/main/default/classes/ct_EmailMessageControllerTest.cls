/**
 * @File Name          : ct_EmailMessageControllerTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 07-08-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/6/2020   Creation Admin     Initial Version
**/
@isTest public class ct_EmailMessageControllerTest {
    
    // @description - To test the ct_EmailMessageController class  
    @isTest()
    public static void chatterPostToUserTest()
    {
        List<String> thisContactList = new List<String>();

        Contact thisContact = ct_TestDataFactory.createContact();
        thisContact.Email   = 'CreationTechnology@creation.com';
        thisContact.Is_A_A_Unaffiliated_Contact__c = true;
        insert thisContact;
        
        String emailString ='('+thisContact.Email+')';
        thisContactList.add(emailString);
        
        // Getting the web donation payment details data in advancement settings 
        ct_Advancement_Setting__mdt thisUserIdMetadata = new ct_Advancement_Setting__mdt();
        thisUserIdMetadata.MasterLabel = 'Bounce Alert User';
        thisUserIdMetadata.Value__c    = UserInfo.getUserId();
        thisUserIdMetadata.DeveloperName = ct_Constants.BOUNCE_ALERT_USER;
        
        ct_MetadataService.advancementSettingMetaData.put(ct_Constants.BOUNCE_ALERT_USER, thisUserIdMetadata);

        Test.StartTest();
            ct_EmailMessageController.postToChatter(thisContactList);          
        Test.StopTest();
        
        List<feeditem> feedItemList = [SELECT Id, ParentId, Body FROM feeditem WHERE ParentId =:UserInfo.getUserId()];
        System.assertEquals ('The contact email address '+thisContact.Email+' is invalid. So the last email to this contact has bounced.', feedItemList[0].Body);
      }
}