/**
 * @File Name          : ct_DeletionScheduleClassTest.cls
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin  - Creation Technology Solutions
 * @Last Modified On   : 5/5/2020, 12:57:21 pm
**/
@isTest global class ct_ScheduledGenericDeleteBatchTest {
    /**
   * @description - Test the Error Log record delete test
   * @return void 
   **/
   @isTest public static void ScheduledDeleteErrorLogsTest(){
        List<Error_Log__c> errorLogList = new List<Error_Log__c>();
        for(Integer i=0; i<5; i++){
            Error_Log__c thisErrorLog = ct_TestDataFactory.createErrorLogs();
            errorLogList.add(thisErrorLog);
        }
        Test.startTest();
        insert errorLogList;
        String sch = '0 0 0 ? * * *';
        ct_ScheduledDeleteErrorLogs thisScheduledDeleteErrorLogs = new ct_ScheduledDeleteErrorLogs();
        System.schedule('Test Generic Deletion', sch, thisScheduledDeleteErrorLogs); 
        Test.stopTest();
   } 
   /**
   * @description - Test the Website Submission record delete test
   * @return void 
   **/
   @isTest public static void ScheduledDeleteWebsubmissionsTest(){
    List<Web_Site_Submission__c> webSubmissionsList = new List<Web_Site_Submission__c>();
    for(Integer i=0; i<5; i++){
        Web_Site_Submission__c thisWebsiteSubmissions = ct_TestDataFactory.createWebSubmissions();
        webSubmissionsList.add(thisWebsiteSubmissions);
    }
    Test.startTest();
    insert webSubmissionsList;
    String sch = '0 0 0 ? * * *';
    ct_ScheduledDeleteWebsubmissions thisScheduledDeleteSubmissions = new ct_ScheduledDeleteWebsubmissions();
    System.schedule('Test Generic Deletion', sch, thisScheduledDeleteSubmissions);
    Test.stopTest();
    } 
}