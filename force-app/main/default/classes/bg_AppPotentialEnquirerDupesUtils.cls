/*****************************************************************
* bg_AppPotentialEnquirerDupesUtils
*
* Utility methods relating to Applicant Potential Enquirer Duplicate object
* 
* Test Class: bg_AppPotentialEnquirerDupesUtils_Test
*
* Author: BrightGen
* Created: 27-11-2018
******************************************************************/

public class bg_AppPotentialEnquirerDupesUtils 
{
	/*
	*
	* Insert APED Records and return Map of Error Description to apply onto related Application record
	*
	*/
	public static Map<Id, String> insertApplicationPotentialEnquirerDuplicateRecords(Map<Id, Set<Lead>> multipleDuplicateEnquirersByAppId)
	{
		List<Applicant_Potential_Enquirer_Duplicate__c> appPotentialEnquirerDupeRecords = createListOfApplicationPotentialEnquirerDuplicateRecords(multipleDuplicateEnquirersByAppId);
		Map<Id, String> errorMessageByAppId = new Map<Id, String>();
		
		if(!appPotentialEnquirerDupeRecords.isEmpty())
		{
			Database.SaveResult[] insertAPEDRecords = Database.insert(appPotentialEnquirerDupeRecords, false);
			String multipleDupeEnquirersErrorMessage = System.Label.App_Multiple_Duplicate_Enquirers_Error_Message;
			
			for(Integer i = 0; i < insertAPEDRecords.size(); i++) 
			{
		       Id appId = appPotentialEnquirerDupeRecords[i].Application__c;

		       if(insertAPEDRecords[i].isSuccess())
		       {
		       		errorMessageByAppId.put(appId, multipleDupeEnquirersErrorMessage);
		       }
		       else
		       {
			       	String errorsConcatenated = '';

			        for(Database.Error error : insertAPEDRecords[i].getErrors()) 
			        {
			            if(String.isEmpty(errorsConcatenated)) 
			            {
			            	errorsConcatenated = error.getMessage();
			            } 
			            else 
			            {
			                errorsConcatenated = errorsConcatenated + ' | ' + error.getMessage();
			            }
			        }

			        errorMessageByAppId.put(appId, errorsConcatenated);
		       }
		    }
		}
		return 	errorMessageByAppId;
	}

	/*
	*
	* Create List of APED Records to be inserted
	*
	*/
	private static List<Applicant_Potential_Enquirer_Duplicate__c> createListOfApplicationPotentialEnquirerDuplicateRecords(Map<Id, Set<Lead>> multipleDuplicateEnquirersByAppId)
	{
		List<Applicant_Potential_Enquirer_Duplicate__c> appPotentialEnquirerDupeRecords = new List<Applicant_Potential_Enquirer_Duplicate__c>();

		if(!multipleDuplicateEnquirersByAppId.isEmpty())
		{

			for(Id appId : multipleDuplicateEnquirersByAppId.keySet())
			{
				Set<Lead> duplicateEnquirers = multipleDuplicateEnquirersByAppId.get(appId);

				for(Lead dupeEnquirer : duplicateEnquirers)
				{
					Applicant_Potential_Enquirer_Duplicate__c apedRecord = new Applicant_Potential_Enquirer_Duplicate__c();
					apedRecord.Application__c = appId;
					apedRecord.Potential_Enquirer_Duplicate__c = dupeEnquirer.Id;

					appPotentialEnquirerDupeRecords.add(apedRecord);					
				}
			}
		}

		return appPotentialEnquirerDupeRecords;
	}
}