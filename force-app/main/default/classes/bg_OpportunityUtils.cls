/*****************************************************************
* bg_OpportunityUtils
*
* Utility methods relating to Opportunity object
* 
* Test Class: bg_OpportunityUtils_Test
******************************************************************/

public class bg_OpportunityUtils 
{
    public static boolean firstRun = true;
    public static boolean programPlanChanged = true;
    public static boolean aicToBeUpserted = false;
    public static boolean processEnrolledOpp = true;
    public static boolean affiliationToBeUpserted = true;
    public static boolean eventBookingRequired = true;
    public static boolean eventRebookingRequired = true;
    
    /*
    * Setup new opportunity (created via Lead conversion process) upon creation of an Application - setup of standard Opportunity fields
    */
    public static Opportunity updateNewOpportunity(Application__c app, string courseDisplayName, string courseStartDateString)
    {
        Opportunity newOpp = new Opportunity();
        
        String oppName = app.First_Name__c + ' ' + app.Last_Name__c + ' - '  + courseDisplayName + ' - ' + courseStartDateString;

        newOpp.Id = app.Opportunity__c;
        newOpp.Name = oppName;

        if(String.isNotBlank(app.Applicant__c))
        {
            newOpp.Applicant__c = app.Applicant__c;
        }
        if(String.isNotBlank(app.Application_Stage__c))
        {
            newOpp.StageName = app.Application_Stage__c;
        }
        if(String.isNotBlank(String.valueOf(app.Start_Date__c)))
        {
            newOpp.CloseDate = app.Start_Date__c;
        }
        if(string.isNotBlank(app.Mode_of_Attendance__c))
        {
            newOpp.Mode_of_Attendance__c = app.Mode_of_Attendance__c;
        }
        newOpp.Unconditional_Offer_Scheme__c = app.Member_of_Unconditional_Offer_Scheme__c;
        newOpp.Type = app.Type__c;

        return newOpp;
    }
    
    /*
    * Check if any Opportunity related fields have been changed on Application record and update related Opportunity 
    */
    public static void updateOpportunityLinkedToUpdatedApplication(List<Application__c> newApplications, Map<Id,Application__c> oldApplicationsById)
    {
        Map<Id, Opportunity> opportunitiesToUpdateByAppId = new Map<Id, Opportunity>();
        Map<Id, String> opportunityNameByOppId = createMapOfOpportunityNamesById(newApplications, oldApplicationsById);

        for(Application__c newApp : newApplications)
        {
            Application__c oldApp = oldApplicationsById.get(newApp.Id);
            Id oppId = newApp.Opportunity__c;
            String oppName; 

            if(String.isNotBlank(oppId))
            {
                if(!opportunityNameByOppId.isEmpty())
                {
                    oppName = opportunityNameByOppId.get(oppId);
                }

                Opportunity oppToUpdate = updateOpportunityFields(newApp, oldApp, oppId, oppName);
                
                if(oppToUpdate != null)
                {
                    opportunitiesToUpdateByAppId.put(newApp.Id, oppToUpdate);
                }
            }
        }

        if(!opportunitiesToUpdateByAppId.isEmpty())
        {
            performUpdateDMLForApplicationOpportunities(opportunitiesToUpdateByAppId);
        }
    }
    
        /*
    * Checks changed in Application record and updates related Opportunity prior to updating
    */
    public static Opportunity updateOpportunityFields(Application__c newApp, Application__c oldApp, Id oppId, String oppName)
    {
        Boolean updateOpportunity = false;
        Opportunity oppToUpdate = new Opportunity(Id = newApp.Opportunity__c);
        List<Application_Opportunity_Mapping__mdt> fieldMappings = [SELECT Id, Application_Field__c, Opportunity_Field__c FROM Application_Opportunity_Mapping__mdt];
        
        for(Application_Opportunity_Mapping__mdt fieldMapping : fieldMappings)
        {
            if(newApp.get(fieldMapping.Application_Field__c) != oldApp.get(fieldMapping.Application_Field__c))
            {
                oppToUpdate.put(fieldMapping.Opportunity_Field__c, newApp.get(fieldMapping.Application_Field__c));
                updateOpportunity = true;
            }
        }
        
        if(String.isNotBlank(oppName))
        {
            oppToUpdate.Name = oppName;
            updateOpportunity = true;
        }
        //not in custom metadata as causes string/int casting error
        if(newApp.Admission_Cycle__c != oldApp.Admission_Cycle__c)
        {
            oppToUpdate.Admissions_Cycle__c = string.valueOf(integer.valueOf(newApp.Admission_Cycle__c));
            updateOpportunity = true;
        }
        if(!updateOpportunity)
        {
            oppToUpdate = null;
        }
        
        

        return oppToUpdate;
    }
    
    /*
    * Check if Opportunity Name requires changing and create map of update Name by Opportunity Id 
    */
    @testVisible private static Map<Id,String> createMapOfOpportunityNamesById(List<Application__c> newApplications, Map<Id,Application__c> oldApplicationsById)
    {
        Map<Id,Id> courseIdsByAppId = new Map<Id,Id>();
        Map<Id, Account> courseAccountsById = new Map<Id, Account>();
        Map<Id, String> opportunityNameByOppId = new Map<Id, String>();
        List<Application__c> updatedApps = new List<Application__c>();

		for(Application__c newApp : newApplications)
        {
            Application__c oldApp = oldApplicationsById.get(newApp.Id);
            Id oppId = newApp.Opportunity__c;

            if(String.isNotBlank(oppId))
            {
                if((newApp.Course__c != oldApp.Course__c) ||
                   (newApp.First_Name__c != oldApp.First_Name__c) ||
                   (newApp.Last_Name__c != oldApp.Last_Name__c))
                {
                    courseIdsByAppId.put(newApp.Id, newApp.Course__c);
                    updatedApps.add(newApp);
                }
            }
        }

        if(!courseIdsByAppId.isEmpty())
        {
            courseAccountsById = new Map<Id, Account>([SELECT Id, Name, Academic_Programme_Display_Course_Name__c, Course_Instance_Start_Date__c 
													          FROM Account
													          WHERE Id IN :courseIdsByAppId.values()]);
        }

        if(!updatedApps.isEmpty())
        {
            for(Application__c updatedApp : updatedApps)
            {
                Application__c oldApp = oldApplicationsById.get(updatedApp.Id);
                Id oppId = updatedApp.Opportunity__c;
                String courseDisplayName;
                string courseStartDate;

                if(String.isNotBlank(oppId))
                {
                    if(!courseAccountsById.isEmpty())
                    {
                        courseDisplayName = courseAccountsById.get(updatedApp.Course__c).Academic_Programme_Display_Course_Name__c;
                        if(courseAccountsById.containsKey(updatedApp.Course__c) && courseAccountsById.get(updatedApp.Course__c).Course_Instance_Start_Date__c != null)
                        {
                            courseStartDate = courseAccountsById.get(updatedApp.Course__c).Course_Instance_Start_Date__c.format();
                        }
                        

                        if(courseDisplayName != null && courseStartDate != null)
                        {
                            String oppName = updatedApp.First_Name__c + ' ' + updatedApp.Last_Name__c + ' - ' + courseDisplayName + ' - ' + courseStartDate;
                            opportunityNameByOppId.put(oppId, oppName);
                        }
                    }
                } 
            }
        }

        return opportunityNameByOppId;
    }

    /*
    * Perform update of Opportunity records and catch any errors to record on associated Application
    */
    @testVisible private static void performUpdateDMLForApplicationOpportunities(Map<Id,Opportunity> opportunitiesToUpdateByAppId)
    {
        Database.SaveResult[] updateOpportunities = Database.update(opportunitiesToUpdateByAppId.values(), false); 
        
        Map<Id, String> errorMessageByAppId = new Map<Id, String>();
		
		for(Integer i = 0; i < updateOpportunities.size(); i++) 
		{
	       Id appId = opportunitiesToUpdateByAppId.values()[i].Application__c;
           

	       if(!updateOpportunities[i].isSuccess())
	       {
	       		String errorMessage = '';

		        for(Database.Error error : updateOpportunities[i].getErrors()) 
		        {
		            if(String.isEmpty(errorMessage)) 
		            {
		                errorMessage = error.getMessage();
		            } 
		        }

		        errorMessageByAppId.put(appId, errorMessage);
	       }
        }

        if(!errorMessageByAppId.isEmpty())
        {
            bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
        }
    }


    
     /*
   * Creates a Map of all Opportunities linked to a Contact
   */
   public static Map<Id, List<Opportunity>> createMapOfOpportunitiesByContact(Map<Id, Opportunity> oppsById)
   {
       Set<Id> contactId = new Set<Id>();

        for(Opportunity opp : oppsById.values())
        {
            contactId.add(opp.Applicant__c);
        }
       
       List<Opportunity> allContactsOpportunities = [SELECT Id, StageName, CloseDate,
                                                     Applicant__c, Applicant_Admission_Cycle__c, Admissions_Cycle__c, Course__r.Course_Level_of_Study__c
                                                     FROM Opportunity WHERE Applicant__c IN :contactId];

       Map<Id, List<Opportunity>> opportunitiesByContactId = new Map<Id, List<Opportunity>>();
        
        for(Opportunity opp : allContactsOpportunities)
        {
            if(opportunitiesByContactId.containsKey(opp.Applicant__c))
            {
                opportunitiesByContactId.get(opp.Applicant__c).add(opp);
            }
            else
            {
                opportunitiesByContactId.put(opp.Applicant__c, new List<Opportunity>{opp});
            }
        }

        return opportunitiesByContactId;
   }
    
    /*
    * Main method for handling updates to Opportunities
    */
    public static void processUpdatedOpportunitiesAfterUpdate(List<Opportunity> newOpportunities, Map<Id,Opportunity> oldOpportunitiesById)
    {
		System.debug('newOpportunities ' + newOpportunities);
        System.debug('oldOpportunitiesById ' + oldOpportunitiesById);
        
        Map<Id, Opportunity> oppsRequiringApplicantIntakeCycleProcessing = new Map<Id, Opportunity>();
        Map<Id, Opportunity> oldOppsRequiringApplicantIntakeCycleProcessing = new Map<Id, Opportunity>();
        Map<Id, Opportunity> oppsWithApplicantIntakeCycle = new Map<Id, Opportunity>();
        Map<Id, Opportunity> oldOppsWithApplicantIntakeCycle = new Map<Id, Opportunity>();
        Set<Id> newAACIds = new Set<Id>();
        for(Opportunity newOpp : newOpportunities)
        {
            Opportunity oldOpp = oldOpportunitiesById.get(newOpp.Id);

            if(newOpp.Applicant_Admission_Cycle__c == null && String.isNotBlank(newOpp.Applicant__c))
            {
                oppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, newOpp);
                oldOppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, newOpp);
                aicToBeUpserted = true;
            }
           /* else if(string.isNotBlank(newOpp.Applicant_Admission_Cycle__c)) //&& string.isBlank(oldOpp.Applicant_Admission_Cycle__c))
            {
                oppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, newOpp);
                oldOppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, oldOpp);
                aicToBeUpserted = true;
                
				newAACIds.add(newOpp.Applicant_Admission_Cycle__c);                
            } */
            else if( String.isNotBlank(newOpp.Applicant__c) && string.isNotBlank(oldOpp.Applicant_Admission_Cycle__c) 
                   && (newOpp.StageName != oldOpp.StageName || newOpp.CloseDate != oldOpp.CloseDate) || newOpp.Admissions_Cycle__c != oldOpp.Admissions_Cycle__c)//&& newOpp.Applicant_Admission_Cycle__c != null
            { 
                oppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, newOpp);
                oldOppsRequiringApplicantIntakeCycleProcessing.put(newOpp.Id, oldOpp);
                aicToBeUpserted = true;
            } 
        }
        
        if(!oppsRequiringApplicantIntakeCycleProcessing.isEmpty() && aicToBeUpserted)
        {
            bg_ApplicantAdmissionCycleUtils.processApplicantIntakeCyclesForOpportunities(oppsRequiringApplicantIntakeCycleProcessing, oldOppsRequiringApplicantIntakeCycleProcessing);
        }
        
     /*   if(!newAACIds.isEmpty())
        {
            bg_ApplicantAdmissionCycleUtils.setFlagsOnAacFromOpps(newAACIds);
        } */

    }
    


  
}