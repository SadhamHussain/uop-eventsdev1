/*****************************************************************
* bg_Enquirer_CompanyFormatting_TDTM_Test
*
* Test methods for bg_Enquirer_CompanyFormatting_TDTM
* 
* Author: Stuart Barber
* Created: 12-09-2018
******************************************************************/

@isTest
private class bg_Enquirer_CompanyFormatting_TDTM_Test {
	
	/*
	*
	* Setup of test data to be used for all test methods
	*
	*/
	@testSetup static void setupData()
	{
		List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
		tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Enquirer_CompanyFormatting_TDTM', 
													 'Lead', 'BeforeInsert;BeforeUpdate', 1.00));
		hed.TDTM_Global_API.setTdtmConfig(tokens);
	}

	/*
	*
	* To test the Company field on Enquirer record is updated to the correct format (First Name + Last Name)
	*
	*/
	@isTest private static void testSetEnquirerCompanyIfNotCorrectFormat() 
	{
		Lead testEnquirer = bg_UnitTestDataFactory.createEnquirer();
		testEnquirer.Company = 'incorrect Company format';
		
		insert testEnquirer;

		Test.startTest();

		Lead createdEnquirer = [SELECT Id, FirstName, LastName, Company FROM Lead WHERE Id = :testEnquirer.Id LIMIT 1];
		String expectedCompanyValue = createdEnquirer.FirstName + ' ' + createdEnquirer.LastName;

		system.assertEquals(expectedCompanyValue, createdEnquirer.Company, 'Company value should be in the format First Name + Last Name');

		Test.stopTest();
	}

	/*
	*
	* To test the Company field on Enquirer record is updated to the correct format (Last Name), if record does not have First Name
	*
	*/
	@isTest private static void testSetEnquirerCompanyIfNotCorrectFormatWithNoFirstName() 
	{
		Lead testEnquirer = bg_UnitTestDataFactory.createEnquirer();
		testEnquirer.FirstName = null;
		testEnquirer.Company = 'incorrect Company format';
		
		insert testEnquirer;

		Test.startTest();

		Lead createdEnquirer = [SELECT Id, FirstName, LastName, Company FROM Lead WHERE Id = :testEnquirer.Id LIMIT 1];

		system.assertEquals(createdEnquirer.LastName, createdEnquirer.Company, 'Company value should match Last Name as no Fist Name has been added');

		Test.stopTest();
	}
	
}