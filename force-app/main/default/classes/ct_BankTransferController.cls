/**
 * @File Name          : ct_BankTransferController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 11/5/2020, 11:56:15 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/26/2019   Creation Admin     Initial Version
**/
public class ct_BankTransferController{
  public npe01__OppPayment__c thisNewPayment {get;set;}
  public PDFWrapper thisWrapper  {get;set;}

  public ct_BankTransferController(ApexPages.StandardController standardController) {
    Id recordId       = Apexpages.currentPage().getParameters().get('id');
    thisNewPayment = getPaymentRecord(recordId);

    if(thisNewPayment.CurrencyIsoCode != null){
      getUOPBankDetails(thisNewPayment.CurrencyIsoCode);
    }
  }

  public ct_BankTransferController () {}
  
  /**
  * @description - Method to fetch the opportunity record
  * @param Id recordId - Opportunity Record Id
  * @return GEM Payment 
  **/
  public npe01__OppPayment__c getPaymentRecord(Id recordId){
    return [SELECT Id,  npe01__Opportunity__r.Name, npe01__Payment_Amount__c, Account__r.Name,
                  npe01__Check_Reference_Number__c, npe01__Scheduled_Date__c, CurrencyIsoCode 
                  FROM npe01__OppPayment__c 
                  WHERE Id=: recordId];
  }

/**
  * @description -  Method to get UOP Bank Details custom setting if the International Payment partner is not present in opportunity
  * @param String currencyIsoCode - Opportunity currency
  **/
  public void getUOPBankDetails(String currencyIsoCode){
    String label;
    thisWrapper = new PDFWrapper();
    UOP_Bank_Details__mdt uopBankDetailsMetadataRecord;

    for(UOP_Bank_Details__mdt thisMetadata : [SELECT Id, Account_Number__c, Bank_Name__c, Branch_Name__c, Currency_Type__c,
                                                      IBAN_Number__c, Sort_Code__c, SWIFT_Code__c
                                                      FROM UOP_Bank_Details__mdt
                                                      WHERE Currency_Type__c =: currencyIsoCode
                                                      LIMIT 1] ){
      uopBankDetailsMetadataRecord = thisMetadata;
     }

    thisWrapper.pdfEntryWrapperList = populateBankDetailValues(uopBankDetailsMetadataRecord);     
  }

  /**
  * @description 
  * @author Creation Admin | 1/10/2020 
  * @param SObject thisRecord 
  * @return List<PDFEntryWrapper> 
  **/
  public List<PDFEntryWrapper> populateBankDetailValues(SObject thisRecord){
    String label;
    List<PDFEntryWrapper> thisPDFEntryWrapperList = new List<PDFEntryWrapper>();
      if(String.isNotBlank((String)thisRecord.get('Bank_Name__c'))){
      label =  thisRecord.getSobjectType().getDescribe().fields.getMap().get('Bank_Name__c').getDescribe().getLabel();
      thisPDFEntryWrapperList.add(new PDFEntryWrapper(label, (String)thisRecord.get('Bank_Name__c')));
    }
    if(String.isNotBlank((String)thisRecord.get('Branch_Name__c'))){
      label =   thisRecord.getSobjectType().getDescribe().fields.getMap().get('Branch_Name__c').getDescribe().getLabel();
      thisPDFEntryWrapperList.add(new PDFEntryWrapper(label, (String)thisRecord.get('Branch_Name__c')));
    }
    if(String.isNotBlank((String)thisRecord.get('Sort_Code__c'))){
      label =   thisRecord.getSobjectType().getDescribe().fields.getMap().get('Sort_Code__c').getDescribe().getLabel();
      thisPDFEntryWrapperList.add(new PDFEntryWrapper(label, (String)thisRecord.get('Sort_Code__c')));
    }
    if(String.isNotBlank((String)thisRecord.get('Account_Number__c'))){
      label = thisRecord.getSobjectType().getDescribe().fields.getMap().get('Account_Number__c').getDescribe().getLabel();
      thisPDFEntryWrapperList.add(new PDFEntryWrapper(label, (String)thisRecord.get('Account_Number__c')));
    }
    if(String.isNotBlank((String)thisRecord.get('IBAN_Number__c'))){
      label =  thisRecord.getSobjectType().getDescribe().fields.getMap().get('IBAN_Number__c').getDescribe().getLabel();
      thisPDFEntryWrapperList.add(new PDFEntryWrapper(label, (String)thisRecord.get('IBAN_Number__c')));
    }
    if(String.isNotBlank((String)thisRecord.get('SWIFT_Code__c'))){
      label =  thisRecord.getSobjectType().getDescribe().fields.getMap().get('SWIFT_Code__c').getDescribe().getLabel();
      thisPDFEntryWrapperList.add(new PDFEntryWrapper(label, (String)thisRecord.get('SWIFT_Code__c')));
    }
    
    return thisPDFEntryWrapperList;
  }
  /**
  * @description - Method to store the file under the Opportunity record
  * @return PageReference 
  **/
  public PageReference addPDFAttachment() {
    Id recordId              = Apexpages.currentPage().getParameters().get('id');
    String isAttachment      = Apexpages.currentPage().getParameters().get('attachment');
    
    PageReference pdfDownloaderPage = Page.ct_BankTransferPage;        
    pdfDownloaderPage.getParameters().put('id', recordId);    

    thisNewPayment = getPaymentRecord(recordId);

    if(isAttachment == 'true'){
      Blob pdfSummary = (Test.isRunningTest() ? Blob.valueOf('Test Invoice') : pdfDownloaderPage.getContentAsPDF() );            
      ContentVersion thisContentver   = new ContentVersion();
      thisContentver.ContentLocation  = 'S';
      thisContentver.VersionData      = pdfSummary;
      thisContentver.Title            = 'Bank Transfer PDF';
      thisContentver.PathOnClient     = '.pdf';
      insert thisContentver;

      ContentDocumentLink thisLink    = new ContentDocumentLink();
      thisLink.ContentDocumentId      = [Select Id,ContentDocumentId FROM ContentVersion WHERE Id =:thisContentver.Id].ContentDocumentId;
      thisLink.LinkedEntityId         = recordId;
      thisLink.ShareType              = 'I';
      insert thisLink;
      
      PageReference pageRef = new PageReference('/'+recordId);
      pageRef.setRedirect(true);
      return pageRef;
    }
    return pdfDownloaderPage;  
  }    	
  
  public class PDFWrapper{
    //public list<PDFEntryWrapper> pdfEntryAccountList {get;set;}
    public list<PDFEntryWrapper> pdfEntryWrapperList {get;set;}
    public PDFWrapper(){
      pdfEntryWrapperList = new List<PDFEntryWrapper>();
      //pdfEntryAccountList = new List<PDFEntryWrapper>();
    }
  }
  public class PDFEntryWrapper{
    public String fieldLabel                  {get;set;}
    public string fieldValue                {get;set;}
    public PDFEntryWrapper(String fieldLabel, String  fieldValue){
      this.fieldLabel      = fieldLabel;
      this.fieldValue      = fieldValue;
    }
  }
}