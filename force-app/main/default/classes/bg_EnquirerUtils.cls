/*****************************************************************
* bg_EnquirerUtils
*
* Utility methods relating to Enquirer (Lead) object
* 
* Test Class: bg_EnquirerUtils_Test
******************************************************************/

public class bg_EnquirerUtils {
		
	/*
	*
	* Perform upsert on Enquirers with updated Application Information
	*
	*/
	public static List<Lead> upsertEnquirersToBeConverted(Map<Id, Lead> updatedEnquirersByAppId)
	{
		
		Map<Id, Lead> existingEnquirersByEnquirerId = new Map<Id, Lead>();
		List<Lead> newEnquirers = new List<Lead>();
		
		for(Lead enquirer : updatedEnquirersByAppId.values())
		{
            System.debug('enquirer' + enquirer.Id + '  ' + enquirer);
			if(enquirer.Id != null)
			{
				existingEnquirersByEnquirerId.put(enquirer.Id, enquirer);
			}
			else
			{
				newEnquirers.add(enquirer);
			}
		}
        
		Set<Id> successfulEnquirerIds = new Set<Id>();
		List<Lead> successfulEnquirers = new List<Lead>();
		Map<Id, String> errorMessageByAppId = new Map<Id, String>();
		
		// Insert New Enquirers
		Database.SaveResult[] insertEnquirers = Database.insert(newEnquirers, false);
		
		for(Integer i = 0; i < insertEnquirers.size(); i++) 
		{
	       Id appId = newEnquirers[i].Most_Recent_Application__c;

	       if(insertEnquirers[i].isSuccess())
	       {
	       		successfulEnquirerIds.add(insertEnquirers[i].getId());
	       }
	       else
	       {
		       	String errorsConcatenated = '';

		        for(Database.Error error : insertEnquirers[i].getErrors()) 
		        {
		            if(String.isEmpty(errorsConcatenated)) 
		            {
		                errorsConcatenated = error.getMessage();
		            } 
		            else 
		            {
		                errorsConcatenated = errorsConcatenated + ' | ' + error.getMessage();
		            }
		        }

		        errorMessageByAppId.put(appId, errorsConcatenated);
	       }
	    }

		// Update Existing Enquirers
		Database.SaveResult[] updateExistingEnquirers = Database.update(existingEnquirersByEnquirerId.values(), false);
		
		for(Integer i = 0; i < updateExistingEnquirers.size(); i++) 
		{
	       Id appId = existingEnquirersByEnquirerId.values()[i].Most_Recent_Application__c;

	       if(updateExistingEnquirers[i].isSuccess())
	       {
	       		successfulEnquirerIds.add(updateExistingEnquirers[i].getId());
	       }
	       else
	       {
		       	String errorsConcatenated = '';

		        for(Database.Error error : updateExistingEnquirers[i].getErrors()) 
		        {
		            if(String.isEmpty(errorsConcatenated)) 
		            {
		                errorsConcatenated = error.getMessage();
		            } 
		            else 
		            {
		                errorsConcatenated = errorsConcatenated + ' | ' + error.getMessage();
		            }
		        }

		        errorMessageByAppId.put(appId, errorsConcatenated);
	       }
	    }

	    if(!errorMessageByAppId.isEmpty())
	    {
	    	bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
	    }

	    if(!successfulEnquirerIds.isEmpty())
	    {
	    	successfulEnquirers = createListOfSuccessfulEnquirers(successfulEnquirerIds);
	    }
	    return successfulEnquirers;	
	
	}

	/*
	*
	* Convert Enquirers and process conversion result (caters for successful conversion, setup of relationships between 
	* successfully converted Enquirers that are linked to one another e.g. student --> influencer,
	* unsuccessful conversion due to duplicate error, unsuccessful conversion due to other error)
	*
	*/
	public static void convertEnquirers(List<Lead> enquirersToBeConverted, Map<String, List<Lead>> outstandingEnquirersToConvertByStudentAppId)
	{
		List<Database.LeadConvert> enquirerConverts = setupEnquirersForConversion(enquirersToBeConverted);

		if(!enquirerConverts.isEmpty())
		{
    		List<Database.LeadConvertResult> enquirerConvertResults = Database.convertLead(enquirerConverts, false);
    		
    		Map<Id, List<Lead>> enquirersByContactId = new Map<Id, List<Lead>>();
    		Map<Id, Map<Id, Lead>> enquirerRelationshipByContactIdByAppId = new Map<Id, Map<Id, Lead>>();
    		Map<Id, String> errorMessageByAppId = new Map<Id, String>();
    		Map<Id, Id> enquirerIdByAppId = new Map<Id, Id>();
    		Map<Id, Id> contactIdByAppId = new Map<Id, Id>();
    		Map<Id, Id> opportunityIdByAppId = new Map<Id, Id>();
			Map<String, Id> contactIdByStudentAppId = new Map<String, Id>();

			for(Integer i = 0; i < enquirerConvertResults.size(); i++)
			{
				Id appId = enquirersToBeConverted[i].Most_Recent_Application__c;
				Lead enquirer = enquirersToBeConverted[i];
				
				if(enquirerConvertResults[i].isSuccess())
				{						
					Id relatedEnquirerId = enquirerConvertResults[i].getLeadId();
					Id relatedContactId = enquirerConvertResults[i].getContactId();
					Id relatedOpportunityId = enquirerConvertResults[i].getOpportunityId();
					
					if(relatedOpportunityId != null)
					{
						enquirerIdByAppId.put(appId, relatedEnquirerId);
						opportunityIdByAppId.put(appId, relatedOpportunityId);
						contactIdByAppId.put(appId, relatedContactId);
					}
					if(enquirerRelationshipByContactIdByAppId.containsKey(appID))
					{
						enquirerRelationshipByContactIdByAppId.get(appId).put(relatedContactId, enquirer);
					}
					else
					{
						enquirerRelationshipByContactIdByAppId.put(appId, new Map<Id, Lead>{relatedContactId => enquirer});
					}

					if(enquirer.App_Student_Applicant_Id__c != null)
					{
						contactIdByStudentAppId.put(enquirer.App_Student_Applicant_Id__c, relatedContactId);
					}
				}
				else
				{
					Lead failedEnquirer = enquirersToBeConverted[i];
					String errorsConcatenated = '';
					
					for(Database.Error error : enquirerConvertResults[i].getErrors()) 
				    {   
				        if(String.isEmpty(errorsConcatenated)) 
					    {
					        errorsConcatenated = String.valueOf(error);
					    } 
					    else 
					    {
					        errorsConcatenated = errorsConcatenated + ' | ' + String.valueOf(error);
					    }
					    errorMessageByAppId.put(appId, errorsConcatenated);
				  	}
				}
			}

			// Conversion successful so update Applications with lookups and mark as Processed
			if(!enquirerIdByAppId.isEmpty())
			{	
				bg_ApplicationUtils.updateSuccessfulApplicationRecordsPostProcessing(enquirerIdByAppId, contactIdByAppId, opportunityIdByAppId);
			}

			// Conversion unsuccessful so update Applications with Error Description and mark as Errored
			if(!errorMessageByAppId.isEmpty())
			{
				bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
			}

			/* 
			*  Setup enquirers to be converted that share the same Student Applicant Id of an Enquirer being converted as part of the same transaction
			*/
			if(!contactIdByStudentAppId.isEmpty() && outstandingEnquirersToConvertByStudentAppId != null)
			{
				List<Lead> updatedEnquirerToBeConverted	= createListOfEnquirersWithRelatedContactDetails(contactIdByStudentAppId, outstandingEnquirersToConvertByStudentAppId);

				if(!updatedEnquirerToBeConverted.isEmpty())
				{
					convertEnquirers(updatedEnquirerToBeConverted, null);
				}
			}

			/*
			* Conversion successful and Application has two enquirers associated to it (Student Enquirer and Influencer Enquirer) - therefore
			* create Relationship record between Contacts based on Enquirer type (e.g. Teacher / Advisor --> Student relationship)
			*/
			if(!enquirerRelationshipByContactIdByAppId.isEmpty())
			{
				Map<Id, Map<Id, Lead>> relatedContactsByAppId = createMapOfEnquirersWithInfluencers(enquirerRelationshipByContactIdByAppId);
				bg_RelationshipUtils.createRelationshipRecords(relatedContactsByAppId);
			}
		}
	}

	/*
	*
	* Setup of Enquirers to be Converted
	*
	*/
	private static List<Database.LeadConvert> setupEnquirersForConversion(List<Lead> enquirersToBeConverted)
	{
		List<Database.LeadConvert> enquirerConverts = new List<Database.LeadConvert>();

		for(Lead enquirer : enquirersToBeConverted)
		{
			if (!enquirer.isConverted)
			{
				Database.LeadConvert enqConvert = new Database.LeadConvert();

	
				
				if(bg_CommonUtils.isGroupId(enquirer.OwnerId) || !String.isNotBlank(enquirer.OwnerId))
				{
					enqConvert.setOwnerId(UserInfo.getUserId());
				}

				if(String.isNotBlank(enquirer.App_Contact__c) && String.isNotBlank(enquirer.App_Account__c))
				{
		
					enqConvert.setContactId(enquirer.App_Contact__c);
					enqConvert.setAccountId(enquirer.App_Account__c);
				}

				if(enquirer.RecordTypeId == bg_CommonUtils.influencerEnquirerRTId)
				{
					enqConvert.setLeadId(enquirer.Id);
	               	enqConvert.setConvertedStatus('Closed - Converted to Applicant');
	               	enqConvert.setDoNotCreateOpportunity(true);
               }
               else
               {
               		enqConvert.setLeadId(enquirer.Id);
	               	enqConvert.setConvertedStatus('Closed - Converted to Applicant');
               }
					enquirerConverts.add(enqConvert);
			}
		}

		return enquirerConverts;
	}

	/*
	*
	* Create List of Enquirers that are associated to the same Contact - applicable when a single person has multiple
	* Applications as part of the same transaction
	*
	*/
	private static List<Lead> createListOfEnquirersWithRelatedContactDetails(Map<String, Id> contactIdByStudentAppId, Map<String, List<Lead>> outstandingEnquirersToConvertByStudentAppId)
	{
		List<Contact> relatedContacts = [SELECT Id, AccountId FROM Contact WHERE Id IN :contactIdByStudentAppId.values()];
		List<Lead> updatedEnquirerToBeConverted = new List<Lead>();

		for(Contact con : relatedContacts)
		{
			for(String studAppId : outstandingEnquirersToConvertByStudentAppId.keySet())
			{
				Id contactId = contactIdByStudentAppId.get(studAppId);
				
				if(con.Id == contactId)
				{
					List<Lead> enquirersToUpdate = outstandingEnquirersToConvertByStudentAppId.get(studAppId);

					for(Lead enquirer : enquirersToUpdate)
					{
						enquirer.App_Contact__c = con.Id;
						enquirer.App_Account__c = con.AccountId;
						updatedEnquirerToBeConverted.add(enquirer);
					}
				}
			}
		}
		return updatedEnquirerToBeConverted;
	}

	/*
	*
	* Created List of Enquirers that have been successfully upserted with Application information to be converted
	*
	*/
	private static List<Lead> createListOfSuccessfulEnquirers(Set<Id> successfulEnquirerIds)
	{
		List<Lead> successfulEnquirers = new List<Lead>();

		if(!successfulEnquirerIds.isEmpty())
		{
			successfulEnquirers = [SELECT Id, OwnerId, RecordTypeId, Influencer__c, Type__c, Most_Recent_Application__c, 
								   App_Contact__c, App_Account__c, IsConverted, App_Student_Applicant_Id__c
								   FROM Lead 
								   WHERE Id IN :successfulEnquirerIds];
		}

		return successfulEnquirers;
	}

	/*
	*
	* Create Map of Applications that have a Student Enquirer with an associated Influencer Enquirer - both to be converted
	*
	*/
	private static Map<Id, Map<Id, Lead>> createMapOfEnquirersWithInfluencers(Map<Id, Map<Id, Lead>> enquirerRelationshipByContactIdByAppId)
	{
		Map<Id, Map<Id, Lead>> relatedContactsByAppId = enquirerRelationshipByContactIdByAppId;

	    for(Id appId : enquirerRelationshipByContactIdByAppId.keySet())
	    {
	    	Map<Id, Lead> enquirerRelationshipByContactId = enquirerRelationshipByContactIdByAppId.get(appId);

	    	if(enquirerRelationshipByContactId.size() == 1)
	    	{
	    		relatedContactsByAppId.remove(appId);
	    	}
	    }

	    return relatedContactsByAppId;
	}

	/*
	*
	* Identify what Student Enquirers that are linked to an Application have an associated Influencer Enquirer
	*
	*/
	private static Map<Id, Lead> createMapOfEnquirerByRelatedInfluencerId(List<Lead>  upsertedEnquirersToBeConverted)
	{
		List<Lead> enquirersWithInfluencers = [SELECT Id, OwnerId, RecordTypeId, Type__c, Influencer__c, Most_Recent_Application__c, IsConverted, App_Contact__c, 
												App_Account__c, App_Student_Applicant_Id__c FROM Lead 
											   WHERE Id IN :upsertedEnquirersToBeConverted
											   AND Influencer__c != null];

		Map<Id, Lead> enquirerByInfluencerId = new Map<Id, Lead>();

		for(Lead enq : enquirersWithInfluencers)
		{
			enquirerByInfluencerId.put(enq.Influencer__c, enq);
		}

		return enquirerByInfluencerId;
	}

	/*
	*
	* Create List of Influencer Enquirers that are associated to Student Enquirers that are linked to Application
	*
	*/
	public static List<Lead> createListOfInfluencerToBeConverted(List<Lead> upsertedEnquirersToBeConverted)
	{
		List<Lead> influencersToBeConverted = new List<Lead>();
		Map<Id, Lead> enquirerByInfluencerId = createMapOfEnquirerByRelatedInfluencerId(upsertedEnquirersToBeConverted);

		List<Lead> influencerEnquirers = [SELECT Id, OwnerId, RecordTypeId, Type__c, Influencer__c, 												  
												Most_Recent_Application__c, IsConverted, App_Contact__c, 		
												App_Account__c, App_Student_Applicant_Id__c FROM Lead 
										  WHERE Id IN :enquirerByInfluencerId.keySet()];

		if(!influencerEnquirers.isEmpty())
		{
			for(Lead influencer : influencerEnquirers)
			{
				Lead applicantEnquirer = enquirerByInfluencerId.get(influencer.Id);
				Id appId = applicantEnquirer.Most_Recent_Application__c;

				influencer.Most_Recent_Application__c = appId;
				influencersToBeConverted.add(influencer);			
			}
		}

		return influencersToBeConverted;
	}

	/*
	*
	* Create List of Temporary Enquirers based on Applicant data to use in FindDuplicateRecords
	*
	*/
	public static List<Lead> createListOfTempEnquirers(List<Application__c> applicationsToBeProcessed)
	{
		List<Lead> tempEnquirers = new List<Lead>();

	    for(Application__c app : applicationsToBeProcessed)
	    {
	        Lead tempEnquirer = createTempEnquirer(app);

	        if(tempEnquirer != null)
	        {
	        	tempEnquirers.add(tempEnquirer);
	        }
	    }
	    return tempEnquirers;
	}

	/*
	*
	* Create Temporary Enquirer based on Applicant data using all fields required for duplicate matching
	*
	*/
	private static Lead createTempEnquirer(Application__c app)
	{
		Lead tempEnquirer = new Lead();

		if(app.Last_Name__c != null)
	    {
	    	tempEnquirer.Most_Recent_Application__c = app.Id;
		    tempEnquirer.LastName = app.Last_Name__c;
		    tempEnquirer.LeadSource = 'Application';

		    if(app.Last_Name__c != null)
	    	{
		    	tempEnquirer.FirstName = app.First_Name__c;
		    }
		    if(app.Last_Name__c != null)
	    	{
		    	tempEnquirer.Email = app.Email_Address__c;
		    }
		    if(app.Phone_Number__c != null)
		    {
		    	tempEnquirer.Phone = app.Phone_Number__c;
		    }
		    /*else if(app.Mobile_Phone_Number__c != null)
		    {
		    	tempEnquirer.Phone = app.Mobile_Phone_Number__c;
		    } */
            if(app.Mobile_Phone_Number__c != null)
		    {
		    	tempEnquirer.MobilePhone = app.Mobile_Phone_Number__c;
		    }
		    /*if(app.Postal_Code__c != null && app.Origin__c == 'SITS')
		    {
		    	tempEnquirer.PostalCode = app.Postal_Code__c;
		    } */
		}
		return tempEnquirer;
	}

	/*
	*
	* Create Map of Updated Enquirer (containing Application information) by Application Id
	*
	*/
	public static Map<Id, Lead> updateMatchingEnquirerWithApplicantInfo(Map<Id, Lead> matchingEnquirerByAppId)
	{
		Map<Id, Lead> updatedEnquirersByAppId = new Map<Id, Lead>();

		List<Application__c> applications = [SELECT Id, Processing_Status__c, Student_Id__c, First_Name__c, Last_Name__c, 
											 Email_Address__c, Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c, 
											 Domicile__c, Domicile_Country_Code__c, Address__c, Nationality__c, Care_Leaver__c,
											 Preferred_Name__c, UCAS_School_Code__c, Studying_on_Campus__c, UCAS_Student_Id__c,
											 Admission_Cycle__c, CreatedDate, SCMS_Application_Id__c, Application_Year__c,
											 Course_Instance__c, Course_Code__c, Entry_Month__c, Entry_Month_Formula__c, Entry_Year__c, Entry_Year_Formula__c, Faculty__c,
											 Interview__c, Application_Stage__c, Offer_Status__c, Applicant_Choice__c,
											 Agent_Application__c, Mature_Student__c, Type__c, Fee_Category__c, Course__c,
											 School_College__c, Applicant__c, Applicant__r.AccountId, Origin__c, Application_Date__c
											 FROM Application__c
											 WHERE Id IN :matchingEnquirerByAppId.keySet()];

		if(!applications.isEmpty())
		{
			for(Id appId : matchingEnquirerByAppId.keySet())
			{
				Lead enq = matchingEnquirerByAppId.get(appId);

				for(Application__c app : applications)
				{
					if(app.Id == appId)
					{
						enq = mapApplicantInfoWithEnquirer(app, enq);
						updatedEnquirersByAppId.put(appId, enq);
					}
				}
			}
		}
		return updatedEnquirersByAppId;
	}

	/*
	*
	* Update Enquirer with information from Application
	*
	*/
	private static Lead mapApplicantInfoWithEnquirer(Application__c application, Lead enquirer)
	{
		Lead updatedEnquirer = enquirer;

		// Contact Mapping Fields

		updatedEnquirer.LastName = application.Last_Name__c;

		if(String.isNotBlank(application.First_Name__c))
		{
			updatedEnquirer.FirstName = application.First_Name__c;
			updatedEnquirer.Company = updatedEnquirer.FirstName + ' ' + updatedEnquirer.LastName;
		}
		else
		{
			updatedEnquirer.Company = updatedEnquirer.LastName;
		}
		if(String.isNotBlank(application.Email_Address__c))
		{
			updatedEnquirer.Email = application.Email_Address__c;
		}
		if(String.isNotBlank(application.Phone_Number__c))
		{
			updatedEnquirer.Phone = application.Phone_Number__c;
		}
		/*else if(String.isNotBlank(application.Mobile_Phone_Number__c))
		{
			updatedEnquirer.Phone = application.Mobile_Phone_Number__c;
		} */
        if(String.isNotBlank(application.Mobile_Phone_Number__c))
		{
			updatedEnquirer.MobilePhone = application.Mobile_Phone_Number__c;
		}
		if(String.isNotBlank(application.Student_Id__c))
		{
			updatedEnquirer.App_Student_Applicant_Id__c = application.Student_Id__c;
		}
		if(String.isNotBlank(application.Domicile__c))
		{
			updatedEnquirer.App_Country_of_Residence__c = application.Domicile__c;
		}
		if(String.isNotBlank(application.Domicile_Country_Code__c))
		{
			updatedEnquirer.App_Country_of_Residence_Code__c = application.Domicile_Country_Code__c;
		}
        if(application.Origin__c == 'SITS')
        {
            /*if(String.isNotBlank(application.Postal_Code__c))
            {
                updatedEnquirer.PostalCode = application.Postal_Code__c;
            } */
            if(String.isNotBlank(application.Address__c))
            {
                String applicantAddress = application.Address__c;
                
                if(applicantAddress.length() > 255)
                {
                    String first255CharsOfAddress = applicantAddress.substring(0,255);
                    String formatedStreetAddress = first255CharsOfAddress.substringBeforeLast(',');
                 //   updatedEnquirer.Street = formatedStreetAddress;
                    
                    String remainingAddressUnformatted = applicantAddress.remove(formatedStreetAddress);
                    updatedEnquirer.App_Additional_Address_Lines__c = remainingAddressUnformatted.substringAfter(',');
                }
                else
                {
                   // updatedEnquirer.Street = applicantAddress;
                }
            }
        }

		if(String.isNotBlank(application.Nationality__c))
		{
			updatedEnquirer.App_Nationality__c = application.Nationality__c;
		}
		if(String.isNotBlank(application.Preferred_Name__c))
		{
			updatedEnquirer.Preferred_Name__c = application.Preferred_Name__c;
		}
		if(String.isNotBlank(application.UCAS_Student_Id__c))
		{
			updatedEnquirer.App_UCAS_Personal_Id__c = application.UCAS_Student_Id__c;
		}

		// Opportunity Mapping Fields

		if(String.isNotBlank(String.valueOf(application.Admission_Cycle__c)))
		{
			updatedEnquirer.App_Admissions_Cycle__c = String.valueOf(application.Admission_Cycle__c);
		}
		if(String.isNotBlank(application.SCMS_Application_Id__c))
		{
			updatedEnquirer.App_Application_Id__c = application.SCMS_Application_Id__c;
		}
		if(String.isNotBlank(application.Application_Year__c))
		{
			updatedEnquirer.App_Application_Year__c = application.Application_Year__c;
		}
		if(String.isNotBlank(application.Entry_Month_Formula__c))
		{
			updatedEnquirer.App_Entry_Month__c = application.Entry_Month_Formula__c;
		}
		if(String.isNotBlank(application.Entry_Year_Formula__c))
		{
			updatedEnquirer.App_Entry_Year__c = application.Entry_Year_Formula__c;
		}
		if(String.isNotBlank(application.Faculty__c))
		{
			updatedEnquirer.App_Faculty__c = application.Faculty__c;
		}
		if(String.isNotBlank(application.Application_Stage__c))
		{
			updatedEnquirer.App_Stage_Name__c = application.Application_Stage__c;
		}
		if(String.isNotBlank(application.Offer_Status__c))
		{
			updatedEnquirer.App_Offer_Status__c = application.Offer_Status__c;
		}
		if(String.isNotBlank(application.Applicant_Choice__c))
		{
			updatedEnquirer.App_Applicant_Choice__c = application.Applicant_Choice__c;
		}
		if(String.isNotBlank(application.Type__c))
		{
			updatedEnquirer.App_Application_Type__c = application.Type__c;
		}
		if(String.isNotBlank(application.Fee_Category__c))
		{
			updatedEnquirer.App_Fee_Status__c = application.Fee_Category__c;
		}
		if(String.isNotBlank(application.Applicant__c) && String.isNotBlank(application.Applicant__r.AccountId))
		{
			 updatedEnquirer.App_Contact__c = application.Applicant__c;
			 updatedEnquirer.App_Account__c = application.Applicant__r.AccountId;
		}

		updatedEnquirer.Directmail_Consent__c = 'Yes';
		updatedEnquirer.Email_Consent__c = 'Yes';
		updatedEnquirer.Phone_Consent__c = 'Yes';
		updatedEnquirer.SMS_Consent__c = 'Yes';
		updatedEnquirer.WhatsApp_Consent__c = 'Yes';
		updatedEnquirer.DoNotCall = false;
		updatedEnquirer.Directmail_Opt_Out__c = false;
		updatedEnquirer.HasOptedOutOfEmail = false;
		updatedEnquirer.et4ae5__HasOptedOutOfMobile__c = false;
		updatedEnquirer.WhatsApp_Opt_Out__c = false;
		updatedEnquirer.Most_Recent_Application__c = application.Id;
		updatedEnquirer.App_Care_Leaver__c = application.Care_Leaver__c;
		updatedEnquirer.School_College__c = application.School_College__c;
		updatedEnquirer.App_Course__c = application.Course__c;
		updatedEnquirer.App_Studying_on_Campus__c = application.Studying_on_Campus__c;
		updatedEnquirer.App_Interview__c = application.Interview__c;
		updatedEnquirer.App_Mature_Student__c = application.Mature_Student__c;
		updatedEnquirer.App_Agent_Application__c = application.Agent_Application__c;
        
        Application_Origins__c systemOrigins = Application_Origins__c.getInstance(userInfo.getProfileId());
        string sitsOrigin = systemOrigins.SITS_Origin_String__c;
        string quercusOrigin = systemOrigins.Quercus_Origin_String__c;
        
        if(application.Origin__c == sitsOrigin)
        {
            updatedEnquirer.App_Application_Date__c = application.Application_Date__c;
        }
        else
        {
            updatedEnquirer.App_Application_Date__c = application.CreatedDate.date();
        }
		

		return updatedEnquirer;
	}

	/*
	*
	* Used to identify whether there are multiple enquirers for the same applicant and categorises them under the same Student Applicant Id
	*
	*/
	public static Map<String, List<Lead>> createMapOfEnquirersByStudentAppId(List<Lead> upsertedEnquirersToBeConverted)
	{
		Map<String, List<Lead>> enquirersByStudentAppId = new Map<String, List<Lead>>();
		
		for(Lead enq : upsertedEnquirersToBeConverted)
		{
			if(enquirersByStudentAppId.containsKey(enq.App_Student_Applicant_Id__c))
			{
				enquirersByStudentAppId.get(enq.App_Student_Applicant_Id__c).add(enq);
			}
			else 
			{
				enquirersByStudentAppId.put(enq.App_Student_Applicant_Id__c, new List<Lead>{enq});
			}
		}

		return enquirersByStudentAppId;
	}
    
      /*		
    *		
    * This method is used to identify LEIs on Lead and Delete old LEI's  		
    *		
    */		
    public static void deleteDuplicateLEIs(List<Lead> deletedLeads)		
    {		
        //Map which contains Lead ID and List of LEI's		
        Map<Id, List<Latest_Enquiry_Information__c>> mapLEI = new Map<Id, List<Latest_Enquiry_Information__c>>();		
        //Map for duplicate indentification key(Enquirer__c+Level_of_Study__c+Year_of_Entry__c) and list of LEI records		
        Map<String, List<Latest_Enquiry_Information__c>> keyLEIMap = new Map<String, List<Latest_Enquiry_Information__c>>();		
        // List to delete duplicate Latest_Enquiry_Information__c		
        List<Latest_Enquiry_Information__c> list2Delete = new List<Latest_Enquiry_Information__c>();		
        		
        //Master Lead(Enquirer) record ids for the merged Leads		
        Set<Id> masterRecordIds = new Set<Id>();		
              		
        for(Lead ld  : deletedLeads)		
        {     		
            if(ld.MasterRecordId != null)		
            {		
                masterRecordIds.add(ld.MasterRecordId);        		
            }		
        }		
        		
        if(!deletedLeads.isEmpty())		
        {		
            for(Latest_Enquiry_Information__c lei : [SELECT Id, Enquirer__c , Level_of_Study__c , Year_of_Entry__c		
                                                     FROM Latest_Enquiry_Information__c  WHERE Enquirer__c IN :masterRecordIds ORDER BY LastModifiedDate DESC] )		
            {		
                String leiKey = lei.Enquirer__c+lei.Level_of_Study__c+lei.Year_of_Entry__c;		
                if(keyLEIMap.containsKey(leiKey))		
                {     		
                      keyLEIMap.get(leiKey).add(lei);		
                }		
                else		
                {		
                      keyLEIMap.put(leiKey,new List<Latest_Enquiry_Information__c>{lei});		
                }		
            }		
            for(String key : keyLEIMap.keySet())		
            {		
                if(keyLEIMap.get(key).size() > 0)		
                {		
                    List<Latest_Enquiry_Information__c> similarLEIs = new List<Latest_Enquiry_Information__c>();		
                    similarLEIs.addAll(keyLEIMap.get(key));		
                    similarLEIs.remove(0); 		
                    list2Delete.addAll(similarLEIs);		
                }		
            }		
            system.debug('list2Delete::==' + list2Delete);                                                                                                             		
        }		
        if(!list2Delete.isEmpty())		
        {		
            delete list2Delete;		
        }		
    }	
    /*
    *
    * To check if a contact already exists. if it does then vconvert the newly cerated lead and then merge it
    * (on Formstack webforms only)
    *
		Caused deployment issues for SITs project so commented out
    */
/*	public static void convertNewLeadAndMergeToExistingContact(List<Lead> enquirerRecords)
	{
        String leadFirstName;
        String leadLastName;
        String leadEmail;
        ID contactOwner;
        ID contactAccount;
        ID newContact;

		for(Lead enquirer : enquirerRecords)
		{
            if(enquirer.Enquiry_Channel__c != 'Web Form')
            {
                continue;
            }

            Id StudentEnquirerRecordID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Student').getRecordTypeId(); 
            if(enquirer.RecordTypeID != StudentEnquirerRecordID)
            {
                continue;
            }

            List<Contact> theContact = getContactReplicationExactMatchingRule(enquirer.FirstName,enquirer.LastName,enquirer.Email,enquirer.PostalCode,enquirer.Phone);

            // If we find a contact than change lead email, convert it to a contact and merge with existing contact
			if(theContact.size() != 0)
			{
                //Corrupt the lead email so the matching rules dont fall over
                Lead leadToConvert = [Select Id,Email FROM Lead WHERE Id = :enquirer.Id];
                leadToConvert.Email = leadToConvert.Email + 'tempremove';
                update leadToConvert;

                contactAccount = getAccountForContact(theContact[0].Id);

                newContact = convertLeadAndReturnContact(leadToConvert.Id,contactAccount);
                Contact convertedContact = [SELECT Id, Email FROM Contact WHERE Id = :newContact];

                 try {
                    merge theContact[0] convertedContact;
                } catch (DmlException e) {
                    System.debug('An unexpected error has occurredhas occured merging contacts: ' + e.getMessage());
                }
			}
		}
	}
    
    private static ID getAccountForContact(ID cn)
	{
        List<Contact> contact = [SELECT Id,AccountId from Contact WHERE Id=:cn ];
        if(contact.size() == 0)
        {
            return null;
        } else {
            return contact[0].AccountId;
        }
	}


	private static ID convertLeadAndReturnContact(ID theLead, ID theAccount)
	{
        ID newContactId;
        Database.LeadConvert lc = new database.LeadConvert();
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];

        lc.setLeadId(theLead);
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setAccountId(theAccount);
        lc.setDoNotCreateOpportunity(TRUE);
        try {
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            newContactId=lcr.getContactId();        
        } catch (DMLException e) {
            System.debug('An unexpected error has occurred converting the lead: ' + e.getMessage());
        }
    return newContactId;
	}
    
    private static List<Contact> getContactReplicationExactMatchingRule(String fn, String ln, String em, String pc, String phone)
	{
        List<Contact> lstContacts = new List<Contact>();

        List<Contact> contact = [
            SELECT Id,OwnerId from Contact 
            WHERE 
            (
                FirstName=:fn
                AND LastName=:ln 
                AND email=:em 
                AND FirstName != null 
                AND LastName != null 
                AND email != null
            ) OR (
                LastName=:ln 
                AND Email=:em 
                AND phone=:phone  
                AND LastName != null 
                AND Email != null 
                AND phone != null
            ) OR (
                LastName=:ln 
                AND Email=:em 
                AND MailingPostalCode=:pc 
                AND LastName!= null  
                AND Email != null  
                AND MailingPostalCode != null
            )];
        if(contact.size() > 0)
        {
            lstContacts.add(contact[0]);
            System.debug('found first name');
            return lstContacts;
        }

        return lstContacts;
	} */
/*		
    *		
    * To check if a contact already exists. if it does then vconvert the newly cerated lead and then merge it		
    * (on Formstack webforms only)		
    *		
    */		
    /*		
	public static void convertNewLeadAndMergeToExistingContact(List<Lead> enquirerRecords)		
	{		
        String leadFirstName;		
        String leadLastName;		
        String leadEmail;		
        ID contactOwner;		
        ID contactAccount;		
        ID newContact;		
		for(Lead enquirer : enquirerRecords)		
		{		
            if(enquirer.Enquiry_Channel__c != 'Web Form')		
            {		
                continue;		
            }		
            Id StudentEnquirerRecordID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Student').getRecordTypeId(); 		
            if(enquirer.RecordTypeID != StudentEnquirerRecordID)		
            {		
                continue;		
            }		
            List<Contact> theContact = getContactReplicationExactMatchingRule(enquirer.FirstName,enquirer.LastName,enquirer.Email,enquirer.PostalCode,enquirer.Phone);		
            // If we find a contact than change lead email, convert it to a contact and merge with existing contact		
			if(theContact.size() != 0)		
			{		
                //Corrupt the lead email so the matching rules dont fall over		
                Lead leadToConvert = [Select Id,Email FROM Lead WHERE Id = :enquirer.Id];		
                leadToConvert.Email = leadToConvert.Email + 'tempremove';		
                update leadToConvert;		
                contactAccount = getAccountForContact(theContact[0].Id);		
                newContact = convertLeadAndReturnContact(leadToConvert.Id,contactAccount);		
                Contact convertedContact = [SELECT Id, Email FROM Contact WHERE Id = :newContact];		
                 try {		
                    merge theContact[0] convertedContact;		
                } catch (DmlException e) {		
                    System.debug('An unexpected error has occurredhas occured merging contacts: ' + e.getMessage());		
                }		
			}		
		}		
	}		
	private static List<Contact> getContactReplicationExactMatchingRule(String fn, String ln, String em, String pc, String phone)		
	{		
        List<Contact> lstContacts = new List<Contact>();		
        List<Contact> contact = [		
            SELECT Id,OwnerId from Contact 		
            WHERE 		
            (		
                FirstName=:fn		
                AND LastName=:ln 		
                AND email=:em 		
                AND FirstName != null 		
                AND LastName != null 		
                AND email != null		
            ) OR (		
                LastName=:ln 		
                AND Email=:em 		
                AND phone=:phone  		
                AND LastName != null 		
                AND Email != null 		
                AND phone != null		
            ) OR (		
                LastName=:ln 		
                AND Email=:em 		
                AND MailingPostalCode=:pc 		
                AND LastName!= null  		
                AND Email != null  		
                AND MailingPostalCode != null		
            )];		
        if(contact.size() > 0)		
        {		
            lstContacts.add(contact[0]);		
            System.debug('found first name');		
            return lstContacts;		
        }		
        return lstContacts;		
	}		
	private static ID getAccountForContact(ID cn)		
	{		
        List<Contact> contact = [SELECT Id,AccountId from Contact WHERE Id=:cn ];		
        if(contact.size() == 0)		
        {		
            return null;		
        } else {		
            return contact[0].AccountId;		
        }		
	}		
	private static ID convertLeadAndReturnContact(ID theLead, ID theAccount)		
	{		
        ID newContactId;		
        Database.LeadConvert lc = new database.LeadConvert();		
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];		
        lc.setLeadId(theLead);		
        lc.setConvertedStatus(convertStatus.MasterLabel);		
        lc.setAccountId(theAccount);		
        lc.setDoNotCreateOpportunity(TRUE);		
        try {		
            Database.LeadConvertResult lcr = Database.convertLead(lc);		
            newContactId=lcr.getContactId();        		
        } catch (DMLException e) {		
            System.debug('An unexpected error has occurred converting the lead: ' + e.getMessage());		
        }		
    return newContactId;		
	}		
*/		

}