/*****************************************************************
 * bg_AssignRecUsingAssignmentRules_Test
 *
 * Test methods for bg_AssignRecordsUsingAssignmentRules class
 *
 * Author: BrightGen
 * Created: 29-09-2018
 * 
 * ************** Version history ***********************
 * CT, Dinesh : 10/07/2020 - Introduced custom label to store Case Record Type Developer Name 
******************************************************************/
@isTest
private class bg_AssignRecUsingAssignmentRules_Test 
{
	
	/*
	*
	* To test assignment rules are being run for Enquirer records
	*
	*/
	@isTest private static void testAssignmentRulesForEnquirer() 
	{
      List<id> enquirerIds= new List<Id>();

      Lead testEnquirer = bg_UnitTestDataFactory.createStudentEnquirer();
      testEnquirer.Residence_Status__c = 'UK';
      testEnquirer.OwnerId = UserInfo.getUserId();
      insert testEnquirer;

      enquirerIds.add(testEnquirer.Id);

      Test.startTest();

      Lead enquirersPreUpdate = [SELECT Id, OwnerId FROM Lead WHERE Id IN :enquirerIds LIMIT 1];
      Boolean ownerIsUser = bg_CommonUtils.isIdOfType(enquirersPreUpdate.OwnerId, '005');
      
      System.assert(ownerIsUser);

      bg_AssignRecordsUsingAssignmentRules.assignRecords(enquirerIds);

      Lead enquirersPostUpdate = [SELECT Id, OwnerId FROM Lead WHERE Id = :enquirersPreUpdate.Id LIMIT 1];
      Boolean ownerIsQueue = bg_CommonUtils.isIdOfType(enquirersPostUpdate.OwnerId, '00G');
      
      System.assert(ownerIsQueue);

      Test.stopTest();
	}

	/*
	*
	* To test assignment rules are being run for Case records
	*
	*/
	@isTest private static void testAssignmentRulesForCase() 
	{
      List<id> caseIds= new List<Id>();
      //CT, Dinesh : 10/07/2020 : Introduced custom label to store Record Type Developer Name
      Case testCase = bg_UnitTestDataFactory.createEnquiryOfRecordType(System.Label.Case_Prospectus_Request_Record_Type);
      testCase.Type = 'Prospectus Request';
      testCase.OwnerId = UserInfo.getUserId();
      insert testCase;

      caseIds.add(testCase.Id);

      Test.startTest();

      Case casesPreUpdate = [SELECT Id, OwnerId FROM Case WHERE Id IN :caseIds LIMIT 1];
      Boolean ownerIsUser = bg_CommonUtils.isIdOfType(casesPreUpdate.OwnerId, '005');
      
      System.assert(ownerIsUser);

      bg_AssignRecordsUsingAssignmentRules.assignRecords(caseIds);

      Case casesPostUpdate = [SELECT Id, OwnerId FROM Case WHERE Id = :casesPreUpdate.Id LIMIT 1];
      Boolean ownerIsQueue = bg_CommonUtils.isIdOfType(casesPostUpdate.OwnerId, '00G');

      System.assert(ownerIsQueue);

      Test.stopTest();
	}		
}