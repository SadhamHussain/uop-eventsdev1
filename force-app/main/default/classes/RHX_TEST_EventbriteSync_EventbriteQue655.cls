@isTest(SeeAllData=true)
public class RHX_TEST_EventbriteSync_EventbriteQue655 {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM EventbriteSync__EventbriteQuestion__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new EventbriteSync__EventbriteQuestion__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}