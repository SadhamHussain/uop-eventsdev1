/**
   * @File Name          : ct_VolunteerHoursTriggerHelper.cls
   * @Author             : Creation Admin
   * @Last Modified By   : Creation Admin
   * Description         : To send an email alert to confirmed volunteers
  **/
public class ct_VolunteerHoursTriggerHelper {
   /**
      * @description - Method to send Email Alerts for confirmed volunteer hours
      * @param volunteerHoursIds - Volunteer Hours Ids 
      * @return void 
      **/
      public static void sentEmailAlertToCompletedVolunteerHours(Set<Id> volunteerHoursIds){ 
      Id orgWideEmailId;
      //Get UOP Advancement Team Email Address
      OrgWideEmailAddress[] orgWideAddress = ct_Util.getOrgWideEmail(ct_Constants.UOP_ADVANCEMENT_TEAM_ADDRESS); 
      if (orgWideAddress.size() > 0 ) { 
          orgWideEmailId = orgWideAddress.get(0).Id; 
      }
      
      //Get Send thank comms Email Template
      EmailTemplate thisSendThankCommsTemplate = [SELECT Id, DeveloperName 
                                                           FROM EmailTemplate 
                                                           WHERE DeveloperName =: ct_Constants.SEND_THANK_COMMS
                                                           AND IsActive = true];

        //List of SingleEmailMessage to be Sent
        List<Messaging.SingleEmailMessage> mailListToSend = new List<Messaging.SingleEmailMessage>();
        if(orgWideEmailId != null){
        // Loop through the volunteer hours by volunteerHoursIds send an email
        for(GW_Volunteers__Volunteer_Hours__c thisVolunteerHours : [SELECT Id,
                                                                        GW_Volunteers__Contact__c,
                                                                        GW_Volunteers__Contact__r.Email,    
                                                                        GW_Volunteers__Contact__r.IsEmailBounced
                                                                        FROM GW_Volunteers__Volunteer_Hours__c
                                                                        WHERE Id IN: volunteerHoursIds]){
            // checking if the email address is available for sending an email (Check if already bounced or not)
            if(!thisVolunteerHours.GW_Volunteers__Contact__r.IsEmailBounced
               && thisVolunteerHours.GW_Volunteers__Contact__r.Email != null
               && thisSendThankCommsTemplate != null){
               // Add this values to mailListToSend for create email message through createVolunteerHoursEmailMessageby passing required parameters
               mailListToSend.add(createVolunteerHoursEmailMessage(thisVolunteerHours.GW_Volunteers__Contact__r.Email, thisVolunteerHours.Id, thisVolunteerHours.GW_Volunteers__Contact__c,  thisSendThankCommsTemplate.Id, orgWideEmailId));                                                                            
            }                                                  
        }
        
        if(!Test.isRunningTest()
           && !mailListToSend.isEmpty()){
           // Sending email to respective email addresses
           Messaging.sendEmail(mailListToSend); 
        }
      }
    }
      
    
    /**
    * @description - Method to create SingleEmailMessage for Volunteer 
    * @param toEmailAddress - Volunteer Email Address
    * @param volunteerHoursId - Volunteer hours ids
    * @param thisContactId - Volunteer Contact Id
    * @param thankYouEmailTempId - Volunteer Send Thank You comms template
    * @param orgwideEmailId - UOP Advancement Team Org wide Address Id
    * @return Messaging.SingleEmailMessage to sentEmailAlertToConfirmedVolunteerHours method 
    **/  
    public static Messaging.SingleEmailMessage createVolunteerHoursEmailMessage(String toEmailAddress,Id volunteerHoursId, Id thisContactId, Id thankYouEmailTempId, Id orgwideEmailId){
      //Create email message
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setOrgWideEmailAddressId(orgwideEmailId); 
      mail.setToAddresses(new String[]{toEmailAddress});
      mail.setTargetObjectId(thisContactId);
      mail.setTemplateId(thankYouEmailTempId);
      mail.setWhatId(volunteerHoursId);
      return mail;
    }
}