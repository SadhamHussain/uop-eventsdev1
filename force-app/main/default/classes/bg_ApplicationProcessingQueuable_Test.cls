@isTest
private class bg_ApplicationProcessingQueuable_Test {
	
	
    

    
	@isTest(seealldata = true)
    private static void testQueueableJob() 
	{
        List<Application_Origins__c> appOrigins = [SELECT Id FROM Application_Origins__c];
        if(appOrigins.isEmpty())
        {
            Application_Origins__c systemOrigins = new Application_Origins__c();
            systemOrigins.Quercus_Origin_String__c = 'Quercus';
            systemOrigins.SITS_Origin_String__c = 'SITS';
            insert systemOrigins; 
        }

        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Contact_AppProcessing_TDTM', 
                                                     'Contact', 'BeforeInsert;AfterInsert;BeforeUpdate;AfterUpdate', 10.00));
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Opportunity_AppProcessing_TDTM', 
                                                     'Opportunity', 'BeforeInsert;AfterInsert;BeforeUpdate;AfterUpdate', 10.00));
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Application_Processing_TDTM', 
                                                     'Application__c', 'BeforeInsert;AfterInsert;BeforeUpdate;AfterUpdate', 1.00));
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Enquirer_Conversion_TDTM', 
													 'Lead', 'BeforeInsert;BeforeUpdate', 2.00));
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        
	    Lead testEnquirer1 = bg_UnitTestDataFactory.createStudentEnquirer();
		testEnquirer1.FirstName = 'test928384';
		testEnquirer1.LastName = 'user';
		testEnquirer1.Email = 'test928384@user.com';
		testEnquirer1.Phone = '01403785206';
		testEnquirer1.PostalCode = 'E2 8YS';
		insert testEnquirer1;

		Account courseAccount = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount.Course_Start_Month__c = '09';
		courseAccount.Course_Start_Year__c = '2020';
        courseAccount.Academic_Programme_Course_Code__c = 'acprogcc';
        courseAccount.SITS_Course_Code__c = 'acprogcc';
	    insert courseAccount;

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
	    testApp1.First_Name__c = 'test928384'; 
	    testApp1.Last_Name__c = 'user';
	    testApp1.Email_Address__c = 'test928384@user.com';
	    testApp1.Phone_Number__c = '01403785206';
	    testApp1.Postal_Code__c = 'E2 8YS';
        testApp1.Course_Code__c = 'acprogcc';
	    testApp1.School_College__c = schoolAccount.Id;
        testApp1.Student_Id__c = 'abcdstudentid';
        testApp1.Origin__c = 'Quercus';

	    List<Application__c> testApplications = new List<Application__c>{testApp1};
	    test.startTest();
            insert testApplications;
        test.stopTest();

	    List<Application__c> appsToProcess = [SELECT Id, Processing_Status__c FROM Application__c 
																WHERE Id IN :testApplications];

		Set<Id> appIdsSet = (new Map<Id,Application__c>(appsToProcess)).keySet();


		List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
															 Enquirer__c, Opportunity__c, Applicant__c, Valid_Application_Data__c
															 FROM Application__c
															 WHERE Id IN :appIdsSet];
        system.debug(updatedTestApplications);

		Contact applicantContact = [SELECT Id FROM Contact WHERE Student_Applicant_Id__c = :updatedTestApplications[0].Student_Id__c]; 
		Opportunity newOpportunity = [SELECT Id FROM Opportunity WHERE Application_Id__c = :updatedTestApplications[0].SCMS_Application_Id__c]; 

		system.assertEquals(testEnquirer1.Id, updatedTestApplications[0].Enquirer__c);
		system.assertEquals(applicantContact.Id, updatedTestApplications[0].Applicant__c);
		system.assertEquals(newOpportunity.Id, updatedTestApplications[0].Opportunity__c);


	}
	
}