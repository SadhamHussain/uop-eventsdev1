/**
 * @File Name          : ct_hed_AffiliationTriggerHandler_TDTM.cls
 * @Description        : TDTM Trigger Handler for Hed_Affiliation Object
 * @Author             : Creation Admin
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 6/25/2020, 1:40:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/16/2020   Creation Admin     Initial Version
**/
global class ct_hed_AffiliationTriggerHandler_TDTM extends hed.TDTM_Runnable {

 
  global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
    hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {

    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CT HED AFFILIATION TDTM TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
    //Set Of Contact Ids for which the Custom Affiliation Roll up needs to processed
    Set<Id> statusRoleUpdatedAffiliationContactIds    = new Set<Id>();
    //Set to store contact Ids
    Set<Id> contactIds                                = new Set<Id>();
    //Set to store Account Ids
    Set<Id> accountIds                                = new Set<Id>();
    //Map to store Contact Records
    Map<Id, Contact> contactMap                       = new Map<Id, Contact>();
    //Map to store Account Records
    Map<Id, Account> accountMap                       = new Map<Id, Account>();
    //Map to store Contact Records to be updated
    Map<Id, Contact> contactMapToUpdate               = new Map<Id, Contact>();
     //Set to store contact Ids
     Set<Id> raocontactIds                                = new Set<Id>();
     //Set to store Account Ids
     Set<Id> raoaccountIds                                = new Set<Id>();



    if (triggerAction == hed.TDTM_Runnable.Action.BeforeDelete) {
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>HED AFFILIATION BEFORE DELETE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

      for(hed__Affiliation__c thisAffiliation : (list<hed__Affiliation__c>)oldlist){
        if(thisAffiliation.hed__Status__c != null
          && thisAffiliation.hed__Role__c != null
          && thisAffiliation.hed__Contact__c != null){
          //Collect Contact Record Id for processing Custom Roll up
          statusRoleUpdatedAffiliationContactIds.add(thisAffiliation.hed__Contact__c);
        }
        if(thisAffiliation.hed__Contact__c != null){
          contactIds.add(thisAffiliation.hed__Contact__c);
        }
        if(thisAffiliation.hed__Account__c != null){
          //Collect Account Id
          accountIds.add(thisAffiliation.hed__Account__c);
        }
      }
      if(!accountIds.isEmpty() && !contactIds.isEmpty() && !System.isFuture() && !System.isBatch()){
        //Invoke future method to process custom affiliation rollup on Organsation Account 
        ct_hed_AffiliationTriggerHelper.updateRAOContactAndAccountRecords(accountIds, contactIds);
      }
    }

    if (triggerAction == hed.TDTM_Runnable.Action.AfterInsert){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>HED AFFILIATION AFTER INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

      //Loop to get The required Ids 
      for(hed__Affiliation__c thisAffiliation : (list<hed__Affiliation__c>)newlist){
        if(thisAffiliation.hed__Status__c != null
          && thisAffiliation.hed__Role__c != null
          && thisAffiliation.hed__Contact__c != null){
            //Collect Contact Record Id for processing Custom Roll up
            statusRoleUpdatedAffiliationContactIds.add(thisAffiliation.hed__Contact__c);
        }
        System.debug('thisAffiliation.RAO_Affiliation__c'+ thisAffiliation.RAO_Affiliation__c);
        if(thisAffiliation.RAO_Affiliation__c
        && thisAffiliation.hed__Status__c == ct_Constants.AFFILIATION_STATUS_CURRENT
        && thisAffiliation.hed__Contact__c != null
        && thisAffiliation.hed__Account__c != null){
          raocontactIds.add(thisAffiliation.hed__Contact__c);
          raoaccountIds.add(thisAffiliation.hed__Account__c);
        }

        if(thisAffiliation.hed__Contact__c != null){
          //Collect Contact Id
          contactIds.add(thisAffiliation.hed__Contact__c);
        }

        if(thisAffiliation.hed__Account__c != null){
          //Collect Account Id
          accountIds.add(thisAffiliation.hed__Account__c);
        }
      }

      if(!raoaccountIds.isEmpty() && !raocontactIds.isEmpty()){
        System.debug('updateRAOOnAccountAndContact');
        ct_hed_AffiliationTriggerHelper.updateRAOOnAccountAndContact(raoaccountIds, raocontactIds);
      }

      //Get Required Map based on the ID Set collected above loop
      accountMap = ct_hed_AffiliationTriggerHelper.getAccountMap(accountIds);
      contactMap = ct_hed_AffiliationTriggerHelper.getContactMap(contactIds);
      //Add the processed Contact record in DML wrapper which will be process by TDTM Handler
      dmlWrapper.objectsToUpdate.addAll(ct_hed_AffiliationTriggerHelper.updateContactRecord(contactMap, accountMap, (list<hed__Affiliation__c>)newlist));
    }

    if (triggerAction == hed.TDTM_Runnable.Action.AfterUpdate){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>HED AFFILIATION AFTER UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

      Map<Id, hed__Affiliation__c> triggerOldMap = new Map<Id, hed__Affiliation__c>((list<hed__Affiliation__c>)oldlist);
      Map<Id, hed__Affiliation__c> triggerNewMap = new Map<Id, hed__Affiliation__c>((list<hed__Affiliation__c>)newlist);

      //Loop to get The required Ids 
      for(hed__Affiliation__c thisAffiliation : triggerNewMap.values()){
        if(thisAffiliation.hed__Status__c != null
        && thisAffiliation.hed__Role__c != null
        && thisAffiliation.hed__Contact__c != null
        && (thisAffiliation.hed__Status__c != triggerOldMap.get(thisAffiliation.Id).hed__Status__c
            || thisAffiliation.hed__Role__c != triggerOldMap.get(thisAffiliation.Id).hed__Role__c
            || thisAffiliation.hed__Contact__c != triggerOldMap.get(thisAffiliation.Id).hed__Contact__c)){
              //Collect Contact Record Id for processing Custom Roll up
              statusRoleUpdatedAffiliationContactIds.add(thisAffiliation.hed__Contact__c);
        }

        if(thisAffiliation.hed__Contact__c != null
        && thisAffiliation.hed__Account__c != null
        &&(thisAffiliation.hed__Status__c !=  triggerOldMap.get(thisAffiliation.Id).hed__Status__c
        || thisAffiliation.RAO_Affiliation__c !=  triggerOldMap.get(thisAffiliation.Id).RAO_Affiliation__c
        || thisAffiliation.hed__Contact__c != triggerOldMap.get(thisAffiliation.Id).hed__Contact__c
        || thisAffiliation.hed__Account__c != triggerOldMap.get(thisAffiliation.Id).hed__Account__c)){
          raocontactIds.add(thisAffiliation.hed__Contact__c);
          raoaccountIds.add(thisAffiliation.hed__Account__c);
        }

        if(thisAffiliation.hed__Contact__c != null){
          //Collect Contact Id
          contactIds.add(thisAffiliation.hed__Contact__c);
        }

        if(thisAffiliation.hed__Account__c != null){
          //Collect Account Id
          accountIds.add(thisAffiliation.hed__Account__c);
        }
      }
      
      //Get Required Map based on the ID Set collected above loop
      accountMap = ct_hed_AffiliationTriggerHelper.getAccountMap(accountIds);
      contactMap = ct_hed_AffiliationTriggerHelper.getContactMap(contactIds);
      //Add the processed Contact record in DML wrapper which will be process by TDTM Handler
      dmlWrapper.objectsToUpdate.addAll(ct_hed_AffiliationTriggerHelper.updateContactRecord(contactMap, accountMap, (list<hed__Affiliation__c>)newlist));
      if(!raoaccountIds.isEmpty() && !raocontactIds.isEmpty() && !System.isFuture() && !System.isBatch()){
        //Invoke future method to process custom affiliation rollup on Organsation Account 
        ct_hed_AffiliationTriggerHelper.updateRAOContactAndAccountRecords(raoaccountIds, raocontactIds);
      }
    }

    

    if(!statusRoleUpdatedAffiliationContactIds.isEmpty() && !System.isFuture() && !System.isBatch()){
      //Invoke future method to process custom affiliation rollup on contact
      ct_hed_AffiliationTriggerHelper.updateContactCustomRollupFields(statusRoleUpdatedAffiliationContactIds);
    }

    if(!accountIds.isEmpty() && !System.isFuture() && !System.isBatch()){
      //Invoke future method to process custom affiliation rollup on Organsation Account 
      ct_hed_AffiliationTriggerHelper.updateStudentOrganisationAccountRollup(accountIds);
    }

    


    return dmlWrapper;
  }
}