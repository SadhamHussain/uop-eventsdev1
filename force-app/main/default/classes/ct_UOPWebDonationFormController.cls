/**
 * @File Name          : ct_UOPWebDonationFormController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/6/2020, 8:05:18 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/18/2019   Creation Admin     Initial Version
**/
public class ct_UOPWebDonationFormController {
  //Schema Map to hold a map fields for the given sObject
  public static Map<String, Schema.SObjectType> objectSchemaMap = new Map<String, Schema.SObjectType>();

  /**
    * Method to Populate Picklist Wrapper to the Map from set of string 
    * @param mapKey : Key for which Picklist Wrapper to be assigned
    * @param thisPicklistMap : Map of Picklist Wrapper List
    * @param picklistValuesSet : Set of string which contains Picklist values
  */
  public static void generatePicklistValues(String mapKey, Map<String, List<PicklistEntryWrapper>> thisPicklistMap, Set<String> picklistValuesSet){
     //Picklist map to hold picklist entries and key to get the value
    List<PicklistEntryWrapper> thisPickListWrapperList = new List<PicklistEntryWrapper>();
    
    //Loop through the list picklist values provided to construct picklist options
    for(String thisValue :picklistValuesSet){
      thisPickListWrapperList.add(new PicklistEntryWrapper(thisValue, thisValue));
    }
    thisPicklistMap.put(mapKey, thisPickListWrapperList);
  }


  /**
    * Method to Populate Picklist Wrapper with GAU Units to the Map from set of string 
    * @param mapKey : Key for which Picklist Wrapper to be assigned
    * @param thisPicklistMap : Map of Picklist Wrapper List
    * @param picklistValuesSet : Set of string which contains Picklist values
  */
  public static void getGAUUnits(String mapKey, Map<String, List<PicklistEntryWrapper>> thisPicklistMap){
    //Define GAU fields that needs to be queried
    Set<String> gauFieldsToQuery = new Set<String>{'Id', 'Name', 'Web_Label__c', 'npsp__Active__c'};
        
    List<PicklistEntryWrapper> thisPickListWrapperList = new List<PicklistEntryWrapper>();
    //Loop through the GAU records to construct GAU Name & Id key value pair for picklist options
    for(npsp__General_Accounting_Unit__c thisGAUUnit : Database.query(ct_Util.getQueryStringForSObject('npsp__General_Accounting_Unit__c', gauFieldsToQuery)+' WHERE npsp__Active__c = true'+' AND Web_Label__c != null ORDER BY Name ASC ')){
      	thisPickListWrapperList.add(new PicklistEntryWrapper(thisGAUUnit.Web_Label__c, thisGAUUnit.Id));
    }
    thisPicklistMap.put(mapKey, thisPickListWrapperList);
  }

  /**
    * Method to create Donation Opportunity, Donation Contact and Create payment
    * @param Dontaion Object : String that contains Payment and Donor Details
    * @return void
  */
  @AuraEnabled
  public static string saveDonationDetails(String donationDetails){
    Map<String, Object> donorObject;
    try{
      //Deserialize the JSON string to map object & string get object values
      donorObject = (Map<String, Object>)JSON.deserializeUntyped(donationDetails);

      //create a instance for Web Site Submission with web form information 
      Web_Site_Submission__c webSubmissionObject = new Web_Site_Submission__c();
      webSubmissionObject.Name                   = 'Donation - '+String.valueOf(donorObject.get('FirstName'))+'-'+String.valueOf(donorObject.get('LastName'));
      webSubmissionObject.Source__c				       = ct_Constants.WEB_SUBMISSION_SOURCE_DONATION;
      webSubmissionObject.Email__c               = String.valueOf(donorObject.get('Email'));
      webSubmissionObject.Payload__c             = donationDetails;
      insert webSubmissionObject;    
      
      //Append Web Site Site submission record id to the JSON string to use when inserting Attachment 
      String donationDetailsString = donationDetails.removeEnd('}');
      donationDetailsString = donationDetailsString +',"webSiteSubmissionId":"'+webSubmissionObject.Id+'"}';
      
      //Invoke Platform Event with Web form data JSON to process 
      triggerPlatformEvent(donationDetailsString);
    }
    catch (Exception e) {
      //Exception mechanism to log any exceptions occured during the dml operation
      ct_Logger.logMessage(e, 'Web Site Submission');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }

    return String.valueOf(donorObject.get('paymentType')) == ct_Constants.DIRECT_DEBIT_PAYMENT_METHOD ? ct_WebDataFormHelper.getAuthorisationReference((Id)donorObject.get('authorizationId')) : ct_WebDataFormHelper.getAsperatoReference((Id)donorObject.get('asperatoId'));
  }
  
  /**
  * @description Generate Asperato Payment Record For Web Donation Payment
  * @author Creation Admin | 17/4/2020 
  * @param donationDetails 
  * @return Id 
  **/
  @AuraEnabled
  public static Id createNewAsperatoPayment(String donationDetails){
    asp04__Payment__c thisAsperatoPayment = new asp04__Payment__c();

    try {
      //Deserialize the JSON string to map object & string get object values
      Map<String, Object> donorObject = (Map<String, Object>)JSON.deserializeUntyped(donationDetails);

      //Create Asperato Payment record & return Id to show Asperatp Payment modal
      thisAsperatoPayment = ct_WebDataFormHelper.createAsperatoPayment(donationDetails, String.valueOf(donorObject.get('paymentType') == null ? '' :donorObject.get('paymentType')), donorObject.get('Amount') == null ? 0 : Integer.valueOf(donorObject.get('Amount')), null, System.today());
      upsert thisAsperatoPayment;

      if(thisAsperatoPayment.asp04__Payment_Route_Selected__c == ct_Constants.PAYPAL_PAYMENT_METHOD){
        thisAsperatoPayment.asp04__Success_Endpoint_Long__c = URL.getSalesforceBaseUrl().toExternalForm()+'/s/web-donation?mode=redirect_from_payment&amount='+String.valueOf(donorObject.get('Amount'))+'&asperatoId='+thisAsperatoPayment.Id;
        update thisAsperatoPayment;
      }
    } catch (Exception e) {
      //Exception mechanism to log any exceptions occured during the dml operation
      ct_Logger.logMessage(e, 'Asperato Payment');
      throw new AuraHandledException('Error '+ e.getMessage());
    }
    return thisAsperatoPayment.Id;
  }

  /**
  * @description Get Authorization Record For Direct Debit Payment Setup
  * @author Creation Admin | 17/4/2020 
  * @param donationDetails 
  * @return Id 
  **/

  @AuraEnabled
  public static Id createAuthorizationRecord(String donationDetails){
    //Deserialize the JSON string to map object & string get object values
    Map<String, Object> donorObject = (Map<String, Object>)JSON.deserializeUntyped(donationDetails);

    //Create Authorisation record & return Id to show Asperatp Payment modal
    asp04__Authorisation__c thisAuthorization = ct_WebDataFormHelper.createAuthorizationRecord(donationDetails, String.valueOf(donorObject.get('paymentType') == null ? '' :donorObject.get('paymentType')), String.isBlank(String.valueOf(donorObject.get('authorizationId')))? null : (Id)donorObject.get('authorizationId'));
    try{
    upsert thisAuthorization;
    }
    catch(Exception e){
      //Exception mechanism to log any exceptions occured during the dml operation
      ct_Logger.logMessage(e, 'Asperato Authorisation');
      throw new AuraHandledException('Error '+ e.getMessage());
    }

    return thisAuthorization.Id;
  }



  /**
    * Method to Invoke Platform Event 
    * @param webSubmissionId : Web Submission record Id 
    * Return Value : void
  */
  private static void triggerPlatformEvent(String donationDetails){
    //Create instance of Web Site Submission Event to fire the Platform Event
    Web_Site_Submission_Event__e eventObject 	= new Web_Site_Submission_Event__e();
    eventObject.Web_Site_Submission_Record__c = donationDetails;
    eventObject.Source__c                     = ct_Constants.WEB_SUBMISSION_SOURCE_DONATION;

    //Publish the event after adding web form details
    Database.SaveResult result = EventBus.publish(eventObject);
    if (result.isSuccess()) {
      System.debug('Successfully published event.');
    } else {
      for(Database.Error err : result.getErrors()) {
        System.debug('Error returned: ' +err.getStatusCode() +' - ' +err.getMessage());
      }
    }       
  }
   
  /**
  * @description get metadata record to construct field mappings
  * @author Creation Admin | 17/4/2020 
  * @param formName 
  * @return MetadataWrapper 
  **/
  @AuraEnabled
  public static MetadataWrapper fetchWebformMetadata(String formName){
    //Get field description for the Contact object
    Map<String,Schema.SObjectField> contactSobjectFielsMap = ct_WebDataFormHelper.getSojectFields('Contact');
    Map<String, List<PicklistEntryWrapper>> picklistMap = new  Map<String, List<PicklistEntryWrapper>>();
    MetadataWrapper thisWrapper = new MetadataWrapper();

    //Loop through the field information metadata records & construct wrapper record
    for(Webform_Data__mdt thisDonationdata : ct_WebDataFormHelper.getWebFormMetadataRecords(ct_Constants.WEB_SUBMISSION_SOURCE_DONATION)){
      thisWrapper.donationMetadataRecords.add(thisDonationdata);
      if(thisDonationdata.SObject__c == 'Contact'
      && contactSobjectFielsMap.containskey(thisDonationdata.SObject_Field__c)
      && String.valueOf(contactSobjectFielsMap.get(thisDonationdata.SObject_Field__c).getDescribe().getType())== 'PICKLIST'){
        picklistMap.put(thisDonationdata.SObject_Field__c, constructPicklistEntryWrapper(ct_WebDataFormHelper.getPicklistValues(thisDonationdata.SObject__c, thisDonationdata.SObject_Field__c)));
        
      }
    }

    //Construct consent options
    Set<String> contactConsent    = new Set<String>{'Yes','No'};
    //Fetch GAU units & construct picklist options
    getGAUUnits('donateTo', picklistMap);
    //Generate picklist values for Consent
    generatePicklistValues('contactConsent', picklistMap, contactConsent);
    thisWrapper.picklistFields = picklistMap;

    //Get Advancment Setting Metadata To Get Payment Method & Apsperato URL
    for(ct_Advancement_Setting__mdt thisAdvancementSetting : ct_WebDataFormHelper.getAdvancementSettingRecords()){
      thisWrapper.advancmentSettingRecords.add(thisAdvancementSetting);
    }
    //Add the donation message to wrapper object
    thisWrapper.donationCardSuccessMessage = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.WEB_DONATION_FORM_CARD_SUCCESS_MESSAGE).Notification_Message__c;
    thisWrapper.donationDDSuccessMessage = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.WEB_DONATION_FORM_DD_SUCCESS_MESSAGE).Notification_Message__c;
    thisWrapper.paypalRedirectURL = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.PAYPAL_THANK_YOU_REDIRECT_URL).Paypal_Thank_You_Redirect_URL__c;
    thisWrapper.showPaypalButton = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.PAYPAL_BUTTON_VISIBILITY_CONFIG).Value__c;

    return thisWrapper;
  }

  /**
  * @description construct picklist entries based on metadata
  * @author Creation Admin | 17/4/2020 
  * @param picklistEntryList 
  * @return List<PicklistEntryWrapper> 
  **/
  public static List<PicklistEntryWrapper> constructPicklistEntryWrapper(List<Schema.PicklistEntry> picklistEntryList){

    List<PicklistEntryWrapper> thisPickListWrapperList        = new List<PicklistEntryWrapper>();
    //Loop through the picklist entries to construct picklist options
    for( Schema.PicklistEntry thisPicklistEntry : picklistEntryList){
      thisPickListWrapperList.add(new PicklistEntryWrapper(thisPicklistEntry.getLabel(), thisPicklistEntry.getValue()));
    }
    return thisPickListWrapperList;
  }

  /*
    Metadata Wrapper to bind Metadata records
  */
  public class MetadataWrapper{
    //get set list to store field information metadata record
    @AuraEnabled
    public List<Webform_Data__mdt> donationMetadataRecords {get;set;}
    //get set string to store donation sucess message 
    @AuraEnabled
    public String donationCardSuccessMessage {get;set;}
    //get set string to store donation sucess message 
    @AuraEnabled
    public String donationDDSuccessMessage {get;set;}
    //get set string to store donation Paypal Thank you screen rediect URL
    @AuraEnabled
    public String paypalRedirectURL {get;set;}
    //Get visibility option for Paypal button
    @AuraEnabled
    public String showPaypalButton {get;set;}
    //get set list to store advancement setting metadata records
    @AuraEnabled
    public List<ct_Advancement_Setting__mdt> advancmentSettingRecords {get;set;}
    //get set list to store picklist entries
    @AuraEnabled
    public Map<String, List<PicklistEntryWrapper>> picklistFields {get;set;}
    
    MetadataWrapper(){
      donationMetadataRecords  = new List<Webform_Data__mdt>();
      advancmentSettingRecords = new List<ct_Advancement_Setting__mdt>();
      picklistFields           = new  Map<String, List<PicklistEntryWrapper>>();
    }
  }

  /**
    * Wrapper that contains Donation Object wrapper and Picklist Values
  */
  public class PicklistEntryWrapper{
    //get set string to store lable of picklist entry
    @AuraEnabled
    public String label                  {get;set;}
    //get set string to store value of picklist entry
    @AuraEnabled
    public string value                  {get;set;}
    
    public PicklistEntryWrapper(String entryLabel,String entryValue){
      this.label    = entryLabel;
      this.value    = entryValue;
    }
  }
}