/**
 * @File Name          : ct_AmountChartComponentController.cls
 * @Description        : OpportunityAmountChangesChart component controller. Collects Opportunity Field History and return the
 *                       values to the component to show the chart
 * @Author             : Rajeshkumar  - Creation Technology Solutions
 * @Last Modified By   : Rajeshkumar  - Creation Technology Solutions
 * @Last Modified On   : 7/4/2020, 8:17:35 pm
**/
public with sharing class ct_AmountChartComponentController {
    
    @AuraEnabled
    public List<LineChartVar> myLineChartVarList;
    public static Boolean addOpportunityInitalAmountOnce = true;
    
    public class LineChartVar {
        String label;
        Decimal firstValue;
        
        public LineChartVar(String xAxisParam, Decimal yAxisParam){
            label       = xAxisParam;
            firstValue  = yAxisParam;
        } 
    }
    
    /**
    * @description - Fetch OpportunityFieldHistory Data and form a structure to show in chart
    * @author Rajeshkumar  - Creation Technology Solutions | 3/4/2020 
    * @param opportunityId 
    * @return string - X and Y axis values as JSON 
    **/
    @AuraEnabled
    public static string getLineChartMap(String opportunityId){
        List<LineChartVar> myLineChartVarList = new List<LineChartVar>();
        //Fetch Opportunity Field History
        for(OpportunityFieldHistory thisOpportunityFieldHistory : [SELECT Id, 
                                                                        OpportunityId, CreatedById, CreatedDate, 
                                                                        Field, NewValue, OldValue, Opportunity.CreatedDate
                                                                    FROM OpportunityFieldHistory 
                                                                    WHERE Field = 'Amount' 
                                                                    AND OpportunityId = :opportunityId 
                                                                    ORDER BY CreatedDate]){
            myLineChartVarList.addAll(constructLineChartValue(thisOpportunityFieldHistory.CreatedDate, String.valueOf(thisOpportunityFieldHistory.NewValue), 
                                                                (thisOpportunityFieldHistory.OldValue != null ? String.valueOf(thisOpportunityFieldHistory.OldValue) : String.valueOf(0)), 
                                                                thisOpportunityFieldHistory.Opportunity.CreatedDate)); 
         }
        return JSON.Serialize(myLineChartVarList);
    }

    
    /**
    * @description - Construct the map to show the values in the chart. 
    *                Purpose of this separate method for test class coverage.Because in test class we can't get OpportunityFieldHistory
    * @author Rajeshkumar  - Creation Technology Solutions | 7/4/2020 
    * @param amountChangedTime - OpportunityFieldHistory Record created Date 
    * @param newAmount         - New Opportunity Amount
    * @param oldAmount         - Old Opportunity Amount. Used this to display Starting point in chart.
    * @param opportunityCreatedDate - Opportunity created Date.
    * @return LineChartVar 
    **/
    public static List<LineChartVar> constructLineChartValue(DateTime amountChangedTime, String newAmount, String oldAmount, DateTime opportunityCreatedDate){
        List<LineChartVar> lineChartVarList = new List<LineChartVar>();
        if(String.isNotBlank(newAmount) && addOpportunityInitalAmountOnce){
            addOpportunityInitalAmountOnce = false;
            lineChartVarList.add(new LineChartVar(opportunityCreatedDate.format('dd MMM kk:mm'), Decimal.valueOf(oldAmount)));
        }
        if(String.isNotBlank(newAmount)){
            lineChartVarList.add(new LineChartVar(amountChangedTime.format('dd MMM kk:mm'), Decimal.valueOf(newAmount)));
        }
        return lineChartVarList;
    }
}