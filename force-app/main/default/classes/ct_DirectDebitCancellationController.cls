/**
 * @File Name          : ct_DirectDebitCancellationController.cls
 * @Description        : 
 * @Author             : Sadham Creation
 * @Group              : 
 * @Last Modified By   : Sadham Creation
 * @Last Modified On   : 5/1/2020, 6:04:34 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/30/2020   Sadham Creation     Initial Version
**/
public class ct_DirectDebitCancellationController {

  /**
  * @description - method to fetch Direct debit record and picklist values
  * @return initialWrapper 
  **/
  @AuraEnabled
  public static initialWrapper fetchInitialWrapper(Id directDebitId){
    initialWrapper thisInitialWrapper = new initialWrapper();
    Map<String, List<PicklistEntryWrapper>> picklistMap = new  Map<String, List<PicklistEntryWrapper>>();
    picklistMap.put('DD_Cancellation_Reason__c', constructPicklistEntryWrapper(ct_WebDataFormHelper.getPicklistValues('npe03__Recurring_Donation__c', 'DD_Cancellation_Reason__c')));
    thisInitialWrapper.thisRecurringDonation = getDirectDebitRecord(directDebitId);
    thisInitialWrapper.picklistFields        = picklistMap;
    return thisInitialWrapper;
  }

  /**
  * @description - Method to construct picklist values ha Label, value pair
  * @param List<Schema.PicklistEntry> picklistEntryList - List of picklist values
  * @return List<PicklistEntryWrapper> 
  **/
  public static List<PicklistEntryWrapper> constructPicklistEntryWrapper(List<Schema.PicklistEntry> picklistEntryList){
    List<PicklistEntryWrapper> thisPickListWrapperList        = new List<PicklistEntryWrapper>();
    for( Schema.PicklistEntry thisPicklistEntry : picklistEntryList){
      if(thisPicklistEntry.getValue() != ct_Constants.DD_CANCELLATION_REASON_SYSTEM_CANCELLATION){
        thisPickListWrapperList.add(new PicklistEntryWrapper(thisPicklistEntry.getLabel(), thisPicklistEntry.getValue()));
      }
    }
    return thisPickListWrapperList;
  }
  

  public static npe03__Recurring_Donation__c getDirectDebitRecord(Id directDebitId){
    return [SELECT Id, npe03__Open_Ended_Status__c, Asperato_Authorisation__c, 
              DD_Cancellation_Reason__c, 
              Direct_Debit_Cancellation__c,
              Date_of_Manual_Cancellation__c,
              User_who_Manually_Cancelled__c,
              Direct_Debit_Status__c,
              DD_Cancellation_Description__c
              FROM npe03__Recurring_Donation__c
              WHERE Id =: directDebitId];
  }
  
  @AuraEnabled
  public static Id cancelDirectDebit(String directDebitObject){
    try{
    npe03__Recurring_Donation__c thisDirectDebit = (npe03__Recurring_Donation__c)JSON.deserialize(directDebitObject, npe03__Recurring_Donation__c.class);
    Map<Id, Opportunity> opportunityMap = getRelatedOpportunityRecords(new Set<Id>{thisDirectDebit.Id});

    updateGEMPaymentsToWrittenOff(opportunityMap.keyset());
    update opportunityMap.values();
    //Process Recurring Donation
    thisDirectDebit.Direct_Debit_Cancellation__c    = ct_Constants.DD_CANCELLATION_MANUAL_DD_CANCELLATION;
    thisDirectDebit.Date_of_Manual_Cancellation__c  = System.today();
    thisDirectDebit.User_who_Manually_Cancelled__c  = UserInfo.getName();
    thisDirectDebit.npe03__Open_Ended_Status__c     = ct_Constants.DIRECT_DEBIT_OPEN_ENDED_CLOSED;
    thisDirectDebit.Authorisation_Status__c         = ct_Constants.DIRECT_DEBIT_AUTHORISATION_STATUS_CANCELLED;
    thisDirectDebit.Direct_Debit_Status__c          = ct_Constants.DIRECT_DEBIT_AUTHORISATION_STATUS_CANCELLED;
    update thisDirectDebit;
    cancelAuthorisation(new Set<Id>{thisDirectDebit.Asperato_Authorisation__c});
   
    return thisDirectDebit.Id;
    }
    catch (Exception e) {
      ct_Logger.logMessage(e, 'Direct Debit Cancellation Process');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }

  public static Map<Id, Opportunity> getRelatedOpportunityRecords(set<Id> recurringDonationIds){
    Map<Id, Opportunity> opportunityMap = new Map<Id,Opportunity>();

      for(Opportunity thisOpportunity :[SELECT Id, npe03__Recurring_Donation__c, StageName, 
                                          npsp__Closed_Lost_Reason__c, Lost_Reason__c,
                                          CloseDate
                                          FROM Opportunity
                                          WHERE npe03__Recurring_Donation__c IN: recurringDonationIds
                                          AND StageName != :ct_Constants.OPPORTUNITY_STAGE_CLOSED_WON]){

        thisOpportunity.Lost_Reason__c                = ct_Constants.OPPORTUNITY_LOST_REASON_DIRECT_DEBIT_CANCELLED;
        thisOpportunity.npsp__Closed_Lost_Reason__c   = ct_Constants.OPPORTUNITY_LOST_REASON_DIRECT_DEBIT_CANCELLED;
        thisOpportunity.CloseDate                     = System.today();
        thisOpportunity.StageName                     = ct_Constants.OPPORTUNITY_STAGE_CLOSED_LOST;
        opportunityMap.put(thisOpportunity.Id, thisOpportunity);
      }
    return opportunityMap;                                                              
  }

  public static void updateGEMPaymentsToWrittenOff(set<Id> opportunityIds){
    try{
      List<npe01__OppPayment__c> paymentListToUpdate = new List<npe01__OppPayment__c>();
      for(npe01__OppPayment__c thisGEMPayment : [SELECT Id, npe01__Opportunity__c, npe01__Payment_Date__c,
                                                            npe01__Written_Off__c
                                                            FROM npe01__OppPayment__c
                                                            WHERE npe01__Opportunity__c IN:opportunityIds]){
        thisGEMPayment.npe01__Written_Off__c  = true;
        thisGEMPayment.npe01__Payment_Date__c = System.today();
        paymentListToUpdate.add(thisGEMPayment);
      }
      update paymentListToUpdate;
    }
    catch (Exception e) {
      ct_Logger.logMessage(e, 'Direct Debit Cancellation : Cancel Written Off Process');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }

  public static void cancelAuthorisation(set<Id> authorisationIds){
    try{
      List<asp04__Authorisation__c> authorisationListToUpdate = new List<asp04__Authorisation__c>();
      for(asp04__Authorisation__c thisAuthorisation : [SELECT Id, asp04__Status__c
                                                            FROM asp04__Authorisation__c
                                                            WHERE Id IN:authorisationIds]){
        thisAuthorisation.asp04__Status__c  = ct_Constants.ASPERATO_AUTHORISATION_STATUS_CANCELLED;
        authorisationListToUpdate.add(thisAuthorisation);
      }
      update authorisationListToUpdate;
    }
    catch (Exception e) {
      ct_Logger.logMessage(e, 'Direct Debit Cancellation : Cancel Asperato Authorisation');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }

  public class initialWrapper{
    @AuraEnabled
    public Map<String, List<PicklistEntryWrapper>> picklistFields {get;set;}
    @AuraEnabled
    public npe03__Recurring_Donation__c thisRecurringDonation {get;set;}
  }

  /**
    * Wrapper that contains Donation Object wrapper and Picklist Values
  */
  public class PicklistEntryWrapper{
    @AuraEnabled
    public String label                  {get;set;}
    @AuraEnabled
    public string value                  {get;set;}
    
    public PicklistEntryWrapper(String entryLabel,String entryValue){
      this.label    = entryLabel;
      this.value    = entryValue;
    }
  }
}