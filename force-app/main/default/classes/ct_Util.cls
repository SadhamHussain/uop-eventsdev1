/**
 * @File Name          : ct_Util.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 17/4/2020, 8:17:33 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/12/2020   Creation Admin     Initial Version
**/
public class ct_Util {
  private static Map<Id, RecordType> reverseCache;

  private static Map<String, Map<String, RecordType>> recordTypeByNameCache;

  /**
  * @description getter method to query & map record type values
  * @author Creation Admin | 17/4/2020 
  * @return Map<String, Map<String, RecordType>> 
  **/
  private static Map<String, Map<String, RecordType>> recordTypeCache {
  get {
    if (recordTypeCache == null) {
      recordTypeCache         = new Map<String, Map<String, RecordType>>();
      recordTypeByNameCache   = new Map<String, Map<String, RecordType>>();
      reverseCache            = new Map<Id, RecordType>();
      List<RecordType> allRecTypes = [SELECT Id, DeveloperName, Name, SobjectType,
                                              IsActive, BusinessProcessId, Description,
                                              NamespacePrefix
                                      FROM RecordType];
        
      for (RecordType r : allRecTypes) {
        if(recordTypeCache.get(r.SobjectType) == null){
            
          recordTypeCache.put(r.SobjectType, new Map<String, RecordType>());
        }
		
        if(recordTypeByNameCache.get(r.SobjectType) == null){
            
          recordTypeByNameCache.put(r.SobjectType, new Map<String, RecordType>());
        }
          recordTypeCache.get(r.SobjectType).put(r.DeveloperName, r);
          recordTypeByNameCache.get(r.SobjectType).put(r.Name, r);
          reverseCache.put(r.Id, r);
        }
      }
    return recordTypeCache;
  }
  private set;
  }

  /**
  * @description get record type based on developer name
  * @author Creation Admin | 17/4/2020 
  * @param sObjectName 
  * @param developerName 
  * @return Id 
  **/
  public static Id getRecordTypeIdByDevName(String sObjectName, String developerName) {
      
    RecordType rt = recordTypeCache.get(sObjectName).get(developerName);
    if (rt == null)
        
      throw new RecordTypeCacheException('RecordType \'' + developerName + '\' for \''
                                            + sObjectName + '\' does not exist.');
      return rt.Id;
  }
    
  /**
  * @description get record type developer name by Id
  * @author Creation Admin | 17/4/2020 
  * @param recTypeId 
  * @return String 
  **/
  public static String getRecordTypeDeveloperNameById(Id recTypeId) {

    Map<String, Map<String, RecordType>> unusedCache = recordTypeCache;

    RecordType rt = reverseCache.get(recTypeId);

    if (rt == null)
        throw new RecordTypeCacheException('RecordType with Id \'' + recTypeId + '\' does not exist.');

    return rt.DeveloperName;
}

  /**
  * @description  - method to form the query string from fieldsets
  * @param String objName - SObject Name
  * @param Set<String> fieldList - List of fields API Name
  * @return String - Query String
  **/
  public static String getQueryStringForSObject(String objName, Set<String> fieldList){
    SObjectType objectType = Schema.getGlobalDescribe().get(objName);
    DescribeSObjectResult objectDescribe = objectType.getDescribe();
    string query ='SELECT ';
    for(String thisField :fieldList) {
      query = query +thisField+',';
    }
    query   = query .removeEnd(',');
    query   = query  + ' FROM '+ objectDescribe.getName();
    return query;
  }
    
  /**
   * @description - common method to query org wide email address to be used in all places
   * @return OrgWideEmailAddress[] - array of addresses matching a Constant
   */
  public static OrgWideEmailAddress[] getOrgWideEmail(String addressDisplayName){
  	return [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName=: addressDisplayName];
  }
    
  public class RecordTypeCacheException extends Exception{}
}