/**
 * @File Name          : ct_CourseLookupControllerTest.cls
 * @Description        : Test Class for ct_CourseLookupController
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 7/2/2020, 8:40:16 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/18/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_CourseLookupControllerTest {
  /**
  * @description test course look up
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @isTest
  public static void getCourseTest(){
    Account thisAccount = ct_TestDataFactory.createEducationalAccount('Engineering');
    insert thisAccount;
    // hed__Course__c thisCourse = ct_TestDataFactory.createCourse('Computer Science', thisAccount.Id);
    // insert thisCourse;
    Account accademicAccount                = ct_TestDataFactory.createEducationalAccount('Computer Science');
    accademicAccount.RecordTypeId           = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    accademicAccount.Available_on_Web__c    = true;
    insert accademicAccount;

    Test.startTest();
    List<ct_CourseLookupController.Wrapper> thisWrapperList = ct_CourseLookupController.getCourse('Comp');
    System.assertEquals(1, thisWrapperList.size(), 'Should return 1');
    Test.stopTest();
  }
}