/****************************************************************************
*  bg_ApplicationSchoolAccountBatch_Test 
*
*  Test class for bg_ApplicationSchoolAccount_Batch
* 
*  Author: Ismail Basser - Brightgen
*  Created: 05/11/2019
* 
*  Changes: 
*  VERSION  AUTHOR            DATE              DETAIL		DESCRIPTION
*  
***********************************************************************/
@isTest
public class bg_ApplicationSchoolAccountBatch_Test {
    
    @testSetup 
    static void setup() 
    {
        Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();
        insert schoolAccount;
        
        Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
		insert testApp1;
        
    }
    
    static testmethod void populateSchoolAccountOnApplication() 
    {
        List<Application__c> existingApplications = [SELECT Id, School_College__c, UCAS_School_Code__c FROM Application__c];
        System.assertEquals(null, existingApplications[0].School_College__c);
        
        Test.startTest();
        
        bg_ApplicationSchoolAccount_Batch testBatch = new bg_ApplicationSchoolAccount_Batch();
        Database.executeBatch(testBatch);
        
        Test.stopTest();
        
        List<Account> existingAccount = [SELECT Id, School_College_UCAS_Code__c FROM ACCOUNT];
        List<Application__c> applicationsUpdated = [SELECT Id, School_College__c, UCAS_School_Code__c FROM Application__c];
        System.assertEquals(existingAccount[0].Id, applicationsUpdated[0].School_College__c);
        
        
    }

}