/**
 * @File Name          : ct_AccountTeamRelatedListControllerTest.cls
 * @Description        : Test Class for ct_AccountTeamRelatedListController
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/12/2020, 2:25:10 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/29/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_AccountTeamRelatedListControllerTest {

  /**
  * @description getAccountTeamMemberList Method
  * @return void 
  **/
  @isTest(SeeAllData=true)
  public static void getAccountTeamMemberListTest(){
    User systemAdminUser = ct_TestDataFactory.createUser();
    insert systemAdminUser;
    System.runAs(systemAdminUser){
      Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
      thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE;
      insert thisAccount;

      Contact thisContact   = ct_TestDataFactory.createContact();
      thisContact.AccountId = thisAccount.Id;
      insert thisContact;

      AccountTeamMember thisUOPTeamMember  = ct_TestDataFactory.createAccountTeamMember(thisAccount.Id, systemAdminUser.Id);
      insert thisUOPTeamMember;
      
      Test.startTest();
      List<AccountTeamMember> thisTeamList = ct_AccountTeamRelatedListController.getAccountTeamMemberList(thisContact.Id);
      Test.stopTest();
      System.assertEquals(thisTeamList.size(), 1, 'Since Only One Account Team Member is Inserted');
    }
  }
}