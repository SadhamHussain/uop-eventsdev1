/**
 * @File Name          : AlumniRegistrationController.cls
 * @Description        : Controller class for Alumni Registration form
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 8/5/2020, 7:50:23 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/12/2020   Creation Admin     Initial Version
**/
public with sharing class ct_AlumniRegistrationController {
  /**
  * @description Fetch Metadata records for dynamic field mapping
  * @author Javid Sherif | 17/4/2020 
  * @return MetadataWrapper 
  **/
  @AuraEnabled(cacheable=true)
  public static MetadataWrapper fetchMetadataRecords(){
    //contactSobjectFielsMap - store the details and describe of Sobject fields from Contact object
    Map<String,Schema.SObjectField> contactSobjectFielsMap = ct_WebDataFormHelper.getSojectFields('Contact');
    //picklistMap - used to store all the picklist values in PicklistEntryWrapper for the Alumni Web form
    Map<String, List<PicklistEntryWrapper>> picklistMap = new  Map<String, List<PicklistEntryWrapper>>();
    MetadataWrapper thisWrapper = new MetadataWrapper();
    //For Loop for running all the Web Form metadata records configured for Alumni Webform
    for(Webform_Data__mdt thisAlumniMetadata : ct_WebDataFormHelper.getWebFormMetadataRecords(ct_Constants.WEB_FORMDATA_SOURCE_ALUMNI)){
      thisWrapper.alumniMetadataRecords.add(thisAlumniMetadata);
      if(contactSobjectFielsMap.containskey(thisAlumniMetadata.SObject_Field__c)
      && (String.valueOf(contactSobjectFielsMap.get(thisAlumniMetadata.SObject_Field__c).getDescribe().getType()) == 'PICKLIST'
      || String.valueOf(contactSobjectFielsMap.get(thisAlumniMetadata.SObject_Field__c).getDescribe().getType()) == 'MultiPicklist')){
        picklistMap.put(thisAlumniMetadata.SObject_Field__c, constructPicklistEntryWrapper(ct_WebDataFormHelper.getPicklistValues(thisAlumniMetadata.SObject__c, thisAlumniMetadata.SObject_Field__c)));
      }
    }
    //picklistMap.put('Course__c', getRelatedCourseDetails());
    thisWrapper.picklistFields = picklistMap;
    return thisWrapper;
  }

  /**
  * @description construct picklist values using wrapper class
  * @author Javid Sherif | 17/4/2020 
  * @param picklistEntryList 
  * @return List<PicklistEntryWrapper> 
  **/
  public static List<PicklistEntryWrapper> constructPicklistEntryWrapper(List<Schema.PicklistEntry> picklistEntryList){
    //thisPickListWrapperList - List of picklist values stores for individual picklist field
    List<PicklistEntryWrapper> thisPickListWrapperList        = new List<PicklistEntryWrapper>();
    for( Schema.PicklistEntry thisPicklistEntry : picklistEntryList){
      thisPickListWrapperList.add(new PicklistEntryWrapper(thisPicklistEntry.getLabel(), thisPicklistEntry.getValue()));
    }
    return thisPickListWrapperList;
  }

  /**
  * @description Get course records to map as picklist entries
  * @author Javid Sherif | 17/4/2020 
  * @return List<PicklistEntryWrapper> 
  **/
  public static List<PicklistEntryWrapper> getRelatedCourseDetails(){
    List<PicklistEntryWrapper> coursePicklist = new List<PicklistEntryWrapper>();
    for(hed__Course__c thisCourse : [SELECT Id, Name FROM hed__Course__c ORDER BY Name asc]){
      coursePicklist.add(new PicklistEntryWrapper(thisCourse.Name, String.valueOf(thisCourse.Id)));
    }
    return coursePicklist;
  }

  /**
  * @description - Method to save Contact Details
  * @param String contactDetails - JSON contact Object
  * @return void 
  **/
  @AuraEnabled
  public static void saveContactDetails(String contactDetails){
    try{
      //alumniObject - Deserialze the data captured in the alumni web form from String to Map
      Map<String, Object> alumniObject = (Map<String, Object>)JSON.deserializeUntyped(contactDetails);
      //webSubmissionObject-  Create WebSite Submission entry data for each alumni registration
      Web_Site_Submission__c webSubmissionObject = new Web_Site_Submission__c();
      webSubmissionObject.Name                   = 'Alumni Registration - '+String.valueOf(alumniObject.get('FirstName'))+String.valueOf(alumniObject.get('LastName'));
      webSubmissionObject.Source__c				       = ct_Constants.WEB_SUBMISSION_SOURCE_ALUMNI;
      webSubmissionObject.Payload__c             = contactDetails;
      insert webSubmissionObject;    
     
      String contactDetailsString = contactDetails.removeEnd('}');
      contactDetailsString = contactDetailsString +',"webSiteSubmissionId":"'+webSubmissionObject.Id+'"}';
      
      //Invoke Platform Event
      triggerPlatformEvent(contactDetailsString);
    }
    catch (Exception e) {
      ct_Logger.logMessage(e, 'Web Site Submission');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }
  
  /**
  * @description - Method to publish platform event
  * @param String contactDetails - JSON Contact Object
  * @return void 
  **/
  private static void triggerPlatformEvent(String contactDetails){
    //eventObject- create new Web Site Submission Event record 
    Web_Site_Submission_Event__e eventObject 	 = new Web_Site_Submission_Event__e();
    eventObject.Web_Site_Submission_Record__c = contactDetails;
    eventObject.Source__c                     = ct_Constants.WEB_SUBMISSION_SOURCE_ALUMNI;
    // By publishing the event it will create Alumni record captured from the webform
    Database.SaveResult result = EventBus.publish(eventObject);
    if (result.isSuccess()) {
    } 
    else {
      for(Database.Error err : result.getErrors()) {
      }
    }       
  }

  /**
  * @description - Wrapper class to bind metadata records
  **/
  public class MetadataWrapper{
    //Contains all webform metadata records configured for Alumni form
    @AuraEnabled
    public List<Webform_Data__mdt> alumniMetadataRecords {get;set;}
    //Contains all picklist field and values 
    @AuraEnabled
    public Map<String, List<PicklistEntryWrapper>> picklistFields {get;set;}
    // Stores today Date value to restrict date picker
    @AuraEnabled
    public String  todayDate {get;set;}
    MetadataWrapper(){
      alumniMetadataRecords = new List<Webform_Data__mdt>();
      picklistFields        = new  Map<String, List<PicklistEntryWrapper>>();
      todayDate             = Datetime.now().format('YYYY-MM-dd');
    }
  }
  /**
    * Wrapper that contains Donation Object wrapper and Picklist Values
  */
  public class PicklistEntryWrapper{
    @AuraEnabled
    public String label                  {get;set;}
    @AuraEnabled
    public string value                  {get;set;}
    
    public PicklistEntryWrapper(String entryLabel,String entryValue){
      this.label    = entryLabel;
      this.value    = entryValue;
    }
  }
}