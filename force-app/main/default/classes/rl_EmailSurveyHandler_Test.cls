/*******************************************************************************************************************************
* @name:           rl_EmailSurveyHandler_Test
* @description:    Contains test methods for rl_EmailSurveyHandler
* 
*
* METHODS
* @setup							Inserts Leads and Cases required for the tests
* @testCreateEmailSurveyRecord		Tests creation of an Email_Survey__c record with a rating & related case
* @testUpdateRelatedCaseRating		Tests update of the Case based on the Email Survey data
*
*
* @Created:   27-04-2020 v0.1 - initial version - Rob Liesicke
* @Updated:   30-04-2020 v0.2 - updating test methods to reference methods directly

********************************************************************************************************************************/


/* =========================================================== */   
/*                       TEST SETUP                    	   	   */
/* =========================================================== */ 

@IsTest
public class rl_EmailSurveyHandler_Test {

    @testSetup static void setup() 
    {
        Lead testLead = bg_UnitTestDataFactory.createEnquirer();
        insert testLead;    
        
        Case testCase = bg_UnitTestDataFactory.createEnquiry();
        testCase.Enquirer__c = testLead.Id;
        testCase.Subject = 'testCreateEmailSurveyRecord';
        insert testCase;  
        
		bg_UnitTestDataFactory.insert200LeadsWithCases();        
    }

    
/* =========================================================== */   
/*                 TEST EMAIL SURVEY CREATION                  */
/* =========================================================== */ 
    
    static testMethod void testCreateEmailSurveyRecord()
    {
        // Get case that the Email_Survey__c will be created for
        Case caseToUpdate = 	[ SELECT Id, Rating__c, Subject
                                  FROM Case
                                  WHERE Subject = 'testCreateEmailSurveyRecord'
                                ];
      
        
		// Simulate the variables passed to the controller by the visualforce page        
		rl_EmailSurveyHandler emailSurveyHandler = new rl_EmailSurveyHandler();
        
        emailSurveyHandler.rating = '3';
        emailSurveyHandler.caseId = caseToUpdate.Id;
        
        
        test.startTest();
        
        	// Simulate page controller creating an Email_Survey__c record
            emailSurveyHandler.createEmailSurveyRecord();
        
        test.stopTest();
        
        
        // Check that the Email_Survey__c was successfully created
        Email_Survey__c insertedEmailSurvey = 	[ 	SELECT Id, Rating__c,Related_Case_Id__c
                                  					FROM Email_Survey__c
                                  					WHERE Related_Case_Id__c = :caseToUpdate.Id
                                				];        
        
        System.assertEquals(insertedEmailSurvey.Rating__c, '3');
        
    }

    
/* =========================================================== */   
/*               UPDATE RELATED CASE WITH RATING               */
/* =========================================================== */ 
    
    static testMethod void testUpdateRelatedCaseRating()
    {

        List<Email_Survey__c> emailSurveysToInsert = new List<Email_Survey__c>();        
        List<Id> insertedSurveys = new List<Id>();  
        
        
        // Get list of 200 Cases
        List<Case> casesToUpdate = 	[ SELECT Id, Rating__c, Subject
                                  	  FROM Case
                                      WHERE Subject != 'testCreateEmailSurveyRecord'
                                	];
        
        
        // Insert an Email_Survey__c record for every Case
        for (Case caseToUpdate : casesToUpdate)
        {
            
            Email_Survey__c emailSurvey = new Email_Survey__c();
            
            emailSurvey.Rating__c = '5';
            emailSurvey.Related_Case_Id__c = caseToUpdate.Id;
            
            emailSurveysToInsert.add(emailSurvey);

        }
        
        insert emailSurveysToInsert;
        
        
        // Create list of the inserted Email_Survey__c Ids
        for (Email_Survey__c insertedSurvey : emailSurveysToInsert)
        {        
        	insertedSurveys.add(insertedSurvey.Id);
        }        
        
  
        test.startTest();
        
        	// Simulate Process Builder setting list of Ids in invocable method
            rl_EmailSurveyHandler.updateRelatedCaseRating(insertedSurveys);
        
        test.stopTest();
        
        
        // Boolean to check all Cases are updated with the Rating set on the Email_Survey__c record
        Boolean allCasesUpdated;
        

        List<Case> casesToCheck = 	[ SELECT Id, Rating__c, Subject
                                  	  FROM Case
                                      WHERE Subject != 'testCreateEmailSurveyRecord'
                                	];
        
        // Loop through the Cases
        for( Case caseToCheck : casesToCheck)
        {
            
            // If Master Rating field is populated, set boolean to true
            if( caseToCheck.Rating__c == '5' )
            {
                
                allCasesUpdated = true;
                
            }
            
            // Else set Boolean to false and exit loop 
            else
            {
                
                allCasesUpdated = false;
                
                break;
                
            }    
        }
         
        
        // Ensure all Cases have updated with Rating
        System.assertEquals(allCasesUpdated,true); 
        
    }    
    
}