/**
 * @File Name          : ct_AccountTeamRelatedListController.cls
 * @Description        : Controller Class For ct_AccountTeamRelatedList LWC Component
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/11/2020, 12:53:13 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/28/2020   Creation Admin            Initial Version
**/
public class ct_AccountTeamRelatedListController {

  /** 
  * @description Get Account Team Member Based on Contact's Account Id for displaying in Custom Account Team Related List LWC
  * @param contactId Contact Record Id whose Related Account's Team member needs to be displayed
  * @return List<AccountTeamMember> List of Account Team Member
  **/
  @AuraEnabled
  public static List<AccountTeamMember> getAccountTeamMemberList(Id contactId){
    Contact thisContact = [SELECT Id, AccountId FROM Contact WHERE Id =:contactId];
    List<AccountTeamMember> accountTeamListToReturn = new  List<AccountTeamMember>();
    if(thisContact.AccountId != null){
      accountTeamListToReturn = [SELECT Id, AccountAccessLevel, AccountId, CaseAccessLevel, 
                                  ContactAccessLevel, OpportunityAccessLevel,
                                  PhotoURL, TeamMemberRole, Title, UserId,User.Name 
                                  FROM AccountTeamMember WHERE AccountId =: thisContact.AccountId];
    }
    return accountTeamListToReturn;
  }
}