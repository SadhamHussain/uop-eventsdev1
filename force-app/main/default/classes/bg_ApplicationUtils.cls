/*****************************************************************
* bg_ApplicationUtils
*
* Utility methods relating to Application__c object
* 
* Test Class: bg_ApplicationUtils_Test
*
************** Version history ***********************
* CT, Sadham : 04/06/2020 - Fix for governor limit to look for the account to fill
*                                            School lookup based on recordtype = Educational Institution
*
* CT, Sadham : 26/06/2020 -  Fix for governor limit to look for the account to fill
*                                             School lookup based on recordtype = Educational Institution
*
******************************************************************/

public class bg_ApplicationUtils 
{ 
    /*
    * Main method populating lookups on Application record prior to main processing
    */
    public static void setupNewApplicationsPreProcessing(List<Application__c> newApplications)
    {
        Set<String> ucasSchoolCodes = new Set<String>();
        Set<String> courseCodes = new Set<String>();
        
        for(Application__c app : newApplications)
        {
            if(app.Valid_Application_Data__c == false)
            {
                app.Processing_Status__c = 'Errored';
                app.Error_Description__c = 'The application is missing required data, please correct this within the source system and resend the data';
            }
            else
            {
                if(!String.isBlank(app.UCAS_School_Code__c))
                {
                    ucasSchoolCodes.add(app.UCAS_School_Code__c);
                }
                if(!String.isBlank(app.Course_Code__c))
                {
                    courseCodes.add(app.Course_Code__c);
                }
                if(app.Decision__c == 'I')
                {
                    app.Interview__c = true;
                }
            }
            
        }

        if(!ucasSchoolCodes.isEmpty())
        {
            populateAccountLookupsOnApplications(newApplications, ucasSchoolCodes);
        }

        if(!courseCodes.isEmpty())
        {
            populateCourseLookupOnApplications(newApplications, courseCodes);
        }

    }
    
    /*
    * Identify School Accounts related to Application and populate lookup
    */
    private static void populateAccountLookupsOnApplications(List<Application__c> applications, Set<String> ucasSchoolCodes)
    {
        // CT, Sadham : 04/06/2020 - Adding record type Id filter condition to avoid governor limit
        List<Account> schoolAccounts = [SELECT Id, School_College_UCAS_Code__c FROM Account         
            WHERE 
                RecordTypeId=:ct_Constants.ACCOUNT_RECORDTYPE_EDUCATIONAL_INSTITUTION
                AND School_College_UCAS_Code__c IN :ucasSchoolCodes];
        
        //if(!schoolAccounts.isEmpty())
       // {
            Map<string, Id> schoolIdByUCASCode = bg_CommonUtils.getMapOfIdsByStringField(schoolAccounts, 'School_College_UCAS_Code__c');
            for(Application__c app : applications)
            {
                if(app.Valid_Application_Data__c == true)
                {
                    if(schoolIdByUCASCode.containsKey(app.UCAS_School_Code__c))
                    {
                        app.School_College__c = schoolIdByUCASCode.get(app.UCAS_School_Code__c);
                    }
                    else
                    {
                        app.Error_Description__c = 'No matching school based on UCAS school code. Please review.';
                    }
                } 
            }
       // }
        
        
    }
    
    
    
     /*
    * Identify Course related to Application and populate lookup
    */
    private static void populateCourseLookupOnApplications(List<Application__c> applications, Set<String> courseCodes)
    {   
      //CT, Sadham : 26/06/2020 - Adding record type Id filter condition to avoid governor limit
        List<Account> courseAccounts = [SELECT Id, Academic_Programme_Course_Code__c, SITS_Course_Code__c FROM Account 
                                        WHERE RecordTypeId=: ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM
                                        AND (Academic_Programme_Course_Code__c IN :courseCodes  OR SITS_Course_Code__c IN: courseCodes)];
        Map<string, Id> accountIdsByQuercusCourseCode = bg_CommonUtils.getMapOfIdsByStringField(courseAccounts, 'Academic_Programme_Course_Code__c');
        Map<string, Id> accountIdsBySITSCourseCode = bg_CommonUtils.getMapOfIdsByStringField(courseAccounts, 'SITS_Course_Code__c');

        for(Application__c app: applications)
        {
            if(app.Valid_Application_Data__c)
            {
                Application_Origins__c systemOrigins = Application_Origins__c.getInstance(userInfo.getProfileId());
                string sitsOrigin = systemOrigins.SITS_Origin_String__c;
                string quercusOrigin = systemOrigins.Quercus_Origin_String__c;
                string currentCourse = app.Course__c;
                if(app.Origin__c == quercusOrigin && accountIdsByQuercusCourseCode.containsKey(app.Course_Code__c))
                {
                    app.Course__c = accountIdsByQuercusCourseCode.get(app.Course_Code__c);
                }
                else if(app.Origin__c == sitsOrigin && accountIdsBySITSCourseCode.containsKey(app.Course_Code__c))
                {
                    app.Course__c = accountIdsBySITSCourseCode.get(app.Course_Code__c);
                }
                
                if(app.Course__c == null)
                {
                    app.Processing_Status__c = 'Errored';
                    app.Error_Description__c = 'No Course could be found within salesforce that matches the details on the application';

                }
                else
                {
                    if(currentCourse == null)
                    {
                        app.Processing_Status__c = 'Ready to Process';
                        app.Error_Description__c = '';
                    }
                    else
                    {
                        app.Processing_Status__c = 'Processed';
                        app.Error_Description__c = '';
                    }
                }
            }
            
        }
    }  
    
    public static void processApplicationsAfterUpdate(List<Application__c> newApplications, Map<Id, Application__c> oldApplicationsById)
    {
        Set<Id> applicationsThatRequireLeadConversion = new Set<Id>();
        List<Application__c> applicationsToBeProcessedAfterUpdate = new List<Application__c>();
        
        for(Application__c newApp : newApplications)
        {
            Application__c oldApp = oldApplicationsById.get(newApp.Id);

            if(newApp.Processing_Status__c == 'Ready to Process' && (newApp.Processing_Status__c != oldApp.Processing_Status__c)
               && newApp.Applicant__c == null && newApp.Opportunity__c == null)
            {
                applicationsThatRequireLeadConversion.add(newApp.Id);
            }
            else if(newApp.Processing_Status__c == 'Processed')
            {
                applicationsToBeProcessedAfterUpdate.add(newApp);
            }
        }

        List<Application__c> appsWithOpp = new List<Application__c>();
        if(!applicationsToBeProcessedAfterUpdate.isEmpty())
        {
            bg_ApplicationUtils.processUpdatedApplicationsAfterUpdate(applicationsToBeProcessedAfterUpdate, oldApplicationsById);
            
            for(Application__c updatedApp : applicationsToBeProcessedAfterUpdate)
            {
                if(updatedApp.Opportunity__c != null)
                {
                    appsWithOpp.add(updatedApp);  
                }
            }
        }

        Set<Id> oppIds = new Set<Id>();
        if(!appsWithOpp.isEmpty())
        {
            for(Application__c appToAdd: appsWithOpp)
            {
                oppIds.add(appToAdd.Opportunity__c);
            }
        }

        List<Opportunity> relatedOpps = [SELECT Id, Applicant_Admission_Cycle__c FROM Opportunity WHERE Id IN:oppIds];
        Set<Id> aacIds = new Set<Id>();
        for(Opportunity relatedOpp : relatedOpps)
        {
            if(relatedOpp.Applicant_Admission_Cycle__c != null )
            {
                aacIds.add(relatedOpp.Applicant_Admission_Cycle__c);
            }
        }

        if(!aacIds.isEmpty())
        {

            bg_ApplicantAdmissionCycleUtils.setFlagsOnAacFromOpps(aacIds);
        }
        
        if(!applicationsThatRequireLeadConversion.isEmpty() && !isQueueableJobCurrentlyRunning())
        {
            System.enqueueJob(new bg_ApplicationProcessingQueuable(applicationsThatRequireLeadConversion));
        }
    }
    
    /*
    *
    * Main method for processing applications - to segment applications that can be processed for conversion and applications that
    * are linked to multiple potential enquirers
    *
    */
    public static void processApplications(List<Application__c> applicationsToBeProcessed)
    {
        List<Lead> tempEnquirers = bg_EnquirerUtils.createListOfTempEnquirers(applicationsToBeProcessed);
        Map<Id, Set<Lead>> enquirersByAppId = bg_FindDuplicateRecords.checkForApplicationsRelatedToZeroToManyEnquirers(tempEnquirers);
        
        // Enquirers to be Converted
        Map<Id, Lead> enquirersToBeConvertedByAppId = createMapOfEnquirersToBeProcessed(enquirersByAppId);

        // Applications with Multiple Enquirers
        Map<Id, Set<Lead>> multipleDuplicateEnquirersByAppId = bg_FindDuplicateRecords.checkForApplicationsRelatedToManyEnquirers(tempEnquirers);

        if(!enquirersToBeConvertedByAppId.isEmpty())
        {
            updateApplicationsRelatedToExistingContactPriorToConversion(enquirersToBeConvertedByAppId, applicationsToBeProcessed);
            Map<Id, Lead> updatedEnquirersByAppId = bg_EnquirerUtils.updateMatchingEnquirerWithApplicantInfo(enquirersToBeConvertedByAppId);

            if(!updatedEnquirersByAppId.isEmpty())
            {
                List<Lead> upsertedEnquirersToBeConverted = bg_EnquirerUtils.upsertEnquirersToBeConverted(updatedEnquirersByAppId);

                // identify if duplicate applicants are in list of leads, using student applicant Id - categorise them using student app Id
                Map<String, List<Lead>> enquirersByStudentAppId = bg_EnquirerUtils.createMapOfEnquirersByStudentAppId(upsertedEnquirersToBeConverted);

                upsertedEnquirersToBeConverted = new List<Lead>();
                Map<String, List<Lead>> outstandingEnquirersToConvertByStudentAppId = new Map<String, List<Lead>>();
                
                for(String studentAppId : enquirersByStudentAppId.keySet())
                {
                    List<Lead> enquirers = enquirersByStudentAppId.get(studentAppId);

                    if(enquirers.size() == 1)
                    {
                        upsertedEnquirersToBeConverted.add(enquirers[0]);
                    }
                    else 
                    {
                        List<Lead> relatedEnquirers = new List<Lead>();

                        for(Integer i = 0; i < enquirers.size(); i++)
                        {
                            if(i == 0)
                            {
                                upsertedEnquirersToBeConverted.add(enquirers[i]);
                            }
                            else 
                            {
                               relatedEnquirers.add(enquirers[i]);
                            }
                        }

                        outstandingEnquirersToConvertByStudentAppId.put(studentAppId, relatedEnquirers);
                    }
                }
                
                List<Lead> influencersToBeConverted = bg_EnquirerUtils.createListOfInfluencerToBeConverted(upsertedEnquirersToBeConverted);
                
                if(!influencersToBeConverted.isEmpty())
                {
                    upsertedEnquirersToBeConverted.addAll(influencersToBeConverted);
                }

                bg_EnquirerUtils.convertEnquirers(upsertedEnquirersToBeConverted, outstandingEnquirersToConvertByStudentAppId);
            }
        }

        if(!multipleDuplicateEnquirersByAppId.isEmpty())
        {
            Map<Id, String> errorDescriptionByAppId = bg_AppPotentialEnquirerDupesUtils.insertApplicationPotentialEnquirerDuplicateRecords(multipleDuplicateEnquirersByAppId);
        
            // no longer want to error the records
        //      updateFailedApplicationRecordsPostProcessing(errorDescriptionByAppId);
        }
    }
    
    

    /*
    *
    * This updates applications and it's associated Contact where the Applicant already exists in the system (based on a matching Student Applicant Id)
    *
    */  
    private static void updateApplicationsRelatedToExistingContactPriorToConversion(Map<Id, Lead> enquirersToBeConvertedByAppId, List<Application__c> applicationsToBeProcessed)
    {
        Map<Id, Lead> updatedEnquirersByAppId = new Map<Id, Lead>();
        List<Application__c> appsToBeUpdate = new List<Application__c>();
        Map<Id, Contact> contactToBeUpdate = new Map<Id, Contact>();

        Set<String> studentAppIds = new Set<String>();

        for(Application__c app : applicationsToBeProcessed)
        {
            studentAppIds.add(app.Student_Id__c);
        }

        List<Contact> existingContactsRelatedToApplication = [SELECT Id, Student_Applicant_Id__c FROM Contact WHERE Student_Applicant_Id__c IN :studentAppIds];
        Map<string, Id> contactIdsByApplicantId = bg_CommonUtils.getMapOfIdsByStringField(existingContactsRelatedToApplication, 'Student_Applicant_Id__c');

        if(!existingContactsRelatedToApplication.isEmpty())
        {
            for(Application__c appWithContact : applicationsToBeProcessed)
            {
                Contact updatedContact = new Contact();
                Application__c updatedApp = new Application__c();
                if(contactIdsByApplicantId.containsKey(appWithContact.Student_Id__c))
                {
                    Id matchingContactId = contactIdsByApplicantId.get(appWithContact.Student_Id__c);
                    updatedApp.Id = appWithContact.Id;
                    updatedApp.Applicant__c = matchingContactId;
                    appsToBeUpdate.add(updatedApp);
                    
                    updatedContact.Id = matchingContactId;
                    updatedContact.Most_Recent_Application__c = appWithContact.Id;
                    contactToBeUpdate.put(updatedContact.Id, updatedContact);
                }
                
            }
        }

        if(!appsToBeUpdate.isEmpty())
        {
            Database.SaveResult[] updatedApps = Database.update(appsToBeUpdate, false);
        }

        if(!contactToBeUpdate.isEmpty())
        {
            Database.SaveResult[] updateContacts = Database.update(contactToBeUpdate.values(), false);
        }  
    }

    /*
    *
    * Creates Map for Applications that have a single Enquirer match to be processed for conversion
    *
    */
    private static Map<Id, Lead> createMapOfEnquirersToBeProcessed(Map<Id, Set<Lead>> enquirersByAppId)
    {
        Map<Id, Lead> enquirersToBeConvertedByAppId = new Map<Id, Lead>();

        for(Id appId : enquirersByAppId.keySet())
        {
            Set<Lead> enquirerSet = enquirersByAppId.get(appId);

            if(enquirerSet.size() == 1)
            {
                List<Lead> enquirerList = new List<Lead>();
                enquirerList.addAll(enquirerSet);
                enquirersToBeConvertedByAppId.put(appId, enquirerList[0]);
            }
        }

        return enquirersToBeConvertedByAppId;
    }

    /*
    *
    * Creates Map for Applications that have multiple potential Enquirers associated to it
    *
    */
    private static Map<Id, Set<Lead>> createMapOfMultipleEnquirersLinkedToSingleApp(Map<Id, Set<Lead>> enquirersByAppId, Map<Id, Lead> enquirersToBeConvertedByAppId)
    {
        Map<Id, Set<Lead>> multipleDuplicateEnquirersByAppId = enquirersByAppId;

        for(Id appId : enquirersByAppId.keySet())
        {
            if(enquirersToBeConvertedByAppId.containsKey(appId))
            {
                multipleDuplicateEnquirersByAppId.remove(appId);
            }
        }
        
        return multipleDuplicateEnquirersByAppId;
    }
    
    /*
    * Setup list of successful applications to be updated
    */
    public static void updateSuccessfulApplicationRecordsPostProcessing(Map<Id, Id> leadIdByAppId, Map<Id, Id> contactIdByAppId, Map<Id, Id> opportunityIdByAppId)
    {   
        List<Application__c> applicationsToUpdate = createListOfSuccessfulApplicationsToBeProcessed(leadIdByAppId, contactIdByAppId, opportunityIdByAppId);
        
        if(!applicationsToUpdate.isEmpty())
        {
            Set<Id> successfulApplicationIds = new Set<Id>();

            Database.SaveResult[] updateApplicationRecords = Database.update(applicationsToUpdate, false);

            for(Integer i = 0; i < updateApplicationRecords.size(); i++) 
            {
                if(updateApplicationRecords[i].isSuccess())
                {
                    successfulApplicationIds.add(updateApplicationRecords[i].getId());
                }
            }

            if(!successfulApplicationIds.isEmpty())
            {
                List<Application__c> newApplications = [SELECT Id, First_Name__c, Course__c, Last_Name__c, Applicant__c, Opportunity__c, Start_Date__c, 
                                                        Qualification_1__c, Qualification_2__c, Qualification_3__c, Qualification_4__c, Qualification_5__c, 
                                                        Qualification_6__c, Qualification_7__c, Qualification_8__c,
                                                        Application_Stage__c, Type__c, Date_of_Birth__c, Student_Gender__c,  Mode_Of_Attendance__c,
                                                        Member_Of_Unconditional_Offer_Scheme__c, Address_Type__c, Address_1__c, Address_2__c,
                                                        Address_3__c, Address_4__c, Address_5__c, Postal_Code__c, Origin__c
                                                        FROM Application__c WHERE Id IN :successfulApplicationIds AND Processing_Status__c  = 'Processed'];

                populateContactAndOpportunityFieldsPostApplicationCreation(newApplications);
            }
        }
    }
    
    public static void processUpdatedApplicationsBeforeUpdate(List<Application__c> newApplications, Map<Id,Application__c> oldApplicationsById)
    {
        Set<String> locationFacultySchoolCodes = new Set<String>();
        Set<String> courseCodes = new Set<String>();
        List<Application__c> applicationsWithChangedPersonId = new List<Application__c>();

        for(Application__c newApp : newApplications)
        {   
            Application__c oldApp = oldApplicationsById.get(newApp.Id);
            
            if(newApp.Decision__c != oldApp.Decision__c && newApp.Decision__c == 'i')
            {
                newApp.Interview__c = true;
            }

            if(newApp.UCAS_School_Code__c != oldApp.UCAS_School_Code__c)
            {
                locationFacultySchoolCodes.add(newApp.UCAS_School_Code__c);
            }
            /*if(newApp.Course_Code__c != oldApp.Course_Code__c || newApp.Entry_Year__c != oldApp.Entry_Year__c 
               || newApp.Entry_Month__c != oldApp.Entry_Month__c || newApp.Course__c == null) */
               
            //Added !String.isBlank(newApp.Course_Code__c) By CT, Sadham to prevent non selective query
            if((newApp.Course_Code__c != oldApp.Course_Code__c || newApp.Start_Date__c != oldApp.Start_Date__c 
               || newApp.Course__c == null) 
               && !String.isBlank(newApp.Course_Code__c))
            {               
                courseCodes.add(newApp.Course_Code__c);
            }

            if((newApp.Student_Id__c != oldApp.Student_Id__c) && String.isNotBlank(newApp.Applicant__c) && String.isNotBlank(newApp.Opportunity__c))
            {
                applicationsWithChangedPersonId.add(newApp);
            }
        }

        if(!locationFacultySchoolCodes.isEmpty())
        {
            populateAccountLookupsOnApplications(newApplications, locationFacultySchoolCodes);
        }

        if(!courseCodes.isEmpty())
        {
            populateCourseLookupOnApplications(newApplications, courseCodes);
        }
    }
    
    /*
    * Main After Update method for handling updated Applications
    */
    public static void processUpdatedApplicationsAfterUpdate(List<Application__c> newApplications, Map<Id,Application__c> oldApplicationsById)
    {
        bg_OpportunityUtils.updateOpportunityLinkedToUpdatedApplication(newApplications, oldApplicationsById);
        bg_ContactUtils.updateContactLinkedToUpdatedApplication(newApplications, oldApplicationsById);
        
        Map<Id, Opportunity> relatedOpportunitiesById = new Map<Id, Opportunity>([SELECT Id, CloseDate FROM Opportunity WHERE Application__c IN: oldApplicationsById.keyset()]);

        bg_ProgramEnrollmentUtils.createProgramEnrollmentPostApplicationEnrollment(newApplications, oldApplicationsById, relatedOpportunitiesById);
    }
    
    
    /*
    * Main method for setting up data required to update Contact and Opportunity fields post Application creation
    */
    public static void populateContactAndOpportunityFieldsPostApplicationCreation(List<Application__c> newApplications)
    {
        Set<Id> courseIds = new Set<Id>();
        
        for(Application__c newApplication : newApplications)
        {
            courseIds.add(newApplication.Course__c);
        }
        
        Map<Id, Account> coursesById = new Map<Id, Account>([SELECT Id, Academic_Programme_Display_Course_Name__c, Course_Instance_Start_Date__c
                                                                FROM Account WHERE Id IN: courseIds]);
        
        updateOpportunitiesPostApplicationCreation(newApplications, coursesById);
        updateContactsPostApplicationCreation(newApplications);
    }
    
    /*
    * To update the newly created Contacts with their Birthdate
    */
    private static void updateContactsPostApplicationCreation(List<Application__c> newApplications)
    {
        Map<Id, Contact> newContactsToUpdateById = new Map<Id, Contact>();

        for(Application__c newApp : newApplications)
        {   
            if(String.isNotBlank(newApp.Applicant__c) && (String.isNotBlank(String.valueOf(newApp.Date_of_Birth__c))))
            {
                Contact newContact = bg_ContactUtils.updateContactKeyFields(newApp);
                newContactsToUpdateById.put(newApp.Applicant__c, newContact);
            }
        }

        if(!newContactsToUpdateById.isEmpty())
        {
            Database.SaveResult[] updateContacts = Database.update(newContactsToUpdateById.values(), false); 

            Map<Id, String> errorMessageByAppId = new Map<Id, String>();
            
            for(Integer i = 0; i < updateContacts.size(); i++) 
            {
                Id appId = newContactsToUpdateById.values()[i].Most_Recent_Application__c;

                if(!updateContacts[i].isSuccess())
                {
                    String errorMessage = '';

                    for(Database.Error error : updateContacts[i].getErrors()) 
                    {
                        if(String.isEmpty(errorMessage)) 
                        {
                            errorMessage = error.getMessage();
                        } 
                    }

                    errorMessageByAppId.put(appId, errorMessage);
                }
            }

            if(!errorMessageByAppId.isEmpty())
            {   
                bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
            }
        }
    }
    
    /*
    * To update the newly created Opportunities post Application creation - update to standard fields
    */
    private static void updateOpportunitiesPostApplicationCreation(List<Application__c> newApplications, Map<Id, Account> coursesById)
    {
        List<Opportunity> newOpportunitiesToUpdate = new List<Opportunity>();
        
        for(Application__c newApp : newApplications)
        {
            string courseDisplayName;
            string courseStartDateString;
            Account courseAccount = coursesById.get(newApp.Course__c);
            if(courseAccount != null)
            {
                courseDisplayName = courseAccount.Academic_Programme_Display_Course_Name__c;
                if(courseAccount.Course_Instance_Start_Date__c != null)
                {
                    courseStartDateString = courseAccount.Course_Instance_Start_Date__c.format();
                }
                else
                {
                    courseStartDateString = newApp.Start_Date__c.format();
                }
                
            }
            if(String.isNotBlank(newApp.Opportunity__c))
            {
                Opportunity newOpp = bg_OpportunityUtils.updateNewOpportunity(newApp, courseDisplayName, courseStartDateString);
                newOpportunitiesToUpdate.add(newOpp);
            }
        }

        if(!newOpportunitiesToUpdate.isEmpty())
        {
            Database.SaveResult[] updateOpps = Database.update(newOpportunitiesToUpdate, false);

            Map<Id, String> errorMessageByAppId = new Map<Id, String>();
            
            for(Integer i = 0; i < updateOpps.size(); i++) 
            {
                Id appId = newOpportunitiesToUpdate[i].Application__c;

                if(!updateOpps[i].isSuccess())
                {
                    String errorMessage = '';

                    for(Database.Error error : updateOpps[i].getErrors()) 
                    {
                        if(String.isEmpty(errorMessage)) 
                        {
                            errorMessage = error.getMessage();
                        } 
                    }

                    errorMessageByAppId.put(appId, errorMessage);
                }
            }

            if(!errorMessageByAppId.isEmpty())
            {   
                bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
            } 
        }
    }


    /*
    *
    * Setup list of unsuccessful applications to be updated
    *
    */
    public static void updateFailedApplicationRecordsPostProcessing(Map<Id, String> errorDescriptionByAppId)
    {   
        List<Application__c> applicationsToUpdate = createListOfFailedApplicationRecordsToBeProcessed(errorDescriptionByAppId);

        if(!applicationsToUpdate.isEmpty())
        {
            updateApplicationRecordsPostProcessing(applicationsToUpdate);
        }
    }

    /*
    *
    * Update Applications that have been processed (both successful and unsuccessful records)
    *
    */
    private static void updateApplicationRecordsPostProcessing(List<Application__c> applicationsToUpdate)
    {   
        if(!applicationsToUpdate.isEmpty())
        {
            Database.SaveResult[] updateApplicationRecords = Database.update(applicationsToUpdate, false);
        }
    }

    /*
    *
    * Setup successful application records with lookups and mark as Processed
    *
    */
    private static List<Application__c> createListOfSuccessfulApplicationsToBeProcessed(Map<Id, Id> enquirerIdByAppId, Map<Id, Id> contactIdByAppId, Map<Id, Id> opportunityIdByAppId)
    {
        List<Application__c> applicationsToUpdate = new List<Application__c>();

        if(!enquirerIdByAppId.isEmpty())
        {
            for(Id appId : enquirerIdByAppId.keySet())
            {
                Id enquirerId = enquirerIdByAppId.get(appId);
                Id contactId = contactIdByAppId.get(appId);
                Id opportunityId = opportunityIdByAppId.get(appId);
                
                Application__c app = new Application__c();
                app.Id = appId;
                app.Enquirer__c = enquirerId;
                app.Applicant__c = contactId;
                app.Opportunity__c = opportunityId;
                app.Processing_Status__c = 'Processed';
                app.Error_Description__c = '';
                applicationsToUpdate.add(app);
            }
        }

        return applicationsToUpdate;
    }

    /*
    *
    * Setup unsuccessful application records with Error Description and mark as Errored 
    *
    */
    private static List<Application__c> createListOfFailedApplicationRecordsToBeProcessed(Map<Id, String> errorDescriptionByAppId)
    {
        List<Application__c> applicationsToUpdate = new List<Application__c>();

        if(!errorDescriptionByAppId.isEmpty())
        {
            for(Id appId : errorDescriptionByAppId.keySet())
            {
                String errorDescription = errorDescriptionByAppId.get(appId);

                Application__c applicationToBeUpdated = updateApplicationProcessingStatus(appId, 'Errored', errorDescription);
                applicationsToUpdate.add(applicationToBeUpdated);
            }
        }
        return applicationsToUpdate;
    }

    
    /*
    *
    * Setup individual application records that have been unsuccessful with Error Description and mark as Errored 
    *
    */
    private static Application__c updateApplicationProcessingStatus(Id appId, String processingStatus, String processingErrorDescription)
    {
        Application__c applicationToBeUpdated = new Application__c();

        applicationToBeUpdated.Id = appId;
        applicationToBeUpdated.Processing_Status__c = processingStatus;
        
        if(String.isNotBlank(processingErrorDescription)){
            applicationToBeUpdated.Error_Description__c = processingErrorDescription;
        }
        
        return applicationToBeUpdated;
    }
    
    public static Boolean isQueueableJobCurrentlyRunning()
    {
        Integer enqueuedJobs = [SELECT COUNT() FROM AsyncApexJob 
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE Name = 'bg_ApplicationProcessingQueuable') 
                                AND Status IN ('Processing','Preparing','Queued')];
        
        if(enqueuedJobs > 0)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
}