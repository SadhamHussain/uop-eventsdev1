/*
 * The following class is overrides the standard functionality when a conversion is done via Duplicate Check
 * 
 * Convert Plugin documentation - https://suptest.duplicatecheck.com/development-guide/apex-plugin-typed#convertPlugin
 *
 * LeadConvert documentation - https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_dml_convertLead.htm#apex_dml_convertLead
 *
 * LeadConvertResult documenation - https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_class_database_leadconvertresult.htm#apex_class_database_leadconvertresult
 *
 * Created By: Rob Liesicke (UoP)
 * Created On: 28th November 2019
 *
 **************** Version history ***********************
 *
 * 17/12/19		RL		Updated comments throughout code
 * 01/06/20		CT, Sadham : Modification done to transfer all the Latest Enquiry Information 
 *                         associated to the converted Lead to Converted contact
 * 24/06/20		RL		Updated email fields processed on Contact
 * 
 */



global class rl_ConvertPlugin implements dupcheck.dc3Plugin.InterfaceConvert {
    
    
  // Custom code that runs before the conversion takes place
  global void beforeConvert(Database.LeadConvert leadConvertData) {
      // No custom code
      return;
  }
  
  // Custom code that runs if the conversion fails
  global void convertFailed(Database.LeadConvert leadConvertData, dupcheck.dc3Exception.ConvertException exceptionData) {
      // No custom code
      return;
  }
  
  /*
   * Custom code that runs after the lead is converted
   *
   * The following code runs when Duplicate Check converts a lead into a matching contact.
   *
   * It takes all the phone numbers and email addresses from both the lead and contact, deduplicates them, 
   * and then ensures as many as possible remain on the contact.
   *
   * It also updates all the objects related to the lead, to be associated with the contact instead. This includes 
   * Event Registrations, Cases, Individual Email Results, and Campaign Members.
   *
   * For Campaign Members it's not possible to update the Contact ID in an update call, so the record is cloned 
   * (preserving time stamps) and the original is deleted.
   *
   */ 
   
  global void afterConvert(Database.LeadConvertResult leadConvertResult, Task taskData) {
      
      // Get the ID of the converted lead
      String leadId = leadConvertResult.getLeadID(); 
      
      // Get the ID of the contact (new or matched existing one)
      String contactId = leadConvertResult.getContactID();
      
      
      // Use the ID of the converted lead to get all the contact details for the lead, and store as Sobject
      Lead leadToConvert = [ SELECT Id, FirstName, LastName, Email, Email_Alternative__c, MobilePhone, 
                                    Mobile_Alternative__c, Phone, Phone_Alternative__c, 
                                    Event_Mobile__c, Event_Email__c
                             FROM Lead 
                             WHERE Id = :leadId ];
      
      // Use the ID of the contact to get all the contact details for the contact, and store as Sobject                  
      Contact contactToUpdate = [ SELECT Id, FirstName, LastName, hed__AlternateEmail__c, MobilePhone, 
                                         Mobile_Alternative__c, Phone, Phone_Alternative__c, 
                                         Event_Mobile__c, Event_Email__c,	hed__Preferred_Email__c
                                  FROM Contact
                                  WHERE Id = :contactId ];      
      
  
  // If preffered email type is not already set, default it to Alternate to avoid errors
  String preferredEmailType = contactToUpdate.hed__Preferred_Email__c;
      
      if (preferredEmailType == '' || preferredEmailType == null)
      {
          preferredEmailType = 'Alternate';
      }
      
      // Create a list to store email addressess, and another to store phone numbers
      List<String> emailAddresses = new List<String>();
      List<String> phoneNumbers = new List<String>();    
      
      // Add all email addressess from lead and contact to list of emails (order of these is intentional, to priortise fields)
      emailAddresses.add(leadToConvert.Email);  
      emailAddresses.add(contactToUpdate.hed__AlternateEmail__c);     		
      emailAddresses.add(leadToConvert.Email_Alternative__c);


              
      // ..and add all phone numbers from lead and contact to list of phone numbers
      phoneNumbers.add(contactToUpdate.Phone);
      phoneNumbers.add(contactToUpdate.MobilePhone);
      phoneNumbers.add(contactToUpdate.Phone_Alternative__c);
      phoneNumbers.add(contactToUpdate.Mobile_Alternative__c);     
      phoneNumbers.add(leadToConvert.Phone);
      phoneNumbers.add(leadToConvert.MobilePhone);
      phoneNumbers.add(leadToConvert.Phone_Alternative__c);
      phoneNumbers.add(leadToConvert.Mobile_Alternative__c); 
      
              
      /*
       * The following code in this method processes the email addresses
       * 
       * It takes the list of all email addresses associated to the lead and contact
       * and removes all duplicates and nulls. It then stores the first email addresses
       * in the hed__AlternateEmail__c field.
       */
      
      // Dedupe list of email addresses with use of Set
      Set<String> tempSet = new Set<String>();
      List<String> uniqueEmailAddresses = new List<String>();
      
      // Whilst looping through, ensure that null values are not added to the set as we don't need those..
      for (String email : emailAddresses) 
      {
          if (email != null && email !='' && tempSet.add(email)) 
          {
              uniqueEmailAddresses.add(email);
          }
      }
      
      // Calculate and store the number of unique email addresses in the deduped list
      Integer emailCount = uniqueEmailAddresses.size();
      
      // Create variables to store email addresses that will be kept
      String email;
      
      // If list size of email addresses is one, set the email variable with that email address
      if (emailCount >= 1) 
      {
          email = uniqueEmailAddresses.get(0);
      }
      
      
      /*
       * The following code in this method processes the phone and mobile numbers
       *
       * It takes the list of all phone numbers associated to the lead and contact
       * and processes each one to remove unwanted characters, and also format numbers
       * that look like mobile numbers into a standard format.
       *
       * It then removes the duplicate numbers from the list.
       *
       * It then sorts the unique numbers into 2 lists, one for (UK) mobile numbers, and another
       * for everything else. 
       * 
       * Finally up to 4 phone numbers are kept, with mobile numbers being prioritised.
       */
      
      // List to store list of unique phone numbers and another for unique mobile numbers
      List<String> uniquePhoneNumbers = new List<String>();
      List<String> uniqueMobileNumbers = new List<String>();
      
      // Loop through every phone number
      for (String phoneNo : phoneNumbers)
      {
          
          if (phoneNo != null)
          {
              
              // Remove spaces, brackets, dashes, commas, full stops, replace o's with zeros
              phoneNo = phoneNo.replaceAll( '\\s+', '');
              phoneNo = phoneNo.replaceAll( 'o', '0');
              phoneNo = phoneNo.replaceAll( 'O', '0');
              phoneNo = phoneNo.replaceAll( '\\(', '');
              phoneNo = phoneNo.replaceAll( '\\)', '');
              phoneNo = phoneNo.replaceAll( '\\-', '');
              phoneNo = phoneNo.replaceAll( '\\,', '');
              phoneNo = phoneNo.replaceAll( '\\.', '');
              
              // If number starts with 00, replace start with + and remove first 2 digits 
              if (phoneNo.left(2) == '00') { phoneNo = '+' + phoneNo.right( (phoneNo.length() - 2) ); }
              
              // If number starts with 7, replace start with +44 
              // Business descision has been made to risk this change despite number potentially not being UK
              if (phoneNo.left(1) == '7') { phoneNo = '+44' + phoneNo; }
              
              // If number starts with 07, replace start with +44 and remove first digit
              if (phoneNo.left(2) == '07') { phoneNo = '+44' + phoneNo.right( (phoneNo.length() - 1) ); }            
              
              // If number starts with 447, prefix number with +
              if (phoneNo.left(3) == '447') { phoneNo = '+' + phoneNo; }   
              
              // If number starts with +4407, replace start with +447
              if( phoneNo.left(5) == '+4407') { phoneNo = '+44' + phoneNo.right( (phoneNo.length() - 4) ); }
              
              
              // Add processed phone number to unique list of numbers, using set to ensure duplicates are not added
              if (phoneNo != null && tempSet.add(phoneNo)) 
              {
                  // If phone number looks like a mobile number, add to mobile list
                  if (phoneNo.left(4) == '+447')
                  {
                      uniqueMobileNumbers.add(phoneNo);
                  } 
                  
                  // Add any other number to phone number list
                  else 
                  { 
                      uniquePhoneNumbers.add(phoneNo); 
                  }
              }            
          }
      }
      
      
      // Create variables to store phone numbers that will be kept
      String phone;
      String phoneAlt;
      String mobile;
      String mobileAlt;
      
      // Calculate and store the number of unique phone numbers in the deduped list
      Integer phoneCount = uniquePhoneNumbers.size();
      
      // Calculate and store the number of unique mobile numbers in the deduped list
      Integer mobileCount = uniqueMobileNumbers.size(); 
      
      // Mobile numbers have priority over other numbers
      // If number of mobile numbers is 1, store it in mobile variable
      if (mobileCount == 1)
      {
          mobile = uniqueMobileNumbers.get(0); 
      }       
      
      // If number of mobile numbers is 2, populate them in the mobile and mobile alternative fields
      if (mobileCount == 2)
      {
          mobile = uniqueMobileNumbers.get(0); 
          mobileAlt = uniqueMobileNumbers.get(1); 
      }
      
      // If number of mobile numbers is 3, put third one in the phone field
      if (mobileCount == 3)
      {
          mobile = uniqueMobileNumbers.get(0); 
          mobileAlt = uniqueMobileNumbers.get(1); 
          phone = uniqueMobileNumbers.get(2);             
      }
      
      // If number of mobile numbers is 4 or more, put fourth one in the phone alternative field        
      if (mobileCount >= 4)
      {
          mobile = uniqueMobileNumbers.get(0); 
          mobileAlt = uniqueMobileNumbers.get(1); 
          phone = uniqueMobileNumbers.get(2);  
          phoneAlt = uniqueMobileNumbers.get(3);                       
      }
      
      // If number of other phone numbers is 1..
      if (phoneCount == 1)
      {
          // ..if phone is still null, put it there
          if (phone == null)
          {
              phone = uniquePhoneNumbers.get(0);
          }
          
          // .. else if phoneAlt is still null, put it there
          else if (phoneAlt == null)
          {
              phoneAlt = uniquePhoneNumbers.get(0); 
          } 
      }  
      
      // If number of other phone numbers is 2 or more
      if (phoneCount >= 2)
      {
          // ..if phone is still null, put it there
          if (phone == null)
          {
              phone = uniquePhoneNumbers.get(0);
              phoneAlt = uniquePhoneNumbers.get(1); 
          }
          
          // .. else if phoneAlt is still null, put it there
          else if (phoneAlt == null)
          {
              phoneAlt = uniquePhoneNumbers.get(0); 
          } 
      }                    
      
      
      /*
       * The following code updates the contact with the emails and phone numbers 
       * that have been processed in the code above i.e. the ones we want to keep
       */
      
      contactToUpdate.hed__AlternateEmail__c = email;     
      contactToUpdate.Phone = phone;
      contactToUpdate.MobilePhone = mobile;
      contactToUpdate.Phone_Alternative__c = phoneAlt;
      contactToUpdate.Mobile_Alternative__c = mobileAlt;     
            
      // If lead has an event email address, set it on the contact event email address field
      if (leadToConvert.Event_Email__c != null)
      {
          contactToUpdate.Event_Email__c = leadToConvert.Event_Email__c;
      }    

      // If lead has an event mobile, set it on the contact event mobile field
      if (leadToConvert.Event_Mobile__c != null)
      {
          contactToUpdate.Event_Mobile__c = leadToConvert.Event_Mobile__c;
      }           
      
      // Update the converted contact in the database                 
      update contactToUpdate;

      //CT, Sadham : Update the code to transfer all the Latest Enquiry Information associated to the 
      //                               converted Lead to Converted contact

      //Get all Latest Enquiry Information associated to the converted Lead
      List<Latest_Enquiry_Information__c> LEIToBeTransferred = [SELECT Id, Enquirer__c, Contact__c
                                                                  FROM Latest_Enquiry_Information__c
                                                                  WHERE Enquirer__c = :leadId];
      // Loop through the Latest Enquiry Information and assign them to the Contact                                              
      for (Latest_Enquiry_Information__c thisLEIToBeTransferred : LEIToBeTransferred ){
        thisLEIToBeTransferred.Contact__c  = contactID;
        thisLEIToBeTransferred.Enquirer__c = null;
      }                                                          
      //Update transferred Latest Enquiry Information
      if(!LEIToBeTransferred.isEmpty()){
        update LEIToBeTransferred;
      }

              
      
      // Get all Event Registrations associated to the Lead
      List<Event_Registration__c> AllEventRegistrations = [    SELECT Id, Enquirer__c, Contact__c
                                                               FROM Event_Registration__c
                                                               WHERE Enquirer__c = :leadConvertResult.getLeadID()
                                                          ];
      
      // Loop through the Event Registrations and assign them to the Contact                                              
      for( Event_Registration__c ev : AllEventRegistrations)
      {
          ev.Contact__c = contactID;
          ev.Enquirer__c = null;
      }

      // Update the Event Registrations in the database   
      update AllEventRegistrations;
      
      

      // Get all Cases associated to the Lead
      List<Case> AllCases = [    SELECT Id, Enquirer__c, ContactId
                                 FROM Case
                                 WHERE Enquirer__c = :leadConvertResult.getLeadID()
                            ];
      
      // Loop through the Cases and assign them to the Contact                                              
      for (Case cs : AllCases)
      {
          cs.ContactId = contactID;
          cs.Enquirer__c = null;
          
      }      
      
      // Update the Cases in the database   
      update AllCases; 
        
        
        
      // Get all Individual Email Results associated to the Lead
      List<et4ae5__IndividualEmailResult__c> AllEmailResults = [   SELECT Id, et4ae5__Lead__c, et4ae5__Contact__c
                                                                   FROM et4ae5__IndividualEmailResult__c
                                                                   WHERE et4ae5__Lead__c = :leadConvertResult.getLeadID()
                                                               ];
      
      // Loop through the Individual Email Results and assign them to the Contact                                              
      for (et4ae5__IndividualEmailResult__c er : AllEmailResults )
      {
          er.et4ae5__Contact__c= contactID;
      }      
      
      // Update the Individual Email Results in the database   
      update AllEmailResults;  

      return;
      
  }
  
}