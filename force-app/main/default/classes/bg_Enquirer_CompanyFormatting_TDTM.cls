/*****************************************************************
* bg_Enquirer_CompanyFormatting_TDTM
*
* Enquirer Trigger Handler for checking Company field format BeforeInsert or BeforeUpdate
* 
* Author: Stuart Barber
* Created: 12-09-2018
******************************************************************/

global class bg_Enquirer_CompanyFormatting_TDTM extends hed.TDTM_Runnable 
{
	/*
    *
    * Trigger Handler on BeforeInsert & BeforeUpdate for checking the Enquirer Company field format
    *
    */
	public override DmlWrapper run(List<SObject> newlist, List<SObject> oldlist, 
    Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {
		if (triggerAction == Action.BeforeInsert || triggerAction == Action.BeforeUpdate)
		{
			List<Lead> enquirerRecords = newlist;

			setEnquirerCompanyIfNotCorrectFormat(enquirerRecords);
		}

		return null;
	}

	/*
    *
    * To update Company field if not in the correct format (First Name + Last Name)
    *
    */
	private static void setEnquirerCompanyIfNotCorrectFormat(List<Lead> enquirerRecords)
	{
		for(Lead enquirer : enquirerRecords)
		{
			if(enquirer.Company != enquirer.FirstName + ' ' + enquirer.LastName)
			{
				if(!String.isBlank(enquirer.FirstName))
				{
					enquirer.Company = enquirer.FirstName + ' ' + enquirer.LastName;
				}
				else
				{
					enquirer.Company = enquirer.LastName;
				}
			}
		}
	}  	
}