/**
 * @File Name          : ct_PaymentsExporterCtlrTest.cls
 * @Description        : 
 * @Author             : Creation Admin  - Creation Technology Solutions
 * @Group              : 
 * @Last Modified By   : Creation Admin  - Creation Technology Solutions
 * @Last Modified On   : 4/6/2020, 3:22:41 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/6/2020   Creation Admin  - Creation Technology Solutions     Initial Version
**/
@isTest
public class ct_PaymentsExporterCtlrTest {
    @isTest(SeeAllData=true)
    public static void paymentExporterJsonFromDateTest() {
        Test.startTest();
        Contact thisContact = ct_TestDataFactory.createContact();
        insert thisContact;

        Opportunity thisOpportunity = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
        thisOpportunity.Amount = 200.00;
        insert thisOpportunity;

        npsp__General_Accounting_Unit__c thisGAU = ct_TestDataFactory.createGAURecord('Creation GAU');
        insert thisGAU;
   
        npsp__Allocation__c thisGAUAllocationOne = ct_TestDataFactory.createGAUAllocation(thisGAU.Id, thisOpportunity.Id, 200.00);
        insert thisGAUAllocationOne;

        asp04__Payment__c thisAsperatoPayment = new asp04__Payment__c();
        thisAsperatoPayment.Opportunity__c                      = thisOpportunity.Id;
        thisAsperatoPayment.asp04__Amount__c                    = thisOpportunity.Amount;
        thisAsperatoPayment.asp04__Payment_Route_Selected__c    = ct_Constants.ASPERATO_CARD_PAYMENT_METHOD;
        thisAsperatoPayment.asp04__Payment_Route_Options__c     = ct_Constants.ASPERATO_CARD_PAYMENT_METHOD;
        thisAsperatoPayment.asp04__Payment_Stage__c             = ct_Constants.ASPERATO_PAYMENT_STAGE_COLLECTED;
        insert thisAsperatoPayment;

        npe01__OppPayment__c thisGEMPayment   = ct_TestDataFactory.createGEMPayment(thisOpportunity, ct_Constants.CASH_PAYMENT_METHOD);
        thisGEMPayment.npe01__Paid__c         = true;
        thisGEMPayment.npe01__Payment_Date__c = System.today();
        thisGEMPayment.Asperato_Payment__c    = thisAsperatoPayment.Id;
        insert thisGEMPayment;

        PageReference pageRef = Page.ct_PaymentsExporter;
        pageRef.getParameters().put('from', String.valueOf((System.today()-1).format()));
        Test.setCurrentPage(pageRef);
        ct_PaymentsExporterCtlr thisPaymentExporterCtrl = new ct_PaymentsExporterCtlr();
        //Assert the JSON
        String jsonData = thisPaymentExporterCtrl.jsonAsperatoContent;
        System.assert(String.IsNotEmpty(jsonData));

        Test.stopTest();
    }
    @isTest(SeeAllData=true)
    public static void paymentExporterJsonBothDateTest() {
        Test.startTest();
        Contact thisContact = ct_TestDataFactory.createContact();
        insert thisContact;

        Opportunity thisOpportunity = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
        thisOpportunity.Amount = 200.00;
        insert thisOpportunity;

        npsp__General_Accounting_Unit__c thisGAU = ct_TestDataFactory.createGAURecord('Creation GAU');
        insert thisGAU;
   
        npsp__Allocation__c thisGAUAllocationOne = ct_TestDataFactory.createGAUAllocation(thisGAU.Id, thisOpportunity.Id, 200.00);
        insert thisGAUAllocationOne;

        asp04__Payment__c thisAsperatoPayment = new asp04__Payment__c();
        thisAsperatoPayment.Opportunity__c                      = thisOpportunity.Id;
        thisAsperatoPayment.asp04__Amount__c                    = thisOpportunity.Amount;
        thisAsperatoPayment.asp04__Payment_Route_Selected__c    = ct_Constants.ASPERATO_CARD_PAYMENT_METHOD;
        thisAsperatoPayment.asp04__Payment_Route_Options__c     = ct_Constants.ASPERATO_CARD_PAYMENT_METHOD;
        thisAsperatoPayment.asp04__Payment_Stage__c             = ct_Constants.ASPERATO_PAYMENT_STAGE_COLLECTED;
        insert thisAsperatoPayment;

        npe01__OppPayment__c thisGEMPayment   = ct_TestDataFactory.createGEMPayment(thisOpportunity, ct_Constants.CASH_PAYMENT_METHOD);
        thisGEMPayment.npe01__Paid__c         = true;
        thisGEMPayment.npe01__Payment_Date__c = System.today();
        thisGEMPayment.Asperato_Payment__c    = thisAsperatoPayment.Id;
        insert thisGEMPayment;

        PageReference pageRef = Page.ct_PaymentsExporter;
        pageRef.getParameters().put('from', String.valueOf((System.today()-3).format()));
        pageRef.getParameters().put('to', String.valueOf((System.today()-1).format()));
        Test.setCurrentPage(pageRef);
        ct_PaymentsExporterCtlr thisPaymentExporterCtrl = new ct_PaymentsExporterCtlr();
        //Assert the JSON
        String jsonData = thisPaymentExporterCtrl.jsonAsperatoContent;
        System.assert(String.IsNotEmpty(jsonData));
        Test.stopTest();
    }
}