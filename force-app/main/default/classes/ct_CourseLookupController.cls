/**
 * @File Name          : ct_CourseLookupController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 7/2/2020, 8:26:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/17/2020   Creation Admin     Initial Version
**/
public with sharing class ct_CourseLookupController {
  /**
  * @description get course records to wrap as picklist entries
  * @param courseName 
  * @return List<Wrapper> 
  **/
  @AuraEnabled
  public static List<Wrapper> getCourse(String courseName){
    //courseWrapperList - stores list of course records in a wrapper
    List<Wrapper> courseWrapperList = new List<Wrapper>();
    try{
      //query to fetch course record based on search string and stores in wrapper
      for(Account thisCourse : [SELECT Id, Name, Available_on_Web__c FROM Account 
                                  WHERE Name LIKE :'%'+courseName+'%' 
                                  AND Available_on_Web__c = true
                                  AND RecordTypeId =:ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM
                                  ORDER BY Name asc
                                  LIMIT 50000]){
        courseWrapperList.add(new Wrapper(thisCourse));
      }
    }
    catch (Exception ex){
      ct_Logger.logMessage(ex, 'Course Lookup Component');
      throw new AuraHandledException('Error '+ ex.getMessage() + ex.getLineNUmber());

    }
    //return list of course records
    return courseWrapperList;
  }
  
  public class Wrapper{    
    //To store course record name
    @AuraEnabled
    public String label {get;set;} 
    //To store course record Id   
    @AuraEnabled
    public String value {get;set;}
    Wrapper(Account thisCourse){
      label = thisCourse.Name;
      value = thisCourse.Name+'UNIQUE_ID'+thisCourse.Id;
    }
  }

}