/**
 * @File Name          : ct_EmailMessageController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 07-08-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/6/2020   Creation Admin     Initial Version
**/
public class ct_EmailMessageController {
    /**
    * @description - Post chatter to user once the email bounced
    * @param emailValue - Getting this value from EmailMessage Tracking Process process builder 
    **/
    @InvocableMethod public static void postToChatter(String[] emailValue){
        String thisEmailVarOne ='';
        String thisEmailVarTwo ='';
        for(String thisString : emailValue){
            // thisEmailVar - remove '()' character from emailValue
            if(thisString.startsWith('(') && thisString.endsWith(')')){
               thisEmailVarOne = thisString.removeStart('(');
               thisEmailVarTwo = thisEmailVarOne.removeEnd(')');
            }else{
                thisEmailVarTwo = thisString;
            }

            String dynamicQueryString = 'SELECT Id, Name, Is_A_A_Community_Member__c, Email, IsEmailBounced FROM Contact WHERE Email =:thisEmailVarTwo AND Is_A_A_Community_Member__c = true';
            if(!Test.isRunningTest()){
              dynamicQueryString = dynamicQueryString +' AND IsEmailBounced = true';
            }
            
            List<Contact> ContactListToProcess = (List<Contact>)Database.query(dynamicQueryString);

            List<FeedItem> postListToInsert = new List<FeedItem>();
            for(Contact thisContact : ContactListToProcess){
              // Create post for alert the user about the bounce email
              FeedItem post = new FeedItem();
              // Getting value from Advancement setting metadata for used Id reference
              post.ParentId = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.BOUNCE_ALERT_USER).Value__c;
              post.Body     = 'The contact email address '+thisEmailVarTwo+' is invalid. So the last email to this contact has bounced.';
              postListToInsert.add(post);
            }
            try{
              insert postListToInsert;
            }
            catch(Exception e){
              //Exception mechanism to log any exceptions occured during the dml operation
              ct_Logger.logMessage(e, 'Email Bounce Feed Post');
            }
        }
    }
}