/**
 * @File Name          : ct_UOPWebDonationFormController_Test.cls
 * @Description        : 
 * @Author             : Javid Sherif
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 15/6/2020, 1:15:02 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/3/2020   Javid Sherif     Initial Version
**/
@IsTest
public class ct_UOPWebDonationFormController_Test{

  /**
  * @description test single donation submission web form data
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @IsTest(SeeAllData=true)
  static void testSingleDonation(){
    String donationFormData = '{'+
                              '"donationReason": "A General Donation",'+
                              '"donationToProject": "",'+
                              '"paymentType": "'+ct_Constants.GEM_CARD_PAYMENT_METHOD+'",'+
                              '"paymentFrequency": "Single",'+
                              '"donateToList": null,'+
                              '"hasOpportunity": false,'+
                              '"hed__AlternateEmail__c": "test@test.com",'+
                              '"opportunityId": "",'+
                              '"asperatoId": "",'+
                              '"myOwnDonation": true,'+
                              '"fundraisingOrCollected": false,'+
                              '"wishToGiftAid": true,'+
                              '"notWishToGiftAid": false,'+
                              '"My_donation_is_in_memory_of__c": "",'+
                              '"Phone_Consent__c": "",'+
                              '"Salutation": "Mr.",'+
                              '"hed__MailingCountry__c": "India",'+
                              '"MailingState": "Tamil Nadu",'+
                              '"LastName": "Salesforce",'+
                              '"hed__MailingPostalCode__c": "636302",'+
                              '"MailingCountry": "India",'+
                              '"Email": "javid.sherif@uuik-tec.com",'+
                              '"My_donation_is_in_honour_of__c": "",'+
                              '"Amount": "50",'+
                              '"Asperato_Payment__c": "",'+
                              '"MailingCity": "salem",'+
                              '"hed__MailingCity__c": "salem",'+
                              '"SMS_Consent__c": "",'+
                              '"FirstName": "JavidAsperato",'+
                              '"Phone": "",'+
                              '"MailingStreet": "",'+
                              '"MailingPostalCode": "636302",'+
                              '"Directmail_Consent__c": "",'+
                              '"hed__MailingState__c": "Tamil Nadu",'+
                              '"Email_Consent__c": "",'+
                              '"MiddleName": "",'+
                              '"Contact__c": "",'+
                              '"hed__MailingStreet__c": "",'+
                              '"PhoneNumber": "09597799033"'+
                            '}';
    ct_UOPWebDonationFormController thisDonationCtrlObject = new ct_UOPWebDonationFormController();

    Test.startTest();

    //Metadata Setup
    Webform__mdt donationForm = new Webform__mdt();
    donationForm.MasterLabel = 'Donation';
    donationForm.DeveloperName = 'Donation';

    List<Webform_Data__mdt> metaDataList = new List<Webform_Data__mdt>();
    metaDataList.add(new Webform_Data__mdt(MasterLabel = 'Donor Salutation', Form_Field_Name__c = 'Donor Salutation', SObject__c = 'Contact',
    SObject_Field__c  = 'Salutation', Webform__c = donationForm.Id, Web_Label__c='Title', Web_Placeholder__c ='Select  your Title'));

    ct_UOPWebDonationFormController.fetchWebformMetadata('Donation');

    //Get General Account Unit
    npsp__General_Accounting_Unit__c thisGAU = ct_TestDataFactory.createGAURecord('Plastics Project');
    Insert thisGAU;

    //Create Asperato Payment Record
    Id asperatoPaymentId = ct_UOPWebDonationFormController.createNewAsperatoPayment(donationFormData);

    //Create Donation Records
    String donationFormDataWithAsperato = '{'+
                              '"donationReason": "A General Donation",'+
                              '"donationToProject": "'+thisGAU.Id+'",'+
                              '"paymentType": "'+ct_Constants.GEM_CARD_PAYMENT_METHOD+'",'+
                              '"paymentFrequency": "Single",'+
                              '"donateToList": null,'+
                              '"hasOpportunity": false,'+
                              '"opportunityId": "",'+
                              '"asperatoId": "'+asperatoPaymentId+'",'+
                              '"hed__AlternateEmail__c": "test@test.com",'+
                              '"myOwnDonation": true,'+
                              '"fundraisingOrCollected": false,'+
                              '"wishToGiftAid": true,'+
                              '"notWishToGiftAid": false,'+
                              '"My_donation_is_in_memory_of__c": "",'+
                              '"Phone_Consent__c": "",'+
                              '"Salutation": "Mr.",'+
                              '"hed__MailingCountry__c": "India",'+
                              '"MailingState": "Tamil Nadu",'+
                              '"LastName": "Salesforce",'+
                              '"hed__MailingPostalCode__c": "636302",'+
                              '"MailingCountry": "India",'+
                              '"Email": "javid.sherif@uuik-tec.com",'+
                              '"My_donation_is_in_honour_of__c": "",'+
                              '"Amount": "50",'+
                              '"Asperato_Payment__c": "",'+
                              '"MailingCity": "salem",'+
                              '"hed__MailingCity__c": "salem",'+
                              '"SMS_Consent__c": "",'+
                              '"FirstName": "JavidAsperato",'+
                              '"Phone": "",'+
                              '"MailingStreet": "",'+
                              '"MailingPostalCode": "636302",'+
                              '"Directmail_Consent__c": "",'+
                              '"hed__MailingState__c": "Tamil Nadu",'+
                              '"Email_Consent__c": "",'+
                              '"MiddleName": "",'+
                              '"Contact__c": "",'+
                              '"hed__MailingStreet__c": "",'+
                              '"PhoneNumber": "09597799033"'+
                            '}';
    ct_UOPWebDonationFormController.saveDonationDetails(donationFormDataWithAsperato);
    Test.stopTest();

    //Assert Whether All Neccessary Records Got created
    //Opportunity dononationOpportunity = [SELECT Id FROM Opportunity WHERE Name LIKE '%JavidAsperato%'];
    //system.assertEquals(1, [SELECT Id FROM npe01__OppPayment__c WHERE Asperato_Payment__c =: asperatoPaymentId].size());
  }

  /**
  * @description test recurring donation submission web form data
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @IsTest(SeeAllData=true)
  static void testRecurringDonation(){
    String donationFormData = '{'+
                              '"donationReason": "A General Donation",'+
                              '"donationToProject": "",'+
                              '"paymentType": "Direct Debit",'+
                              '"paymentFrequency": "Monthly",'+
                              '"donateToList": null,'+
                              '"hasOpportunity": false,'+
                              '"opportunityId": "",'+
                              '"asperatoId": "",'+
                              '"authorizationId":"",'+
                              '"myOwnDonation": true,'+
                              '"fundraisingOrCollected": false,'+
                              '"hed__AlternateEmail__c": "test@test.com",'+
                              '"wishToGiftAid": true,'+
                              '"notWishToGiftAid": false,'+
                              '"My_donation_is_in_memory_of__c": "",'+
                              '"Phone_Consent__c": "",'+
                              '"Salutation": "Mr.",'+
                              '"hed__MailingCountry__c": "India",'+
                              '"MailingState": "Tamil Nadu",'+
                              '"LastName": "Salesforce",'+
                              '"hed__MailingPostalCode__c": "636302",'+
                              '"MailingCountry": "India",'+
                              '"Email": "javid.sherif@creation-tec.com",'+
                              '"My_donation_is_in_honour_of__c": "",'+
                              '"Amount": "50",'+
                              '"Asperato_Payment__c": "",'+
                              '"MailingCity": "salem",'+
                              '"hed__MailingCity__c": "salem",'+
                              '"SMS_Consent__c": "",'+
                              '"FirstName": "Javid",'+
                              '"Phone": "",'+
                              '"MailingStreet": "",'+
                              '"MailingPostalCode": "636302",'+
                              '"Directmail_Consent__c": "",'+
                              '"hed__MailingState__c": "Tamil Nadu",'+
                              '"Email_Consent__c": "",'+
                              '"MiddleName": "",'+
                              '"Contact__c": "",'+
                              '"hed__MailingStreet__c": "",'+
                              '"PhoneNumber": "09597799033"'+
                            '}';
    ct_UOPWebDonationFormController thisDonationCtrlObject = new ct_UOPWebDonationFormController();

    Test.startTest();
    //Invoke Picklist Value Methods
    //ct_UOPWebDonationFormController.getinitialPicklistValues();

    //Metadata Setup
    Webform__mdt donationForm = new Webform__mdt();
    donationForm.MasterLabel = 'Donation';
    donationForm.DeveloperName = 'Donation';

    List<Webform_Data__mdt> metaDataList = new List<Webform_Data__mdt>();
    metaDataList.add(new Webform_Data__mdt(MasterLabel = 'Donor Salutation', Form_Field_Name__c = 'Donor Salutation', SObject__c = 'Contact',
    SObject_Field__c  = 'Salutation', Webform__c = donationForm.Id, Web_Label__c='Title', Web_Placeholder__c ='Select  your Title'));

    ct_UOPWebDonationFormController.fetchWebformMetadata('Donation');

    //Get General Account Unit
    npsp__General_Accounting_Unit__c thisGAU = ct_TestDataFactory.createGAURecord('Plastics Project');
    Insert thisGAU;

    //Create Asperato Payment Record
    Id authorizationId = ct_UOPWebDonationFormController.createAuthorizationRecord(donationFormData);

    //Create Donation Records
    String donationFormDataWithAsperato = '{'+
                              '"donationReason": "A General Donation",'+
                              '"donationToProject": "'+thisGAU+'",'+
                              '"paymentType": "Direct Debit",'+
                              '"paymentFrequency": "Monthly",'+
                              '"donateToList": null,'+
                              '"hasOpportunity": false,'+
                              '"opportunityId": "",'+
                              '"asperatoId": "",'+
                              '"authorizationId": "'+authorizationId+'",'+
                              '"hed__AlternateEmail__c": "test@test.com",'+
                              '"paymentCollectionPoint": "15",'+
                              '"paymentCollection": "Monthly",'+
                              '"myOwnDonation": true,'+
                              '"fundraisingOrCollected": false,'+
                              '"wishToGiftAid": true,'+
                              '"notWishToGiftAid": false,'+
                              '"My_donation_is_in_memory_of__c": "",'+
                              '"Phone_Consent__c": "",'+
                              '"Salutation": "Mr.",'+
                              '"hed__MailingCountry__c": "India",'+
                              '"MailingState": "Tamil Nadu",'+
                              '"LastName": "Salesforce",'+
                              '"hed__MailingPostalCode__c": "636302",'+
                              '"MailingCountry": "India",'+
                              '"Email": "javid.sherif@creation-tec.com",'+
                              '"My_donation_is_in_honour_of__c": "",'+
                              '"Amount": "50",'+
                              '"Asperato_Payment__c": "",'+
                              '"MailingCity": "salem",'+
                              '"hed__MailingCity__c": "salem",'+
                              '"SMS_Consent__c": "",'+
                              '"FirstName": "Javid",'+
                              '"Phone": "",'+
                              '"MailingStreet": "",'+
                              '"MailingPostalCode": "636302",'+
                              '"Directmail_Consent__c": "",'+
                              '"hed__MailingState__c": "Tamil Nadu",'+
                              '"Email_Consent__c": "",'+
                              '"MiddleName": "",'+
                              '"Contact__c": "",'+
                              '"hed__MailingStreet__c": "",'+
                              '"PhoneNumber": "09597799033"'+
                            '}';
    ct_UOPWebDonationFormController.saveDonationDetails(donationFormDataWithAsperato);
    Test.stopTest();
  }
}