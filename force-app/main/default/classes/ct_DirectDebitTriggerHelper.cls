/**
   * @File Name          : ct_DirectDebitTriggerHelper.cls
   * @Author             : Creation Admin
   * @Last Modified By   : Creation Admin
   * Description         : To link the General Account unit from Recurring donation to Opportunity and payment related GAU.
  **/
  public class ct_DirectDebitTriggerHelper {    
      /*
      * populateGAUAllocation : To populate the GAU to opportunity and payment related allocation from recurring donation
      * @param : Set<Id> recurringDonationId - GAU having recurring donations
      * return - void
      */ 
      public static List<npsp__Allocation__c> populateGAUAllocation(Set<Id> recurringDonationId){
          Set<Id> opportunityIds = new Set<Id>();
          Set<Id> paymentIds     = new Set<Id>();
          List<npsp__Allocation__c> allocationList = new List<npsp__Allocation__c>();
          /* Loop through the GAU allocation by allocation opportunity's related recurring doantion is matching with the recurringDonationId parameter
          and updating the allocation list*/
          for(npsp__Allocation__c thisAllocation : [SELECT Id, Name, 
                                                          npsp__General_Accounting_Unit__c, 
                                                          npsp__Opportunity__c,
                                                          npsp__Opportunity__r.npe03__Recurring_Donation__c,
                                                          npsp__Opportunity__r.npe03__Recurring_Donation__r.General_Accounting_Unit__c
                                                          FROM npsp__Allocation__c
                                                          WHERE npsp__Opportunity__r.npe03__Recurring_Donation__c IN: recurringDonationId]){
              // Check if the allocation GAU and Opportunity related recurring donation GAU is different or not and If yes will enter here
              if(thisAllocation.npsp__General_Accounting_Unit__c != thisAllocation.npsp__Opportunity__r.npe03__Recurring_Donation__r.General_Accounting_Unit__c){
                 // Map allocation GAU with Opportunity related recurring donation GAU  
                 thisAllocation.npsp__General_Accounting_Unit__c = thisAllocation.npsp__Opportunity__r.npe03__Recurring_Donation__r.General_Accounting_Unit__c; 
                 allocationList.add(thisAllocation);                                                        
              }                                                                                                     
          }
          return allocationList;         
      }
      
      /*
      * Description : To populate the authorisation status from authorisation record's status
      * @param : List<npe03__Recurring_Donation__c> recurringDonationList - New list of recurring doantion
      * @param : Set<Id> authorisationId - Recurring donation authorisation id
      * return - void
      */
      public static void populateAuthorisationStatus(List<npe03__Recurring_Donation__c> recurringDonationList, Set<Id> authorisationId){
          // To query the asperato authorisation based on authorisationId and stored in thisAuthorisationMap
          Map<Id, asp04__Authorisation__c> thisAuthorisationMap = new Map<Id, asp04__Authorisation__c>([SELECT Id, Name, asp04__Status__c 
                                                                                                            FROM asp04__Authorisation__c 
                                                                                                            WHERE Id IN:authorisationId]);

          // If thisAuthorisationMap is not empty this will map Authorisation's status field to recurring donation Authorisation status value
          if(!thisAuthorisationMap.isEmpty()){
             // Loop through the recurring donation with recurringDonationList and mapping the Authorisation Status value
             for(npe03__Recurring_Donation__c thisRecurringDonation : recurringDonationList){
                 if(thisAuthorisationMap.containsKey(thisRecurringDonation.Asperato_Authorisation__c)){
                    // Map Authorisation's status field to recurring donation Authorisation status value
                    thisRecurringDonation.Authorisation_Status__c = thisAuthorisationMap.get(thisRecurringDonation.Asperato_Authorisation__c).asp04__Status__c; 
                 }
             } 
          }     
      }
      
      /*
      * populatePrimaryCampaignSource : To populate the opportunity primary campaign source from recurring donation
      * @param : Set<Id> recurringDonationId - Primary campaign source having recurring donation Ids
      * return : void
      */ 
      public static List<Opportunity> populatePrimaryCampaignSource(Set<Id> recurringDonationId){
          List<Opportunity> opportunityList = new List<Opportunity>();
          //Loop through recurring donation by recurringDonationId to update the opportunity
          for(Opportunity thisOpportunity : [SELECT Id, Name, 
                                                    CampaignId, 
                                                    npe03__Recurring_Donation__c,
                                                    npe03__Recurring_Donation__r.Primary_Campaign_Source__c
                                                    FROM Opportunity 
                                                    WHERE npe03__Recurring_Donation__c IN: recurringDonationId]){
              // If primary campign source in opportunity is not same with recurring donation primary campaign source, it will enter here                                         
              if(thisOpportunity.CampaignId != thisOpportunity.npe03__Recurring_Donation__r.Primary_Campaign_Source__c){
                 // Mapping the primary campign source in opportunity with recurring donation primary campaign source
                 thisOpportunity.CampaignId  = thisOpportunity.npe03__Recurring_Donation__r.Primary_Campaign_Source__c; 
                 opportunityList.add(thisOpportunity);                                            
              }                                             
          }
          return opportunityList;
      }
      
      /**
      * @description - Method to send Email Alerts for Direct Debit Schedules
      * @param recurringDonationIds - Once the Authorisation status is changed to Inforce that related recurring donation id will collect here
      * @return void 
      **/
      public static void sentEmailAlertDirectDebitSchedule(Set<Id> recurringDonationIds){ 
      Id orgWideEmailId;
      //Get UOP Advancement Team Email Address
      OrgWideEmailAddress[] orgWideAddress = ct_Util.getOrgWideEmail(ct_Constants.UOP_ADVANCEMENT_TEAM_ADDRESS); 
      if (orgWideAddress.size() > 0 ) { 
          orgWideEmailId = orgWideAddress.get(0).Id; 
      }
      
      //Get Direct debit payment schedule Email Template
      EmailTemplate thisDirectDebitEmailTemplate = [SELECT Id, DeveloperName 
                                                           FROM EmailTemplate 
                                                           WHERE DeveloperName =: ct_Constants.DIRECT_DEBIT_EMAIL_TEMPLATE  
                                                           AND IsActive = true];

        //List of SingleEmailMessage to be Sent
        List<Messaging.SingleEmailMessage> mailListToSend = new List<Messaging.SingleEmailMessage>();
        if(orgWideEmailId != null){
        // Loop through the recurring donation by recurringDonationIds send an email
        for(npe03__Recurring_Donation__c thisRecurringDonation : [SELECT Id,
                                                                        Asperato_Authorisation__c,    
                                                                        npe03__Contact__c,
                                                                        npe03__Contact__r.Email,    
                                                                        npe03__Contact__r.IsEmailBounced
                                                                        FROM npe03__Recurring_Donation__c 
                                                                        WHERE Id IN: recurringDonationIds]){
            // checking if the email address is available for sending an email (Check if already bounced or not)
            if(thisRecurringDonation.Asperato_Authorisation__c != null 
               && !thisRecurringDonation.npe03__Contact__r.IsEmailBounced
               && thisRecurringDonation.npe03__Contact__r.Email != null
               && thisDirectDebitEmailTemplate != null){
               // Add this values to mailListToSend for create email message through createDirectDebitEmailMessage by passing required parameters
               mailListToSend.add(createDirectDebitEmailMessage(thisRecurringDonation.npe03__Contact__r.Email, thisRecurringDonation.Id, thisRecurringDonation.npe03__Contact__c,  thisDirectDebitEmailTemplate.Id, orgWideEmailId));                                                                            
            }                                                  
        }
        
        if(!Test.isRunningTest()
           && !mailListToSend.isEmpty()){
           // Sending email to respective email addresses
           Messaging.sendEmail(mailListToSend); 
        }
      }
    }
      
    
    /**
    * @description - Method to create SingleEmailMessage for Direct debit donors
    * @param toEmailAddress - Donor Email Address
    * @param recurringDonationId - Donation Opportunity's recurring donation Id
    * @param thisContactId - Donor Contact Id
    * @param thankYouEmailTempId - Direct debit payment schedule notification
    * @param orgwideEmailId - UOP Advancement Team Org wide Address Id
    * @return Messaging.SingleEmailMessage to sentEmailAlertDirectDebitSchedule method 
    **/  
    public static Messaging.SingleEmailMessage createDirectDebitEmailMessage(String toEmailAddress,Id recurringDonationId, Id thisContactId, Id thankYouEmailTempId, Id orgwideEmailId){
      //Create email message
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setOrgWideEmailAddressId(orgwideEmailId); 
      mail.setToAddresses(new String[]{toEmailAddress});
      mail.setTargetObjectId(thisContactId);
      mail.setTemplateId(thankYouEmailTempId);
      mail.setWhatId(recurringDonationId);
      return mail;
    }
  }