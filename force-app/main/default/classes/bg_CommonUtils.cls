/*****************************************************************
* bg_CommonUtils
*
* Utility methods for general/common purposes
* 
* Test Class: bg_CommonUtils_Test
*
* Author: Stuart Barber
* Created: 11-09-2018
* ************** Version history ***********************
* CT, Dinesh : 10/07/2020 - Introduced custom label to store Record Type Developer Name for retrieving Record Type ID
                                             by Developer Name instead of Record Type Name
******************************************************************/

public with sharing class bg_CommonUtils {
	
	// ID Prefixes
    public static final String USER_ID_PREFIX = '005';
    public static final String GROUP_ID_PREFIX = '00G';

	// Object Constants
	public static final String CASE_OBJECT = 'Case';
	public static final String CONTACT_OBJECT = 'Contact';
	public static final String OPPORTUNITY_OBJECT = 'Opportunity';
	public static final String ACCOUNT_OBJECT = 'Account';
	public static final String LEAD_OBJECT = 'Lead';

  // Record Type Constants
  //CT, Dinesh : 10/07/2020 : Update the Student Contact Record type to UOP Contact
	public static final String CONTACT_STUDENT_RT = 'UOP Contact';
	public static final String ACCOUNT_ADMINISTRATIVE_RT = 'Administrative';
	public static final String CASE_GENERAL_ENQUIRY_RT = 'General Enquiry';
	public static final String LEAD_INFLUENCER_RT = 'Influencer';
	public static final String LEAD_STUDENT_RT = 'Student';

  // Record Type Ids

  //CT, Dinesh : 10/07/2020 : Introduced custom label to store Record Type Developer Name
  public static Id generalEnquiryCaseRTId = getRecordTypeId(CASE_OBJECT, System.Label.Case_General_Enquiry_Record_Type);
  //CT, Dinesh : 10/07/2020 : Introduced custom label to store Record Type Developer Name
  public static Id studentContactRTId = getRecordTypeId(CONTACT_OBJECT, System.Label.Contact_Student_Record_Type);
  
	public static Id administrativeAccountRTId = getRecordTypeId(ACCOUNT_OBJECT, ACCOUNT_ADMINISTRATIVE_RT);
	public static Id influencerEnquirerRTId = getRecordTypeId(LEAD_OBJECT, LEAD_INFLUENCER_RT);
	public static Id studentEnquirerRTId = getRecordTypeId(LEAD_OBJECT, LEAD_STUDENT_RT);

	/*
	*
	* Gets the Id of a Record Type based on the Object Name and Record Type Name
	*
	*/
	public static Id getRecordTypeID(String objectName, String recordTypeName)
    {
        //CT, Dinesh : Update the code to get the Record type Id by record type developer Name instead of Record type Name
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
    }

    /*
    *
    * Return a boolean value based on whether the Id specified corresponds to a Group (Queue).
	*
    */
    public static Boolean isGroupId(Id idToCheck)
    {
        return isIdOfType(idToCheck, GROUP_ID_PREFIX);
    }

    /*
    *
    * Return a boolean value based on whether the Id specified has the specified prefix
    *
    */
    public static Boolean isIdOfType(Id idToCheck, String idPrefix) 
    {
        String idCheckString = idToCheck;
        return (String.isNotBlank(idCheckString) && idCheckString.startsWith(idPrefix));
    }
    
    /*
    *
    * Return a map of Ids to string fields
    *
    */
    public static Map<string, Id> getMapOfIdsByStringField(List<SObject> recordsToMap, string fieldToMapBy)
    {
        Map<string, Id> recordsByStringField = new Map<string, Id>();
        for(SObject recordToMap : recordsToMap)
        {
            string fieldValue = (string)recordToMap.get(fieldToMapBy);
            recordsByStringField.put(fieldValue, recordToMap.Id);
        }
        
        return recordsByStringField;
    }
}