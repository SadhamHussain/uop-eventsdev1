/**
   * @File Name          : ct_VolunteerHoursTriggerHandler_Test.cls
   * @Author             : Creation Admin
   * @Last Modified By   : Creation Admin
   * Description         : To test ct_VolunteerHoursTriggerHandler.cls
  **/
@isTest public class ct_VolunteerHoursTriggerHandler_Test { 
   @isTest(SeeAllData=true) public static void sendVolunteerHourConfirmedEmail(){
     // Create contact
     Contact thisContact = ct_TestDataFactory.createContact();
     insert thisContact;
     // Create contact
     Campaign thisCampaign = new Campaign();
     thisCampaign.Name = 'Test Campaign';  
     insert thisCampaign;
     // Create Volunteer Job
     GW_Volunteers__Volunteer_Job__c thisJob = ct_TestDataFactory.createVolunteerJob(thisCampaign.Id);
     insert thisJob;
     // Create Volunteer Hours
     GW_Volunteers__Volunteer_Hours__c thisHours = ct_TestDataFactory.createVolunteerHours(thisContact.Id, thisJob.Id);
     insert thisHours;
     
     Test.startTest();
       thisHours.GW_Volunteers__Status__c       = ct_Constants.VOLUNTEER_COMPLETED_STATUS;
       thisHours.GW_Volunteers__Hours_Worked__c = 8;
       update thisHours;
     Test.stopTest();
   }
    @isTest(SeeAllData=true) public static void testEmailOnInsert(){
     // Create contact
     Contact thisContact = ct_TestDataFactory.createContact();
     insert thisContact;
     // Create contact
     Campaign thisCampaign = new Campaign();
     thisCampaign.Name = 'Test Campaign';  
     insert thisCampaign;
     // Create Volunteer Job
     GW_Volunteers__Volunteer_Job__c thisJob = ct_TestDataFactory.createVolunteerJob(thisCampaign.Id);
     insert thisJob;
     // Create Volunteer Hours
     GW_Volunteers__Volunteer_Hours__c thisHours = ct_TestDataFactory.createVolunteerHours(thisContact.Id, thisJob.Id);
     thisHours.GW_Volunteers__Status__c       = ct_Constants.VOLUNTEER_COMPLETED_STATUS;
     thisHours.GW_Volunteers__Hours_Worked__c = 8;   
     
     Test.startTest();
        insert thisHours;
     Test.stopTest();
   }
}