/**
 * @File Name          : ct_AuthorisationTriggerHelper.cls
 * @Description        : Trigger Helper Class for ct_AuthorisationTriggerHandler_TDTM Trigger Handler Class
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/4/2020, 2:14:37 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/1/2020   Creation Admin     Initial Version
**/
public class ct_AuthorisationTriggerHelper {

  /**
  * @description : Cancel the recurring donations if the asperto authorisation is cancelled
  * @param recurringDonationIds Set of recurring donation Id's to be cancelled
  * @return void 
  **/
  public static void directDebitGoCardlessCancellationProcess(Set<Id> recurringDonationIds){
    try{
      //List of Recurring donation record to be updated
      List<npe03__Recurring_Donation__c> directDebitListToUpdate = new List<npe03__Recurring_Donation__c>();
      //Opportunity Map related to Recurring donation which also gone through cancellation process
      Map<Id, Opportunity> opportunityMap = ct_DirectDebitCancellationController.getRelatedOpportunityRecords(recurringDonationIds);
      //Written off the payment records related to the Opportunities gone through cancellation process
      ct_DirectDebitCancellationController.updateGEMPaymentsToWrittenOff(opportunityMap.keyset());
      //Update the processed Opportunities records
      update opportunityMap.values();
      //Get the Recurring Donation records to be cancelled and process the records for Go Cardless Cancellation
      //Since Manual Cancellation will be processed already
      for(npe03__Recurring_Donation__c thisDirectDebit : [SELECT Id, npe03__Open_Ended_Status__c, Asperato_Authorisation__c, 
                                                                DD_Cancellation_Reason__c, 
                                                                Direct_Debit_Cancellation__c,
                                                                Date_of_Manual_Cancellation__c,
                                                                Date_of_GoCardless_Cancellation__c,
                                                                User_who_Manually_Cancelled__c,
                                                                Direct_Debit_Status__c,
                                                                Authorisation_Status__c,
                                                                DD_Cancellation_Description__c
                                                            FROM npe03__Recurring_Donation__c
                                                            WHERE Direct_Debit_Status__c != :ct_Constants.DIRECT_DEBIT_AUTHORISATION_STATUS_CANCELLED
                                                            AND Id IN :recurringDonationIds]){

        thisDirectDebit.Direct_Debit_Cancellation__c        = ct_Constants.DD_CANCELLATION_GOCARDLESS_DD_CANCELLATION;
        thisDirectDebit.Date_of_GoCardless_Cancellation__c  = System.today();
        thisDirectDebit.User_who_Manually_Cancelled__c      = ct_Constants.DD_CANCELLATION_REASON_SYSTEM_CANCELLATION;
        thisDirectDebit.DD_Cancellation_Reason__c           = ct_Constants.DD_CANCELLATION_REASON_SYSTEM_CANCELLATION;
        thisDirectDebit.DD_Cancellation_Description__c      = ct_Constants.DD_CANCELLATION_REASON_SYSTEM_CANCELLATION;
        thisDirectDebit.Direct_Debit_Status__c              = ct_Constants.DIRECT_DEBIT_AUTHORISATION_STATUS_CANCELLED;
        thisDirectDebit.npe03__Open_Ended_Status__c         = ct_Constants.DIRECT_DEBIT_OPEN_ENDED_CLOSED;
        thisDirectDebit.Authorisation_Status__c             = ct_Constants.DIRECT_DEBIT_AUTHORISATION_STATUS_CANCELLED;
        directDebitListToUpdate.add(thisDirectDebit);
      }
      //Update GoCardless Cancelled Recurring Donation
      update directDebitListToUpdate;
    }
    catch (Exception e) {
      //Invoke Error Logger if any error occurs
      ct_Logger.logMessage(e, 'GoCardLess Direct Debit Cancellation Process');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }
}