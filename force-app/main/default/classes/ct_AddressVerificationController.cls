/**
 * @File Name          : ct_AddressVerificationController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 6/19/2020, 7:41:51 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/25/2020   Creation Admin     Initial Version
**/
public class ct_AddressVerificationController {
  /**
  * @description - Make callout based on Address search string
  * @param String addressString - text to make callout
  * @return list<AddressWrapper> - Wrapper class
  **/
  @AuraEnabled
  public static list<AddressWrapper> addressVerificationCallout(String addressString){
    List<AddressWrapper> addressList = new List<AddressWrapper>();
    try {
      //Make http callout to AFD based on the searched Address string
      string params = '&Data=Address&Task=FastFind&Fields=International&Lookup='+ addressString+'&Format=JSON';
      params = params.replaceAll(' ','%20');
      String endPointURLString = String.valueOf(getEndpoitURL())+params;
      HttpRequest thisPostRequest = new HttpRequest();
      thisPostRequest.setMethod('GET');
      thisPostRequest.setEndpoint(endPointURLString);
      thisPostRequest.setTimeout(20000); 
      // construct Http   
      Http getUserHttp      = new Http();
      HttpResponse response = new HttpResponse();
      //Make callout
      response = getUserHttp.send(thisPostRequest);
      if(response.getStatusCode() == 200){
        //Parsing the response bu sending the response to ct_AddressVerificationParser class
        ct_AddressVerificationParser thisJsonBody    = (ct_AddressVerificationParser)JSON.deserialize(response.getBody(), ct_AddressVerificationParser.class);
        if(thisJsonBody.Result != null 
        && Integer.valueOf(thisJsonBody.Result) == 1){
          // run a for loop for each address comes from the JSON body
          for(ct_AddressVerificationParser.Item  thisItem : thisJsonBody.Item){
            String address = thisItem.Address1 +' , '+ thisItem.Town+' , '+thisItem.Postcode ;
            // stores and construct the each adderess values in address wrapper
            addressList.add(new AddressWrapper(address, thisItem));
          }
        }
      }
      else{
        throw new AuraHandledException('Error Callout Failed'+ response.getStatusCode());
      }
      // return address wrapper list
      return addressList;
    }
     catch (Exception e) {
     throw new AuraHandledException('Error '+ e.getMessage() + e.getLineNUmber());    
    }
  }

  /**
  * @description Get Endpoint URL From Meta data
  * @author Javid Sherif | 17/4/2020 
  * @return String 
  **/
  private static String getEndpoitURL(){
    //return the end poin URL by query from the AFD Validator metadata
    return [SELECT Endpoint_URL__c FROM AFD_Validator_URL__mdt LIMIT 1].Endpoint_URL__c;
  }
  @AuraEnabled
  public static void saveAddressDetails(String addressDetails, String recordId, String relatedAPIName){
    try{
      //alumniObject - Deserialze the data captured in the alumni web form from String to Map
      Map<String, Object> addressObject = (Map<String, Object>)JSON.deserializeUntyped(addressDetails);
      if(Id.valueOf(recordId).getSObjectType().getDescribe().getName() == 'Contact'
      && (addressObject.containsKey('hed__Default_Address__c')
      && Boolean.valueOf(addressObject.get('hed__Default_Address__c')))){
        contact thisContact = constructContactRecord(addressObject,Id.valueOf(recordId));
        update thisContact;
      }
      else if(Id.valueOf(recordId).getSObjectType().getDescribe().getName() == 'Account'
      && (addressObject.containsKey('hed__Default_Address__c')
      && Boolean.valueOf(addressObject.get('hed__Default_Address__c')))){
        Account thisAccount = constructAccountRecord(addressObject,Id.valueOf(recordId));
        update thisAccount;
      }
      else{
        constructAddressRecord(addressObject, Id.valueOf(recordId), relatedAPIName);
      }
    }
    catch(Exception ex){
      ct_Logger.logMessage(ex, 'Address Updation');
      throw new AuraHandledException('Error '+ ex.getMessage());  
    }
  }

  /**
  * @description - Mapping the address field values with contact address fields
  * @param Map<String Object> addressObject - Deserialized address Object 
  * @param Id recordId - contact Id
  * @return contact - contact to Update
  **/
  public static contact constructContactRecord(Map<String, Object> addressObject, Id recordId){
    contact thisContact = new Contact();
    thisContact.Id = recordId;
    if(addressObject.containsKey('Street') && !String.isEmpty(String.valueOf(addressObject.get('Street')))){
      thisContact.MailingStreet = String.valueOf(addressObject.get('Street'));
    }
    else{
      thisContact.MailingStreet = null;
    }
    if(addressObject.containsKey('City') && !String.isEmpty(String.valueOf(addressObject.get('City')))){
      thisContact.MailingCity = String.valueOf(addressObject.get('City'));
    }
    else{
      thisContact.MailingCity = null;
    }
    if(addressObject.containsKey('PostalCode') && !String.isEmpty(String.valueOf(addressObject.get('PostalCode')))){
      thisContact.MailingPostalCode = String.valueOf(addressObject.get('PostalCode'));
    }
    else{
      thisContact.MailingPostalCode = null;
    }
    if(addressObject.containsKey('State') && !String.isEmpty(String.valueOf(addressObject.get('State')))){
      thisContact.MailingState = String.valueOf(addressObject.get('State'));
    }
    else{
      thisContact.MailingState = null;
    }
    if(addressObject.containsKey('Country') && !String.isEmpty(String.valueOf(addressObject.get('Country')))){
      thisContact.MailingCountry = String.valueOf(addressObject.get('Country'));
    }
    else{
      thisContact.MailingCountry = null;
    }
    if(addressObject.containsKey('hed__Address_Type__c') && !String.isEmpty(String.valueOf(addressObject.get('hed__Address_Type__c')))){
      thisContact.hed__Primary_Address_Type__c = String.valueOf(addressObject.get('hed__Address_Type__c'));
    }
    else{
      thisContact.hed__Primary_Address_Type__c = null;

    }
    return thisContact;
  }
  /**
  * @description - Mapping the address field values with Account address field
  * @param Map<String Object> addressObject - Deserialized address Object 
  * @param Id recordId - account Id
  * @return Account - Account to Update
  **/
  public static Account constructAccountRecord(Map<String, Object> addressObject, Id recordId){
    Account thisAccount = new Account();
    thisAccount.Id = recordId;
    if(addressObject.containsKey('Street') && !String.isEmpty(String.valueOf(addressObject.get('Street')))){
      thisAccount.BillingStreet = String.valueOf(addressObject.get('Street'));
    }
    else{
      thisAccount.BillingStreet = null;
    }
    if(addressObject.containsKey('City') && !String.isEmpty(String.valueOf(addressObject.get('City')))){
      thisAccount.BillingCity = String.valueOf(addressObject.get('City'));
    }
    else{
      thisAccount.BillingCity = null;
    }
    if(addressObject.containsKey('PostalCode') && !String.isEmpty(String.valueOf(addressObject.get('PostalCode')))){
      thisAccount.BillingPostalCode = String.valueOf(addressObject.get('PostalCode'));
    }
    else{
      thisAccount.BillingPostalCode = null;
    }
    if(addressObject.containsKey('State') && !String.isEmpty(String.valueOf(addressObject.get('State')))){
      thisAccount.BillingState = String.valueOf(addressObject.get('State'));
    }
    else{
      thisAccount.BillingState = null;
    }
    if(addressObject.containsKey('Country') && !String.isEmpty(String.valueOf(addressObject.get('Country')))){
      thisAccount.BillingCountry = String.valueOf(addressObject.get('Country'));
    }
    else{
      thisAccount.BillingCountry = null;
    }
    return thisAccount;
  }

  public static void constructAddressRecord(Map<String, Object> addressObject, Id recordId, String relatedAPIName){
    hed__Address__c thisAddress = new hed__Address__c();
    thisAddress.put(relatedAPIName, recordId);
    thisAddress.hed__Default_Address__c = false;
    if(addressObject.containsKey('hed__Address_Type__c') && !String.isEmpty(String.valueOf(addressObject.get('hed__Address_Type__c')))){
      thisAddress.hed__Address_Type__c = String.valueOf(addressObject.get('hed__Address_Type__c'));
    }
    if(addressObject.containsKey('Address1') && !String.isEmpty(String.valueOf(addressObject.get('Address1')))){
      thisAddress.hed__MailingStreet__c = String.valueOf(addressObject.get('Address1'));
    }
    if(addressObject.containsKey('Address2') && !String.isEmpty(String.valueOf(addressObject.get('Address2')))){
      thisAddress.hed__MailingStreet2__c = String.valueOf(addressObject.get('Address2'));
    }
    if(addressObject.containsKey('City') && !String.isEmpty(String.valueOf(addressObject.get('City')))){
      thisAddress.hed__MailingCity__c = String.valueOf(addressObject.get('City'));
    }
    if(addressObject.containsKey('PostalCode') && !String.isEmpty(String.valueOf(addressObject.get('PostalCode')))){
      thisAddress.hed__MailingPostalCode__c = String.valueOf(addressObject.get('PostalCode'));
    }
    if(addressObject.containsKey('State') && !String.isEmpty(String.valueOf(addressObject.get('State')))){
      thisAddress.hed__MailingState__c = String.valueOf(addressObject.get('State'));
    }
    if(addressObject.containsKey('Country') && !String.isEmpty(String.valueOf(addressObject.get('Country')))){
      thisAddress.hed__MailingCountry__c = String.valueOf(addressObject.get('Country'));
    }
    insert thisAddress;

  }

  public class AddressWrapper{  
    //Stores the address value   
    @AuraEnabled
    public String label {get;set;}    
    // Stores the Address JSON String
    @AuraEnabled
    public String value {get;set;}
    AddressWrapper(String address, ct_AddressVerificationParser.Item  thisItem){
      label = address;
      value = JSON.serialize(thisItem);
    }
  }
}