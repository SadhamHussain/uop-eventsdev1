/*****************************************************************
* bg_ContactUtils_Test
*
* Test class for bg_ContactUtils
*
* Author: Lizzy Baird
* Created: 02-08-2019
******************************************************************/

@isTest

private class bg_ContactUtils_Test 
{
    @TestSetup
    private static void testDataSetup()
    {
        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
		tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Application_Processing_TDTM', 
													 'Application__c', 'BeforeInsert;AfterInsert;BeforeUpdate;AfterUpdate', 1.00));
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Contact_AppProcessing_TDTM', 
													 'Contact', 'AfterInsert;BeforeUpdate', 1.00));                                         
		
        hed.TDTM_Global_API.setTdtmConfig(tokens);
    }
    
    static testMethod void testUpdateContactKeyFieldsSetsFieldsFromApplication()
    {
        Contact testContact = bg_UnitTestDataFactory.createApplicantContact();
        insert testContact;
        
        Application__c application = new Application__c(Applicant__c = testContact.Id, Date_Of_Birth__c = Date.today(), Student_Gender__c = 'Male',
                                                     Address_Type__c = 'Home', Qualification_1__c = 'GCSE');
        
        test.startTest();
        Contact resultContact = bg_ContactUtils.updateContactKeyFields(application);
        test.stopTest();
        
        system.assertEquals(testContact.Id, resultContact.Id);
        system.assertEquals(Date.today(), resultContact.Birthdate);
        system.assertEquals('Male', resultContact.hed__Gender__c);
        system.assertEquals('GCSE', resultContact.Qualification_1__c);
        system.assertEquals('Home', resultContact.hed__Primary_Address_Type__c);
    }
    
    static testMethod void testUpdateContactLinkedToUpdatedApplicationSetsContactFirstNameWhenUpdatedOnApplication()
    {
        Contact testContact = bg_UnitTestDataFactory.createApplicantContact();
        insert testContact;
        System.debug('contact before update ' + testContact);
        
        Application__c testApplication = new Application__c(First_Name__c = 'firstname old', Last_Name__c = 'last1' , Email_Address__c = 'last1@abc.com', Applicant__c = testContact.Id);
        Application__c testNewApplication = new Application__c(First_Name__c = 'new for test', Last_Name__c = 'last2' , Email_Address__c = 'last2@abc.com', Applicant__c = testContact.Id);

        Map<Id, Application__c> applicationsById = new Map<ID, Application__c>();
        applicationsById.put(testApplication.Id, testApplication);
        
        test.startTest();
        bg_ContactUtils.updateContactLinkedToUpdatedApplication(new List<Application__c>{testNewApplication}, applicationsById);
        test.stopTest();
        
        List<Contact> resultContacts = [SELECT Id, FirstName FROM Contact WHERE FirstName = 'new for test'];
        system.assertEquals(1, resultContacts.size());
        
    }
    
    static testMethod void testUpdateContactFieldsUpdatesAddressFieldsIfChangedOnApplication()
    {
        Contact testContact = bg_UnitTestDataFactory.createApplicantContact();
        insert testContact;
        
        string addressString = 'this is a really long address, a really very long address, can you imagine a longer address than this one, so so so so so long, long long, ';
            addressString += ' very long address wow, this is a really long address, a really very long address, can you imagine a longer address than this one ';
        
        Application__c testApplication = new Application__c(Address__c = 'address' , Applicant__c = testContact.Id, Origin__c = 'SITS');
        Application__c testNewApplication = new Application__c(Address__c = addressString, Applicant__c = testContact.Id, Origin__c = 'SITS');
        
        Map<Id, Application__c> applicationsById = new Map<ID, Application__c>();
        applicationsById.put(testApplication.Id, testApplication);
        
        test.startTest();
        bg_ContactUtils.updateContactLinkedToUpdatedApplication(new List<Application__c>{testNewApplication}, applicationsById);
        test.stopTest();
        
        string expectedAddress = 'this is a really long address, a really very long address, can you imagine a longer address than this one, so so so so so long, long long,  very long address wow, this is a really long address, a really very long address';
        
        List<Contact> resultContacts = [SELECT Id FROM Contact WHERE MailingStreet =: expectedAddress];
        List<Contact> result2Contacts = [SELECT Id, MailingStreet FROM Contact];
        system.debug(result2Contacts[0]);
                
        system.assertEquals(1, resultContacts.size());
    }
}