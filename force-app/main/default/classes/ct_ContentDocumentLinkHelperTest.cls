/**
 * @File Name          : ct_ContentDocumentLinkHelperTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 17/4/2020, 6:27:04 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/19/2020   Creation Admin     Initial Version
**/

@isTest
public class ct_ContentDocumentLinkHelperTest {
  /**
  * @description to test attachment to opportunity
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @isTest(SeeAllData=true)
  public static void addAttachmentsToChildOppTest(){
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    Test.startTest();
    npe03__Recurring_Donation__c thisDonation = ct_TestDataFactory.createRecurringDonation(thisContact, null, 100.00);
    insert thisDonation;

    Blob pdfSummary =  Blob.valueOf('Test Invoice');            
    ContentVersion thisContentver   = new ContentVersion();
    thisContentver.ContentLocation  = 'S';
    thisContentver.VersionData      = pdfSummary;
    thisContentver.Title            = 'Test PDF';
    thisContentver.PathOnClient     = '.pdf';
    insert thisContentver;

    ContentDocumentLink thisLink    = new ContentDocumentLink();
    thisLink.ContentDocumentId      = [Select Id,ContentDocumentId FROM ContentVersion WHERE Id =:thisContentver.Id].ContentDocumentId;
    thisLink.LinkedEntityId         = thisDonation.Id;
    thisLink.ShareType              = 'I';
    insert thisLink;
    Test.stopTest();
    List<Opportunity> opportunityList = [SELECT Id, npe03__Recurring_Donation__c 
                                                FROM Opportunity
                                                WHERE npe03__Recurring_Donation__c =: thisDonation.Id];
    System.debug('opportunityList.size()'+ opportunityList.size());
    System.assertEquals(1, opportunityList.size(), 'Should return 1');
    List<ContentDocumentLink> contentDocumentLinkList = [SELECT Id, LinkedEntityId 
                                                                FROM ContentDocumentLink
                                                                WHERE LinkedEntityId =: opportunityList[0].Id ];
    System.assertEquals(1, contentDocumentLinkList.size(), 'Should return 1');

  }
}