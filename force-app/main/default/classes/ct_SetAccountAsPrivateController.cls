/**
 * @File Name          : ct_SetAccountAsPrivateController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 29/6/2020, 7:51:10 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/6/2020   Creation Admin     Initial Version
**/
public class ct_SetAccountAsPrivateController {
  /**
  * @description Update related Account as Private
  * @author Creation Admin | 24/6/2020 
  * @param contactId 
  * @return boolean 
  **/
  @AuraEnabled
  public static Boolean setAccountAsPrivate(Id contactId, Boolean isPrivate){
    Id accountId;
    List<hed__Address__c> addressListToUpdate = new List<hed__Address__c>();

    for(Contact thisContact : [SELECT Id, AccountId 
                                      FROM Contact 
                                      WHERE Id = :contactId
                                      AND Account.RecordTypeId = :ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE]){
      accountId = thisContact.AccountId;
    }

    //Set Account As Private
    if(accountId != null){
      Account thisAccount = new Account();
      thisAccount = [SELECT Id, AAA_Private_Record__c FROM Account WHERE Id =:accountId];
      thisAccount.AAA_Private_Record__c = isPrivate;

      try{
        update thisAccount;
      }
      catch(Exception e){
        //Exception mechanism to log any exceptions occured during the dml operation
        ct_Logger.logMessage(e, 'Account');
        throw e;
      }
    }

    return true;
  }

  /**
  * @description Get Initial related Account private/not private status 
  * @author Creation Admin | 29/6/2020 
  * @param contactId 
  * @return Boolean 
  **/
  @AuraEnabled
  public static Boolean getInitialAccountPrivacyStatus(Id contactId){
    return [SELECT Id, AAA_Private_Record__c FROM Account
                    WHERE Id =: [SELECT AccountId FROM Contact WHERE Id =:contactId].AccountId].AAA_Private_Record__c;
  }
}