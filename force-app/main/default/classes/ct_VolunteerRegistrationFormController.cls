/**
 * @File Name          : ct_VolunteerRegistrationFormController.cls
 * @Description        : 
 * @Author             : Javid Sherif
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 8/5/2020, 7:52:06 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   Javid Sherif     Initial Version
**/
public with sharing class ct_VolunteerRegistrationFormController {

  /**
  * @description get metadata record to construct field mappings
  * @author Creation Admin | 17/4/2020 
  * @param formName 
  * @return MetadataWrapper 
  **/
  @AuraEnabled
  public static MetadataWrapper fetchMetadataRecords(){
    //Get field description for the Contact object
    Map<String,Schema.SObjectField> contactSobjectFielsMap = ct_WebDataFormHelper.getSojectFields('Contact');
    //Get field description for the Opportunity object
    Map<String,Schema.SObjectField> opportunitySobjectFielsMap = ct_WebDataFormHelper.getSojectFields('Opportunity');
    //Picklist map to hold picklist entries and key to get the value
    Map<String, List<PicklistEntryWrapper>> picklistMap = new  Map<String, List<PicklistEntryWrapper>>();
    //Metadata wrapper to hold field volunteer information metadata records
    MetadataWrapper thisWrapper = new MetadataWrapper();

    //Loop through the field information metadata records & construct wrapper record
    for(Webform_Data__mdt thisVolunteerMetadata : ct_WebDataFormHelper.getWebFormMetadataRecords(ct_Constants.WEB_SUBMISSION_SOURCE_VOLUNTEER)){
      //Add volunteer field metadata information to metadata records wrapper
      thisWrapper.volunteerMetadataRecords.add(thisVolunteerMetadata);

      //Construct Picklist entries for Contact object
      if(contactSobjectFielsMap.containskey(thisVolunteerMetadata.SObject_Field__c)
      && (String.valueOf(contactSobjectFielsMap.get(thisVolunteerMetadata.SObject_Field__c).getDescribe().getType()) == 'PICKLIST'
      || String.valueOf(contactSobjectFielsMap.get(thisVolunteerMetadata.SObject_Field__c).getDescribe().getType()) == 'MultiPicklist')){
        picklistMap.put(thisVolunteerMetadata.SObject_Field__c, constructPicklistEntryWrapper(ct_WebDataFormHelper.getPicklistValues(thisVolunteerMetadata.SObject__c, thisVolunteerMetadata.SObject_Field__c)));
      }
      //Construct Picklist entries for Opportunity object
      else if(opportunitySobjectFielsMap.containskey(thisVolunteerMetadata.SObject_Field__c)
      && (String.valueOf(opportunitySobjectFielsMap.get(thisVolunteerMetadata.SObject_Field__c).getDescribe().getType()) == 'PICKLIST'
      || String.valueOf(opportunitySobjectFielsMap.get(thisVolunteerMetadata.SObject_Field__c).getDescribe().getType()) == 'MultiPicklist')){
        picklistMap.put(thisVolunteerMetadata.SObject_Field__c, constructPicklistEntryWrapper(ct_WebDataFormHelper.getPicklistValues(thisVolunteerMetadata.SObject__c, thisVolunteerMetadata.SObject_Field__c)));
      }
    }

    thisWrapper.picklistFields = picklistMap;
    return thisWrapper;
  }

  /**
  * @description construct picklist entries based on metadata
  * @author Creation Admin | 3/5/2020 
  * @param picklistEntryList 
  * @return List<PicklistEntryWrapper> 
  **/
  public static List<PicklistEntryWrapper> constructPicklistEntryWrapper(List<Schema.PicklistEntry> picklistEntryList){
    List<PicklistEntryWrapper> thisPickListWrapperList        = new List<PicklistEntryWrapper>();

    //Loop through the list picklist values provided to construct picklist options
    for( Schema.PicklistEntry thisPicklistEntry : picklistEntryList){
      thisPickListWrapperList.add(new PicklistEntryWrapper(thisPicklistEntry.getLabel(), thisPicklistEntry.getValue()));
    }
    return thisPickListWrapperList;
  }

  /**
  * @description - Method to save Contact Details
  * @param String volunteerDetails - JSON contact Object
  * @return void 
  **/
  @AuraEnabled
  public static void saveVolunteerDetails(String volunteerDetails){
    try{
      //Deserialize the JSON string to map object & string get object values
      Map<String, Object> volunteerObject = (Map<String, Object>)JSON.deserializeUntyped(volunteerDetails);

      //create a instance for Web Site Submission with web form information 
      Web_Site_Submission__c webSubmissionObject = new Web_Site_Submission__c();
      webSubmissionObject.Name                   = 'Volunteer Registration - '+String.valueOf(volunteerObject.get('FirstName'))+String.valueOf(volunteerObject.get('LastName'));
      webSubmissionObject.Source__c				       = ct_Constants.WEB_SUBMISSION_SOURCE_VOLUNTEER;
      webSubmissionObject.Payload__c             = volunteerDetails;
      insert webSubmissionObject;    
      
      //Append Web Site Site submission record id to the JSON string to use when inserting Attachment 
      String volunteerDetailsString = volunteerDetails.removeEnd('}');
      volunteerDetailsString = volunteerDetailsString +',"webSiteSubmissionId":"'+webSubmissionObject.Id+'"}';
      
      //Invoke Platform Event
      triggerPlatformEvent(volunteerDetailsString);
    }
    catch (Exception e) {
      //Exception mechanism to log any exceptions occured during the dml operation
      ct_Logger.logMessage(e, 'Web Site Submission');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }
  
  /**
  * @description - Method to publish platform event
  * @param String volunteerDetails - JSON Contact Object
  * @return void 
  **/
  private static void triggerPlatformEvent(String volunteerDetails){
    //Create instance of Web Site Submission Event to fire the Platform Event
    Web_Site_Submission_Event__e eventObject 	 = new Web_Site_Submission_Event__e();
    eventObject.Web_Site_Submission_Record__c = volunteerDetails;
    eventObject.Source__c                     = ct_Constants.WEB_SUBMISSION_SOURCE_VOLUNTEER;
    
    //Publish the event after adding web form details
    Database.SaveResult result = EventBus.publish(eventObject);
    if (result.isSuccess()) {
      System.debug('Successfully published event.');
    } 
    else {
      for(Database.Error err : result.getErrors()) {
        System.debug('Error returned: ' +err.getStatusCode() +' - ' +err.getMessage());
      }
    }       
  }

  /**
    * Wrapper that contains webform metadata records and Picklist Values
  */
  public class MetadataWrapper{
    //get set list to store field information metadata record
    @AuraEnabled
    public List<Webform_Data__mdt> volunteerMetadataRecords {get;set;}
    //get set list to store picklist entries
    @AuraEnabled
    public Map<String, List<PicklistEntryWrapper>> picklistFields {get;set;}
    
    MetadataWrapper(){
      volunteerMetadataRecords = new List<Webform_Data__mdt>();
      picklistFields           = new  Map<String, List<PicklistEntryWrapper>>();
    }
  }

  /**
    * Wrapper that contains Donation Object wrapper and Picklist Values
  */
  public class PicklistEntryWrapper{
    //get set string to store lable of picklist entry
    @AuraEnabled
    public String label                  {get;set;}
    //get set string to store value of picklist entry
    @AuraEnabled
    public string value                  {get;set;}
    
    public PicklistEntryWrapper(String entryLabel,String entryValue){
      this.label    = entryLabel;
      this.value    = entryValue;
    }
  }
}