/*
 * rl_SessionAssignmentUtils_Test
 * 
 * Tests interim process for events with sessions. 1 positive assertion test, 1 negative assertion test.
 *
 * Needs to use seeAllData because of an EventBrite trigger on Opportunity which needs access to a custom setting or custom metadata.
 *
 * There must be a metadata record (AED_Course_Session_Map__mdt) with Course Name "Test Course" and Session Name "Test Session" for this to pass.
 *
 * Batch size is set to 50 as otherwise contact creation fails..
 * 
 * Tested Class: rl_SessionAssignmentUtils 
 * 
 * Created: 19th February 2020
 * **************** Version history ***********************
 * CT, Umashankar : 10/07/2020 - Test class Error fixed(CPU Time Limit) by introducing recordSize variable with value 25 
 * ******************************************************************/

@IsTest (seeAllData=true)
public class rl_SessionAssignmentUtils_Test 
{ 
  //CT, Umashankar : Introduced a variable to reduce the record size to 25 to avoid time limit exception
    static Integer recordSize = 25;

    // Positive Test - Ensures session matches
    @isTest 
    public static void testPopulateSessions() 
    {  
        
        // Dummy CRON expression: midnight on March 15 2026
       String CRON_EXP = '0 0 0 15 3 ? 2026';

        
        
        // Insert 25 Contacts
        List<Contact> contactList = new List<Contact>();
        
        for(Integer i=0; i<recordSize; i++)
            
        {
            Contact newContact = new Contact();
            
            newContact.FirstName = 'A' + i;
            newContact.LastName = 'B' + i;
            newContact.Email = 'test@test.com' + i;    
            
            contactList.add(newContact);
        }
		// Run test
        Test.StartTest();
        
        insert contactList;
        
        
        // Insert an Event 
        Event__c newEvent = new Event__c();
        
        newEvent.Name = Label.BatchEventName;
        
        insert newEvent;
        
        
        // Insert a Session
        Session__c newSession = new Session__c();
        
        newSession.Name = 'Test Session';
        newSession.Event__c = newEvent.Id;    
        
        insert newSession;
        
        
        // Insert 2 courses
        List<Account> courseList = new List<Account>();
        
			Account newCourse1 = new Account();
            
            newCourse1.Name = 'Test Course';
            newCourse1.RecordTypeId = '0121r000000tAqRAAU';
            newCourse1.Academic_Programme_Display_Course_Name__c = 'Test Course';
            
            courseList.add(newCourse1);
        
        
			Account newCourse2 = new Account();
            
            newCourse2.Name = 'Different Test Course';
            newCourse2.RecordTypeId = '0121r000000tAqRAAU';
            newCourse2.Academic_Programme_Display_Course_Name__c = 'Different Test Course';
            
            courseList.add(newCourse2);        
        
        insert courseList;
        
        
        // Add all Contacts to the Event
        List<Event_Registration__c> eventRegistrationList = new List<Event_Registration__c>();
        
        for( Contact contact : contactList )
        {
            Event_Registration__c newEventRegistration = new Event_Registration__c();
            
            newEventRegistration.Contact__c = contact.Id;
            newEventRegistration.Event__c = newEvent.Id;
            
            eventRegistrationList.add(newEventRegistration);
        }
        
        insert eventRegistrationList;
        
        
        // Create Opportunity (same course) for all Contacts except 1
        List<Opportunity> opportunityList = new List<Opportunity>();
        
        for(Integer i=0; i<recordSize - 1; i++)
            
        {
            Opportunity newOpportunity = new Opportunity();
            
            newOpportunity.Name = 'AB' + i;
            newOpportunity.Course__c = courseList[0].Id;
            newOpportunity.StageName = 'Offered';
            newOpportunity.CloseDate = System.today();
            newOpportunity.Applicant__c = contactList[i].Id;
            
            opportunityList.add(newOpportunity);
        }
        
        // For final contact, associate with an Opportunity with a different course     
        Opportunity diffOpportunity = new Opportunity();
        
        diffOpportunity.Name = 'Different Opportunity';
        diffOpportunity.Course__c = courseList[1].Id;
        diffOpportunity.StageName = 'Offered';
        diffOpportunity.CloseDate = System.today();
        diffOpportunity.Applicant__c = contactList[9].Id;   
        
        opportunityList.add(diffOpportunity);    
        
        insert opportunityList;
        
        
        // Create an Applicant Admission Cycle for all Contacts
        List<Applicant_Admission_Cycle__c> aacList = new List<Applicant_Admission_Cycle__c>();
        
        for(Integer i=0; i<recordSize; i++)
            
        {
            Applicant_Admission_Cycle__c newAAC = new Applicant_Admission_Cycle__c();
            
            newAAC.Admission_Cycle__c = Label.BatchEntryYear;
            newAAC.BestOffer__c = opportunityList[i].Id;
            newAAC.Applicant__c = contactList[i].Id;
            
            aacList.add(newAAC);
            
        }
        
        insert aacList;
        
        
        // Get the IDs of the opportunities we just inserted
        Map<Id, Event_Registration__c> eventRegMap = new Map<Id, Event_Registration__c>(eventRegistrationList);
        
        List<Id> eventRegIds = new List<Id>(eventRegMap.keySet());
        
        
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new rl_SessionAssignmentUtils());
        
        // Verify the scheduled job has not run yet.
        List<Task> lt = [SELECT Id 
            FROM Task 
            WHERE WhatId IN :eventRegIds];
        
        System.assertEquals(0, lt.size(), 'Tasks exist before job has run');
        
        // Stopping the test will run the job synchronously
        Test.stopTest();
        
        
        // Get one of the updated Event Registrations
        Event_Registration__c postiveTest = [ SELECT Id, Session__c 
                                              FROM Event_Registration__c 
                                              WHERE Id = :eventRegistrationList[0].Id 
                                            ];
        
        // Ensure the session has matched successfully
        system.assertEquals(postiveTest.Session__c, newSession.Id);     
        
        
        //Get the Event Registration that shouldn't have a session
        Event_Registration__c negativeTest = [ SELECT Id, Session__c 
                                               FROM Event_Registration__c 
                                               WHERE Id = :eventRegistrationList[recordSize-1].Id 
                                             ];
        
        // Ensure the session field is still null
        system.assertEquals(negativeTest.Session__c, null);  
        
    }      
    
}