/*****************************************************************
* bg_Contact_Conversion_TDTM
*
* Contact Trigger Handler for reparenting child objects on Conversion - AfterInsert
* 
* Author: Mike Jones
* Created: 01-08-2019
******************************************************************/
global class bg_Contact_Conversion_TDTM extends hed.TDTM_Runnable {
    
    public override DmlWrapper run(List<SObject> newlist, List<SObject> oldlist, 
    Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {
        System.Debug('### ASDF In bg_Contact_Conversion_TDTM');
        if (triggerAction == Action.AfterInsert)
        {
            List<Contact> newContacts = newList;
            bg_ContactUtils.processUpdatedContactsAfterInsert(newContacts);
        }
System.Debug('### ASDF Out bg_Contact_Conversion_TDTM');
        return null;
    }
}