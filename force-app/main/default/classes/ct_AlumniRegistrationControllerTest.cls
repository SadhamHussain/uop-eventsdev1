/**
 * @File Name          : ct_AlumniRegistrationControllerTest.cls
 * @Description        : Test Class for ct_AlumniRegistrationController class
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 15/6/2020, 1:25:09 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/5/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_AlumniRegistrationControllerTest {
  
  /**
  * @description to test picklist entries returned
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @isTest
  public static void picklistValuesMethodTest(){
    Webform__mdt alumniForm = new Webform__mdt();
    alumniForm.MasterLabel = 'Alumni Registration';
    alumniForm.DeveloperName = 'Alumni_Registration';

    List<Webform_Data__mdt> metaDataList = new List<Webform_Data__mdt>();
    metaDataList.add(new Webform_Data__mdt(MasterLabel = 'Alumni Salutation', Form_Field_Name__c = 'Alumni Salutation', SObject__c = 'Contact',
    SObject_Field__c  = 'Salutation', Webform__c = alumniForm.Id, Web_Label__c='Title', Web_Placeholder__c ='Select  your Title'));
    
    Test.startTest();
    ct_AlumniRegistrationController.MetadataWrapper thisWrapper = ct_AlumniRegistrationController.fetchMetadataRecords();
    Test.stopTest();
  }

  /**
  * @description to test submission of form data
  * @author Creation Admin | 17/4/2020 
  * @return void 
  **/
  @isTest(SeeAllData=true)
  public static void saveWebSiteSubmissionRecordTest(){
    Lead thisLead = ct_TestDataFactory.createLead();
    insert thisLead;

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;


    String alumniObject = '{'+
    '"Alumni_Interest__c": "'+ct_Constants.ALUMNI_WILLING_TO_GIFT+'",'+
    '"LastName": "Alumni",'+
    '"Birthdate": "2020-03-04",'+
    '"Salutation": "Mr.",'+
    '"Course__c": null,'+
    '"Email": "test@test.com",'+
    '"hed__AlternateEmail__c": "test@test.com",'+
    '"Phone": "12345678",'+
    '"hed__MailingPostalCode__c": "W4 4PH",'+
    '"MailingStreet": "Barley Mow Passage",'+
    '"MailingCity": "London",'+
    '"hed__MailingState__c": "chiswick",'+
    '"MailingState": "chiswick",'+
    '"hed__MailingCity__c": "London",'+
    '"MobilePhone": "12345678",'+
    '"hed__MailingStreet__c": "Barley Mow Passage",'+
    '"MailingPostalCode": "W4 4PH",'+
    '"FirstName": "Test",'+
    '"MiddleName": null,'+
    '"hed__MailingCountry__c": "United Kingdom",'+
    '"MailingCountry": "United Kingdom"'+
    '}';
    Test.startTest();
      Id [] fixedSearchResults= new Id[2];
      fixedSearchResults[0] = thisLead.Id;
      fixedSearchResults[1] = thisContact.Id;
      // To confine the results to be test covered under a FIND statement
      System.Test.setFixedSearchResults(fixedSearchResults);
      
      ct_AlumniRegistrationController.saveContactDetails(alumniObject);
    Test.stopTest();

  }
}