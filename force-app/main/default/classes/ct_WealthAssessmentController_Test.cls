/**
 * @File Name          : ct_WealthAssessmentController_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 24/4/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/4/2020   Creation Admin    Initial Version
**/

 /**
  * @description Test Opportunity Updation With Wealth Assesment Record 
  * @author Creation Admin | 24/4/2020 
  * @return void 
  **/
@IsTest
public class ct_WealthAssessmentController_Test {
  @isTest(SeeAllData=true)
  public static void testOpportunityUpdate(){
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    
    //Create Wealth Assessment
    Wealth_Assessment__c thisWealthAssessment = new Wealth_Assessment__c();
    thisWealthAssessment.Name = 'Javid Wealth Assessment';
    thisWealthAssessment.Capacity_Score__c = 'D - £200K';
    thisWealthAssessment.Date_Of_Wealth_Screen__c = system.today();
    thisWealthAssessment.Property_Indicator__c = 'B: £.5m - £3m';
    thisWealthAssessment.Wealth_Indicator__c = 'B: £25m - £50m';
    thisWealthAssessment.Contact__c = thisContact.Id;
    insert thisWealthAssessment;
        
    Opportunity thisOpportunity = ct_TestDataFactory.createVolunteerOpportunity(thisContact);
    thisOpportunity.StageName = 'Identification';
    thisOpportunity.RecordTypeId = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Major_Donation');
    insert thisOpportunity;

    Test.startTest();
      ct_WealthAssessmentComponentController.getWealthAssessmentRecord(thisWealthAssessment.Id, thisOpportunity.Id);
    Test.stopTest();
  }  
    
     /**
  * @description Test Opportunity Clearing The Wealth Assesment Values 
  * @author Creation Admin | 24/4/2020 
  * @return void 
  **/
   @isTest(SeeAllData=true)
  public static void testOpportunityClearing(){
      
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
      
    Opportunity thisOpportunity = ct_TestDataFactory.createVolunteerOpportunity(thisContact);
    thisOpportunity.StageName = 'Identification';
    thisOpportunity.RecordTypeId = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Major_Donation');
    insert thisOpportunity;
      
    Test.startTest();
      ct_WealthAssessmentComponentController.clearWealthAssessmentValuesFromOpportunity( thisOpportunity.Id);
    Test.stopTest();
  }
}