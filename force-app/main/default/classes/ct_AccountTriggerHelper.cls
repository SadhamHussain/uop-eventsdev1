/**
 * @File Name          : ct_AccountTriggerHelper.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 25/6/2020, 4:03:20 pm
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    6/5/2020   Creation Admin     Initial Version
**/
public class ct_AccountTriggerHelper {
  
  

  /**
  * @description Set Related Contact's Address records to Private/Not private based on Account record
  * @author Creation Admin | 25/6/2020 
  * @param accountNewMap 
  * @return void 
  **/
  public static void setRelatedAddressToPrivate(Map<Id, Account> accountNewMap){
    List<hed__Address__c> addressListToUpdate = new List<hed__Address__c>();

    for(hed__Address__c thisAddress : [SELECT Id, hed__Parent_Contact__c, AAA_Private_Record__c,
                                              hed__Parent_Contact__r.AccountId 
                                              FROM hed__Address__c
                                              WHERE hed__Parent_Contact__r.AccountId IN: accountNewMap.keySet()]){
      if(accountNewMap.containsKey(thisAddress.hed__Parent_Contact__r.AccountId)){
        thisAddress.AAA_Private_Record__c = accountNewMap.get(thisAddress.hed__Parent_Contact__r.AccountId).AAA_Private_Record__c;
        addressListToUpdate.add(thisAddress);
      }       
    }

    try{
      update addressListToUpdate;
    }
    catch(Exception e){
      //Exception mechanism to log any exceptions occured during the dml operation
      ct_Logger.logMessage(e, 'Account');
      throw e;
    }
  }
}