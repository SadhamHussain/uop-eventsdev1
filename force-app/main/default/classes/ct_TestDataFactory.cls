/**
 * @File Name          : ct_TestDataFactory.cls
 * @Description        : Provides reusable methods for test data to be used for the same type of data in multiple test classes.
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 24/6/2020, 8:07:59 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/16/2019   Creation Admin     Initial Version
**/
@isTest
public class ct_TestDataFactory {
  public static Lead createStudentLead(){
    Lead thisLead = new lead();
    thisLead.RecordTypeId   = ct_Util.getRecordTypeIdByDevName('Lead', 'Student');
    thisLead.FirstName      = 'Creation';
    thisLead.LastName       = 'Technology';
    thisLead.Company        = 'Creation';
    thisLead.Email          = 'CreationTechnology@creation.com';
    thisLead.Status         = 'New - Unverified';
    return thisLead;
  }

  /**
  * @description create Lead Test Record
  * @author Creation Admin | 17/4/2020 
  * @return Lead 
  **/
  public static Lead createLead(){
    Lead thisLead = new lead();
    thisLead.FirstName      = 'Creation';
    thisLead.LastName       = 'Technology';
    thisLead.Company        = 'Creation';
    thisLead.Email          = 'CreationTechnology@creation.com';
    thisLead.Status         = 'New - Unverified';
    return thisLead;
  }

   /**
   * @description Create Contact Test Record
   * @author Creation Admin | 17/4/2020 
   * @return Contact 
   **/
   public static Contact createContact(){
    Contact thisContact                   = new Contact();
    thisContact.FirstName                 = 'Creation';
    thisContact.LastName                  = 'Technology';
    thisContact.hed__AlternateEmail__c    = 'CreationTechnology@creation.com';
    thisContact.MailingCountry            = 'United Kingdom';
    thisContact.MailingStreet             = '10 Barley Mow Passage';
    thisContact.MailingCity               = 'London';
    thisContact.MailingCountry            = 'United Kingdom';
    thisContact.hed__Preferred_Email__c   = ct_Constants.CONTACT_PREFERRED_EMAIL_TYPE_ALTERNATE;
    return thisContact;
  }

  /**
  * @description Create Project Test Record
  * @author Creation Admin | 17/4/2020 
  * @return Project__c 
  **/
  public static Project__c createProject(){
    Project__c thisProject                = new Project__c();
    thisProject.Name                      = 'Creation Fundraising Project';
    thisProject.Project_Description__c    = 'Test Fundraising Project';
    thisProject.Project_Status__c         = ct_Constants.FUNDRASING_PROJECT_IN_PROGRESS;
    return thisProject;
  }
  /**
  * @description Create Project Task Test Record
  * @author Creation Admin | 19/5/2020 
  * @return Project_Task__c 
  **/
	public static Project_Task__c createProjectTask(Id projectId){
		Project_Task__c thisTask                = new Project_Task__c();
		thisTask.Name                     		= 'Creation Fundraising Project Task - 1';
		thisTask.Project_Task_Description__c    = 'Test Fundraising Project Task';
		thisTask.Project__c    					= projectId;
		thisTask.Project_Task_Status__c         = ct_Constants.PROJECT_TASK_STATUS_ACTIVE;
		thisTask.Priority__c         			= ct_Constants.PROJECT_TASK_PRIORITY_LOW;
		thisTask.Start_Date__c       			= System.Today();
		thisTask.End_Date__c         			= System.Today()+10;
		thisTask.Estimated_Hours__c         	= 20;
		thisTask.Estimated_Expense__c          	= 200;
		return thisTask;
	}
    /**
  * @description Create Project Time Test Record
  * @author Creation Admin | 19/5/2020 
  * @return Project_Time__c 
  **/
	public static Project_Time__c createProjectTime(Id projectId,Id projectTaskId){
		Project_Time__c    thisTime  = new Project_Time__c();
		thisTime.Description__c    = 'Test Fundraising Project Time';
		thisTime.Project__c    = projectId;
		thisTime.Project_Task__c     = projectTaskId;
		thisTime.Date_Incurred__c         = System.Today();
		thisTime.Hours__c         = 10;
		return thisTime;
	}
	/**
  * @description Create Project Expense Test Record
  * @author Creation Admin | 19/5/2020 
  * @return Project_Expense__c 
  **/
	public static Project_Expense__c createProjectExpense(Id projectId,Id projectTaskId){
		Project_Expense__c    thisExpense  = new Project_Expense__c();
		thisExpense.Description__c    = 'Test Fundraising Project Expense';
		thisExpense.Project__c    = projectId;
		thisExpense.Project_Task__c     = projectTaskId;
		thisExpense.Date_Incurred__c         = System.Today();
		thisExpense.Amount__c         = 100;
		return thisExpense;
	}
  /**
  * @description Create Faculty Recruitment Event
  * @author Creation Admin | 17/4/2020 
  * @return Event__c 
  **/
  public static Event__c createFacultyRecruitmentEvent(){
    Event__c thisStudentRecruitmentEvent              = new Event__c();
    thisStudentRecruitmentEvent.RecordTypeId          = ct_Util.getRecordTypeIdByDevName('Event__c', 'Faculty_Recruitment_and_Outreach_Event');
    thisStudentRecruitmentEvent.Name                  = 'Student Recruitment Event';
    thisStudentRecruitmentEvent.Event_Start_Date__c   = Date.today();
    thisStudentRecruitmentEvent.Event_End_Date__c     = Date.today() + 1;
    thisStudentRecruitmentEvent.Year_of_Entry__c      = '2022';
    return thisStudentRecruitmentEvent;
  }

  /**
  * @description Create Faculty Test Record
  * @author Creation Admin | 17/4/2020 
  * @param gauName 
  * @return npsp__General_Accounting_Unit__c 
  **/
  public static npsp__General_Accounting_Unit__c createGAURecord(String gauName){
    npsp__General_Accounting_Unit__c thisGAUObject= new npsp__General_Accounting_Unit__c();
    thisGAUObject.Name = gauName;
    thisGAUObject.npsp__Active__c = true;
    return thisGAUObject;
  }

  /**
  * @description Create Test GAU Allocation
  * @author Creation Admin | 17/4/2020 
  * @param GAUId 
  * @param OpportunityId 
  * @param allocationAmount 
  * @return npsp__Allocation__c 
  **/
  public static npsp__Allocation__c createGAUAllocation(Id GAUId, Id OpportunityId, Decimal allocationAmount ){
    npsp__Allocation__c thisGAUAllocation= new npsp__Allocation__c();
    thisGAUAllocation.npsp__General_Accounting_Unit__c = GAUId;
    thisGAUAllocation.npsp__Opportunity__c = OpportunityId;
    thisGAUAllocation.npsp__Opportunity__c = OpportunityId;
    thisGAUAllocation.npsp__Amount__c = allocationAmount;
    return thisGAUAllocation;
  }

  /**
  * @description create Test Educational Account
  * @author Creation Admin | 17/4/2020 
  * @param accountName 
  * @return Account 
  **/
  public static Account createEducationalAccount(String accountName){
    Account thisAccount = new Account();
    thisAccount.Name    = accountName;
    thisAccount.RecordTypeId = ct_Util.getRecordTypeIdByDevName('Account', 'Educational_Institution');
    return thisAccount;
  }

  /**
  * @description Create Test Course
  * @author Creation Admin | 17/4/2020 
  * @param courseName 
  * @param accountId 
  * @return hed__Course__c 
  **/
  public static hed__Course__c createCourse(String courseName, Id accountId){
    hed__Course__c thisCourse = new hed__Course__c();
    thisCourse.Name               = courseName;
    thisCourse.hed__Account__c    = accountId;
    return thisCourse;
  }

  /**
  * @description Create Test Term
  * @author Creation Admin | 17/4/2020 
  * @param accountId 
  * @return hed__Term__c 
  **/
  public static hed__Term__c createTerm(Id accountId){
    hed__Term__c thisTerm = new hed__Term__c();
    thisTerm.Name               = 'Test Term';
    thisTerm.hed__Account__c    = accountId;
    thisTerm.hed__Start_Date__c = System.today();
    thisTerm.hed__End_Date__c   = System.today();
    return thisTerm;
  }


  /**
  * @description Create User Record
  * @author Sadham Creation | 4/29/2020 
  * @return User 
  **/
  public static User createUser(){
    Profile systemAdminProfile = [SELECT Id FROM profile WHERE Name = 'System Administrator']; 
    String orgId        = UserInfo.getOrganizationId(); 
    String dateString   = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer RandomId    = Integer.valueOf(Math.rint(Math.random()*1000000)); 
    String uniqueName   = orgId+dateString+RandomId; 

    User thisUser = new User();
    thisUser.FirstName          = 'Creation';
    thisUser.LastName           = 'admin';
    thisUser.Email              = 'creation.technology@creation-tec.com';
    thisUser.UserName           = 'creationtechnology'+uniqueName+'@creation-tec.com';
    thisUser.ProfileId          = systemAdminProfile.Id;
    thisUser.EmailEncodingKey   = 'ISO-8859-1';
    thisUser.Alias              = uniqueName.substring(18, 23);
    thisUser.TimeZoneSidKey     = 'America/Los_Angeles';
    thisUser.LocaleSidKey       = 'en_US';
    thisUser.LanguageLocaleKey  = 'en_US';
    thisUser.IsActive           = true;
    return thisUser;  
  }

  /**
  * @description Create Account Team Member
  * @author Sadham Creation | 4/29/2020 
  * @param accountId 
  * @param userId 
  * @return AccountTeamMember 
  **/
  public static AccountTeamMember createAccountTeamMember(Id accountId, Id userId){
    AccountTeamMember thisUOPTeamMember         = new AccountTeamMember();
    thisUOPTeamMember.AccountId                 = accountId;
    thisUOPTeamMember.UserId                    = userId;
    thisUOPTeamMember.TeamMemberRole            = ct_Constants.ACCOUNT_TEAM_MEMBER_ROLE_CONTACT_MANAGER;
    thisUOPTeamMember.AccountAccessLevel        = ct_Constants.ACCOUNT_TEAM_MEMBER_READ_EDIT_ACCESS;
    thisUOPTeamMember.CaseAccessLevel           = ct_Constants.ACCOUNT_TEAM_MEMBER_READ_EDIT_ACCESS;
    thisUOPTeamMember.OpportunityAccessLevel    = ct_Constants.ACCOUNT_TEAM_MEMBER_READ_EDIT_ACCESS;
    return thisUOPTeamMember;
  }

  /**
  * @description Create Test Course Offering
  * @author Creation Admin | 17/4/2020 
  * @param courseId 
  * @param termId 
  * @return hed__Course_Offering__c 
  **/
  public static hed__Course_Offering__c createCourseOffering(Id courseId, Id termId){
    hed__Course_Offering__c thisOffering = new hed__Course_Offering__c();
    thisOffering.Name = 'Test Offering';
    thisOffering.hed__Course__c = courseId;
    thisOffering.hed__Term__c = termId;
    return thisOffering;
  }

  /**
  * @description Create Test Opportunity
  * @author Creation Admin | 17/4/2020 
  * @param thisContact 
  * @return Opportunity 
  **/
  public static Opportunity createDonationPledgeOpportunity(Contact thisContact){
    Opportunity thisOpportunity   = new Opportunity();
    thisOpportunity.Name          = 'Test '+thisContact.LastName+' Opportunity';
    thisOpportunity.StageName     = ct_Constants.NEW_PLEDGE_OPPORTUNITY_STAGE;
    thisOpportunity.CloseDate     = System.today();
    thisOpportunity.RecordTypeId  = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Donation');
    thisOpportunity.npsp__Primary_Contact__c = thisContact.Id;
    thisOpportunity.Amount = 100.00;
    thisOpportunity.npe01__Do_Not_Automatically_Create_Payment__c = true;

    return thisOpportunity;
  }

  /**
  * @description Create Test Opportunity Record
  * @author Creation Admin | 17/4/2020 
  * @param thisContact 
  * @return Opportunity 
  **/
  public static Opportunity createVolunteerOpportunity(Contact thisContact){
    Opportunity thisOpportunity   = new Opportunity();
    thisOpportunity.Name          = 'Test '+thisContact.LastName+' Opportunity';
    thisOpportunity.StageName     = ct_Constants.OPPORTUNITY_STAGE_NEW;
    thisOpportunity.CloseDate     = System.today();
    thisOpportunity.RecordTypeId  = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Volunteer_Recruitment');
    thisOpportunity.npsp__Primary_Contact__c = thisContact.Id;
    thisOpportunity.Amount = 100.00;
    thisOpportunity.npe01__Do_Not_Automatically_Create_Payment__c = true;

    return thisOpportunity;
  }  

  /**
  * @description Create Test Application Opportunity Record
  * @author Creation Admin | 17/4/2020 
  * @param thisContact 
  * @return Opportunity 
  **/
  public static Opportunity createApplicationOpportunity(){
    Opportunity thisOpportunity   = new Opportunity();
    thisOpportunity.Name          = 'Test Opportunity';
    thisOpportunity.StageName     = ct_Constants.OPPORTUNITY_STAGE_APPLIED;
    thisOpportunity.CloseDate     = System.today();
    thisOpportunity.RecordTypeId  = ct_Constants.OPPORTUNITY_RECORDTYPE_COURSE_ADMISSION;
    thisOpportunity.Amount = 100.00;
    return thisOpportunity;
  }  
    
  /**
  * @description Create Test GEW Payment
  * @author Creation Admin | 17/4/2020 
  * @param thisOpportunity 
  * @param paymentMethod 
  * @return npe01__OppPayment__c 
  **/
  public static npe01__OppPayment__c createGEMPayment(Opportunity thisOpportunity, String paymentMethod){
    npe01__OppPayment__c thisPayment = new npe01__OppPayment__c();
    thisPayment.npe01__Opportunity__c = thisOpportunity.Id;
    thisPayment.npe01__Payment_Method__c = paymentMethod;
    thisPayment.npe01__Payment_Amount__c = thisOpportunity.Amount;
    return thisPayment;
  }

  /**
  * @description Create Application
  * @return Application__c 
  **/
  public static Application__c createApplication(){
		Application__c creationApplication             = new Application__c();
    creationApplication.First_Name__c              = 'Creation';
    creationApplication.Last_Name__c               = 'Application';
    creationApplication.Preferred_Name__c          = 'Creation Test';
    creationApplication.Email_Address__c           = 'creation@tec.com';
    creationApplication.Phone_Number__c            = '0739357673';
    creationApplication.Postal_Code__c             = 'E2 8YS';
    creationApplication.Admission_Cycle__c         = 2020;
    creationApplication.Application_Year__c        = '2019';
    creationApplication.Domicile_Country_Code__c   = 'UK';
    creationApplication.Type__c                    = 'UCAS';
    creationApplication.Start_Date__c              = System.Today();
    creationApplication.Address__c                 = 'Barley Street, London W44ph,UK';
    return creationApplication;
  }

  /**
  * @description Create Test Authorization Record
  * @author Creation Admin | 17/4/2020 
  * @param thisContact 
  * @return asp04__Authorisation__c 
  **/
  public static asp04__Authorisation__c createAuthorisation(Contact thisContact){
    asp04__Authorisation__c thisAuthorisation = new asp04__Authorisation__c();
    thisAuthorisation.asp04__Payment_Route_Options__c = 'Direct Debit';
    thisAuthorisation.asp04__First_Name__c            = thisContact.FirstName;
    thisAuthorisation.asp04__Last_Name__c             = thisContact.LastName;
    thisAuthorisation.asp04__Email__c                 = thisContact.Email;
    thisAuthorisation.asp04__Status__c                = 'Pending';
    return thisAuthorisation;
  }
    
  /**
  * @description Create Test Recurring Donation Record
  * @author Creation Admin | 17/4/2020 
  * @param thisContact 
  * @param authorisationId 
  * @param amountValue 
  * @return npe03__Recurring_Donation__c 
  **/
  public static npe03__Recurring_Donation__c createRecurringDonation(Contact thisContact, Id authorisationId, Decimal amountValue){
    npe03__Recurring_Donation__c thisRecurringDonation = new npe03__Recurring_Donation__c();
    thisRecurringDonation.Asperato_Authorisation__c    = authorisationId;
    thisRecurringDonation.Name                         = thisContact.LastName;
    thisRecurringDonation.npe03__Contact__c            = thisContact.Id;
    thisRecurringDonation.npe03__Date_Established__c   = System.today();
    thisRecurringDonation.npe03__Amount__c             = amountValue;
    thisRecurringDonation.npe03__Installment_Period__c = ct_Constants.INSTALLMENT_PERIOD;
    return thisRecurringDonation;
  }

  /**
  * @description Create Test HEDA Affiliation
  * @author Creation Admin | 17/4/2020 
  * @param accountId 
  * @param contactId 
  * @return hed__Affiliation__c 
  **/
  public static hed__Affiliation__c createHedaAffiliation(Id accountId, Id contactId){
    hed__Affiliation__c thisAffiliation = new hed__Affiliation__c();
    thisAffiliation.hed__Contact__c	  = contactId;
    thisAffiliation.hed__Account__c   = accountId;
    return thisAffiliation;
  }

  /**
  * @description Create Test Program enrollment
  * @author Creation Admin | 4/17/2020 
  * @param Id accountId 
  * @param Id contactId 
  * @return hed__Program_Enrollment__c 
  **/
  public static hed__Program_Enrollment__c createHedaProgramEnrollment(Id accountId, Id contactId){
    hed__Program_Enrollment__c thisEnrollment = new hed__Program_Enrollment__c();
    thisEnrollment.hed__Contact__c	  = contactId;
    thisEnrollment.hed__Account__c    = accountId;
    return thisEnrollment;
  }

  
  /**
  * @description - Create Website Submission
  * @author Rajeshkumar  - Creation Technology Solutions | 27/4/2020 
  * @return Web_Site_Submission__c 
  **/
  public static Web_Site_Submission__c createWebSubmissions(){
    Web_Site_Submission__c thisSubmissions  = new Web_Site_Submission__c();
    thisSubmissions.Name	                  = 'Test WebSite Submissions';
    thisSubmissions.CreatedDate               = System.today()-10;
    return thisSubmissions;
  }

  /**
  * @description - Create Error Log
  * @author Rajeshkumar  - Creation Technology Solutions | 27/4/2020 
  * @return Error_Log__c 
  **/
  public static Error_Log__c createErrorLogs(){
    Error_Log__c thisErrorLogs      = new Error_Log__c();
    thisErrorLogs.Error_Detail__c   = 'Error Logs';
    thisErrorLogs.CreatedDate       = System.today()-40;
    return thisErrorLogs;
  }
    /**
    * @description - Create Volunteer Hours
    * @return GW_Volunteers__Volunteer_Hours__c 
    * @param Id jobId 
    * @param Id contactId
    **/
     public static GW_Volunteers__Volunteer_Hours__c createVolunteerHours(Id contactId, Id jobId){
          GW_Volunteers__Volunteer_Hours__c thisHours = new GW_Volunteers__Volunteer_Hours__c();
          thisHours.GW_Volunteers__Contact__c         = contactId;
          thisHours.GW_Volunteers__Status__c          = ct_Constants.VOLUNTEER_PROSPECT_STATUS;
          thisHours.GW_Volunteers__Volunteer_Job__c   = jobId;
          thisHours.GW_Volunteers__Start_Date__c      = System.today();
          return thisHours;
      }
        
    /**
    * @description - Create Volunteer Job
    * @return GW_Volunteers__Volunteer_Job__c  
    * @param Id campaignId
    **/
    public static GW_Volunteers__Volunteer_Job__c createVolunteerJob(Id campaignId){
        GW_Volunteers__Volunteer_Job__c thisJob = new GW_Volunteers__Volunteer_Job__c();
        thisJob.Name                            = 'Test Job';
        thisJob.GW_Volunteers__Campaign__c      = campaignId;
        return thisJob;
    }
     /**
    * @description - Create Campaign
    * @return Campaign  
    * @param Id projectId
    **/
    public static campaign createCampaign(Id projectId){
        campaign thisCampaign = new campaign();
        thisCampaign.Name='Test Campaign';
        thisCampaign.Project__c=projectId;
        return thisCampaign;
    }

    /**
    * @description Create HED Address
    * @author Creation Admin | 24/6/2020 
    * @param contactId 
    * @return hed__Address__c 
    **/
    public static hed__Address__c createHedAddress(Id contactId){
      hed__Address__c thisAddress = new hed__Address__c();
      thisAddress.hed__Parent_Contact__c = contactId;
      thisAddress.hed__MailingCity__c = 'Test City';
      thisAddress.hed__MailingCountry__c = 'Test Country';
      thisAddress.hed__MailingState__c = 'Test State';
      thisAddress.hed__MailingStreet__c = 'Test Street';
      thisAddress.hed__MailingPostalCode__c = '123455';
      return thisAddress;
    }
}