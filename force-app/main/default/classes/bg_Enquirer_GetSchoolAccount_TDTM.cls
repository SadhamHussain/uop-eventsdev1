/*****************************************************************
* bg_Lead_GetSchoolAccount
*
* Lead Trigger Handler for populating the school_college__c field or the failure flags
* when the school_college_freetext field is populatd. BeforeInsert or BeforeUpdate
* 
* Author: Mark Brown
* Created: 17-06-2019
******************************************************************/

global class bg_Enquirer_GetSchoolAccount_TDTM extends hed.TDTM_Runnable 
{
    /*
    *
    * Trigger Handler on BeforeInsert & BeforeUpdate for for populating the school/college and failure flag fields
    *
    */
    public override DmlWrapper run(List<SObject> newlist, List<SObject> oldlist, Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {
        
        if (triggerAction == Action.BeforeInsert || triggerAction == Action.BeforeUpdate) 
        {
            List<Lead> leadRecords = newlist;
            List<Lead> oldLeadRecords = oldList;
            
            if(triggerAction == Action.BeforeUpdate)
            {
                bg_AccountUtils.bg_GetAccountFromName(leadRecords,oldLeadRecords,true);
            } else {
                bg_AccountUtils.bg_GetAccountFromName(leadRecords,oldLeadRecords,false);
            }

        }

        return null;
    }
}