/**
 * @File Name          : ct_AccountTriggerHelper_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 25/6/2020, 6:33:45 pm
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    6/5/2020   Creation Admin     Initial Version
**/
@isTest
public  class ct_AccountTriggerHelper_Test {

  @IsTest
  static void testToggelAddressVisibility(){

    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_AccountTriggerHandler_TDTM', 'Account', 'AfterUpdate', 2.00));
    hed.TDTM_Global_API.setTdtmConfig(tokens);

    Account thisAccount      = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ADMINISTRATIVE;
    thisAccount.AAA_Private_Record__c = false;
    insert thisAccount;
  
    Contact thisContact   = ct_TestDataFactory.createContact();
    thisContact.AccountId = thisAccount.Id;
    insert thisContact;

    hed__Address__c thisAddress = ct_TestDataFactory.createHedAddress(thisContact.id);
    insert thisAddress;

    Test.starttest();
    thisAccount.AAA_Private_Record__c = true;
    update thisAccount;
    Test.stoptest();
  }
}