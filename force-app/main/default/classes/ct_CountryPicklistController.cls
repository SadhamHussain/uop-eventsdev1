/**
 * @File Name          : ct_CountryPicklistController.cls
 * @Description        : 
 * @Author             : Javid Sherif
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 17/4/2020, 6:32:04 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/4/2020   Javid Sherif     Initial Version
**/
public class ct_CountryPicklistController {
  /**
  * @description parse country values from static resource
  * @author Creation Admin | 17/4/2020 
  * @return List<PicklistEntryWrapper> 
  **/
  @AuraEnabled
  public static List<PicklistEntryWrapper> parseCountryValues(){
    List<PicklistEntryWrapper> thisPickListWrapperList = new List<PicklistEntryWrapper>();
    StaticResource srObject = [select Id,Body from StaticResource Where Name = 'ct_CountryValues'];
    String contents = blobToString(srObject.body,'ISO-8859-1');
    String[] filelines = contents.split('\r\n');

    for(Integer i = 1; i < filelines.size(); i++){
      String[] inputvalues = filelines[i].split(',');
      thisPickListWrapperList.add(new PicklistEntryWrapper(inputvalues[0], inputvalues[0]));
    }

    return thisPickListWrapperList;
  }

   /**
    This function convers the input CSV file in BLOB format into a string
    @param input    Blob data representing correct string in @inCharset encoding
    @param inCharset    encoding of the Blob data (for example 'ISO 8859-1')
  */
    public static String blobToString(Blob input, String inCharset){
      String hex = EncodingUtil.convertToHex(input);
      System.assertEquals(0, hex.length() & 1);
      final Integer bytesCount = hex.length() >> 1;
      String[] bytes = new String[bytesCount];
      for(Integer i = 0; i < bytesCount; ++i)
          bytes[i] =  hex.mid(i << 1, 2);
      return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
  }         

   /**
    * Wrapper that contains Donation Object wrapper and Picklist Values
  */
  public class PicklistEntryWrapper{
    @AuraEnabled
    public String label                  {get;set;}
    @AuraEnabled
    public string value                  {get;set;}
    
    public PicklistEntryWrapper(String entryLabel,String entryValue){
      this.label    = entryLabel;
      this.value    = entryValue;
    }
  }
}