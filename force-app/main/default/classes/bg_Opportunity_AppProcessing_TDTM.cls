/*****************************************************************
* bg_Opportunity_AppProcessing_TDTM
*
* Opportunity Trigger Handler for processing Application records
* 
******************************************************************/

global class bg_Opportunity_AppProcessing_TDTM extends hed.TDTM_Runnable {
    
    /*
    * Main method for handling logic for each trigger context
    */
	public override DmlWrapper run(List<SObject> newList, List<SObject> oldList, 
    Action triggerAction, Schema.DescribeSObjectResult objResult) 
    {           
		Bypass_Configuration__c configBypass = Bypass_Configuration__c.getInstance(UserInfo.getProfileId());
        if(!configBypass.Are_Processes_Off__c)
        {        
            if (triggerAction == Action.AfterUpdate)
            {
                List<Opportunity> newOpps = newList;
                List<Opportunity> oldOpps = oldList;
    
                Map<Id,Opportunity> oldOppsById = new Map<Id,Opportunity>(oldOpps);
                bg_OpportunityUtils.processUpdatedOpportunitiesAfterUpdate(newOpps, oldOppsById);
            }
        }

        return null;
    }
}