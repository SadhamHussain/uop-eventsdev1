@isTest
private class bg_AffiliationUtils_Test {
    
	@testSetup static void setupData()
	{
		List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Contact_AppProcessing_TDTM', 
                                                     'Contact', 'BeforeInsert;AfterInsert;BeforeUpdate;AfterUpdate', 10.00));
        
        hed.TDTM_Global_API.setTdtmConfig(tokens);

		
	}


    static testMethod void test_createSchoolAffiliationForUpdatedContacts_positive_single() {
    	
    	Bypass_Configuration__c configBypass = Bypass_Configuration__c.getInstance(UserInfo.getProfileId());
		configBypass.Are_Processes_Off__c = false;
		insert configBypass;
    	
    	//Given
    	List<Account> schoolsToCreate = new List<Account>();
    	Account testSchool1 = bg_UnitTestDataFactory.createSchoolAccount();
    	schoolsToCreate.add(testSchool1);
    	Account testSchool2 = bg_UnitTestDataFactory.createSchoolAccount();
    	testSchool2.School_College_UCAS_Code__c = 'test123';
    	schoolsToCreate.add(testSchool2);
    	insert schoolsToCreate;
        
        system.debug(schoolsToCreate);

    	Contact testContact = bg_UnitTestDataFactory.createContact();
    	testContact.School_College__c = testSchool1.Id;
    	insert testContact;
        
        bg_Contact_AppProcessing_TDTM.afterUpdateHasRun = false;
        

    	//When
    	testContact.School_College__c = testSchool2.Id;
    	update testContact;

    	//Then
    	List<hed__Affiliation__c> insertedAffiliations = [select id, hed__Account__c, hed__Contact__c from hed__Affiliation__c];

    	system.assertEquals(1, insertedAffiliations.size(), 'Wrong number of affiliations inserted');
    	system.assertEquals(testSchool2.Id, insertedAffiliations.get(0).hed__Account__c, 'Wrong school linked');
    	system.assertEquals(testContact.Id, insertedAffiliations.get(0).hed__Contact__c, 'Wrong contact linked');

    }
}