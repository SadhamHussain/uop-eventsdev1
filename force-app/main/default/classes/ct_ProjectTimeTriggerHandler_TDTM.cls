/**
 * @File Name          : ct_ProjectTimeTriggerHandler_TDTM.cls
 * @Description        : TDTM Trigger Handler for Project Time Object
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/18/2020, 7:57:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/18/2020   Creation Admin     Initial Version
**/
global class ct_ProjectTimeTriggerHandler_TDTM extends hed.TDTM_Runnable{
global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
                                                 hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {
			System.debug('/****************************ct_ProjectTimeTriggerHandler_TDTM*****************/');
			hed.TDTM_Runnable.DmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
			//Project Ids
			set<Id> projectTaskIds = new set<Id>();
			 if (triggerAction == hed.TDTM_Runnable.Action.AfterInsert || triggerAction == hed.TDTM_Runnable.Action.AfterUpdate) {
				 for (Project_Time__c pt : (list<Project_Time__c>)newlist) {
					 if (pt.Project_Task__c != null) {
						 projectTaskIds.add(pt.Project_Task__c);
					 }
				 }
			 }
			 else if (triggerAction == hed.TDTM_Runnable.Action.BeforeUpdate || triggerAction == hed.TDTM_Runnable.Action.AfterDelete) {
				 for (Project_Time__c pt : (list<Project_Time__c>)oldlist) {
					 if (pt.Project_Task__c != null) {
						 projectTaskIds.add(pt.Project_Task__c);
					 }
				 }
			 }
			 if(projectTaskIds.size()>0 && !System.isFuture() && !System.isBatch()){
				 ct_ProjectTimeTriggerHelper.updateProjectTaskCustomRollup(projectTaskIds);
			 }
			return dmlWrapper;                                                  
			}
}