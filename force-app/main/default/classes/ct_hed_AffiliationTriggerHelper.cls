/**
 * @File Name          : ct_hed_AffiliationTriggerHelper.cls
 * @Description        : Trigger Helper Class for ct_hed_AffiliationTriggerHandler_TDTM Class
 * @Author             : Creation Admin
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 6/25/2020, 1:48:58 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/2/2020   Creation Admin     Initial Version
**/
public class ct_hed_AffiliationTriggerHelper {
  
  public static Set<String> RO_Organisation_Role = new Set<String>{ct_Constants.AFFILIATION_ROLE_CAREERS_ADVISER,
                                                                  ct_Constants.AFFILIATION_ROLE_CAREER_ENRICHMENT_LEAD,
                                                                  ct_Constants.AFFILIATION_ROLE_EPQ_COORDINATOR,
                                                                  ct_Constants.AFFILIATION_ROLE_HEAD_TEACHER_PRINCIPAL,
                                                                  ct_Constants.AFFILIATION_ROLE_GOVERNOR,
                                                                  ct_Constants.AFFILIATION_ROLE_HEAD_OF_SIXTH_FORM,
                                                                  ct_Constants.AFFILIATION_ROLE_BUSINESS_MANAGER,
                                                                  ct_Constants.AFFILIATION_ROLE_PASTORAL_LEAD,
                                                                  ct_Constants.AFFILIATION_ROLE_SUBJECT_TEACHER};

  /**
  * @description - Method to get Contact Map based on Contact ids
  * @param contactIds : Set of contact Ids
  * @return Map<Id, Contact> Contact Map
  **/
  public static Map<Id, Contact> getContactMap(Set<Id> contactIds){
    return  new Map<Id, Contact>([SELECT Id, Name, Primary_Role__c, R_O_Organisational_Contact__c
                                        FROM Contact 
                                        WHERE Id IN: contactIds]);
  }
  
  /**
  * @description - Method to get Account Map based on Account Ids
  * @param accountIds : Set of Account Ids
  * @return Map<Id, Account> Account Map
  **/
  public static Map<Id, Account> getAccountMap(Set<Id> accountIds){
    return  new Map<Id, Account>([SELECT Id, Name, R_O_Organisation__c, RecordTypeId
                                        FROM Account 
                                        WHERE Id IN: accountIds]);
  }

  /**
  * @description - Method to update Contact record based on Affiliation Primary role and R&O Organisation details
  * @param thisContactMap  : Map of contact record
  * @param thisAccountMap  : Map of Account record
  * @param thisAffiliationList List of Affiliation Records
  * @return List<Contact> Contact List to update
  **/
  public static List<Contact> updateContactRecord(Map<Id, Contact> thisContactMap,Map<Id, Account> thisAccountMap, List<hed__Affiliation__c> thisAffiliationList){
    //Map of Contact Map to update
    Map<Id, Contact> contactMapToUpdate = new Map<Id, Contact>();

    for(hed__Affiliation__c thisAffiliation : thisAffiliationList){
      //Update Contact's Primary role based on affiliation role only if affiliation is Primary
      if(thisAffiliation.hed__Contact__c != null
          && thisAffiliation.hed__Role__c != null
          && thisAffiliation.hed__Primary__c
          && thisContactMap.containskey(thisAffiliation.hed__Contact__c)
          && thisContactMap.get(thisAffiliation.hed__Contact__c).Primary_Role__c  != thisAffiliation.hed__Role__c){
            thisContactMap.get(thisAffiliation.hed__Contact__c).Primary_Role__c = thisAffiliation.hed__Role__c;
            contactMapToUpdate.put(thisAffiliation.hed__Contact__c, thisContactMap.get(thisAffiliation.hed__Contact__c));
        } 
        //Update Contact's R&O Organisation details as true if affiliations account's R&O Organisation is true
        // if( RO_Organisation_Role.contains(thisAffiliation.hed__Role__c) 
        //   && !thisAccountMap.isEmpty()
        //   && thisAccountMap.containskey(thisAffiliation.hed__Account__c)
        //   && thisContactMap.containskey(thisAffiliation.hed__Contact__c)
        //   && thisAccountMap.get(thisAffiliation.hed__Account__c).R_O_Organisation__c
        //   && thisAccountMap.get(thisAffiliation.hed__Account__c).RecordTypeId == ct_Constants.ACCOUNT_RECORDTYPE_EDUCATIONAL_INSTITUTION
        //   && !thisContactMap.get(thisAffiliation.hed__Contact__c).R_O_Organisational_Contact__c){
        //     thisContactMap.get(thisAffiliation.hed__Contact__c).R_O_Organisational_Contact__c = true;
        //     contactMapToUpdate.put(thisAffiliation.hed__Contact__c, thisContactMap.get(thisAffiliation.hed__Contact__c));
        // }
    }
    return contactMapToUpdate.values();
  }

  /**
  * @description - Method to create update UOP Academic Affiliation Rollup fields on Contact
  * @param contactIds Set Of Contact Ids
  * @return void 
  **/
  @future
  public static void updateContactCustomRollupFields(Set<Id> contactIds){
    processContactAcademicAffiliationRollup(contactIds);
  }

  /**
  * @description - Method to update UOP Academic Affiliation Rollup fields on Contact
  * @param contactIds Set Of Contact Ids whose Academic Affiliation needs to be processed
  * @return void 
  **/
  public static void processContactAcademicAffiliationRollup(Set<Id> contactIds){
    String keyCombination;
    //Map of Contact Map to update
    Map<Id, Contact> contactMapToUpdate = new Map<Id, Contact>();
      //Get Contact records whose Affiliation roll ups needs to be processed
      for(Contact thisContact : [SELECT Id, Name, No_Prospect_Affiliations__c,
                                      No_Applicant_Affiliations__c,
                                      No_Student_Affiliations__c,
                                      No_Alumni_Affiliations__c,
                                      No_Rejected_Affiliations__c,
                                      UOP_Academic_Affiliations__c
                                  FROM Contact 
                                  WHERE Id IN :contactIds]){
        //Reset all the custom rollups before processing
        thisContact.No_Prospect_Affiliations__c   = 0;
        thisContact.No_Applicant_Affiliations__c  = 0;
        thisContact.No_Alumni_Affiliations__c     = 0;
        thisContact.No_Student_Affiliations__c    = 0;
        thisContact.No_Rejected_Affiliations__c   = 0;
        thisContact.UOP_Academic_Affiliations__c  = null;
        contactMapToUpdate.put(thisContact.Id, thisContact);
      }
    //Academic affiliation key combination based on Affiliation role and Affiliation status based on which roll ups count will be calculated
    Set<String> academicAffiliationsKeyCombination = new Set<String>{
                                                    ct_Constants.AFFILIATION_ROLE_PROSPECT+'_'+ct_Constants.AFFILIATION_STATUS_CURRENT,
                                                    ct_Constants.AFFILIATION_ROLE_APPLICANT+'_'+ct_Constants.AFFILIATION_STATUS_CURRENT,
                                                    ct_Constants.AFFILIATION_ROLE_STUDENT+'_'+ct_Constants.AFFILIATION_STATUS_CURRENT,
                                                    ct_Constants.AFFILIATION_ROLE_STUDENT+'_'+ct_Constants.AFFILIATION_STATUS_ALUMNI,
                                                    ct_Constants.AFFILIATION_ROLE_APPLICANT+'_'+ct_Constants.AFFILIATION_STATUS_FORMER};
    
    //Get the hed__Affiliation Records based on key combination and Contact Ids
    for(AggregateResult thisResult : [SELECT  hed__Status__c, hed__Role__c, hed__Contact__c,  COUNT(Id) totalRecords
                                        FROM hed__Affiliation__c
                                        WHERE hed__Contact__c IN :contactIds
                                        AND hed__Account__r.RecordTypeId =: ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM 
                                        AND hed__Status__c != null
                                        AND hed__Role__c != null
                                        GROUP BY  hed__Status__c, hed__Role__c,hed__Contact__c]){
      //Generate Key Combination based on Affiliations Role and status                                
      keyCombination = (string)thisResult.get('hed__Role__c')+'_'+(string)thisResult.get('hed__Status__c');
      //Only required key combination needs to be processed                    
      if(academicAffiliationsKeyCombination.contains(keyCombination)
        && contactMapToUpdate.containskey((Id)thisResult.get('hed__Contact__c'))){
        //Get Contact UOP Academic Affiliations Multi Select Picklist values
        String thisContactAffiliation = contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).UOP_Academic_Affiliations__c;

        //Process CURRENT PROSPECT Affiliation : Role = Prospect and Status = Current
        if(keyCombination == ct_Constants.AFFILIATION_ROLE_PROSPECT+'_'+ct_Constants.AFFILIATION_STATUS_CURRENT){
          contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).No_Prospect_Affiliations__c = (Integer)thisResult.get('totalRecords');
          if(thisContactAffiliation == null 
            || !thisContactAffiliation.contains(ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_PROSPECT)){
            //Process related contact's Academic affiliation Multi select Picklist
            contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).UOP_Academic_Affiliations__c = thisContactAffiliation == null ? ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_PROSPECT : thisContactAffiliation +';'+ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_PROSPECT;
          }
        }

        //Process CURRENT APPLICANT Affiliation : Role = Applicant and Status = Current
        if(keyCombination == ct_Constants.AFFILIATION_ROLE_APPLICANT+'_'+ct_Constants.AFFILIATION_STATUS_CURRENT){
          contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).No_Applicant_Affiliations__c = (Integer)thisResult.get('totalRecords');
          if(thisContactAffiliation == null 
            || !thisContactAffiliation.contains(ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_APPLICANT)){
            //Process related contact's Academic affiliation Multi select Picklist
            contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).UOP_Academic_Affiliations__c = thisContactAffiliation == null ? ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_APPLICANT : thisContactAffiliation +';'+ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_APPLICANT;
          }
        }

        //Process CURRENT STUDENT Affiliation : Role = Student and Status = Current
        if(keyCombination == ct_Constants.AFFILIATION_ROLE_STUDENT+'_'+ct_Constants.AFFILIATION_STATUS_CURRENT){
          contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).No_Student_Affiliations__c = (Integer)thisResult.get('totalRecords');
          if(thisContactAffiliation == null 
            || !thisContactAffiliation.contains(ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_STUDENT)){
            //Process related contact's Academic affiliation Multi select Picklist
          contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).UOP_Academic_Affiliations__c = thisContactAffiliation == null ? ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_STUDENT : thisContactAffiliation +';'+ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_STUDENT;
          }
        }
        
        //Process CURRENT ALUMNI Affiliation : Role = Student and Status = Alumni
        if(keyCombination == ct_Constants.AFFILIATION_ROLE_STUDENT+'_'+ct_Constants.AFFILIATION_STATUS_ALUMNI){
          contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).No_Alumni_Affiliations__c = (Integer)thisResult.get('totalRecords');
          if(thisContactAffiliation == null
            || !thisContactAffiliation.contains(ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_ALUMNI)){
            //Process related contact's Academic affiliation Multi select Picklist
            contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).UOP_Academic_Affiliations__c = thisContactAffiliation == null ? ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_ALUMNI : thisContactAffiliation +';'+ct_Constants.UOP_ACADEMIC_AFFILIATIONS_CURRENT_ALUMNI;
          }
        }
        //Process REJECTED APPLICANT Affiliation : Role = Applicant and Status = Former
        if(keyCombination == ct_Constants.AFFILIATION_ROLE_APPLICANT+'_'+ct_Constants.AFFILIATION_STATUS_FORMER){
          contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).No_Rejected_Affiliations__c = (Integer)thisResult.get('totalRecords');
          if(thisContactAffiliation == null
            || !thisContactAffiliation.contains(ct_Constants.UOP_ACADEMIC_AFFILIATIONS_REJECTED_APPLICATIONS)){
            //Process related contact's Academic affiliation Multi select Picklist
            contactMapToUpdate.get((Id)thisResult.get('hed__Contact__c')).UOP_Academic_Affiliations__c = thisContactAffiliation == null ? ct_Constants.UOP_ACADEMIC_AFFILIATIONS_REJECTED_APPLICATIONS : thisContactAffiliation +';'+ct_Constants.UOP_ACADEMIC_AFFILIATIONS_REJECTED_APPLICATIONS;
          }
        }
      }
    }
    try{
      //Update Processed Contact Records
      update contactMapToUpdate.values();
    }
    catch(Exception e){
      //Invoke Error Logger if any error occurs
      ct_Logger.logMessage(e, 'Contact');
      throw e;
    }
  }

  /**
  * @description - Method to create update  Affiliation Rollup fields for Student Organisation account
  * @param accountIds Set of Account Ids
  * @return void 
  **/
  @future
  public static void updateStudentOrganisationAccountRollup(Set<Id> accountIds){
    //Account Map to be updated
    Map<Id, Account> accountMap = new Map<Id, Account>();
    //Get Organisation Account record Affiliated Member count need to be Processed
    for(Account thisAccount : [SELECT Id, Actual_Affiliated_Members__c, RecordTypeId
                                        FROM Account 
                                        WHERE Id IN: accountIds
                                        AND RecordTypeId =: ct_Constants.ACCOUNT_RECORDTYPE_STUDENT_ORGANISATION]){
      thisAccount.Actual_Affiliated_Members__c = 0;   
      accountMap.put(thisAccount.Id, thisAccount);                                
    }

    //Get the hed__Affiliation Aggregate Count based on accountMap Key Set
    for(AggregateResult thisResult : [SELECT  hed__Account__c,  COUNT(Id) totalRecords
                                        FROM hed__Affiliation__c
                                        WHERE  hed__Account__c IN :accountMap.keySet()
                                        GROUP BY hed__Account__c]){
      //Update Actual Affiliated Member Count Based on Aggregate Query
      accountMap.get((Id)thisResult.get('hed__Account__c')).Actual_Affiliated_Members__c =  (Integer)thisResult.get('totalRecords');
    }
    try{
      //Update Processed Account Records
      update accountMap.values();
    }
    catch(Exception e){
      //Invoke Error Logger if any error occurs
      ct_Logger.logMessage(e, 'Account');
      throw e;
    }
  }

  /**
  * @description - Method to Update RAO checkboxe in Account and Contact object
  * @param Set<Id> accountIds 
  * @param Set<Id> contactIds 
  * @return void 
  **/
  public static void updateRAOOnAccountAndContact(Set<Id> accountIds, Set<Id> contactIds){
    List<Contact> contactListToUpdate = new List<Contact>();
    List<Account> accountListToUpdate = new List<Account>();
    for(Account thisAccount : [SELECT Id, R_O_Organisation__c 
                                    FROM Account
                                    WHERE Id IN: accountIds
                                    AND R_O_Organisation__c =  false]){

      thisAccount.R_O_Organisation__c =  true;
      accountListToUpdate.add(thisAccount);
    }
    for(Contact thisContact : [SELECT Id, R_O_Organisational_Contact__c 
                                    FROM Contact
                                    WHERE Id IN: contactIds
                                    AND R_O_Organisational_Contact__c =  false]){
      System.debug('THIS CONTACT'+thisContact );
      thisContact.R_O_Organisational_Contact__c =  true;
      contactListToUpdate.add(thisContact);
    }
    try{
      //Update Processed Account Records
      update accountListToUpdate;
    }
    catch(Exception e){
      //Invoke Error Logger if any error occurs
      ct_Logger.logMessage(e, 'Account');
      throw e;
    }
    try{
      //Update Processed Contact Records
      update contactListToUpdate;
    }
    catch(Exception e){
      //Invoke Error Logger if any error occurs
      ct_Logger.logMessage(e, 'Contact');
      throw e;
    }
  }
  @future
  public static void updateRAOContactAndAccountRecords(Set<Id> accountIds, Set<Id> contactIds){
    List<Account> accountListToUpdate = new List<Account>();
    List<Contact> contactListToUpdate = new List<Contact>();

    for(Account thisAccount : [SELECT Id, R_O_Organisation__c, (SELECT Id, hed__Account__c, 
                                    hed__Status__c, RAO_Affiliation__c FROM hed__Affl_Contacts__r
                                    WHERE hed__Status__c =: ct_Constants.AFFILIATION_STATUS_CURRENT 
                                    AND RAO_Affiliation__c = true
                                    LIMIT 1)
                                    FROM Account
                                    WHERE Id IN: accountIds]){
      if(thisAccount.hed__Affl_Contacts__r.size() == 0
      && thisAccount.R_O_Organisation__c){
        thisAccount.R_O_Organisation__c = false;
        accountListToUpdate.add(thisAccount);
      }
      else if(thisAccount.hed__Affl_Contacts__r.size() == 1
      && !thisAccount.R_O_Organisation__c){
        thisAccount.R_O_Organisation__c = true;
        accountListToUpdate.add(thisAccount);
      }
    }
    for(Contact thisContact : [SELECT Id, R_O_Organisational_Contact__c, (SELECT Id, hed__Account__c, 
                                    hed__Status__c, RAO_Affiliation__c FROM hed__Affl_Accounts__r
                                    WHERE hed__Status__c =: ct_Constants.AFFILIATION_STATUS_CURRENT 
                                    AND RAO_Affiliation__c = true
                                    LIMIT 1)
                                    FROM Contact
                                    WHERE Id IN: contactIds]){
      if(thisContact.hed__Affl_Accounts__r.size() == 0
      && thisContact.R_O_Organisational_Contact__c){
        thisContact.R_O_Organisational_Contact__c = false;
        contactListToUpdate.add(thisContact);
      }
      else if(thisContact.hed__Affl_Accounts__r.size() == 1
      && !thisContact.R_O_Organisational_Contact__c){
        thisContact.R_O_Organisational_Contact__c = true;
        contactListToUpdate.add(thisContact);
      }
    }
    try{
      //Update Processed Account Records
      update accountListToUpdate;
    }
    catch(Exception e){
      //Invoke Error Logger if any error occurs
      ct_Logger.logMessage(e, 'Account');
      throw e;
    }
    try{
      //Update Processed Contact Records
      update contactListToUpdate;
    }
    catch(Exception e){
      //Invoke Error Logger if any error occurs
      ct_Logger.logMessage(e, 'Contact');
      throw e;
    }  
  }
}