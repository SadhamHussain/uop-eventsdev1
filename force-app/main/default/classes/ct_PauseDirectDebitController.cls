/**
 * @File Name          : ct_PauseDirectDebitController.cls
 * @Description        : Apex class for Pausing and un pausing the Direct Debit 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 6/4/2020, 8:24:40 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   Creation Admin     Initial Version
**/
public class ct_PauseDirectDebitController {
    
  /**
  * @description - method to fetch Direct debit record 
  * @return initialWrapper 
  **/
  @AuraEnabled
  public static npe03__Recurring_Donation__c getDirectDebitRecord(Id directDebitId){
    return [SELECT Id, Paused__c
              FROM npe03__Recurring_Donation__c
              WHERE Id =: directDebitId];
  }

  @AuraEnabled
  public static Id updateDirectDebit(String directDebitObject){
    try{
      npe03__Recurring_Donation__c thisDirectDebit = (npe03__Recurring_Donation__c)JSON.deserialize(directDebitObject, npe03__Recurring_Donation__c.class);
      update thisDirectDebit;
      return thisDirectDebit.Id;
      }
      catch (Exception e) {
        ct_Logger.logMessage(e, 'Direct Debit Pausing / Resuming Process');
        throw new AuraHandledException('Error '+ e.getMessage());    
      }
    }
}