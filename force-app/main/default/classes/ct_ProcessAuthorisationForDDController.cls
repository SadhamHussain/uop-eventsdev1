/**
 * @File Name          : ct_ProcessAuthorisationForDDController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 17/4/2020, 8:03:49 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/14/2020   Creation Admin     Initial Version
**/
public class ct_ProcessAuthorisationForDDController {
    
    /**
    * @description - Checking the authrisation status is awaiting submission or not
    * @param recurringDonationId - Getting this value from ct_ProcessAuthorisationForDD aura component
    * @return String 
    **/
    @auraEnabled public static String getAuthrisationStatus(Id recurringDonationId){
        String authStatus;
        //Queriying recurring Donation by recurringDonationId
        npe03__Recurring_Donation__c thisRecurring = [SELECT Id, Name,Authorisation_Status__c FROM npe03__Recurring_Donation__c WHERE Id =: recurringDonationId];
        authStatus = thisRecurring.Authorisation_Status__c;
        system.debug('authStatus>'+authStatus);
        return authStatus;
    }
    
    /**
    * @description - get Process Authorization URL 
    * @param recurringDonationId - Getting this value from ct_ProcessAuthorisationForDD aura component
    * @return String 
    **/
    @auraEnabled public static String getProcessAuthorisationURLasString(Id recurringDonationId){
        String urlValue;
        //Queriying recurring Donation by recurringDonationId
        npe03__Recurring_Donation__c thisRecurring = [SELECT Id, Name,Asperato_Authorisation__c, Asperato_Authorisation__r.Id FROM npe03__Recurring_Donation__c WHERE Id =: recurringDonationId];
        //Set the URL value as per the metadata setting payment url with asperato authorisation Id
        urlValue = ct_MetadataService.getAdvancementSettingMetaDataByDeveloperName(ct_Constants.WEB_DONATION_PAYMENT_DETAILS).Asperato_Payment_URL__c+'&aid='+thisRecurring.Asperato_Authorisation__r.Id;    
        return urlValue;
    }
}