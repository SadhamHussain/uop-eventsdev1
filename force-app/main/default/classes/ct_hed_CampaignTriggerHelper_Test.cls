/**
 * @File Name          : ct_hed_CampaignTriggerHelper_test.cls
 * @Description        : Test class for ct_hed_CampaignTriggerHelper,ct_hed_CampaignTriggerHelper_TDTM
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/12/2020, 7:57:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/12/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_hed_CampaignTriggerHelper_Test {

    testMethod static void unit1(){
        // first retrieve default EDA trigger handlers
       List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_CampaignTriggerHandler_TDTM', 'Campaign', 'AfterInsert;AfterUpdate;AfterDelete;BeforeUpdate', 1.00));

       // Pass trigger handler config to set method for this test run
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        Project__c thisProject = ct_TestDataFactory.createProject();
		insert thisProject;
        Test.startTest();
        Campaign thisCampaign = ct_TestDataFactory.createCampaign(thisProject.Id);
		insert thisCampaign;
        delete thisCampaign;
         System.assertEquals(thisCampaign.Project__c, thisProject.Id);
        Test.stopTest();
    }
    testMethod static void unit2(){
        // first retrieve default EDA trigger handlers
       List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_CampaignTriggerHandler_TDTM', 'Campaign', 'AfterInsert;AfterUpdate;AfterDelete;BeforeUpdate', 1.00));

       // Pass trigger handler config to set method for this test run
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        Project__c thisProject = ct_TestDataFactory.createProject();
		insert thisProject;
        Project__c thisProject1 = ct_TestDataFactory.createProject();
		insert thisProject1;
        Test.startTest();
        Campaign thisCampaign = ct_TestDataFactory.createCampaign(thisProject.Id);
		insert thisCampaign;
        thisCampaign.Project__c = thisProject1.Id;
        update thisCampaign;
        System.assertEquals(thisCampaign.Project__c, thisProject1.Id);
        Test.stopTest();
    }
}