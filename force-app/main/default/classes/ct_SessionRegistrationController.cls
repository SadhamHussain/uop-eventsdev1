public class ct_SessionRegistrationController {
    //public static List<Session__c> sessionsOnSearch;
    
    @auraEnabled
    public static PageData getPageInitData(String recordId){
        system.debug('recordId'+recordId);
        
        // get event registration record first
        Event_Registration__c evtReg = [Select Id, Event__c From Event_Registration__c Where Id=: recordId];
        
        // init target data structure
        PageData data = new PageData();
        data.eventId = evtReg.Event__c;
        data.lstSessions = new List<SessionWrapper>();
        for(Session__c session : [Select Id,Session_Abstract__c,Name,Start_Time__c,
                              End_Time__c,Event__c From Session__c Where Event__c =: evtReg.Event__c ORDER BY Name ASC NULLS LAST]) {
            data.lstSessions.add(new SessionWrapper(session, false));			
        }
        system.debug('>>> Final Data: ' + data);
        system.debug('>>> Final Data: ' + data.lstSessions);
        system.debug('>>> Final Data: ' + data.lstSessions[0]);
        return data;
    }
    
    
    @AuraEnabled
    public static List<SessionWrapper> sessionsOnTitleSearch(String evtId, String searchKey){

        List<SessionWrapper> lstSessions =  new List<SessionWrapper>();
        for(Session__c session : [Select Id,Session_Abstract__c,Name,Start_Time__c,End_Time__c,Event__c From Session__c
                                 Where Event__c =: evtId AND Name LIKE:('%'+searchKey+'%') ORDER BY Name ASC NULLS LAST]) {
            lstSessions.add(new SessionWrapper(session, false));			
        }
       // sessionsOnSearch= [Select Id,Session_Abstract__c,Name,Start_Time__c,End_Time__c,Event__c From Session__c Where Event__c =: evtId AND Name LIKE:('%'+searchKey+'%')];
        system.debug('lstSessions'+lstSessions);
        return lstSessions;
    }
    @AuraEnabled
    public static List<SessionWrapper> sessionsOnTrackSearch(String evtId, String searchKey){
        List<SessionWrapper> lstSessions =  new List<SessionWrapper>();
        for(Session__c session : [Select Id,Session_Abstract__c,Name,Start_Time__c,End_Time__c,Event__c,Track__c, Track__r.Name 
                                 From Session__c Where Event__c =: evtId AND Track__r.Name LIKE:('%'+searchKey+'%') ORDER BY Name ASC NULLS LAST]) {
            lstSessions.add(new SessionWrapper(session, false));			
        }
        system.debug('lstSessions'+lstSessions);
        return lstSessions;
        /*sessionsOnSearch= [Select Id,Session_Abstract__c,Name,Start_Time__c,End_Time__c,Event__c,Track__c, Track__r.Name From Session__c Where Event__c =: evtId AND Track__r.Name LIKE:('%'+searchKey+'%')];
        system.debug('sessionsOnSearch'+sessionsOnSearch);
        return sessionsOnSearch;*/
    }
    @AuraEnabled
    public static String createSessionReg(String jsonData, String evtRegId){
        List<Session_Registration__c> lstSessionReg = new List<Session_Registration__c>();
        try{
            List<SessionWrapper> finalLstSessions = (List<SessionWrapper>)JSON.deserialize(jsonData,List<SessionWrapper>.class);
            system.debug('Pagedata>>>'+finalLstSessions);
            system.debug('PagedataSessions>>>'+finalLstSessions[0]);
            for(SessionWrapper sessionwrpr: finalLstSessions)
                {
                    if(sessionwrpr.selected){
                    system.debug('****');
                    Session_Registration__c sessionReg = new Session_Registration__c();
                    sessionReg.Event_Registration__c = evtRegId;
                    sessionReg.Session__c = sessionwrpr.session.Id;
                    sessionReg.Status__c = 'Attending';
                    lstSessionReg.add(sessionReg);
                    }
                }
            system.debug('lstSessionReg****'+lstSessionReg);
            if(lstSessionReg.size()> 0){
                insert lstSessionReg;
            }
        }   
        catch(exception ex){
            return ex.getMessage();
        }
        return 'Success';
    }
    
    public class PageData {
        @auraEnabled public List<SessionWrapper> lstSessions {get; set;}
        @auraEnabled public String eventId {get; set;}
    }
    
    public class SessionWrapper {
        @auraEnabled public Session__c session {get; set;}
        @auraEnabled public boolean selected {get; set;}
        public SessionWrapper(Session__c session, boolean selected) {
            this.session = session;
            this.selected = selected;
        }
    }
}