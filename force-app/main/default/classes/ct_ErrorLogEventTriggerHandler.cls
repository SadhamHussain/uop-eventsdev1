/**
 * @File Name          : ct_ErrorLogEventTriggerHandler.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 19/5/2020, 10:22:57 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/4/2020   Creation Admin     Initial Version
**/
global class ct_ErrorLogEventTriggerHandler /*extends npsp.TDTM_Runnable*/ {

  
  /**
  * @description Method to process records that follow the TDTM design
  * @author Creation Admin | 8/5/2020 
  * @param newlist 
  * @param oldlist 
  * @param triggerAction 
  * @param objResult 
  * @return hed.TDTM_Runnable.DmlWrapper 
  **/
 /* global override npsp.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,npsp.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {

     npsp.TDTM_Runnable.dmlWrapper dmlWrapper = new npsp.TDTM_Runnable.DmlWrapper();
     List<Error_Log__c> errorLogList = new List<Error_Log__c>();
	 System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ERROR LOG EVENT AFTER INSERT TRIGGER1<<<<<<<<<<<<<<<<<<<<<<<<<<<');
     if (triggerAction == npsp.TDTM_Runnable.Action.AfterInsert) {
       System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ERROR LOG EVENT AFTER INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

       for(Error_Log_Event__e thisEventRecord : (List<Error_Log_Event__e>)newlist){
         Error_Log__c thisErrorLog = new Error_Log__c();
         thisErrorLog.Short_Error__c =  thisEventRecord.Short_Error__c;
         thisErrorLog.Object__c = thisEventRecord.Object_Name__c;
         thisErrorLog.Error_Detail__c = thisEventRecord.Error_Detail__c;
         errorLogList.add(thisErrorLog);
       
      //dmlWrapper.objectsToUpdate.addAll((list<SObject>)errorLogList);
     }
     insert errorLogList;
     
   }
   return dmlWrapper;
  }*/
  /**
  * @description Insert the exceptions in the error log object captured during the transaction
  * @author Creation Admin | 27/4/2020 
  * @param errorLogEventObjectList 
  * @return void 
  **/
  public static void insertErrorLogs(List<Error_Log_Event__e> errorLogEventObjectList){
    List<Error_Log__c> errorLogList = new List<Error_Log__c>();

    for(Error_Log_Event__e thisEventRecord : errorLogEventObjectList){
      Error_Log__c thisErrorLog = new Error_Log__c();
      thisErrorLog.Short_Error__c =  thisEventRecord.Short_Error__c;
      thisErrorLog.Object__c = thisEventRecord.Object_Name__c;
      thisErrorLog.Error_Detail__c = thisEventRecord.Error_Detail__c;
      errorLogList.add(thisErrorLog);
    }
    if ((Limits.getDMLRows() < Limits.getLimitDMLRows()) &&
    (Limits.getDMLStatements() < Limits.getLimitDMLStatements())){
      insert errorLogList;
    }
    else {
      System.debug('Failed to insert the Error Log record. ' +'Error: The APEX RUNTIME GOVERNOR LIMITS has been exhausted.');
    }
  }
}