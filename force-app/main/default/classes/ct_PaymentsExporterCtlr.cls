/**
 * @File Name          : ct_PaymentsExporterCtlr.cls
 * @Description        : Make Asperato Payment Transactions data as CSV or JSON file 
 * @Author             : Creation Admin  - Creation Technology Solutions
 * @Last Modified By   : Creation Admin  - Creation Technology Solutions
 * @Last Modified On   : 2/6/2020, 9:12:55 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/6/2020   Creation Admin  - Creation Technology Solutions     Initial Version
**/
public with sharing class ct_PaymentsExporterCtlr {

    public String jsonAsperatoContent { get; set; }

    public ct_PaymentsExporterCtlr() {
        String fromDate = ApexPages.currentPage().getParameters().get('from');
        String toDate   = ApexPages.currentPage().getParameters().get('to');
        jsonAsperatoContent = fetchAsperatoTxnData(fromDate != null ? Date.parse(fromDate) : System.today(), toDate != null ? Date.parse(toDate) : null);
    }
    /**
    * @description - Using to fetch Asperato Payment Transactions data during 24 hours to make a CSV file if called from ct_ScheduledAsperatoPaymentReport class.
                   - Using to fetch Asperato Payment Transactions data based on FROM and TO value, to make a JSON file if called from ct_PaymentsExporter VF.
    * @author Creation Admin  - Creation Technology Solutions | 2/6/2020 
    * @param paymentFromDate 
    * @param paymentToDate 
    * @return string 
    **/
    public static string fetchAsperatoTxnData(Date paymentFromDate, Date paymentToDate){
        List<AsperatoPaymentWrapper> paymentWrapperList = new List<AsperatoPaymentWrapper>();
        Map<String, String> gemPaymentMap               = new Map<String, String>(); //To store GEM Payment details
        Map<String, String> allocationFundCodeMap       = new Map<String, String>(); //To store GAU Allocation details
        //Fetch Asperato Payment details for the last month
        String asperatoQuery = 'SELECT Id, Name, asp04__Payment_Date__c, asp04__Asperato_Reference__c, CurrencyIsoCode,'+
                                'asp04__Amount__c, asp04__Payment_Route_Options__c, Nominal_Code__c, Opportunity__c,'+
                                'Opportunity__r.AccountId, Opportunity__r.Account.Name, Opportunity__r.npsp__Primary_Contact__c,'+
                                'Opportunity__r.npsp__Primary_Contact__r.FirstName, Opportunity__r.npsp__Primary_Contact__r.LastName,'+ 
                                'asp04__PSP_Reference__c FROM asp04__Payment__c WHERE asp04__Payment_Stage__c =\''+ ct_Constants.ASPERATO_PAYMENT_STAGE_COLLECTED +'\'' +' AND ';
        if(paymentFromDate != null){
            asperatoQuery += 'asp04__Payment_Date__c >= :paymentFromDate';  
            if(paymentToDate != null){
                asperatoQuery += ' AND asp04__Payment_Date__c <= :paymentToDate';
            }
        }
        else{
            asperatoQuery += 'asp04__Payment_Date__c = TODAY';
        }
        asperatoQuery += ' ORDER BY asp04__Payment_Date__c';
        Map<Id, asp04__Payment__c> asperatoPaymentMap   = new Map<Id, asp04__Payment__c>((List<asp04__Payment__c>)Database.query(asperatoQuery));
        //Fetch GEM Payment details related to Asperato Payment
        for(npe01__OppPayment__c thisGemPayment : [SELECT Id, Asperato_Payment__c FROM npe01__OppPayment__c 
                                                        WHERE Asperato_Payment__c =: asperatoPaymentMap.keySet()]){
            gemPaymentMap.put(thisGemPayment.Asperato_Payment__c, thisGemPayment.Id);
        }
        //Fetch GAU Allocations related to GEM Payment to get GAU Fund Code
        for(npsp__Allocation__c thisAllocation :  [SELECT Id, Fund_Code__c, npsp__General_Accounting_Unit__c, 
                                                        npsp__General_Accounting_Unit__r.Fund_Code__c, npsp__Payment__c 
                                                        FROM npsp__Allocation__c 
                                                        WHERE npsp__Payment__c =: gemPaymentMap.values()]){
            allocationFundCodeMap.put(thisAllocation.npsp__Payment__c, thisAllocation.npsp__General_Accounting_Unit__r.Fund_Code__c);
        }
        //Store CSV Columns
        String asperatoDataForCSV = 'Transaction Date,Account Name,Primary Contact,Nominal Code,Fund Code,Payment Route,Payment Number,Payment Reference,Currency,Amount\n';
        //Making CSV file data using Asperato payment details and GAU Fund Code
        for(asp04__Payment__c thisAsperatoPayment : asperatoPaymentMap.values()){
            String fundCode       = allocationFundCodeMap.get(gemPaymentMap.get(thisAsperatoPayment.Id)) != null ? allocationFundCodeMap.get(gemPaymentMap.get(thisAsperatoPayment.Id)) : '';
            String asperatoRef    = thisAsperatoPayment.asp04__Asperato_Reference__c != null ? thisAsperatoPayment.asp04__Asperato_Reference__c : '';
            String accountName    = thisAsperatoPayment.Opportunity__r.AccountId != null ? thisAsperatoPayment.Opportunity__r.Account.Name : '';
            String primaryContact = thisAsperatoPayment.Opportunity__r.npsp__Primary_Contact__c != null ? 
                                      thisAsperatoPayment.Opportunity__r.npsp__Primary_Contact__r.FirstName+' '+thisAsperatoPayment.Opportunity__r.npsp__Primary_Contact__r.LastName : '';
            //Return CSV data If these function execute from ct_ScheduledAsperatoPaymentReport
            if(paymentFromDate == null){                
                asperatoDataForCSV   += thisAsperatoPayment.asp04__Payment_Date__c.format() +','+ accountName + ',' + primaryContact + ',' + thisAsperatoPayment.Nominal_Code__c + ',' +
                                        fundCode + ','+thisAsperatoPayment.asp04__Payment_Route_Options__c + ',' + thisAsperatoPayment.Name + ',' + thisAsperatoPayment.asp04__PSP_Reference__c + ',' +
                                        thisAsperatoPayment.CurrencyIsoCode+','+thisAsperatoPayment.asp04__Amount__c+ '\n';
            }
            //Return JSON data If these function execute from ct_PaymentExporter VF Page
            else{
                AsperatoPaymentWrapper newAsperatoPaymentWrapper    = new AsperatoPaymentWrapper();
                newAsperatoPaymentWrapper.transactionDate           = thisAsperatoPayment.asp04__Payment_Date__c.format();
                newAsperatoPaymentWrapper.accountName               = accountName;
                newAsperatoPaymentWrapper.primaryContact            = primaryContact;
                newAsperatoPaymentWrapper.nominalCode               = thisAsperatoPayment.Nominal_Code__c;
                newAsperatoPaymentWrapper.fundCode                  = fundCode;
                newAsperatoPaymentWrapper.paymentRoute              = thisAsperatoPayment.asp04__Payment_Route_Options__c;
                newAsperatoPaymentWrapper.paymentNumber             = thisAsperatoPayment.Name;
                newAsperatoPaymentWrapper.paymentReference          = thisAsperatoPayment.asp04__PSP_Reference__c;
                newAsperatoPaymentWrapper.paymentCurrency           = thisAsperatoPayment.CurrencyIsoCode;
                newAsperatoPaymentWrapper.amount                    = thisAsperatoPayment.asp04__Amount__c;
                paymentWrapperList.add(newAsperatoPaymentWrapper);     
            }
        }
        //Return CSV data or JSON data based on method execution
        return paymentFromDate == null ? asperatoDataForCSV : JSON.serialize(paymentWrapperList);
    }
    
    public class AsperatoPaymentWrapper {
        //To store Asperato Transaction Date
        public String transactionDate; 
        //To store Account Name related to Asperato Payment 
        public String accountName;      
        //To store Primary Contact related to Asperato Payment
        public String primaryContact;   
        //To store Asperato Payment Nominal Code
        public String nominalCode;      
        //To store GAU Fund Code related to Asperato Payment
        public String fundCode;  
        //To store Asperato Payment options
        public String paymentRoute;
        //To store Asperato Payment Number
        public String paymentNumber; 
        //To store Asperato Payment Reference
        public String paymentReference; 
        //To store Asperato Payment Currency
        public String paymentCurrency;
        //To store Asperato Payment Amount
        public Decimal amount; 
    }
}