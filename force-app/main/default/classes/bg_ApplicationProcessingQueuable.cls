/*****************************************************************
* bg_ApplicationProcessingQueuable
*
* Queuable Job for processing new Applications
* 
* Test Class: bg_ApplicationProcessingQueuable_Test
*
******************************************************************/

public class bg_ApplicationProcessingQueuable implements Queueable {
	
	public Set<Id> allApplicationIdsToBeProcessed = new Set<Id>();
	
	/*
	*
	* To initialise class for processing Applications
	*
	*/
	public bg_ApplicationProcessingQueuable(Set<Id> listOfApplicationIds)
	{
		this.allApplicationIdsToBeProcessed = listOfApplicationIds;
	}

	/*
	*
	* Execute method for Queuable Job
	*
	*/
	public void execute(QueueableContext context) {

        Queuable_Job_Size__c qjs = Queuable_Job_Size__c.getValues('Queuable Job Size');
        Integer jobSize = Integer.valueOf(qjs.Job_Size__c);

        List<Application__c> subsetOfApplications = [SELECT Id, Processing_Status__c, Student_Id__c, First_Name__c, Last_Name__c, 
													 Email_Address__c, Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c, 
													 Domicile__c, Domicile_Country_Code__c, Address__c, Nationality__c, Care_Leaver__c,
													 Preferred_Name__c, UCAS_School_Code__c, Studying_on_Campus__c, UCAS_Student_Id__c,
													 Admission_Cycle__c, CreatedDate, SCMS_Application_Id__c, Application_Year__c,
													 Course_Instance__c, Course_Code__c, Entry_Month__c, Entry_Month_Formula__c, Entry_Year__c, Entry_Year_Formula__c, Faculty__c,
													 Interview__c, Application_Stage__c, Offer_Status__c, Applicant_Choice__c,
													 Agent_Application__c, Mature_Student__c, Type__c, Fee_Category__c, Course__c,
													 School_College__c, Applicant__c, Origin__c, LastModifiedDate
													 FROM Application__c
													 WHERE Id IN :allApplicationIdsToBeProcessed AND Processing_Status__c = 'Ready to Process' 
													 LIMIT :jobSize];
        system.debug('subset of applications is ' + subsetOfApplications);

        if(!subsetOfApplications.isEmpty())
		{
			bg_ApplicationUtils.processApplications(subsetOfApplications);
		}

		List<Application__c> remainingApplicationsToProcess = [SELECT Id, Processing_Status__c FROM Application__c 
													WHERE Processing_Status__c = 'Ready to Process' LIMIT :jobSize];

		Set<Id> remainingApplicationIdsToProcess = (new Map<Id,Application__c>(remainingApplicationsToProcess)).keySet();

		if(!remainingApplicationIdsToProcess.isEmpty())
		{	
			if(!Test.isRunningTest())
			{
				System.enqueueJob(new bg_ApplicationProcessingQueuable(remainingApplicationIdsToProcess));
			}
		}


	}
}