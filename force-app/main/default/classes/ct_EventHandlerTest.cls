/**
 * @File Name          : ct_EventHandlerTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 08/06/2020, 5:10:58 PM
 * @Modification Log   : 
**/
@isTest
public class ct_EventHandlerTest {
    static testMethod void testupdateEventwithRegistrationURL(){
        
        Profile profile = [Select Id From Profile where Name = 'System Administrator' limit 1];
       
        User usr= new User();
        usr.ProfileID = profile.Id;
        usr.Username ='Test@Testemail.com'+ System.currentTimeMillis(); 
        usr.LastName ='TestLastname';
        usr.Email ='Test@Testemail.com';
        usr.Alias ='TestAlia';
        usr.TimeZoneSidKey ='TestCommunityNickname';
        usr.CommunityNickname ='TestCommunityNickname';
        usr.TimeZoneSidKey ='America/Los_Angeles';
        usr.LocaleSidKey ='en_US';
        usr.EmailEncodingKey ='UTF-8';
        usr.LanguageLocaleKey ='en_US';
        insert usr;
       
        Event__c evt = new Event__c();
        evt.Name = 'Test Event';
        evt.Event_Type__c = 'Open Day';
        evt.Event_Manager__c = usr.Id;
        insert evt;
        
        String registrationURL = ct_EventHandler.getBaseURLforCommunity();
        registrationURL = registrationURL + System.Label.community_page_URL_suffix;
        
        List<Event__c> events = [Select id,Event_Registration_URL__c from Event__c where id=:evt.Id];
        system.assertEquals(events[0].Event_Registration_URL__c,registrationURL+evt.Id);
    }
}