/**
 * @File Name          : ct_ContactTriggerHandler_TDTM.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 07-06-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/6/2020   Creation Admin     Initial Version
**/
global class ct_ContactTriggerHandler_TDTM extends hed.TDTM_Runnable  {
  
  public static boolean hasExecuted = false;
  global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
  hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {
    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CONTACT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();

    if(triggerAction == hed.TDTM_Runnable.Action.BeforeInsert){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CONTACT BEFORE INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

      ct_ContactTriggerHelper.setDeceasedValue(newlist);
      
    }

    if (triggerAction == hed.TDTM_Runnable.Action.BeforeUpdate){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CONTACT BEFORE UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      ct_ContactTriggerHelper.setDeceasedValue(newlist);
    }

    if (triggerAction == hed.TDTM_Runnable.Action.BeforeDelete) {
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CONTACT BEFORE DELETE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    }

    if (triggerAction == hed.TDTM_Runnable.Action.AfterInsert
    && !hasExecuted){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CONTACT AFTER INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      hasExecuted = true;

      Map<Id, Contact> triggerNewMap = new Map<Id, Contact>((list<Contact>)newlist);
          
      ct_ContactTriggerHelper.submitAlumniVerificationApproval((List<Contact>)newlist);
      
      ct_ContactHandlerWOSharing.createCommunicationDetailForContact(newlist, null);
    }

    if (triggerAction == hed.TDTM_Runnable.Action.AfterUpdate
    && !hasExecuted){
      hasExecuted = true;
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CONTACT AFTER UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      Map<Id, Contact> triggerOldMap = new Map<Id, Contact>((list<Contact>)oldlist);
      Map<Id, Contact> triggerNewMap = new Map<Id, Contact>((list<Contact>)newlist);

      ct_ContactHandlerWOSharing.createCommunicationDetailForContact(newlist, triggerOldMap);
    }

  return dmlWrapper;
  }
}