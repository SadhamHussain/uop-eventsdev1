/*****************************************************************
* bg_AffiliationUtils
*
* Utility methods relating to Affiliation__c object
* 
* Test Class: bg_AffiliationUtils_Test
******************************************************************/

public class bg_AffiliationUtils 
{
    
    
    public static void createSchoolAffiliationForUpdatedContacts(List<Contact> updatedContacts, Map<Id, Contact> oldContactsById)
    {
        List<hed__Affiliation__c> newAffiliationsToCreate = new List<hed__Affiliation__c>();
        for(Contact updatedContact : updatedContacts)
        {
            Contact oldContact = oldContactsById.get(updatedContact.Id);
            if(updatedContact.School_College__c != oldContact.School_College__c && updatedContact.School_College__C != null)
            {
                hed__Affiliation__c newAffiliation = createContactSchoolAffiliation(updatedContact.Id, updatedContact.School_College__c, 'Former', 'Student');
                newAffiliationsToCreate.add(newAffiliation);
            }
        }
        insert newAffiliationsToCreate;

    }
    
    /*
    * Create affiliation between Contact and Account
    */
    public static hed__Affiliation__c createContactSchoolAffiliation(Id contactId, Id schoolId, String status, String role)
    {
        hed__Affiliation__c aff = new hed__Affiliation__c();
        aff.hed__Contact__c = contactId;
        aff.hed__Account__c = schoolId;
        aff.hed__Status__c = status;
        aff.hed__Role__c = role;
        aff.hed__Primary__c = false;
        aff.hed__StartDate__c = Date.today();

        return aff;
    }


}