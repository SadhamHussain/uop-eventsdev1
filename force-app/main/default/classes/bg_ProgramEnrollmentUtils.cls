/*****************************************************************
* bg_ProgramEnrollmentUtils
*
* Utility methods relating to HEDA Program Enrollment object
* 
* Test Class: bg_ProgramEnrollmentUtils_Test
*  *************** Version history ***********************
* CT, Sadham  10/07/2020 - Update the code accordingly to match the new Application Management Structure,
*                                            While creating the Program Enrollment update existing Affiliation status that 
*                                            was created when Application Opportunity was inserted and link the new program enrollment with
*                                            existing Affiliation instead of creating new affiliation
******************************************************************/

public class bg_ProgramEnrollmentUtils 
{
    
    public static void createProgramEnrollmentPostApplicationEnrollment(List<Application__c> updatedApplications, Map<Id,Application__c> oldApplicationsById, Map<Id, Opportunity> relatedOppsById)
    {
        //Set of Id to collect the Applications Course Ids
        Set<Id> courseIDs      = new Set<Id>();
        //Set of Id to collect the Applicant Id 
        Set<Id> applicantIds = new Set<Id>();

        Map<String, hed__Affiliation__c> affiliationMapToUpdate = new Map<String, hed__Affiliation__c>();

        system.debug('createProgramEnrollmentPostApplicationEnrollment updatedApplications: ' + updatedApplications);
        system.debug('createProgramEnrollmentPostApplicationEnrollment oldApplicationsById: ' + oldApplicationsById);
        system.debug('createProgramEnrollmentPostApplicationEnrollment relatedOppsById: ' + relatedOppsById);

        Map<Id, hed__Program_Enrollment__c> programEnrollmentsToCreateByAppId = new Map<Id, hed__Program_Enrollment__c>();
        //Class updated by CT, Sadham for Application Management functionality
        //While creating the Program Enrollment update existing Affiliation status that was created when Application Opportunity was inserted

        for(Application__c thisUpdatedApplication : updatedApplications){
          courseIDs.add(thisUpdatedApplication.Course__c);
          applicantIds.add(thisUpdatedApplication.Applicant__c);
        }

        if(!courseIDs.isEmpty() 
          && !applicantIds.isEmpty()){
            affiliationMapToUpdate = getAffiliationMap(applicantIds, courseIDs);
        }

        for(Application__c updatedApplication : updatedApplications)
        {
            system.debug('updatedApplication.Application_Enrolled__c: ' + updatedApplication.Application_Enrolled__c);
            system.debug('oldApplicationsById.get(updatedApplication.Id).Application_Enrolled__c: ' + oldApplicationsById.get(updatedApplication.Id).Application_Enrolled__c);
			if(updatedApplication.Application_Enrolled__c && !oldApplicationsById.get(updatedApplication.Id).Application_Enrolled__c)
            {
                Date oppCloseDate;
                Opportunity relatedOpp = relatedOppsById.get(updatedApplication.Opportunity__c);
                system.debug('relatedOpp: ' + relatedOpp);
                if(relatedOpp != null)
                {
                    oppCloseDate = relatedOpp.CloseDate;
                }
                system.debug('updatedApplication: ' + updatedApplication);
                system.debug('updatedApplication.Applicant__c: ' + updatedApplication.Applicant__c);
                system.debug('updatedApplication.Course__c: ' + updatedApplication.Course__c);
                system.debug('oppCloseDate: ' + oppCloseDate);
                hed__Program_Enrollment__c newProgramEnrollment =  createProgramEnrollment(updatedApplication, updatedApplication.Applicant__c, updatedApplication.Course__c, oppCloseDate, affiliationMapToUpdate);
                system.debug('newProgramEnrollment: ' + newProgramEnrollment);
                programEnrollmentsToCreateByAppId.put(updatedApplication.Id, newProgramEnrollment);
            }
        }

        if(!affiliationMapToUpdate.isEmpty()){
          update affiliationMapToUpdate.values();
        }

        system.debug('programEnrollmentsToCreateByAppId.values(): ' + programEnrollmentsToCreateByAppId.values());
        Database.SaveResult[] createProgramEnrollments = Database.insert(programEnrollmentsToCreateByAppId.values(), false);
        List<hed__Program_Enrollment__c> insertedProgramEnrollments = programEnrollmentsToCreateByAppId.values();
        List<Id> appIds = new List<Id>(programEnrollmentsToCreateByAppId.keySet());
        Map<Id, String> errorMessageByAppId = new Map<Id, String>();
        
        for(Integer i = 0; i < createProgramEnrollments.size(); i++) 
        {
            Id appId = appIds[i];
            hed__Program_Enrollment__c pe = insertedProgramEnrollments[i];
            
            if(!createProgramEnrollments[i].isSuccess())
            {
                String errorMessage = '';
                
                for(Database.Error error : createProgramEnrollments[i].getErrors()) 
                {
                    if(String.isEmpty(errorMessage)) 
                    {
                        errorMessage = error.getMessage();
                    } 
                }
                
                errorMessageByAppId.put(appId, errorMessage);
            }
        }
        
        if(!errorMessageByAppId.isEmpty())
        {	
            bg_ApplicationUtils.updateFailedApplicationRecordsPostProcessing(errorMessageByAppId);
        }
    }


    /**
    * @description Get Affiliation record based on Course Id and Applicant Id
    * @author CT Sadham
    * @param applicantIds : Set of Applicant Ids
    * @param courseIds    : Set of Course Ids
    * @return Map<String, hed__Affiliation__c> Map of Affiliation records for Current Applicant
    **/
    private static Map<String, hed__Affiliation__c> getAffiliationMap(Set<Id> applicantIds, Set<Id> courseIds){
      Map<String, hed__Affiliation__c> thisAffiliationMap = new Map<String, hed__Affiliation__c>();

      for(hed__Affiliation__c thisAffiliation : [SELECT Id, hed__Role__c, hed__Status__c, hed__Contact__c, hed__Account__c
                                                  FROM hed__Affiliation__c
                                                  WHERE hed__Contact__c IN: applicantIds
                                                  AND hed__Account__c IN:courseIds
                                                  AND hed__Role__c =: ct_Constants.AFFILIATION_ROLE_APPLICANT
                                                  AND hed__Status__c =: ct_Constants.AFFILIATION_STATUS_CURRENT]){
              thisAffiliationMap.put(thisAffiliation.hed__Contact__c+'_'+ thisAffiliation.hed__Account__c, thisAffiliation);
      }
      return thisAffiliationMap;
    }


    /*
    * Setup of Program Enrollment record prior to Insertion
    */
    private static hed__Program_Enrollment__c createProgramEnrollment(Application__c enrolledApplication, Id contactId, Id academicProgramId, Date oppCloseDate, Map<String, hed__Affiliation__c> thisAffiliationMap)
    {
        hed__Program_Enrollment__c newProgramEnrollment = new hed__Program_Enrollment__c();
        newProgramEnrollment.hed__Contact__c = contactId;
        newProgramEnrollment.hed__Account__c = academicProgramId;
        newProgramEnrollment.hed__Start_Date__c = oppCloseDate;
        newProgramEnrollment.hed__Admission_Date__c = oppCloseDate;
        newProgramEnrollment.hed__Application_Submitted_Date__c = enrolledApplication.CreatedDate.Date();
        if(!thisAffiliationMap.isEmpty()
         && thisAffiliationMap.containsKey(contactId+'_'+academicProgramId)){
          newProgramEnrollment.hed__Affiliation__c = thisAffiliationMap.get(contactId+'_'+academicProgramId).Id;
          thisAffiliationMap.get(contactId+'_'+academicProgramId).hed__Role__c   = ct_Constants.AFFILIATION_ROLE_STUDENT;
          thisAffiliationMap.get(contactId+'_'+academicProgramId).hed__Status__c = ct_Constants.AFFILIATION_STATUS_CURRENT;
        }
        //newProgramEnrollment.hed__Graduation_Year__c = enrolledApplication.Entry_Year__c;
        newProgramEnrollment.hed__Graduation_Year__c = enrolledApplication.Entry_Year_Formula__c;
        
        switch on enrolledApplication.Mode_of_Study__c
        {
            when 'Full Time'
            {
                newProgramEnrollment.hed__Enrollment_Status__c = 'Full-Time';
            }
            when 'Part Time'
            {
                newProgramEnrollment.hed__Enrollment_Status__c = 'Half-Time';
            }
            when else
            {
                newProgramEnrollment.hed__Enrollment_Status__c = 'Full-Time';
            }
        }

        return newProgramEnrollment;
    }
}