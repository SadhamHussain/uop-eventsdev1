/**
 * @File Name          : ct_GEMPaymentTriggerHandler.cls
 * @Description        : Handler class for GEM Payment Trigger
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/8/2020, 11:37:04 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/12/2020   Creation Admin     Initial Version
**/
global class ct_GEMPaymentTriggerHandler_TDTM extends npsp.TDTM_Runnable {

  global override npsp.TDTM_Runnable.DmlWrapper run(List<SObject> newList, List<SObject> oldList,
  npsp.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {
    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>GEM PAYMENT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    npsp.TDTM_Runnable.DmlWrapper dmlWrapper = new npsp.TDTM_Runnable.DmlWrapper();

    if(triggerAction ==npsp.TDTM_Runnable.Action.BeforeInsert){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>GEM PAYMENT BEFORE INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

      Set<Id> opportunityIds                = new Set<Id>();
      Map<Id, Opportunity> opportunityMap   = new Map<Id, Opportunity>();
      for(npe01__OppPayment__c thisGEMPayment : (list<npe01__OppPayment__c>)newlist){
        if(thisGEMPayment.npe01__Opportunity__c != null){
          opportunityIds.add(thisGEMPayment.npe01__Opportunity__c);
        }
      }
      opportunityMap = ct_GEMPaymentTriggerHelper.getRelatedOpportunityRecords(opportunityIds);
      for(npe01__OppPayment__c thisGEMPayment : (list<npe01__OppPayment__c>)newlist){
        if(String.isEmpty(thisGEMPayment.npe01__Payment_Method__c)
        && opportunityMap.containskey(thisGEMPayment.npe01__Opportunity__c)
        && !String.isEmpty(opportunityMap.get(thisGEMPayment.npe01__Opportunity__c).npe03__Recurring_Donation__r.npsp__PaymentMethod__c)){
          thisGEMPayment.npe01__Payment_Method__c = opportunityMap.get(thisGEMPayment.npe01__Opportunity__c).npe03__Recurring_Donation__r.npsp__PaymentMethod__c;
        }
        if(opportunityMap.containskey(thisGEMPayment.npe01__Opportunity__c)
        && opportunityMap.get(thisGEMPayment.npe01__Opportunity__c).AccountId != null){
          thisGEMPayment.Account__c = opportunityMap.get(thisGEMPayment.npe01__Opportunity__c).AccountId;
        }
        if(opportunityMap.containskey(thisGEMPayment.npe01__Opportunity__c)
        && opportunityMap.get(thisGEMPayment.npe01__Opportunity__c).npsp__Primary_Contact__c != null){
          thisGEMPayment.Primary_Contact__c = opportunityMap.get(thisGEMPayment.npe01__Opportunity__c).npsp__Primary_Contact__c;
        }
      }
    }

    if (triggerAction == npsp.TDTM_Runnable.Action.AfterInsert){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>GEM PAYMENT AFTER INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      Map<Id, npe01__OppPayment__c> triggerNewMap = new Map<Id, npe01__OppPayment__c>((list<npe01__OppPayment__c>)newlist);
      Set<Id> paidGemPayment = new Set<Id>();
      for(npe01__OppPayment__c thisPayment : triggerNewMap.values()){
  
        if(thisPayment.npe01__Paid__c){
          paidGemPayment.add(thisPayment.Id);
        }
      }
      if(!paidGemPayment.isEmpty()){
        if(ct_CustomSettingService.getCurrentUserByPassConfiguration() == null
          || (!ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Emails_Off__c && 
             !ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Alumni_Emails_off__c)){
              ct_GEMPaymentTriggerHelper.sentEmailAlertForPaidDonation(paidGemPayment);
            }
        //Update Opportunity Payment Status to Paid once GEM payment is paid
        dmlWrapper.objectsToUpdate.addAll(ct_GEMPaymentTriggerHelper.updateOpportunityPaymentStatus(triggerNewMap, paidGemPayment));
      }
    }

    if (triggerAction == npsp.TDTM_Runnable.Action.AfterUpdate){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>GEM PAYMENT AFTER Update TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      Set<Id> paidGemPayment = new Set<Id>();
      Map<Id, npe01__OppPayment__c> triggerOldMap = new Map<Id, npe01__OppPayment__c>((list<npe01__OppPayment__c>)oldlist);
      Map<Id, npe01__OppPayment__c> triggerNewMap = new Map<Id, npe01__OppPayment__c>((list<npe01__OppPayment__c>)newlist);

      for(npe01__OppPayment__c thisPayment : triggerNewMap.values()){
          if(thisPayment.npe01__Paid__c
            && thisPayment.npe01__Paid__c != triggerOldMap.get(thisPayment.Id).npe01__Paid__c){
              paidGemPayment.add(thisPayment.Id);
          }
      }

      if(!paidGemPayment.isEmpty()){
        if(ct_CustomSettingService.getCurrentUserByPassConfiguration() == null
        || (!ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Emails_Off__c && 
          !ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Alumni_Emails_off__c)){
            ct_GEMPaymentTriggerHelper.sentEmailAlertForPaidDonation(paidGemPayment);
          }
        //Update Opportunity Payment Status to Paid once GEM payment is paid
        dmlWrapper.objectsToUpdate.addAll(ct_GEMPaymentTriggerHelper.updateOpportunityPaymentStatus(triggerNewMap, paidGemPayment));
      }
    }
    return dmlWrapper;
  }  
}