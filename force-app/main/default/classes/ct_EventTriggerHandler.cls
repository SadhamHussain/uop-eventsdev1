/**
 * @File Name          : ct_EventTriggerHandler.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 1/22/2020, 1:10:58 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    12/12/2019   Creation Admin     Initial Version
**/
public class ct_EventTriggerHandler {
   
  public static final Id EVENT_RECORDTYPE_FACULTY_RECRUITMENT_OUTREACH_EVENTS = CT_Util.getRecordTypeIdByDevName('Event__c', 'Faculty_Recruitment_and_Outreach_Event');

  /**
    * createCampaignFromEvent - Method to create Campaign records for events
    * @param eventNewMap  Event Trigger New Map 
  **/
  public static void createCampaignFromEvent(Map<Id, Event__c> eventNewMap) {
    List<Campaign> campaignListToInsert = new List<Campaign>();
    for(Event__c thisEvent : eventNewMap.values()){
      if(thisEvent.RecordTypeId == EVENT_RECORDTYPE_FACULTY_RECRUITMENT_OUTREACH_EVENTS){
        campaignListToInsert.add(createCampaignObject(thisEvent));
      }
    }
    if(!campaignListToInsert.isEmpty()){
      insert campaignListToInsert;
    }
  }

  /**
    * createCampaignObject - Method to create Campaign from an event
    * @param thisEvent  - Event record for which Campaign to be created
  **/
  public static Campaign createCampaignObject(Event__c thisEvent) {
    Campaign thisCampaign               = new Campaign();
    thisCampaign.IsActive               = true;
    thisCampaign.Name                   = thisEvent.Name;
    //Populate RAO Team Email address for sending Notification configured in Custom Label values
    thisCampaign.Rao_team_email__c      = System.Label.CT_RAO_Email_Address;
    thisCampaign.Description            = thisEvent.Event_Description__c;
    thisCampaign.EndDate                = thisEvent.Event_End_Date__c;
    thisCampaign.Event__c               = thisEvent.Id;
    thisCampaign.StartDate              = thisEvent.Event_Start_Date__c;
    thisCampaign.Year_of_Entry__c       = thisEvent.Year_of_Entry__c;
    return thisCampaign;
  }
}