/**
 * @File Name          : ct_DuplicateCheckApexAction_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 7/1/2020, 1:21:50 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/16/2019   Creation Admin     Initial Version
**/
@isTest
public class ct_DuplicateCheckApexAction_Test {

  @isTest(SeeAllData = true)
  static void getLeadDuplicateTest() {
    Lead creationLead             = ct_TestDataFactory.createStudentLead();
    Lead creationLeadDuplicate    = ct_TestDataFactory.createStudentLead();
    Contact creationContact       = ct_TestDataFactory.createContact();
    List<Lead> leadListToInsert   = new List<Lead>();
    leadListToInsert.add(creationLead);
    leadListToInsert.add(creationLeadDuplicate);
    Test.startTest();
    insert leadListToInsert;
    insert creationContact;
    
    ct_DuplicateCheckApexAction.searchLeadWrapper  thisSearchWrapper    = new ct_DuplicateCheckApexAction.searchLeadWrapper();
    thisSearchWrapper.leadFirstName       = creationLead.FirstName;
    thisSearchWrapper.leadlastName        = creationLead.LastName;
    thisSearchWrapper.leadEmailAddress    = creationLead.Email;
    thisSearchWrapper.leadRecordTypeID    = creationLead.RecordTypeId;
	  // Id [] fixedSearchResults= new Id[2];
    // fixedSearchResults[0] = creationLead.Id;
    // fixedSearchResults[1] = creationContact.Id;
    // // To confine the results to be test covered under a FIND statement
    // System.Test.setFixedSearchResults(fixedSearchResults);

    Map<String, List<dupcheck.dc3SearchResult>> searchResult;
    searchResult = new Map<String, List<dupcheck.dc3SearchResult>>();

    searchResult.put('00Q', new List<dupcheck.dc3SearchResult>());
    searchResult.put('003', new List<dupcheck.dc3SearchResult>());

    dupcheck.dc3SearchResult leadDupeResult = new dupcheck.dc3SearchResult();
    leadDupeResult.score      = 100;
    leadDupeResult.objectData = [SELECT Id, FirstName, LastName,Phone,MobilePhone,PostalCode,CreatedDate,LastModifiedDate, Name, Company, Status, RecordType.Name,RecordTypeId,RecordType.DeveloperName,Owner.Name,Record_Type_Name__c, OwnerId,Email FROM Lead WHERE Id =:creationLeadDuplicate.Id LIMIT 1];
    searchResult.get('00Q').add(leadDupeResult);

    dupcheck.dc3SearchResult contactDupe = new dupcheck.dc3SearchResult();
    contactDupe.score      = 100;
    contactDupe.objectData = [SELECT Id, FirstName, MobilePhone,LastName, Postcode__c,Student_Applicant_Id__c,Phone, Name, Email FROM Contact WHERE Id =:creationContact.Id LIMIT 1];
    searchResult.get('003').add(contactDupe);

    ct_DuplicateCheckApexAction.searchResult = searchResult;

    List<ct_DuplicateCheckApexAction.Results> duplicateResult = new List<ct_DuplicateCheckApexAction.Results>();
    duplicateResult = ct_DuplicateCheckApexAction.getDuplicateLead(new List<ct_DuplicateCheckApexAction.searchLeadWrapper>{thisSearchWrapper});
    Test.stopTest();
  }
}