/**
 * @File Name          : ct_Constants.cls
 * @Description        : Class which stores the global constants 
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 07-08-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/19/2019   Creation Admin     Initial Version
**/
public class ct_Constants {
  
  //--------------------------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------   SOBJECT Constants    ------------------------------------------------------------------/


  /***************************************************   Lead Constants    ******************************************************************/
  //Lead  Picklist values Constants

  //Lead Source	: LeadSource
  public static final string LEAD_SOURCE_APPLICATION   = 'Application';

  // NOT USED ANY WHERE
  //public static final Id LEAD_RECORDTYPE_BUSINESS   = ct_Util.getRecordTypeIdByDevName('Lead', 'B2B_R_I_Leads');




  /***************************************************   Account Constants    ******************************************************************/
  
  //Account RecordType Id Constants
  public static final Id ACCOUNT_RECORDTYPE_STUDENT_ORGANISATION    = ct_Util.getRecordTypeIdByDevName('Account', 'Sports_Organization');
  public static final Id ACCOUNT_RECORDTYPE_ADMINISTRATIVE          = ct_Util.getRecordTypeIdByDevName('Account', 'Administrative');
  public static final Id ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM        = ct_Util.getRecordTypeIdByDevName('Account', 'Academic_Program');
  public static final Id ACCOUNT_RECORDTYPE_EDUCATIONAL_INSTITUTION = ct_Util.getRecordTypeIdByDevName('Account', 'Educational_Institution');

  //Account Record Type Name Constants
  public static final string ACCOUNT_BUSINESS_ORGANISATION_RECORDTYPE_NAME       = 'Business_Organization';




  /***************************************************   Contact Constants   ******************************************************************/
  
  //Contact RecordType Id Constants
  public static final Id CONTACT_RECORDTYPE_UOPSTUDENT = ct_Util.getRecordTypeIdByDevName('Contact', 'UOP_Contact');

  //Contact Picklist values Constants

  //Donor Type : Donor_Type__c
  public static final string CONTACT_DONOR_TYPE_COMMUNITY_DONOR   = 'Community Donor (Less than £1k)';
  public static final string CONTACT_DONOR_TYPE_LEADERSHIP_DONOR  = 'Leadership Donor (£1k to £5k)';
  public static final string CONTACT_DONOR_TYPE_MAJOR_DONOR       = 'Major Donor (£5k to £10k)';
  public static final string CONTACT_DONOR_TYPE_PRINCIPAL_DONOR   = 'Principal Donor (£10k+)';

  //Donor Status : Donor_Status__c
  public static final string CONTACT_DONOR_STATUS_PROSPECT    = 'Prospect (Open Opp)';
  public static final string CONTACT_DONOR_STATUS_PLEDGED     = 'Pledged (Pledged Opp)';
  public static final string CONTACT_DONOR_STATUS_CURRENT     = 'Current (CW Last 12 Months)';
  public static final string CONTACT_DONOR_STATUS_LAPSED      = 'Lapsed (CW 12 Months+)';

  //Volunteer Status : GW_Volunteers__Volunteer_Status__c
  public static final String VOLUNTEER_STATUS_ACTIVE        = 'Active';
  public static final String VOLUNTEER_STATUS_PROSPECTIVE   = 'Prospective';
  
  //Community Relationship : Community_Relationship__c
  public static final String COMMUNITY_RELATIONSHIP_STAFF   = 'Staff';

  //Lead Source	: LeadSource
  public static final String ONLINE_DONATION_CONTACT_LEAD_SOURCE        = 'Online Donation';
  public static final String ALUMNI_REGISTRATION_CONTACT_LEAD_SOURCE    = 'Alumni Registration';
  public static final String VOLUNTEER_REGISTRATION_CONTACT_LEAD_SOURCE = 'Online Volunteer Registration';

  //Preferred Email Type : hed__Preferred_Email__c
  public static final String CONTACT_PREFERRED_EMAIL_TYPE_ALTERNATE     = 'Alternate';

  // NOT USED ANY WHERE Contact Donor Type Constants
  // public static final string PRINCIPAL_DONOR_TYPE   = 'Principal Donor';
  // public static final string MAJOR_DONOR_TYPE       = 'Major Donor';
  // public static final string STANDARD_DONOR_TYPE    = 'Standard Donor';



  /***************************************************   Opportunity Constants   **************************************************************/

  //Opportunity RecordType Id Constants
  public static final Id OPPORTUNITY_RECORDTYPE_DONATION_PLEDGE        = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Donation');
  public static final Id OPPORTUNITY_RECORDTYPE_LEGACY_DONATION_PLEDGE = ct_Util.getRecordTypeIdByDevName('Opportunity', 'ADV_Legacy_Pledge');
  public static final Id OPPORTUNITY_RECORDTYPE_COURSE_ADMISSION       = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Application');
  public static final Id OPPORTUNITY_RECORDTYPE_VOLUNTEER              = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Volunteer_Recruitment');
  public static final Id OPPORTUNITY_RECORDTYPE_MAJOR_DONATION         = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Major_Donation');
  public static final Id OPPORTUNITY_RECORDTYPE_IN_KIND_GIFT           = ct_Util.getRecordTypeIdByDevName('Opportunity', 'InKindGift');

  //Opportunity Picklist values Constants
  
  //Stage : StageName
  public static final string OPPORTUNITY_STAGE_CLOSED_STEWARDSHIP         = 'Closed / Stewardship';
  public static final string DONATION_OPPORTUNITY_STAGE_CLOSED_COMPLETE   = 'Complete';
  public static final string ACTIVE_OPPORTUNITY_STAGE                     = 'Active';
  public static final string NEW_PLEDGE_OPPORTUNITY_STAGE                 = 'New Pledge';
  public static final string PROSPECTING_OPPORTUNITY_STAGE                = 'Prospecting';
  public static final string PLEDGE_OPPORTUNITY_STAGE                     = 'Pledge';
  public static final string OPPORTUNITY_STAGE_ENQUIRER                   = 'Enquirer';
  public static final string OPPORTUNITY_STAGE_APPLIED                    = 'Applied';
  public static final string OPPORTUNITY_STAGE_DECLINED_OFFER             = 'Closed - Declined Offer';
  public static final string OPPORTUNITY_STAGE_NOT_OFFER                  = 'Closed - Not Offered';
  public static final string OPPORTUNITY_STAGE_WITHDRAWN                  = 'Closed - Withdrawn';
  public static final string OPPORTUNITY_STAGE_CLOSED_WON                 = 'Closed Won';
  public static final string OPPORTUNITY_STAGE_CLOSED_LOST                = 'Closed Lost';
  public static final string OPPORTUNITY_STAGE_NEW                        = 'New';
  public static final string OPPORTUNITY_STAGE_CLOSED_ENROLLED            = 'Closed - Enrolled';
  public static final string OPPORTUNITY_STAGE_OFFERED                    = 'Offered';
  public static final string OPPORTUNITY_STAGE_DEFERRED                   = 'Deferred';
  public static final string OPPORTUNITY_STAGE_OFFER_ACCEPTED             = 'Offer Accepted';
  
  //Closed Lost Reason	: Lost_Reason__c
  public static final string OPPORTUNITY_LOST_REASON_DIRECT_DEBIT_CANCELLED = 'Direct Debit Cancelled';
  public static final string OPPORTUNITY_LOST_REASON_PAUSED_DIRECT_DEBIT    = 'Paused Direct Debit';
  
  //Source : Source__c
  public static final string OPPORTUNITY_SOURCE_SELF_IDENTIFIED = 'Self Identified';

  //Donation Size	: Donation_Size__c
  public static final string SMALL_GIFT_SIZE      = 'Small Gift (Up to £999)';
  public static final string MEDIUM_GIFT_SIZE     = 'Medium Gift (£1k to £4,999)';
  public static final string LARGE_GIFT_SIZE      = 'Large Gift (£5k+)';

  //Type : Type
  public static final string OPPORTUNITY_SINGLE_DONATION_TYPE       = 'One-Off Donation';
  public static final string OPPORTUNITY_RECURRING_DONATION_TYPE    = 'Regular Donation';
  
  

  

  
  
  

  //Asperato Authorisation Status
  public static final string ASPERATO_AUTHORISATION_STATUS_CANCELLED = 'Cancelled';

  // Consent Source Values
  public static final String CONSENT_ONLINE_DONATION_VALUE  = 'Online Donation';
  public static final String CONSENT_VOLUNTEER_REGISTRATION_VALUE = 'Volunteer Registration';
  
 
  
  //Fundrasing Project Status
  public static final string FUNDRASING_PROJECT_IN_PROGRESS   = 'In Progress';
  
 //Fundrasing Project Task Status
 public static final string PROJECT_TASK_STATUS_ACTIVE   = 'Active';
//Fundrasing Project Task Priority
 public static final string PROJECT_TASK_PRIORITY_LOW   = 'Low';
  
  //Asperato Payment Stage
  public static final string ASPERATO_PAYMENT_STAGE_COLLECTED = 'Collected from customer';


  //Email Template Unique Name
  public static final String THANK_DONOR_FOR_DONATION_EMAIL_TEMPLATE = 'Thank_Donor_Email_Template';
  public static final String DIRECT_DEBIT_EMAIL_TEMPLATE = 'Direct_Debit_Payment_Schedule_Notification';
  public static final String BUSINESS_LEAD_FORM_TEMPLATE = 'ct_Business_Lead_Form_Template';
  public static final String VOLUNTEER_WELCOME_EMAIL = 'Volunteer_Welcome_Email';
  public static final String CREDIT_DEBIT_CARD_PAYMENT_EMAIL = 'Credit_Debit_Card_Payment_Email';
  public static final String SEND_THANK_COMMS = 'Send_Thank_You_Comms';

  //OPPORTUNITY SEND TRANSACTIONAL EMAIL
  public static final String OPPORTUNITY_SEND_TRANSACTIONAL_EMAIL_YES = 'Yes';

  //Organization Wide Address
  public static final String UOP_BUSINESS_SUPPORT_ADDRESS = 'rohit.arora+uopbusiness@port.ac.uk';
  public static final String UOP_ADVANCEMENT_TEAM_ADDRESS = System.Label.ct_UOP_Advancement_Team_Address;

  //Event Registration
  public static final String EVENT_REGISTRATION_CHECKED_IN = 'Checked In';
    
  //Web Submission Source
  public static final String WEB_SUBMISSION_SOURCE_ALUMNI = 'Alumni';
  public static final String WEB_SUBMISSION_SOURCE_DONATION = 'Donation';
  public static final String WEB_SUBMISSION_SOURCE_VOLUNTEER = 'Volunteer';

  //Web Form Name
  public static final String WEB_FORMDATA_SOURCE_ALUMNI = 'Alumni Registration';
  // Direct Debit Form Name
  public static final String WEB_FORMDATA_SOURCE_DIRECT_DEBIT = 'Direct Debit';

  //Alumni Verififcation Status values
  public static final String ALUMNI_VERIFICATION_UNVERIFIED = 'Unverified';

  //Opportunity Email Acknowledgment Status
  public static final String EMAIL_ACKNOWLEDGMENT_STATUS_ACKNOWLEDGED = 'Acknowledged';

  //Advancement Setting MetaData
  public static final String HIGH_VALUE_ONLINE_DONATION_ADVANCEMENT_SETTING = 'High_Value_Online_Donation';
  public static final String WEB_DONATION_FORM_CARD_SUCCESS_MESSAGE = 'Web_Donation_Form_Card_Success_Message';
  public static final String WEB_DONATION_FORM_DD_SUCCESS_MESSAGE = 'Web_Donation_Form_DD_Success_Message';
  public static final String PAYPAL_THANK_YOU_REDIRECT_URL = 'Paypal_Thank_You_Redirect_URL';
  public static final String PAYPAL_BUTTON_VISIBILITY_CONFIG = 'Show_Paypal_Button';
  public static final String OPPORTUNITY_RECORD_TYPE_AAA_PRIVATE_EXCLUSION_SETTING = 'Opportunity_Type_AAA_Private_Exclusion';

  public static final String WEB_DONATION_PAYMENT_DETAILS = 'Web_Donation_Payment_Details';
  public static final String BOUNCE_ALERT_USER = 'Bounce_Alert_User';

  //Site Domain Setting MetaData
  public static final String PERSONAL_SITE_CONTACT_INFO_URL = 'PersonalSiteContactInfoURL';
  public static final String VOLUNTEER_REGISTRATION_SITE_URL = 'Volunteer_Registration_Site_URL';
  


  //Payment Frequency
  public static final String PAYMENT_FREQUENCY_SINGLE = 'Single';
  public static final String PAYMENT_FREQUENCY_MONTHLY = 'Monthly';

  //Payment Method Values
  public static final String DIRECT_DEBIT_PAYMENT_METHOD = 'Direct Debit';
  public static final String CASH_PAYMENT_METHOD = 'Cash';
  public static final String PAYPAL_PAYMENT_METHOD = 'Paypal';
  public static final String ASPERATO_CARD_PAYMENT_METHOD = 'Card';
  public static final String GEM_CARD_PAYMENT_METHOD = 'Credit / Debit Card';

  //Authorization Open Ended Status
  public static final String AUTHORIZATION_OPEN_ENDED_STATUS = 'Open';
  public static final String AUTHORIZATION_INFORCE_STATUS = 'In Force';
  public static final String INSTALLMENT_PERIOD = 'Monthly';

  //Recurring Donation ScheduleType Values
  public static final String SCHEDULE_TYPE_MULTIPLE_BY = 'Multiply By';

   //Recurring Donation Direct Debit Cancellation Reason
   public static final String DD_CANCELLATION_REASON_SYSTEM_CANCELLATION = 'System Cancellation';

  //Direct Debit Authorisation Status
  public static final String DIRECT_DEBIT_AUTHORISATION_STATUS_CANCELLED = 'Cancelled';
   //Direct Debit Cancellation
   public static final String DD_CANCELLATION_MANUAL_DD_CANCELLATION     = 'Manual DD Cancellation';
   public static final String DD_CANCELLATION_GOCARDLESS_DD_CANCELLATION = 'GoCardless DD Cancellation';
   //Direct Debit Open Ended Status
   public static final String DIRECT_DEBIT_OPEN_ENDED_CLOSED = 'Closed';
  
  //Enquiry Source
  public static final String ENQUIRY_SOURCE_WEB_FORM  = 'Web-Form';
 // Schema SobjectType
 public static final Schema.SObjectType  IS_RECURRING_DONATION   = npe03__Recurring_Donation__c.sObjectType.getDescribe().getSobjectType();

  //Recurring Donation Name Format
  public static final String RECURRING_DONATION_NAME = ' - Web Direct Debit - Open Term - ';

  //One-Off Donation Opportunity Name Format
  public static final String ONE_OFF_OPPORTUNITY_NAME = ' - Web Donation - ';

  //Volunteer Opportunity Name Format
  public static final String VOLUNTEER_OPPORTUNITY_NAME = ' - Volunteer - ';

   //Legacy Opportunity Name Format
   public static final String LEGACY_OPPORTUNITY_NAME = ' - Legacy - ';

  //Asperato Payment Stage
  public static final String PAYMENT_STAGE_NEW = 'New';

  //HED AFFILIATION Role
  public static final String AFFILIATION_ROLE_APPLICANT                 = 'Applicant';
  public static final String AFFILIATION_ROLE_STUDENT                   = 'Student';
  public static final String AFFILIATION_ROLE_PROSPECT                  = 'Prospect';
  public static final String AFFILIATION_ROLE_CAREERS_ADVISER           = 'Careers Adviser';
  public static final String AFFILIATION_ROLE_CAREER_ENRICHMENT_LEAD    = 'Careers & Enrichment Lead';
  public static final String AFFILIATION_ROLE_EPQ_COORDINATOR           = 'EPQ Coordinator';
  public static final String AFFILIATION_ROLE_HEAD_TEACHER_PRINCIPAL    = 'Head Teacher/Principal';
  public static final String AFFILIATION_ROLE_GOVERNOR                  = 'Governor';
  public static final String AFFILIATION_ROLE_HEAD_OF_SIXTH_FORM        = 'Head of sixth Form';
  public static final String AFFILIATION_ROLE_BUSINESS_MANAGER          = 'Business Manager';
  public static final String AFFILIATION_ROLE_PASTORAL_LEAD             = 'Pastoral Lead';
  public static final String AFFILIATION_ROLE_SUBJECT_TEACHER           = 'Subject teacher';

  //HED AFFILIATION Status
  public static final String AFFILIATION_STATUS_CURRENT = 'Current';
  public static final String AFFILIATION_STATUS_FORMER  = 'Former';
  public static final String AFFILIATION_STATUS_ALUMNI  = 'Alumni';

  //HED UOP Academic Affiliations 
  public static final String UOP_ACADEMIC_AFFILIATIONS_CURRENT_PROSPECT  = 'Current Prospect';
  public static final String UOP_ACADEMIC_AFFILIATIONS_CURRENT_APPLICANT = 'Current Applicant';
  public static final String UOP_ACADEMIC_AFFILIATIONS_CURRENT_STUDENT   = 'Current Student';
  public static final String UOP_ACADEMIC_AFFILIATIONS_CURRENT_ALUMNI    = 'Current Alumni';
  public static final String UOP_ACADEMIC_AFFILIATIONS_REJECTED_APPLICATIONS = 'Rejected Applications';

  //Opportunity Payment Status
  public static final String OPPORTUNITY_PAYMENT_STATUS_INITIATED = 'Initiated';
  public static final String OPPORTUNITY_PAYMENT_STATUS_PAID = 'Paid';

 
  //Task fields values
  public static final String TASK_SUBJECT            = 'Contact Submitted from Alumni Form';
  public static final String TASK_PRIORITY_NORMAL    = 'Normal';
  public static final String TASK_STATUS_COMPLETED   = 'Completed';
  public static final String TASK_DESCRIPTION        = 'This contact is submitted from the alumni registration form';

  //Account Team Member role
  public static final String  ACCOUNT_TEAM_MEMBER_ROLE_CONTACT_MANAGER = 'Contact Manager';

  //Account Team Member Access
  public static final String  ACCOUNT_TEAM_MEMBER_READ_EDIT_ACCESS = 'Edit';

  // Volunteer Hours Status value
  public static final String VOLUNTEER_COMPLETED_STATUS = 'Completed';
  public static final String VOLUNTEER_PROSPECT_STATUS = 'Prospect';

  //Webform Records Default Users Metadata Developer Names
  public static final String ALUMNI_REGISTRATION_WEB_FORM       = 'Alumni_Registration';
  public static final String VOLUNTEER_REGISTRATION_WEB_FORM    = 'Volunteer_Registration';
  public static final String WEB_DONATION_WEB_FORM              = 'Web_Donation';
  public static final String LEGACY_DONATION_OPPORTUNITY        = 'Legacy_Donation';

  // GiftAid - Type of Declaration values
  public static final String ELECTRONIC_TYPE_DECLARATION = 'Electronic';

  //Alumni Willing To Gift To The University
  public static final String ALUMNI_WILLING_TO_GIFT = 'Leaving a gift to the University in my will';

  //Contact Alumni Verification Status
  public static final String CONTACT_ALUMNI_VERIFICATION_STATUS_UNVERIFIED = 'Unverified';

}