/*******************************************************************************************************************************
* @name:           rl_LiveChatController_Test
* @description:    Contains test methods for rl_LiveChatController
* 
*
* METHODS
* @ setup							Creates data for shared use during the test run
* @ testRegisterRating				Tests registerRating() method
* @ testGetPicklistOptions			Tests getPicklistOptions() method
* @ testSubmitForm					Tests submitForm() & initialisePostChat() methods
* @ testUpdateRelatedCaseRating		Tests updateRelatedCaseRating method
*
*
* @Created:   24-04-2020 v0.1 - initial version - Rob Liesicke
**************** Version history ***********************
* @Updated:   27-04-2020 v0.2 - update test scripts for updated functionality - Rob Liesicke
* CT, Dinesh : 10/07/2020 - Updated the Code to get the Record Type Id by Developer Name instead of Record Type Name
********************************************************************************************************************************/

@IsTest
public class rl_LiveChatController_Test {

    
/* =========================================================== */   
/*                       SETUP TEST DATA                       */
/* =========================================================== */ 
    
    @testSetup static void setup() 
    {
        
        // Insert a test lead
        Lead testLead = bg_UnitTestDataFactory.createEnquirer();
        testLead.LastName = 'testlead1';
        insert testLead;
        
        
        // Insert a test case of record type Clearing Enquiry
        //CT, Dinesh : 10/07/2020 : Updated the Code to get the Record Type Id by Developer Name instead of Record Type Name
        Id caseRecordTypeId = bg_CommonUtils.getRecordTypeID('Case', 'Clearing_Enquiry');
        Case testCase = bg_UnitTestDataFactory.createEnquiry();
        testCase.Enquirer__c = testLead.Id;
        testCase.Subject = 'testcase1';
        testCase.Origin = 'Live Chat';
        testCase.RecordTypeId = caseRecordTypeId;
        insert testCase;   
        
        
        // Insert 200 Leads and 200 Cases for bulk testing
        bg_UnitTestDataFactory.insert200LeadsWithCases();  
    }

    
/* =========================================================== */   
/*                      TEST REGISTER RATING                   */
/* =========================================================== */ 
    
    static testMethod void testRegisterRating()
    {
        
		rl_LiveChatController liveChatController = new rl_LiveChatController();
        
        liveChatController.showSubmit= false;
        
        
        test.startTest();
        
        	// This method is called in JavaScript OnClick when a radio selection is made
            liveChatController.registerRating();
        
        test.stopTest();
        
        // Method should update the showSubmit variable to true
        System.assertEquals(liveChatController.showSubmit, true);
        
    }
    
    
/* =========================================================== */   
/*                   TEST GET PICKLIST OPTIONS                 */
/* =========================================================== */  
    
    static testMethod void testGetPicklistOptions()
    {
        test.startTest();
        	
        	// Pass method object name and field name of picklist field to obtain picklist values
            List<rl_LiveChatController.picklistValuesWrapper> picklistValues = rl_LiveChatController.getPicklistOptions('Case', 'Primary_Subject_Area_Interest__c');
        
        test.stopTest();
        
        // Ensure returned list is not null
        system.assertNotEquals(null, picklistValues, 'Success');
        
    }
    
    
/* =========================================================== */   
/*                       TEST SUBMIT FROM                      */
/* =========================================================== */     
    
    static testMethod void testSubmitForm() 
    {
        // Get Lead and Case for testing 
        String testLeadId = [ SELECT Id, LastName FROM Lead WHERE LastName = 'testlead1' LIMIT 1 ].Id;
        String testCaseId = [ SELECT Id, Subject FROM Case WHERE Subject = 'testcase1' LIMIT 1 ].Id;
        
        
        // Create JSON String in expected format using Lead Id and Case Id e.g. {"LeadId":"00Q1234567890AB","CaseId":"0051234567890AB"}
        String attachedRecordsJsonString = '{"LeadId":"' + testLeadId + '","CaseId":"' + testCaseId + '"}';

        
        // Simulate loading visualforce page "rl_Clearing_PostChat_Page"
        PageReference postChatPage = Page.rl_Clearing_PostChat_Page;
		Test.setCurrentPage(postChatPage);

        
        // Update current page variable with JSON String
        ApexPages.currentPage().getParameters().put('attachedRecords', attachedRecordsJsonString);

        
        // Create instance of page controller and call method that is called on page initiation
        rl_LiveChatController liveChatController = new rl_LiveChatController();
        liveChatController.initialisePostChat();
        
        
        // Set variables to simulate submitted form
        liveChatController.rating = '5';
        liveChatController.comments = 'Test Comment';
        
        
        // Call submitForm method on the instance of the liveChatController
        test.startTest();
        
        	liveChatController.submitForm();
        
        test.stopTest();
        
        
        // Query for Post_Chat_Submission__c that should be created
        Post_Chat_Submission__c insertedPostChatSubmission = 	[ SELECT Id, LiveChat_Rating__c, LiveChat_RatingComment__c, Related_Case_Id__c
                                  				  				  FROM Post_Chat_Submission__c
                                  				  				  WHERE Related_Case_Id__c = :testCaseId
                                								];
        
        
        // Check that the form data is successfully captured in Post_Chat_Submission__c record
        system.assertEquals(insertedPostChatSubmission.LiveChat_Rating__c, '5');
        system.assertEquals(insertedPostChatSubmission.LiveChat_RatingComment__c, 'Test Comment');        
        
    }
    
    
/* =========================================================== */   
/*                  TEST RELATED CASE IS UPDATED               */
/* =========================================================== */ 
    
    static testMethod void testUpdateRelatedCaseRating() 
    {    
    	
        // List to hold Post_Chat_Submission__c records for bulk insert
        List<Post_Chat_Submission__c> postChatSubmissionsToInsert = new List<Post_Chat_Submission__c>();  

        // List to hold Post_Chat_Submission__c records for bulk insert
        List<Id> insertedPostChatSubmissions = new List<Id>();  
        
        
        // Get list of cases, will be 200 due to test setup
		List<Case> casesToUpdate = 	[ SELECT Id, LiveChat_Rating__c, LiveChat_RatingComment__c, Subject
                                  	  FROM Case
                                      WHERE Subject != 'testCreateEmailSurveyRecord'
                                	];    
        
        
        // For each case, create a Post_Chat_Submission__c record and add them to list for bulk insert
        for (Case caseToUpdate : casesToUpdate)
        {
            Post_Chat_Submission__c postChatSubmission = new Post_Chat_Submission__c();
            
            postChatSubmission.LiveChat_Rating__c = '1';
            postChatSubmission.LiveChat_RatingComment__c = 'Test Comment';            
            postChatSubmission.Related_Case_Id__c = caseToUpdate.Id;
            
            postChatSubmissionsToInsert.add(postChatSubmission);

        }  
        
   		insert postChatSubmissionsToInsert;
        
        // Create list of the inserted Ids
        for (Post_Chat_Submission__c insertedPCS : postChatSubmissionsToInsert)
        {        
        	insertedPostChatSubmissions.add(insertedPCS.Id);
        }
        
        
        test.startTest();
        
			// Bulk insert Post_Chat_Submission__c records, to test updateRelatedCaseRating() fired by Process Builder on record creation
        	rl_LiveChatController.updateRelatedCaseRating(insertedPostChatSubmissions);
        
        test.stopTest(); 
        
        
        // Boolean to check all Cases are updated with a Rating
        Boolean allCasesUpdated;
        
		
        // Get list of cases, will be 200 due to test setup
        List<Case> casesToCheck = 	[ SELECT Id, LiveChat_Rating__c, LiveChat_RatingComment__c, Subject
                                  	  FROM Case
                                      WHERE Subject != 'testcase1'
                                	];
        
        
        // Loop through the Cases to check all cases were updated
        for( Case caseToCheck : casesToCheck)
        {
            
            // If Rating related fields are populated, set boolean to true
            if( caseToCheck.LiveChat_Rating__c == '1' && caseToCheck.LiveChat_RatingComment__c == 'Test Comment')
            {
                
                allCasesUpdated = true;
                
            }
            
            // Else set Boolean to false and exit loop 
            else
            {
                
                allCasesUpdated = false;
                
                break;
                
            }    
        }
         
        
        // Ensure all Cases have updated with Rating
        System.assertEquals(allCasesUpdated,true); 
        
    }    
    
}