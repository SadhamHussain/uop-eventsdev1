/**
 * @File Name          : ct_ProjectExpenseTriggerHelper_Test.cls
 * @Description        : Test class for ct_ProjectExpenseTriggerHelper,ct_ProjectExpenseTriggerHandler_TDTM
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/19/2020, 12:09:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/19/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_ProjectExpenseTriggerHelper_Test {

    testMethod static void unit1(){
        // first retrieve default EDA trigger handlers
       List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_ProjectExpenseTriggerHandler_TDTM', 'Project_Expense__c', 'AfterInsert;AfterUpdate;AfterDelete;BeforeUpdate', 1.00));

       // Pass trigger handler config to set method for this test run
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        Project__c thisProject = ct_TestDataFactory.createProject();
		insert thisProject;
		Project_Task__c thisTask = ct_TestDataFactory.createProjectTask(thisProject.Id);
		insert thisTask;
        Test.startTest();
        Project_Expense__c thisExpense = ct_TestDataFactory.createProjectExpense(thisProject.Id,thisTask.Id);
		insert thisExpense;
        delete thisExpense;
        System.assertEquals(thisExpense.Project__c, thisProject.Id);
        Test.stopTest();
    }
    testMethod static void unit2(){
        // first retrieve default EDA trigger handlers
       List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_ProjectExpenseTriggerHandler_TDTM', 'Project_Expense__c', 'AfterInsert;AfterUpdate;AfterDelete;BeforeUpdate', 1.00));

       // Pass trigger handler config to set method for this test run
        hed.TDTM_Global_API.setTdtmConfig(tokens);
        
        Project__c thisProject = ct_TestDataFactory.createProject();
		insert thisProject;
        Project_Task__c thisTask = ct_TestDataFactory.createProjectTask(thisProject.Id);
		insert thisTask;
        Project_Task__c thisTask1 = ct_TestDataFactory.createProjectTask(thisProject.Id);
		insert thisTask1;
        Test.startTest();
        Project_Expense__c thisExpense = ct_TestDataFactory.createProjectExpense(thisProject.Id,thisTask.Id);
		insert thisExpense;
        thisExpense.Project_Task__c = thisTask1.Id;
        update thisExpense;
        System.assertEquals(thisExpense.Project_Task__c, thisTask1.Id);
        Test.stopTest();
    }
}