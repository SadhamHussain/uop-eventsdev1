/*****************************************************************
* bg_ContactConsentManager
*
* Manages creation of consent records for new Contacts
* 
* Test Class: bg_ContactConsentManager_Test
************** Version history ***********************
* CT, Rajesh : 01/05/2020 - Added Alumni Consent Preference fields
*
******************************************************************/
public with sharing class bg_ContactConsentManager {


    public static void createAndInsertConsentsForNewContacts(List<Contact> newContacts)
    {
        string profileName = bg_UserUtils.getProfileName(UserInfo.getProfileId());
        List<Consent__c> consentsToCreate = new List<Consent__c>();
        for(Contact newContact : newContacts)
        {
            Consent__c consentToCreate = createContactConsentRecordFromCreatedContact(newContact, profileName);
            consentsToCreate.add(consentToCreate);
        } 
        insert consentsToCreate;
        
    }

    private static Consent__c createContactConsentRecordFromCreatedContact(Contact consentContact, string profileName)
    {
        Consent__c newConsent                                 = new Consent__c();
        newConsent.Campaign__c                                = consentContact.Latest_Consent_Change_Campaign__c;
        newConsent.Contact__c                                 = consentContact.Id;
        newConsent.Directmail_Consent_Prior__c                = 'Undetermined';
        newConsent.Directmail_Consent__c                      = consentContact.Directmail_Consent__c;
        newConsent.Effective_Date__c                          = Date.today();
        newConsent.Email_Consent_Prior__c                     = 'Undetermined';
        newConsent.Phone_Consent_Prior__c                     = 'Undetermined';
        newConsent.Phone_Consent__c                           = consentContact.Phone_Consent__c;
        newConsent.SMS_Consent_Prior__c                       = 'Undetermined';
        newConsent.SMS_Consent__c                             = consentContact.SMS_Consent__c;
        newConsent.Whatsapp_Consent_Prior__c                  = 'Undetermined';
        newConsent.Whatsapp_Consent__c                        = consentContact.WhatsApp_Consent__c;
        //Added by CT, Rajesh on 01/05/2020 : To record Alumni Consent Communication Preferences
        //Do not Contact
        newConsent.New_Alumni_Do_not_Contact__c               = consentContact.Alumni_Do_not_Contact__c;
        newConsent.New_Alumni_Do_not_Contact_Notes__c        = consentContact.Alumni_Do_not_Contact_Notes__c;
        newConsent.New_Alumni_Do_not_Contact_Source__c        = consentContact.Alumni_Do_not_Contact_Source__c;
        newConsent.New_Alumni_Do_Not_Contact_Effective_Date__c= consentContact.Alumni_Do_Not_Contact_Effective_Date__c;
        //Alumni Email
        newConsent.New_Alumni_Email_Consent__c                = consentContact.Alumni_Email_Consent__c;
        newConsent.New_Alumni_Email_Consent_Notes__c         = consentContact.Alumni_Email_Consent_Notes__c;
        newConsent.New_Alumni_Email_Consent_Source__c         = consentContact.Alumni_Email_Consent_Source__c;
        newConsent.New_Alumni_Email_Consent_Effective_Date__c = consentContact.Alumni_Email_Consent_Effective_Date__c;
        //Alumni Directmail
        newConsent.New_Alumni_Directmail_Consent__c           = consentContact.Alumni_Directmail_Consent__c;
        newConsent.New_Alumni_Directmail_Consent_Notes__c    = consentContact.Alumni_Directmail_Consent_Notes__c;
        newConsent.New_Alumni_Directmail_Consent_Source__c    = consentContact.Alumni_Directmail_Consent_Source__c;
        newConsent.New_Alumni_Directmail_Consent_Eff_Date__c  = consentContact.Alumni_Directmail_Consent_Effective_Date__c;
        //Alumni Phone
        newConsent.New_Alumni_Phone_Consent__c                = consentContact.Alumni_Phone_Consent__c;
        newConsent.New_Alumni_Phone_Consent_Notes__c         = consentContact.Alumni_Phone_Consent_Notes__c;
        newConsent.New_Alumni_Phone_Consent_Source__c         = consentContact.Alumni_Phone_Consent_Source__c;
        newConsent.New_Alumni_Phone_Consent_Effective_Date__c = consentContact.Alumni_Phone_Consent_Effective_Date__c;
        //Alumni SMS
        newConsent.New_Alumni_SMS_Consent__c                  = consentContact.Alumni_SMS_Consent__c;
        newConsent.New_Alumni_SMS_Consent_Notes__c           = consentContact.Alumni_SMS_Consent_Notes__c;
        newConsent.New_Alumni_SMS_Consent_Source__c           = consentContact.Alumni_SMS_Consent_Source__c;
        newConsent.New_Alumni_SMS_Consent_Effective_Date__c   = consentContact.Alumni_SMS_Consent_Effective_Date__c;
        //Preset Old values as 'Undetermined'
        newConsent.Alumni_Email_Consent__c                    = 'Undetermined';
        newConsent.Alumni_Phone_Consent__c                    = 'Undetermined';
        newConsent.Alumni_SMS_Consent__c                      = 'Undetermined';
        newConsent.Alumni_Directmail_Consent__c               = 'Undetermined';
        
        if(profileName == 'System Administrator')
        {
            if(consentContact.Latest_Consent_Change_Source__c == 'Preference Centre')
            {
                newConsent.Change_Source__c = 'Preference Centre';
            }
            else
            {
                newConsent.Change_Source__c = 'Webform';    
            }
        }
        else
        {
            newConsent.Change_Source__c = 'Internal User';    
        }
        return newConsent;
    }


    
}