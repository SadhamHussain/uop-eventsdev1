/*****************************************************************
* bg_FindDuplicateRecords
*
* methods to utilise standard Duplicate Matching Rules in Apex
* 
* Test Class: bg_FindDuplicateRecords_Test
*
* Author: BrightGen
* Created: 26-11-2018
******************************************************************/

public class bg_FindDuplicateRecords {

  /*
  *
  * Creates Map of One to Many Enquirers that match Applicant Information
  *
  */
    public static Map<Id, Set<Lead>> checkForApplicationsRelatedToZeroToManyEnquirers(List<Lead> tempEnquirers) 
    {
        Map<Id, Lead> appIdByLead = new Map<Id, Lead>();
        for(Lead newLead : tempEnquirers)
        {
            appIdByLead.put(newLead.Most_Recent_Application__c, newLead);   
        }
        Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(tempEnquirers);
        System.debug('duplicate results ' + results);
        
        Map<Id, Set<Lead>> enquirerSetByAppIdMap = new Map<Id, Set<Lead>>();
        
        Map<Id, Map<String, Set<Lead>>> enquirerSetByRuleNameByAppIdMap = new Map<Id, Map<String, Set<Lead>>>();
        
        for(Integer i = 0; i < results.size(); i++)
        {
            Integer enquirerResultSize = 0;
            
            Map<String, Set<Lead>> leadResultsByRuleName = new Map<String, Set<Lead>>();
            
            for (Datacloud.DuplicateResult dupeResult : results[i].getDuplicateResults()) 
            {
                Set<Lead> enquirerResultsSet = new Set<Lead>();
                System.debug('duplicate rule name ' + dupeResult.getDuplicateRule());
                for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) 
                { 
                    enquirerResultSize += matchResult.getSize(); 

                    for (Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) 
                    {
                        Lead foundLead = (Lead)matchRecord.getRecord();
                        enquirerResultsSet.add(foundLead);
                    }
                }
                leadResultsByRuleName.put(dupeResult.getDuplicateRule(), enquirerResultsSet);
            }
            System.debug('leadResultsByRuleName ' + leadResultsByRuleName);
            // If no match found, then add the temporary (in-memory) Enquirer to Map
            Set<Lead> newLeads = new Set<Lead>();
            if(enquirerResultSize == 0)
            {
                newLeads.add(tempEnquirers[i]);
                //enquirerSetByAppIdMap.put(tempEnquirers[i].Most_Recent_Application__c, enquirerResultsSet);
                Map<String, Set<Lead>> leadByRuleName = new Map<String, Set<Lead>>();
                leadByRuleName.put('none', newLeads);
                
                enquirerSetByRuleNameByAppIdMap.put(tempEnquirers[i].Most_Recent_Application__c, leadByRuleName);
                
            }
            // If more than 1 match found, then add the matching Enquirer(s) to Map
            else
            {
                //enquirerSetByAppIdMap.put(tempEnquirers[i].Most_Recent_Application__c , enquirerResultsSet);
                if(!enquirerSetByRuleNameByAppIdMap.containsKey(tempEnquirers[i].Most_Recent_Application__c))
                {
                    for(String drNameP : leadResultsByRuleName.keySet())
                    {
                        Set<Lead> matchingLeads = leadResultsByRuleName.get(drNameP);
                        
                        if(matchingLeads.size() > 1 && drNameP != 'Customer_Enquirer_Duplicate_Rules' && matchingLeads != null)
                        {
                            matchingLeads.clear();
                            matchingLeads.add(tempEnquirers[i]);
                            //leadSetByRuleName.put(drNameP, matchingLeads);
                        }
                    }
                    enquirerSetByRuleNameByAppIdMap.put(tempEnquirers[i].Most_Recent_Application__c, leadResultsByRuleName);
                }
                else
                {
                    Map<String, Set<Lead>> leadSetByRuleName = enquirerSetByRuleNameByAppIdMap.get(tempEnquirers[i].Most_Recent_Application__c);
                    if(leadSetByRuleName != null)
                    {
                        for(String drName : leadSetByRuleName.keySet())
                        {
                            if(!leadSetByRuleName.containsKey(drName))
                            {
                                leadSetByRuleName.putAll(leadResultsByRuleName);
                            }
                        }
                    }
                } 
            }
        }

        for(Id appId : enquirerSetByRuleNameByAppIdMap.keyset())
        {
            Map<String, Set<Lead>> leadByDupeRuleName = enquirerSetByRuleNameByAppIdMap.get(appId);
            Set<Lead> leadsToUse = new Set<Lead>();
            
            for(String name : leadByDupeRuleName.keySet())
            {  
                if(name == 'Customer_Enquirer_Duplicate_Rules' && leadByDupeRuleName.get(name).size() == 1)
                {
                    /*
                    Set<Lead> existingLeadSet = new Set<Lead>();
                    Lead existingLead = appIdByLead.get(appId);
                    System.debug('existing lead ' + existingLead.id + ' ' + existingLead);
                    existingLeadSet.add(existingLead);
                    
                    leadByDupeRuleName.get(name).clear();
                    leadByDupeRuleName.get(name).add(existingLead); */
                    
                    leadsToUse = leadByDupeRuleName.get(name);
                    break;
                }
                else if(name == 'Customer_Enquirer_Duplicate_Rules' && leadByDupeRuleName.get(name).size() > 1)
                {
                    Set<Lead> exactMatchLeads = leadByDupeRuleName.get(name);
                    Integer last = exactMatchLeads.size()-1;
                    Lead firstLead = (new list<Lead>(exactMatchLeads)[last]);
 
                  /*  for(Lead exactMatchLead : exactMatchLeads)
                    {
                        if(exactMatchLead.LastModifiedDate > firstLead.LastModifiedDate)
                        {
                            firstLead = exactMatchLead;
                        }
                    } */
                    
                    leadsToUse = new Set<Lead>{firstLead};
                 }
                else
                {

                    leadsToUse = leadByDupeRuleName.get(name);
                } 
                
            }
            enquirerSetByAppIdMap.put(appId, leadsToUse);
        }

        return enquirerSetByAppIdMap;
    }
    
    public static Map<Id, Set<Lead>> checkForApplicationsRelatedToManyEnquirers(List<Lead> tempEnquirers) 
    {
        Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(tempEnquirers);
        
        Map<Id, Set<Lead>> enquirerSetByAppIdMap = new Map<Id, Set<Lead>>();
        
        for(Integer i = 0; i < results.size(); i++)
        {
            Integer enquirerResultSize = 0;
            Set<Lead> enquirerResultsSet = new Set<Lead>();
            
            for (Datacloud.DuplicateResult dupeResult : results[i].getDuplicateResults()) 
            {
                for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) 
                { 
                    enquirerResultSize += matchResult.getSize(); 
                    
                    for (Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) 
                    {
                        Lead foundLead = (Lead)matchRecord.getRecord();
                        
                        enquirerResultsSet.add(foundLead);
                    }
                }
            }
            
            if(enquirerResultSize > 0)
            {
                enquirerSetByAppIdMap.put(tempEnquirers[i].Most_Recent_Application__c , enquirerResultsSet);
            }
        }
        
        return enquirerSetByAppIdMap;
    }
    

}