/**
 * @File Name          : ct_MetadataService.cls
 * @Description        : Service class for Metadata Objects
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 29/5/2020, 9:54:48 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/27/2020   Creation Admin     Initial Version
**/
public  class ct_MetadataService {

  /**
  * @description Fetch Advancement Setting metadata records
  * @return Map<String, ct_Advancement_Setting__mdt> 
  **/
  public static Map<String,  ct_Advancement_Setting__mdt> advancementSettingMetaData {
    get {
      if(advancementSettingMetaData == null) {
        advancementSettingMetaData = new Map<String,  ct_Advancement_Setting__mdt>();
        //run a forloop for fetching the Advancement Setting Metadata records 
        for(ct_Advancement_Setting__mdt thisAdvancementSetting : [SELECT DeveloperName, Amount_Value__c, Asperato_Payment_URL__c, Notification_Message__c , Asperato_Due_Date_Delay__c,
                                                                  Value__c, Purpose__c, Paypal_Thank_You_Redirect_URL__c                                                      
                                                                  FROM ct_Advancement_Setting__mdt]){
          advancementSettingMetaData.put(thisAdvancementSetting.DeveloperName, thisAdvancementSetting);
        }
      }
      return advancementSettingMetaData;
    }
    set;
  }

  /**
  * @description Get Site Domain URL metadata records
  * @return Map<String, Site_Domain_URL__mdt> 
  **/
  public static Map<String,  Site_Domain_URL__mdt> siteDomainMetaData {
    get {
      if(siteDomainMetaData == null) {
        siteDomainMetaData = new Map<String,  Site_Domain_URL__mdt>();
        // run a for loop for fetching the Site Domain URL and stores in  siteDomainMetaData Map
        for(Site_Domain_URL__mdt thisSiteSetting : [SELECT DeveloperName, PersonalSiteContactInfoURL__c                                                              
                                                                  FROM Site_Domain_URL__mdt]){
          siteDomainMetaData.put(thisSiteSetting.DeveloperName, thisSiteSetting);
        }
      }
      return siteDomainMetaData;
    }
    set;
  }

  
  /**
  * @description get Opportunity Donation Size Configuration MetaData
  * @return Map<String, Opportunity_Donation_Size_Configuration__mdt> 
  **/
  public static Map<String,  Opportunity_Donation_Size_Configuration__mdt> opportunityDonationSizeMetadata {
    get {
      if(opportunityDonationSizeMetadata == null) {
        opportunityDonationSizeMetadata = new Map<String,  Opportunity_Donation_Size_Configuration__mdt>();
        // run a loop for for fecthing the opportunity Donation Size metadata records
        for(Opportunity_Donation_Size_Configuration__mdt thisDonationConfiguration : [SELECT DeveloperName, 
                                                                                            Large_Donation_Value__c,
                                                                                            Medium_Donation_Value__c,
                                                                                            Small_Donation_Value__c,
                                                                                            Small_Donation_Size_Value__c,
                                                                                            Medium_Donation_Size_Value__c,
                                                                                            Large_Donation_Size_Value__c
                                                                                      FROM Opportunity_Donation_Size_Configuration__mdt]){
          opportunityDonationSizeMetadata.put(thisDonationConfiguration.DeveloperName, thisDonationConfiguration);
        }
      }
      return opportunityDonationSizeMetadata;
    }
    set;
  }

    

  /**
  * @description get Opportunity Departmental Users Settings MetaData
  * @return Map<String, Department_User__mdt> 
  **/
  public static Map<String,  Department_User__mdt> departmentalUsersSettingsMetaData {
    get {
      if(departmentalUsersSettingsMetaData == null) {
        departmentalUsersSettingsMetaData = new Map<String,  Department_User__mdt>();
        // run a for loop for fetching the Department User Metadata records
        for(Department_User__mdt thisUserSetting : [SELECT DeveloperName, 
                                                          Departmental_User_Id__c
                                                          FROM Department_User__mdt]){
          departmentalUsersSettingsMetaData.put(thisUserSetting.DeveloperName, thisUserSetting);
        }
      }
      return departmentalUsersSettingsMetaData;
    }
    set;
  }

    /**
  * @description get Webform default record user Settings MetaData
  * @return Map<String, Webform_Records_Default_User__mdt> 
  **/
  public static Map<String,  Webform_Records_Default_User__mdt> webformRecordDefaultUsersSettingsMetaData {
    get {
      if(webformRecordDefaultUsersSettingsMetaData == null) {
        webformRecordDefaultUsersSettingsMetaData = new Map<String,  Webform_Records_Default_User__mdt>();
        // run a for loop for fetching the Webform default record user metadata records
        for(Webform_Records_Default_User__mdt thisUserSetting : [SELECT DeveloperName, 
                                                              UserId__c
                                                              FROM Webform_Records_Default_User__mdt]){
          webformRecordDefaultUsersSettingsMetaData.put(thisUserSetting.DeveloperName, thisUserSetting);
        }
      }
      return webformRecordDefaultUsersSettingsMetaData;
    }
    set;
  }



  /**
  * @description get Asperato Due Date delay metadata record
  * @return Integer 
  **/
  public static Integer getAsperatoDueDateDelay(){
    Integer delayDays = 0;
    // get the asperato due date from the advancement setting metadata records
    for(ct_Advancement_Setting__mdt thisMetaData : advancementSettingMetaData.values()){
      if(thisMetaData.Asperato_Due_Date_Delay__c != null){
        delayDays = Integer.valueOf(thisMetaData.Asperato_Due_Date_Delay__c);
      }
    }
    return delayDays;
  }

  /**
  * @description Get Advancement Setting metadata By Developer Name
  * @param developerName 
  * @return ct_Advancement_Setting__mdt 
  **/
  public static ct_Advancement_Setting__mdt getAdvancementSettingMetaDataByDeveloperName(String developerName){
    if(!advancementSettingMetaData.isEmpty()
    && advancementSettingMetaData.containsKey(developerName)){
      return advancementSettingMetaData.get(developerName);
    }
    return null;
  }

  /**
  * @description get map of Personal Site Contact Info URL by Developer name
  * @param developerName 
  * @return String 
  **/
  public static String getSiteDomainMetaDataByDeveloperName(String developerName){
    if(!siteDomainMetaData.isEmpty()
    && siteDomainMetaData.containsKey(developerName)){
      return siteDomainMetaData.get(developerName).PersonalSiteContactInfoURL__c;
    }
    return null;
  }

  /**
  * @description get Donation Size Configuration By Record Type Developer name
  * @param developerName 
  * @return Opportunity_Donation_Size_Configuration__mdt 
  **/
  public static Opportunity_Donation_Size_Configuration__mdt getDonationSizeConfigurationByRecordTypeDeveloperName(String developerName){
    if(!opportunityDonationSizeMetadata.isEmpty()
    && opportunityDonationSizeMetadata.containsKey(developerName)){
      return opportunityDonationSizeMetadata.get(developerName);
    }
    return null;
  }

  
  /**
  * @description get Departmental UsersSettings MetaData By Record Type Developer name
  * @param developerName 
  * @return Department_User__mdt 
  **/
  public static Department_User__mdt getDepartmentalUsersSettingsByRecordTypeDeveloperName(String developerName){
    if(!departmentalUsersSettingsMetaData.isEmpty()
    && departmentalUsersSettingsMetaData.containsKey(developerName)){
      return departmentalUsersSettingsMetaData.get(developerName);
    }
    return null;
  }

  /**
  * @description get Webform Default User Id Users MetaData By Web Form name
  * @param webFormName 
  * @return User Id 
  **/
  public static Id getDefaultUsersByWebFormName(String webFormName){
    if(!webformRecordDefaultUsersSettingsMetaData.isEmpty()
    && webformRecordDefaultUsersSettingsMetaData.containsKey(webFormName)){
      return Id.valueOf(webformRecordDefaultUsersSettingsMetaData.get(webFormName).UserId__c);
    }
    return null;
  }

  /**
  * @description Get default mapping record from legacy donation opportunity mapping metadata
  * @author Creation Admin | 29/5/2020 
  * @return Legacy_Donation_Opportunity_Mapping__mdt 
  **/
  public static Legacy_Donation_Opportunity_Mapping__mdt getLegacyDonationOpportunityMapping(){
    Legacy_Donation_Opportunity_Mapping__mdt thislegacyDonationMapping = new Legacy_Donation_Opportunity_Mapping__mdt();

    for(Legacy_Donation_Opportunity_Mapping__mdt thisLegacyMDT : [SELECT Id, Opportunity_Close_Date__c, Opportunity_Currency__c,
                                                                          Opportunity_Stage__c, Opportunity_Amount__c
                                                                          FROM Legacy_Donation_Opportunity_Mapping__mdt
                                                                          LIMIT 1]){
      thislegacyDonationMapping = thisLegacyMDT;
    }

    return thislegacyDonationMapping;
  }
}