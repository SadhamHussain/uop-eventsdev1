/****************************************************************************
 *  bg_UserTestUtils
 *
 * Utility class to create a user in unit tests
 * 
 *  Author: Ismail Basser - Brightgen
 *  Created: 01/08/2019
 * 
 *  Changes: 
 *  VERSION  AUTHOR            DATE              DETAIL		DESCRIPTION
 *  
***********************************************************************/
public class bg_UserTestUtils {
    
    public static User createStandardUser(String alias) 
    {
        Profile standardProfile = [SELECT Id FROM Profile WHERE Name='Standard User' Limit 1];
        
        User newUser  = new User(
            Username = alias+'@test123.com.abc',
            CompanyName = 'TEST',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Alias = alias, 
            Email = alias+'@abc.com', 
            LastName = 'Test121', 
            ProfileId = standardProfile.Id
        );
        
        return newUser;
    }
    
    public static User createSystemAdminUser(String alias) 
    {
        Profile standardProfile = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        
        User newUser  = new User(
            Username = alias+'@test123.com.abc',
            CompanyName = 'TEST',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Alias = alias, 
            Email = alias+'@abc.com', 
            LastName = 'Test121', 
            ProfileId = standardProfile.Id
        );
        
        return newUser;
    }

}