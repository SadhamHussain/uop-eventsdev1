/**
 * @File Name          : ct_HED_AddressTriggerHandler_TDTM.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 24/6/2020, 7:43:17 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/6/2020   Creation Admin     Initial Version
**/
global class ct_HED_AddressTriggerHandler_TDTM extends hed.TDTM_Runnable{
  static boolean hasExecuted = false;

  global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
  hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {
    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ADDRESS TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
    List<hed__Address__c> addressNewList = (List<hed__Address__c>)newlist;

    if(triggerAction == hed.TDTM_Runnable.Action.BeforeInsert){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ADDRESS BEFORE INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      //Set Address Record As Private If Related Contact=>Account Is Set As Private
      ct_HED_AddressTriggerHelper.setAddressAsPrivate(addressNewList);
    }

    if (triggerAction == hed.TDTM_Runnable.Action.BeforeUpdate){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ADDRESS BEFORE UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      //Set Address Record As Private If Related Contact=>Account Is Set As Private
      ct_HED_AddressTriggerHelper.setAddressAsPrivate(addressNewList);
    }

    if (triggerAction == hed.TDTM_Runnable.Action.BeforeDelete) {
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ADDRESS BEFORE DELETE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    }

    if (triggerAction == hed.TDTM_Runnable.Action.AfterInsert){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ADDRESS AFTER INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    }

    if (triggerAction == hed.TDTM_Runnable.Action.AfterUpdate){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ADDRESS AFTER UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    }
    return dmlWrapper;
  }
}