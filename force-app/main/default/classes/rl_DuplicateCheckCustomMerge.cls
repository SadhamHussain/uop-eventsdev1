global class rl_DuplicateCheckCustomMerge implements dupcheck.dc3Plugin.InterfaceMerge {

   global void beforeMerge(String objectPrefix, Sobject masterRecord, List <sobject> mergedRecordList) 
   {
 
    // If object prefix for merge is for a Lead
    if (objectPrefix == '00Q')
    {
  
        // Convert master Sobject to Lead
        Lead masterLead = (Lead)masterRecord;
        
        // Convert list of child sobects to Leads
        List<Lead> childLeads = mergedRecordList;
        
        // Create a list to store email addressess
        List<String> emailAddresses = new List<String>();
       
        // Add email addresses from the master record to the list of email addresses
        emailAddresses.add(masterLead.Email);
        emailAddresses.add(masterLead.Email_Alternative__c);
       
        // Add email addresses from the child records to the list of email address (unless blank)
        for(Lead lead : childLeads)
        {
            emailAddresses.add(lead.Email);
            emailAddresses.add(lead.Email_Alternative__c);
        }
        
        // Dedupe list of email addresses with use of Set
        Set<String> tempSet = new Set<String>();
        List<String> uniqueEmailAddresses = new List<String>();
        
        for (String s : emailAddresses) 
        {
            if (s != null && tempSet.add(s)) 
            {
                uniqueEmailAddresses.add(s);
            }
        }
        
        // Store number of unique email addresses
        Integer emailCount = uniqueEmailAddresses.size();
       
        // If list size is one and master email is blank, copy email address to master email address
        if (emailCount == 1 && masterLead.Email == null) 
        {
            masterLead.Email = uniqueEmailAddresses.get(0);
            update masterLead;
        }
        
        // If list size is two or more, and master email address is blank, copy first email to master email address, and second email address to email alternative
        if (emailCount >= 2 && masterLead.Email == null) 
        {
            masterLead.Email = uniqueEmailAddresses.get(0); 
            masterLead.Email_Alternative__c = uniqueEmailAddresses.get(1);
            update masterLead;
        }       
        
        // If list size is two or more and master is not blank, or three or more, copy second email address to email alternative
        if (emailCount >= 2 && masterLead.Email != null) 
        {
            masterLead.Email_Alternative__c = uniqueEmailAddresses.get(1);
            update masterLead;
        }

    }
    
    return;
    
   }

   global void mergeFailed(String objectPrefix, Sobject masterRecord, Set < id > mergedRecordsIds, dupcheck.dc3Exception.MergeException exceptionData) {
      // NO CUSTOM CODE
      return;
   }

   global void afterMerge(String objectPrefix, Sobject masterRecord, Set < id > mergedRecordIds) {
      // NO CUSTOM CODE
      return;
   }
}