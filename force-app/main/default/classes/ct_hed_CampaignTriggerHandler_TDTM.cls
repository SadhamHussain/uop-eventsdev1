/**
 * @File Name          : ct_hed_CampaignTriggerHandler_TDTM.cls
 * @Description        : TDTM Trigger Handler for Campaign Object
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/11/2020, 7:57:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/11/2020   Creation Admin     Initial Version
**/
global class ct_hed_CampaignTriggerHandler_TDTM extends hed.TDTM_Runnable{
global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
                                                 hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {
			System.debug('/****************************ct_hed_CampaignTriggerHandler_TDTM*****************/');
			hed.TDTM_Runnable.DmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
			//Project Ids
			set<Id> projectIds = new set<Id>();
			 if (triggerAction == hed.TDTM_Runnable.Action.AfterInsert || triggerAction == hed.TDTM_Runnable.Action.AfterUpdate) {
				 for (Campaign cm : (list<Campaign>)newlist) {
					 if (cm.Project__c != null) {
						 projectIds.add(cm.Project__c);
					 }
				 }
			 }
			 else if (triggerAction == hed.TDTM_Runnable.Action.BeforeUpdate || triggerAction == hed.TDTM_Runnable.Action.AfterDelete) {
				 for (Campaign cm : (list<Campaign>)oldlist) {
					 if (cm.Project__c != null) {
						 projectIds.add(cm.Project__c);
					 }
				 }
			 }
			 if(projectIds.size()>0 && !System.isFuture() && !System.isBatch()){
				 ct_hed_CampaignTriggerHelper.updateProjectCustomRollup(projectIds);
			 }
			return dmlWrapper;                                                  
			}
}