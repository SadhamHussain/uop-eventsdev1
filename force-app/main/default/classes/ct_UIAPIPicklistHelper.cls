/**
 * @File Name          : ct_UIAPIPicklistHelper.cls
 * @Description        : Metadata api class for getting picklist values from a custom object. This uses SF metadata api using endpoint of the current sf org to get the needed data for picklist according to the recordtype
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 2/11/2020, 5:20:22 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/9/2019   Creation Admin     Initial Version
**/
public class ct_UIAPIPicklistHelper {

  /**
    * Method to get the Picklist values based on Record Type
    * @param recordTypeId : Record Type Id 
    * @param ObjectApiName : SObject API Name
    * @param fieldApiName : Picklist Field API Name for which picklist values to be retrived
    * Return Results : The Wrapper which contains Picklist values for the requested Field
  */
  @AuraEnabled 
  public static List<picklistValuesWrapper> getPicklistValues(String recordTypeId, String ObjectApiName,String fieldApiName){
    //Set the Callout endpoint for User Interface API to get the picklist values based on the record types
    String endpoint = URL.getSalesforceBaseUrl().toExternalForm();
    endPoint += '/services/data/v46.0';
    endPoint += '/ui-api/object-info/{0}/picklist-values/{1}/{2}';
    /**
     * Pass the required parameter to the User Interface Picklist API:
     *  Object API name which has the resquest picklist field
     *  Record Type Id based on which the picklist values are restricted
     *  Field API name for which the picklist values needs to be retrived based on the Record Type Id requested
     * */
    endPoint = String.format(endPoint, new String[]{ ObjectApiName, recordTypeId , fieldApiName });
    EncodingUtil.urlEncode(endPoint,'UTF-8');
    //Getting session Id for making the callout which is required parameter for User Interface API Callouts
    //For Getting the User Session Id Invoked the Visualforce page which has the User Session Id attribute in it
    PageReference userSessionIdPage = Page.ct_UserSessionId;
    String vfContent;
    //Validate wheather the method is invoked by the test class
    if(!Test.isRunningTest()){
      // If the method is not invoked by the test class then get the session Id from the visual force page
      vfContent                    = userSessionIdPage.getContent().toString();
    }
    else if(Test.isRunningTest()){
      // If the method is invoked by the test class then get the session Id by using UserInfo Class
      vfContent     = 'Start_Of_Session_Id'+UserInfo.getSessionId()+'End_Of_Session_Id';
    }
    Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length();
    Integer endP = vfContent.indexOf('End_Of_Session_Id');
    string sessionId = vfContent.substring(startP, endP);
    //HTTP Request for User Interface API to get Picklist values
    HttpRequest thisPicklistValueRequest = new HttpRequest();
    //Set the Session Id obtained as Bearer Authorization header for the API Callout
    thisPicklistValueRequest.setHeader('Authorization', 'Bearer ' +sessionId); 
    //Set the End Point URL Dynamically generated above
    thisPicklistValueRequest.setEndpoint(endPoint);
    //Set the API Method as GET
    thisPicklistValueRequest.setMethod('GET');
    Http http = new Http();
    //Make the callout by invoking the send() method for the New Http instance with the HttpRequest configured as Parameter
    HTTPResponse response = http.send(thisPicklistValueRequest);
    List<picklistValuesWrapper> thispiclistValuesToReturn = new List<picklistValuesWrapper>();
    // Process the response from the above request only if the Status code is 200 which denotes that the request made was successfull
    if(response.getStatusCode() == 200){
      String thisResponseBody = response.getBody().replaceAll('&#39;','\'');
      //To Process the picklist values from the response, deserialize the Response Body to get Map of Objects
      Map<String,Object> root = (Map<String,Object>) JSON.deserializeUntyped(thisResponseBody);
      List<Object> thisPicklistValuesList = (List<Object>)root.get('values');
      //Loop through the deserialized picklist map values to get individuals values
      for(Object thisPicklistvalue : thisPicklistValuesList){
        Map<String,Object> pickValueMap = (Map<String,Object>)thisPicklistvalue;
        picklistValuesWrapper thisWrapper = new picklistValuesWrapper();
        //create a new instance of picklistValuesWrapper which has label, Value attributes and collect them in a list
        thisWrapper.label = (String)pickValueMap.get('label');
        thisWrapper.value = (String)pickValueMap.get('value');
        thispiclistValuesToReturn.add(thisWrapper);
      }
    }
    //Return the List which contains the picklistValuesWrapper Objects that holds the picklist values
    return thispiclistValuesToReturn;
  }

  //Wrapper class that has label value attributes that hold the respective picklist value attributes
  public class picklistValuesWrapper {
    @AuraEnabled public String label;	
    @AuraEnabled public String value;
  }
}