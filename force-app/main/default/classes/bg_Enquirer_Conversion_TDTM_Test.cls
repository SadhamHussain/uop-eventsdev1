@isTest
private class bg_Enquirer_Conversion_TDTM_Test {
	
	/*
	*
	* Setup of test data to be used for all test methods
	*
	*/
	@testSetup static void setupData()
	{
		List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
		tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Enquirer_Conversion_TDTM', 
													 'Lead', 'BeforeInsert;BeforeUpdate', 2.00));
		hed.TDTM_Global_API.setTdtmConfig(tokens);
	}

	@isTest 
	private static void testCustomConversionFlagIsSetUponConversionOfEnquirer() {
		
		Lead enq = bg_UnitTestDataFactory.createStudentEnquirer();
		insert enq;

		Database.LeadConvert enqConvert = new Database.LeadConvert();

		if(bg_CommonUtils.isGroupId(enq.OwnerId) || !String.isNotBlank(enq.OwnerId))
		{
			enqConvert.setOwnerId(UserInfo.getUserId());
		}
		enqConvert.setLeadId(enq.Id);
	    enqConvert.setConvertedStatus('Closed - Converted to Applicant');
	   	enqConvert.setDoNotCreateOpportunity(true);

	   	Test.startTest();

	   	Database.LeadConvertResult enqConvertResult = Database.convertLead(enqConvert);

	   	Lead updatedEnquirer = [SELECT Id, Has_Enquirer_Converted__c, isConverted FROM Lead WHERE Id = :enq.Id LIMIT 1];

	   	system.assertEquals(true, updatedEnquirer.isConverted);
	   	system.assertEquals(true, updatedEnquirer.Has_Enquirer_Converted__c);

	   	Test.stopTest();
	}
}