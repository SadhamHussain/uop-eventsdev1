/**
 * @File Name          : ct_ProcessLapsedOppSchedulableBatch_Test.cls
 * @Description        : Batch class to process Lapsed Donation Opportunity Test
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 4/27/2020, 4:11:53 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/26/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_ProcessLapsedOppSchedulableBatch_Test {
  @isTest(SeeAllData=true)
  public static void processLapsedOpportunityTest(){
    Opportunity thisOpportunity                                     = new Opportunity();
    thisOpportunity.Name                                            = 'Test Lapsed Opportunity';
    thisOpportunity.StageName                                       = ct_Constants.OPPORTUNITY_STAGE_CLOSED_WON;
    thisOpportunity.CloseDate                                       = System.today().addMonths(-13);
    thisOpportunity.RecordTypeId                                    = ct_Util.getRecordTypeIdByDevName('Opportunity', 'Donation');
    thisOpportunity.Amount                                          = 100000.00;
    thisOpportunity.npe01__Do_Not_Automatically_Create_Payment__c   = true;
    insert thisOpportunity;
    Contact thisContactOne         = ct_TestDataFactory.createContact();
    thisContactOne.Donor_Status__c = ct_Constants.CONTACT_DONOR_STATUS_CURRENT;
    thisContactOne.RecordTypeId    = ct_Constants.CONTACT_RECORDTYPE_UOPSTUDENT;
    insert thisContactOne;
    thisOpportunity.npsp__Primary_Contact__c     = thisContactOne.Id;
    update thisOpportunity;
    Test.startTest();
    ct_ProcessLapsedOppSchedulableBatch batch = new ct_ProcessLapsedOppSchedulableBatch();
    Database.executeBatch(batch);
    Test.stopTest();

    Contact lapsedDonor    = [SELECT Id, Name, Donor_Type__c, Donor_Status__c FROM Contact WHERE Id =: thisContactOne.Id];
    System.assertEquals(true, lapsedDonor.Donor_Status__c.contains(ct_Constants.CONTACT_DONOR_STATUS_LAPSED), 'Since one Closed Won opportunity is not within last 12 months');
  }

  @IsTest
  public static void processLapsedOpportunitySchedulerTest(){
    Test.startTest();
    ct_ProcessLapsedOppSchedulableBatch schedulerClassObj = new ct_ProcessLapsedOppSchedulableBatch();
    schedulerClassObj.execute(Null);
    Test.stopTest();
  }
}