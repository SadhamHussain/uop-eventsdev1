////////////////////////////////////////////////////////////////////////////////
//  CheckInHandlerController
//
//  Controller for CheckInHandler visualforce page
//
//  @Author:        Thomas Packer - BrightGen Ltd
//  @Created:       19/01/2020
//
//  @Changes: 
////////////////////////////////////////////////////////////////////////////////
public without sharing class CheckInHandlerController 
{
    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 17th January 2020
	//  @description: Method to process page params. Called on init.
	//
	//  @changes:	
    //  @usage: 
	////////////////////////////////////////////////////////////////////////////
    public static void RunHandler()
    {
        if (ApexPages.currentPage().getParameters().containsKey('cmid'))
        {
            Id campaignMemberId = ApexPages.currentPage().getParameters().get('cmid');

            if (ValidateCampaignMember(campaignMemberId))
            {
                CheckInCampaignMember(campaignMemberId);
            }
        }
    }   

    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 17th January 2020
	//  @description: Method to validate a campaign member exists from 
    //                the provided cmid
	//
	//  @changes:	
    //  @usage: 
	////////////////////////////////////////////////////////////////////////////
    private static Boolean ValidateCampaignMember(Id campaignMemberId)
    {
        List<CampaignMember> campaignMembers = new List<CampaignMember>([SELECT Id
                                                                         FROM CampaignMember
                                                                         WHERE Id = :campaignMemberId]);

        if (!campaignMembers.isEmpty())
        {
            return true;
        }

        return false;
    }

    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 17th January 2020
	//  @description: Method to mark a campaign member as "Checked In"
	//
	//  @changes:	
    //  @usage: 
	////////////////////////////////////////////////////////////////////////////
    private static void CheckInCampaignMember(Id campaignMemberId)
    {
        CampaignMember member = new CampaignMember();

        member.Id = campaignMemberId;
        member.Status = 'Checked In';

        try
        {
            Database.UpsertResult r = Database.upsert(member);
            if (r.isSuccess())
            {
                ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Check-in successful!','');
                ApexPages.addMessage(message);
            }

        }
        catch (Exception e)
        {
            ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.Error, e.getMessage(),'');

            ApexPages.addMessage(message);
        }
    }
}