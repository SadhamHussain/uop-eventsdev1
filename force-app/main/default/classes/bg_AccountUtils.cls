/*****************************************************************
* bg_AccountUtils
*
* Class to look at a freetxt field on Lead and see if it can find 
* a matching Account with that name
*
* Test Class: bg_AccountUtils_Test
* 
* Author: Mark Brown
* Created: 13-06-2019
* Case: 00042630
*
* Changes: 14/02/20 - JAG - Removed escapesingleqoutes from Account lookup.
******************************************************************/
public with sharing class bg_AccountUtils 
{
    public static void bg_GetAccountFromName(List<Lead> LeadsList, List<Lead> oldLeads, Boolean isUpdate)
    {
        System.debug('bg_AccountUtils.bg_GetAccountFromName - START');
        String strAccountName;
        String newSearchText;
        Boolean accountPopulated = false;


        try { 
            Id EducationalInstituteRecordID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Educational_Institution').getRecordTypeId();
            Id StudentEnquirerRecordID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Student').getRecordTypeId();

            //if(isUpdate) 
           // {
           //     Map<Id, Lead> oldMap = new Map<Id, Lead>(oldLeads);
           // }

            // Loop through the list of leads passedin from the trigger handler
            for (Lead theLead : LeadsList)
            {

                accountPopulated = false;

                // Ignore non-student leads
                if(theLead.RecordTypeId != StudentEnquirerRecordID)
                {
                    System.debug('NOT a student record so exitting');
                    continue;
                }

                // Ignore if there is no school name to look up
                if (String.isBlank(theLead.School_College_Freetext__c))
                {
                    System.debug('Blank school name so exitting');
                    theLead.Multiple_School_Account_Matches__c = false;
                    theLead.Failed_School_Account_Matches__c = false;
                    continue;
                }

            if(isUpdate) 
            {
                Map<Id, Lead> oldMap = new Map<Id, Lead>(oldLeads);
                // get the old lead
                Lead theoldLead = oldMap.get(theLead.ID);

                system.debug('theLead school   account = ' + theLead.School_College__c);
                system.debug('theoldLead school   account = ' + theoldLead.School_College__c);

                // If the lookup field has changed and there is a valid lookup to an account then reset the flags and blank the old freetext and move on to next record
                if(theLead.School_College__c != null && theoldLead.School_College__c != theLead.School_College__c)
                {
                    System.debug('The school account is not blank so setting the populated flag');
                    theLead.Multiple_School_Account_Matches__c = false;
                    theLead.Failed_School_Account_Matches__c = false;
                    theLead.School_College_Freetext__c = null;
                    continue;
                }
            }



                // if we get here and the school ispopulatd that means we have a new freetext field which we need to try and replace the scool account with
                if(theLead.School_College__c != null)
                {
                    accountPopulated = true;
                }

                // Search for accounts with an account name that matches the freetext school name
                strAccountName = theLead.School_College_Freetext__c;
                newSearchText = '%'+strAccountName+'%';
                System.debug('newSearchText = '+newSearchText);

                List<Account> lstAccount = new List<Account>([SELECT Id, Name 
                                                              From Account 
                                                              WHERE Name LIKE :newSearchText 
                                                              AND RecordTypeId = :EducationalInstituteRecordID]);

                // Set the failed flag if no matches are returned
                if(lstAccount.size() < 1)
                {
                    System.debug('no result');
                    if(accountPopulated)
                    {
                        theLead.School_Update_Mismatch__c = true;
                    } else {
                        theLead.School_College__c = null;
                        theLead.School_Update_Mismatch__c = false; 
                    }
                    theLead.Failed_School_Account_Matches__c = true;
                    theLead.Multiple_School_Account_Matches__c = false;
                }

                // Set the multiuple failed flag if more than one match is found
                if(lstAccount.size() > 1)
                {
                    if(accountPopulated)
                    {
                        theLead.School_Update_Mismatch__c = true;
                    } else {
                        theLead.School_College__c = null;
                        theLead.School_Update_Mismatch__c = false; 
                    }                    
                    theLead.Multiple_School_Account_Matches__c = true;
                    theLead.Failed_School_Account_Matches__c = false;
                    System.debug('multiple result');
                }

                // Set the lookup to the account if a distinct match is found
                if(lstAccount.size() == 1)
                {
                    theLead.Failed_School_Account_Matches__c = false;
                    theLead.Multiple_School_Account_Matches__c = false;
                    theLead.School_Update_Mismatch__c = false;
                    theLead.School_College__c = lstAccount[0].Id;
                    // also unset the freetext in case the account is updated directly later and this would then check against the free text again
                    theLead.School_College_Freetext__c = null;
                    System.debug('good result');
                }

            } // End of for loop over map
        }catch(Exception e) {
            System.debug('EXCEPTION: '+e);
            System.debug(e.getLineNumber());
            System.debug(e.getStackTraceString());
        }
        System.debug('bg_AccountUtils.bg_GetAccountFromName - STOP');
        return;
    }
}