/****************************************************************************
 *  bg_CountryCodeHelper_Test
 *
 * Test class for bg_CountryCodeHelper
 * 
 *  Author: Ismail Basser - Brightgen
 *  Created: 01/08/2019
 * 
 *  Changes: 
 *  VERSION  AUTHOR            DATE              DETAIL		DESCRIPTION
 *  
***********************************************************************/
@isTest
public class bg_CountryCodeHelper_Test {
    
    @testSetup static void setup() 
    {
        Account account1 = new Account(Name = 'Test Account 1');
        insert account1;
        
        List<Country_Mapping__c> countryMappings = new List<Country_Mapping__c>();
        Country_Mapping__c brazil = new Country_Mapping__c(Name = 'Brazil', Country__c = 'Brazil', Country_Code__c = 'BR', Fee_Status__c = 'International', Residence_Status__c = 'International');
        Country_Mapping__c germany = new Country_Mapping__c(Name = 'Germany', Country__c = 'Germany', Country_Code__c = 'DE', Fee_Status__c = 'EU', Residence_Status__c = 'EU');
        Country_Mapping__c unitedKingdom = new Country_Mapping__c(Name = 'United Kingdom', Country__c = 'United Kingdom', Country_Code__c = 'GB', Fee_Status__c = 'UK', Residence_Status__c = 'UK');
        
        countryMappings.add(brazil);
        countryMappings.add(germany);
        countryMappings.add(unitedKingdom);
        insert countryMappings;
        
        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
        tokens.add(new hed.TDTM_Global_API.TdtmToken('bg_Contact_AppProcessing_TDTM', 
                                                     'Contact', 'BeforeInsert;AfterInsert;BeforeUpdate;AfterUpdate', 10.00));
        
        hed.TDTM_Global_API.setTdtmConfig(tokens);

    }
    
    static testMethod void setResidenceStatusOnContactInsertTest() 
    {   
        Account testAccount = [SELECT Id FROM Account Limit 1];
        Contact testContact = new Contact(FirstName = 'John', LastName = 'Doe', accountId = testAccount.Id, Phone_Consent__c = 'Yes',
                                         WhatsApp_Consent__c = 'No', Directmail_Consent__c = 'Yes', 
                                          Country_of_Residence_Code__c = 'BR');
        insert testContact;

        List<Contact> insertedConctacts = [SELECT Id, Residence_Status__c FROM Contact];
        System.assertEquals('International', insertedConctacts[0].Residence_Status__c);
    }
    
    static testMethod void setResidenceStatusOnContactUpdateTest() 
    {   
        Account testAccount = [SELECT Id FROM Account Limit 1];
        Contact testContact = new Contact(FirstName = 'John', LastName = 'Doe', accountId = testAccount.Id, Phone_Consent__c = 'Yes',
                                         WhatsApp_Consent__c = 'No', Directmail_Consent__c = 'Yes', 
                                          Country_of_Residence_Code__c = 'BR');
        insert testContact;

        List<Contact> insertedConctacts = [SELECT Id, Residence_Status__c, Country_of_Residence_Code__c FROM Contact];
        insertedConctacts[0].Country_of_Residence_Code__c = 'DE';
        update insertedConctacts;
        
       	Contact updatedContact = [SELECT Id, Residence_Status__c, Country_of_Residence_Code__c FROM Contact Limit 1];
        System.assertEquals('EU', updatedContact.Residence_Status__c);
    }

}