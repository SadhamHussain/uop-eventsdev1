/*****************************************************************
 * bg_EnquirerUtils_Test
 *
 * Test methods for bg_EnquirerUtils class
 *
 * Author: Stuart Barber
 * Created: 11-09-2018
 *  *************** Version history ***********************
 * CT, Sadham : 17/04/2020 - Test class Error fixed(CPU Time Limit) by inserting One Application record 
 *                           instead of two application records
******************************************************************/

@isTest
private class bg_EnquirerUtils_Test {
	
	@isTest (seeAllData = true)
	private static void testMatchingEnquirerIsUpdatedWithApplicantInfo()
	{		
		Account courseAccount1 = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount1.Academic_Programme_Course_Code__c = 'COMPSCI1234';
		courseAccount1.Course_Start_Month__c = '09';
		courseAccount1.Course_Start_Year__c = '2020';

		// Account courseAccount2 = bg_UnitTestDataFactory.createCourseAccount();
		// courseAccount2.Academic_Programme_Course_Code__c = 'COMPSCI4321';
		// courseAccount2.Course_Start_Month__c = '09';
		// courseAccount2.Course_Start_Year__c = '2020';
	    
	    insert new List<Account>{courseAccount1};

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();
		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
		testApp1.SCMS_Application_Id__c = 'testapp0001';
		testApp1.Course_Code__c = 'COMPSCI1234';
	    testApp1.First_Name__c = 'test098765'; 
	    testApp1.Last_Name__c = 'testuser';
	    testApp1.Email_Address__c = 'test098765@testuser.com';
	    testApp1.Postal_Code__c = 'E2 8YS';
	    testApp1.Mobile_Phone_Number__c = '01492838382';
	    testApp1.Course__c = courseAccount1.Id;
	    testApp1.School_College__c = schoolAccount.Id;

	  //   Application__c testApp2 = bg_UnitTestDataFactory.createApplication();
	  //   testApp2.SCMS_Application_Id__c = 'testapp0002';
		// testApp2.Course_Code__c = 'COMPSCI4321';
	  //   testApp2.First_Name__c = 'test098765'; 
	  //   testApp2.Last_Name__c = 'testuser';
	  //   testApp2.Email_Address__c = 'test098765@testuser.com';
	  //   testApp2.Phone_Number__c = '0739357673';
	  //   testApp2.Postal_Code__c = 'E2 8YS';
	  //   testApp2.Course__c = courseAccount2.Id;
	  //   testApp2.School_College__c = schoolAccount.Id;
		Test.startTest();

	    List<Application__c> testApplications = new List<Application__c>{testApp1};
	    insert testApplications;


			bg_ApplicationUtils.processApplications(testApplications);

			List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
															 Enquirer__c, Opportunity__c, Applicant__c
															 FROM Application__c
															 WHERE Id IN :testApplications];

			system.assertEquals(1, updatedTestApplications.size());

			Set<String> studentIds = new Set<String>();
			Set<String> applicationIds = new Set<String>();

			for(Application__c app : updatedTestApplications)
			{
				studentIds.add(app.Student_Id__c);
				applicationIds.add(app.SCMS_Application_Id__c);
			}

			List<Contact> applicantContact = [SELECT Id FROM Contact WHERE Student_Applicant_Id__c IN :studentIds]; 
			List<Opportunity> newOpportunity = [SELECT Id FROM Opportunity WHERE Application_Id__c IN :applicationIds]; 

		system.assertEquals(1, applicantContact.size());
		system.assertEquals(1, newOpportunity.size());

		Test.stopTest();
	}

	@isTest (seeAllData = true)
	private static void testMatchingEnquirerIsUpdatedWithApplicantInfoOneByOneInsertion()
	{		
		Account courseAccount1 = bg_UnitTestDataFactory.createCourseAccount();
		courseAccount1.Academic_Programme_Course_Code__c = 'COMPSCI1234';
		courseAccount1.Course_Start_Month__c = '09';
		courseAccount1.Course_Start_Year__c = '2020';

		// Account courseAccount2 = bg_UnitTestDataFactory.createCourseAccount();
		// courseAccount2.Academic_Programme_Course_Code__c = 'COMPSCI4321';
		// courseAccount2.Course_Start_Month__c = '09';
		// courseAccount2.Course_Start_Year__c = '2020';
	    
	    insert new List<Account>{courseAccount1};

	    Account schoolAccount = bg_UnitTestDataFactory.createSchoolAccount();

		Application__c testApp1 = bg_UnitTestDataFactory.createApplication();
		testApp1.SCMS_Application_Id__c = 'testapp0001';
		testApp1.Course_Code__c = 'COMPSCI1234';
	    testApp1.First_Name__c = 'test098765'; 
	    testApp1.Last_Name__c = 'testuser';
	    testApp1.Email_Address__c = 'test098765@testuser.com';
	    testApp1.Postal_Code__c = 'E2 8YS';
	    testApp1.Mobile_Phone_Number__c = '01492838382';
	    testApp1.Course__c = courseAccount1.Id;
	    testApp1.School_College__c = schoolAccount.Id;
    insert testApp1;
    
    //CT, Sadham: Modified the class to avoid time limit exception while executing test class by inserting one Application
    //                              instead of two Application records

	  //   Application__c testApp2 = bg_UnitTestDataFactory.createApplication();
	  //   testApp2.SCMS_Application_Id__c = 'testapp0002';
		// testApp2.Course_Code__c = 'COMPSCI4321';
	  //   testApp2.First_Name__c = 'test098765'; 
	  //   testApp2.Last_Name__c = 'testuser';
	  //   testApp2.Email_Address__c = 'test098765@testuser.com';
	  //   testApp2.Phone_Number__c = '0739357673';
	  //   testApp2.Postal_Code__c = 'E2 8YS';
	  //   testApp2.Course__c = courseAccount2.Id;
	  //   testApp2.School_College__c = schoolAccount.Id;
    //   insert testApp2;
    
		Test.startTest();

			bg_ApplicationUtils.processApplications(new List<Application__c>{testApp1});

			List<Application__c> updatedTestApplications = [SELECT Id, Processing_Status__c, Student_Id__c, SCMS_Application_Id__c, 
															 Error_Description__c, First_Name__c, Last_Name__c, Email_Address__c,
															 Phone_Number__c, Mobile_Phone_Number__c, Postal_Code__c,
															 Enquirer__c, Opportunity__c, Applicant__c
															 FROM Application__c
															 WHERE Id = :testApp1.Id];

			system.assertEquals(1, updatedTestApplications.size());

			Set<String> studentIds = new Set<String>();
			Set<String> applicationIds = new Set<String>();

			for(Application__c app : updatedTestApplications)
			{
				studentIds.add(app.Student_Id__c);
				applicationIds.add(app.SCMS_Application_Id__c);
			}

			List<Contact> applicantContact = [SELECT Id FROM Contact WHERE Student_Applicant_Id__c IN :studentIds]; 
			List<Opportunity> newOpportunity = [SELECT Id FROM Opportunity WHERE Application_Id__c IN :applicationIds]; 

			system.assertEquals(1, applicantContact.size());
			system.assertEquals(1, newOpportunity.size());

		Test.stopTest();
	}
}