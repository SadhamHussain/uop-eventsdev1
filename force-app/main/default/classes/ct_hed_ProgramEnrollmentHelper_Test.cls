/**
 * @File Name          : ct_hed_ProgramEnrollmentHelper_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 5/8/2020, 8:15:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/17/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_hed_ProgramEnrollmentHelper_Test {
  @isTest (seeAllData = true)
  private static void testUpdateRelatedAffiliation(){
    // first retrieve default EDA trigger handlers
    List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_ProgramEnrollmentHandler_TDTM', 'Program_Enrollment__c', 'AfterUpdate', 2.00));
    // Pass trigger handler config to set method for this test run

    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    Account thisAccount = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    insert thisAccount;

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;

    hed__Program_Enrollment__c thisEnrollment = ct_TestDataFactory.createHedaProgramEnrollment(thisAccount.Id, thisContact.Id);
    insert thisEnrollment;
    
    thisEnrollment.Completed__c = true;
    update thisEnrollment;
    hed__Program_Enrollment__c thisTestEnrollment = [SELECT Id, hed__Affiliation__c, hed__Affiliation__r.hed__Role__c, 
                                                          hed__Affiliation__r.hed__Status__c
                                                          FROM hed__Program_Enrollment__c
                                                          WHERE hed__Affiliation__c != null
                                                          AND Id=: thisEnrollment.Id];
    System.assertEquals(ct_Constants.AFFILIATION_ROLE_STUDENT, thisTestEnrollment.hed__Affiliation__r.hed__Role__c, 'Should return Student');
    System.assertEquals(ct_Constants.AFFILIATION_STATUS_ALUMNI, thisTestEnrollment.hed__Affiliation__r.hed__Status__c, 'Should return Alumni');
    Test.stopTest();
  }
  @isTest
  private static void testUpdateCourseDetailBatch(){
    // first retrieve default EDA trigger handlers
   // List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    //tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_ProgramEnrollmentHandler_TDTM', 'Program_Enrollment__c', 'AfterUpdate', 2.00));
    // Pass trigger handler config to set method for this test run

    //hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    Account thisAccount = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    insert thisAccount;

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;

    hed__Program_Enrollment__c thisEnrollment = ct_TestDataFactory.createHedaProgramEnrollment(thisAccount.Id, thisContact.Id);
    //thisEnrollment.hed__Graduation_Year__c = '2019';
    insert thisEnrollment;

    ct_Advancement_Setting__mdt thisMetadata = new ct_Advancement_Setting__mdt(label= 'Program_Enrollment_Run_for_All', DeveloperName = 'Program_Enrollment_Run_for_All',
                                                Value__c = 'False', Purpose__c = 'Test');
    
    Database.executebatch(new ct_UpdateCourseDetailsToConScheduleBatch(), 50);
    
    Test.stopTest();
    hed__Program_Enrollment__c thisAssertEnrollmenmt = [SELECT Id, hed__Account__c, hed__Account__r.Name, hed__Graduation_Year__c, hed__Contact__c, CreatedDate 
                                      FROM hed__Program_Enrollment__c WHERE Id =:thisEnrollment.Id];
    Contact thisAssertContact = [SELECT Id, RecordTypeId, Last_Course_Year__c, Last_Course_Taken__c FROM Contact WHERE Id =:thisContact.Id];
    System.assertEquals(thisAssertEnrollmenmt.hed__Account__r.Name, thisAssertContact.Last_Course_Taken__c, 'Should return Same Account Name');
  }
  @IsTest
  public static void ct_UpdateCourseDetailsToConScheduleBatchTest(){
    Test.startTest();
    ct_UpdateCourseDetailsToConScheduleBatch schedulerClassObj = new ct_UpdateCourseDetailsToConScheduleBatch();
    schedulerClassObj.execute(Null);
    Test.stopTest();
  }
  @isTest
  private static void deleteProgramEnrollmentRecord(){
    // first retrieve default EDA trigger handlers
   List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();
    // Create our trigger handler using the constructor
    tokens.add(new hed.TDTM_Global_API.TdtmToken('ct_hed_ProgramEnrollmentHandler_TDTM', 'Program_Enrollment__c', 'BeforeDelete', 2.00));
    // Pass trigger handler config to set method for this test run

    hed.TDTM_Global_API.setTdtmConfig(tokens);
    Test.startTest();
    Account thisAccount = ct_TestDataFactory.createEducationalAccount('Test Student Org Account');
    thisAccount.RecordTypeId = ct_Constants.ACCOUNT_RECORDTYPE_ACADEMIC_PROGRAM;
    insert thisAccount;

    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;

    hed__Program_Enrollment__c thisEnrollment = ct_TestDataFactory.createHedaProgramEnrollment(thisAccount.Id, thisContact.Id);
    //thisEnrollment.hed__Graduation_Year__c = '2019';
    insert thisEnrollment;

    delete thisEnrollment;
    Test.stopTest();
    Contact thisAssertContact = [SELECT Id, RecordTypeId, Last_Course_Year__c, Last_Course_Taken__c FROM Contact WHERE Id =:thisContact.Id];
    System.assertEquals(true, String.isBlank(thisAssertContact.Last_Course_Taken__c), 'Should return true');


    
  }
}