/*******************************************************************************************************************************
* @name:          	rl_Case_To_Lead_TDTM_Test
* @description:		Test methods for rl_Case_To_Lead_TDTM
* 
*
* METHODS
* @testLeadCreation				Tests case creation where there isn't a matching Lead in the database
* @testMatchWithExistingLead	Tests case creation where there is a matching Lead in the database
*
*
* @Created:   28-04-2020 v0.1 - initial version - Rob Liesicke
* @Updated:   

********************************************************************************************************************************/

@IsTest
public class rl_Case_To_Lead_TDTM_Test {

    
/* =========================================================== */   
/*                      TEST LEAD CREATION                     */
/* =========================================================== */ 

   @IsTest (SeeAllData=true)
   static void testLeadCreation() {
    
		//  Retrieve default EDA trigger handlers
        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();

        // Create custom trigger handler
    	tokens.add(new hed.TDTM_Global_API.TdtmToken('rl_Case_To_Lead_TDTM', 'Case', 'AfterInsert', 2.00));

       	// Pass custom trigger handler to set method for this test run
       	hed.TDTM_Global_API.setTdtmConfig(tokens);
       
		List<Case> caseList = new List<Case>();
        List<Id> insertedCaseIds = new List<Id>();
       
       
        // Insert 20 leads with Live Chat details
        for(integer i=0; i<20; i++)
		{
            
           	Case testCase = new Case();
            
            testCase.LiveChat_FirstName__c = 'Ima ' + i;
            testCase.LiveChat_LastName__c = 'ChatCase ' + i;
            testCase.LiveChat_Email__c = i + 'ima@chatcase.com';
            testCase.LiveChat_Mobile__c = '0700054354'+ i;
            testCase.Subject = 'Live Chat Case';
            testCase.Origin = 'Live Chat';

            caseList.add(testCase);
            
        }            
        
        insert caseList;
            
            
        // Create list of the inserted Ids
        for (Case cs : caseList)
        {        
       		insertedCaseIds.add(cs.Id);
        }
        
       
		rl_Case_To_Lead_TDTM caseToLeadClass = new rl_Case_To_Lead_TDTM();

       
       	test.startTest();
       		caseToLeadClass.processCases(insertedCaseIds);
       	test.stopTest();
       
       
        // Boolean to check all Cases are updated with a Lead on Enquirer__c
        Boolean allCasesUpdated;
        
		
        // Get list of cases, will be 20 due to test setup
        List<Case> casesToCheck = 	[ SELECT Id, Subject, Enquirer__c 
                           	 		  FROM Case
                             		  WHERE Subject = 'Live Chat Case'
                           			];
        
        
        // Loop through the Cases to check all cases were updated
        for(Case caseToCheck : casesToCheck)
        {
            
            // Check Enquirer__c is populated
            if( caseToCheck.Enquirer__c != null )
            {
                
                allCasesUpdated = true;
                
            }
            
            // Else set Boolean to false and exit loop 
            else
            {
                
                allCasesUpdated = false;
                
                break;
                
            }    
        }
         
        
        // Ensure all Cases have updated correctly
        System.assertEquals(allCasesUpdated,true); 
        
    }           

    
    
/* =========================================================== */   
/*                      TEST LEAD MATCHING                     */
/* =========================================================== */ 

   @IsTest (SeeAllData=true)
   static void testMatchWithExistingLead() {
        
		//  Retrieve default EDA trigger handlers
        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();

        // Create custom trigger handler
    	tokens.add(new hed.TDTM_Global_API.TdtmToken('rl_Case_To_Lead_TDTM', 'Case', 'AfterInsert', 2.00));

       	// Pass custom trigger handler to set method for this test run
       	hed.TDTM_Global_API.setTdtmConfig(tokens);

       
		List<Case> caseList = new List<Case>();
        List<Id> insertedCaseIds = new List<Id>();             
       
       
        // Insert Lead to be matched with by test records
        Lead testLead = new Lead();
        
        testLead.FirstName = 'A';
        testLead.LastName = 'testLead1-UN1QU3';
        testLead.Email = 'a@testLead1.com';
        testLead.Enquiry_Channel__c = 'Live Chat';
        testLead.Company = 'Defaulted';
        
        insert testLead;

       
       	// Insert 20 Cases
        for(integer i=0; i<20; i++)
		{
           	Case testCase = new Case();
           
            testCase.LiveChat_FirstName__c = 'A';
            testCase.LiveChat_LastName__c = 'testLead1-UN1QU3';
            testCase.LiveChat_Email__c = 'a@testLead1.com';
            testCase.Subject = 'Live Chat Case';
            testCase.Origin = 'Live Chat';

            caseList.add(testCase); 
        }
     
        insert caseList;
       
       
        // Create list of the inserted Ids
        for (Case cs : caseList)
        {        
       		insertedCaseIds.add(cs.Id);
        }
        
       
		rl_Case_To_Lead_TDTM caseToLeadClass = new rl_Case_To_Lead_TDTM();

       
       	test.startTest();
       		caseToLeadClass.processCases(insertedCaseIds);
       	test.stopTest();
       
             
       String existingLeadId = 	[ SELECT Id, LastName
                                  FROM Lead
                                  WHERE LastName = 'testLead1-UN1QU3'
                                  LIMIT 1
                                ].Id;   
       
       
        // Boolean to check all Cases are updated with a Lead on Enquirer__c
        Boolean allCasesUpdated;
        
		
        // Get list of cases, will be 20 due to test setup
        List<Case> casesToCheck = 	[ SELECT Id, LiveChat_LastName__c, Enquirer__c 
                           	 		  FROM Case
                             		  WHERE LiveChat_LastName__c = 'testLead1-UN1QU3'
                           			];
        
        
        // Loop through the Cases to check all cases were updated
        for(Case caseToCheck : casesToCheck)
        {
            
            // Check Enquirer__c is populated
            if( caseToCheck.Enquirer__c == existingLeadId )
            {
                allCasesUpdated = true;
            }
            
            // Else set Boolean to false and exit loop 
            else
            {
                allCasesUpdated = false;
                
                break;
            }    
        }
         
        
        // Ensure all Cases have updated correctly
        System.assertEquals(allCasesUpdated,true); 
        
    }    
    
    
    
/* =========================================================== */   
/*                     TEST CONTACT MATCHING                   */
/* =========================================================== */ 

   @IsTest (SeeAllData=true)
   static void testMatchWithExistingContact() {
        
		//  Retrieve default EDA trigger handlers
        List<hed.TDTM_Global_API.TdtmToken> tokens = hed.TDTM_Global_API.getTdtmConfig();

        // Create custom trigger handler
    	tokens.add(new hed.TDTM_Global_API.TdtmToken('rl_Case_To_Lead_TDTM', 'Case', 'AfterInsert', 2.00));

       	// Pass custom trigger handler to set method for this test run
       	hed.TDTM_Global_API.setTdtmConfig(tokens);

       
		List<Case> caseList = new List<Case>();
        List<Id> insertedCaseIds = new List<Id>();             
       
       
        // Insert Contact to be matched with by test records
        Contact testContact = new Contact();
        
        testContact.FirstName = 'A';
        testContact.LastName = 'testContact1-UN1QU3';
        testContact.Email = 'a@testContact1.com';
        
        insert testContact;

       
       	// Insert 20 Cases
        for(integer i=0; i<20; i++)
		{
           	Case testCase = new Case();
           
            testCase.LiveChat_FirstName__c = 'A';
            testCase.LiveChat_LastName__c = 'testContact1-UN1QU3';
            testCase.LiveChat_Email__c = 'a@testContact1.com';
            testCase.Subject = 'Live Chat Case';
            testCase.Origin = 'Live Chat';

            caseList.add(testCase); 
        }
     
        insert caseList;
       
       
        // Create list of the inserted Ids
        for (Case cs : caseList)
        {        
       		insertedCaseIds.add(cs.Id);
        }
        
       
		rl_Case_To_Lead_TDTM caseToLeadClass = new rl_Case_To_Lead_TDTM();

       
       	test.startTest();
       		caseToLeadClass.processCases(insertedCaseIds);
       	test.stopTest();
       
             
       String existingContactId = 	[ SELECT Id, LastName
                                   	  FROM Contact
                                      WHERE LastName = 'testContact1-UN1QU3'
                                      LIMIT 1
                                	].Id;   
       	System.Debug('existingContactId='+existingContactId);
        // Boolean to check all Cases are updated with a Lead on Enquirer__c
        Boolean allCasesUpdated;
        
		
        // Get list of cases, will be 20 due to test setup
        List<Case> casesToCheck = 	[ SELECT Id, LiveChat_LastName__c, ContactId 
                           	 		  FROM Case
                             		  WHERE LiveChat_LastName__c = 'testContact1-UN1QU3'
                           			];
        
        
        // Loop through the Cases to check all cases were updated
        for(Case caseToCheck : casesToCheck)
        {
            System.Debug('currentContactId='+caseToCheck.ContactId);
            // Check ContactId is populated
            if( caseToCheck.ContactId == existingContactId )
            {
                allCasesUpdated = true;
            }
            
            // Else set Boolean to false and exit loop 
            else
            {
                allCasesUpdated = false;
                
                //break;
            }    
        }
         
        
        // Ensure all Cases have updated correctly
        System.assertEquals(allCasesUpdated,true); 
        
    }        
}