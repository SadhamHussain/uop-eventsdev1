public without sharing class CT_AddToCalendarController {
    
    @AuraEnabled(cacheable=true)
    public static CT_Calendar getCalendar(String recordId) {
        if (String.isBlank(recordId)) {
            throw new CT_CalendarException('Add To Calendar - recordId is missing');
        }
        return getCalendarById(Id.valueOf(recordId));
    }
    
    
    @TestVisible
    private static CT_Calendar getCalendarById(Id recordId) {
        CT_Calendar calendar;
        switch on recordId.getSobjectType().newSObject() {
            when Event__c event {
                event = fetchEventById(recordId);
                calendar = new CT_Calendar(event);
            } when Session__c session {
                session = fetchSessionById(recordId);
                calendar = new CT_Calendar(session);
            } when else {
                throw new CT_CalendarException('Add To Calendar - This object is not supported');
            }
        }
        return calendar;
    }
    
    @TestVisible
    private static Event__c fetchEventById(Id id) {
        return [SELECT Id, Name, Event_Description__c, Venue_City__c,
        Event_Start_Date__c, Start_Time__c, Event_End_Date__c, End_Time__c
        FROM Event__c WHERE Id =: id];
    } 
    
    @TestVisible
    private static Session__c fetchSessionById(Id id) {
        return [SELECT Id, Name, Event__r.Event_Description__c, Event__r.Venue_City__c,
        Session_Date__c, Start_Time__c, End_Time__c                 
        FROM Session__c WHERE Id =: id];
    }

    public class CT_CalendarException extends Exception{}
    
    public class CT_Calendar {
        @AuraEnabled public String subject;
        @AuraEnabled public Datetime startDateTime;
        @AuraEnabled public Datetime endDateTime;
        @AuraEnabled public String description;
        @AuraEnabled public String location;
        
        public CT_Calendar(Event__c event) {
            this.subject = event.Name;        
            this.startDateTime = Datetime.newInstance(event.Event_Start_Date__c, event.Start_Time__c);
            this.endDateTime = Datetime.newInstance(event.Event_End_Date__c, event.End_Time__c);
            this.description = event.Event_Description__c;
            this.location = event.Venue_City__c;
        }
        
        public CT_Calendar(Session__c session) {
            this.subject = session.Name;
            this.startDateTime = Datetime.newInstance(session.Session_Date__c, session.Start_Time__c);
            this.endDateTime = Datetime.newInstance(session.Session_Date__c, session.End_Time__c);
            this.description = session.Event__r.Event_Description__c;
            this.location = session.Event__r.Venue_City__c;
        }
    }
}