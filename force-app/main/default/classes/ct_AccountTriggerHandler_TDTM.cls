/**
 * @File Name          : ct_AccountTriggerHandler_TDTM.cls
 * @Description        : TDTM framework for Account Object 
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 07-08-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    6/5/2020   Creation Admin     Initial Version
**/
global class ct_AccountTriggerHandler_TDTM extends hed.TDTM_Runnable {

  global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
    hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {
    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CT ACCOUNT TDTM TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
    //Set of RAO Organisation Education Institution Ids
    Set<Id> RAO_EducationInstitutionAccountIDs       = new Set<Id>();

    if (triggerAction == hed.TDTM_Runnable.Action.AfterUpdate){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>ACCOUNT AFTER UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

      Map<Id, Account> triggerOldMap = new Map<Id, Account>((list<Account>)oldlist);
      Map<Id, Account> triggerNewMap = new Map<Id, Account>((list<Account>)newlist);

      //Loop to get The required Ids 
      /*for(Account thisAccount : triggerNewMap.values()){
        if(thisAccount.R_O_Organisation__c 
          && thisAccount.R_O_Organisation__c != triggerOldMap.get(thisAccount.Id).R_O_Organisation__c
          && thisAccount.RecordTypeId == ct_Constants.ACCOUNT_RECORDTYPE_EDUCATIONAL_INSTITUTION){
            RAO_EducationInstitutionAccountIDs.add(thisAccount.Id);
        }       
      }
      if(!RAO_EducationInstitutionAccountIDs.isEmpty()){
        //ct_AccountTriggerHelper.processRAOEducationInstitutionAffiliatedContact(RAO_EducationInstitutionAccountIDs);
      }*/

      //Set Related Contact's Address records to Private/Not private based on Account record
      ct_AccountTriggerHelper.setRelatedAddressToPrivate(triggerNewMap);
    }
    return dmlWrapper;
  }
}