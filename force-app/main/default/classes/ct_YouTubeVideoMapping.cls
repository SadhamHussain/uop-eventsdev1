/**
 * @File Name          : ct_YouTubeVideoMapping.cls
 * @Description        : Controller class for getting Training video URL 
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 7/01/2020, 01:35:00 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/29/2020   Creation Admin     Initial Version
**/
public class ct_YouTubeVideoMapping {
	@AuraEnabled(cacheable=true)
    public static Map<String,Object> getURL(string recordId){
        Map<String,Object> returnval = new Map<String,Object>();
        try{
        if(String.isEmpty(recordId)) return returnval;
        
        string objectName = findObjectNameFromRecordIdPrefix(recordId);
        sObject temp = Database.query('SELECT RecordTypeId FROM '+objectName+' WHERE Id=\''+recordId+'\'');
        String RecordTypeDeveloperName= (String)Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosById().get((String)temp.get('RecordTypeId')).getDeveloperName();
        /*Query on Metadata*/
        List<Training_Video_Mapping__mdt> metadataObjList =    [SELECT Id, DeveloperName, sObject_Name__c, Record_Type_Developer_Name__c, video_URL__c, Video_Title__c FROM Training_Video_Mapping__mdt WHERE sObject_Name__c=:objectName AND Record_Type_Developer_Name__c =:RecordTypeDeveloperName];
        returnval.put('result',metadataObjList);
        }catch(Exception e){
            returnval.put('error',e.getMessage());
        }
        return returnval;
    }
    public static String findObjectNameFromRecordIdPrefix(String recordIdOrPrefix){        
		String objectName = '';        
		try{            
			//Get prefix from record ID            
			//This assumes that you have passed at least 3 characters             
			String myIdPrefix = String.valueOf(recordIdOrPrefix).substring(0,3);       
			//Get schema information            
			Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe();                          
			//Loop through all the sObject types returned by Schema             
			for(Schema.SObjectType stype : gd.values()){   
				//if (!sObj.contains('__')) to exclude managed package objects                
				Schema.DescribeSObjectResult r = stype.getDescribe();                 
				String prefix = r.getKeyPrefix();                                     
				//Check if the prefix matches with requested prefix                 
				if(prefix!=null && prefix.equals(myIdPrefix)){                     
				objectName = r.getName();                              
				break;                 
				}             
			}         
		}catch(Exception e){  
			System.debug(e);         
		}        
		return objectName;     
	}
	@AuraEnabled(cacheable=true)
    public static List<Object> getAllRecords(String searchKey){
		if(String.isEmpty(searchKey)){
			return   [SELECT Id, DeveloperName, sObject_Name__c, Record_Type_Developer_Name__c, video_URL__c, Video_Title__c FROM Training_Video_Mapping__mdt ];
		}
		String key = '%' + searchKey + '%';
        return   [SELECT Id, DeveloperName, sObject_Name__c, Record_Type_Developer_Name__c, video_URL__c, Video_Title__c FROM Training_Video_Mapping__mdt WHERE Video_Title__c LIKE :key];
	}

}