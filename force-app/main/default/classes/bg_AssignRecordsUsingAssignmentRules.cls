/*****************************************************************
* bg_AssignRecordsUsingAssignmentRules
*
* Invocable class for triggering assignment rules for the specfied Object
* 
* Author: BrightGen
* Created: 28-09-2018
******************************************************************/

public class bg_AssignRecordsUsingAssignmentRules
{
    @InvocableMethod
    public static void assignRecords(List<Id> recordIds)
    {
        Schema.SObjectType objectType = recordIds[0].getSobjectType();
        Schema.SObjectType enquirerType = Schema.getGlobalDescribe().get('Lead');
        Schema.SObjectType caseType = Schema.getGlobalDescribe().get('Case');

        if(objectType === enquirerType)
        {
            List<Lead> enquirersToBeAssigned = new List<Lead>();

            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true; 

            List<Lead> enquirerRecords = [SELECT Id, Run_Assignment_Rules__c FROM Lead WHERE Id IN :recordIds];

            for(Lead e : enquirerRecords)
            {
                e.setOptions(dmo);
                e.Run_Assignment_Rules__c = false;
                enquirersToBeAssigned.add(e);
            }

            if(!enquirersToBeAssigned.isEmpty())
            {
                try
                {
                    update enquirersToBeAssigned;   
                }
                catch(DmlException e) 
                {
                    System.debug('+++ The following exception has occurred: ' + e.getMessage()); 
                }
            }
        }
        	
        else if(objectType === caseType)
        {
        	List<Case> casesToBeAssigned = new List<Case>();

            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true; 

            List<Case> caseRecords = [SELECT Id FROM Case WHERE Id IN :recordIds];

            for(Case c : caseRecords)
            {
                c.setOptions(dmo);
                casesToBeAssigned.add(c);
            }

            if(!casesToBeAssigned.isEmpty())
            {
                try
                {
                    update casesToBeAssigned;   
                }
                catch(DmlException e) 
                {
                    System.debug('+++ The following exception has occurred: ' + e.getMessage()); 
                }
            }
        }
    }
}