/**
 * @File Name          : ct_DirectDebitTriggerHandler_TDTM.cls
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * Description         : Execute the ct_DirectDebitsTriggerHelper
**/
global class ct_DirectDebitTriggerHandler_TDTM extends npsp.TDTM_Runnable {

    /**
    * @description After Trigger Context Helper Method
    * @param recurringDonationNewMap 
    * @param recurringDonationOldMap 
    * @return void 
    **/
    global override npsp.TDTM_Runnable.DmlWrapper run(List<SObject> newlist,
                                             List<SObject> oldlist,
                                             npsp.TDTM_Runnable.Action triggerAction,
                                             Schema.DescribeSObjectResult objResult) {

        npsp.TDTM_Runnable.dmlWrapper dmlWrapper = new npsp.TDTM_Runnable.DmlWrapper();
        
        Set<Id> recurringDonationIds                                  = new Set<Id>();
        Set<Id> primaryCampignRecurringDonationIds                    = new Set<Id>();
        Set<Id> authorisationIds                                      = new Set<Id>();

        if (triggerAction == npsp.TDTM_Runnable.Action.AfterInsert || triggerAction == npsp.TDTM_Runnable.Action.AfterUpdate) {
            for (npe03__Recurring_Donation__c thisRecurringDonation : (list<npe03__Recurring_Donation__c>)newlist) {
                    // Checking if General Account Unit has values or not
                if(thisRecurringDonation.General_Accounting_Unit__c != null){
                    recurringDonationIds.add(thisRecurringDonation.Id);       
                }
                // Checking if Primary Campaign source has values or not
                if(thisRecurringDonation.Primary_Campaign_Source__c != null){
                    primaryCampignRecurringDonationIds.add(thisRecurringDonation.Id);
                }
            }
            if(!recurringDonationIds.isEmpty()){
                // Calling populateGAUAllocation method 
                dmlWrapper.objectsToUpdate.addAll(ct_DirectDebitTriggerHelper.populateGAUAllocation(recurringDonationIds));
            }
            if(!primaryCampignRecurringDonationIds.isEmpty()){
                // Calling primaryCampignRecurringDonationIds method 
                dmlWrapper.objectsToUpdate.addAll(ct_DirectDebitTriggerHelper.populatePrimaryCampaignSource(primaryCampignRecurringDonationIds));
            }
        }

        if(triggerAction == npsp.TDTM_Runnable.Action.BeforeInsert){
            
            for(npe03__Recurring_Donation__c thisRecurringDonation : (list<npe03__Recurring_Donation__c>)newlist) {
               if(thisRecurringDonation.Asperato_Authorisation__c != null){
                  authorisationIds.add(thisRecurringDonation.Asperato_Authorisation__c);   
                }
            }
            if(!authorisationIds.isEmpty()){
                // Calling populateAuthorisationStatus method
                ct_DirectDebitTriggerHelper.populateAuthorisationStatus((list<npe03__Recurring_Donation__c>)newlist, authorisationIds); 
            }
        }

        if(triggerAction == npsp.TDTM_Runnable.Action.BeforeUpdate){
            Map<Id, npe03__Recurring_Donation__c> recurringDonationOldMap = new Map<Id, npe03__Recurring_Donation__c>((List<npe03__Recurring_Donation__c>) oldlist);
            // Loop through new list recurringDonationList for getting ids
            for(npe03__Recurring_Donation__c thisRecurringDonation : (list<npe03__Recurring_Donation__c>)newlist){
                // Checking if authorisation status is changing from other value to In force, then it will collect the recurring donation ids
                if(thisRecurringDonation.Asperato_Authorisation__c != null
                   && thisRecurringDonation.Authorisation_Status__c != recurringDonationOldMap.get(thisRecurringDonation.Id).Authorisation_Status__c 
                   && thisRecurringDonation.Authorisation_Status__c == ct_Constants.AUTHORIZATION_INFORCE_STATUS){
                   
                   recurringDonationIds.add(thisRecurringDonation.Id);
                }
                // Checking if asperato authorisation has value or not 
                if(thisRecurringDonation.Asperato_Authorisation__c != null){
                   authorisationIds.add(thisRecurringDonation.Asperato_Authorisation__c);   
                }
            }
            
            if(!authorisationIds.isEmpty()){
            // Calling populateAuthorisationStatus method
            ct_DirectDebitTriggerHelper.populateAuthorisationStatus((list<npe03__Recurring_Donation__c>)newlist, authorisationIds); 
            }
            if(!recurringDonationIds.isEmpty()){
                // Checking by pass settings for email process
                if(ct_CustomSettingService.getCurrentUserByPassConfiguration() == null
                || (!ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Emails_Off__c 
                && !ct_CustomSettingService.getCurrentUserByPassConfiguration().Are_Alumni_Emails_off__c)){
                // Calling sentEmailAlertDirectDebitSchedule method
                ct_DirectDebitTriggerHelper.sentEmailAlertDirectDebitSchedule(recurringDonationIds); 
                }
            }
        } 
        return dmlWrapper;   
    } 
}