/**
 * @File Name          : ct_WealthAssessmentComponentController.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 3/5/2020, 1:55:24 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   Creation Admin    Initial Version
**/
public class ct_WealthAssessmentComponentController {
  //Metadata wrapper to hold Opportunity record and picklist entries 
  public static WealthAssesmentWrapper thisWealthAssesmentWrapper = new WealthAssesmentWrapper();
  
  /**
  * @description Retrive Wealth Assessment Records 
  * @author Creation Admin | 24/4/2020 
  * @param wealthAssessmentId 
  * @param opportunityId 
  * @return WealthAssesmentWrapper 
  **/
  @AuraEnabled
  public static WealthAssesmentWrapper getWealthAssessmentRecord(String wealthAssessmentId, String opportunityId){
    try{
      //Create Wealth Assessment object and Query existing Wealth Assessment records if wealthAssessmentId is not null
      Wealth_Assessment__c thisWealthAssessment = new Wealth_Assessment__c();
        thisWealthAssessment =  [SELECT Id, Name, Wealth_Indicator__c, Date_Of_Wealth_Screen__c, Capacity_Score__c,
                                        Wealth_Screening_Bureau__c, Wealth_Screening_Bureau__r.Name, Property_Indicator__c
                                        FROM Wealth_Assessment__c 
                                        WHERE Id = :wealthAssessmentId LIMIT 1];
      //Invoke updateOpportunityRecord method to update Opportunity record with Wealth Assessment values
      return updateOpportunityRecord(thisWealthAssessment, opportunityId);
    }
    catch (Exception e) {
      //Exception mechanism to log any exceptions occured during the dml operation
      ct_Logger.logMessage(e, 'Opportunity');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  } 

  /**
  * @description Update Opportunity With Wealth Assessment Record Values
  * @author Creation Admin | 24/4/2020 
  * @param wealthAssessmentRecord 
  * @param opportunityId 
  * @return WealthAssesmentWrapper 
  **/
  @AuraEnabled
  public static WealthAssesmentWrapper updateOpportunityRecord(Wealth_Assessment__c wealthAssessmentRecord, String opportunityId){
    Opportunity thisOpportunity = [SELECT Id, Wealth_Assessment__c, Wealth_Assessment__r.Name, Date_Of_Wealth_Screen__c, 
                                          Wealth_Screening_Bureau__c, Wealth_Screening_Bureau__r.Name, npsp__Primary_Contact__c,
                                          Wealth_Indicator__c, Property_Indicator__c, StageName, Capacity_Score__c
                                          FROM Opportunity
                                          WHERE Id = :opportunityId
                                          LIMIT 1];

    //Update Opportunity with Wealth Assessment record values only when Wealth Assessment Record is present
    if(wealthAssessmentRecord != null){

      if(wealthAssessmentRecord.Date_Of_Wealth_Screen__c != null){
        thisOpportunity.Date_Of_Wealth_Screen__c = wealthAssessmentRecord.Date_Of_Wealth_Screen__c;
      }

      thisOpportunity.Wealth_Assessment__c = wealthAssessmentRecord.Id;
      thisOpportunity.Wealth_Screening_Bureau__c = wealthAssessmentRecord.Wealth_Screening_Bureau__c;
      thisOpportunity.Wealth_Indicator__c = wealthAssessmentRecord.Wealth_Indicator__c;
      thisOpportunity.Property_Indicator__c = wealthAssessmentRecord.Property_Indicator__c;
      thisOpportunity.Capacity_Score__c = wealthAssessmentRecord.Capacity_Score__c;
      update thisOpportunity;
    }

    /* set  Wealth Assessment values to wealthAssessmentName & bureauName when a Wealth Assessment Record is updated in UI*/
    if(thisOpportunity.Wealth_Assessment__c != null && wealthAssessmentRecord != null){
      thisWealthAssesmentWrapper.wealthAssessmentName = wealthAssessmentRecord.Name;
      thisWealthAssesmentWrapper.bureauName = wealthAssessmentRecord.Wealth_Screening_Bureau__r.Name;
    }
    /* set  Wealth Assessment values to wealthAssessmentName & bureauName when a Wealth Assessment Record is already
    linked to Opportunity on load*/
    else if(thisOpportunity.Wealth_Assessment__c != null && wealthAssessmentRecord == null){
      thisWealthAssesmentWrapper.wealthAssessmentName = thisOpportunity.Wealth_Assessment__r.Name;
      thisWealthAssesmentWrapper.bureauName = thisOpportunity.Wealth_Screening_Bureau__r.Name;
    }

    //Get Wealth Assessments linked under Primary Contact & construct as picklist entries
    thisWealthAssesmentWrapper.assessmentPickListWrapperList = getWealthAssessments(thisOpportunity.npsp__Primary_Contact__c);
    thisWealthAssesmentWrapper.thisOpportunityRecord = thisOpportunity;
  
    return thisWealthAssesmentWrapper;
  } 

  /**
  * @description Clear Wealth Assessment values from Opportunity on removing Wealth Assessment from UI
  * @author Creation Admin | 3/5/2020 
  * @param opportunityId 
  * @return void 
  **/
  @AuraEnabled
  public static void clearWealthAssessmentValuesFromOpportunity(String opportunityId){
    try{
      //Create an opportunity instance with provided Opportunity Id and set Wealth Assessment values to null
      Opportunity thisOpportunity = new Opportunity();
      thisOpportunity.Id = opportunityId;
      thisOpportunity.Wealth_Assessment__c = null;
      thisOpportunity.Date_Of_Wealth_Screen__c = null;
      thisOpportunity.Wealth_Screening_Bureau__c = null;
      thisOpportunity.Wealth_Indicator__c = null;
      thisOpportunity.Property_Indicator__c = null;
      thisOpportunity.Capacity_Score__c = null;
      update thisOpportunity;
    }
    catch (Exception e) {
      //Exception mechanism to log any exceptions occured during the dml operation
      ct_Logger.logMessage(e, 'Opportunity');
      throw new AuraHandledException('Error '+ e.getMessage());    
    }
  }

    /**
    * Method to Populate Picklist Wrapper with GAU Units to the Map from set of string 
    * @param mapKey : Key for which Picklist Wrapper to be assigned
    * @param thisPicklistMap : Map of Picklist Wrapper List
    * @param picklistValuesSet : Set of string which contains Picklist values
  */
  public static List<PicklistEntryWrapper> getWealthAssessments(Id contactId){
        
    List<PicklistEntryWrapper> thisPickListWrapperList = new List<PicklistEntryWrapper>();
    //Get Wealth Assessment linked under Primary Contact & Construct as picklist entries
    for(Wealth_Assessment__c thisWealthAssessment :[SELECT Id, Name 
                                                           FROM Wealth_Assessment__c 
                                                           WHERE Contact__c = :contactId 
                                                           AND Contact__c != null
                                                           LIMIT 200]){
    	thisPickListWrapperList.add(new PicklistEntryWrapper(thisWealthAssessment.Name, thisWealthAssessment.Id));
    }

    //When no records are found add message
    if(thisPickListWrapperList.isEmpty()){
      thisPickListWrapperList.add(new PicklistEntryWrapper('No Wealth Assessments were found', ''));
    }
    return  thisPickListWrapperList;
  }

    /**
    * Wrapper to bind Picklist Values
  */
  public class PicklistEntryWrapper{
    //get set string to store lable of picklist entry
    @AuraEnabled
    public String label                  {get;set;}
    //get set string to store value of picklist entry
    @AuraEnabled
    public string value                  {get;set;}

    public PicklistEntryWrapper(String entryLabel,String entryValue){
      this.label    = entryLabel;
      this.value    = entryValue;
    }
  }

  public class WealthAssesmentWrapper{
    //get set variable to store Opportunity record
    @AuraEnabled
    public  Opportunity thisOpportunityRecord;
    //get set string to store Wealth Assessment name
    @AuraEnabled
    public  String wealthAssessmentName;
    //get set string to store Bureau name
    @AuraEnabled
    public  String bureauName;
    //get set picklist wrapper list to store Wealth Assessments as picklist entries
    @AuraEnabled
    public List<PicklistEntryWrapper> assessmentPickListWrapperList;

    //Initialize variables
    public WealthAssesmentWrapper(){
      this.thisOpportunityRecord = new Opportunity();
      this.wealthAssessmentName = '';
      this.bureauName = '';
      this.assessmentPickListWrapperList = new List<PicklistEntryWrapper>();
    }
  }
}