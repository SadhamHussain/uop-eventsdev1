////////////////////////////////////////////////////////////////////////////////
//  bg_CampaignCheckinHandlerController
//
//  Controller for CampaignCheckInManager lwc component
//
//  @Author:        Thomas Packer - BrightGen Ltd
//  @Created:       21/01/2020
//
//  @Changes: 
////////////////////////////////////////////////////////////////////////////////
public without sharing class bg_CampaignCheckinHandlerController 
{
    ////////////////////////////////////////////////////////////////////////////
	//  @author: Thomas Packer (BrightGen)
	//  @created: 21st January 2020
	//  @description: Method to get campaign Members
	//
	//  @changes:	
    //  @usage: 
	////////////////////////////////////////////////////////////////////////////
    @AuraEnabled(cacheable=true)
    public static Map<Id, CampaignMember> GetCampaignMembers(Id campaignId)
    {
        return bg_CampaignMemberUtils.GetCampaignMembersFromCampaignId(campaignId);
    }

    @AuraEnabled
    public static Integer GetCampaignMemberCount(String searchString, String campaignIdStr){
        Id campaignId = campaignIdStr;
        String query = 'SELECT COUNT() FROM CampaignMember WHERE CampaignId =\'' + campaignId + '\'';
        if (searchString != null && searchString !='')
        {
            query += 'AND (Name LIKE \'%' + searchString + '%\' OR Email LIKE \'%' + searchString + '%\')';
        }
        return Integer.valueOf(Database.countQuery(query));
    }

    @AuraEnabled
    public static Map<Id,CampaignMember> GetCampaignMembersList(Integer pageNumber, Integer numberOfRecords, Integer pageSize, String searchString, String campaignIdStr)
    {
        Id campaignId = campaignIdStr;
        String searchKey = '%' + searchString + '%';
        String query = 'SELECT Id, FirstName, LastName, Email, Status, CampaignId FROM CampaignMember WHERE CampaignId =\'' + campaignId + '\'';
        
        if (searchString != null && searchString != '')
        {
            query += 'AND (Name LIKE \'%' + searchString + '%\' OR Email LIKE \'%' + searchString + '%\')';
        }

        query += 'ORDER BY LastName LIMIT ' + pageSize + ' OFFSET ' + (pageSize * (pageNumber - 1));
        Map<Id,CampaignMember> campaignMembers = new Map<id,CampaignMember>();
        List<CampaignMember> campaignMemberList =  Database.query(query);
        for (CampaignMember cm : campaignMemberList)
        {
            campaignMembers.put(cm.Id,cm);
        }
        if(!campaignMembers.isEmpty())
        {
            return campaignMembers;
        }

        return null;
    }
}