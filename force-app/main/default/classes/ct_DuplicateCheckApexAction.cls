/**
 * @File Name          : ct_DuplicateCheckApexAction.cls
 * @Description        : The apex class is meant to provide methods around DC tool for finding 
 *                      duplicate contact or lead based on scenarios defined in DC. It then further
 *                      filters the records based on filters provided to suit Enquiry Management tool for e.g.
 *                      recordtype matching etc. 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 7/1/2020, 11:30:28 AM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    12/15/2019   Creation Admin     Initial Version
**/
global class ct_DuplicateCheckApexAction {
  //Record Type Id variable for respective Lead Record type which are returned from ct_Util class
  public static final Id LEAD_RECORDTYPE_EMPLOYER   = ct_Util.getRecordTypeIdByDevName('Lead', 'Employer');
  public static final Id LEAD_RECORDTYPE_STUDENT    = ct_Util.getRecordTypeIdByDevName('Lead', 'Student');
  public static final Id LEAD_RECORDTYPE_INFLUENCER = ct_Util.getRecordTypeIdByDevName('Lead', 'Influencer');
  @TestVisible 
  public static Map<String, List<dupcheck.dc3SearchResult>> searchResult;
  
  //baseORGURL : Variable to store Current Host connection name which is used in redirecting operations
  public static String baseORGURL {
    get {
      if(baseORGURL == null) {
        //URL class to get Current Host connection Name
        baseORGURL = Url.getOrgDomainUrl().toExternalForm();
      }
      return baseORGURL;
    }
    set;
  }

  /**
    * Apex action to call Duplicate Check Function to get duplicate Lead and contact 
    * @param thisLeadSearchWrapper : Wrapper that contains Search details
    * Return Results : The Wrapper which contains all duplciate records
  */
  @InvocableMethod
  public static List<Results> getDuplicateLead(List<searchLeadWrapper> thisLeadSearchWrapper){
    //Initialise dupcheck API Object to invoke the API methods
    dupcheck.dc3Api api = new dupcheck.dc3Api(); 
    //Create a sample lead data from the search wrapper passed from the Flow
    Lead searchLead = new Lead(FirstName=thisLeadSearchWrapper[0].leadFirstName, LastName=thisLeadSearchWrapper[0].leadlastName, Email=thisLeadSearchWrapper[0].leadEmailAddress);
    //Duplicate Check API to get Duplicated Records with the sample lead data
    if (!Test.isRunningTest()) {
      searchResult = api.doSearch(searchLead); 
    }
    //The duplicate records returned by the dupe check API will contain only some field. If extra field value for the records are required 
    //configure them in DC Setup for respective Objects

    //Store the Lead Duplicates that are returned from the Dupecheck API to ct_LeadDuplicateResult Wrapper class
    List<ct_LeadDuplicateResult> thisleadDuplicateResult = new List<ct_LeadDuplicateResult>();
    //Retrive Lead Duplicates from the result returned by the Dupe check API
    getLeadDuplicates(thisleadDuplicateResult, thisLeadSearchWrapper, searchResult);
    //Avoid retriving Contact Duplicates for Influencer Lead
    if(thisLeadSearchWrapper[0].leadRecordTypeID != LEAD_RECORDTYPE_INFLUENCER){
      //Retrive Contact Duplicates from the result returned by the Dupe check API
      getContactDuplicates(thisleadDuplicateResult, thisLeadSearchWrapper, searchResult);
    }
    //Store the Duplicate record Wrapper list in Results Wrapper
    Results leadDuplicateResult = new Results();
    leadDuplicateResult.thisLeadDuplicateList.thisLeadResultList = thisleadDuplicateResult;
    List<Results> resultsList   = new List<Results>();
    resultsList.add(leadDuplicateResult);
    return resultsList;
  }        

  /**
    * Method To get All Lead duplicates matching search criteria
    * @param thisAPI : dupcheck Object
    * @param thisleadDuplicateWrapper : List to store Duplicates records
    * @param thisLeadSearchWrapper :  Wrapper that contains Search detail
  */
  public static void getLeadDuplicates(List<ct_LeadDuplicateResult> thisleadDuplicateWrapper, List<searchLeadWrapper> thisLeadSearchWrapper, Map<String, List<dupcheck.dc3SearchResult>> duplicateSearchResult){
    //Retrive the Lead duplicates from the Dupe API Result map using the Lead Object Record Key 00Q
    if(duplicateSearchResult.containskey('00Q')){
      //Loop through each Lead duplicates and create ct_LeadDuplicateResult wrapper variables
      for (dupcheck.dc3SearchResult thisDuplicate : duplicateSearchResult.get('00Q')) {
        //Restrict Lead duplicates by requested record type
        if(Id.valueOf(thisLeadSearchWrapper[0].leadRecordTypeID) == ct_Util.getRecordTypeIdByDevName('Lead', String.valueOf(thisDuplicate.getDisplayField().get('RecordTypeId')))){
          //Create ct_LeadDuplicateResult instants to store the Lead duplicates
          ct_LeadDuplicateResult thisDuplicateLead = new ct_LeadDuplicateResult();
          //Populate respective data attributes for the ct_LeadDuplicateResult wrapper from the list returned by API
          //If extra attributes are needed then create an aura variable in ct_LeadDuplicateResult class
          //The Description for each ct_LeadDuplicateResult Attributes are mentioned in ct_LeadDuplicateResult Class
          thisDuplicateLead.displayText         = thisDuplicate.getDisplayField().get('Name')+' '+thisDuplicate.getDisplayField().get('Email');
          thisDuplicateLead.leadEmailAddress    = String.valueOf(thisDuplicate.getDisplayField().get('Email'));
          thisDuplicateLead.IdValue             = String.valueOf(thisDuplicate.getDisplayField().get('Id'));
          thisDuplicateLead.recordURL           = baseORGURL+'/'+String.valueOf(thisDuplicate.getDisplayField().get('Id'));
          thisDuplicateLead.selectedLeadId      = String.valueOf(thisDuplicate.getDisplayField().get('Id'));
          thisDuplicateLead.selectedContactId   = null;
          thisDuplicateLead.isContact           = false;
          thisleadDuplicateWrapper.add(thisDuplicateLead);
        }
      } 
    }
  }

  /**
    * Method To get All contact duplicates matching search criteria
    * @param thisAPI : dupcheck Object
    * @param thisleadDuplicateWrapper : List to store Duplicates records
    * @param thisLeadSearchWrapper :  Wrapper that contains Search detail
  */
   public static void getContactDuplicates(List<ct_LeadDuplicateResult> thisleadDuplicateWrapper, List<searchLeadWrapper> thisLeadSearchWrapper,Map<String, List<dupcheck.dc3SearchResult>> duplicateSearchResult){
    
    //Retrive the Lead duplicates from the Dupe API Result map using the Lead Object Record Key 003
    if(duplicateSearchResult.containskey('003')){
      //Loop through each Lead duplicates and create ct_LeadDuplicateResult wrapper variables
      for (dupcheck.dc3SearchResult thisDuplicate : duplicateSearchResult.get('003')) {
        //Create ct_LeadDuplicateResult instants to store the Lead duplicates
        ct_LeadDuplicateResult thisDuplicateLead    = new ct_LeadDuplicateResult();
        //Populate respective data attributes for the ct_LeadDuplicateResult wrapper from the list returned by API
        //If extra attributes are needed then create an aura variable in ct_LeadDuplicateResult class
        //The Description for each ct_LeadDuplicateResult Attributes are mentioned in ct_LeadDuplicateResult Class
        thisDuplicateLead.displayText         = thisDuplicate.getDisplayField().get('Name')+' '+thisDuplicate.getDisplayField().get('Email');
        thisDuplicateLead.leadEmailAddress    = String.valueOf(thisDuplicate.getDisplayField().get('Email'));
        thisDuplicateLead.IdValue             = String.valueOf(thisDuplicate.getDisplayField().get('Id'));
        thisDuplicateLead.selectedLeadId      = null;
        thisDuplicateLead.selectedContactId   = String.valueOf(thisDuplicate.getDisplayField().get('Id'));
        thisDuplicateLead.isContact           = true;
        thisDuplicateLead.recordURL           = baseORGURL+'/'+thisDuplicateLead.selectedContactId;
        thisleadDuplicateWrapper.add(thisDuplicateLead);
      } 
    }
  }

  //A Wrapper class which has data attributes which is passed to Dupe Check search API to get the Duplicate record that matchs the criteria in search wrappe
  global class searchLeadWrapper {
    //Make sure to create the Data attributes as InvocableVariable so that they can be accessed in flows

    //Attribute to store Lead's First name
    @InvocableVariable 
    global String leadFirstName;
    //Attribute to store Lead's last name
    @InvocableVariable 
    global String leadlastName;
     //Attribute to store Lead's Email
    @InvocableVariable 
    global String leadEmailAddress;
     //Attribute to store Leads Record Type Id
    @InvocableVariable 
    global String leadRecordTypeID;
  }
  
  //Wrapper class to hold the Duplicate records
  global class Results {
    @InvocableVariable
    global ct_LeadDuplicateResultList thisLeadDuplicateList;
    global Results(){
      thisLeadDuplicateList = new ct_LeadDuplicateResultList(); 
    }
  }
}