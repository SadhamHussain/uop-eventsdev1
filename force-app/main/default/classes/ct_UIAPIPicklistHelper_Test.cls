/**
 * @File Name          : ct_UIAPIPicklistHelper_Test.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 2/14/2020, 6:32:07 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/9/2019   Creation Admin     Initial Version
**/
@isTest
global class ct_UIAPIPicklistHelper_Test {
 	 global class Get_PicklistValues_Mock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest thisRequest) {
        System.assertEquals('GET', thisRequest.getMethod());
        HttpResponse thisResponse = new HttpResponse();
        thisResponse.setHeader('Content-Type', 'application/json');
        string responseBody = '{'+
                    '"controllerValues": {},'+
                    '"defaultValue": null,'+
                    '"eTag": "113edd3ea2e22bafd5187cc79ab8165c",'+
                    '"url": "/services/data/v46.0/ui-api/object-info/Case/picklist-values/0121r000000igS0AAI/Level_of_Study__c",'+
                    '"values": ['+
                        '{'+
                            '"attributes": null,'+
                            '"label": "Continuous Professional Development (CPD)",'+
                            '"validFor": [],'+
                            '"value": "Continuous Professional Development (CPD)"'+
                        '},'+
                        '{'+
                            '"attributes": null,'+
                            '"label": "CPD",'+
                            '"validFor": [],'+
                            '"value": "CPD"'+
                        '},'+
                        '{'+
                            '"attributes": null,'+
                            '"label": "Degree Apprenticeship",'+
                            '"validFor": [],'+
                            '"value": "Degree Apprenticeship"'+
                        '}'+
                    ']'+
                  '}';
        thisResponse.setBody(responseBody);
        thisResponse.setStatusCode(200);
        return thisResponse;
    }
  }

    @isTest
    static void getPicklistCalloutTest() {
    List<ct_UIAPIPicklistHelper.picklistValuesWrapper> picklistWrapList = new List<ct_UIAPIPicklistHelper.picklistValuesWrapper>();
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new Get_PicklistValues_Mock());
    picklistWrapList = ct_UIAPIPicklistHelper.getPicklistValues('caseRecordTypeId', 'Case','Level_of_Study__c');
    Test.stopTest();
  }
}