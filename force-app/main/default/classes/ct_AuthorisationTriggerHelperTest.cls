/**
 * @File Name          : ct_AuthorisationTriggerHelperTest.cls
 * @Description        : Test Class for ct_AuthorisationTriggerHelper Class
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 6/4/2020, 8:39:59 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/2/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_AuthorisationTriggerHelperTest {
  
  /**
  * @description Test Method to test directDebitGoCardlessCancellationProcess method
  * @return void 
  **/
  @isTest(SeeAllData=true)
  public static void cancelRecurringDonationRecordGoCardlessTransaction(){
    
    npe03__Recurring_Donations_Settings__c npspRecurringDonationSetting = npe03__Recurring_Donations_Settings__c.getInstance();
    
    Contact thisContact = ct_TestDataFactory.createContact();
    insert thisContact;
    
    Campaign thisCampaign   = new Campaign();
    thisCampaign.Name       = 'Test Campaign';
    insert thisCampaign;
      
    Project__c thisCreationFundProject = ct_TestDataFactory.createProject();
    insert thisCreationFundProject;

    npsp__General_Accounting_Unit__c thisGAUForDD   = ct_TestDataFactory.createGAURecord('Default GAU');
    thisGAUForDD.Project__c                         = thisCreationFundProject.Id;
    insert thisGAUForDD; 

    asp04__Authorisation__c thisAuthorisation = ct_TestDataFactory.createAuthorisation(thisContact);
    insert thisAuthorisation;

    Test.startTest();
    npe03__Recurring_Donation__c thisRecurringDonation    = ct_TestDataFactory.createRecurringDonation(thisContact, thisAuthorisation.Id, 200.00);
    thisRecurringDonation.General_Accounting_Unit__c      = thisGAUForDD.Id;
    thisRecurringDonation.Primary_Campaign_Source__c      = thisCampaign.Id;
    thisRecurringDonation.npe03__Installments__c          = 1;
    insert thisRecurringDonation;

    
    List<Opportunity> opportunityList = [SELECT Id , RecordTypeId, RecordType.DeveloperName, npe03__Recurring_Donation__c, LeadSource
                                            FROM Opportunity
                                            WHERE npe03__Recurring_Donation__c =:thisRecurringDonation.Id];
                                            
    opportunityList[0].RecordTypeId             = npspRecurringDonationSetting.npe03__Record_Type__c;
    update opportunityList;
    thisAuthorisation.Recurring_Donations__c    = thisRecurringDonation.Id;
    thisAuthorisation.asp04__Status__c          = ct_Constants.ASPERATO_AUTHORISATION_STATUS_CANCELLED;
    update thisAuthorisation;
    Test.stopTest();

    npe03__Recurring_Donation__c thisCancelledRecurringDonation = [SELECT Id, Direct_Debit_Cancellation__c, Direct_Debit_Status__c, npe03__Open_Ended_Status__c
                                                                    FROM npe03__Recurring_Donation__c
                                                                    WHERE Id =: thisRecurringDonation.Id];

    Opportunity thisCancelledOpportunity = [SELECT Id, npe03__Recurring_Donation__c, StageName, 
                                              npsp__Closed_Lost_Reason__c, Lost_Reason__c,
                                              CloseDate
                                              FROM Opportunity
                                              WHERE Id =: opportunityList[0].Id];

    System.assertEquals(thisCancelledOpportunity.npsp__Closed_Lost_Reason__c , ct_Constants.OPPORTUNITY_LOST_REASON_DIRECT_DEBIT_CANCELLED, 'Updating Closed Lost Reason as Direct Debit Cancelled');
    System.assertEquals(thisCancelledOpportunity.StageName , ct_Constants.OPPORTUNITY_STAGE_CLOSED_LOST, 'Updating Stage to Closed Lost');
    System.assertEquals(thisCancelledOpportunity.Lost_Reason__c , ct_Constants.OPPORTUNITY_LOST_REASON_DIRECT_DEBIT_CANCELLED, 'Updating Lost Reason as Direct Debit Cancelled');
    System.assertEquals(thisCancelledRecurringDonation.Direct_Debit_Cancellation__c , ct_Constants.DD_CANCELLATION_GOCARDLESS_DD_CANCELLATION, 'Testing the GoCardless Cancellation Process');
    System.assertEquals(thisCancelledRecurringDonation.Direct_Debit_Status__c , ct_Constants.DIRECT_DEBIT_AUTHORISATION_STATUS_CANCELLED, 'Updating the status to Cancel');
    System.assertEquals(thisCancelledRecurringDonation.npe03__Open_Ended_Status__c , ct_Constants.DIRECT_DEBIT_OPEN_ENDED_CLOSED, 'Updated the Open Status to Closed');
  }

  @isTest(SeeAllData=true)
  public static void cancelRecurringDonationRecordManualTransaction(){
    
    npe03__Recurring_Donations_Settings__c npspRecurringDonationSetting = npe03__Recurring_Donations_Settings__c.getInstance();
    
    Contact thisContact   = ct_TestDataFactory.createContact();
    insert thisContact;
    
    Campaign thisCampaign = new Campaign();
    thisCampaign.Name     = 'Test Campaign';
    insert thisCampaign;
      
    Project__c thisCreationFundProject = ct_TestDataFactory.createProject();
    insert thisCreationFundProject;

    npsp__General_Accounting_Unit__c thisGAUForDD = ct_TestDataFactory.createGAURecord('Default GAU');
    thisGAUForDD.Project__c = thisCreationFundProject.Id;
    insert thisGAUForDD; 

    asp04__Authorisation__c thisAuthorisation = ct_TestDataFactory.createAuthorisation(thisContact);
    insert thisAuthorisation;

    Test.startTest();
    npe03__Recurring_Donation__c thisRecurringDonation    = ct_TestDataFactory.createRecurringDonation(thisContact, thisAuthorisation.Id, 200.00);
    thisRecurringDonation.General_Accounting_Unit__c      = thisGAUForDD.Id;
    thisRecurringDonation.Primary_Campaign_Source__c      = thisCampaign.Id;
    thisRecurringDonation.npe03__Installments__c          = 1;
    insert thisRecurringDonation;

    ct_DirectDebitCancellationController.initialWrapper thisInitialWrapper = ct_DirectDebitCancellationController.fetchInitialWrapper(thisRecurringDonation.Id);
    //Pause Direct Debit
    npe03__Recurring_Donation__c thisPauseDirectDebit = ct_PauseDirectDebitController.getDirectDebitRecord(thisRecurringDonation.Id);

    List<Opportunity> opportunityList = [SELECT Id , RecordTypeId, RecordType.DeveloperName, npe03__Recurring_Donation__c, LeadSource
                                            FROM Opportunity
                                            WHERE npe03__Recurring_Donation__c =:thisRecurringDonation.Id];
                                            
    opportunityList[0].RecordTypeId             = npspRecurringDonationSetting.npe03__Record_Type__c;
    update opportunityList;
    thisAuthorisation.Recurring_Donations__c    = thisRecurringDonation.Id;
    thisAuthorisation.asp04__Status__c          = ct_Constants.ASPERATO_AUTHORISATION_STATUS_CANCELLED;
    update thisAuthorisation;
    Test.stopTest();

    String jsonString ='{"thisRecurringDonation":{"Id":"'+ thisRecurringDonation.Id +'","npe03__Open_Ended_Status__c":"Open","Asperato_Authorisation__c":"'+thisAuthorisation.Id+'"}}';

    //Pause Update Direct Debit Test
    Id thisPauseUpdateDirectDebit = ct_PauseDirectDebitController.updateDirectDebit(jsonString);
    try{
      String jsonStringException ='{"thisRecurringDonation":{"Id":"TestId","npe03__Open_Ended_Status__c":"Open","Asperato_Authorisation__c":"'+thisAuthorisation.Id+'"}}';
      thisPauseUpdateDirectDebit = ct_PauseDirectDebitController.updateDirectDebit(jsonStringException);
    }
    catch (Exception e) {

    }
    
    Id thisCancelledDDID = ct_DirectDebitCancellationController.cancelDirectDebit(jsonString);

    npe03__Recurring_Donation__c thisCancelledRecurringDonation = [SELECT Id, Direct_Debit_Cancellation__c, Direct_Debit_Status__c, npe03__Open_Ended_Status__c
                                                                    FROM npe03__Recurring_Donation__c
                                                                    WHERE Id =: thisCancelledDDID];

    Opportunity thisCancelledOpportunity = [SELECT Id, npe03__Recurring_Donation__c, StageName, 
                                              npsp__Closed_Lost_Reason__c, Lost_Reason__c,
                                              CloseDate
                                              FROM Opportunity
                                              WHERE Id =: opportunityList[0].Id];

    System.assertEquals(thisCancelledOpportunity.npsp__Closed_Lost_Reason__c , ct_Constants.OPPORTUNITY_LOST_REASON_DIRECT_DEBIT_CANCELLED, 'Updating Closed Lost Reason as Direct Debit Cancelled');
    System.assertEquals(thisCancelledOpportunity.StageName , ct_Constants.OPPORTUNITY_STAGE_CLOSED_LOST, 'Updating Stage to Closed Lost');
    System.assertEquals(thisCancelledOpportunity.Lost_Reason__c , ct_Constants.OPPORTUNITY_LOST_REASON_DIRECT_DEBIT_CANCELLED, 'Updating Lost Reason as Direct Debit Cancelled');
    System.assertEquals(thisCancelledRecurringDonation.Direct_Debit_Cancellation__c , ct_Constants.DD_CANCELLATION_MANUAL_DD_CANCELLATION, 'Testing the GoCardless Cancellation Process');
    System.assertEquals(thisCancelledRecurringDonation.Direct_Debit_Status__c , ct_Constants.DIRECT_DEBIT_AUTHORISATION_STATUS_CANCELLED, 'Updating the status to Cancel');
    System.assertEquals(thisCancelledRecurringDonation.npe03__Open_Ended_Status__c , ct_Constants.DIRECT_DEBIT_OPEN_ENDED_CLOSED, 'Updated the Open Status to Closed');
  }
}