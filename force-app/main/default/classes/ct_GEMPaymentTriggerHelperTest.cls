/**
 * @File Name          : ct_GEMPaymentTriggerHelperTest.cls
 * @Description        : Test Class for ct_GEMPaymentTriggerHelper Class
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 5/8/2020, 11:30:53 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/18/2020   Creation Admin     Initial Version
**/
@isTest
public class ct_GEMPaymentTriggerHelperTest {
  
  /**
  * @description Test Method to test sentEmailAlertForPaidDonation Method 
  * @return void 
  **/
  @isTest(SeeAllData=true)
  public static void sentEmailAlertForPaidDonationTest(){
    Id orgWideEmailId;
    OrgWideEmailAddress[] orgWideAddress = ct_Util.getOrgWideEmail(ct_Constants.UOP_ADVANCEMENT_TEAM_ADDRESS); 

    ct_Advancement_Setting__mdt thisHighValueDonationMetadata   = new ct_Advancement_Setting__mdt();
    thisHighValueDonationMetadata.MasterLabel                   = 'High Value Donation';
    thisHighValueDonationMetadata.Amount_Value__c               = 500.00;
    thisHighValueDonationMetadata.Notification_Message__c       = 'Test notification From Test Creation class';
    thisHighValueDonationMetadata.DeveloperName                 = ct_Constants.HIGH_VALUE_ONLINE_DONATION_ADVANCEMENT_SETTING;

    ct_MetadataService.advancementSettingMetaData.put(ct_Constants.HIGH_VALUE_ONLINE_DONATION_ADVANCEMENT_SETTING, thisHighValueDonationMetadata);

    EmailTemplate thisThankYouEmailTemplate = [SELECT Id, DeveloperName, HTMLValue 
                                                FROM EmailTemplate 
                                                WHERE DeveloperName =: ct_Constants.THANK_DONOR_FOR_DONATION_EMAIL_TEMPLATE  
                                                AND IsActive = true];
    Account thisAccount = new Account();
    thisAccount.Name    = 'Test Account';
    insert thisAccount;

    Project__c thisCreationFundProject          = ct_TestDataFactory.createProject();
    insert thisCreationFundProject;

    npsp__General_Accounting_Unit__c thisGAU    = ct_TestDataFactory.createGAURecord('Creation GAU');
    thisGAU.Project__c                          = thisCreationFundProject.Id;
    insert thisGAU;

    Contact thisContact                         = ct_TestDataFactory.createContact();
    insert thisContact;
    Opportunity thisOpportunity                 = ct_TestDataFactory.createDonationPledgeOpportunity(thisContact);
    thisOpportunity.Amount                      = 700.00;
    thisOpportunity.AccountId                   = thisAccount.Id;
    insert thisOpportunity;

    List<npsp__Allocation__c> GAUAllocationsToInsert    = new List<npsp__Allocation__c>();

    npsp__Allocation__c thisGAUAllocationOne            = ct_TestDataFactory.createGAUAllocation(thisGAU.Id, thisOpportunity.Id, 100.00);

    GAUAllocationsToInsert.add(thisGAUAllocationOne);

    npsp__Allocation__c thisGAUAllocationTwo            = ct_TestDataFactory.createGAUAllocation(thisGAU.Id, thisOpportunity.Id, 600.00);
    GAUAllocationsToInsert.add(thisGAUAllocationTwo);
    insert GAUAllocationsToInsert;

   
    Test.startTest();
    
    npe01__OppPayment__c thisGEMPayment       = ct_TestDataFactory.createGEMPayment(thisOpportunity, ct_Constants.CASH_PAYMENT_METHOD);
    thisGEMPayment.npe01__Payment_Date__c     = System.Today();
    thisGEMPayment.npe01__Scheduled_Date__c   = System.Today();
    insert thisGEMPayment;
    Test.stopTest();

    thisGEMPayment.npe01__Paid__c       =  true;
    update thisGEMPayment;

    ct_GEMPaymentTriggerHelper.createNonHighValueDonationEmailMessage(thisContact.Email, thisGEMPayment.Id, thisContact.Id, thisThankYouEmailTemplate.Id, 'Test Email Body', 'Test Email Subject', orgWideAddress[0].Id);
    ct_GEMPaymentTriggerHelper.createHighValueDonationEmailMessage(thisCreationFundProject.OwnerId, thisGAUAllocationOne.Id, orgWideAddress[0].Id);
    ct_GEMPaymentTriggerHelper.updateOpportunityAcknowledgmentFields(new Set<Id>{thisOpportunity.Id});
  } 
}