/*
* Test Class - rl_DefaultGenericPlugin_Test
*
* This class tests rl_DefaultGenericPlugin, which is a class used to customise the 
* behaviour of Duplicate Check (Managed Package) merges. 
*
* Created By: Rob Liesicke (UoP)
* Created On: 8th November 2019
*
* Changes:
*
* 17/12/2019   Updated test scripts to increase code coverage
*
*/

@isTest
private class rl_DefaultGenericPlugin_Test 
{
 
   /* 
    *
    * Create test data for use by other test class methods
    *
    */
    
    @testSetup static void setupDate() 
    {
 
    // Uses unit test helper to create lead one, and updates email to make it identifiable later
    Lead leadOne = bg_UnitTestDataFactory.createEnquirer();
    leadOne.Email = 'lead@one.com';
    insert leadOne;
 
    // Uses unit test helper to create lead two, and updates email to make it identifiable later    
    Lead leadTwo = bg_UnitTestDataFactory.createEnquirer();
    leadTwo.Email = 'lead@two.com';
    leadTwo.Phone = '7 777 777';
    insert leadTwo; 

    // Uses unit test helper to create lead three, and updates first name to make it identifiable later    
    Lead leadThree = bg_UnitTestDataFactory.createEnquirer();
    leadThree.Email = '';
    leadThree.FirstName = 'Lead Three';
    insert leadThree;

    
    // Uses unit test helper to create lead four, for testing merging with phone numbers
    Lead leadFour = bg_UnitTestDataFactory.createEnquirer();
    leadFour.Email = 'lead@four.com';
    leadFour.Phone = 'o7,1-1 1(1)1.1';
    insert leadFour;
 
    // Uses unit test helper to create lead five, for testing merging with phone numbers    
    Lead leadFive = bg_UnitTestDataFactory.createEnquirer();
    leadFive.Email = 'lead@five.com';
    leadFive.Phone = '3333333';
    leadFive.MobilePhone = '07222222';
    insert leadFive;    
         
    }
    
    
   /* 
    *
    * Test that, IF number of email addresses is two or more AND master record email is not blank
    * second email address is copied to email alternative on master record
    *
    */    
  
    @isTest static void testStoreChildEmailAsAlternative()   
    {
    
    Map<String,String> fieldSourceMap = new Map<String,String>();
    
    String masterRecordId;
    
    String objectPrefix = '00Q';
    
    Sobject leadOne = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@one.com'];
    Sobject leadTwo = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@two.com'];    
    
    masterRecordId = leadOne.Id;
    
    fieldSourceMap.put('Email',leadOne.Id);
    fieldSourceMap.put('Email_Alternative__c',leadOne.Id);

    
    List<sobject> objectDataList = new List<sobject>(); 
    objectDataList.add(leadOne);
    objectDataList.add(leadTwo);
    
    dupcheck.dc3PluginModel.MergeSetFieldInput input = new dupcheck.dc3PluginModel.MergeSetFieldInput(objectPrefix,masterRecordId,fieldSourceMap,objectDataList);
    
    
    Test.startTest();
    
    rl_DefaultGenericPlugin plugin = new rl_DefaultGenericPlugin();
    
    dupcheck.dc3PluginModel.MergeSetFieldOutput output = plugin.mergeSetField(input);
   
    Test.stopTest();
  
   
    String outputEmailAlt =  '' + output.customValueMap.get('Email_Alternative__c');
    
    // Check that the email address from the child record will be populated on Email Alternative on the master record    
    system.assertEquals(outputEmailAlt, 'lead@two.com');
  }

   /* 
    *
    * Test that, IF list size is two or more AND master record email is blank
    * copy first email address to email on master record AND
    * copy second email address to email alternative on master record
    *
    */  
  @isTest static void testStoreBothChildEmailAddresses() 
  {
    Map<String,String> fieldSourceMap = new Map<String,String>();
    
    String masterRecordId;
    
    String objectPrefix = '00Q';
    
    Sobject leadOne = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@one.com'];
    Sobject leadTwo = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@two.com'];    
    Sobject leadThree = [SELECT Id, FirstName, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE FirstName = 'Lead Three'];
        
    masterRecordId = leadThree.Id;
    
    fieldSourceMap.put('Email',leadOne.Id);
    fieldSourceMap.put('Email_Alternative__c',leadOne.Id);

    
    List<sobject> objectDataList = new List<sobject>(); 
    objectDataList.add(leadOne);
    objectDataList.add(leadTwo);
    objectDataList.add(leadThree);    
    
    dupcheck.dc3PluginModel.MergeSetFieldInput input = new dupcheck.dc3PluginModel.MergeSetFieldInput(objectPrefix,masterRecordId,fieldSourceMap,objectDataList);
    
    
    Test.startTest();
    
    rl_DefaultGenericPlugin plugin = new rl_DefaultGenericPlugin();
    
    dupcheck.dc3PluginModel.MergeSetFieldOutput output = plugin.mergeSetField(input);
   
    Test.stopTest();
  
  
    String outputEmail =  '' + output.customValueMap.get('Email');   
    String outputEmailAlt =  '' + output.customValueMap.get('Email_Alternative__c');
    
    // Check that the email address from child record 1 will be populated on Email on the master record    
    system.assertEquals(outputEmail, 'lead@one.com');   
    
    // Check that the email address from child record 2 will be populated on Email Alternative on the master record    
    system.assertEquals(outputEmailAlt, 'lead@two.com');
  }
  
   /* 
    *
    * Test that, IF email list size is one AND master record email is blank
    * copy second email address to email on master record
    *
    */  
    
    @isTest static void testStoreChildEmailAddressAsEmail() 
    {
    
    Map<String,String> fieldSourceMap = new Map<String,String>();
    
    String masterRecordId;
    
    String objectPrefix = '00Q';
    
    Sobject leadOne = [SELECT Id, FirstName, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE FirstName = 'Lead Three'];
    Sobject leadTwo = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@two.com'];    
    
    masterRecordId = leadOne.Id;
    
    fieldSourceMap.put('Email',leadOne.Id);
    fieldSourceMap.put('Email_Alternative__c',leadOne.Id);

    
    List<sobject> objectDataList = new List<sobject>(); 
    objectDataList.add(leadOne);
    objectDataList.add(leadTwo);
    
    dupcheck.dc3PluginModel.MergeSetFieldInput input = new dupcheck.dc3PluginModel.MergeSetFieldInput(objectPrefix,masterRecordId,fieldSourceMap,objectDataList);
    
    
    Test.startTest();
    
    rl_DefaultGenericPlugin plugin = new rl_DefaultGenericPlugin();
    
    dupcheck.dc3PluginModel.MergeSetFieldOutput output = plugin.mergeSetField(input);
   
    Test.stopTest();
  
   
    String outputEmail =  '' + output.customValueMap.get('Email');
    
    // Check that the email address from the child record will be populated on Email Alternative on the master record    
    system.assertEquals(outputEmail, 'lead@two.com');
  }


   /* 
    *
    * Test that, phone numbers are processed and that mobiles are sorted into the mobile fields:
    * Test records have 3 numbers in total, 2 mobile and 1 phone. 
    * Outcome is that the mobile numbers are sorted into the mobile fields, and the phone number on the phone fields.
    *
    */  
    
    @isTest static void testPhoneNumberProcessing() 
    {
    
    Map<String,String> fieldSourceMap = new Map<String,String>();
    
    String masterRecordId;
    
    String objectPrefix = '00Q';
    
    Sobject leadOne = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@four.com'];
    Sobject leadTwo = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@five.com'];    
    
    masterRecordId = leadOne.Id;
    
    fieldSourceMap.put('Phone',leadOne.Id);
    fieldSourceMap.put('MobilePhone',leadOne.Id);
    fieldSourceMap.put('Phone_Alternative__c',leadOne.Id);
    fieldSourceMap.put('Mobile_Alternative__c',leadOne.Id);
    
    List<sobject> objectDataList = new List<sobject>(); 
    objectDataList.add(leadOne);
    objectDataList.add(leadTwo);
    
    dupcheck.dc3PluginModel.MergeSetFieldInput input = new dupcheck.dc3PluginModel.MergeSetFieldInput(objectPrefix,masterRecordId,fieldSourceMap,objectDataList);
    
    
    Test.startTest();
    
    rl_DefaultGenericPlugin plugin = new rl_DefaultGenericPlugin();
    
    dupcheck.dc3PluginModel.MergeSetFieldOutput output = plugin.mergeSetField(input);
   
    Test.stopTest();
  
   
    String outputMobilePhone =  '' + output.customValueMap.get('MobilePhone');
    String outputMobileAlt =  '' + output.customValueMap.get('Mobile_Alternative__c');
    String outputPhone =  '' + output.customValueMap.get('Phone');
    String outputPhoneAlt =  '' + output.customValueMap.get('Phone_Alternative__c');
                
    // Check that the email address from the child record will be populated on Email Alternative on the master record    
    system.assertEquals(outputMobilePhone, '+447111111');
    system.assertEquals(outputMobileAlt, '+447222222');   
    system.assertEquals(outputPhone, '3333333'); 
  }

   /* 
    *
    * Test that, phone numbers are processed and that mobiles are sorted into the mobile fields:
    * The child record has one mobile number stored as phone, the master has none.
    * Outcome is that the master should have mobile field populated.
    *
    */  
    
    @isTest static void testPhoneNumberProcessingTwo() 
    {
    
    Map<String,String> fieldSourceMap = new Map<String,String>();
    
    String masterRecordId;
    
    String objectPrefix = '00Q';
    
    Sobject leadOne = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@one.com'];
    Sobject leadTwo = [SELECT Id, Email, Email_Alternative__c, Phone, MobilePhone, Phone_Alternative__c, Mobile_Alternative__c FROM Lead WHERE Email = 'lead@two.com'];    
    
    masterRecordId = leadOne.Id;
    
    fieldSourceMap.put('Phone',leadOne.Id);
    fieldSourceMap.put('MobilePhone',leadOne.Id);
    fieldSourceMap.put('Phone_Alternative__c',leadOne.Id);
    fieldSourceMap.put('Mobile_Alternative__c',leadOne.Id);
    
    List<sobject> objectDataList = new List<sobject>(); 
    objectDataList.add(leadOne);
    objectDataList.add(leadTwo);
    
    dupcheck.dc3PluginModel.MergeSetFieldInput input = new dupcheck.dc3PluginModel.MergeSetFieldInput(objectPrefix,masterRecordId,fieldSourceMap,objectDataList);
    
    
    Test.startTest();
    
    rl_DefaultGenericPlugin plugin = new rl_DefaultGenericPlugin();
    
    dupcheck.dc3PluginModel.MergeSetFieldOutput output = plugin.mergeSetField(input);
   
    Test.stopTest();
  
   
    String outputMobilePhone =  '' + output.customValueMap.get('MobilePhone');
    String outputMobileAlt =  '' + output.customValueMap.get('Mobile_Alternative__c');
    String outputPhone =  '' + output.customValueMap.get('Phone');
    String outputPhoneAlt =  '' + output.customValueMap.get('Phone_Alternative__c');
                
    // Check that the email address from the child record will be populated on Email Alternative on the master record    
    system.assertEquals(outputMobilePhone, '+447777777');
  }  
  
}