/**
 * @File Name          : ct_LoggerTest.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 28/4/2020, 7:42:20 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/4/2020   Creation Admin     Initial Version
**/
@IsTest(SeeAllData = true)
public class ct_LoggerTest {
  @IsTest
  static void testRequiredFieldErrorHandling(){
    Opportunity thisOpportunity = new Opportunity();
    thisOpportunity.Name = 'Error handling test';
    thisOpportunity.CloseDate = system.today();
    test.startTest();
    try{
      insert thisOpportunity;    
    }
    catch(Exception e){
        ct_Logger.logMessage(e, 'Opportunity');
    }
    test.stopTest();
  }

  @IsTest
  static void testAnonymousExceptionErrorHandling(){
    Opportunity thisOpportunity = new Opportunity();
    test.startTest();
    try{
      insert thisOpportunity;    
    }
    catch(Exception e){
        ct_Logger.logMessage(e, 'Opportunity');
    }
    test.stopTest();
  }
}