/**
 * @File Name          : ct_LeadDuplicateResultList.cls
 * @Description        : Provides a wrapper for the list of Duplicate result
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 2/11/2020, 8:22:18 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/15/2019   Creation Admin     Initial Version
**/
public class ct_LeadDuplicateResultList {
  /**
   *  List of ct_LeadDuplicateResult Wrapper which contains Lead Duplicates that is used in flow.
   *  An Apex defined varible has to created for ct_LeadDuplicateResultList to store the duplicate lead record returned by
   *  Action ct_DuplicateCheckApexAction invoked from Flow.
  **/
  @AuraEnabled 
  public List<ct_LeadDuplicateResult> thisLeadResultList;	
}