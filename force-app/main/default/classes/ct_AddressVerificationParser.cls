/**
 * @File Name          : ct_AddressVerificationParser.cls
 * @Description        : parser class to parse the response AFD API
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 2/14/2020, 9:01:09 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/13/2020   Creation Admin     Initial Version
**/
public class ct_AddressVerificationParser {
  public class Item {
    public String Name {get;set;} 
    public String Address1 {get;set;} 
    public String Address2 {get;set;} 
    public String Address3 {get;set;} 
    public String Address4 {get;set;} 
    public String Address5 {get;set;} 
    public String Address6 {get;set;} 
    public String Address7 {get;set;} 
    public String OrganisationName {get;set;} 
    public String Department {get;set;} 
    public String SubBuilding {get;set;} 
    public String Building {get;set;} 
    public String Number_Z {get;set;} // in json: Number
    public String DependentThoroughfare {get;set;} 
    public String Thoroughfare {get;set;} 
    public String Locality {get;set;} 
    public String Town {get;set;} 
    public String Postcode {get;set;} 
    public String Principality {get;set;} 
    public String Region {get;set;} 
    public String Cedex {get;set;} 
    public String PostcodeFrom {get;set;} 
    public String Product {get;set;} 
    public String Key {get;set;} 
    public String List_Z {get;set;} // in json: List
    public String Country {get;set;} 
    public String CountryISO {get;set;} 
  }
  public String Result {get;set;} 
  public String ErrorText {get;set;} 
  public String Reason {get;set;}
  public String Other {get;set;}
  public String Status {get;set;}
  public List<Item> Item {get;set;} 
}