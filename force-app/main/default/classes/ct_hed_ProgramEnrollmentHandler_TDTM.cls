/**
 * @File Name          : ct_hed_ProgramEnrollmentHandler_TDTM.cls
 * @Description        : 
 * @Author             : Creation Admin
 * @Group              : 
 * @Last Modified By   : Umashankar Creation
 * @Last Modified On   : 5/22/2020, 9:10:47 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/17/2020   Creation Admin     Initial Version
**/
global class ct_hed_ProgramEnrollmentHandler_TDTM extends hed.TDTM_Runnable {
  global override hed.TDTM_Runnable.DmlWrapper run(List<SObject> newlist, List<SObject> oldlist,
  hed.TDTM_Runnable.Action triggerAction, Schema.DescribeSObjectResult objResult) {

    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>CT HED PROGRAM ENROLLMENT TDTM TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

    hed.TDTM_Runnable.dmlWrapper dmlWrapper = new hed.TDTM_Runnable.DmlWrapper();
    Set<Id> affiliationIds  = new Set<Id>();
    Set<Id> contactIds      = new Set<Id>();
    if (triggerAction == hed.TDTM_Runnable.Action.BeforeDelete) {
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>HED AFFILIATION BEFORE DELETE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

      for(hed__Program_Enrollment__c thisEnrollment: (list<hed__Program_Enrollment__c>)oldlist){
        if(thisEnrollment.hed__Contact__c != null){
          contactIds.add(thisEnrollment.hed__Contact__c);
        }
      }
      if(!contactIds.isEmpty() && !System.isBatch() && !System.isFuture() && !System.isQueueable() && !System.isScheduled()){
        ct_hed_ProgramEnrollmentHelper.updateLatestCourseDetailsToRelatedContact(contactIds);
      }
    }

    if(triggerAction == hed.TDTM_Runnable.Action.AfterInsert){
      System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>HED PROGRAM ENROLLMENT AFTER INSERT TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');
      for(hed__Program_Enrollment__c thisEnrollment: (list<hed__Program_Enrollment__c>)newlist){
        if(thisEnrollment.hed__Contact__c != null){
          contactIds.add(thisEnrollment.hed__Contact__c);
        }
      }
      if(!contactIds.isEmpty() && !System.isBatch() && !System.isFuture() && !System.isQueueable() && !System.isScheduled()){
        ct_hed_ProgramEnrollmentHelper.updateLatestCourseDetailsToRelatedContact(contactIds);
      }
    }
    if (triggerAction == hed.TDTM_Runnable.Action.AfterUpdate){
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>HED PROGRAM ENROLLMENT AFTER UPDATE TRIGGER<<<<<<<<<<<<<<<<<<<<<<<<<<<');

        Map<Id, hed__Program_Enrollment__c> triggerOldMap = new Map<Id, hed__Program_Enrollment__c>((list<hed__Program_Enrollment__c>)oldlist);
        Map<Id, hed__Program_Enrollment__c> triggerNewMap = new Map<Id, hed__Program_Enrollment__c>((list<hed__Program_Enrollment__c>)newlist);

        //Loop to get The required Ids 
        for(hed__Program_Enrollment__c thisEnrollment : triggerNewMap.values()){
          if(thisEnrollment.Completed__c != triggerOldMap.get(thisEnrollment.Id).Completed__c
          && thisEnrollment.Completed__c
          && thisEnrollment.hed__Affiliation__c != null){
            affiliationIds.add(thisEnrollment.hed__Affiliation__c);
          }
          if(thisEnrollment.hed__Contact__c != null
          && (thisEnrollment.hed__Graduation_Year__c != triggerOldMap.get(thisEnrollment.Id).hed__Graduation_Year__c
          || thisEnrollment.hed__Account__c != triggerOldMap.get(thisEnrollment.Id).hed__Account__c
          || thisEnrollment.hed__End_Date__c != triggerOldMap.get(thisEnrollment.Id).hed__End_Date__c)){
            contactIds.add(thisEnrollment.hed__Contact__c);
          }
        }
      if(!affiliationIds.isEmpty()){
        dmlWrapper.objectsToUpdate.addAll(ct_hed_ProgramEnrollmentHelper.updateRelatedAffiliationRoleAndStatus(affiliationIds));
      }
      if(!contactIds.isEmpty() && !System.isBatch() && !System.isFuture() && !System.isQueueable() && !System.isScheduled()){
        ct_hed_ProgramEnrollmentHelper.updateLatestCourseDetailsToRelatedContact(contactIds);
      }
    }
    return dmlWrapper;
  }
}