/**
 * @File Name          : ct_PersonalSiteContactLookupExtension.cls
 * @Description        : Extension class for PersonalSiteContactLookup page for 
 *                       implementation of dupcheck to get Contact records
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin
 * @Last Modified On   : 7/1/2020, 11:32:09 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/13/2020   Creation Admin     Initial Version
**/
public class ct_PersonalSiteContactLookupExtension {
  
  public static String contactFirstname { get; set; }
  public static String contactLastname  { get; set; }
  public static String contactEmail     { get; set; }
  public static String personalSiteURL  { get; set; }
  @TestVisible 
  public static Map<String, List<dupcheck.dc3SearchResult>> searchResult;
  public ct_PersonalSiteContactLookupExtension(GW_Volunteers.VOL_CTRL_PersonalSiteContactLookup ctrlParam) {
  } 
  
  /**
  * @description page reference method to redirect to either Personal Site or Volunteer Registration based on Dupecheck result
  * @return PageReference 
  **/
  public static PageReference personalSiteURL() {
    contactFirstname    = contactFirstname.trim();
    contactLastname     = contactLastname.trim();
    contactEmail        = contactEmail.trim();

    Integer highestScore;
    Boolean hasExistingContact = false;
    //Get Volunteer Personal site Contact Info URL From Metadata
    String siteBaseURL         = ct_MetadataService.getSiteDomainMetaDataByDeveloperName(ct_Constants.PERSONAL_SITE_CONTACT_INFO_URL);
    //Initialise dupcheck API Object to invoke the API methods
    dupcheck.dc3Api api = new dupcheck.dc3Api(); 
    //Create a sample lead data from the search wrapper passed from Contact Lookup visual force form
    Lead searchLead = new Lead(FirstName =contactFirstname, LastName = contactLastname, Email = contactEmail);
    //Duplicate Check API to get Duplicated Records with the sample lead data
    if (!Test.isRunningTest()) {
      searchResult = api.doSearch(searchLead); 
    }
    //Retrive the Lead duplicates from the Dupe API Result map using the Lead Object Record Key 003
    if(searchResult.containskey('003')){
      //Loop through the Contact Duplicates and get Existing contact if match found based on parameter passed for visualforce page 
      for (dupcheck.dc3SearchResult thisDuplicate : searchResult.get('003')) {
        if((highestScore == null
            || thisDuplicate.score > highestScore)
            && thisDuplicate.getDisplayField().get('Email') != null
            && thisDuplicate.getDisplayField().get('Email') == contactEmail
            && thisDuplicate.getDisplayField().get('FirstName') == contactFirstname
            && thisDuplicate.getDisplayField().get('LastName') == contactLastname){
              //Set the Boolen to true since a matching record is found
              hasExistingContact = true;  
              //Update the Page reference URL With Contact id and Email to Personal site Contact Page  
              personalSiteURL = siteBaseURL+'/ct_GW_Volunteers_PersonalSiteContactInfo?contactId='+String.valueOf(thisDuplicate.getDisplayField().get('Id'))+'&Email='+EncodingUtil.urlEncode(String.valueOf(thisDuplicate.getDisplayField().get('Email')), 'UTF-8');
        }
      }
    }
    //If Contact Match is not found the redirect the user to Volunteer registration Page
    if(personalSiteURL == null){
      //Get Volunteer registration Page URL from Metadata
      personalSiteURL =  ct_MetadataService.getSiteDomainMetaDataByDeveloperName(ct_Constants.VOLUNTEER_REGISTRATION_SITE_URL)+'?volunteerFirstName='+EncodingUtil.urlEncode(contactFirstname,'UTF-8')+'&volunteerLastName='+EncodingUtil.urlEncode(contactLastname, 'UTF-8')+'&volunteerEmail='+EncodingUtil.urlEncode(contactEmail, 'UTF-8');
    }
    //Set the Page reference with the URL to be redirected
    PageReference pageRef = new PageReference(personalSiteURL);
    //Set he Redirect to True to redirect the User to the required Page
    pageRef.setRedirect(true);
    return pageRef;
  }
}