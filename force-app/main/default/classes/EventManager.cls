public with sharing class EventManager {    
    
    @AuraEnabled(cacheable=true)
    public static Event fetchEventById(String eventId) {
        System.debug('EVENT ID: '+eventId);
        return [
            SELECT
            Id,
            Location,
            IsAllDayEvent,
            ActivityDateTime,
            StartDateTime,
            EndDateTime,
            DurationInMinutes,
            ActivityDate,
            EndDate,
            Description,
            IsRecurrence,
            Subject
            FROM Event
            WHERE ID = :eventId
        ];
    }
    
}