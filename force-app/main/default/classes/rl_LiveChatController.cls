/*******************************************************************************************************************************
* @name:           rl_LiveChatController
* @description:    This class is for methods relating to Live Chat funtionality
* 
*
* METHODS
* @initialisePostChat		Initialises page variables and calls method on JSON2Apex to return Case Id from Post Chat related variable
* @registerRating			Sets showSubmit variable to true, controlling visibility on VF page
* @submitForm				Creates a Post_Chat_Submission__c record to store post chat form submission
* @updateRelatedCaseRating	Called by Process Builder to update related case with information from Post_Chat_Submission__c
* @getPicklistOptions		Takes object API name and field API name and returns active picklist values
*
*
* @Created:   06-04-2020 v0.1 - initial version - Rob Liesicke
* @Updated:   27-04-2020 v0.2 - updated methods to update case using relay object so that site user doesn't need access to cases
			  21-05-2020 v1.1 - added additional functionality to capture consent preference

********************************************************************************************************************************/

public class rl_LiveChatController 
{
    public Boolean showSubmit { get; set; }
    public Boolean showForm { get; set; }  
    public Boolean submitSuccess { get; set; } 
    public String rating { get; set; }
    public String comments { get; set; }
    public String consentPreference { get; set; }
    public String caseId {get;set;}
    public String attachedRecords {get;set;}


/* =========================================================== */   
/*                  INITIALISE POST CHAT PAGE                  */
/* =========================================================== */    
    
    public void initialisePostChat (){
        rating = ''; 
        comments = '';
        consentPreference = '';
        showForm = true;
        showSubmit = false;
        submitSuccess = false;
        attachedRecords = ApexPages.currentPage().getParameters().get('attachedRecords');
        caseId = JSON2Apex.parse(attachedRecords).CaseId;
    } 
    
    
/* =========================================================== */   
/*                      SHOW SUBMIT BUTTON                     */
/* =========================================================== */     
    
    public void registerRating(){
        showSubmit = true;
        system.debug('Selected rating: '+ rating);
    }

    
/* =========================================================== */
/*       CREATE POST CHAT SUBMISSION TO STORE FORM DETAILS     */
/* =========================================================== */   
    
    public void submitForm(){
        showSubmit = false;
        showForm = false;
        submitSuccess = true;  
        
        Post_Chat_Submission__c postChatSubmission = new Post_Chat_Submission__c(); 
        
        postChatSubmission.Related_Case_Id__c = caseId;
		postChatSubmission.LiveChat_Rating__c = rating;
        postChatSubmission.LiveChat_RatingComment__c = comments;
        postChatSubmission.Marketing_Consent__c = consentPreference;
        
        insert postChatSubmission;

    }    

    
/* =========================================================== */   
/*            UPDATE LIVE CHAT RATING ON RELATED CASES         */
/* =========================================================== */    
    
    @InvocableMethod
    public static void updateRelatedCaseRating(List<Id> postChatSubmissionIds)
    {
        List<Id> caseIds = new List<Id>();
        List<Case> updatedCases = new List<Case>();
		
        
        // Get Email Survey records for provided Ids
        List<Post_Chat_Submission__c> postChatSubmissions = [ SELECT Id, LiveChat_Rating__c, LiveChat_RatingComment__c, Related_Case_Id__c 
                                                     		  FROM Post_Chat_Submission__c 
                                                     		  WHERE Id IN :postChatSubmissionIds ];
        
        
        // Extract the related case IDs into a list
        for(Post_Chat_Submission__c pcs : postChatSubmissions)
        {
            caseIds.add(pcs.Related_Case_Id__c);
        }
        
        
        // Create a map of the cases found in the case list
        Map<Id,Case> caseRecords = new Map<Id,Case>  ([ SELECT Id, LiveChat_Rating__c, LiveChat_RatingComment__c
                                   						FROM Case
                                  						WHERE Id IN :caseIds ]);
        
        
        // For each email survey, update the rating on the related case
        for(Post_Chat_Submission__c pcs : postChatSubmissions)
        {
            Case relatedCase = caseRecords.get(pcs.Related_Case_Id__c);
            
            relatedCase.LiveChat_Rating__c = pcs.LiveChat_Rating__c;
            
            relatedCase.LiveChat_RatingComment__c = pcs.LiveChat_RatingComment__c;
            
            updatedCases.add(relatedCase);
        }
        
        
        // Bulk update related cases
        try 
        {
        	update updatedCases;
        } 
        
        catch(DmlException e) 
        {
        	System.debug('The following exception has occurred: ' + e.getMessage());
		}        
        
    }

    
/* =========================================================== */    
/*            GET PICKLIST VALUES FOR SUPPLIED FIELD           */
/* =========================================================== */        
    
   @AuraEnabled
    public static List<picklistValuesWrapper> getPicklistOptions(String objectApiName, String fieldApiName) {
        
        List<picklistValuesWrapper> picklistOptions = new List<picklistValuesWrapper>();
        
        try {
            Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectApiName);
            
            Schema.DescribeSObjectResult objectDescribeResult = objectType.getDescribe();
            
            Map<String,Schema.SObjectField> objectFields = objectDescribeResult.fields.getMap();
            
            Schema.DescribeFieldResult fieldResult = objectFields.get(fieldApiName).getDescribe();
            
            List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
            
            for (Schema.PicklistEntry pickListEntry : picklistEntries) 
            {
                if(pickListEntry.isActive())
                {
                    picklistValuesWrapper thisWrapper = new picklistValuesWrapper();
                      
                    thisWrapper.label = pickListEntry.label;
                    thisWrapper.value = pickListEntry.value;
                    
                    picklistOptions.add(thisWrapper);         
                }
            }
            
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return picklistOptions;
    }
    
	//Wrapper class that holds label and value of picklist entries
	public class picklistValuesWrapper
    {
        @AuraEnabled public String label;	
        @AuraEnabled public String value;
  	}

}