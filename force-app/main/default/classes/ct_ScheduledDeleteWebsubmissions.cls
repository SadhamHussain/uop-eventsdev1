/**
 * @File Name          : ct_ScheduledDeleteWebsubmissions.cls
 * @Description        : Execute the ct_GenericDeleteBatch class by sending WebSite Submission object API 
 *                       and Duration to delete the records which are older than 1 week
 * @Author             : Creation Admin
 * @Last Modified By   : Creation Admin  - Creation Technology Solutions
 * @Last Modified On   : 5/5/2020, 12:58:06 pm
 */

global class ct_ScheduledDeleteWebsubmissions implements Schedulable{
    global void execute(SchedulableContext sc){
        Database.executeBatch(new ct_GenericDeleteBatch('Web_Site_Submission__c', 'LAST_N_DAYS: 7'));    
    }
}